<?PHP
@session_start();
$statutError = "?statut=error";

$redirectURL = str_replace('?statut=error', '', $_SERVER['HTTP_REFERER']);
$redirectURL = str_replace('&statut=error', '', $redirectURL);
$redirectURL = str_replace('&statut=success', '', $redirectURL);
$redirectURL = str_replace('?statut=success', '', $redirectURL);


$userId = $_SESSION['connected']['id'];

$isSpecial = 0;

$specialMessage = "";

$a = 0;
$b = 0;

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

$page = 1;
$pageTilte = "ajout";

if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

if ($_GET['block'] == "recharger") {
    $rechargement = "true";
}

include('files/includes/topHaut.php');

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Messages.php';

// purify POST-data
$HTML = new HTML();

//var_dump($_POST['commentaire']);

$_POST = $HTML->purifyParams($_POST);

//var_dump($_POST['commentaire']);
//die;

$_SESSION['alertLogincontent'] = "";
$_SESSION['tamponForm'] = "";

foreach ($_POST as $keys => $valeurs) {
    $_SESSION['tamponForm'][$keys] = $valeurs;
}

if ($_GET['block'] ==  "site") {


    // unset errors
    if (isset($_SESSION['new_project_duplicates_error']))
        unset($_SESSION['new_project_duplicates_error']);

    // errors for inner_pages and subdomains
    if (isset($_SESSION['error_inner_pages_subdomains']))
        unset($_SESSION['error_inner_pages_subdomains']);

    if (isset($_SESSION['error_subdomain_type']))
        unset($_SESSION['error_subdomain_type']);

    if (isset($_SESSION['error_inner_pages_type']))
        unset($_SESSION['error_inner_pages_type']);

    if (isset($_SESSION['error_annuaire']))
        unset($_SESSION['error_annuaire']);

    if (isset($_SESSION['error_legal_content']))
        unset($_SESSION['error_legal_content']);

    if (isset($_SESSION['error_legal_content_data']))
        unset($_SESSION['error_legal_content_data']);

    // clear
    //die;

    $new_domains_query = array();

    if (empty($_POST['site'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer l'url du ou des site(s) � r�f�rencer<br/>";
    } else {

        if (isMultiple($_POST['site'])) {

            $listURL = explode(PHP_EOL, $_POST['site']);
            $error = "";
            foreach ($listURL as $value) {
                $value = trim($value);
                if ($value != "" && !Functions::checkUrlFormat($value)) {
                    $error = "Le format d'une des URL entr�es est incorrecte<br/>";
                    $new_domains[] = $value;
                }
            }
            if ($error != "") {
                $_SESSION['alertLogincontent'] .= $error;
            }
        } else {
            $_POST['site'] = trim($_POST['site']);

            if (!Functions::checkUrlFormat($_POST['site'])) {
                $_SESSION['alertLogincontent'] .= "Le format de l'URL est incorrecte<br/>";
            }
        }

        if (empty($_SESSION['alertLogincontent'])){
            $new_domains_query = array();
            $url_lists = preg_split('/\r\n|[\r\n]/', $_POST['site']);
            foreach($url_lists as $url){
                $filtered_url = str_replace(array('https://','http://','www.'),'',$url);
                if (!empty($filtered_url))
                    $new_domains_query[] =  $filtered_url ;//'(`lien` LIKE "'.$filtered_url.'%")';
            }

            $sql = 'SELECT lien,
                           annuaire AS annuaire_list_id
                    FROM projets
                    WHERE proprietaire = '.$dbh->quote($idUser).'
                    AND replace(replace(replace(lien,"http://",""),"https://.",""),"www.","") REGEXP ' . $dbh->quote('^('.implode('|',$new_domains_query).')');

            $result = $dbh->query($sql);
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);

            // filtering
            $duplicates = array();
            foreach($rows as $row){
                $lien= str_replace(array('https://','http://','www.'),'',$row['lien']);
                //$duplicates[] = trim(urldecode(html_entity_decode(strip_tags($lien))));//fix "/r bug"
                $lien =  trim(urldecode(html_entity_decode(strip_tags($lien))));//fix "/r bug"
                $duplicates[$row['annuaire_list_id']] = $lien;
            }
            //$duplicates = array_unique(array_values($duplicates));


            if(count($duplicates)>0){
                $_SESSION['new_project_duplicates_error'] = true;
                $_SESSION['new_project_duplicates'] = $duplicates;

            }
        }
    }

    if (empty($_POST['annuaire'])) {
        $_SESSION['alertLogincontent'] .= "S�lectionnez la liste d'annuaires � utiliser<br/>";
    }

    if (empty($_POST['annuaireFrequence']) || $_POST['annuaireFrequence'] <= 0 || empty($_POST['jourFrequence']) || $_POST['jourFrequence'] <= 0) {
        $_SESSION['alertLogincontent'] .= "Choissisez la fr�quence de soumission de votre site.<br/>";
    }

    if (!isset($_POST['proprietaire'])) {
        $_POST['proprietaire'] = $idUser;
    } else {
        $_POST['proprietaire'] = intval($_POST['proprietaire']);
    }

    if (!isset($_POST['ancres'])) {
        $_POST['ancres'] = "";
    }

    if ($_POST['commentaire'] == '') {
        $_POST['commentaire'] = "";
    }

    // add error if all fine, except duplicates
    if (isset($_SESSION['new_project_duplicates_error']) && empty($_SESSION['alertLogincontent'])){
        $_SESSION['alertLogincontent'] = 'new_project_duplicates_error';
    }


    // checking not inner page and subdomain annuaires
    $annuaireModel = new Annuaire();
    $annuaire_list_id = intval($_POST['annuaire']);
    $error_subdomain_type = false;
    $error_inner_pages_type = false;


    if (!empty($annuaire_list_id) && !empty($_POST['site']) && !isset($_POST['subdomain_checked'])){ // empty($_SESSION['alertLogincontent'])

        $client_domains = array();

        $error_checker = array();
        $error_checker['error_subdomain_type'] = 0;
        $error_checker['error_inner_pages_type'] = 0;

        // prepare
        if (isMultiple($_POST['site'])) {
            $client_domains = preg_split('/\r\n|[\r\n]/', $_POST['site']);
        }else{
            $client_domains[] = trim($_POST['site']);
        }

        foreach($client_domains as $domain_url){
            // check has inner-pages or subdomain
            $check_status = false;

            // subdomain
            $parsed_url = parse_url($domain_url);
            $host = $parsed_url['host'] ? $parsed_url['host'] : array_shift(explode('/', $parsed_url['path'], 2));
            $host_data = array_filter(explode('.',$host));

            if (count($host_data)>2){
                $first_part = reset($host_data);
                if ( (count($host_data)==3) && ($first_part !='www')){
                    $error_checker['error_subdomain_type']++;
                }
                if (count($host_data)>3)
                    $error_checker['error_subdomain_type']++;
            }


            // check inner-pages
            if (isset($parsed_url['path']) && (!empty($parsed_url['host']))){
                $path = $parsed_url['path'];

                $path_parts = array_filter(explode('/',$path));
                if (count($path_parts)>0){
                    $error_checker['error_inner_pages_type']++;
                }
            }
        }

        // check status errors
        if ($error_checker['error_subdomain_type']>0){
            $error_subdomain_type = true;
        }

        if ($error_checker['error_inner_pages_type']>0){
            $error_inner_pages_type = true;
        }


        //var_dump($error_subdomain_type,$error_inner_pages_type);


        $not_allowed_annuaires = $annuaireModel->checkAllowedInnerPagesAndSubdomains($annuaire_list_id);

        //var_dump($not_allowed_annuaires);

        if (!empty($not_allowed_annuaires) && ($error_subdomain_type || $error_inner_pages_type)){
            $annuaire = Functions::getAnnuaireNameById($annuaire_list_id);
            $_SESSION['error_annuaire'] = $annuaire['libelle'];
            $_SESSION['error_inner_pages_subdomains'] = true;
            $_SESSION['error_inner_pages_subdomains_data'] = $not_allowed_annuaires;
            $_SESSION['error_subdomain_type'] = $error_subdomain_type;
            $_SESSION['error_inner_pages_type'] = $error_inner_pages_type;
        }

    }


    // check annuaires
    if (empty($_SESSION['alertLogincontent']) && !empty($annuaire_list_id) && !empty($_POST['site']) && !isset($_POST['legal_checked'])){

        $legal_annuaires = $annuaireModel->checkLegalAnnuaires($annuaire_list_id);

        if (!empty($legal_annuaires)){
            $annuaire = Functions::getAnnuaireNameById($annuaire_list_id);
            $_SESSION['error_annuaire'] = $annuaire['libelle'];
            $_SESSION['error_legal_content'] = true;
            $_SESSION['error_legal_content_data'] = $legal_annuaires;
        }


    }



    //var_dump($_SESSION['error_inner_pages_subdomains'],$_SESSION['error_legal_content']); die;

    // add error if all fine, except not inner page and subdomain annuaires
    if (isset($_SESSION['error_inner_pages_subdomains']) && empty($_SESSION['alertLogincontent'])){
        $_SESSION['alertLogincontent'] = 'error_inner_pages_subdomains';
    }


    // add error if all fine, except not inner page and subdomain annuaires
    if (isset($_SESSION['error_legal_content']) && empty($_SESSION['alertLogincontent']) && (empty($_SESSION['error_inner_pages_subdomains']))){
        $_SESSION['alertLogincontent'] = 'error_legal_content';
    }

    if ( ($_SESSION['alertLogincontent'] == "") && (!isset($_SESSION['error_legal_content'])) ) {
        $frequence = intval($_POST['annuaireFrequence']) . "x" . intval($_POST['jourFrequence']);

        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isWebmaster($idTypeUser)) {

            /* @deprecated
              $sql = 'INSERT INTO projets VALUES("", "0", "", "' . $_POST['site'] . '", "", "' . $_POST['proprietaire'] . '", "' . $frequence . '", "0", "' . $_POST['annuaire'] . '", "' . addslashes($_POST['commentaire']) . '", "' . addslashes($_POST['ancres']) . '", "0", "' . time() . '", "0","0","0","0","0","0","0", "0","1","")';
              $dbh->query($sql);
             */

            // purify
            $HTML = new HTML();
            $_POST = $HTML->purifyParams($_POST);

            // prepared
            $stmt = $dbh->prepare('INSERT INTO projets
                               VALUES(null, "0", "", :site , "", :proprietaire, :frequence, "0", :annuaire, :commentaire,
                                :ancres, "0", :time, "0","0","0","0","0","0","0", "0","1",:created,0,0,0,0,0,0,0,0)');

            $stmt->bindParam(':site', $_POST['site']);
            $stmt->bindParam(':proprietaire', $_POST['proprietaire']);
            $stmt->bindParam(':frequence', $frequence);
            $stmt->bindParam(':annuaire', $_POST['annuaire']);
            $stmt->bindParam(':commentaire', $_POST['commentaire']);
            $stmt->bindParam(':ancres', $_POST['ancres']);
            $stmt->bindParam(':time', time());
            $stmt->bindParam(':created', date('Y-m-d H:i:s'));

            $status = $stmt->execute();

            $query = $dbh->query("SELECT id FROM projets WHERE proprietaire = '" . $idUser . "' ORDER BY id DESC LIMIT 1")->fetch();

            // insert related annuaires with project
            if ($query['id'] && isset($_POST['subdomain_checked']) && !empty($_POST['subdomains'])){
                $items = $_POST['subdomain_ids'];
                $project_id = $query['id'];
                $annuaireModel->insertActiveStatusProjectAnnuaires($items,$project_id,'subdomain');
            }

            if ($query['id'] && isset($_POST['legal_checked']) && !empty($_POST['legal_ids'])){
                $items = $_POST['legal_ids'];
                $project_id = $query['id'];
                $annuaireModel->insertActiveStatusProjectAnnuaires($items,$project_id,'legal');
            }

            if ($query['id']) {
                header("location:../ancres.html?id=" . $query['id']);
                exit;
            }
        }
    }
}

if ($_GET['block'] == "ancres") {

    $HTML = new HTML;

    $_GET['id'] = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if (!isReferer() && $_GET['id'] > 0) {
        $user_Requte = "WHERE proprietaire = '" . $idUser . "'";
        if (isSu()) {
            $user_id = "";
        }
        foreach ($_POST as $key => $value) {
            $keys = explode("_", $key);
            if (!empty($value) || $value != NULL) {
                //purify
                $value = $HTML->purifyParams($value);

                $stmt = $dbh->prepare('INSERT INTO ancres
                                   VALUES("", :projet, "", :annuaire, :ancre, :webmasterID, :updated, :created)');

                $stmt->bindParam(':projet', $keys[0]);
                $stmt->bindParam(':annuaire', $keys[1]);
                $stmt->bindParam(':ancre', $value);
                $stmt->bindParam(':webmasterID', $idUser);
                $stmt->bindParam(':updated', time());
                $stmt->bindParam(':created', time());

                $status = $stmt->execute();
//                $query = $dbh->query("SELECT id FROM projets " . $user_Requte . " ORDER BY id DESC LIMIT 1")->fetch();
//                if ($status && $query['id']) {
//                    header("location:../emailpro.html?id=" . $query['id']);
                header("location:../emailpro.html?id=" . $_GET['id'] . "");
//                }
            }
        }
    }
}

if ($_GET['block'] == "emailpro") {
    $_GET['id'] = isset($_GET['id']) ? intval($_GET['id']) : 0;
    if (!isReferer() && $_GET['id'] > 0) {
        $url = "./compte/accueil.html?section=nostart";
        $specialMessage = "F�licitation pour votre nouveau projet, il ne reste plus qu'� le d�marer.";
        $lienButton = $url;
        $texteButton = "D�marer mon r�f�rencement";
        $widthButton = "200px";
    }
}


if ($_GET['block'] == "demandepaiement") {

    if (empty($_POST['montant'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un montant<br/>";
    } else {
        if ($_POST['montant'] > $solde) {
            $_SESSION['alertLogincontent'] .= "Le montant ne doit pas d�passer le solde de votre compte.<br/>";
        } else if ($_POST['montant'] < $limitePaiement) {
            $_SESSION['alertLogincontent'] .= "Le montant minimum doit �tre de " . $limitePaiement . " " . $_SESSION['devise'] . "<br/>";
        }
    }

    if (!refererCanAsk()) {
        $_SESSION['alertLogincontent'] .= "Vous ne pouvez plus faire de demande de paiement ce mois<br/>";
    } else {
        if (refererHavePay()) {
            $_SESSION['alertLogincontent'] .= "Vous avez d�ja une demande de paiement en attente<br/>";
        } else {

            if ($solde < $limitePaiement) {
                $_SESSION['alertLogincontent'] .= "Le solde de votre compte est insuffisant pour faire une demande de paiement.<br/>";
            }
        }
    }


    if ($_SESSION['alertLogincontent'] == "") {
        if (isReferer($idTypeUser)) {
            $setUser = false;
            $setUser = $dbh->query('INSERT INTO requestpayment (id,type,requesterID,requestAmount,answer,time) VALUES("", "' . $_SESSION['connected']['typeutilisateur'] . '", "' . $_SESSION['connected']['id'] . '",  "' . floatval($_POST['montant']) . '", "0", "' . time() . '")');

            if ($setUser) {
//                $userInfos = Functions::getUserInfos($_SESSION['connected']['id']);
                $name = $_SESSION['allParameters']['logotext']["valeur"];
                $urlSite = $_SESSION['allParameters']['url']["valeur"];
                $EmailSite = $_SESSION['allParameters']['email']["valeur"];

                @require_once('./files/includes/phpmailer/class.phpmailer.php');
                $body = '<br/>';
                $body .= 'Demande de paiement de ' . $_SESSION['connected']['nom'] . ' ' . $_SESSION['connected']['prenom'] . '<br/>';
                $body .= 'Montant : ' . $_POST['montant'] . " Dollar(s)<br/><br/>";

                $mail = new PHPMailer();
                $mail->Timeout = '30';
                $mail->CharSet = 'UTF-8';
                $mail->From = "noreply@ereferer.fr";
                $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
                $mail->Subject = $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom'] . " souhaite se faire payer " . $_POST['montant'] . " Dollar(s)";
                $mail->AltBody = "";
                $mail->IsHTML(true);
                $mail->MsgHTML($body);
                $mail->AddAddress($EmailSite, "");
                $mail->Send();
            }
        }
    }
}

if ($_GET['block'] == "recharger") {

    $HTML = new HTML;
    $_POST['montant'] = $HTML->purifyParams($_POST['montant']);

    if (empty($_POST['montant'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un montant<br/>";

    } else {
        if ($_POST['montant'] < $chargeMinimale) {
            $_SESSION['alertLogincontent'] .= "Le montant minimum doit �tre de " . $chargeMinimale . " " . $_SESSION['devise'] . "<br/>";
        }
    }

    if (empty($_SESSION['alertLogincontent'])) {

        if (isWebmaster($idTypeUser)) {

            $taxes = "0.00";
            $tvaValue = $_SESSION['allParameters']['tax']["valeur"];
            $montantBrute = $_POST['montant'];

            /* OLD METHOD
            if (!isset($_POST['tax']) || (isset($_POST['tax']) && intval($_POST['tax']) == 0)) {
                $tvaValue = $_SESSION['allParameters']['tax']["valeur"];
            } 
            NEW METHOD
            */
            
            $user = Functions::getUserInfos($idUser);

            $europe[]="Allemagne";
            $europe[]="Autriche";
            $europe[]="Belgique";
            $europe[]="Bulgarie";
            $europe[]="Chypre";
            $europe[]="Croatie";
            $europe[]="Danemark";
            $europe[]="Espagne";
            $europe[]="Estonie";
            $europe[]="Finlande";
            $europe[]="Grece";
            $europe[]="Hongrie";
            $europe[]="Irlande";
            $europe[]="Italie";
            $europe[]="Lettonie";
            $europe[]="Lituanie";
            $europe[]="Luxembourg";
            $europe[]="Malte";
            $europe[]="Pays_Bas";
            $europe[]="Pologne";
            $europe[]="Portugal";
            $europe[]="Replublique_Tcheque";
            $europe[]="Roumanie";
            $europe[]="Royaume_Uni";
            $europe[]="Slovaquie";
            $europe[]="Slovenie";
            $europe[]="Suede";

            if($user['country']=="France" || empty($user['country'])){ 

                $tvaValue = $_SESSION['allParameters']['tax']["valeur"];

            } elseif(in_array($user['country'], $europe) && !empty($user['numtva'])){

                $tvaValue = 0;

            } elseif(!in_array($user['country'], $europe) && $user['country']!="France"){

                $tvaValue = 0;

            }

            $taxes = $_POST['montant'] * 0.01 * $tvaValue;

            $isDemande = Functions::getPaiementByIdUser($idUser);
            $setUser = false;

            // change "comma" to "point"
            if (strpos($_POST['montant'],',')!== false){
                $_POST['montant'] = str_replace(',','.',$_POST['montant']);
            }

            $buttonPaypal = $_POST['montant'];

//            if (isset($isDemande['requestAmount']) && $isDemande['requestAmount'] != NULL) {
////                $_POST['montant'] = $_POST['montant'] + $isDemande['requestAmount'];
//                $setUser = $dbh->query('UPDATE requestpayment SET requestAmount = "' . floatval($_POST['montant']) . '" WHERE id=' . $isDemande['id']);
//            } else {


            /* @deprecated
              $setUser = $dbh->query('INSERT INTO requestpayment (id,type,requesterID,requestAmount,tva,answer,time) VALUES("", "4", "' . $_SESSION['connected']['id'] . '", "' . floatval($_POST['montant']) . '", "' . $tvaValue . '", "0", "' . time() . '")');
             */

            // prepared
            $stmt = $dbh->prepare('INSERT INTO requestpayment (type,requesterID,requestAmount,tva,answer,time)
                                   VALUES("4", :connected, :montant ,:tva, "0", :time )');

            $stmt->bindParam(':connected', $_SESSION['connected']['id']);
            $stmt->bindParam(':montant', floatval($_POST['montant']));
            $stmt->bindParam(':tva', $tvaValue);
            $stmt->bindParam(':time', time());

            $setUser = $stmt->execute();
            //            }
            if ($setUser) {
                $idRequete = 0;
                $idRequeteSelect = $dbh->query('SELECT id FROM requestpayment WHERE requesterID = ' . $dbh->quote($idUser) . ' ORDER BY id DESC LIMIT 1')->fetch();
                $idRequete = $idRequeteSelect['id'];
//                $specialMessage = "Merci pour votre demande de rechargement. <br/>Une notification vous a �t� envoy�e.<br/> Une facture vous sera d�livr�e sous 24h.";
                $specialMessage = "Merci pour votre demande de rechargement. <br/>Une notification vous a �t� envoy�e.<br/> Une facture vous sera d�livr�e sous 24h.<br/> Patientez, vous serez redirig� vers paypal dans 5 secondes....";

//                $userInfos = Functions::getUserInfos($_SESSION['connected']['id']);
                $name = $_SESSION['allParameters']['logotext']["valeur"];
                $urlSite = $_SESSION['allParameters']['url']["valeur"];
                $EmailSite = $_SESSION['allParameters']['email']["valeur"];

                @require_once('./files/includes/phpmailer/class.phpmailer.php');
                $body = '<br/>';
                $body .= 'Demande de rechargement de ' . $_SESSION['connected']['nom'] . ' ' . $_SESSION['connected']['prenom'] . '<br/>';
                $body .= 'Montant : ' . $_POST['montant'] . " Euro(s)<br/><br/>";

                $mail = new PHPMailer();
                $mail->Timeout = '30';
                $mail->CharSet = 'UTF-8';
                $mail->From = "noreply@ereferer.fr";
                $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
                $mail->Subject = $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom'] . " souhaite recharger " . $_POST['montant'] . " Euro(s) sur son compte";
                $mail->AltBody = "";
                $mail->IsHTML(true);
                $mail->MsgHTML($body);
                $mail->AddAddress($EmailSite, "");
                $mail->Send();
            }
        }
    }
}

//obtenir des cr�dits
if ($_GET['block'] == "obtenir") {
    if ($_POST['credits']) {
        $client = Functions::getUserInfos($_SESSION['connected']['id']);

        $max = floor($client['solde']/$_SESSION['allParameters']['prix_achat_credit']['valeur']);
        $coutcredit = $_SESSION['allParameters']['prix_achat_credit']['valeur'];
        $deb = $coutcredit * $_POST['credits'];

        if($_POST['credits'] > $max){
             $_SESSION['alertLogincontent'] .= "Votre solde est insuffisant";
        } else {
            if(is_numeric($_POST['credits'])){
                $credit = $_POST['credits'];
                $dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` + ".$credit."), `solde` = (`solde` - ".$deb.") WHERE `id` = ".$_SESSION['connected']['id']);

                $transaction = new Transaction($dbh, $userId);
                $transaction->addTransaction("Obtenir " . $credit . " des cr�dits", 0, $deb);
            }
        }
    }

    if($_POST['credit_auto']=="on"){
        $dbh->query("UPDATE `utilisateurs` SET `credit_auto` = '1' WHERE `id` = ".$_SESSION['connected']['id']);
    } else {
        $dbh->query("UPDATE `utilisateurs` SET `credit_auto` = '0' WHERE `id` = ".$_SESSION['connected']['id']);
    }
}


//obtenir des cr�dits
if ($_GET['block'] == "echanger") {
    if ($_POST['credits']) {
        $client = Functions::getUserInfos($_SESSION['connected']['id']);
        $max = $client['credit'];
        $coutcredit = $_SESSION['allParameters']['prix_achat_credit']['valeur'];
        $cred = $coutcredit * $_POST['credits'];
        if($_POST['credits']>$max){
             $_SESSION['alertLogincontent'] .= "Votre solde est insuffisant";
        } else {
            if(is_numeric($_POST['credits'])){
                 $credit = $_POST['credits'];
                 $dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` - ".$credit."), `solde` = (`solde` + ".$cred.") WHERE `id` = ".$_SESSION['connected']['id']);

                $transaction = new Transaction($dbh, $userId);
                $transaction->addTransaction("�change " . $credit . " des cr�dits", $cred, 0);
            }
        }
    }
}


if ($_GET['block'] == "message") {
    $HTML = new HTML;

    if (empty($_POST['destinataire'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez choisir le destinataire du message<br/>";
    }

    if (empty($_POST['objet']) || $_POST['objet'] == "Objet du message") {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer l'objet du message<br/>";
    }

    if (empty($_POST['message']) || $_POST['message'] == 'Contenu du message') {
        $_SESSION['alertLogincontent'] .= "Vous devez entrer une message<br/>";
    }

    // purify
    $_POST['objet'] = $HTML->purifyParams($_POST['objet']);
    $_POST['message'] = $HTML->purifyParams($_POST['message']);

    if ($_SESSION['alertLogincontent'] == "") {
        $setUser = false;
        $current_time = time();
        @require_once('./files/includes/phpmailer/class.phpmailer.php');

        $mail = new PHPMailer();
        $messages = new Messages($dbh, $mail);

        $sendMessage = isset($_POST['sendMessage']) ? true:false;
        $objet = !empty($_POST['objet']) ? $_POST['objet']:'';
        $message = !empty($_POST['message']) ? $_POST['message']:'';
        $body = '';

        if (preg_match("#ALL#", $_POST['destinataire'])) {
            $whos = preg_replace("ALL", "", $_POST['destinataire']);
//            echo $whos;
            if ($whos == 0) {
                $requete = 'SELECT id FROM utilisateurs WHERE typeutilisateur > 1 ORDER BY id DESC';
            } else {
                $requete = 'SELECT id FROM utilisateurs WHERE typeutilisateur = ' . $dbh->quote($whos) . ' ORDER BY id DESC';
            }
            $execution = $dbh->query($requete);

            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {

                $userInfos = Functions::getUserInfos($retour['id']);
                if ($sendMessage) {
                    $body = $messages->getPreparedMessage($userInfos, $message);
                } else {
                    $body = $messages->getSimpleMessage();
                }

                $setUser = $messages->addNewMassage(
                        [
                                'objet' => $objet,
                                'message' => $sendMessage ? $body:$message,
                                'connected' => $_SESSION['connected']['id'],
                                'destinataire' => $retour['id'],
                                'current_time' => $current_time,

                        ]
                );

                if ($setUser) {
                    $name = $_SESSION['allParameters']['logotext']["valeur"];
                    $urlSite = $_SESSION['allParameters']['url']["valeur"];
                    $EmailSite = $_SESSION['allParameters']['email']["valeur"];

                    $subject = $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom'] . " vous a envoy&eacute; un message.";
                    $messages->sendMessage($name, $subject, $userInfos['email'], $body);
                }
            }
        } else {
            $userInfos = Functions::getUserInfos($_POST['destinataire']);

            if ($sendMessage) {
                $body = $messages->getPreparedMessage($userInfos, $message);
            } else {
                $body = $messages->getSimpleMessage();
            }

            $setUser = $messages->addNewMassage(
                [
                    'objet' => $objet,
                    'message' => $sendMessage ? $body:$message,
                    'connected' => $_SESSION['connected']['id'],
                    'destinataire' => $_POST['destinataire'],
                    'current_time' => $current_time,

                ]
            );

            if ($setUser) {
                $name = $_SESSION['allParameters']['logotext']["valeur"];
                $urlSite = $_SESSION['allParameters']['url']["valeur"];
                $EmailSite = $_SESSION['allParameters']['email']["valeur"];

                $subject = $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom'] . " vous a envoy&eacute; un message.";
                $recipient = ($userInfos['typeutilisateur'] == 1) ? $EmailSite:$userInfos['email'];
                $messages->sendMessage($name, $subject, $recipient, $body);
            }
        }
    }
}


if ($_GET['block'] == "ajouterannuaire" || $_GET['block'] == "ajouterannuaire_") {
    if ($_GET['block'] == "ajouterannuaire_") {
        $statutError = "&statut=error";
    }

    if (empty($_POST['annuaire'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir l'annuaire � ajouter.<br/>";
    } else {
        if (!Functions::checkUrlFormat($_POST['annuaire'])) {
            $_SESSION['alertLogincontent'] .= "Le format de l'url est incorrect.<br/>";
        } else {
            $resultDoublon = Functions::getDoublon("annuaires", "annuaire", addslashes($_POST['annuaire']));
            if ($resultDoublon) {
                $_SESSION['alertLogincontent'] .= "Cet annuaire existe d�ja dans les r�pertoires.<br/>";
            }
        }
    }


    if ($_SESSION['alertLogincontent'] == "") {

        $retour = 1;
        if (isset($_POST['retour']) && $_POST['retour'] == 1) {
            $retour = 1;
        }
        if (isWebmaster($idTypeUser)) {
            $who = $idUser;
        }
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        }

        $page_rank = !empty($_POST['page_rank']) ? intval($_POST['page_rank']) : 0;
        $webmasterPartenaire = !empty($_POST['webmasterPartenaire']) ? intval($_POST['webmasterPartenaire']) : 0;
        $WebPartenairePrice = !empty($_POST['WebPartenairePrice']) ? floatval($_POST['WebPartenairePrice']) : 0;
        $display = !empty($_POST['ancre']) ? intval($_POST['ancre']) : 0;
        $consignes = !empty($_POST['consignes']) ? $_POST['consignes'] : "";
        $displayAncre = !empty($_POST['ancre']) ? intval($_POST['ancre']) : 0;
        $ComptePersoWebmaster = !empty($_POST['poss']) ? intval($_POST['poss']) : 0;
        $consignesAncre = !empty($_POST['consignesAncre']) ? $_POST['consignesAncre'] : "";

//        echo $consignes;
        $ajoutannuaire = false;
        $ajoutannuaire = $dbh->query('INSERT INTO annuaires '
                . '(id,annuaire,page_rank,tarifW,tarifR,importedBy,active,display,webmasterAncre,'
                . 'webmasterConsigne,webmasterPartenaire,WebPartenairePrice,ComptePersoWebmaster,consignes,tf,cp,age,tdr,tb) '
                . 'VALUES("", "' . trim($_POST['annuaire']) . '", "' . $page_rank . '",'
                . '"' . floatval($_POST['tarifW']) . '","' . floatval($_POST['tarifR']) . '", '
                . '"' . $who . '", "' . $retour . '","' . $display . '","' . $displayAncre . '",'
                . '"' . $consignesAncre . '" , "' . $webmasterPartenaire . '","' . $WebPartenairePrice . '","' . $ComptePersoWebmaster . '", "' . addslashes($consignes) . '", "' . intval($_POST['tf']) . '",'
                . '"' . intval($_POST['cp']) . '","' . strtotime($_POST['age']) . '","' . intval($_POST['tdr']) . '",'
                . '"' . intval($_POST['tb']) . '")');
        if (isset($_POST['repertoire']) && !empty($_POST['repertoire'][0]) && $ajoutannuaire) {
            $lastIdAnnuaire = $dbh->query('SELECT * FROM annuaires WHERE importedBy ="' . $who . '" ORDER BY id DESC LIMIT 1')->fetch();
            if (isset($lastIdAnnuaire['id'])) {
                foreach ($_POST['repertoire'] as $key => $value) {
                    if ($value != "") {
                        $value = intval($value);
                        $getAnnuairesList = Functions::getAnnuaireNameById($value);
                        if (isset($getAnnuairesList['id'])) {
                            $annuaire = $getAnnuairesList['annuairesList'] . $lastIdAnnuaire['id'] . ";";
                            $dbh->query('UPDATE annuaireslist SET annuairesList = "' . $annuaire . '" WHERE id =' . $getAnnuairesList['id']);
                        }
                    }
                }
            }
        }
        if ($_GET['block'] == "ajouterannuaire_") {
            $_SESSION['alertLogincontent'] = "Annuaire ajout� avec succ�s.";
            $isSpecial = 1;
        }
        if ($_GET['block'] == "ajouterannuaire") {
            
        }
    }
}

if ($_GET['block'] == "utilisateur") {


    if (empty($_POST['nom']) || !Functions::checkNameFormat($_POST['nom'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un nom valide.<br/>";
    }

    if (empty($_POST['prenom']) || !Functions::checkNameFormat($_POST['prenom'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un ou des pr�nom(s) valide(s).<br/>";
    }

    if (isset($_POST['typeutilisateur']) && empty($_POST['typeutilisateur'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez obligatoirement choisir le type d'utilisateur.<br/>";
    }

    if (empty($_POST['email']) || !Functions::checkEmailFormat($_POST['email'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez obligatoirement saisir une adresse email valide.<br/>";
    } else {
        if (Functions::isEmailExist($_POST['email'])) {
            $_SESSION['alertLogincontent'] .= "Cette adresse email existe d�j� dans la base.<br/>";
        }
    }

    if (empty($_POST['pass']) || empty($_POST['confpass'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer le mot de passe.<br/>";
    } else {
        if ($_POST['pass'] != $_POST['confpass']) {
            $_SESSION['alertLogincontent'] .= "Les mots de passe ne concordent pas.<br/>";
        }
    }

    if (!empty($_POST['societe'])) {
        if (!Functions::checkNameFormat($_POST['societe'])) {
//            $_SESSION['alertLogincontent'] .= "Veuillez saisir le nom de votre soci�t� <br/>";
        }
    } else {
        $_POST['societe'] = "";
    }

    if (!empty($_POST['adresse'])) {
        if (strlen($_POST['adresse']) < 5) {
//            $_SESSION['alertLogincontent'] .= "Veuillez saisir une adresse compl�te <br/>";
        }
    } else {
        $_POST['adresse'] = "";
    }

    if (!empty($_POST['telephone']) && is_int($_POST['telephone']) && strlen($_POST['telephone']) < 8) {
//        $_SESSION['alertLogincontent'] .= "Veuillez saisir un num�ro de t�l�phone valide.Evitez les espaces.<br/>";
    }


    if (!empty($_POST['siteweb']) && Functions::checkUrlFormat($_POST['siteweb'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir l'url correcte de votre site<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {

        $active = 1;
        $devise = 1;
        $contractAccepted = 0;
        $soldeAct = 0;
        $frais = 0;
        $typeutilisateur = 3;
        $annuaire_writer = 0;
        $redaction_writer = 0;

        if ($_POST['typeutilisateur'] == 3) {
            $annuaire_writer = isset($_POST['annuaire_writer'])? 1 : 0;
            $redaction_writer = isset($_POST['redaction_writer'])? 1 : 0;

            // by default
            if (!$annuaire_writer && !$redaction_writer){
                // by default
                $annuaire_writer = 1;
                $redaction_writer = 0;
            }
        }

        if ($_POST['typeutilisateur'] == 4) {
            $devise = 2;
            $typeutilisateur = 4;
            $frais = 0;
        }

        //var_dump($annuaire_writer,$redaction_writer);
        //die;

        if ((isSu($idTypeUser) || isAdmin($idTypeUser))) {
            $dbh->query('INSERT INTO utilisateurs (nom,prenom,societe,email,emailsecondaire,username,password,lastlogin,joinTime,adresse,telephone,siteweb,solde,frais,devise,typeutilisateur,active,contractAccepted,annuaire_writer,redaction_writer)
                         VALUES("' . $_POST['nom'] . '", "' . $_POST['prenom'] . '", "' . $_POST['soceite'] . '", "' . $_POST['email'] . '", "' . $_POST['email'] . '", "' . $_POST['email'] . '", "' . md5($_POST['pass']) . '", "' . time() . '", "' . time() . '", "' . $_POST['adresse'] . '", "' . intval($_POST['telephone']) . '", "' . $_POST['siteweb'] . '", "' . $soldeAct . '", "' . $frais . '", "' . $devise . '", "' . $typeutilisateur . '", "' . $active . '", "' . $contractAccepted . '",  ' . $annuaire_writer. ',  "' . $redaction_writer . '" ) ');
        }
    }
}

if ($_GET['block'] == "commentaire") {

    if (isset($_SESSION['ajout_commentaire_error_words_count'])){
        unset($_SESSION['ajout_commentaire_error_words_count']);
        unset($_SESSION['ajout_commentaire_required_words_count']);
        unset($_SESSION['ajout_commentaire_passed_words']);
    }

    if (!empty($_POST['redir'])) {
        $_POST['redir'] = urldecode($_POST['redir']);
    }

    if (!isset($_POST['annuaire']) || !isset($_POST['projet'])) {
        if (!empty($_POST['redir'])) {
            header('location: ' . $_POST['redir']);
        }
    }

    if (empty($_POST['commentaire'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un commentaire ou une description. <br/>";
    }


    if (empty($_SESSION['alertLogincontent']) && isReferer()){
        $project_id = intval($_POST['projet']);
        $annuaire_id = intval($_POST['annuaire']);

        // $project_id = intval($_POST['projet']);
        if ($project_id && $annuaire_id){
            $project =  Functions::getCommandeByID($project_id);
            $annuaire_list_id = $project['annuaire'];

            // get annuairelist-data
            $annuaire_list = Functions::getAnnuaireNameById($project['annuaire']);
            $LIST_ANNUAIRE_WORDS_COUNT = $annuaire_list['words_count'];

            // if webmaster set that value
            if ($LIST_ANNUAIRE_WORDS_COUNT > 0 ) {

                // get annuaire-data
                $annuaire = Functions::getAnnuaire($annuaire_id);
                $annuaire_max_words = $annuaire['max_words_count'];
                $annuaire_min_words = $annuaire['min_words_count'];


                // overrided asked by user to max annuaire
                if ($LIST_ANNUAIRE_WORDS_COUNT > $annuaire_max_words) {
                    $LIST_ANNUAIRE_WORDS_COUNT = $annuaire_max_words;
                }

                if ($LIST_ANNUAIRE_WORDS_COUNT < $annuaire_min_words) {
                    $LIST_ANNUAIRE_WORDS_COUNT = $annuaire_min_words;
                }

                //  if annuare max_words not zero
                if ($LIST_ANNUAIRE_WORDS_COUNT > 0) {
                    $comment = $_POST['commentaire'];
                    $text_words = preg_split('/\s+/', $comment);
                    $text_words = array_filter($text_words);
                    $words_count = count($text_words);

                    $accepted_percent = 0.9;
                    $total_percent = ($words_count / $LIST_ANNUAIRE_WORDS_COUNT);

                    // var_dump($words_count,$LIST_ANNUAIRE_WORDS_COUNT,$total_percent);
                    // die;
                    //var_dump($total_percent);

                    if ($total_percent < 0.9) {
                        $_SESSION['ajout_commentaire_error_words_count'] = true;
                        $_SESSION['ajout_commentaire_required_words_count'] = $LIST_ANNUAIRE_WORDS_COUNT;
                        $_SESSION['ajout_commentaire_passed_words'] = $words_count;
                        $_SESSION['alertLogincontent'] = 'error';
                    }

                }

            }


        }

    }

    if ($_SESSION['alertLogincontent'] == "") {
        $_POST['annuaire'] = intval($_POST['annuaire']);
        $_POST['projet'] = intval($_POST['projet']);

        /*
        echo '<pre>';
        var_dump($_POST);
        echo '</pre>';
        die;*/

        if ((isSu($idTypeUser) || isAdmin($idTypeUser) || isWebmaster() || isReferer()) && $_POST['annuaire'] > 0 && $_POST['projet'] > 0) {

            $added = $dbh->query('INSERT INTO commentaires VALUES(null, "' . $_POST['projet'] . '", "' . $_POST['annuaire'] . '", "' . $idUser . '", "' . $idTypeUser . '", "' . addslashes($_POST['commentaire']) . '", "0", "' . time() . '")');

            // insert message about comments for writer
            if (isWebmaster()) {
                $project_id = $_POST['projet'];
                $annuaire_id = $_POST['annuaire'];
                $project_data = Functions::getProjetByID($project_id);
                $annuaire_data = Functions::getAnnuaire($annuaire_id);

                // funcking code
                $comments = Functions::getAllCommentaire($project_id,$annuaire_id);

                $writer_id = $project_data['affectedTO'];
                foreach ($comments as $keys => $values) {
                    if ($keys != "common") {
                        if (isset($comments[$keys]) && $comments[$keys]['commentaire'] != "") {
                            // get writer_id
                            if ($keys == 3) {
                                $writer_id = $comments[$keys]['user'];
                            }
                        }
                    }
                }

                $project_name = str_replace('http://', '', $project_data['lien']);
                $annuaire_name = str_replace('http://', '', $annuaire_data['annuaire']);
                $object = 'Commentaire re�u pour le projet ' . $project_name . ' sur ' . $annuaire_name;
                $message = "Bonjour,\n\nVous avez re�u un commentaire pour votre texte:\n" . addslashes($_POST['commentaire']);
                $admin_id = 1;
                $sql = 'INSERT INTO messages VALUES("", "' . $object . '","' . $message . '", "' . $admin_id . '", "' . $writer_id . '", "1", "1", "1", "' . time() . '")';
                $result_inserting = $dbh->query($sql);
            }
        }

        /*
        var_dump($added);
        die; */

        if (!empty($_POST['redir']) || $added !== true) {
            header('location: ' . $_POST['redir']);
        }
    }
}

if ($_GET['block'] == "listannuaire") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $who = 0;
    } else {
        $who = $idUser;
    }

    // purify
    $HTML = new HTML;
    $_POST['listannuaire'] = $HTML->purifyParams($_POST['listannuaire']);

    if (empty($_POST['listannuaire'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir le nom de la liste � cr��.<br/>";
    } else {
        //if (Functions::getAnnuaireListDuplicate("annuaireslist", "libelle", $_POST['listannuaire'], "proprietaire", $who)) {
        if (Functions::getAnnuaireListDuplicate("annuaireslist", "libelle", $_POST['listannuaire'], "proprietaire", $who)) {
            $_SESSION['alertLogincontent'] .= "Vous avez d�ja une liste du m�me nom.<br/>";
        }
    }

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isWebmaster($idTypeUser)) {

            /* @deprecated
              $dbh->query('INSERT INTO annuaireslist (id,libelle,proprietaire,annuaireslist,created) VALUES("", "' . $_POST['listannuaire'] . '","' . $who . '", "", "' . time() . '")');
             */

            // prepared
            $stmt = $dbh->prepare('INSERT INTO annuaireslist (libelle,proprietaire,annuaireslist,created)
                                   VALUES( :listannuaire, :who , "", :time)');

            $stmt->bindParam(':listannuaire', $_POST['listannuaire']);
            $stmt->bindParam(':who', $who);
            $stmt->bindParam(':time', time());
            $stmt->execute();

            $lastIdList = $dbh->query('SELECT id FROM annuaireslist WHERE proprietaire=' . $dbh->quote(intval($who)) . ' ORDER BY id DESC LIMIT 1')->fetch();

            $lienButton = "./compte/modifier/listannuairecontent.html?id=" . $lastIdList['id'];
            $texteButton = "Ajouter des annuaires existant � cette liste";
            $widthButton = "300px";
            $lienButton2 = "./compte/importer.html?id=" . $lastIdList['id'];
            $texteButton2 = "Importer des annuaires dans cette liste";
            $widthButton2 = "300px";
            header("location:../modifier/listannuairecontent.html?id=" . $lastIdList['id']);
        }
    }
}

if ($_GET['block'] == "contrat") {

    $module = (isset($_POST['module']) && !empty($_POST['module'])) ? $_POST['module'] : '';

    if (empty($_POST['typeUser'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez selectionner le groupe auquel appliquer ce contrat.<br/>";
    } else {
        $contrat = Functions::getcontrat($_POST['typeUser'],$module);
        if (isset($contrat['contrat'])) {
            $_SESSION['alertLogincontent'] .= "Un contrat existe d�j� pour ce groupe d'utilisateurs.<br/>";
        } else {
            if (empty($_POST['content'])) {
                $_SESSION['alertLogincontent'] .= "Veuillez saisir le contenu du contrat.<br/>";
            }
        }
    }

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('INSERT INTO contrats (id,typeUser,content,`module`) VALUES("", "' . $_POST['typeUser'] . '", "' . $_POST['content'] . '", "' . $_POST['module'] . '")');
        }
    }
}



if ($_GET['block'] == "importer") {

//if(empty($_POST['separator'])){
//    $_SESSION['alertLogincontent'] .= "Veuillez indiquer un s�parateur valide (, ou ; ou ligne)<br/>";
//}


    if (empty($_FILES['file']['size'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez s�lectionner un fichier.<br/>";
    } else {
        $extention = strrchr(basename($_FILES['file']['name']), ".");
        if (isset($_GET['type']) && $_GET['type'] == "excel") {
            if ($extention != ".csv") {
                $_SESSION['alertLogincontent'] .= "Le fichier a uploader doit �tre un fichier Excel (.csv).<br/>";
            }
        } else {
            if ($extention != ".txt") {
                $_SESSION['alertLogincontent'] .= "Le fichier a uploader doit �tre un fichier texte (.txt).<br/>";
            }
        }
    }



    if ($_SESSION['alertLogincontent'] == "") {


        $arrayContentNoFilter = array();

        $image_sizes = getimagesize($_FILES['file']['tmp_name']);
        $nomimage = $idUser . "anu." . $extention;

        $image = "files/upload/temp/" . $nomimage;
        move_uploaded_file($_FILES['file']['tmp_name'], $image);


        if (isset($_GET['type']) && $_GET['type'] == "excel") {
            $row = 1;
            if (($handle = fopen("./files/upload/temp/" . $nomimage, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    $row++;
                    if (Functions::checkUrlFormat($data[0])) {
                        $arrayContentNoFilter[$row]['url'] = $data[0];
                        $arrayContentNoFilter[$row]['tb'] = $data[1];
                        $arrayContentNoFilter[$row]['cf'] = $data[2];
                        $arrayContentNoFilter[$row]['tf'] = $data[3];
                    }
                }
                fclose($handle);
                $arrayContent = $arrayContentNoFilter;
            }
//            print_r($arrayContentNoFilter);
        } else {
            $content = file_get_contents("./files/upload/temp/" . $nomimage);
            $arrayContentNoFilter = explode(PHP_EOL, $content);
            $arrayContent = array_map('trim', $arrayContentNoFilter);
        }

//        print_r($arrayContent);

        $who = $_SESSION['connected']['id'];
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        }

        if (is_array($arrayContent) && count($arrayContent) > 0) {
            $depart = 0;
            $requeteForm = "";

            foreach ($arrayContent as $annuaire) {
//                print_r($annuaire);

                if ($annuaire != "") {
                    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {

                        if (isset($_GET['type']) && $_GET['type'] == "excel") {
                            $annuaire['url'] = trim($annuaire['url']);
                            $dbh->query('UPDATE annuaires SET 
                         cf = "' . intval($annuaire['cf']) . '",
                         tf = "' . intval($annuaire['tf']) . '",
                         tb = "' . intval($annuaire['tb']) . '" 
                         WHERE annuaire LIKE "%' . $annuaire['url'] . '%"');
                        } else {
                            $getDoublon = Functions::getDoublon("annuaires", "annuaire", $annuaire, "importedBy", $who);
                            if (!$getDoublon) {
                                $dbh->query('INSERT INTO annuaires (id,annuaire,importedBy,active) VALUES(null, "' . $annuaire . '", "' . $who . '", "1")');
                            }
                        }
                        if (isset($_POST['repertoire']) && !empty($_POST['repertoire'][0])) {
                            $lastIdAnnuaire = $dbh->query('SELECT * FROM annuaires WHERE importedBy ="' . $who . '" ORDER BY id DESC LIMIT 1')->fetch();
                            if (isset($lastIdAnnuaire['id'])) {
                                foreach ($_POST['repertoire'] as $key => $value) {
                                    if ($value != "") {
                                        $value = intval($value);
                                        $getAnnuairesList = Functions::getAnnuaireNameById($value);
                                        if (isset($getAnnuairesList['id'])) {
                                            $annuaireZ = $getAnnuairesList['annuairesList'] . $lastIdAnnuaire['id'] . ";";
                                            $dbh->query('UPDATE annuaireslist SET annuairesList = "' . $annuaireZ . '" WHERE id =' . $getAnnuairesList['id']);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $depart++;
            }
        }
        @unlink("./files/upload/temp/" . $idUser . "anu.txt");
        @unlink("./files/upload/temp/" . $idUser . "anu.csv");
    }
}


if ($_GET['block'] == "importercategories") {

//if(empty($_POST['separator'])){
//    $_SESSION['alertLogincontent'] .= "Veuillez indiquer un s�parateur valide (, ou ; ou ligne)<br/>";
//}

    if (empty($_FILES['file']['size'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez s�lectionner un fichier.<br/>";
    } else {
        $extention = strrchr(basename($_FILES['file']['name']), ".");
        if ($extention != ".txt") {
            $_SESSION['alertLogincontent'] .= "Le fichier a uploader doit �tre un fichier texte (.txt).<br/>";
        }
    }


    if ($_SESSION['alertLogincontent'] == "") {


        $image_sizes = getimagesize($_FILES['file']['tmp_name']);
        $nomimage = $idUser . "cat.txt";
        $image = "files/upload/temp/" . $nomimage;
        move_uploaded_file($_FILES['file']['tmp_name'], $image);

        $content = file_get_contents("./files/upload/temp/" . $nomimage);
        $arrayContentNoFilter = explode(PHP_EOL, $content);
        $arrayContent = array_map('trim', $arrayContentNoFilter);
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        }
        foreach ($arrayContent as $categorie) {
            if (!empty($categorie) && $categorie != NULL && $categorie != "") {
                if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                    $dbh->query('INSERT INTO categories (id,libelle,active) VALUES("", "' . $categorie . '", "1")');
                }
            }
        }
        @unlink("/files/upload/temp/" . $idUser . "cat.txt");
    }
}


$redirectURL = str_replace($statutError, '', $_SERVER['HTTP_REFERER']);

if ($_SESSION['alertLogincontent'] != "") {
    if ($isSpecial == 1) {
        header('location:' . $redirectURL . "&statut=success");
    } else {
        if (!strpos($redirectURL, "?")) {
            $redirectURL = $redirectURL . "?statut=error";
        } else {
            $redirectURL = $redirectURL . "&statut=error";
        }
        header('location:' . $redirectURL);
    }
} else {
    unset($_SESSION['tamponForm']);
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">

            <div class="permalink">
                <h4><a nohref="#">Statut de l'op�ration en cours</a></h4>
            </div>

            <div class="notification success">
                <p>
                    <?PHP
                    if ($specialMessage != "") {
                        ?>
                        <span></span> <?PHP echo $specialMessage; ?>
                        <?PHP
                    } else {
                        ?>
                        <span>Success:</span> Op�ration effectu�e avec succ�s.
                        <?PHP
                    }
                    ?>
                </p>

            </div>
            <?PHP if ($buttonPaypal && $buttonPaypal > 0 && $idRequete) { ?>

                <form method="post" name="paypal_form" id="paypal_form_Auto" class="paypal_form_Auto" target="" action="https://www.paypal.com/cgi-bin/webscr">
                    <input name="currency_code" type="hidden" value="EUR" />
                    <input name="shipping" type="hidden" value="0.00" />
                    <input name="tax" type="hidden" value="<?PHP echo $taxes; ?>" />
                    <input type="hidden" name="item_name" value="Rechargerment <?PHP echo $NomSite; ?>">
                    <input type="hidden" name="amount" value="<?PHP echo $buttonPaypal; ?>">
                    <input type="hidden" name="business" value="<?PHP echo $EmailPaypal; ?>">
                    <input type="hidden" name="return" value="<?PHP echo $URLsiteServer; ?>compte/paypal.html?statut=success">
                    <input type="hidden" name="cancel_return" value="<?PHP echo $URLsiteServer; ?>compte/recharger.html">
                    <input type="hidden" name="notify_url" value="<?PHP echo $URLsiteServer; ?>verification.php">

                    <input name="cmd" type="hidden" value="_xclick" />
                    <input name="no_note" type="hidden" value="1" />
                    <input name="lc" type="hidden" value="FR" />
                    <input name="bn" type="hidden" value="PP-BuyNowBF" />
                    <input type="hidden" name="custom" value="<?PHP echo $idRequete; ?>">

                    <input type="image" src="images/btn_paynowCC_LG.gif" border="0"  style="display:block;position:absolute;margin-top:-75px;margin-left:550px;" id="paypal_Submit" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus s�curis�e !">
                    <img alt="" border="0" src="images/pixel.gif" width="1" height="1">
                </form>
            <?PHP } ?>
            <?PHP echo isButton(); ?>



            <!-- Pagination -->

            <!-- End pagination -->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php");
?>