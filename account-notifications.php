<?PHP
@session_start();

$a = 1;
$b = 70;
$page = 1;
include('files/includes/topHaut.php');

// user
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';
$userModel = new User($idUser);

// test
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/MailProvider.php';
$mailer = new MailProvider();


?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        ?>

        <div class="three-fourth">
            <?PHP
            include("pages.php");
            ?>
            <!-- End pagination -->
        </div>

        <?php
            $user_settings = $userModel->getParam('settings');

            // if empty - set default
            if (empty($user_settings)){
                $user_settings['notification_zero_amount'] = true;
                $user_settings['notification_project_finished'] = true;
                $user_settings['notification_backlink_found'] = false;
                $user_settings['notification_new_prop'] = true;
                $user_settings['notification_site_disabled'] = true;
                $user_settings['notification_mod_prop'] = true;
            }
        ?>

        <form action="compte/modification/notifications.html" method="post" style="margin-top:0px;float:left;<?PHP echo $specialWidth; ?>">
            <label>
                <input type="hidden" name="user_settings[notification_zero_amount]" value="0" />
                <input type="checkbox" <?php echo ($user_settings['notification_zero_amount'])?'checked':'';?> name="user_settings[notification_zero_amount]" value="1" /> &nbsp;Recevoir un email quand votre solde est �puis�</label>
            <label>
                <input type="hidden" name="user_settings[notification_project_finished]" value="0" />
                <input type="checkbox" <?php echo ($user_settings['notification_project_finished'])?'checked':'';?> name="user_settings[notification_project_finished]" value="1"/> &nbsp;Recevoir un email quand l'un de vos projets est termin�</label>
            <label>
                <input type="hidden" name="user_settings[notification_backlink_found]" value="0" />
                <input type="checkbox" <?php echo ($user_settings['notification_backlink_found'])?'checked':'';?> name="user_settings[notification_backlink_found]" value="1" /> &nbsp;Recevoir un email quotidien r�capitulant les liens trouv�s</label>
             <label>
                <input type="hidden" name="user_settings[notification_new_prop]" value="0" />
                <input type="checkbox" <?php echo ($user_settings['notification_new_prop'])?'checked':'';?> name="user_settings[notification_new_prop]" value="1" /> &nbsp;Recevoir un email � chaque fois qu'un article est propos� sur l'un de vos blogs</label>
            <label>
                <input type="hidden" name="user_settings[notification_mod_prop]" value="0" />
                <input type="checkbox" <?php echo ($user_settings['notification_mod_prop'])?'checked':'';?> name="user_settings[notification_mod_prop]" value="1" /> &nbsp;Recevoir un email � chaque fois qu'une demande de modification sur l'un de vos blogs � �t� effectu�e</label>
            <label>
                <input type="hidden" name="user_settings[notification_site_disabled]" value="0" />
                <input type="checkbox" <?php echo ($user_settings['notification_site_disabled'])?'checked':'';?> name="user_settings[notification_site_disabled]" value="1" /> &nbsp;Recevoir un email � chaque fois que l'un de vos blogs devient inactif</label>
            <br/>
            <input type="submit" class="button color small round " value="Modifier" style="color:white;"/>
        </form>

    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>