<?PHP


//session_start();





// lire le formulaire provenant du syst�me PayPal et ajouter 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
    $value = urlencode(stripslashes($value));
    $req .= "&$key=$value";
}

// renvoyer au syst�me PayPal pour validation
$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
$fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);

$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_amount = $_POST['mc_gross'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];
$id_transac = intval($_POST['custom']);

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';


if (!$fp) {
    // ERREUR HTTP
} else {
    fputs($fp, $header . $req);
    while (!feof($fp)) {
        $res = fgets($fp, 1024);
        if (strcmp($res, "VERIFIED") == 0) {
            // transaction valide
            include("./files/includes/config/configuration.php");
            include("./files/includes/functions.php");

            //
            include("./files/includes/Classes/Projects.php");
            $projectsModel = new Projects($dbh);


            $parameters = Functions::getAllParameters();
            $EmailPaypal = $parameters['emailpaypal']["valeur"];
            $tva = $parameters['tax']["valeur"];
            file_put_contents("log", $payment_amount);
            if ($payment_status == "Completed" && strtolower($payment_currency) == "eur" && $receiver_email == $EmailPaypal) {

                
                $request = Functions::getPaiementById($id_transac);
                if ($request) {

                    $tvaAmount = 0;
                    $ExpectedAmount = 0;
                    $tocharge = 0;

                    if ($request['tva'] > 0) {
                        $tvaAmount = (($request['tva'] * $request['requestAmount']) / 100);
                    }

                    $ExpectedAmount = $request['requestAmount'] + $tvaAmount;
//                    file_put_contents("log", $payment_amount."|".$ExpectedAmount);


                    if ($ExpectedAmount > $payment_amount) {
                        $newTVA = (($request['tva'] * $payment_amount) / 100);
                        $tocharge = $payment_amount - $newTVA;
                        if ($tocharge < 0) {
                            $tocharge = 0;
                        }
//                        $setUser = $dbh->query('UPDATE requestpayment SET requestAmount = "' . $tocharge . '" WHERE id=' . $id_transac);
//                        $request['requestAmount'] = $tocharge;
                    }

                    $userInfos = Functions::getUserInfos($request['requesterID']);
                    if ($userInfos) {

                        // get "paused" projects
                        $paused_project_ids = $projectsModel->getPausedProjectsIds($userInfos['id']);

                        $newSolde = $userInfos['solde'];
                        if (isWebmaster($userInfos['typeutilisateur'])) {
                            $newSolde = $userInfos['solde'] + $request['requestAmount'];
                        }

                        $setUserFirst = false;
                        $setUser = false;

                        $setUserFirst = $dbh->query('UPDATE utilisateurs SET solde="' . $newSolde . '", AmountLastPayment = "' . $request['requestAmount'] . '", lastPayment ="' . time() . '" WHERE id=' . $request['requesterID']);
                        if ($setUserFirst) {
                            $transaction = new Transaction($dbh, $request['requesterID']);
                            $transaction->addTransaction($newSolde . "Rechargement Paypal ", $newSolde, 0);

                            $setUser = $dbh->query('UPDATE requestpayment SET answer = "1", answerTime="' . time() . '" WHERE id=' . $id_transac);
                            if ($setUser) {
                                $facture = $dbh->query('INSERT INTO factures (`id`,`user`,`amount`,`tva`,`reference_paypal`,`reference_ereferer`,`time`) VALUES ("","' . $userInfos['id'] . '","' . $request['requestAmount'] . '","' . $request['tva'] . '","0","ERF' . time() . '","' . time() . '")');
                            }
                        }

                        if ($setUser) {
                            $name = $parameters['logotext']["valeur"];
                            $urlSite = $parameters['url']["valeur"];
                            $limitepaiement = $parameters['limitepaiement']["valeur"];
                            $EmailSite = $parameters['email']["valeur"];

                            @require_once('./files/includes/phpmailer/class.phpmailer.php');
                            $body = '<br/>';
                            $body = '';
                            $body .= 'Bonjour ' . $userInfos['nom'] . ' ' . $userInfos['prenom'] . ',<br/><br/>';
                            $body .= "Votre compte a &eacute;t&eacute; recharg&eacute; de " . $request['requestAmount'] . " Euros<br/><br/>";
                            $body .= "Bien Cordialement, <br/>";
                            $body .= 'Emmanuel';

                            $mail = new PHPMailer();
                            $mail->Timeout = '30';
                            $mail->CharSet = 'UTF-8';
                            $mail->From = "noreply@ereferer.fr";
                            $mail->FromName = $name;
                            $mail->Subject = "Votre compte a &eacute;t&eacute; recharg&eacute; de " . $request['requestAmount'] . " Euro(s)";
                            $mail->AltBody = "";
                            $mail->IsHTML(true);
                            $mail->MsgHTML($body);
                            $mail->AddAddress($userInfos['email'], "");
                            $mail->Send();
                        }

                        // reset paused projects on balance increasing
                        if ($setUser){
                            $user_id = $userInfos['id'];
                            try{
                                if (!empty($paused_project_ids)){
                                    $projectsModel->resetPausedProjects($paused_project_ids,$user_id);
                                }
                            }catch (Exception $e){
                                var_dump($e->getMessage());
                            }

                        }


                    }
                }
            }
        } else if (strcmp($res, "INVALID") == 0) {
            // Transaction invalide                
        }
    }
    fclose($fp);
}
?>