<?PHP
@session_start();

//error_reporting(E_ALL);


if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

$section = $_GET['block'];


if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

switch ($section) {
    case 'progress':
        $a = 40;
        $b = 3;

        $status = 'progress';
        break;

    case 'waiting':
        $a = 40;
        $b = 4;

        $status = 'waiting';
        break;

    case 'finished':
        $a = 40;
        $b = 5;

        $status = 'finished';
        break;

    case 'review':
        $a = 40;
        $b = 6;

        $status = 'review';
        break;

    case 'modification':
        $a = 40;
        $b = 20;

        $status = 'modification';
        break;

    default:
        $a = 0;
        $b = 0;

        $status = '';
        break;
}

$page = 1;

include('files/includes/topHaut.php');

$filters = [];

if (strtolower($_SERVER['REQUEST_METHOD']) == 'post'){
    if (isset($_POST['filters'])){
       $filters = $_POST['filters'];
    }
}


//models
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';

// helpers
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionViewHelper.php';

$RedactionProjects = new RedactionProjects($dbh);

// count
$user_id = 0;
$type = 'admin';
$count = null;
$webmaster_show_viewed = false;

if (isWebmaster()) {
    $user_id = $idUser;
    $type = 'webmaster';

    if ($select_writer_id == 'finished'){
        $webmaster_not_viewed = true;
    }
}

if ( isReferer() && !isSuperReferer() ){
    $type = 'writer';
    $user_id = $idUser;
}

// filters for admin
if ( (isAdmin() || isSu() || isSuperReferer())  && ($status=='finished')){

    if (isset($_POST['select_writer_id'])){
        $select_writer_id = intval($_POST['select_writer_id']);
        $type = 'writer';
        $user_id = $select_writer_id;

        $count = $RedactionProjects->getProjectsCount($status,$select_writer_id,$type,$webmaster_show_viewed,$filters);
    }
}

if (!$count){
    $count = $RedactionProjects->getProjectsCount($status,$user_id,$type,$webmaster_show_viewed,$filters);
}

include("pages.php");

// getting list
$projects = $RedactionProjects->getProjects($user_id,$status,$page,$pagination,$type,$webmaster_show_viewed,$filters);

$viewHelper = new RedactionViewHelper;

//error_reporting(E_ERROR);

// check - is webmaster waiting page
$is_webmaster_waiting_page = (($status == 'waiting') && isWebmaster());

// if waiting

if ($section == 'waiting' ){
    $projects_estimated_start = $RedactionProjects->getEstimatedStartDates();

}
?>


<!--breadcrumbs ends -->
<div class="container redaction_listing_page" xmlns="http://www.w3.org/1999/html">
    <div class="one">

        <?php /*
        <?php if (isWebmaster() && ($section=='waiting')){?>
            <?php
                $words_capacity = $_SESSION['allParameters']['delay_words_count']['valeur'];
                $days_starting = $RedactionProjects->getDaysCountWaiting($idUser,$words_capacity);
                 if ($days_starting>=1){
                ?>
                <div class="notification warning">
                    D�lai d'attente actuellement estim� � <b><?php echo  $days_starting;?></b> jour(s).
                </div><br/>
            <?php } ?>
        <?php } ?> */ ?>


        <h4>Liste des projets : <span style="font-size:12px;">
                    <?PHP echo $count . " R�sultat(s)."; ?>
                </span>
        </h4>

        <div style="position:absolute;top:0px;right:0">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend_ button color small round' style='padding:2px 5px;'/>";
            }
            ?>
        </div>

        <form action="compte/modifier/redaction_affectationgroupee.html" class="redaction_listing_page" method="post" id="superForm">
            <div>

                <!--  -->
                <?php  if ( (isSu() || isAdmin() || isSuperReferer()) && ($status=='finished')) { ?>

                    <div class="filters">
                        <?php $select_writer_id = isset($_POST['select_writer_id'])? intval($_POST['select_writer_id']) : 0;?>
                        <label>R�f�renceur: </label>
                        <select data-placeholder="Tous les r�dacteurs" id="select_writer" name="affectedTO" class="select affected_select">
                            <option value="0">Tous les r�dacteurs</option>
                            <?PHP
                            if (isSU() || isAdmin() || isSuperReferer()) {
                                echo  $viewHelper->getWritersOptionList(0,$select_writer_id);
                            }
                            ?>
                        </select> <br/><br/>
                        <label> Textes non aim�s: <input <?php if (isset($filters['not_liked_texts'])) {?> checked="checked" <?php } ?> type="checkbox" name="filters[not_liked_texts]" /> </label>
                        <input type='button' value='Filter' id='filter_btn' class=' button color round' style='padding:6px;'/>
                    </div>
                <?php } ?>


                <?php if ($count){ ?>
                    <?php
                        $affected_btn_title = ($section=='waiting')?"Affecter projet(s) �":"R�affecter projet(s) �";
                    ?>

                    <?php if (isAdmin() || isSu()) {?>
                    <div style="float:right;">
                        <input type="hidden" name="action" value="redaction_groups" />
                        <input type="submit" class="button color small round" value="Annuler projet(s)" style="color:white;float:right;margin:5px 0px 0px 10px;" name="envoyer3" onclick="return confirmation();">
                        <input type="submit" class="button color small round" value="<?php echo $affected_btn_title;?>" style="color:white;float:right;margin:5px 0px 0px 10px;" name="envoyer">
                    </div>
                    <?php }?>

                    <div class="clearfix"></div><br/>
                    <?PHP
                    if ($nbre_result > $pagination) {
                    echo $list_page . "<br/>";
                    }
                    ?>

                    <?php if ($is_webmaster_waiting_page){ ?>
                        <!-- Extra-table for simulating drag-n-drop table rows  -->
                        <table class="simple-table table_waiting" id="tableClasses">
                            <tbody>
                            <tr>
                                <th>Dates</th>
                            </tr>
                            <?php foreach($projects as $project) : ?>
                                <tr data-project_id="<?PHP echo $project['id']; ?>"
                                    data-created_time_unix="<?php echo strtotime($project['created_time']);?>" >
                                    <td class="not_draggable">
                                        <span class="Notes">Propos� le</span> : <br/><span class="created_time"><?PHP echo date('d/m/Y', strtotime($project['created_time'])); ?></span><br/>
                                        <span class="Notes">Date de lancement estim�e</span> :  <br/><?PHP echo date('d/m/Y', $projects_estimated_start[$project['id']]); ?><br/>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    <?php } ?>

                    <table class="simple-table redaction_table <?php echo ($is_webmaster_waiting_page)?'table_draggable': '';?>" id="tableClasses">
                        <tbody>
                            <tr>
                                <?PHP if (!isReferer() && (!isWebmaster())) { ?>
                                    <th>
                                        <input id="checkClasses" type="checkbox" value="0" title="Tout Cocher"/>&nbsp;
                                        <!--<label for="checkClasses" style="color:white;display:inline-block;"></label>-->
                                    </th>
                                <?PHP } ?>
                                <th class="info_column_header  <?php if ($is_webmaster_waiting_page){?> webmaster_info_column_header <?php } ?>">
                                    Informations
                                </th>
                                <th class="desc_column_header <?php if ($is_webmaster_waiting_page){?> webmaster_description_column_header <?php } ?>">
                                    Consignes
                                </th>
                                <th class="actions_column_header <?php if ($is_webmaster_waiting_page){?> webmaster_actions_column_header <?php } ?>" >
                                    Statuts
                                </th>
                            </tr>
                        </tbody>
                        <?php foreach($projects as $project):?>
                            <tr class="<?php if ($is_webmaster_waiting_page) { ?> sortable-drag-item <?php } ?>" data-project_id="<?php echo $project['id'];?>"
                                data-created_time_unix="<?php echo strtotime($project['created_time']);?>"
                                data-created_time="<?php echo date('d/m/Y', strtotime($project['created_time']));?>"
                            >
                                <?php if (!isReferer() && !isWebmaster()) { ?>
                                    <td class="checkbox">
                                        <input type="checkbox" value="<?PHP echo $project['id']; ?>" id="check<?PHP echo $project['id']; ?>" name="projets[]" />
                                    </td>
                                <?php } ?>
                                <td class="draggable">
                                    <?php
                                        // change starting date
                                    ?>
                                    <?php if (isAdmin() || isSu() || isSuperReferer()) { ?>
                                      <span class="Notes">Propos� le</span> : <?php echo date('d-m-Y',strtotime($project['created_time']));?><br/>

                                      <?php if ( (isAdmin() || isSu() || isSuperReferer() ) && ($status == 'waiting') ) { ?>
                                         <span class="Notes">Date de lancement estim�e : </span> <?PHP echo date('d/m/Y', $projects_estimated_start[$project['id']]);?> <br/>
                                      <?php } ?>

                                      <?php if ($project['status'] == 'finished'){ ?>
                                         <span class="Notes">Termin� le</span> : <?php echo date('d-m-Y',strtotime($project['finished_time']));?><br/>
                                      <?php }?>
                                    <?php } ?>


                                    <span class="Notes">Nom du projet</span> : <?php echo utf8_decode($project['title']);?><br/>
                                    <span class="Notes">Nombre de mots</span> : <?php echo $project['words_count'];?><br/>

                                    <?php if ( (isSuperReferer() || isSu() || isAdmin()) && ($status == 'review')) { ?>
                                        <span class="Notes">R�dacteur</span> :  <a href="#" onclick="return false;"><?php echo strtoupper($project['wr_nom']).' '.strtoupper($project['wr_prenom']); ?> </a> <br/>
                                    <?php } ?>

                                    <?php if (isAdmin() || isSu() || isWebmaster()) { ?>
                                      <span class="Notes">Co�t du projet</span> : <?php echo number_format($project['amount'],2);?> &euro;<br/>
                                    <?php } ?>

                                    <?PHP if (isAdmin() || isSu() || isSuperReferer()) { ?>
                                        <?php if ($status =='progress'){ ?>
                                            <?php
                                            $affected_time = new DateTime($project['affected_time']);
                                            $current_time = new DateTime();
                                            $diff_time = $affected_time->diff($current_time);
                                            $delay_days_count = $diff_time->days;
                                            if($delay_days_count>0){
                                                ?>
                                                <span class="Notes">Statut</span> : <span class="delay_days statoo<?php echo $project['id'];?>">Texte en retard de +/- <?php echo $delay_days_count; ?> jour(s) </span> <br/>
                                            <?php }else{ ?>
                                                <span class="Notes">Statut</span> : <span class="no_delay_days statoo<?php echo $project['id'];?>">Texte en retard de +/- <?php echo $delay_days_count; ?> jour(s) </span> <br/>
                                            <?php  } ?>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if ( (isAdmin() || isSu() || isSuperReferer()) && ( ($project['status']=='progress') || ($project['status'] == 'review' ) ||  ($project['status']=='finished') || ($status=='waiting'))  ){ ?>

                                        <?php if ( isAdmin() || isSu() ) { ?>
                                            <span class="Notes "> Webmaster:</span> <a target="_blank" href="compte/modifier/profil.html?id=<?php echo $project['webmaster_id'];?>"> <?php echo strtoupper($project['w_nom']).' '.ucfirst($project['w_prenom']); ?> </a> <br/>
                                        <?php } ?>



                                        <?php if($status=='waiting') { ?>

                                         <div><span class="Notes "> R�dacteur:</span>
                                             <span class="writer<?php echo $project['id'];?>"> --- </span> </div>
                                        <?php }else{ ?>

                                        <span class="Notes "> R�dacteur:</span>
                                        <span class="writer<?php echo $project['id'];?>">

                                               <?php if ( isSuperReferer() ) { ?>
                                                   <a href="#" onclick="return false;"><?php echo strtoupper($project['wr_nom']).' '.strtoupper($project['wr_prenom']); ?> </a> <br/>
                                               <?php } else { ?>
                                                   <a target="_blank" href="compte/modifier/profil.html?id=<?php echo $project['affectedTO'];?>"> <?php echo strtoupper($project['wr_nom']).' '.strtoupper($project['wr_prenom']); ?> </a> <br/>
                                               <?php } ?>

                                        </span>
                                        <?php } ?>

                                    <?php } ?>

                                    <?php if ( ( isSu() || isAdmin() || isSuperReferer() )  && ( ($status=='progress') || ($status=='modification') || ($status=='waiting'))) {?>
                                        <span class="Notes">re-Affecter �</span> :
                                        <select data-placeholder="Changer le r�f�renceur" name="affectedTOO"
                                                data-project_id="<?php echo $project['id'];?>"
                                                class="redaction_reaffect"
                                                rel="<?PHP echo $project['affectedTO']; ?>">

                                            <?php if ($status=='waiting'){?>
                                                <option value="0">Changer le r�f�renceur</option>
                                            <?php } ?>

                                            <?PHP

                                            if (isSU() || isAdmin() || isSuperReferer()) {
                                                echo  $viewHelper->getWritersOptionList($project_id,$project['affectedTO']);
                                            }
                                            ?>

                                        </select>
                                    <?php } ?>

                                </td>
                                <td class="desc draggable"">

                                   <?php if (!empty($project['article_title'])){ ?>
                                       <div><b>Nom de l'article</b> : <?php echo utf8_decode($project['article_title']);?></div><br/>
                                   <?php } ?>

                                    <?php if (!empty($project['instructions'])){ ?>
                                        <div><b>Consigne sp�cifique � l'article</b> :
                                        <pre><?php echo utf8_decode($project['instructions']);?></pre></div><br/>
                                    <?php } ?>

                                   <b>Consigne g�n�rale li�e au projet</b> :
                                   <pre><?php echo utf8_decode($project['desc']);?></pre>

                                </td>
                                <td class="actions draggable">

                                    <?php if ($status =='progress'){ ?>
                                        Projet en cours <br/><br/>
                                    <?php } ?>

                                    <?php if ($status =='waiting'){ ?>
                                        Projet en attente <br/><br/>
                                    <?php } ?>

                                    <?php if (isWebmaster() && ($status =='finished')){ ?>
                                        <?php if (!$project['viewed']){ ?>
                                            <div class="not_viewed">Non consult�</div>
                                        <?php }else{ ?>
                                            <div class="set_not_viewed" data-project_id="<?PHP echo $project['id'];?>">Marquer comme<br/>non-consult�</div>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if ($status =='finished'){ ?>
                                        <div><br/>Projet termin� </div><br/>
                                        <a href="./compte/redactionview.html?id=<?PHP echo $project['id']; ?>" ><i class="icon-eye-open"></i>Voir votre texte</a><br/>
                                    <?php } ?>

                                    <?php if (  ((isSuperReferer() || isAdmin() || isSu())  && ( ($status=='waiting') || ($status=='progress') ) )
                                                || ( isWebmaster() && ($status=='waiting')  )) { ?>
                                        <a href="./compte/redaction/creating_project.html?id=<?PHP echo $project['id']; ?>&action=modify"><i class="icon-edit"></i> Modifier </a><br/>
                                        <a href="./compte/supprimer/redaction.html?id=<?PHP echo $project['id']; ?>" onclick="return confirmation();"><i class="icon-trash"></i> Supprimer </a><br/>
                                   <?php } ?>

                                   <?php if ( ( isAdmin() || isSu() || isSuperReferer()) && ($status=='review') ){?>
                                        <a href="./compte/redactiontask.html?id=<?PHP echo $project['id']; ?>"><i class="icon-edit"></i> Contr�ler le texte </a><br/>
                                    <?php } ?>

                                    <?php if ( (isReferer() && !isSuperReferer() ) && ( ($status=='progress') || ($status=='modification'))){?>
                                        <a href="./compte/redactiontask.html?id=<?PHP echo $project['id']; ?>" ><i class="icon-eye-open"></i> Voir mes t�che</a>
                                    <?php } ?>

                                </td>
                            </tr>
                        <?php endforeach;?>

                    </table>
                    <?PHP
                    if ($nbre_result > $pagination) {
                        echo $list_page . "<br/>";
                    }
                    ?>

                <?php  } ?>
           </div>
       </form>
    <script>
        (function($){
            // url for updating
            var ajax_url = 'files/includes/ajax/update.php';

            var finished_base_url = '/compte/redactionlisting/finished.html';
            var finished_selected_id = <?php echo json_encode(intval($select_writer_id));?>;

            // turn the element to select2 select style
            $('select[name=affectedTOO]').chosen();


            <?php if ( (isSu() || isAdmin() || isSuperReferer()) && ($status=='finished')) {?>

                // filter button
                $("#filter_btn").on('click',function(e){
                    $("#select_writer").trigger('change');
                });


                // filter finished by writer
                $('#select_writer').on('change',function(){
                    var selected_id = $(this).val();
                    var current_url = window.location.href;

                    var hidden_writer_id = '<input type="hidden" name="select_writer_id" value="'+selected_id+'" />';
                    $("#superForm").append(hidden_writer_id);


                    if (selected_id != finished_selected_id){
                        current_url = finished_base_url;
                    }

                    $('#superForm').prop('action',current_url).submit();
                    console.warn(selected_id);
                });

                $('nav.pagination a').on('click',function(e){
                    e.preventDefault();
                    var selected_id = $('#select_writer').val();
                    var href = $(this).prop('href');
                    var hidden_writer_id = '<input type="hidden" name="select_writer_id" value="'+selected_id+'" />';
                    $("#superForm").append(hidden_writer_id);

                    if (selected_id != finished_selected_id){
                        href = finished_base_url;
                    }

                    $('#superForm').prop('action',href).submit();
                });
            <?php } ?>


            // action
            // webmaster set project as not viewed

            $(".set_not_viewed").click(function(){
                var project_id = $(this).data('project_id');
                var button = $(this);

                if (confirm('En �tes-vous s�r?')) {

                    var data = {
                        type: 'redaction_project_not_viewed',
                        project_id: project_id
                    };

                    $.post(ajax_url, data)
                        .done(function (response) {
                            $(button).hide().before('<div class="not_viewed">Non consult�</div>');
                        })
                        .fail(function (error) {
                            console.log('error ',error);
                        })
                        .always(function () {
                        });
                }

            });

        })(jQuery);
    </script>
    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>