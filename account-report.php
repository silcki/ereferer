<?php
session_start();

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/config/configuration.php';

if (isset($_GET)){

    $action = $_GET['block'];

    $accessAdmin = (isset($_SESSION['connected']['typeutilisateur']) && (intval($_SESSION['connected']['typeutilisateur']) == 1) );

    // ADMIN SECTION

    if ($accessAdmin) {

        // get last writer earnings-report
        if ($action == 'last_writer_balances') {
            require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';

            // get report
            $userModel = new User(null);
            $filename = $userModel->getLastReport('writers_earnings_report');
            $filename_path = ADMIN_REPORTS_FOLDER.$filename;
            if (!empty($filename) && file_exists($filename_path)){
                header('Content-Type: text/plain');
                header('Content-Disposition: attachment; filename='.$filename);
                header('Content-Length: ' . filesize($filename_path));
                readfile($filename_path);
                die;
            }
        }

    }

}

