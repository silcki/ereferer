<?PHP
/* Redaction "PostController" */


@session_start();
if (isset($_SERVER['HTTP_REFERER'])) {
    $redirectURL = str_replace('?statut=error', '', $_SERVER['HTTP_REFERER']);
    $redirectURL = str_replace('&statut=error', '', $redirectURL);
    $redirectURL = str_replace('&statut=success', '', $redirectURL);
    $redirectURL = str_replace('?statut=success', '', $redirectURL);
}
$special = 0;
$specialMessage = "";

$a = 1;
$b = 0;

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}


$page = 1;
$pageTilte = "modif";
include('files/includes/topHaut.php');

// user
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/EchangesProposition.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/EchangeSites.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionValidation.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Partners.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Logger.php';

$partnersModel = new Partners($dbh);

$_SESSION['alertLogincontent'] = "";
$_SESSION['tamponForm'] = "";


if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

if (isset($_GET['block']) && ($_GET['block'] == "task" )) {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $_GET['id'] = isset($_GET['id']) ? intval($_GET['id']) : $idUser;
    } else {
        $_GET['id'] = $idUser;
    }
}

if (!isset($_GET['id']) || empty($_GET['id'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
} else {
    $_GET['id'] = intval($_GET['id']);
}


if ($_GET['block'] == "task") {


    // clear errors
    if (isset($_SESSION['redaction_task_errors']))
        unset($_SESSION['redaction_task_errors']);

    //if (isset($_SESSION['declined_conditions']))
       // unset($_SESSION['declined_conditions']);


    if (isset($_SESSION['redaction_post_no_errors']))
        unset($_SESSION['redaction_post_no_errors']);

    if (isset($_SESSION['post_response'])){
        unset($_SESSION['post_response']);
    }

    // validate text
    if (!empty($_POST['checking']) || !empty($_POST['finishing'])){

        $redactionValidation = new RedactionValidation($dbh);

        // validate text & metas
        // get_error
        list($finished_project,$errors) = $redactionValidation->validateText($_POST);

        /*
        echo '<pre>';
        var_dump($errors);
        var_dump($finished_project);
        echo '</pre>';
        die;*/


        // fix "bad" french accent
        $_POST['text'] = str_replace("�","'",$_POST['text']);

        $_SESSION['redaction_task_text'] = $_POST['text'];
        $_SESSION['category'] = $_POST['category'];
        $_SESSION['meta_title'] = $_POST['meta_title'];
        $_SESSION['meta_desc'] = $_POST['meta_desc'];
        $_SESSION['image_sources'] = $_POST['image_sources'];
        $_SESSION['front_image'] = $_POST['front_image'];
        //var_dump($project_id);

        //var_dump($errors); die;


        // Verify-action. Check POST
        $declined_conditions = isset($_POST['declined_conditions'])? $_POST['declined_conditions'] : array();

        if (!empty($declined_conditions)){
            $_SESSION['declined_conditions'] = $declined_conditions;
        }

        // test
        //$errors = array();

        // checking diff errors and declined conditions
        // Terminer-action. Check SESSION
        $declined_checking_finish = false;
        if (isset($_SESSION['declined_conditions']) && !empty($_SESSION['declined_conditions'])){
            $declined_conditions =  $_SESSION['declined_conditions'];
            $errors_diff = array_diff_key($errors,$declined_conditions);

            // test
            //$errors_diff = array();
            if(empty($errors_diff)){
                $declined_checking_finish = true;
                //$errors = array(); // reset errors. all fine!
            }
        }



        if (!empty($errors) && !$declined_checking_finish){
            $_SESSION['redaction_task_errors'] = $errors;
            $_SESSION['post_response'] = true;
            header('location:' . $_SERVER['HTTP_REFERER']); die;
        }else{

            // save errors for showing declined errors
            if ($declined_checking_finish){
                $_SESSION['redaction_task_errors'] = $errors;
            }

            $_SESSION['redaction_post_no_errors'] = true;

            if (empty($_POST['finishing'])){
                $_SESSION['post_response'] = true;
                header('location:' . $_SERVER['HTTP_REFERER']); die;
            }
        }

        /*
        // prepare for saving final text
        if (!empty($project['images'])){
            $text =  $_POST['text'];
            $doc = phpQuery::newDocument($text);

            $text_images = $doc->find('img');

            if ($text_images->length){
                foreach($text_images as $el) {
                    $pq = pq($el);

                    $src = $pq->attr('src');

                    foreach($project['images'] as $image){
                        $image_url = $image['image_url'];
                        $alt = $image['alt'];
                        if (($src == $image_url) && !empty($alt)){
                            $pq->attr('alt',$alt);
                        }
                    }

                }
            }
        }*/

        $doc_html = $_POST['text'];

        //var_dump($doc_html); die;
        // fixing cp1252 problem
        //$doc_html = utf8_decode($doc_html);
    }

    // send text to admin reviewing
    if (!empty($_POST['finishing'])){
        $project_id = (int) $_POST['project_id'];
        $reportsModel = new RedactionReports($dbh);

        // preparing report-data
        $data = array();

        if(isset($_SESSION['declined_conditions']) && !empty($_SESSION['declined_conditions'])){
            $finished_project['declined_conditions'] = $_SESSION['declined_conditions'];
        }

        $data['report'] = serialize($finished_project);
        $data['text'] = $doc_html;
        $data['meta_title'] = trim($_POST['meta_title']);
        $data['meta_desc'] = trim($_POST['meta_desc']);
        $data['image_sources']= trim($_POST['image_sources']);
        $data['front_image']= trim($_POST['front_image']);

        $result = $reportsModel->insert($data, $project_id);

        if ($result){

            $partnersModel->setRedactionReportsCategories($project_id, $_POST['category']);

            // writer send to review
            if (isReferer()) {
                $RedactionProjects->setStatus('review',$project_id);
            }

            // corrector finishes project
            if (isSuperReferer() || isSu() || isAdmin()) {
                $RedactionProjects->finishProccessing($project_id, $idUser);

                Functions::finishPropRedac($project_id);

                $partnersModel->publishArticle($project_id);
            }


        }
        $_SESSION['alertLogincontent'] = '';
    }


    // client send to modification
    if (!empty($_POST['modification'])){
        $comment = $_POST['comment'];
        $project_id = $_POST['project_id'];

        // check declined conditions
        $declined_conditions = isset($_POST['declined_conditions']) ? $_POST['declined_conditions'] : array();

        $admin_reject = isset($_POST['admin_reject'])? true: false;


        $projectModel = new RedactionProjects($dbh);
        $reportModel = new RedactionReports($dbh);
        $userModel = new User($idUser);

        $project = $projectModel->getProject($project_id);
        $report = $reportModel->getReport($project_id);

        // override declined conditions by corrector
        if ($admin_reject && !empty($declined_conditions)){
            $report_data = unserialize($report['report']);

            $report_data['declined_conditions'] = $declined_conditions;
            $serialized_report_data = serialize($report_data);
            $reportModel->setParam('report',$serialized_report_data,$project_id);
        }

         // purify comment
        $HTML = new HTML();
        $comment = $HTML->purifyParams($comment);
        $user_rejection = (isset($report_data['user_rejected']) && $report_data['user_rejected']);


        if ($project['status']!='modification'){
            $projectModel->setStatus('modification',$project_id);
            $projectModel->setViewed($project_id,0);

            $writer_earn = $report['writer_earn'];
            $writer_id = $project['affectedTO'];

            $corrector_earn = $report['corrector_earn'];
            $corrector_id = $report['user_approved_id'];

            // if user`s rejection
            // set flag
            if (!$admin_reject && !isset($report_data['user_rejected'])){
                $report = $reportModel->getReport($project_id);
                $report_data = unserialize($report['report']);

                $report_data['user_rejected'] = true;
                $serialized_report_data = serialize($report_data);
                $reportModel->setParam('report',$serialized_report_data,$project_id);
            }

            if ( ($writer_earn > 0) && !$admin_reject) {
                $userModel->changeBalance($writer_id, $writer_earn, false);
            }

            // overwrite
            if ($admin_reject){
                $corrector_id = $idUser;
            }

            if ( ($corrector_earn > 0) && !$user_rejection && !$admin_reject )
                $userModel->changeBalance($corrector_id,$corrector_earn,false);

            //die;
            if ($admin_reject){
                $reportModel->setParam('admin_comment',$comment,$project_id);
            }else{
                $reportModel->setParam('webmaster_comment',$comment,$project_id);
            }

        }
   }

    /*
     * @deprecated
     *
     */
    if (!empty($_POST['modification_review'])){
        $projectModel = new RedactionProjects($dbh);
        $reportModel = new RedactionReports($dbh);

    }

}

// get text-source
if ($_GET['block'] == "get_text") {
    ob_end_clean();
    $reportModel = new RedactionReports($dbh);
    $projectModel = new RedactionProjects($dbh);

    $project_id = intval($_GET['id']);
    if ($project_id){
        $project = $projectModel->getProject($project_id);

        if (!(isSuperReferer() || isAdmin() || isSu())) {
            // access denied
            if ($project['webmaster_id'] != $idUser) {
                header('location:' . $_SERVER['HTTP_REFERER']);
            }
        }

        $title = $project['title'];
        $filename = str_replace(' ','_',$title).'.txt';
        $report = $reportModel->getReport($project_id);
        $text = $report['text'];
        header("Content-type: text/html; charset=iSO-8859-1");
        header("Content-Disposition: attachment; filename=".$filename);
        $source = '';
        // add meta`s
        if (!empty($report['meta_title'])){
            $source .= '<title>'.$report['meta_title'].'</title>';
        }
        if (!empty($report['meta_desc'])){
            $source .= '<meta name="description" content="'.$report['meta_desc'].'" />';
        }
        $source .= $text;
        echo $source;
        die;
    }else{
        header('location:' . $_SERVER['HTTP_REFERER']);
    }
}

if ($_GET['block'] == "delete") {

}


if ($_SESSION['alertLogincontent'] != "") {
    if (!strpos($redirectURL, "?")) {
        $redirectURL = $redirectURL . "?statut=error";
    } else {
        $redirectURL = $redirectURL . "&statut=error";
    }

    if ($special == 0) {
        header('location:' . $redirectURL);
    }
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">

            <div class="permalink">
                <h4><a nohref="#">Statut de l'op�ration en cours</a></h4>
            </div>

            <div class = "notification success">
                <p>
                    <?PHP
                    if ($specialMessage != "") {
                        ?>
                        <span>Success</span> <?PHP echo $specialMessage; ?>.
                        <?PHP
                    } else {
                        // override basic success message for creating project
                        if (strpos($_SERVER['HTTP_REFERER'],'emailpro')) {
                        ?>
                            Merci pour votre projet, n'oubliez pas de le d�marrer maintenant !
                        <?php
                        }else{?>
                            <span>Success</span> Op�ration effectu�e avec succ�s.
                        <?php
                            }
                        ?>
                        <?PHP
                    }
                    ?>
                </p>
            </div>
            <?PHP echo isButton(); ?>

            <!--Pagination-->

            <!--End pagination-->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php");
?>