<?php

$file = isset($_GET["file"]) ? ($_GET["file"]) : "";
if ($file != "") {
//    $file = "./files/factures/" . $file;
}
$shortfilename = substr($file, strrpos($file, "/") + 1);
if (!file_exists($file))
    header("location:./");
//echo "$file<br />$shortfilename"; exit;
//Telechargement du fichier	
if (ob_get_contents())
    die('Some data has already been output, can\'t send file');
if (isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE'))
    header('Content-Type: application/force-download');
else
    header('Content-Type: application/octet-stream');
if (headers_sent())
    die('Some data has already been output to browser, can\'t send file');

// reconnait l'extension pour que le t&eacute;l&eacute;chargement
// corresponde au type de fichier afin d'&eacute;viter les erreurs de corruptions
switch (strrchr(basename($file), ".")) {
    case ".gz": $type = "application/x-gzip";
        break;
    case ".tgz": $type = "application/x-gzip";
        break;
    case ".zip": $type = "application/zip";
        break;
    case ".pdf": $type = "application/pdf";
        break;
    case ".png": $type = "image/png";
        break;
    case ".gif": $type = "image/gif";
        break;
    case ".jpg": $type = "image/jpeg";
        break;
    case ".txt": $type = "text/plain";
        break;
    case ".htm": $type = "text/html";
        break;
    case ".html": $type = "text/html";
        break;
    case ".doc": $type = "application/msword";
        break;
    default: $type = "application/octet-stream";
        break;
}
header("Content-disposition: attachment; filename=$shortfilename");
header("Content-Type: application/force-download");
header("Content-Transfer-Encoding: $type\n"); // ne pas enlever le \n
header("Content-Length: " . filesize($file));
header("Pragma: no-cache");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
header("Expires: 0");
readfile($file);
?>