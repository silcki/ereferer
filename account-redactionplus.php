<?PHP
@session_start();

$_GET['block'] = 'waiting';

if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

$section = $_GET['block'];


if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

$a = 40;
$b = 21;



$page = 1;
include('files/includes/topHaut.php');

//models
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';

// helpers
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionViewHelper.php';

$project_model = new RedactionProjects($dbh);

// count

$projects = array();
$topProjects = $project_model->getTopForWriterChoosing($idUser);
$bestProjects = $project_model->getProjectsByFavouriteWriter($idUser);

$best_ids = array();
foreach($bestProjects as $best){
    $best_ids[] = $best['id'];
}

foreach($topProjects as $top){
    if (!in_array($top['id'],$best_ids)){
       $projects[]=$top;
    }
}

foreach($bestProjects as $project){
    $project['is_best'] = true;
    $projects[] = $project;
}

$count = count($projects);
?>

    <script type="text/javascript">
        (function($){
            $(document).ready(function(){

                $("#add_projects").click(function(e){
                    e.preventDefault();
                    var checkboxes =  $("#superForm input[type=checkbox]:checked");

                    if (checkboxes.length>0){
                        if (confirmation()){
                            $("#superForm").submit();
                        }
                    }
                });

                $(".add_single_project").click(function(e){
                    e.preventDefault();
                    var id = $(this).data('id');
                    if(confirmation()){
                        $("#superForm .is_top:checked").prop('checked',false);
                        $("#check"+id).prop('checked',true);
                        $("#superForm").submit();
                    }else{
                        $("#check"+id).prop('checked',false);
                    }
                });


                $(".is_top").click(function(){
                    $("#superForm .is_top:checked").not(this).prop('checked',false);

                });

            });



        })(jQuery);
    </script>


<!--breadcrumbs ends -->
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="one">

        <h4>Liste des projets : <span style="font-size:12px;">
                    <?PHP echo $count . " R�sultat(s)."; ?>
                </span>
        </h4>

        <div style="position:absolute;top:0px;right:0">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend_ button color small round' style='padding:2px 5px;'/>";
            }
            ?>
        </div>

        <form action="compte/modification/redaction_affectationgroupee.html" method="post" id="superForm">
            <input type="hidden" name="okChecked" value="redaction_affectationgroupee" />
            <input type="hidden" name="writer_id" value="<?php echo $idUser;?>" />
            <div style="float:right;">
                <input type="submit" id="add_projects" class="button color small round" value="Prendre ce projets" style="color:white;float:right;margin:5px 0px 0px 10px;" name="envoyer2" <?PHP echo $onclick2; ?>/>
            </div>
            <div>
                <?php if ($count){ ?>
                    <?php
                        $affected_btn_title = ($section=='waiting')?"Affecter projet(s) �":"R�affecter projet(s) �";
                    ?>

                    <div class="clearfix"></div><br/>
                    <?PHP
                    if ($nbre_result > $pagination) {
                    echo $list_page . "<br/>";
                    }
                    ?>
                    <table class="simple-table redaction_table" id="tableClasses">
                        <tbody>
                            <tr>
                               <th>

                               </th>
                               <th>
                                    Informations
                                </th>
                                <th>
                                    Consignes
                                </th>
                                <th>
                                    Statuts
                                </th>
                            </tr>
                        </tbody>
                        <?php $showed_favourite_title = false;  ?>
                        <?php foreach($projects as $project):?>
                            <?php
                                if (!$showed_favourite_title && (isset($project['is_best']) && (!empty($topProjects)))){
                                ?>
                                <tr>
                                    <td colspan="4" style="text-align: center;padding-top:15px;"><h3>Vous �tes le r�dacteur favori pour ces projets</h3></td>
                                </tr>
                                <?php
                                $showed_favourite_title = true;

                            }?>

                            <tr>

                               <td>
                                    <input type="checkbox" <?php if (!isset($project['is_best'])):?> class="is_top" <?php endif;?> value="<?PHP echo $project['id']; ?>" id="check<?PHP echo $project['id']; ?>" name="projets[]" />
                                </td>

                                <td>
                                   <?php
                                        // change starting date
                                    ?>
                                    <span class="Notes">Nom du projet</span> : <?php echo utf8_decode($project['title']);?><br/>
                                    <span class="Notes">Nombre de mots</span> : <?php echo $project['words_count'];?><br/>
                                </td>
                                <td class="desc">

                                    <?php if (!empty($project['article_title'])){ ?>
                                        <div><b>Nom de l'article</b> : <?php echo utf8_decode ($project['article_title']);?></div><br/>
                                    <?php } ?>

                                    <?php if (!empty($project['instructions'])){ ?>
                                        <div><b>Consigne sp�cifique � l'article</b> :
                                            <pre><?php echo utf8_decode ($project['instructions']);?></pre></div><br/>
                                    <?php } ?>

                                    <b>Consigne g�n�rale li�e au projet</b> :
                                    <pre><?php echo utf8_decode ($project['desc']);?></pre>
                                </td>
                                <td class="actions">
                                    <a href="compte/modification/redaction_affectationgroupee.html?id=<?PHP echo $project['id']; ?>" class="add_single_project" data-id="<?PHP echo $project['id']; ?>" >Prendre ce projet </a><br/>
                                </td>
                            </tr>
                        <?php endforeach;?>

                    </table>
                    <?PHP
                    if ($nbre_result > $pagination) {
                        echo $list_page . "<br/>";
                    }
                    ?>

                <?php  } ?>
           </div>
       </form>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>