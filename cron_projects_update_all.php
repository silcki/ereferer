<?php

session_start();

// Cron  Annuaire-Projects`s states Updating
// @voodoo417

error_reporting(E_ALL);

$document_root = dirname(__FILE__);

// mailing manager
require_once $document_root.'/files/includes/Classes/Projects.php';

ini_set('memory_limit', '1024M');
ini_set('max_execution_time','9999');

var_dump(ini_get('max_execution_time'));
var_dump(ini_get('memory_limit'));


set_time_limit(0);
//error_reporting(E_ALL);


// log open
$fp = fopen(dirname(__FILE__).'/cron_projects_update_all.log', 'a+');
if ($fp) {
    $time = date("Y-m-d H:i:s");
    fwrite($fp, __FILE__ . ' running at ' . $time . '. Process ID = ' . getmypid() . PHP_EOL);
    fclose($fp);
}


//var_dump('unset_running'); unset($_SESSION['checker_running']);die;

$checker = new ProjectsUpdateAllChecker($dbh);
var_dump('unset_running');

//unset($_SESSION['projects_update_all_running']);

if (!isset($_SESSION['projects_update_all_running'])) {
    echo 'Projects Update All Checker.Running<br/>';
    $_SESSION['projects_update_all_running'] = true;
    $checker->run();
} else {
    echo 'Already running<br/>';
    die;
}

class ProjectsUpdateAllChecker
{

    /**
     * @var PDO
     */
    private $dbh;

    /**
     * ProjectsUpdateAllChecker constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh){
        $this->dbh = $dbh;
    }

    function run(){
        //error_reporting(E_ALL);
        $projects = new Projects($this->dbh);
        $projects->updateCronAllProjectsStates();
    }


    function __destruct(){
        unset($_SESSION['projects_update_all_running']);
    }

}


// helper-functions
function __debug() {
    echo '<pre>';
    var_dump(func_get_args());
    echo '</pre>';
}
?>

