<?PHP
@session_start();

$a = 29;
$p = 1;
$b = 16;
$_GET['section'] = "default";


$page = 1;
include('files/includes/topHaut.php');
$stitle = "one";
//$stitle = "three-fourth";
if (isReferer() && !isSuperReferer()) {
    $stitle = "one";
}

if (!isset($_SESSION['OrdreProjets']) || empty($_SESSION['OrdreProjets']) || $_SESSION['OrdreProjets'] == "" || !isset($_GET['ordre'])) {
    $_GET['ordre'] = "def";
}

$_SESSION['OrdreProjets'] = $_GET['ordre'];

if (!empty($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}


if ($pagination > 200) {
    $pagination = 200;
}
//  $pagination = 30;

$classer = intval($_SESSION['OrdreProjets']);

$count = 0;

//echo $classer;

// malus writer page
$malus_writers = 0;
if (isset($_GET['malus_writers']) && !empty($_GET['malus_writers'])){
    $malus_writers = $_GET['malus_writers'];
}


if (isSu() || isAdmin() || ( isSuperReferer() && !$malus_writers) ) {
    $couplesList = Functions::getBacklinkCouples('not_found_yet', $page, $pagination, $classer,0,0,$malus_writers);
    $count = Functions::getBacklinkCouplesCount('not_found_yet', $_SESSION['OrdreProjets'],0,0,$malus_writers);
}

if ( ( isReferer() && !isSuperReferer()) || (isSuperReferer() && $malus_writers )) {
    $couplesList = Functions::getBacklinkCouples('not_found_yet', $page, $pagination, $classer, $idUser, $delaiRefererInsert,$malus_writers);
    $count = Functions::getBacklinkCouplesCount('not_found_yet', $_SESSION['OrdreProjets'], $idUser, $delaiRefererInsert,$malus_writers);
    //var_dump('count',$count);
    //die;
    //var_dump($couplesList);
}

if (isWebmaster()) {
    $couplesList = Functions::getBacklinkCouples('not_found_yet', $page, $pagination, $classer, $idUser);
    $count = Functions::getBacklinkCouplesCount('not_found_yet', $_SESSION['OrdreProjets'], $idUser);
}


//echo $count;
include("pages.php");
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">
        <div class="<?PHP echo $stitle; ?>">
            <h4>Projects backlinks: <span style="font-size:14px;">
                    <?PHP echo $count . " soumissions(s) sont introuvables"; ?>
                    <?PHP
                    if ($_SESSION['OrdreProjets'] > 0) {
                        echo " sur : <span style='color:red'>" . Functions::urlWithoutHash(Functions::getAnnuaireName($_SESSION['OrdreProjets']), 1) . "</span>";
                    }
                    ?>
                    <!--� ce jour.-->.
                </span>
            </h4>

            <form action="compte/<?PHP echo $parametresURl; ?>" method="post" id="superForm">

                <div class="clear">  </div>
                <?PHP
                $urlComplete = "";
                $urlComplete = $_SERVER['REQUEST_URI'];
                $query = $_SERVER['QUERY_STRING'];

                if (strpos($urlComplete, "&page")) {
                    $urlComplete = preg_replace("#\\&page\=[0-9]{3}#i", "", $urlComplete);
                } else {
                    $urlComplete = preg_replace("#\\?page\=[0-9]{3}#i", "", $urlComplete);
                }

                if (!preg_match("#ordre#", $urlComplete)) {
                    if (strpos($urlComplete, "?")) {
                        $urlComplete = $urlComplete . "&ordre=";
                    } else {
                        $urlComplete = $urlComplete . "?ordre=";
                    }
                } else {
//                                    echo $urlComplete;
                    if (strpos($urlComplete, "&ordre")) {
                        $urlComplete = preg_replace("#\\&ordre\=[0-9]{2,3}#i", "", $urlComplete);
                        $urlComplete = $urlComplete . "&ordre=";
                    }
                    if (strpos($urlComplete, "?ordre")) {
                        $urlComplete = preg_replace("#\\?ordre\=[0-9]{2,}#i", "", $urlComplete);
                        $urlComplete = $urlComplete . "?ordre=";
                    }
                }
                ?>

                <select name="order" id="order" style="float:left;">
                    <option value="./compte/backlinks.html">Afficher tous backlinks</option>
                    <?PHP
                    if (isSU() || isAdmin() || isSuperReferer()) {
                        echo Functions::getAnnuaireListOption($classer, $urlComplete, 0, 'backlinks');
                    }

                    if (isReferer() && !isSuperReferer()) {
                        echo Functions::getAnnuaireListOption($classer, $urlComplete, $idUser, 'backlinks');
                    }

                    if (isWebmaster()) {
                        echo Functions::getAnnuaireListOption($classer, $urlComplete, 0, 'backlinks', $idUser);
                    }
//                    echo $totalBacklinks;
                    ?>
                </select>
                <br/><br/>
                <?php
                    if (isSu() || isReferer()) {
                    $malus_writers = 1;
                ?>
                    <a style="color:#000;text-decoration: underline;" href="/compte/backlinks.html?malus_writers=<?php echo $malus_writers;?>">Voir les t�ches octroyant un malus!</a>
                <?php
                    }
                ?>

                <div class="cleared"><br/></div>
                <?PHP
                //die;
                if ($nbre_result > $pagination) {
                    echo $list_page . "<br/>";
                }
                ?>
                <div style="position:absolute;top:0px;right:0">
                    <?PHP
                    if (isSu() || isSuperReferer()) {
                        echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                        echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:2px 5px;'/>";
                    }
                    ?>
                </div>

                <script>
                    (function ($) {

                        $(document).ready(function () {

                            $(".google_check").click(function (e) {
                                // e.preventDefault();
                            });


                            $(".set_status").click(function (e) {
                                e.preventDefault();
                                var ajax_url = 'files/includes/ajax/update.php';
                                var couple_ids = $(this).data('couple');
                                var backlink = $(this).parent().find('input[type=text]').val();
                                var action = $(this).data('action');
                                var response_div = $(this).parent().next();

                                var params = {
                                    type: 'backlink_status',
                                    couple_ids: couple_ids,
                                    backlink: backlink,
                                    action: action
                                };


                                if (confirm('En �tes-vous s�r?')) {

                                    $.post(ajax_url, params).done(function (data) {
                                        if (data == '1') {
                                            $(response_div).text('Action effectu�e avec succ�s.').show(0).delay(4000).hide(0);
                                        } else if (data == '2') {
                                            $(response_div).text('Cette  mise � dej� �t� effectu�e.').show(0).delay(4000).hide(0);
                                        } else {                                       
//                                            $(response_div).text(data).show(0).delay(50000000).hide(0);
                                            $(response_div).text('Action impossible..').show(0).delay(5000).hide(0);
                                        }
                                    }).error(function (data) {
                                        $(response_div).text('Action impossible.').show(0).delay(5000).hide(0);
                                    });
                                }


                            });

                        });



                    })(jQuery);

                </script>
                <?PHP
                if ($classer > 0 && !isWebmaster() && count($couplesList) > 0) {
                    $annuaireClassement = Functions::getAnnuaire($classer);
                    echo '<p style="font-weight:bold;font-size:14px;">';
                    if ($annuaireClassement && (isSu() || isAdmin() || isSuperReferer() || (isReferer() && $annuaireClassement['tav'] > 70))) {
                        echo 'Cet annuaire accepte normalement <span style="color:green;">' . $annuaireClassement['tav'] . '%</span> des soumissions.';
                    }

                    if ((isSu() || isSuperReferer()) && $annuaireClassement['tv'] > 0) {
                        echo ' Cet annuaire valide les soumissions en moyenne sous <span style="color:green;">' . $annuaireClassement['tv'] . '</span> jour(s).</p>';
                    }

                    echo '</p>';
                }
                ?>
                <?php if (count($couplesList) > 0) { ?>
                    <?php
                    $getRestarted = Functions::getRestartedSoumissionsByProcject();
                    $ancreAnnuaireAll = Functions::getAncre();
                    $AllSoumissionsAge = Functions::getAllSoumissionsAge();
                    $getSupps = Functions::getPersoWebmasterCompte();
                    $displayed = 0;

//                    print_r($AllSoumissionsAge);
                    ?>
                    <div style="display:none;" id="tooltip_status_backlinks" class="tooltip tooltip_status_backlinks" >
                        <div class="tooltip_header">Qu'est ce que c'est : Repropos�</div>
                        <div class="tooltip_body">
                            <p><b></b> Cliquez sur <b>"<u>repropos�</u>"</b> chaque fois vous resoumettez un m�me site sur un m�me annuaire.</p>
                        </div>
                    </div>

                    <table  id="backlinks_table" class="simple-table">
                        <tbody>
                            <tr>
                                <th style="width:50%">
                                    Backlink info
                                </th>
                                <th style="width:50%;">
                                    Description
                                </th>
                            </tr>

                            <?php foreach ($couplesList as $item) : ?>
                            
                                <?php
//                                if (isSu() || isAdmin() || (isWebmaster() && $item['partenaire'] == $idUser) || (isReferer() && $item['referenceur'] == $idUser && ($item['date_project_created'] + (($delaiReferer + $item['tv']) * $jourTime) > time() ))) {
                                if (isSu() || isAdmin() || isSuperReferer() || (isWebmaster() && $item['partenaire'] == $idUser) || (isReferer() && $item['referenceur'] == $idUser )) {
                                    //var_dump($displayed);
                                    $couple_id = $item['site_id'] . '_' . $item['annuaire_id'];
                                    $domain_url = rtrim(str_replace('http://', '', $item['domain']), '/');
                                    $item['lien'] = str_replace('http://', '', rtrim($item['lien'], '/'));
//                                    echo $displayed;
                                    ?>

                                    <tr>
                                        <td>
                                            <div class="block-info" style="">
                                                <div class="backlink-info">
                                                    <div class="">
                                                        <?PHP
//                                                        print_r ($getRestarted[$item['site_id'] . $item['annuaire_id']]); 
                                                        if ($getRestarted[$item['site_id'] . $item['annuaire_id']]) {
                                                            echo ' <span style="color:red !important;"> ';

                                                            if (intval($getRestarted[$item['site_id'] . $item['annuaire_id']]['RestartCount']) > 0) {
                                                                $supSym = ($getRestarted[$item['site_id'] . $item['annuaire_id']]['RestartCount'] == 1) ? "i�re" : "i�me";
                                                                echo 'Cette t�che a �t� refaite pour la ' . intval($getRestarted[$item['site_id'] . $item['annuaire_id']]['RestartCount']) . '<sup>' . $supSym . '</sup> fois';
                                                            }

                                                            //var_dump($item['site_id'],$item['annuaire_id']);

                                                            if ($getRestarted[$item['site_id'] . $item['annuaire_id']]['LatestResoumission'] > 0) {
                                                                echo ' le ' . date('d/m/Y', $getRestarted[$item['site_id'] . $item['annuaire_id']]['LatestResoumission']) . '';
                                                            }

                                                            echo ".";
                                                            echo "</span>";
                                                        }
                                                        ?>
                                                    </div><br>
                                                    <div class="domain"><strong>Annuaire</strong>: <?php echo $domain_url; ?></div><br>
                                                    <div class="backlink-url"><strong>Projet</strong>: <?php echo $item['lien']; ?> &nbsp;
                                                        <?PHP
                                                        if (isSu() || isSuperReferer()) {
                                                            ?>
                                                            (<a target="_blank" href="compte/details.html?id=<?php echo $item['site_id']; ?>">lien</a>)
                                                            <?PHP
                                                        }
                                                        ?>
                                                    </div><br>
                                                    <div class="date"><strong>Date</strong>: <?php echo date("d/m/Y", $item['date_project_created']); ?> </div><br>
                                                    <?PHP
                                                    if (!isWebmaster()) {
                                                        if ((isSu() || isSuperReferer() ) && $item['tv'] > 0 && $classer != $item['annuaire_id']) {
                                                            echo ' Cet annuaire valide les soumissions en moyenne sous <span style="color:green :important;">' . $item['tv'] . '</span> jour(s).</p>';
                                                        }
                                                        $ageSoumissions = intval($AllSoumissionsAge[$item['site_id'] . $item['annuaire_id']]['age']);
                                                        if ($item['tv'] > 0) {
                                                            $statutsJours = $item['tv'] - $ageSoumissions;
                                                            if ($statutsJours >= 1) {
                                                                if (isSu() || isSuperReferer()) {
                                                                    if ($statutsJours == 0) {
                                                                        echo ' <span style="color:green !important;font-weight:bold;"> Ce projet devrait �tre valider <span style="color:green !important;">demain</span>.</span>';
                                                                    } else {
                                                                        echo ' <span style="color:green !important;font-weight:bold;"> Ce projet devrait �tre valider dans environ <span style="color:green !important;">' . Functions::JoursToMois($statutsJours) . '</span>.</span>';
                                                                    }
                                                                }
                                                            } else {
                                                                if ($statutsJours == 0) {
                                                                    echo ' <span style="color:orange !important;font-weight:bold;"> Ce projet aurait d� �tre valider <span style="color:inherit;font-weight:bold !important;">aujourd\'hui</span>. </span>';
                                                                } else if ($statutsJours == -1) {
                                                                    echo ' <span style="color:orangered !important;font-weight:bold;"> Ce projet aurait d� �tre valider <span style="color:inherit;font-weight:bold !important;">hier</span>. </span>';
                                                                } else {
                                                                    echo ' <span style="color:red !important;font-weight:bold;"> Ce projet aurait d� �tre valider il y a environ <span style="color:red;font-weight:bold !important;">' . Functions::JoursToMois($statutsJours) . '</span>. </span>';
                                                                }
                                                                if (isReferer() && !isSuperReferer()) {
                                                                    echo '<span style="color:red;">V�rifiez que vous avez respecter toutes les conditions exig�es par l\'annuaire et le client.</span>';
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                    <?PHP ?>
                                                </div>
                                                <div class="backlink-manually">
                                                    <?php
                                                    $parseUrl = parse_url(trim($domain_url));
                                                    $host = '';
                                                    if (!empty($parseUrl['host'])) {
                                                        $host = $parseUrl['host'];
                                                    } else {
                                                        if (!empty($parseUrl['path'])) {
                                                            $host = array_shift(explode('/', $parseUrl['path'], 2));
                                                        }
                                                    }

                                                    $domain_url = str_replace('www.', '', $host);
                                                    $project_url = str_replace('www.', '', $item['lien']);


                                                    $google_search_url = 'https://www.google.fr/search?hl=fr&q=site:' . $domain_url . '+%22' . trim($project_url) . '%22&gws_rd=ssl';
                                                    ?>
                                                </div>
                                                <?PHP
                                                if (isSu() || isSuperReferer() || (isReferer() && $item['referenceur'] == $idUser) || (isWebmaster() && $item['partenaire'] == $idUser)) {
                                                    ?>
                                                    <div class="backlink-input">
                                                        <?PHP
                                                        if (isWebmaster()) {
                                                            ?>
                                                            <span style="font-style: italic;">Page faisant le lien: &nbsp;</span>                                                
                                                            <?PHP
                                                        } else {
                                                            ?>
                                                            <span style="font-style: italic;">Ins�rez l'URL du Backlink ci-dessous: &nbsp;</span>
                                                            <a href="<?php echo $google_search_url; ?>" class="google_check" data-google="<?php echo $google_search_url; ?>" target="_blank"> => V�rifier le Backlink</a>

                                                            <?PHP
                                                        }
                                                        ?>
                                                        <br>
                                                        <input type="text" data-couple="<?php echo $couple_id; ?>" value="<?php echo $item['backlink']; ?>" style="width:220px;"/>
                                                        <button class="set_status" data-action="found" data-couple="<?php echo $couple_id; ?>" >Valider</button>
                                                        <button class="set_status" data-action="not_found" data-couple="<?php echo $couple_id; ?>" >Rejeter</button>
                                                        <?PHP
                                                        if (isSu() || isSuperReferer() || (isReferer() && $item['referenceur'] == $idUser)) {
                                                            ?>
                                                            <a class="button color small round" style="padding:1px 2px;font-size:11px" href="compte/supprimer/repropositionTache.html?id=<?PHP echo $item['soumissions_id']; ?>" onclick="return confirmation();">Repropos�</a>
                                                            <span id="statut_what_is_proposee" class="statut_what_is_proposee" style="margin:1px;"><b style="font-size:14px;color:red">?</b></span>
                                                            <?PHP
                                                        }
                                                        ?>
                                                    </div>
                                                    <?PHP
                                                }
                                                ?>
                                                <div class="response_status" style="color:cadetblue;"></div>

                                            </div>
                                        </td>
                                        <td><!--
                                           <b>Project description:</b>
                                           <pre>
                                            <?php $desc = !empty($item['project_desc']) ? $item['project_desc'] : 'Empty description'; ?>
                                            
                                            -->                                     <div class="">

                                                <?php
                                                // echo $desc;
                                                if (isSu() || isSuperReferer()) {
                                                    $nomRef = Functions::getfullname($item['referenceur']);
                                                    if ($item['referenceur'] && intval($item['referenceur']) > 0 && trim($nomRef) != "") {
                                                        ?>
                                                        <span class="Notes">R�f�renceur</span> : <a href="compte/modifier/profil.html?id=<?PHP echo $item['referenceur']; ?>" title="Voir le profil de ce R�f�renceur"><?PHP echo $nomRef; ?></a>&nbsp;<a href='./compte/accueil.html?section=projects&iduser=<?PHP echo $item['referenceur']; ?>' title="Voir la liste des projets de ce r�f�renceur"><i class="icon-list"></i></a><br/>
                                                        <?PHP
                                                    } else {
                                                        ?>
                                                        <span style="color:red;font-weight:normal;">
                                                            R�f�renceur introuvable
                                                        </span>                                               

                                                        <?PHP
                                                    }
                                                } else {
                                                    if (isReferer() && $item['referenceur'] == $idUser) {
//                                                echo "Moi-m�me";
                                                    }
                                                }
                                                ?>
                                            </div>

                                            <?PHP
                                            $getComm = Functions::getAllCommentaire($item['site_id'], $item['idannu']);
                                            if ($getComm[3]['commentaire'] != "") {
                                                echo '<span class="activeComment" rel="' . $getComm[3]['id'] . '" data-title="la description">'
                                                . '<i class="icon-eye-open"></i> Voir la description</span><br/>';
                                                echo '<div id="commentaireslide' . $getComm[3]['id'] . '" style="display:none;font-size:12px !important;">';
                                                echo nl2br(PHP_EOL . stripslashes($getComm[3]['commentaire']) . PHP_EOL);
                                                echo "</div>";
                                            } else {
                                                echo "Aucune description";
                                            }
                                            ?>
                                            <?PHP
                                            if (isSu() || isSuperReferer() || isWebmaster() || isReferer()) {
                                                ?>
                                                <br/>
                                                <a href="#ancres<?PHP echo $item["site_id"] . $item["annuaire_id"]; ?>" class="ancresDisplay" onclick=""><i class="icon-eye-open"></i> Voir consigne(s) </a><br/>
                                                <div id="ancres<?PHP echo $item["site_id"] . $item["annuaire_id"]; ?>" class="ancres">
                                                    <h5 style="color:maroon;">
                                                        <?PHP
                                                        echo "<a href='" . $item["lien"] . "' target='_blank'>" . $item["lien"] . "</a> � r�f�rencer sur <a href='" . $item["domain"] . "' target='_blank'>" . $item["domain"] . "</a>";
                                                        echo "<br>";
                                                        ?>
                                                    </h5>
                                                    <?PHP
                                                    $ancreContent = "";
                                                    $ancreSpecial = "";
//                                                                    echo  $tableau[$limit1]["AnnuaireDisplayAncre"];
//                                                                    print_r( $ancreAnnuaireAll[$getCommande["id"]]["269"]);
                                                    if (trim($ancreAnnuaireAll[$item["site_id"]][$item["annuaire_id"]]['ancre']) != "" && $item["AnnuaireDisplayAncre"] == 1) {
                                                        $ancreContent = $ancreAnnuaireAll[$item["site_id"]][$item["annuaire_id"]]['ancre'];
                                                        $ancreSpecial = "font-weight:bold;color:red;font-size:17px;";
                                                    } else {
                                                        if (trim($item["ancres"]) != "" && $item["AnnuaireDisplayAncre"] == 1) {
                                                            $item = $item["ancres"];
                                                        }
                                                    }
                                                    if ($getSupps[$item["site_id"]] && in_array($item["annuaire_id"], $getSupps[$item["site_id"]]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                                        $bonusAdded = floatval($getSupps[$item["site_id"]]['costReferenceur']);
                                                        $bonusAdded_Web = floatval($getSupps[$item["site_id"]]['costWebmaster']);
                                                        if ($bonusAdded == 0) {
                                                            $bonusAdded = $ComptePersoReferenceur;
                                                        }
                                                        if ($bonusAdded_Web == 0) {
                                                            $bonusAdded_Web = $ComptePersoWebmaster;
                                                        }
                                                    }
                                                    if ($bonusAdded > 0 && !isWebmaster() && !empty($getSupps[$item["site_id"]]['url']) && !empty($getSupps[$item["site_id"]]['username']) && !empty($getSupps[$item["site_id"]]['password'])) {
                                                        ?>
                                                        <h5 style="color:red;font-size : 15px;">ATTENTION!!! L'inscription est � r�aliser avec l'adresse email du webmaster accessible avec les informations ci-dessous:</h5>
                                                        <span style='font-size:14px;'>
                                                            <?PHP
                                                            echo "<u style='font-size:14px;font-weight:bold;'>URL d'acc�s Web</u>:  " . $getSupps[$item["site_id"]]['url'] . "<br/>";
                                                            echo "<u style='font-size:14px;font-weight:bold;'>Adresse Email</u>: " . $getSupps[$item["site_id"]]['username'] . "<br/>";
                                                            echo "<u style='font-size:14px;font-weight:bold;'>Mot de passe</u>: " . $getSupps[$item["site_id"]]['password'] . "<br/>";
                                                            ?>
                                                        </span>
                                                        <br/>
                                                        <hr/>
                                                        <br/>
                                                        <?PHP
                                                    }
                                                    if (trim($ancreContent) != "") {
                                                        ?>
                                                        <h4><u>Ancres � utiliser</u>:</h4>
                                                        <span style="<?PHP echo $ancreSpecial; ?>">
                                                            <?PHP
                                                            echo nl2br(stripslashes($ancreContent));
                                                            ?>
                                                        </span>
                                                        <br/>
                                                        <hr/>
                                                        <br/>
                                                        <?PHP
                                                    }
                                                    if (trim($item["consignesAnnuaire"]) != "") {
                                                        ?>
                                                        <h4><u>Consignes de l'annuaire</u>:</h4>
                                                        <?PHP
                                                        echo nl2br(stripslashes($item["consignesAnnuaire"]));
                                                        ?>
                                                        <br/>
                                                        <hr/>
                                                        <br/>
                                                        <?PHP
                                                    }
                                                    if (trim($item["consignes"]) != "") {
                                                        ?>
                                                        <h4><u>Consignes du projet</u>:</h4>
                                                        <?PHP
                                                        echo nl2br(stripslashes($item["consignes"]));
                                                        ?>
                                                        <br/>
                                                        <hr/>
                                                        <br/>
                                                        <?PHP
                                                    }
                                                    ?>

                                                </div>                                                <?PHP
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php $displayed++; } ?>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                <?php echo $displayed." R�sultat(s)"; } else { ?>
                    <br/>
                    <h3 style="text-align: center">
                        Aucun r�sultat � afficher
                        <?PHP
                        if ($afficher && $afficher > 0) {
                            echo 'pour cet annuaire';
                        }
                        ?>
                        .
                    </h3>
                <?php } ?>

            </form>
            <!-- Pagination 
            <?PHP
            if ($nbre_result > pagination) {
                echo $list_page;
            } else {
                ?>
                                                                                                                                                                                                            <div align="center" style="width:700px;margin:auto;">

                                                                                                                                                                                                            </div>
                <?PHP
            }
            ?>


            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>