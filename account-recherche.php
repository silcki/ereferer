<?PHP
@session_start();

$a = 5;

if (isset($_POST['section']) && !empty($_POST['section'])) {
    if ($_POST['section'] == "projets") {
        $a = 1;
        $p = 3;
        $b = 8;
    } else if ($_POST['section'] == "utilisateurs") {
        $a = 5;
        $p = 4;
        $b = 9;
    } else if ($_POST['section'] == "administrateurs") {
        $p = 2;
        $b = 10;
    } else if ($_POST['section'] == "sites") {
        $a=2000;
        //$p = 2;
        //$b = 10;
    }
    else {
        $_POST['section'] = "projets";
        $a = 1;
        $p = 3;
        $b = 8;
    }
} else {
    $_POST['section'] = "projets";
    $a = 1;
    $p = 3;
    $b = 8;
}



if (!isset($_POST['recherche']) || empty($_POST['recherche']) || strlen(trim($_POST['recherche'])) < 2) {
    $_POST['recherche'] = "";
} else {
    $_POST['recherche'] = addslashes($_POST['recherche']);
}

$_SESSION['rechercheWords'] = $_POST['recherche'];




$page = 1;
include('files/includes/topHaut.php');


?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">
        <?PHP
//        include("files/includes/menu.php")
        ?>
        <div class="one">
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertLogincontent']; ?>
                    </p>
                </div>



                <?PHP
            }
            $count = 0;
            if ($_POST['section'] == "utilisateurs" && (isAdmin() || isSu())) {
                $getUsers = Functions::getRechercheResultats($idUser, $_POST['section'], $_POST['recherche']);
                $count = count($getUsers);
            }

            if ($_POST['section'] == "projets") {
                $getCommande = Functions::getRechercheResultats($idUser, $_POST['section'], $_POST['recherche']);
                $count = count($getCommande);
            }

            if ($_POST['section'] == "sites") {
                $getSites = Functions::getSitesList(array("search"=>$_POST['recherche']));
                $count = count($getSites);
            }

//        $getRechercheResultats = Functions::getRechercheResultats($idUser, $_POST['section'], $_POST['recherche']);

            $annuairesTous = Functions::getAnnuaireAll();

            include("pages.php");

            echo '<h4>R�sultats de la recherche : ' . $count . ' r�sultat(s)</h4>';

            if ($count > 0 && $_POST['recherche'] != "" && $_POST['section'] != "") {
                ?>
                <!--<h4>Tableau de Bord</h4>-->
                <table class="simple-table">
                    <tbody>
                        <tr>
                            <?PHP if ($_POST['section'] == "utilisateurs" && (isAdmin() || isSu())) { ?>
                                <th>
                                    Informations
                                </th>
        <!--                                <th>
                                    Commentaires 
                                </th>-->
                                <th>
                                    Statuts
                                </th>
                            <?PHP } ?>
                            <?PHP if ($_POST['section'] == "projets" && (isAdmin() || isSu()) || isWebmaster() || isReferer()) { ?>
                                <th>
                                    <input id="checkClasses" type="checkbox" value="0" title="Tout Cocher"/>&nbsp;
                                    <!--<label for="checkClasses" style="color:white;display:inline-block;"></label>-->
                                </th>
                                <th>
                                    Informations
                                </th>
                                <th>
                                    Commentaires 
                                </th>
                                <th>
                                    Statuts
                                </th>

                            <?PHP } ?>
                             <?PHP if ($_POST['section'] == "sites" && (isAdmin() || isSu())) { ?>
                             <th class="info_column_header ">
                                    Sites
                                </th>
                                
                                <th class="stats_column_header ">
                                    Metrics
                                    
                                </th>
                                <th class="actions_column_header " style="min-width: 100px;">
                                    Pr�f�rences
                                </th>
                                <th class="actions_column_header " style="min-width: 100px;">
                                    Actions
                                </th>
                                 <?PHP } ?>
                        </tr>
                        <?PHP
                        while ($limit1 <= $limit2) {
                            ?>
                            <?PHP


                              if ($_POST['section'] == "sites" && (isAdmin() || isSu())) { 
                              $client = Functions::getUserInfos($getSites[$limit1]['user_id']); 
                              ?>

                              <tr class="sortable-drag-item">
                                <!--<td class="not_draggable">
                                    <input type="checkbox" value="69" id="check69" name="projets[]">
                                </td>-->
                                
                                <td class="draggable">
                                    <span class="Notes">URL(s)</span> : <a href="<?=$getSites[$limit1]['url']?>" target="_blank" title="Ouvrir l'url"><?=$getSites[$limit1]['url']?> <i class="icon-external-link"></i></a><br>
                                    
                                    <? if(isSu()){ ?>
                                        <span class="Notes">Client</span> : <a target="_blank" href="./compte/modifier/profil.html?id=<?=$getSites[$limit1]['user_id']?>"><?=$client['nom']." ".$client['prenom']?></a><br>
                                    <? } ?>

                                    <span class="Notes">Cr�dit demand�</span> : <?=$getSites[$limit1]['credit']?><br>
                                    <span class="Notes">Cat�gories</span> : <?=$cats[$getSites[$limit1]['cat_id1']]['value']?> / <?=$cats[$getSites[$limit1]['cat_id2']]['value']?><br>
                                    <span class="Notes">Tag</span> : <?=$getSites[$limit1]['tags']?><br>
                                    <span class="Notes">Note</span> : �toiles (x avis)<br>
                                    <span class="Notes"><a href="">Voir les commentaires</a></span><br>
                                </td>

                                <td class="desc_project_info draggable">
                                    <span><?=$getSites[$limit1]['nb_mots_max']?> Mots</span><br/>
                                    <span>Liens : <?=$getSites[$limit1]['nb_liens_max']?> max</span><br/>
                                    <span>Images : <?=$getSites[$limit1]['nb_images_min']?> min</span> <br/>
                                </td>

                                 <td class="desc_project_info draggable">
                                    <span class="Notes">Age</span> : <? if($getSites[$limit1]['age_m']!="0000-00-00"){ echo dateDifference(date("Y-m-d"),$getSites[$limit1]['age_m'],'%y an(s) %m mois'); } else {  echo dateDifference(date("Y-m-d"),$getSites[$limit1]['age'],'%y an(s) %m mois'); } ?><br/>
                                    <span class="Notes">Alexa</span> : <?=$getSites[$limit1]['alexa_m'] ? $getSites[$limit1]['alexa_m'] : $getSites[$limit1]['alexa']?> <br/>
                                    <span class="Notes">Trust Flow</span> : <?=$getSites[$limit1]['tf_m'] ? $getSites[$limit1]['tf_m'] : $getSites[$limit1]['tf']?> <br/>
                                    <span class="Notes">Domaines r�f�rents</span> : <?=$getSites[$limit1]['dom_ref_m'] ? $getSites[$limit1]['dom_ref_m'] : $getSites[$limit1]['dom_ref']?><br/>
                                </td>

                                <td style="" class="draggable">
                                    <a href="compte/echangesgestion/ajout.html?edit_id=<?=$getSites[$limit1]['id']?>"><i class="icon-edit"></i> Modifier</a><br>
                                    <a class="mc_delete" rel="<?PHP echo $getSites[$limit1]['id']; ?>"><i class="icon-trash"></i> Supprimer</a><br>
                                    <a class="mc_active" rel="<?PHP echo $getSites[$limit1]['id']; ?>">Site <span><?=$getSites[$limit1]['active']? "actif" : "inactif" ?><span> ! Cliquez pour <?=$getSites[$limit1]['active']? "d�sactiver" : "activer" ?></a>
                                </td>
                                
                            </tr>


                            <?
                            }

                            if ($_POST['section'] == "utilisateurs" && (isAdmin() || isSu())) {
                                if ($count == 1) {
                                    //header('location: ./accueil.html?section=projects&iduser=' . $getUsers[$limit1]['id']);
                                }
                                ?>
                                <tr>
                                    <td>
                                        <?PHP // if ($_GET['section'] == "inscriptions") { ?>
                                        <span class="Notes">Type de compte</span> :  <?PHP echo $arrayLvl[$getUsers[$limit1]['typeutilisateur']]; ?> <br/>
                                        <?PHP // } ?>
                                        <?PHP if ($_GET['section'] == "utilisateurs" && $getUsers[$limit1]['active'] == 1 && $getUsers[$limit1]['lastIP'] > 0 && $getUsers[$limit1]['lastIP'] == 111111) { ?>
                                            <span class="Notes">Parrain</span> : <a href='./compte/utilisateurs.html?section=affiliation&byuser=<?PHP echo $getUsers[$limit1]['lastIP']; ?>' title="Voir la liste des affili�s de ce webmaster"><?PHP echo Functions::getfullname($getUsers[$limit1]['lastIP']); ?>&nbsp;<i class="icon-list"></i></a> <br/>
                                        <?PHP } ?>
                                        <span class="Notes">Nom & Pr�nom(s)</span> :  <?PHP echo $getUsers[$limit1]['nom'] . " " . $getUsers[$limit1]['prenom']; ?><br/>
                                        <span class="Notes">Email</span> : <?PHP echo $getUsers[$limit1]['email']; ?><br/>
                                        <span class="Notes">T�l�phone</span> : <?PHP echo $getUsers[$limit1]['telephone']; ?><br/>
                                        <span class="Notes">Code Postal</span> : <?PHP echo $getUsers[$limit1]['codepostal']; ?><br/>
                                        <span class="Notes">Ville</span> : <?PHP echo $getUsers[$limit1]['ville']; ?><br/>
                                        <?PHP if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) { ?>
                                            <span class="Notes">Soci�t�</span> :  <?PHP echo $getUsers[$limit1]['societe']; ?><br/>
                                        <?PHP } ?>
                                        <?PHP if (isReferer($getUsers[$limit1]['typeutilisateur'])) { ?>
                                            <span class="Notes">Projet(s)</span> :  <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 3); ?>  en cours, <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 3, 1); ?> termin�(s)<br/>
                                        <?PHP } ?>
                                        <?PHP if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) { ?>
                                            <span class="Notes">Projet(s)</span> :  <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 4); ?> en cours, <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 4, 1); ?> termin�(s)<br/>
                                        <?PHP } ?>
                                        <span class="Notes">Statut</span> : <?PHP echo $statutLvl[$getUsers[$limit1]['connected']]; ?><br/>
                                        <?PHP if ($getUsers[$limit1]['lastlogin'] > 0) { ?>
                                            <span class="Notes">Derni�re Connexion</span> : <?PHP echo date("d/m/Y � H:m:s", $getUsers[$limit1]['lastlogin']); ?><br/>
                                        <?PHP } ?>
                                        <span class="Notes">Inscription</span> : <?PHP echo date("d/m/Y � H:m:s", $getUsers[$limit1]['joinTime']); ?><br/>

                                        <?PHP if ($_GET['section'] == "utilisateurs" && $getUsers[$limit1]['active'] == 1 && $getUsers[$limit1]['lastIP'] != '111111' && $getUsers[$limit1]['lastIP'] > 0) { ?>
                                            <span class="Notes" style="color:blue;text-decoration:none;">A recharger</span> : 
                                            <?PHP
                                            echo "au moins " . intval($getUsers[$limit1]['AmountLastPayment']);
                                            if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) {
                                                $deviz = " <i class='icon-euro'></i>";
                                                echo $deviz;
                                            }
                                            if (isReferer($getUsers[$limit1]['typeutilisateur'])) {
                                                $deviz = " <i class='icon-dollar'></i>";
                                                echo $deviz;
                                            }
                                            if (isAdmin($getUsers[$limit1]['typeutilisateur'])) {
                                                $deviz = " <i class='icon-euro'></i>";
                                                echo $deviz;
                                            }
                                            ?>
                                            dans son compte.
                                            <br/>    
                                        <?PHP } ?>

                                        <?PHP
                                        if ($_GET['section'] == "utilisateurs" && $getUsers[$limit1]['active'] == 1 && $getUsers[$limit1]['lastIP'] == '111111') {
                                            $joined = Functions::getCountPaid($getUsers[$limit1]['id'], date('n'), date('Y'));
                                            ?>
                                            <span class="Notes" style="color:green">Compte(s) Affili�(s)</span> :  <?PHP echo Functions::getCountAffilies($getUsers[$limit1]['id']) ?> <a href='./compte/utilisateurs.html?section=affiliation&byuser=<?PHP echo $getUsers[$limit1]['id']; ?>' title="Voir la liste des affili�s de ce webmaster"><i class="icon-list"></i></a> <br/>
                                            <span class="Notes" style="color:green">Gain ce mois</span> :  <?PHP echo $joined['paid']; ?> <i class='icon-euro'></i><br/>
                                            <span class="Notes" style="color:green">Total des gains</span> :  <?PHP echo Functions::getCountTotalPaid($getUsers[$limit1]['id']); ?> <i class='icon-euro'></i><br/>
                                        <?PHP } ?>

                                        <span class="Notes" style="color:red;">Solde</span> : 
                                        <?PHP
                                        echo $getUsers[$limit1]['solde'];
                                        if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) {
                                            $deviz = " <i class='icon-euro'></i>";
                                            echo $deviz;
                                        }
                                        if (isReferer($getUsers[$limit1]['typeutilisateur'])) {
                                            $deviz = " <i class='icon-dollar'></i>";
                                            echo $deviz;
                                        }
                                        if (isAdmin($getUsers[$limit1]['typeutilisateur'])) {
                                            $deviz = " <i class='icon-euro'></i>";
                                            echo $deviz;
                                        }
                                        ?>
                                        <br/>

                                    </td>
                                    <?PHP if ($_GET['section'] != "inscriptions" || $getUsers[$limit1]['active'] == 1) { ?>
                                        <?PHP if (!isAdmin($getUsers[$limit1]['typeutilisateur']) && !isSu($getUsers[$limit1]['typeutilisateur'])) { ?>
                                                                                                                                                                                <!--<td>--> 

                                            <?PHP
//                                        if ($getUsers[$limit1]['typeutilisateur'] == 3) {
//                                            $encoursProject = Functions::getProjetsListByUser($getUsers[$limit1]['id'], $getUsers[$limit1]['typeutilisateur'], 0, 1, 1, 1);
//                                        }
//                                        if ($getUsers[$limit1]['typeutilisateur'] == 4) {
//                                        $encoursProject = Functions::getProjetsListByUser($getUsers[$limit1]['id'], $getUsers[$limit1]['typeutilisateur'], 0, 1, 1, 1);
////                                        }
//                                        $ptiCount = count($encoursProject);
//                                        $startPtiCount = 1;
//                                        $rapport = "";
//                                        $rapport .= "<span class='Notes'>Projets en cours</span> (" . $ptiCount . ") : <br/>";
//                                        while ($startPtiCount <= $ptiCount && $ptiCount > 0) {
//                                            if ($encoursProject[$startPtiCount]['lien'] != "") {
//                                                $rapport .="- " . $encoursProject[$startPtiCount]['lien'] . "<br/>";
//                                            }
//                                            $startPtiCount++;
//                                        }
//                                        if ($startPtiCount > 0) {
//                                            echo $rapport;
//                                        } else {
//                                            $rapport = "Aucun projet";
//                                            echo $rapport;
//                                        }
                                            ?>

                                            <?PHP if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) { ?>

                                                <?PHP
//                                            if ($getUsers[$limit1]['typeutilisateur'] == 4) {
//                                                $encoursProject = Functions::getProjetsListByUser($getUsers[$limit1]['id'], 4, 0, 0, 1, 1);
//                                            }
//                                            $ptiCount = count($encoursProject);
//                                            $startPtiCount = 1;
//                                            $rapport = "";
//                                            $rapport .= "<br/><span class='Notes'>Projets en Attente</span>  : <br/>";
//                                            while ($startPtiCount <= $ptiCount) {
//                                                if ($encoursProject[$startPtiCount]['lien'] != "") {
//                                                    $rapport .="- " . $encoursProject[$startPtiCount]['lien'] . "<br/>";
//                                                }
//                                                $startPtiCount++;
//                                            }
//                                            if ($startPtiCount > 0) {
//                                                echo $rapport;
//                                            } else {
//                                                $rapport = "Aucun projet";
//                                                echo $rapport;
//                                            }
                                                ?>
                                            <?PHP } ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                                        <br/><span class="Notes">Termin�</span> : <br/>-->

                                            <!--</td>-->
                                        <?PHP } ?>
                                    <?PHP } ?>
                                    <td style="">
                                        <?PHP
                                        if (isAdmin() || isSu()) {
                                            ?>
                                            <?PHP if ($_GET['section'] != "inscriptions" || $getUsers[$limit1]['active'] == 1) { ?>
                                                <a href="compte/writemessage.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-envelope"></i> Envoyer un message</a><br/>
                                                <a href="compte/accueil.html?section=projects&iduser=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-list"></i> Liste de ses projets</a><br/>
                                                <a href="compte/modifier/profil.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-user"></i> Modifier le Profil</a><br/>
                                                <a href="compte/modifier/password.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-edit"></i> Modifier le password</a><br/>
                                                <a href="compte/modifier/solde.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><?PHP echo $deviz; ?> Modifier le solde</a><br/>

                                                <?PHP if ($getUsers[$limit1]['active'] == 1) { ?> 
                                                    <a href="compte/modification/utilisateurdisable.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-power-off"></i> D�sactiver le compte</a><br/>
                                                <?PHP } if ($getUsers[$limit1]['active'] == 0) { ?> 
                                                    <a href="compte/modification/utilisateurenable.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-check"></i> Activer le compte</a><br/>

                                                <?PHP } ?> 
                                                <a href="compte/supprimer/utitlisateur.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-trash"></i> Supprimer le compte </a>
                                            <?PHP } ?> 

                                            <?PHP if ($_GET['section'] == "inscriptions" && $getUsers[$limit1]['active'] == 0) { ?>
                                                <a href="compte/modification/utilisateurenable.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-check-sign"></i> Approuver la demande</a><br/>
                                                <a href="compte/supprimer/utitlisateur.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove-sign"></i> Rejeter la demande </a>

                                            <?PHP } ?> 
                                        <?PHP } ?>

                                    </td>

                                </tr>

                            <?PHP } ?>



                             



                            <?PHP
                            $idAnnuaireParent = $getCommande[$limit1]['annuaire'];
                            $currentAnnuaireList = "";
                            if ($idAnnuaireParent) {
                                $currentAnnuaireList = Functions::getOneAnnuaireListe($idAnnuaireParent);
                            }

                            $nnu = Functions::getAnnuaireNameById($idAnnuaireParent);

                            if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                                $annuairesFromList = explode(";", $currentAnnuaireList['annuairesList']);
                            } else {
                                $annuairesFromList = array();
                            }


                            $annuairesFromListNew = $annuairesFromList;
                            $annuairesFromListNew = array_filter($annuairesFromListNew);

                            $aEdit = 0;
                            foreach ($annuairesFromListNew as $annuaireThatId) {
                                if (!isset($annuairesTous[$annuaireThatId]) || (isset($annuairesTous[$annuaireThatId]) && (!Functions::checkUrlFormat($annuairesTous[$annuaireThatId]['annuaire']) || $annuairesTous[$annuaireThatId]['active'] == 0))) {
                                    $aEdit++;
                                    unset($annuairesFromListNew[array_search($annuaireThatId, $annuairesFromListNew)]);
                                }
                            }
                            $contentThatListImploded = "";
                            if ($aEdit > 0) {
                                $contentThatListImploded = implode(";", $annuairesFromListNew);
                                $contentThatListImploded .= ";";
                                if ($contentThatListImploded != ";") {
                                    $requete = "UPDATE annuaireslist SET annuairesList = '" . $contentThatListImploded . "' WHERE id=" . intval($getCommande[$limit1]['annuaire']);
                                    $execution = $dbh->query($requete);
                                }
                            }

                            $countAnnuaires = 0;
                            $annuairesFromList = array_filter($annuairesFromListNew);

                            $countAnnuaires = (count($annuairesFromList) - 0);

                            if ($countAnnuaires < 0) {
                                $countAnnuaires = 0;
                            }
                            if (strpos($getCommande[$limit1]['email'], "|")) {
                                list($timePret, $pointAffaire) = explode("|", $getCommande[$limit1]['email']);
                            }

                            $done_tasks_ids = Functions::getCountTaskDone($getCommande[$limit1]['id']);
                            $effectuees = intval(count($done_tasks_ids));

                            // calculating not done tasks
                            $not_done_tasks = array_diff($annuairesFromListNew,$done_tasks_ids);
                            $not_done_tasks_count = count($not_done_tasks);


                            if ($_POST['section'] == "projets" && (isAdmin() || isSu()) || isWebmaster() || isReferer()) {

                                if (isset($getCommande[$limit1]) && (isAdmin() || isSu() || (isWebmaster() && $getCommande[$limit1]['showProprio'] == 1 && $getCommande[$limit1]['proprietaire'] == $idUser) || (isReferer() && $getCommande[$limit1]['affectedTO'] == $idUser && (($getCommande[$limit1]['over'] == 0))))) {
                                    ?>
                                    <tr>
                                        <?PHP if (!isReferer()) { ?>
                                            <td>                                        
                                                <input type="checkbox" value="<?PHP echo $getCommande[$limit1]['id']; ?>" id="check<?PHP echo $getCommande[$limit1]['id']; ?>" name="projets[]" />
                                            </td>
                                        <?PHP } ?>
                                        <td>
                                            <?PHP if (isAdmin() || isSu()) { ?>
                                                <?PHP
                                                $class = "";
                                                $iconClass = "";
                                                if (($getCommande['over'] == 0)) {
                                                    $class = "<input type='text' readonly name='datedemarrage" . $getCommande[$limit1]['id'] . "' class='specialdatedemarrage' id='specialdatedemarrage" . $getCommande[$limit1]['id'] . "' rel='" . $getCommande[$limit1]['id'] . "' style=''  value='";
                                                    $iconClass = "'/>&nbsp;<input type='button' value='ok' id='buttonSend' rel='" . $getCommande[$limit1]['id'] . "' class='buttonSend button color small round'/>";
//                                                    $iconClass .= "<span class='icon-check ' id='".$getCommande[$limit1]['id']."' class='buttonValide'></span>";
                                                }
                                                ?>
                                                <?PHP if ($getCommande[$limit1]['affectedTO'] != 0 && $getCommande[$limit1]['adminApprouve'] == 1 && $getCommande[$limit1]['adminApprouveTime'] <= time()) { ?> 
                                                    <span class="Notes">D�marr� le</span> :  <?PHP echo $class; ?><?PHP echo date('d-m-Y', $getCommande[$limit1]['adminApprouveTime']); ?><?PHP echo $iconClass; ?><br/>
                                                    <?PHP if ($getCommande[$limit1]['adminApprouveTime'] < $getCommande[$limit1]['affectedTime']) { ?> 
                                                        <span class="Notes">(re)Affect� le</span> : <span class="affTOO<?PHP echo $getCommande[$limit1]['id']; ?>"><?PHP echo date('d-m-Y', $getCommande[$limit1]['affectedTime']); ?></span><br/>
                                                    <?PHP } ?>
                                                <?PHP } else { ?>

                                                    <?PHP // if ($getCommande[$limit1]['affectedTime'] != 0 && $getCommande[$limit1]['adminApprouve'] == 1 && !isReferer() && $getCommande[$limit1]['over'] == 0) {                                ?> 
                                                    <?PHP if ($getCommande[$limit1]['envoyer'] == 1 && $getCommande[$limit1]['adminApprouve'] == 1 && $getCommande[$limit1]['affectedTO'] != 0) { ?> 
                                                        <span class="Notes">A D�marrer le</span> :  <?PHP echo $class; ?> <?PHP echo date('d-m-Y', $getCommande[$limit1]['adminApprouveTime']); ?><?PHP echo $iconClass; ?><br/>
                                                    <?PHP } ?>
                                                <?PHP } ?>
                                            <?PHP } ?>
                                            <?PHP
                                            if (isMultiple($getCommande[$limit1]['lien']) && (isWebmaster() || isAdmin() || isSu())) {
//                                                $rs = isMultiple($getCommande[$limit1]['lien']);
                                                $urls = explode(PHP_EOL, $getCommande[$limit1]['lien']);
                                                ?>
                                                <span class="Notes">URL(s)</span> : <?PHP echo count($urls); ?> Site(s) dans le projet.<br/>
                                                <?PHP
                                            } else {
                                                $lienExterne = "#";
                                                if (!strpos($getCommande[$limit1]['lien'], "http")) {
                                                    $lienExterne = "http://" . $getCommande[$limit1]['lien'];
                                                }
                                                ?>
                                                <span class="Notes">URL(s)</span> : <a href="<?PHP echo $lienExterne; ?>" target="_blank" title="Ouvrir l'url"><?PHP echo Functions::longeur($getCommande[$limit1]['lien'], 50); ?> <i class="icon-external-link"></i></a><br/>

                                            <?PHP }
                                            ?>

                                            <?PHP
                                            if ($getCommande[$limit1]['parent'] != 0 && $getCommande[$limit1]['parent'] != $getCommande[$limit1]['id'] && (isWebmaster() || isAdmin() || isSu())) {
                                                $projet = Functions::getLiaisonByID($getCommande[$limit1]['parent']);
                                                ?>
                                                <span class="Notes">Li� au projet</span> : <?PHP echo $projet['lien']; ?><br/>
                                            <?PHP } ?>
                <!--<span class="Notes">Mail</span> : <?PHP echo $getCommande[$limit1]['email']; ?><br/>-->
                <!--                                            <span class="Notes">Cat�gorie</span> : <?PHP echo $getCommande[$limit1]['categorie']; ?><br/>-->
                                            <?PHP if (isAdmin() || isSu() || isWebmaster()) { ?>
                                                <span class="Notes">Annuaires</span> : <?PHP
                                                echo "<a href='./compte/voirliste.html?id=" . $getCommande[$limit1]['annuaire'] . "' title='Voir la liste'>" . $nnu['libelle'] . "</a>";
                                                ?><br/>
                                            <?PHP } ?>
                                            <?PHP if (!isReferer()) { ?>
                                                <span class="Notes">Fr�quence</span> : <?PHP echo Functions::getResult($getCommande[$limit1]['frequence']); ?><br/>
                                            <?PHP } ?>
                                            <?PHP if (isAdmin() || isSu() && $_GET['section'] == "default") { ?>
                                                <span class="Notes">Statut</span> : <span class="statoo<?PHP echo $getCommande[$limit1]['id']; ?>" style="color:green;"> <?PHP echo $echoSpecial; ?></span>
                                            <?PHP } ?>

                                            <?PHP if (isAdmin() || isSu() || isWebmaster()) { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--<span class="activePlus" rel="<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-eye-open"></i> Afficher les d�tails de ce projet</span><br/>-->
                                                <div id="reduire<?PHP echo $getCommande[$limit1]['id']; ?>" class="reduire">
                                                    <span class="Notes">Propos� le</span> : <?PHP echo date('d/m/Y', $getCommande[$limit1]['sendTime']); ?><br/>
                                                    <?PHP if (isAdmin() || isSu()) { ?>

                                                        <?PHP if ($getCommande[$limit1]['over'] == 1 && $getCommande[$limit1]['overTime'] != 0) { ?> 
                                                            <span class="Notes">Termin� le</span> : <?PHP echo date('d/m/Y � H\h:m\m\i\\n', $getCommande[$limit1]['overTime']); ?><br/>
                                                        <?PHP } ?>

                                                        <span class="Notes">Webmaster</span> : <a href="compte/modifier/profil.html?id=<?PHP echo $getCommande[$limit1]['proprietaire']; ?>" title="Voir le profil de ce webmaster"><?PHP echo Functions::getfullname($getCommande[$limit1]['proprietaire']); ?></a>&nbsp;<a href='./compte/accueil.html?section=projects&iduser=<?PHP echo $getCommande[$limit1]['proprietaire']; ?>' title="Voir la liste des projets de ce webmaster"><i class="icon-list"></i></a><br/>
                                                        <?PHP if ($getCommande[$limit1]['affectedTO'] != 0) { ?> 
                                                            <span class="Notes">R�f�renceur</span> : <span class="refTOO<?PHP echo $getCommande[$limit1]['id']; ?>"><a href="compte/modifier/profil.html?id=<?PHP echo $getCommande[$limit1]['affectedTO']; ?>" title="Voir le profil de ce r�f�renceur"><?PHP echo Functions::getfullname($getCommande[$limit1]['affectedTO']); ?></a></span>&nbsp;<a href='./compte/accueil.html?section=projects&iduser=<?PHP echo $getCommande[$limit1]['affectedTO']; ?>' title="Voir la liste des projets de ce r�f�renceur"><i class="icon-list"></i></a><br/>
                                                            <span class="Notes">re-Affecter �</span> : 
                                                            <select name="affectedTOO" class="editRef" rel="<?PHP echo $getCommande[$limit1]['affectedTO']; ?>" data-idProjet="<?PHP echo $getCommande[$limit1]['id']; ?>">
                                                                <?PHP
                                                                if (isSU() || isAdmin()) {
                                                                    echo Functions::getUserOptionList(3, 1, $getCommande[$limit1]['affectedTO'],0,$getCommande[$limit1]['id']);
                                                                }
                                                                ?>
                                                            </select>
                                                        <?PHP } ?>
                                                    <?PHP } ?>



                                                </div>
                                                                                                                                                                                                                        <!--<span class="Notes">Budget</span> : <?PHP // echo $getCommande[$limit1]['budget'] . " " . $_SESSION['devise'];                                                                                                                                                                                                                                                                                                                                     ?><br/>-->
                                            <?PHP } ?>

                                            <?PHP ?>

                                        </td>
                                        <?PHP if (1 == 1) { ?>
                                            <td style="max-width :300px;">
                                                <?PHP
                                                if ($getCommande[$limit1]['consignes'] != "") {
                                                    echo "<div class='comBox' id='commentaires'" . $getCommande[$limit1]['id'] . "' class='reduire'>" . nl2br(stripslashes($getCommande[$limit1]['consignes'])) . "</div>";
                                                } else {
                                                    echo "Aucun commentaire";
                                                }
                                                ?>
                                            </td>
                                        <?PHP } ?>
                                        <td style="">

                                            <?PHP
                                            if ($getCommande[$limit1]['envoyer'] == 1 && $getCommande[$limit1]['adminApprouve'] == 1 && $getCommande[$limit1]['over'] == 0 && $getCommande[$limit1]['affectedTO'] == 0) {
                                                if (isAdmin() || isSu()) {
                                                    ?>
                                                    <a href="compte/modifier/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-edit"></i> Modifier </a><br/>
                                                    <a href="compte/modifier/referenceur.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-user"></i> Affecter � </a><br/>
                                                    <a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove"></i> Annuler </a>

                                                <?PHP } if (isWebmaster()) { ?>
                                                    Propos� D�marr�.<br/>
                                                    Validation en cours.<br/>

                                                    <a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove"></i> Annuler ce projet</a>


                                                    <?PHP
                                                }
                                            } else if ($getCommande[$limit1]['envoyer'] == 1 && $getCommande[$limit1]['adminApprouve'] == 1 && $getCommande[$limit1]['over'] == 0 && $getCommande[$limit1]['affectedTO'] != 0) {
                                                ?>
                                                <?PHP if (!isReferer($idTypeUser) && (isWebmaster($idTypeUser) || isSU($idTypeUser) || isAdmin($idTypeUser))) { ?>
                                                    Projet  en cours <br/>
                                                    <a href="compte/details.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" ><i class="icon-signal"></i> Progression</a><br/>
                                                    <a href="compte/modifier/description.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" ><i class="icon-info"></i> Rapports</a><br/>
                                                    <a href="compte/modification/stopperprojet.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove-sign"></i> Arr�ter projet </a><br/>
                                                    <a href="compte/modifier/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-edit"></i> Modifier </a>

                                                <?PHP } if (isReferer($idTypeUser) && $pointAffaire >= 0) { ?>
                                                    <a href="compte/details.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" ><i class="icon-eye-open"></i> Voir mes t�ches (<?PHP echo intval($pointAffaire); ?>)</a>
                                                <?PHP } ?>
                                                <?PHP if (isAdmin() || isSu()) { ?>
                                                    <!--<br/><a href="compte/modifier/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-edit"></i> Modifier </a><br/>-->
                                                    <!--<br/><a href="compte/modifier/referenceur.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-user"></i> R�-affecter �</a>-->
                                                    <?PHP if ($getCommande[$limit1]['adminApprouveTime'] > time()) { ?>
                                                                                                                                                                                                                                    <!--<br/><a href="compte/modifier/referenceur.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-calendar"></i> D�marrage le</a>-->
                                                    <?PHP } ?>
                                                    <br/><a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-trash"></i> Supprimer </a>

                                                <?PHP } ?>
                                                <?PHP if (isAdmin() || isSu() || isWebmaster()) { ?>
                                                <?PHP } ?>
                                                <?PHP
                                            } else if ($getCommande[$limit1]['envoyer'] == 1 && $getCommande[$limit1]['adminApprouve'] == 1 && $getCommande[$limit1]['over'] == 1 && $getCommande[$limit1]['affectedTO'] != 0) {
                                                ?>
                                                Projet Termin�<br/>
                                                <?PHP if (!isReferer($idTypeUser) && !isReferer($idTypeUser)) { ?>
                                                    <a href="compte/details.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>&from=over"><i class="icon-list"></i> Voir d�tails </a><br/>
                                                    <a href="compte/modification/reprendreprojet.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-refresh"></i> Reprendre </a><br/>
                                                    <?PHP if (isWebmaster($idTypeUser)) { ?>
                                                        <a href="compte/supprimer/retirersite.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-archive"></i> Archiver </a>
                                                    <?PHP } ?>
                                                <?PHP } ?>
                                                <?PHP
                                            } else if ($getCommande[$limit1]['envoyer'] == 1 && $getCommande[$limit1]['adminApprouve'] == 0) {
                                                if (isWebmaster()) {
                                                    if ($solde >= $getCommande[$limit1]['budget']) {
                                                        ?>
                                                        En cours de validation<br/>
                                                        <?PHP
                                                    } else {
                                                        ?>
                                                        <span style='color:red'>Vous n'avez pas assez <br/>de fond sur votre compte<br/> pour ce projet.</span> <br/><br/>
                                                        <?PHP
                                                    }
                                                    ?>
                                                    <a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove"></i> Annuler</a>


                                                    <?PHP
                                                } else if (isSU() || isAdmin()) {
                                                    ?>
                                                    <a href="compte/modification/accepterprojet.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmationAccepter();"><i class="icon-check"></i> Accepter</a><br/>
                                                    <a href="compte/modifier/rejeterprojet.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-trash"></i> Rejeter </a><br/>
                                                    <a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove"></i> Annuler </a>

                                                    <?PHP
                                                }
                                            } else if ($getCommande[$limit1]['envoyer'] == 1 && $getCommande[$limit1]['adminApprouve'] == 2) {
                                                ?>

                                                <span style="color:red">Projet rejet�.</span><br/> 
                                                <div style="" class="tic"><?PHP echo $getCommande[$limit1]['adminRaison']; ?></div><br/><br/>
                                                <?PHP if (isWebmaster()) { ?>
                                                    <a href="compte/modification/soumettresite.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" ><i class="icon-save"></i> Re-proposer</a><br/>
                                                    <a href="compte/modifier/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>"><i class="icon-edit"></i> Editer</a><br/>
                                                <?PHP } ?> 
                                                <?PHP if (isAdmin() || isSu()) { ?>
                                                    <a href="compte/modification/accepterprojet.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmationAccepter();"><i class="icon-check"></i> R�-accepter </a><br/>
                                                <?PHP } ?> 

                                                <a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove"></i> Annuler </a>


                                                <?PHP
                                            } else {
                                                ?>
                                                <a href="compte/modification/accepterprojet.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" ><i class="icon-save"></i> D�marrer</a><br/>
                                                <a href="compte/modifier/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" ><i class="icon-edit"></i> Editer</a><br/>
                                                <a href="compte/supprimer/site.html?id=<?PHP echo $getCommande[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-trash"></i> supprimer</a>
                                                <?PHP
                                            }

                                            if ($getCommande[$limit1]['adminApprouve'] == 1 && $getCommande[$limit1]['affectedTO'] != 0 && (isReferer() || isSu() || isAdmin() || isWebmaster())) {
                                                if ($getCommande[$limit1]['adminApprouveTime'] > time()) {
                                                    $pointAffaire = 0;
                                                }
                                                if (!isWebmaster() && !isReferer() && ($_GET['section'] == "default" || $_GET['section'] == "projects")) {
                                                    if ($getCommande[$limit1]['over'] == 0 && $countAnnuaires > 0) {
                                                        echo '<br/>- T�che(s) du jour : ' . intval($pointAffaire) . '';
                                                    } else {
                                                        echo '<br/>- T�che(s) du jour : 0';
                                                    }
                                                }
                                                $total_tasks = $effectuees+$not_done_tasks_count;

                                                if (isAdmin() || isSu() || isWebmaster()) {
                                                    echo '<br/>- T�che(s) effectu�e(s) : ' . $effectuees . '';
                                                    echo '<br/>- Total de(s) t�che(s) : ' . $total_tasks . '';
                                                }
                                            }
                                            ?>

                                        </td>

                                    </tr>
                                    <?PHP
                                }
                            }
                            ?>

                            <?PHP
                            $limit1++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- Pagination -->
                <?PHP
                if ($nbre_result > pagination) {
                    echo $list_page;
                }
            } else {
                ?>
                <div align="center" width="500" style="width:700px;margin:auto;">
                    <h1>Aucun r�sultat � afficher.</h1>

                </div>

                <?PHP
            }
            ?>


            <!-- End pagination -->
        </div>


    </div>

    <script>
        (function($){
            // turn the element to select2 select style
            $('select[name=affectedTOO]').chosen();
        })(jQuery);
    </script>

<? if ($_POST['section'] == "sites") {?>
     <script type="text/javascript">

             //Delete cat
            function handle_delete(){
                jQuery(".mc_delete").click(function(){

                    var thisis=this;

                    if (confirm('Etes vous sur de vouloir supprimer ce site ?')) {

                        jQuery.post( "files/includes/ajax/echanges_sites.php", {action:'delete',id:jQuery(this).attr("rel")} ).done(function( data ) {

                            data=jQuery.parseJSON( data );

                            alert(data.message);
                            jQuery(thisis).parent().parent().remove();

                        });;

                    }

                });
            }
            handle_delete();

            function handle_active(){
                jQuery(".mc_active").click(function(){

                    var thisis=this;

                        jQuery.post( "files/includes/ajax/echanges_sites.php", {action:'toggle_active', id:jQuery(this).attr("rel")} ).done(function( data ) {

                            data=jQuery.parseJSON( data );
                            jQuery(thisis).html(data.replace_text);

                        });;

                    

                });
            }
            handle_active();

    </script>

<?}?>

</div>
<?PHP
include("files/includes/bottomBas.php")
?>