<?php
	// analyseur V2

	global $_hostname;
	global $_bdd;  
	global $_login; 
	global $_pwd; 
	global $_connexionPDO;

	include_once 'bdd_config.php';
	$_connexionPDO = new PDO('mysql:host='.$_hostname.';dbname='.$_bdd, $_login, $_pwd);

	global $monlien;
    global $multiple;
    global $multiple_urls; // multiple checking
    global $multiple_urls_current; // current-found of multiple checking

    $maxIteration 	= 2000;

	$monlien	 = isset($_REQUEST['monlien']) ? trim($_REQUEST['monlien']): null;
    $nomDomaine	 = isset($_REQUEST['nddcible']) ? trim($_REQUEST['nddcible']): null;
	$pageDeb	 = isset($_REQUEST['pageentree']) ? trim($_REQUEST['pageentree']): null;
    $multiple    = isset($_REQUEST['multiple']) ? intval($_REQUEST['multiple']): null; // several backlinks

    // fix with|without www
    $parse_monlien = parse_url($monlien);
    if (!empty($parse_monlien['host'])){
        $monlien =  str_replace('www.','',$parse_monlien['host']);
    }

	function runner($urlgo, $ndd) {

		global $_connexionPDO;
		global $find;
		global $monlien;
        global $multiple;
        global $multiple_urls;
        global $multiple_urls_current;
		
		$urlCnt = urldecode($urlgo);

		$optionsCnt=array(
			  CURLOPT_URL            	=> $urlCnt,      
			  CURLOPT_RETURNTRANSFER 	=> true,       
			  CURLOPT_HEADER         	=> false,      
			  CURLOPT_FAILONERROR    	=> true,   
			  CURLOPT_FOLLOWLOCATION	=> true,
			  CURLOPT_USERAGENT 		=> 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13',
			  CURLOPT_REFERER			=> 'http://www.google.fr/search?hl=fr&source=hp&q='.$ndd.'&aq=f&aqi=g2&aql=&oq=',
		);
		$CURLCnt = curl_init();
		if(empty($CURLCnt)){
            die("ERREUR curl_init : Il semble que cURL ne soit pas disponible.");
        }
		curl_setopt_array($CURLCnt, $optionsCnt);
        $request_error = true;
        $attempts_count = 0;
        $max_attempts_cnt = 3; // max request-attempts
        while ($request_error){
            $cnt 	= curl_exec($CURLCnt);
            if(curl_errno($CURLCnt)){
                $attempts_count++;
                if ($attempts_count>=$max_attempts_cnt){// check max request attempts
                    // may be write "total error"?
                    break;
                }
                echo "<br/>Try again.Attempt #".$attempts_count.'<br/>';
                echo "ERREUR curl_exec : ".curl_error($CURLCnt).'. Url:'.$urlgo;
                usleep(300000); // 0.3 second
                $request_error = true;
            }else{
               if ($attempts_count!=0){
                 $attempts_count++;
                 echo "<br/>Try again.Attempt #".$attempts_count.'<br/>';
                 echo "SUCCESS curl_exec. Url:".$urlgo;
               }
               break; // all fine
            }
        }

        $mimetype =curl_getinfo($CURLCnt, CURLINFO_CONTENT_TYPE); // get mitetype
		curl_close($CURLCnt);

        // fix rss feed (xml files)
        $rss_feed = false;
        if ( (strpos($ndd,'.xml') !== false) && ($mimetype=='text/xml')) {
            $rss_links = get_links_from_xml($cnt);
            $rss_feed = true;
        }

        $matches = array();
        $find = '';
		if( ( preg_match_all("/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU", $cnt, $matches) ) || ($rss_feed)  ) {

            // set rss links
            if ($rss_feed){
                $matches[2] = $rss_links;
            }

            foreach($matches[2] as $link) {
				$urlpropre = '';
				if('http://www.'.$ndd == substr($link, 0, (11+strlen($ndd))) ) {
					$urlpropre = $link;
				} else {
					if('http://'.$ndd == substr($link, 0, (7+strlen($ndd))) ) {
						$urlpropre = $link; 
					} else {
						if('tel:' != substr($link, 0, 4) && 'https://' != substr($link, 0, 8) && 'http://' != substr($link, 0, 7) && 'javascript' != substr($link, 0, 10) && substr($link, 0, 1) != "'" ) {
							$urlpropre = $link;
							if(strlen($link) < 7 || 'http://' != substr($link, 0, 7)) { 
								if($link != '') { 
									if(substr($link, 0, 1) != '/') {
										$urlpropre = 'http://www.'. str_replace('//', '/', $ndd . '/' . $link); 
									} else {
										$urlpropre = 'http://www.'. str_replace('//', '/', $ndd . $link); 
									}
								} else {
									$urlpropre = 'http://www.'. $ndd;
								}
							}
						}
					}
				}

                if ($rss_feed){
                    $urlpropre = $link;
                }

                // inserting urls for "parsing tree"
				if($urlpropre != '') {

                    // fix: catching relative path
                    $path_pieces = explode('/',$ndd);
                    if ( (count($path_pieces)>1) && !$rss_feed){
                        if(substr($link, 0, 1) == '/') {
                            $host = array_shift($path_pieces);
                            $urlpropre = 'http://www.'.str_replace('//', '/',$host.$link);
                        }
                    }

					$nb = 0;
					$req = "SELECT count(1) nb FROM bot_urls WHERE url_analysee = '" . $urlpropre . "' "; // echo $req . '<br/>';
					$resultats2 = $_connexionPDO->query($req);

                    if ($resultats2) {
                        while ($resultat2 = $resultats2->fetch(PDO::FETCH_OBJ)) {
                            $nb = intval($resultat2->nb);
                        }
                    }


                    // check if url is unique
					if($nb < 1) {
						$req = "INSERT INTO `bot_urls` (`url_analysee`, `url_recherchee`, `est_traitee`) VALUES('" . $urlpropre . "', '" . $monlien . "', 0)";
						$_connexionPDO->exec($req);
					}
				}


                // checking monlien/s
                if (!$multiple){
                    $parsed_link = parse_url($link);
                    if (!empty($parsed_link['host'])){
                        $link_host = str_replace('www.','',$parsed_link['host']);
                        if ($link_host==$monlien){
                            //var_dump($urlgo,$link,$link_host,$monlien);
                            $find = $urlgo;
                            break;
                        }
                    }
                }else{ // multiple checking

                    // testing multichecking
                    /*
                    $test = array_fill(0,15,'http://www.example.com');
                    $test[] = 'http://www.urmad.fr';
                    $test[] = 'http://www.netplus-proprete.com';
                    $test[] = 'http://www.clown-sirouy.fr';
                    $test[] = 'http://www.camping-vagues-oceanes.com';
                    shuffle($test);
                    $link = $test[array_rand($test)];
                    */

                    $parsed_link = parse_url($link);
                    if (!empty($parsed_link['host'])) {
                        $link_host = str_replace('www.', '', $parsed_link['host']);

                        foreach($multiple_urls as $index=>$item){
                            $info = parse_url($item);
                            $backlink = str_replace('www.', '',$info['host']);
                            if($link_host==$backlink){
                                $find = $urlgo;
                                $multiple_urls_current = $backlink;
                                // remove found backlink from checking
                                if(($key = array_search($item, $multiple_urls)) !== false) {
                                    unset($multiple_urls[$key]);
                                }
                                break;
                            }
                        }
                    }
                }

                // @deprecated
                /*
				if( substr($link, 0, strlen($monlien)) == $monlien) {
					$find = $urlgo;
                    var_dump($link);
                    die;
					break;
				}*/

			}
		}
			
		$_connexionPDO->exec("UPDATE `bot_urls` SET `est_traitee` = 1 WHERE `url_analysee` = '" . $urlgo . "' ");
	}

    /*********************************************************************************************/
    /*********************************************************************************************/

    function get_links_from_xml($content){
        $rss = simplexml_load_string($content);
        $links = array();
        //$links[] = (string)$rss->channel->link; // need?
        foreach($rss->channel->item as $item){
            $links[] = (string)$item->link;
        }
        return $links;
    }

	
	/*********************************************************************************************/
	/*********************************************************************************************/
	
	function install_bdd($nomDomaine, $pageDeb) {

		global $_connexionPDO;
		global $monlien;


		$_connexionPDO->exec("TRUNCATE TABLE bot_urls");


        $_connexionPDO->exec("CREATE TABLE IF NOT EXISTS `bot_urls` (
							  `id_url` int(11) NOT NULL AUTO_INCREMENT,
							  `url_analysee` varchar(300) DEFAULT NULL,
							  `url_recherchee` varchar(300) DEFAULT NULL,
							  `est_traitee` int(11) DEFAULT 0,
							  PRIMARY KEY (`id_url`)
							) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");

        $_connexionPDO->exec("INSERT INTO `bot_urls` (`url_analysee`, `url_recherchee`, `est_traitee`) VALUES('" . urlDebut($nomDomaine, $pageDeb) . "', '" . $monlien . "', 0)");
    }
	
	/*********************************/
	
	function urlDebut($ndd, $pageDebut) {

		$urlgo = $pageDebut;
		if(strlen($pageDebut) < 7 || 'http://' != substr($pageDebut, 0, 7)) { 
			if($pageDebut != '') { 
				if(substr($pageDebut, 0, 1) != '/') {
					$urlgo = 'http://www.'. str_replace('//', '/', $ndd . '/' . $pageDebut); 
				} else {
					$urlgo = 'http://www.'. str_replace('//', '/', $ndd . $pageDebut);
				}
			} else {
				$urlgo = 'http://www.'. $ndd;
			}
		}

		return trim($urlgo);
	}
	
	/*********************************/
	
	function findInterneLink() {	
		global $_connexionPDO;
		$result = '';
		
		$resultats = $_connexionPDO->query(" SELECT url_analysee FROM bot_urls WHERE est_traitee = 0 LIMIT 0, 1");
        if ($resultats) {
            while ($resultat = $resultats->fetch(PDO::FETCH_OBJ)) {
                $result = $resultat->url_analysee;
            }
        }
		
		return $result;
	}
	
	/*********************************************************************************************/
	/*********************************************************************************************/
	
	install_bdd($nomDomaine, $pageDeb);

    // if multiple checking
    if ($multiple){
        // get list of checking backlinks
        // you can get from another place
        $nomDomaine = rtrim($nomDomaine,'/');
        $result = $_connexionPDO->query("SELECT `backlink`
                                         FROM    bot_checking_url
                                         WHERE  `domain` LIKE '%".$nomDomaine."%'");
        if ($result) {
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $multiple_urls[] = $row['backlink'];
            }
        }

        if (!count($multiple_urls)){
            die("ERROR multiple-checking: url `".$nomDomaine."` hasn`t checking backlinks");
        }
    }


	$iterator = 0;

	echo '<ol>';
	while(($urlEnAnalyse = findInterneLink()) !== '' && $iterator < $maxIteration) {
		$iterator++;
		runner($urlEnAnalyse, $nomDomaine);
		global $find;
		echo '<li style="font-size:10px;color:';
		if( $find != '') {
            echo '#46B525;';
            if (!$multiple)
                $iterator = $maxIteration + 1;
            else{
                if (count($multiple_urls)==0){
                    break;
            }
        }
        } else {
            echo '#dd0000;';
        }
		echo '"> ' . $urlEnAnalyse ;
        if ($multiple && ($find!='')){ // backlink
            echo '    Backlink for: <b>'.strtoupper($multiple_urls_current).'</b>';
        }
		echo '</li>';
	}
	echo '</ol>';
?>