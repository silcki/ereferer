<?PHP
@session_start();
$_SESSION['alertLogincontent'] = "";

$a = 3;
$page = 3;
include('files/includes/topHaut.php');

//print_r($_POST);
$_POST['typeutilisateur'] = 4;

if (!isset($_COOKIE['ID_PARRAIN']) || (isset($_COOKIE['ID_PARRAIN']) AND intval($_COOKIE['ID_PARRAIN'] == 0))) {
    $_SESSION['ID_PARRAIN'] = 0;
} else {
//    $montantAffiliation = $parametres['affiliation']["valeur"];
    $_SESSION['ID_PARRAIN'] = intval($_COOKIE['ID_PARRAIN']);
}



$montantAffiliation = 0;


if (empty($_POST['nom']) || !Functions::checkNameFormat($_POST['nom'])) {
    $_SESSION['alertLogincontent'] .= "Veuillez saisir un nom valide.<br/>";
}

if (empty($_POST['prenom']) || !Functions::checkNameFormat($_POST['prenom'])) {
    $_SESSION['alertLogincontent'] .= "Veuillez saisir un ou des pr�nom(s) valide(s).<br/>";
}

if (isset($_POST['typeutilisateur']) && empty($_POST['typeutilisateur'])) {
//    $_SESSION['alertLogincontent'] .= "Veuillez obligatoirement choisir le type d'utilisateur.<br/>";
}

if (empty($_POST['email']) || !Functions::checkEmailFormat($_POST['email'])) {
    $_SESSION['alertLogincontent'] .= "Veuillez obligatoirement saisir une adresse email valide.<br/>";
} else {
    if (Functions::isEmailExist($_POST['email'])) {
        $_SESSION['alertLogincontent'] .= "Cette adresse email existe d�j� dans la base.<br/>";
    }
}

if (empty($_POST['pass']) || empty($_POST['confpass'])) {
    $_SESSION['alertLogincontent'] .= "Veuillez entrer le mot de passe.<br/>";
} else {
    if ($_POST['pass'] != $_POST['confpass']) {
        $_SESSION['alertLogincontent'] .= "Les mots de passe ne concordent pas.<br/>";
    }
}

if (!empty($_POST['societe'])) {
//    if (!Functions::checkNameFormat($_POST['societe'])) {
//        $_SESSION['alertLogincontent'] .= "Veuillez saisir le nom de votre soci�t� <br/>";
//    }
} else {
    $_POST['societe'] = "";
}

if (!isset($_POST['ville']) || empty($_POST['ville'])) {
    $_POST['ville'] = "";
}

if (!empty($_POST['adresse'])) {
    if (strlen($_POST['adresse']) < 5) {
//        $_SESSION['alertLogincontent'] .= "Veuillez saisir une adresse compl�te <br/>";
    }
} else {
    $_POST['adresse'] = "";
}

if (!empty($_POST['telephone']) && is_int($_POST['telephone']) && strlen($_POST['telephone']) < 8) {
    $_SESSION['alertLogincontent'] .= "Veuillez saisir un num�ro de t�l�phone valide<br/>";
}


if (!empty($_POST['siteweb']) && !Functions::checkUrlFormat($_POST['siteweb'])) {
    $_SESSION['alertLogincontent'] .= "Veuillez saisir l'url de votre site personnel<br/>";
}

if ($_SESSION['alertLogincontent'] == "") {

    $active = 0;
    $devise = 1;
    $contractAccepted = 0;
    $soldeAct = 0;
    $frais = 0;
    $typeutilisateur = 3;

    if ($_POST['typeutilisateur'] == 4) {
        $devise = 2;
        $typeutilisateur = 4;
        $frais = 0;
    }

    $setUser = false;

    /* @deprecated
    $setUser = $dbh->query('INSERT INTO utilisateurs (id,nom,prenom,societe,email,emailsecondaire,ville,username,
    password,lastlogin,joinTime,adresse,telephone,siteweb,lastIP,solde,frais,devise,
    typeutilisateur,codepostal,styleAdmin,active,contractAccepted)
    VALUES("", "' . addslashes($_POST['nom']) . '", "' . addslashes($_POST['prenom']) . '",
     "' . addslashes($_POST['societe']) . '", "' . $_POST['email'] . '",
     "' . $_POST['email'] . '", "' . addslashes($_POST['ville']) . '",
      "' . $_POST['email'] . '", "' . Functions::getCryptePassword($_POST['pass']) . '",
       "' . time() . '", "' . time() . '", "' . addslashes($_POST['adresse']) . '",
        "' . addslashes($_POST['telephone']) . '", "' . $_POST['siteweb'] . '",
         "' . $_SESSION['ID_PARRAIN'] . '","' . $soldeAct . '", "' . $frais . '",
          "' . $devise . '", "' . $typeutilisateur . '", "' . addslashes($_POST['codepostal']) . '",
          "' . $montantAffiliation . '","' . $active . '", "' . $contractAccepted . '" ) '); */


    // purify
    $HTML = new HTML;
    $_POST = $HTML->purifyParams($_POST);


    // prepared
    $stmt = $dbh->prepare('INSERT INTO  utilisateurs  (
                                        nom,
                                        prenom,
                                        societe,
                                        email,
                                        emailsecondaire,
                                        ville,
                                        username,
                                        password,
                                        lastlogin,
                                        joinTime,
                                        adresse,
                                        telephone,
                                        siteweb,
                                        lastIP,
                                        solde,
                                        frais,
                                        devise,
                                        typeutilisateur,
                                        codepostal,
                                        styleAdmin,
                                        active,
                                        contractAccepted,
                                        country,
                                        numtva)
                                        VALUES(
                                                :nom,
                                                :prenom,
                                                :societe,
                                                :email,
                                                :emailsecondaire,
                                                :ville,
                                                :username,
                                                :password,
                                                :lastlogin,
                                                :joinTime,
                                                :adresse,
                                                :telephone,
                                                :siteweb,
                                                :lastIP,
                                                :solde,
                                                :frais,
                                                :devise,
                                                :typeutilisateur,
                                                :codepostal,
                                                :styleAdmin,
                                                :active,
                                                :contractAccepted,
                                                :country,
                                                :numtva)');

    $stmt->bindParam(':nom', $_POST['nom']);
    $stmt->bindParam(':prenom', $_POST['prenom']);
    $stmt->bindParam(':societe', $_POST['societe']);
    $stmt->bindParam(':email',$_POST['email']);
    $stmt->bindParam(':emailsecondaire', $_POST['email']);
    $stmt->bindParam(':ville',$_POST['ville']);
    $stmt->bindParam(':username', $_POST['email']);
    $stmt->bindParam(':password', Functions::getCryptePassword($_POST['pass']));
    $stmt->bindParam(':lastlogin',time());
    $stmt->bindParam(':joinTime', time());
    $stmt->bindParam(':adresse',$_POST['adresse']);
    $stmt->bindParam(':telephone', $_POST['telephone']);
    $stmt->bindParam(':siteweb',$_POST['siteweb']);
    $stmt->bindParam(':lastIP', $_SESSION['ID_PARRAIN']);
    $stmt->bindParam(':solde',$soldeAct);
    $stmt->bindParam(':frais',$frais);
    $stmt->bindParam(':devise', $devise);
    $stmt->bindParam(':typeutilisateur',$typeutilisateur );
    $stmt->bindParam(':codepostal', $_POST['codepostal'] );
    $stmt->bindParam(':styleAdmin',$montantAffiliation);
    $stmt->bindParam(':active', $active);
    $stmt->bindParam(':contractAccepted', $contractAccepted );
    $stmt->bindParam(':country', $_POST['country'] );
    $stmt->bindParam(':numtva', $_POST['numtva'] );

    $setUser = $stmt->execute();

    if ($setUser) {

        if (isset($_COOKIE['ID_PARRAIN'])) {
            unset($_COOKIE['ID_PARRAIN']);
            setcookie('ID_PARRAIN', null, -1, '/');
        }

        $name = $_SESSION['allParameters']['logotext']["valeur"];
        $urlSite = $_SESSION['allParameters']['url']["valeur"];
        @require_once('./files/includes/phpmailer/class.phpmailer.php');
        $body = '';
        $body .= 'Bienvenue ' . $_POST['nom'] . ' ' . $_POST['prenom'] . '<br/><br/>';
        $body .= 'Votre compte ' . $name . ' &agrave; bien &eacute;t&eacute; cr&eacute;&eacute;.<br/><br/>';
        $body .= 'Vos identifiants sont:<br/>';
        $body .= '<u>Identifiant </u> : <strong>' . $_POST['email'] . '</strong><br/>';
        $body .= '<u>Mot de passe</u> : <i>(Votre mot de passe)</i><br/><br/>';
        $body .= 'Il vous faudra cependant activer votre compte en cliquant sur le lien ci-dessous:<br/><br/>';
        $body .= $urlSite . 'activation.html?hash=' . base64_encode($_POST['email']);
        $body .= '<br/><br/>';
        $body .= "PS : Si ce lien ne marche pas, copiez-collez le dans la barre d'adresse de votre navigateur et appuyez sur la touche ENTER.<br/><br/>";
        $body .= "Bien Cordialement,<br/>";
        $body .= 'Emmanuel';
// echo "ok";
        $mail = new PHPMailer();
        $mail->Timeout = '30';
        $mail->CharSet = 'UTF-8';
        $mail->From = "noreply@ereferer.fr";
        $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
        $mail->Subject = "Bienvenue sur " . $name . "";
        $mail->AltBody = "";
        $mail->IsHTML(true);
        $mail->MsgHTML($body);
        $mail->AddAddress($_POST['email'], "");
        $mail->Send();
    }
}

if ($_SESSION['alertLogincontent'] != "") {
    header('location:/inscription.html?statut=error');
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <div class="permalink">
            <h4><a nohref="#">Inscription d�roul�e avec succ�s.</a></h4>
        </div>

        <div class="notification success" style="width:600px;">
            <p>
                <span>Succ�s</span> 
                Votre demande d'inscription a �t� enregistr�e avec succ�s. <br/>
                Un email d'activation vous a �t� envoy� dans votre courriel.<br/><br/>
                <strong> Veuillez consulter votre bo�te de r�ception ou <span style="color:red;">vos spam</span>.</strong><br/><br/>

                Merci.
            </p>
        </div>



        <!-- Pagination -->

        <!-- End pagination -->

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php");
?>