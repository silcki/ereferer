<?PHP
@session_start();


if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

$section = $_GET['block'];


if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

if ($section==='about'){
    $a = 40;
    $b = 1;
}

$page = 1;
include('files/includes/topHaut.php');
$desc = $_SESSION['allParameters']['redaction_desc']['valeur'];
?>

<!--breadcrumbs ends -->
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="one">
            <?php if ($section==='about'){?>
                <div>
                    <?php echo  html_entity_decode($desc,ENT_COMPAT,'ISO-8859-1');?>
                </div>
            <?php } ?>
    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>