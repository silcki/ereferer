<?PHP
session_start();

$a = 1;
$b = 0;
$page = 1;

include('files/includes/topHaut.php');

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/MailProvider.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Likes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Projects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Jobs.php';


$projectModel = new Projects($dbh);
$jobsModel = new Jobs;

//var_dump($remuneration);
$stitle = "three-fourth";
//if (isReferer()) {
// strtotime(date("d-m-Y", (time()+$dureeJournee)
$stitle = "one";
//}
?>
<!--breadcrumbs ends -->
<div class="container annuaire_details_page">
    <div class="one">

        <?PHP
        if ($stitle == "tree-fourth") {
            include("files/includes/menu.php");
        }
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            header('location:./accueil.html');
        } else {
            $_GET['id'] = intval($_GET['id']);
        }
//        unset($_SESSION['tacheTampon']);


        // CLEAR TEST

        /*
        if (isset($_GET['test_clear']) && !empty($_GET['test_clear'])){
            unset($_SESSION['test_details_reaffecting']);
            unset($_SESSION['test_details_writer_id']);
            unset($_SESSION['test']);
        }

        // TEST
        if (isset($_GET['test']) && !empty($_GET['test']) && !isset($_SESSION['test'])){
            $date = $_GET['date'];
            $freq = $_GET['freq'];
            $project_id = $_GET['id'];
            $writer_id = isset($_GET['writer_id'])? $_GET['writer_id'] : $idUser;
            $rea_time = $_GET['reaffected_time'];

            $_SESSION['test_details_writer_id'] = $writer_id;
            $_SESSION['test_details_reaffecting'] = 0;
            $_SESSION['test'] = 1;

            echo '<br/>TEST<br/>';
            $projectModel->testDetailsPage($project_id,$writer_id,$date,$freq,$rea_time,true);
        }*/


        $getCommande = Functions::getCommandeByID($_GET['id']);
        $count = 0;
        if ($getCommande) {
            $count = count($getCommande);
        }


        // checking access
        Functions::checkAccess(isWebmaster(),$idUser,$getCommande['proprietaire']);

        //echo '<pre>';
        //var_dump($getCommande);
        //echo '</pre>';
        //die;

        if ($count > 0 && $getCommande) {

            if ($getCommande['affectedSec'] != "" && $getCommande['affectedSec'] != $defaultHash) {
                $specialAssignement = 0;
                $secondaireRef = explode(";", $getCommande['affectedSec']);
                $secondaireRef = array_filter($secondaireRef);
                if (in_array($idUser, $secondaireRef)) {
                    $idUser = $getCommande['affectedTO'];
                    $specialAssignement = 1;
                }
            }

            if ($getCommande['envoyer'] != 1 || $getCommande['sendTime'] == 0 || $getCommande['adminApprouve'] != 1) {
//                $_SESSION['alertLogincontent'] = "Ce projet n'est pas encore soumis ou n'existe pas!!<br/>";
//                header('location: ./accueil.html?statut=error');
//                header('location: ./accueil.html');
            }

            if ($getCommande['over'] == 1 && isReferer()) {
//                $_SESSION['alertLogincontent'] = "Ce projet est d�j� terminer.<br/>";
//                header('location:./accueil.html?statut=success');
            }
            if (isReferer() && $getCommande['affectedTO'] != $idUser) {
//                $_SESSION['alertLogincontent'] = "Ce projet ne vous a pas �t� affecter ou vous a �t� repris par l'administrateur.<br/>";
//                header('location: ./accueil.html?statut=error');
            }
            if (isReferer() && ($getCommande['adminApprouveTime'] > time() || $getCommande['adminApprouveTime'] == 0)) {
                $_SESSION['alertLogincontent'] = "Ce projet n'est pas encore d�marrer ou est reporter pour plutard.<br/>";
//                header('location: ./accueil.html?statut=warning');
            } else {
                $timeControl = $getCommande['adminApprouveTime'];
            }

            $varExplode = $getCommande['email'];
            $timeControl = "";
            if (strpos($varExplode, "|")) {
                list($timeControl, $pointAffaireControl) = explode("|", $varExplode);
            }
        } else {
            $_SESSION['alertLogincontent'] = "Le projet que vous essayez d'atteindre est inexistant!<br/>";
            header('location: ./accueil.html?statut=error');
        }

        //var_dump($getCommande);


        if (isAdmin() || isSu() || isWebmaster() || isSuperReferer() || (isReferer() && ($timeControl == $timeControl) )) {

            // BONUS PROJECTS FOR WRITERS
            $show_bonus_projects = false;

            if (isReferer()){
                $show_bonus_projects = !empty($_SESSION['connected']['bonus_projects'])? true : false;
            }

            // test

            //var_dump($enCours);
            //$enCours = 0;
            //$show_bonus_projects = true;

            // checking writer project bonus
            // basic condition
            $BONUS_WRITER_ACCESS = ( isReferer() && ($enCours==0) &&  $show_bonus_projects); // basic condition

            // extra condition
            if (!$BONUS_WRITER_ACCESS){
                $BONUS_WRITER_ACCESS = ( isReferer() && isset($_SESSION['connected']['BONUS_ACTIVE']) &&  $show_bonus_projects);
            }

            //var_dump($getCommande);
            //die;

        //var_dump($getCommande['affectedTO'] == $idUser && isReferer());
        //die;
            if ($count > 0 && (  $BONUS_WRITER_ACCESS ||  ($getCommande['affectedTO'] == $idUser && isReferer() ) || isAdmin($idTypeUser) || isSU($idTypeUser) || isSuperReferer() || ($getCommande['proprietaire'] == $idUser && isWebmaster()) )) {

                $idAnnuaireParent = $getCommande['annuaire'];
                $projet_id = $getCommande['id'];


                // Annuaire Words Count
                $LIST_ANNUAIRE_WORDS_COUNT = 0;
                $writer_tarif_redaction = 0;

                if (isReferer()){
                    // specified for writer tarif
                    $writer_tarif_redaction = floatval($_SESSION['connected']['tarif_redaction']);
                    if (!$writer_tarif_redaction){ // common tarif
                        $writer_tarif_redaction = floatval($_SESSION['allParameters']['writer_price_100_words']['valeur']);
                    }
                }

                if (isSu() || isAdmin()){
                    $current_writer_id = $getCommande['affectedTO'];
                    if (!empty($current_writer_id)){
                        $userInfo = Functions::getUserInfos($current_writer_id);

                        $writer_tarif_redaction = floatval($userInfo['tarif_redaction']);
                        if (!$writer_tarif_redaction){ // common tarif
                            $writer_tarif_redaction = floatval($_SESSION['allParameters']['writer_price_100_words']['valeur']);
                        }
                    }
                }

                //var_dump('WRITER TARIF REDACTION = '.$writer_tarif_redaction);

                $getRestarted = Functions::getRestartedSoumissionsByProcject($getCommande['id']);
                $getSupps = Functions::getPersoWebmasterCompte($getCommande['id']);
                $webmasterCan = Functions::isWebmasterCan($getCommande['proprietaire']);


                $ownerInfo = Functions::getUserInfos($getCommande['proprietaire']);
                $owner_solde = floatval($ownerInfo['solde']);

                // get & prepare writers`s likes
                $likesModel = new Likes();
                $likes_data = $likesModel->getWritersLikesByProjectId($projet_id);
                $writers_likes = array();
                foreach($likes_data as $data){
                    $writers_likes[$data['annuaire_id']][$data['writer_id']]['value'] = $data['value'];
                    $writers_likes[$data['annuaire_id']][$data['writer_id']]['unliked_text'] = $data['webmaster_comment'];
                }

                ?>
                <div class="<?PHP echo $stitle; ?>">
                    <?PHP
                    $nnu = Functions::getAnnuaireNameById($getCommande['annuaire']);
                    $LIST_ANNUAIRE_WORDS_COUNT = $nnu['words_count'];

                    if (isset($_GET['statut']) && $_GET['statut'] != "") {
                        ?>
                        <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:900px">
                            <p>
                                <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>-->
                                <?PHP echo $_SESSION['alertLogincontent']; ?>
                            </p>
                        </div>
                    <?PHP } ?>

                    <h4>D�tails du projet </h4>
                    <?PHP if (isAdmin() || isSu() || isWebmaster()) { ?>

                        <h6 style="width:400px;float:left;">
                            <?PHP if (isAdmin() || isSu()) { ?>
                                <span class="litleTitle">Client</span> : <a href="compte/modifier/profil.html?id=<?PHP echo $getCommande['proprietaire']; ?> " title="Voir le profil de ce webmaster"><?PHP echo Functions::getfullname($getCommande['proprietaire']); ?></a>  <a href="./compte/accueil.html?section=projects&amp;iduser=<?PHP echo $getCommande['proprietaire']; ?>" title="Voir la liste des projets de ce webmaster"><i class="icon-list"></i></a><br/>
                                <span class="litleTitle">R�f�renceur(s)</span> : <br>
                                <i class="icon-arrow-right"></i> <a href="compte/modifier/profil.html?id=<?PHP echo $getCommande['affectedTO']; ?>" title="Voir le profil de ce r�f�renceur"><?PHP echo Functions::getfullname($getCommande['affectedTO']); ?></a>  <a href="./compte/accueil.html?section=projects&amp;iduser=<?PHP echo $getCommande['affectedTO']; ?>" title="Voir la liste des projets de ce r�f�renceur"><i class="icon-list"></i></a><br/>
                                <?PHP
                                $refList = 0;
                                $refSecList = explode(';', $getCommande['affectedSec']);
                                foreach ($refSecList as $ref) {
                                    $nameREF = Functions::getfullname($ref);
                                    if ($ref > 0 && $ref != "" && trim($nameREF) != "") {
                                        ?>
                                        <i class="icon-arrow-right"></i> <a href="compte/modifier/profil.html?id=<?PHP echo $ref; ?>" title="Voir le profil de ce r�f�renceur"><?PHP echo $nameREF; ?></a>  <a href="./compte/accueil.html?section=projects&amp;iduser=<?PHP echo $ref; ?>" title="Voir la liste des projets de ce r�f�renceur"><i class="icon-list"></i></a><br/>
                                        <?PHP
                                        $refList++;
                                    }
                                }
                                if ($refList == 0) {
                                echo "Aucun r�f�renceur secondaire<br/>";
                                }
                                ?>
                                <?PHP if ($getCommande['affectedTO'] != 0 && $getCommande['adminApprouve'] == 1 && $getCommande['adminApprouveTime'] <= time()) { ?>
                                    <span class="litleTitle">D�marr� le</span> : <?PHP echo date('d/m/Y', $getCommande['adminApprouveTime']); ?><br/>
                                    <?PHP if ($getCommande['adminApprouveTime'] < $getCommande['affectedTime']) { ?>
                                        <span class="litleTitle">R�affect� le</span> : <?PHP echo date('d/m/Y', $getCommande['affectedTime']); ?><br/>
                                    <?PHP } ?>
                                <?PHP } else { ?>

                                    <?PHP // if ($getCommande['affectedTime'] != 0 && $getCommande['adminApprouve'] == 1 && !isReferer() && $getCommande['over'] == 0) {      ?>
                                    <?PHP if ($getCommande['envoyer'] == 1 && $getCommande['adminApprouve'] == 1 && $getCommande['affectedTO'] != 0) { ?>
                                        <span class="litleTitle">A D�marrer le</span> : <?PHP echo date('d/m/Y', $getCommande['adminApprouveTime']); ?><br/>
                                    <?PHP } ?>
                                <?PHP } ?>

                            <?PHP } ?>

                            <span class="litleTitle">Projet</span> : <a href="<?PHP echo $getCommande['lien']; ?>" target='_blank'><?PHP echo $getCommande['lien']; ?></a> <br/>
                            <span class="litleTitle">Annuaires</span> : <?PHP echo "<a href='./compte/voirliste.html?id=" . $getCommande['annuaire'] . "' title='Voir la liste'>" . $nnu['libelle'] . "</a>"; ?> <br/>
                            <!--<span class="litleTitle ">Email</span> : <?PHP echo $getCommande['email']; ?><br/>-->
                            <span class="litleTitle">Fr�quence</span> : <?PHP echo Functions::getResult($getCommande['frequence']); ?><br/>
                            <span class="litleTitle">Nombre de mots souhait�s</span> :
                            <?php
                                if($LIST_ANNUAIRE_WORDS_COUNT > 0 ){
                            ?>
                                <?php echo $LIST_ANNUAIRE_WORDS_COUNT; ?> mots<br/>
                            <?php } else { ?>
                                  <a href="/compte/modifier/listannuairecontent.html?id=<?php echo $nnu['id'];?>">Pas de minimum souhait�</a>
                            <?php } ?>


                            <?PHP if ($getCommande['over'] == 1) { ?>
                                <span class="litleTitle">Termin� le</span> : <?PHP echo date('d/m/Y', $getCommande['overTime']); ?><br/>
                            <?PHP } ?><br/>

                            <?php if (1) { ?>
                                <?php if (!isReferer()) { ?>
                                    <div style="font-weight:bold;">
                                        <span class="" style="color:green;">Liens trouv�s:</span><span class="nbrFound"><span></span> <span class="nbrFoundPercent"></span></span><br/>
                                        <span class="" style="color:orange;">Liens en attente:</span><span class="nbrNotFoundYet"><span></span> <span class="nbrNotFoundYetPercent"></span></span><br/>
                                        <span class="" style="color:orangered;">Liens non-trouv�s:</span><span class="nbrNotFound"><span></span> <span class="nbrNotFoundPercent"></span></span><br/>
                                        <span class="" style="color:black;">Soumissions Impossibles:</span><span class="nbrImpossible"><span></span> <span class="nbrImpossiblePercent"></span></span><br/>
                                    </div>
                                <?php } ?>

                                <br/>
                                <span class="project_costs_title" >Co�t actuel du projet:</span>
                                <span class="project_costs_total"><span class="sum"></span> euros</span>
                                <span class="what_is_this" data-type="cost_tooltip" data-required="cost_paid"> (qu'est-ce que c'est?)</span>

                                <div style="display:none;width:380px;" id="cost_paid" class="tooltip" >
                                    <div class="tooltip_header">Co�t actuel du projet</div>
                                    <div class="tooltip_body">
                                        <p>Il s'agit du co�t actuel engag� sur le projet. Cette somme a d�j� �t� d�bit�e de votre compte.
                                            Seules les soumissions r�ussies ont �t� d�bit�es (celles sur fond vert).
                                            Les soumissions "impossibles" (fond jaune) ne sont pas d�bit�es.</p>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if (1) { ?>
                                <br/>
                                <span class="project_costs_title"  >Co�t restant:</span>
                                <span class="project_costs_total"><span class="sum"></span> euros</span>
                                <span class="what_is_this"  data-type="cost_tooltip" data-required="cost_not_paid" > (qu'est-ce que c'est?)</span>
                                <br/>

                                <div style="display:none;width:380px;" id="cost_not_paid" class="tooltip" >
                                    <div class="tooltip_header">Co�t restant</div>
                                    <div class="tooltip_body">
                                        <p>Le co�t restant correspond aux soumissions qui n'ont pas encore �t� effectu�e.
                                            Cette somme n'a donc pas encore �t� d�bit�e de votre compte.
                                            Dans le tableau ci-dessous, elles sont affich�es sur un fond blanc.</p>
                                    </div>
                                </div>
                            <?php } ?>
                        </h6>
                        <div style="float:left;width:350px;margin-left:10px;">
                            <span class="litleTitle">Commentaires</span>: <br/>
                            <?PHP
                            if ($getCommande['consignes'] != "") {
                                echo html_entity_decode(nl2br(stripslashes($getCommande['consignes'])), ENT_QUOTES, "ISO-8859-1");
                            } else {
                                echo "Aucun commentaire";
                            }
                            ?>
                            <br/> <br/>
                        </div>
                        <div style="position:absolute;top:0px;right:0">
                            <?PHP
                            if (isSu()) {
                                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
                            }
                            ?>
                        </div>
                    <?PHP } ?>
                    <div class="cleared"></div>
                    <?PHP
                    $tableau = array();
                    $temoin = array();
                    $keyTab = 0;
                    $noAnnuaire = 0;
                    $erreurLienAnnuaire = "URL de l'Annuaire Introuvable";

                    $annuaireModel = new Annuaire();

                    $annuairesTous = Functions::getAnnuaireAll();
                    $ancreAnnuaireAll = Functions::getAncre($getCommande['id']);


                    $idUserPropList = 0;
                    if (isReferer()) {
                        $idUserPropList = $getCommande['proprietaire'];
                    }

                    $currentAnnuaireList = Functions::getOneAnnuaireListe($idAnnuaireParent, $idUserPropList);

                    if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                        $annuairesFromListNew = explode(";", $currentAnnuaireList['annuairesList']);
                    } else {
                        $annuairesFromListNew = array();
                    }



                    $project_id = intval($_GET['id']);
                    if ($project_id) {
                        $project_backlinks = Functions::getProjectBacklinksStatuses($project_id);
                    }

                    if (empty($project_backlinks)) {

                        $countImpossible = 0;
                        $countFound = 0;
                        $countNotFoundYet = 0;
                        $countNotFound = 0;
                    }

                    if (trim($getCommande['affectedSec'] != "")) {
                        $affectSecSuccess = false;
                        $changedOccur = 0;

                        $secondairesRef = explode(";", $getCommande['affectedSec']);
                        $secondairesRef = array_filter($secondairesRef);

                        if (in_array($getCommande['affectedTO'], $secondairesRef) === true) {
                            unset($secondairesRef[array_search($getCommande['affectedTO'], $secondairesRef)]);
                            $changedOccur = 1;
                        }

                        if (array_search(0, $secondairesRef) === true) {
                            unset($secondairesRef[array_search(0, $secondairesRef)]);
                            $changedOccur = 1;
                        }

                        if ($changedOccur == 1) {
                            $newSecondaire = implode(";", $secondairesRef);
                            $affectSecSuccess = $dbh->query('UPDATE projets SET categories = "' . $newSecondaire . '" WHERE id=' . $getCommande['id']);
                            if ($affectSecSuccess) {
                                $getCommande['affectedSec'] = $newSecondaire;
                            }
                        }
                    }



                    $raisonArray = array();
                    if ($getCommande['adminRaison'] != "" || $getCommande['adminRaison'] != ";") {
                        $raisonArray = explode(";", $getCommande['adminRaison']);
                        $raisonArray = array_filter($raisonArray);
                    }

                    $annuairesFromListNew = array_filter($annuairesFromListNew);

                    $currentAnnuaireList = "";

                    if ($idAnnuaireParent) {
                        $currentAnnuaireList = Functions::getOneAnnuaireListe($idAnnuaireParent, $idUserPropList);
                    }


                    if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                        $annuairesFromList = explode(";", $currentAnnuaireList['annuairesList']);
                    } else {
                        $annuairesFromList = array();
                    }


                    $annuairesFromListNew = $annuairesFromList;
                    $annuairesFromListNew = array_filter($annuairesFromListNew);

                    // fucking code
                    // changing project annuaires list
                    // by activite/disable annuaire
                    $aEdit = 0;

                    $annuaireModel = new Annuaire;
                    // get not allowed annuaires
                    $projectNotActiveAnnuaires = $annuaireModel->getNotAllowAnnuairesByProjectId($getCommande['id']);

                    //var_dump($projectNotActiveAnnuaires);

                    $disabled_annuaires = $annuaireModel->getDisabledAnnuiresByProjectId($getCommande['id']);
                    $new_disabled_annuaires = array();
                    foreach ($annuairesFromListNew as $annuaireThatId) {
                        if (!isset($annuairesTous[$annuaireThatId]) || (isset($annuairesTous[$annuaireThatId]) && (!Functions::checkUrlFormat($annuairesTous[$annuaireThatId]['annuaire']) || $annuairesTous[$annuaireThatId]['active'] == 0))) {
                            $aEdit++;
                            // prepare disabled annuaire
                            $disabled = array();
                            $disabled['project_id'] = $getCommande['id'];
                            $disabled['annuaire_id'] = $annuaireThatId;
                            $new_disabled_annuaires[] = $disabled;
                            unset($annuairesFromListNew[array_search($annuaireThatId, $annuairesFromListNew)]);
                        }
                    }

                    // checking annuaires status & removing disabled annuaires
                    foreach ($annuairesTous as $annuaire) {
                        if (($annuaire['active']== 1) && (in_array($annuaire['id'],$disabled_annuaires))){
                            $aEdit++; // throw update annuaires list
                            $annuaireModel->removeDisabledAnnuiare($getCommande['id'],$annuaire['id']);
                            $annuairesFromListNew[] = $annuaire['id']; // return disabled annuaire to project list
                        }
                    }

                    //var_dump($annuairesFromListNew);
                    // inserting new disabled annuaires
                    if (!empty($new_disabled_annuaires)){
                        $annuaireModel->setDisabledAnnuaires($new_disabled_annuaires);
                    }
                    //die;

                    $contentThatListImploded = "";
                    if ($aEdit > 0) {
                        $contentThatListImploded = implode(";", $annuairesFromListNew);
                        $contentThatListImploded .= ";";
                        if ($contentThatListImploded != ";") {
                            $requete = "UPDATE annuaireslist SET annuairesList = '" . $contentThatListImploded . "' WHERE id=" . intval($getCommande['annuaire']);
                            $execution = $dbh->query($requete);
                            $currentAnnuaireList['annuairesList'] = $contentThatListImploded;
                        }
                    }

                    $annuairesFromListNew = array_filter($annuairesFromListNew);
                    $annuCount = count($annuairesFromListNew);

                    if ($getCommande['adminRaison'] == "" || $getCommande['adminRaison'] == 0 || ($getCommande['adminRaison'] != "" && (count(array_diff($annuairesFromListNew, $raisonArray)) > 0 || count(array_diff($raisonArray, $annuairesFromListNew)) > 0))) {
                        shuffle($annuairesFromListNew);
                        shuffle($annuairesFromListNew);
                        shuffle($annuairesFromListNew);

                        $annuairesFromListUpdate = implode(";", $annuairesFromListNew);
                        $annuairesFromListUpdate .=";";
                        $dbh->query('UPDATE projets SET adminRaison = "' . $annuairesFromListUpdate . '" WHERE id=' . $getCommande['id']);
                        $getCommande['adminRaison'] = $annuairesFromListUpdate;
                    }

                    $annuairesFromList = explode(";", $getCommande['adminRaison']);
                    $annuairesFromList = array_filter($annuairesFromList);


                    if ($getCommande['over'] == 0) {
                        $done = Functions::getCountTaskDone($getCommande['id'],true);
                        $doneCount = count($done);

                        if ($annuCount > 0 && $doneCount > 0) {
                            $fusion = array_diff($annuairesFromListNew, $done);
                            if (count($fusion) == 0) {
                                $dbh->query('UPDATE projets SET over = "1", overTime = "' . time() . '", email = 0 WHERE id=' . $getCommande['id']);
                                $getCommande['over'] = 1;

                                // notice user
                                $mailer = new MailProvider();
                                $mailer->NoticeProjectIsFinished($getCommande['id']);
                            }
                        }
                    }


                    $countNamesAnnuaires = count($annuairesFromList);

                    // rejected tasks to top
                    $project_has_rejected_tasks = false;
                    $rejected_task_annuaires = $jobsModel->getRejectedByProjectId($getCommande['id']);

                    $rejected_cnt = $jobsModel->getCountRejectedByProjectId($getCommande['id']);

                    if (count($rejected_task_annuaires)>0){
                         $top_rejected_tasks = array();
                         $top_rejected_tasks = $rejected_task_annuaires;
                         foreach($annuairesFromList as $item){
                             //$item = intval($item);
                             if (!in_array($item,$rejected_task_annuaires)){
                                 $top_rejected_tasks[] = $item;
                             }
                         }
                        $annuairesFromList = $top_rejected_tasks;
                        $project_has_rejected_tasks = true;
                    }



                    //var_dump($project_has_rejected_tasks); die;

                    $annuaires = array();
                    $startUp = 1;
                    if ($countNamesAnnuaires > 0) {
                        foreach ($annuairesFromList as $value) {
                            if ($value != "" && $value != NULL && $value && $value != 0) {
                                $currentData = Functions::getAnnuaire($value);

                                if ($currentData && $currentData['annuaire'] != "") {
                                    $annuaires[$startUp]['id'] = $currentData['id'];
                                    $annuaires[$startUp]['annuaire'] = $currentData['annuaire'];
                                    $annuaires[$startUp]['webmasterAncre'] = $currentData['webmasterAncre'];
                                    $annuaires[$startUp]['AnnuaireDisplayAncre'] = $currentData['webmasterAncre'];
                                    $annuaires[$startUp]['page_rank'] = $currentData['page_rank'];
                                    $annuaires[$startUp]['tarifW'] = $currentData['tarifW'];
                                    $annuaires[$startUp]['tarifR'] = $currentData['tarifR'];
                                    $annuaires[$startUp]['active'] = $currentData['active'];
                                    $annuaires[$startUp]['display'] = $currentData['display'];
                                    $annuaires[$startUp]['consignes'] = $currentData['consignes'];
                                    $annuaires[$startUp]['importedBy'] = $currentData['importedBy'];
                                    $annuaires[$startUp]['created'] = $currentData['created'];
                                    $annuaires[$startUp]['link_submission'] = $currentData['link_submission'];
                                    $annuaires[$startUp]['min_words_count'] = $currentData['min_words_count'];
                                    $annuaires[$startUp]['max_words_count'] = $currentData['max_words_count'];
                                    $startUp++;
                                }
                            }
                        }
                    }

                    // START GENERATING/PREPARING TASKS FOR TABLE
                    $isJobbed = Functions::getJob($_GET['id']);

                    /* voodoo417 */
                    $sum_paid = 0.0;
                    $sum_not_paid = 0.0;


                    $point = count($isJobbed);

                    // #8
                    $array_frequence_checker = array();
                    $array_frequence_not_done_checker = array();

                    while ($keyTab < $point) {


                        // hide not allowed annuaires
                        if ( isset($isJobbed[$keyTab]['annuaireID'])){
                            // inc|dec $keyTab - don`t break logic
                            $keyTab++;
                            if (in_array($isJobbed[$keyTab]['annuaireID'],$projectNotActiveAnnuaires)){
                                continue;
                            }else{
                                $keyTab--;
                            }
                        }


                        $keyTab++;
                        if (isset($isJobbed[$keyTab]['annuaireID'])) {
                            //                            $currentAnnuaire = Functions::getAnnuaireName($isJobbed[$keyTab]['annuaireID']);
                            $currentData = Functions::getAnnuaire($isJobbed[$keyTab]['annuaireID']);

                            Functions::getAnnuaire($value);
                            $tableau[$keyTab]["id"] = $isJobbed[$keyTab]['id'];
                            $tableau[$keyTab]["siteID"] = $isJobbed[$keyTab]['siteID'];
                            $tableau[$keyTab]["affectedTO"] = $isJobbed[$keyTab]['affectedto'];
                            //                            $tableau[$keyTab]["annuaire"] = $currentAnnuaire;
                            $tableau[$keyTab]["annuaire"] = $currentData['annuaire'];
                            $tableau[$keyTab]["AnnuaireDisplayAncre"] = $currentData['webmasterAncre'];
                            $tableau[$keyTab]["tarifW"] = $currentData['tarifW'] + Functions::getRemunuerationWebmaster($getCommande['proprietaire']);


                            $tableau[$keyTab]["coutWebmaster"] = $isJobbed[$keyTab]['coutWebmaster'];

                            // @deprecated
                            //if ($isJobbed[$keyTab]['coutWebmaster'] > 0) {
                                //$tableau[$keyTab]["tarifW"] = round($isJobbed[$keyTab]['coutWebmaster'],2);
                            //}

                            $tableau[$keyTab]["tarifR"] = 0;
                            $tableau[$keyTab]["annuaireID"] = $isJobbed[$keyTab]['annuaireID'];
                            if (trim($tableau[$keyTab]["annuaire"]) == "") {
                                $tableau[$keyTab]["annuaire"] = $isJobbed[$keyTab]['annuaireURL'];
                                if (trim($tableau[$keyTab]["annuaire"]) == "") {
                                    $tableau[$keyTab]["annuaire"] = $erreurLienAnnuaire;
                                }
                            }
                            //                          $tableau[$keyTab]["annuaire"] = $isJobbed[$keyTab]['annuaireID'];
                            $tableau[$keyTab]["soumissible"] = date("d/m/Y", $isJobbed[$keyTab]['soumissibleTime']);
                            $tableau[$keyTab]["soumissibleTimestamp"] = $isJobbed[$keyTab]['soumissibleTime'];

                            //echo date("d/m/Y", $isJobbed[$keyTab]['soumissibleTime']);
                            //                          $tableau[$keyTab]["soumission"] = date("d-m-Y � H\h:m\m\i\\n:s\s", $isJobbed[$keyTab]['soumissionTime']);

                            $tableau[$keyTab]["soumission"] = date("d/m/Y", $isJobbed[$keyTab]['soumissionTime']);
                            $tableau[$keyTab]["soumission_unix"] = $isJobbed[$keyTab]['soumissionTime'];
                            $tableau[$keyTab]["statut"] = 'OK';
                            $tableau[$keyTab]["dayJour"] = '0';
                            $tableau[$keyTab]["todayJob"] = $isJobbed[$keyTab]['adminApprouved'];

                            // calcualating delays
                            //$tableau[$]

                            $check_date = $tableau[$keyTab]["soumissibleTimestamp"];
                            if (!isset($array_frequence_checker[$check_date])){
                                $array_frequence_checker[$check_date] = 1;
                            }else{
                                $array_frequence_checker[$check_date]++;
                            }

                            $temoin[] = $isJobbed[$keyTab]['annuaireID'];
                        }
                    }

                    //var_dump(date('d/m/Y',$tableau[1]['soumissibleTimestamp']));
                    //die;

                    $countTableau = count($tableau);
                    $keyAnnuaire = 1;
                    $keyTab++;
                    $countAnnuaires = count($annuaires);
                    $frequenceTime = 0;
                    $lastSoumissible = 0;
                    $firstSoumissibleTampon = 0;
                    $indic = 0;
                    $retard = 0;
                    $nbreJoursRetard = 0;
                    $affectedTO['id'] = 0;
                    $retardataires = 0;
                    $terminator = 0;
                    $pointAffaire = 1;
                    $afficher = 0;
                    $iscreated = 0;
                    $todayJob = 0;
                    $pointAffaireReferer = 0;
                    $retardTampon = 0;
                    $keyAnnuaireAuto = 0;
                    $timeRounded = strtotime(date('d-m-Y', time()));

                    $freq = Functions::getFrequence($getCommande['frequence']);


                    //var_dump()


                    //$freq[0] = $freq[0];
                    // $freq[1] = 10;
                    //var_dump($freq); die;

                    $frequenceTime = (24 * $freq[1] * 3600);
                    $frequenceTache = $freq[0];

                    if ($pointAffaireControl) {
                        $toShow = $pointAffaireControl;
                    }
                    //                    echo $toShow;

                    $indicateur = 1;
                    $addOne = 0;


                    //var_dump(date('d/m/Y',$tableau[$countTableau]['soumissibleTimestamp']));
                    //die;

                    if ($countTableau == 0 || !isset($tableau[$countTableau]['soumissibleTimestamp'])) {
                        $tableau[$countTableau]['soumissibleTimestamp'] = $getCommande['adminApprouveTime'];
                        $tableau[$countTableau]['affectedTO'] = $getCommande['affectedTO'];
                        $tableau[$countTableau]["todayJob"] = 0;
                        $iscreated = 1;
                    }


                    // set time of last submitting task ( TABLE JOBS)
                    $lastSoumissible = $tableau[$countTableau]['soumissibleTimestamp'];
                    $lastaffectedTO = $tableau[$countTableau]['affectedTO'];



                    //var_dump(date('d/m/Y',$lastSoumissible)); die;
                    // TEST
                    //$lastSoumissible = 1479041268;



                    //var_dump($countTableau);die;

                    if ($timeControl > 0 && $lastSoumissible < $timeControl) {
                    //                        $lastSoumissible = $timeControl;
                    //                        $iscreated = 1;
                    }



                    // check if project was reaffecteds
                    $reffected_time = 0;
                    $is_reaffected = $getCommande['is_reaffected'];
                    $reaffecting_continue = $getCommande['reaffecting_continue'];

                    if ($is_reaffected){
                        $reaffected_time = $getCommande['reaffected_time'];

                        //var_dump('REAFFECTED<br/>');
                        //var_dump(' Reaffected_time = '.date('d/m/Y H:i:s',$reaffected_time).'<br/>');
                    }

                    // set start date based on creating/affecting to  project`s

                    if (($lastSoumissible < $getCommande['affectedTime'])) {
                        $lastSoumissible = $getCommande['affectedTime'];
                        $iscreated = 1;
                    }



                    if ($lastSoumissible < $getCommande['adminApprouveTime']) {
                        $lastSoumissible = $getCommande['adminApprouveTime'];
                        $iscreated = 1;
                    }


                   // }


                    // if project was stopped before and then restarted
                    // start tasks from restarting-time
                    //$getCommande['from_pause'] = 0;
                    if ( ($countTableau > 0) && ($getCommande['from_pause'])){
                         $lastSoumissible = $getCommande['affectedTime'];
                        // disable overriding
                        $getCommande['from_pause'] = 0;
                    }



                    // check if writer submit taks not in order by min_date
                    // if yes - override last submission time to start-project time
                    if ( ($countTableau > 0 && (!$getCommande['from_pause'])) ){
                        $checking_not_order = false;
                        ksort($array_frequence_checker);

                        foreach($array_frequence_checker as $task_time=>$done_cnt){
                            if ( ($task_time < $lastSoumissible)
                                 && ($done_cnt < $frequenceTache)
                                 && ($task_time > $getCommande['adminApprouveTime']) ){
                                $lastSoumissible = $task_time;
                                $checking_not_order = true;
                                break;
                            }
                        }



                       // POSSIBLE BUG
                       // if makes not in order and nothing before
                       if (!$checking_not_order){
                           if ($lastSoumissible > $getCommande['adminApprouveTime']){
                               $lastSoumissible = $getCommande['adminApprouveTime'];
                           }
                        }

                        /*
                        echo 'Freq = '.$frequenceTache.'<br/>';
                        echo date('d/m/Y H:i:s', $lastSoumissible);
                        echo '<pre>';
                        var_dump($array_frequence_checker);
                        echo '</pre>';*/
                    }


                    // ???
                    // TODO add explanation
                    if (!$webmasterCan) {
                        $lastSoumissible = $timeRounded;
                        $iscreated = 1;
                    }

                    $lastSoumissibleTemoin = $lastSoumissible;
                    $frequenceTacheInit = $frequenceTache;

                    $pointNew = 0;
                    $nextTime = 0;
                    $soumissibleSave = 0;
                    $currentTimeD = Functions::timeToSet(time());

                    // TEST change day to future
                    //  1479304068

                    //$currentTimeD = 1479390468;
                    //var_dump(date('d/m/Y H:i:s',$currentTimeD));
                    //var_dump(date('d/m/Y H:i:s',$lastSoumissible));

                    //die;

                    //(date('Y-m-d H:i:s',$currentTimeD));
                    $currentTime = time();

                    $TIME_SHIFT = 1*24*3600;
                    $TIME_SHIFT = 0;

                    /*
                    if ($_GET['id'] == 6242){
                        $TIME_SHIFT = 5*24*3600;
                    }*/

                    $currentTimeD += $TIME_SHIFT;
                    $currentTime  += $TIME_SHIFT;

                    //var_dump(date('Y-m-d H:i:s',$currentTime));
                    $decompteTache = 0;
                    $tacheFaite = count($tableau);
                    //                    print_r($temoin);

                    $_SESSION['test'] = 1;
                    unset($_SESSION['test']);
                    if (isset($_SESSION['test'])) {
                        echo '<br/> <h3> CURRENT FRENCH TIME  = ' . date('d/m/Y H:i:s', $currentTime) . '</h3><br/>';
                        if (isset($_SESSION['test_details_reaffecting'])) {
                            echo '<br/> <h3> REAFFECTING COUNT = ' . $_SESSION['test_details_reaffecting'] . '</h3><br/>';
                        }

                    }

                    $checker_step_finishing = false;

                    $project_delay_step = $getCommande['project_delay_step'];
                    $writer_delay_step = $getCommande['writer_delay_step'];

                    //var_dump('<br/>DELAYS '.' LATE PROJECT = '.$project_delay_step.' LATE WRITER = '.$writer_delay_step.'<br/>');

                    // TEST
                    $test_time = time() + $TIME_SHIFT;
                    $checker_step_finishing = false;



                    // $last_annuaire = end($annuaires);

                    //var_dump($rejected_task_annuaires);
                    //var_dump($iscreated);

                    $first_task_date = 0;
                    $hidden_writer_today_tasks = array();
                    $webmaster_default_task_cost = Functions::getRemunuerationWebmaster($getCommande['proprietaire']);

                    $checked_first_days_delay = false;
                    $first_days_delay = 0;

                    if ($getCommande['over'] == 0 || ($getCommande['over'] == 1 && isset($_GET['from']) && $_GET['from'] == "over")) {
                        if ($countAnnuaires > 0) {
                            while ($keyAnnuaire <= $countAnnuaires) {

                                $rejected_task = false;
                                if (isset($annuaires[$keyAnnuaire]['id']) && $annuaires[$keyAnnuaire]['active'] == 1 && !in_array($annuaires[$keyAnnuaire]['id'], $temoin) && $annuaires[$keyAnnuaire]['id'] != "") {
                                    $terminator = 1;


                                    $bonusAdded = 0;
                                    $bonusAdded_Web = 0;

                                    /*
                                    * Check if owner has enough money for task
                                    */

                                    // calculations task`s cost
                                    /*$bonusAdded = 0;
                                    $bonusAdded_Web = 0;

                                    if ($getSupps[$projet_id] && in_array($tableau[$limit1]["annuaireID"], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                        $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                                        $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                                        if ($bonusAdded_Web == 0) {
                                            $bonusAdded_Web = $ComptePersoWebmaster;
                                        }
                                    }
                                    //$owner_solde = 3.0;
                                    $owner_task_cost =  ($tableau[$limit1]["tarifW"] + Functions::getRemunuerationWebmaster($tableau[$limit1]["proprietaire"]) + $bonusAdded_Web);
                                    */

                                    /* */
                                    // hide not allowed annuaires
                                    $annuaire_id = $annuaires[$keyAnnuaire]['id'];

                                    if (in_array($annuaire_id, $projectNotActiveAnnuaires)) {
                                        //var_dump($annuaire_id . ' excluded');
                                        $keyAnnuaire++;
                                        //$indic++;
                                        continue;
                                    }

                                    // check if rejected task
                                    // changing logice
                                    if (in_array($annuaires[$keyAnnuaire]['id'], $rejected_task_annuaires)) {
                                        $rejected_task = true;
                                        //var_dump('Rejected Task = '.$annuaires[$keyAnnuaire]['id']);
                                    }


                                    $owner_task_cost = 0.0;
                                    //if (isReferer() || isSu() || ) {
                                    $bonusAdded = 0;
                                    $bonusAdded_Web = 0;

                                    if ($getSupps[$projet_id] && in_array($annuaires[$keyAnnuaire]['id'], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                        $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                                        $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                                        if ($bonusAdded_Web == 0) {
                                            $bonusAdded_Web = $ComptePersoWebmaster;
                                        }
                                    }
                                    $owner_task_cost = $annuaires[$keyAnnuaire]['tarifW'] + Functions::getRemunuerationWebmaster($annuaires[$keyAnnuaire]["proprietaire"]) + $bonusAdded_Web;

                                    // add extra cost for task
                                    $extra_webmaster_cost = 0.0;
                                    if ($LIST_ANNUAIRE_WORDS_COUNT > 0) {
                                        $extra_webmaster_cost = $annuaireModel->calculateExtraPrice($annuaires[$keyAnnuaire], $LIST_ANNUAIRE_WORDS_COUNT);
                                        if ($extra_webmaster_cost > 0) {
                                            $owner_task_cost += $extra_webmaster_cost;
                                        }
                                    }

                                    // check if possible for standard task count
                                    // save it for writer-checking
                                    if ($webmasterCan) {
                                        $hidden_writer_today_tasks[$annuaires[$keyAnnuaire]['id']] = $owner_task_cost;
                                    }

                                    if (($owner_task_cost > $owner_solde) && isReferer()) {

                                        if ($rejected_task) {
                                            $rejected_key = array_search($annuaires[$keyAnnuaire]['id'], $rejected_task_annuaires);
                                            //var_dump($rejected_key);
                                            unset($rejected_task_annuaires[$rejected_key]);
                                        }

                                        $keyAnnuaire++;
                                        //echo 'Hide '.$annuaires[$keyAnnuaire]['id'].' '.$owner_task_cost.' '.$annuaires[$keyAnnuaire]['annuaire'].'<br/>';
                                        continue;
                                    }

                                    //}


                                    //if ($array_frequence_checker[$lastSoumissible]<=$frequenceTacheInit)
                                    // echo 'BEFORE changing DATE: '.date("d/m/Y", $lastSoumissible).' count = '.$array_frequence_checker[$lastSoumissible].' <br/>';

                                    /* BUG "3 days later" FIXED */
                                    if (!$rejected_task) {

                                        // "shift"-increasing current date
                                        if (!isset($array_frequence_checker[$lastSoumissible])) {
                                            $array_frequence_checker[$lastSoumissible] = 1;
                                        } else {
                                            //$shift_future = $array_frequence_checker[$lastSoumissible];
                                            //$shift_future++;
                                            //if ()
                                            //if ($array_frequence_checker[$lastSoumissible] < $frequenceTacheInit)
                                            $array_frequence_checker[$lastSoumissible]++;
                                        }

                                        // skip date for done tasks if done cnt = frequence per day
                                        if (isset($array_frequence_checker[$lastSoumissible]) && ($array_frequence_checker[$lastSoumissible] > $frequenceTacheInit)) {
                                            $checker_step_finishing = false;
                                            $array_frequence_checker[$lastSoumissible]--;
                                            $lastSoumissible = $lastSoumissible + $frequenceTime;
                                            continue;
                                        }

                                        // calculating current date/range for task
                                        if (($iscreated == 1) || ($keyAnnuaire == 1)) { // "NULL-project" or first task
                                            $lastSoumissible = $lastSoumissible;
                                            $iscreated = 2;
                                            $first_task_date = $lastSoumissible;
                                        }
                                    }

                                    //echo 'After changing DATE: '.date("d/m/Y", $lastSoumissible).' count = '.$array_frequence_checker[$lastSoumissible].' <br/>';


                                    if ($tacheFaite > 0 && ($indic == 0 || $decompteTache > 0)) {
                                        //                                        $tableau[$keyTab]["soumissibleTimestamp"]
                                    }

                                    // affected not paid
                                    $tableau[$keyTab]["id"] = $annuaires[$keyAnnuaire]['id'];
                                    $tableau[$keyTab]["siteID"] = $getCommande['id'];
                                    $tableau[$keyTab]["affectedTO"] = $getCommande['affectedTO'];
                                    $tableau[$keyTab]["annuaire"] = $annuaires[$keyAnnuaire]['annuaire'];
                                    $tableau[$keyTab]["annuaireDisplay"] = $annuaires[$keyAnnuaire]['display'];
                                    $tableau[$keyTab]["consignesAnnuaire"] = $annuaires[$keyAnnuaire]['consignes'];
                                    $tableau[$keyTab]["annuaireID"] = $annuaires[$keyAnnuaire]['id'];
                                    $tableau[$keyTab]["AnnuaireDisplayAncre"] = $annuaires[$keyAnnuaire]['webmasterAncre'];
                                    $tableau[$keyTab]["DejaPaid"] = 0;
                                    $tableau[$keyTab]["tarifW"] = $annuaires[$keyAnnuaire]['tarifW'] + $remuneration;
                                    $tableau[$keyTab]["tarifR"] = $annuaires[$keyAnnuaire]['tarifR'];

                                    $tableau[$keyTab]["soumissible"] = date("d/m/Y", $lastSoumissible);
                                    $tableau[$keyTab]["soumissibleTimestamp"] = $lastSoumissible;


                                    if ($rejected_task) {
                                        $tableau[$keyTab]["soumissible"] = date("d/m/Y", time());
                                        $tableau[$keyTab]["soumissibleTimestamp"] = time();
                                    }


                                    /*
                                    echo '<br/>'.$tableau[$keyTab]["annuaire"].'<br/>';
                                    echo $tableau[$keyTab]["soumissibleTimestamp"].'<br/>';
                                    echo $tableau[$keyTab]["soumissible"].'<br/>';
                                    echo date("d/m/Y", $tableau[$keyTab]["soumissibleTimestamp"]) . '<br/>';
                                    echo $lastSoumissible.'<br/><br/>'; */


                                    $tableau[$keyTab]["link_submission"] = $annuaires[$keyAnnuaire]['link_submission'];
                                    // annuaire min/max word counts
                                    $tableau[$keyTab]["min_words_count"] = $annuaires[$keyAnnuaire]['min_words_count'];
                                    $tableau[$keyTab]["max_words_count"] = $annuaires[$keyAnnuaire]['max_words_count'];

                                    $tableau[$keyTab]["soumission"] = "Indisponible";
                                    $tableau[$keyTab]["statut"] = 'NOK';

                                    // refactoring
                                    // "old behaviour"
                                    if ($LIST_ANNUAIRE_WORDS_COUNT == 0) {
                                        $sum_not_paid += $tableau[$keyTab]['tarifW'];
                                        if (in_array($tableau[$keyTab]["annuaireID"], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                            $sum_not_paid += $getSupps[$projet_id]['costWebmaster'];
                                            if ($getSupps[$projet_id]['costWebmaster'] == 0) {
                                                $sum_not_paid += $ComptePersoWebmaster;
                                            }
                                        }
                                    }else {
                                        // calculate with "extra annuaires cost"
                                        $sum_not_paid += $owner_task_cost;
                                    }


                                    if ($project_has_rejected_tasks){
                                        if (in_array($tableau[$keyTab]["id"],$rejected_task_annuaires)){
                                            $tableau[$keyTab]["todayJob"] = 1;
                                            $toShow++;
                                        }
                                    }

                                   //if ($lastSoumissible <= time() || ($lastSoumissible > time() && date("d/m/Y", $lastSoumissible) == date("d/m/Y", time()))) {
                                   if ( $rejected_task || ($lastSoumissible <= $test_time) || ($lastSoumissible > $test_time && date("d/m/Y", $lastSoumissible) == date("d/m/Y", $test_time))) {
                                        $tableau[$keyTab]["todayJob"] = 1;
                                        $toShow++;
                                   }

                                    if (!$webmasterCan) {
                                        $tableau[$keyTab]["todayJob"] = 0;
                                        $tableau[$keyTab]["soumissible"] = "Indisponible";
                                    }


                                    if ($keyAnnuaireAuto == 0) {
                                        $keyAnnuaireAuto = 1;
                                        $nextTime = $lastSoumissible;
                                    }

                                    if ($tableau[$keyTab]["todayJob"] == 1) {
                                        $pointAffaire++;
                                        $todayJob++;
                                    }


                                    $tableau[$keyTab]['owner_task_cost'] = $owner_task_cost;

                                    // Calcilating  delays-periods
                                    // Override

                                    if ($tableau[$keyTab]["todayJob"] == 1 && !isWebmaster()) {

                                        // OLD BEHAVIOUR
                                        // calculating delay-days_count
                                        $infoRetard = Functions::getRetard($currentTime, $tableau[$keyTab]["soumissibleTimestamp"], $dureeJournee);
                                        $retardataireDta = $infoRetard[0];
                                        $retardataires = $infoRetard[1];
                                        $nbreJoursRetards = $retardataires;


                                        //var_dump($infoRetard,'<br/>');

                                        if ($nbreJoursRetards > 0){
                                            //var_dump($nbreJoursRetards);
                                        }

                                        // calculating $writer_delay_step
                                        $basic_delay = $retardataires;
                                        //var_dump($infoRetard);
                                        //echo 'Basic Delay = '.$basic_delay.'<br/>';
                                        //var_dump($nbreJoursRetards).'<br/>';



                                        // override "default" calculation retard - days count
                                        // by current writer_step
                                        if ($is_reaffected){

                                            $reaffRetard = Functions::getRetard($currentTime,$reaffected_time, $dureeJournee);
                                            //var_dump($infoRetard,'<br/>');
                                            //var_dump($reaffRetard,'<br/>');
                                            $reaffected_delay = $reaffRetard[1];
                                            //var_dump($reaffected_delay);
                                            //echo 'Reaffected Delay = '.$reaffected_delay.'<br/>';
                                            $writer_delay_step = $reaffected_delay;

                                            //var_dump($writer_delay_step);

                                            if ($writer_delay_step > 0){ // action-continue reaffecting
                                                $diff_delay = $basic_delay - $writer_delay_step;

                                                $out = '<br/>'.$tableau[$keyTab]["soumissible"].' ---  LW = '.$writer_delay_step.'  '.$current_delay.' '.$diff_delay.'<br/>';
                                                //echo $out;

                                                if ( ($basic_delay > $writer_delay_step) ){ // reaffecting day there!
                                                    $nbreJoursRetards = $writer_delay_step;
                                                }else{
                                                    $nbreJoursRetards = $basic_delay;
                                                }

                                            }else{  // after writer`s reseting
                                                 $nbreJoursRetards = 0;
                                            }
                                        }

                                        //var_dump($nbreJoursRetards);

                                        if ($retardTampon == 0) {
                                            $retardTampon = $nbreJoursRetards;
                                        }

                                        if (!isWebmaster()) {
                                            if ($nbreJoursRetards > 0) {
                                                $tableau[$keyTab]["soumission"] = "<span style='color:red'>" . $nbreJoursRetards . " jour(s) de retard.</span>";
                                            } else {

                                                $tableau[$keyTab]["soumission"] = "<span style='color:green'>T�che du jour</span>";
                                            }
                                        }

                                        $tableau[$keyTab]["dayJour"] = $nbreJoursRetards;

                                        // save first delay days value
                                        if (!$checked_first_days_delay){
                                            $checked_first_days_delay = true;
                                            $first_days_delay = $nbreJoursRetards;
                                        }

                                    }

                                    //var_dump($todayJob);

                                    if ($lastSoumissible < time()){
                                       //echo date("d/m/Y", $lastSoumissible).'<br/>';
                                    }
                                    $keyTab++;
                                }

                                $keyAnnuaire++;
                                $indic++;
                            }

                            //die;

                            // REAFFECTING USER
                            // if retard-delay is more
                            if ($getCommande['over'] == 0) {

                                if ($getCommande['over'] == 0) {

                                    if ($retardTampon > 0) {
                                        $retard = 1;

                                        // changing project`s writer
                                        // if delay-days limit is exceed

                                        // TEST
                                        //$getCommande['is_fixed_writer'] = true;
                                         if ( ($retardTampon >= $joursRetards) && (!$getCommande['is_fixed_writer']) ) {

                                            if ($varNew == "") {
                                                $varNew = $nextTime . "|" . $frequenceTache;
                                            }


                                            //var_dump($nextTime,$frequenceTache);
                                            $affectedTOnew = Functions::getRandomUser($getCommande['affectedTO'], 3, $getCommande['affectedSec']);

                                            //$affectedTO = 593;
                                            //var_dump('Reaffecting'); die;
                                            //die;

                                            $dbh->query('UPDATE projets
                                                         SET email = "' . $varNew . '",
                                                             affectedTO = "' . $affectedTOnew . '",
                                                             affectedBY = "1",
                                                             affectedTime = "' . $currentTimeD . '" ,
                                                             is_reaffected = 1,
                                                             reaffected_time = "' . $currentTimeD . '" ,
                                                             writer_delay_step = 0,
                                                             from_pause = 0
                                                         WHERE id=' . $getCommande['id']);

                                            if (isReferer()) {
                                                 header('location:./accueil.html');
                                            }

                                        }

                                        if (!$webmasterCan && ($currentTime - $getCommande['affectedTime'] > ($dureeJournee))) {
                                            $webAffected = $dbh->query('UPDATE projets SET affectedTime = "' . $currentTimeD . '" WHERE id=' . $getCommande['id']);
                                            $getCommande['affectedTime'] = $currentTime;
                                        }
                                    }
                                }

                                //die;



                                $varJobs = "";
                                if ($todayJob > 0) {
                                    $timePut = $nextTime;
                                    $jobPut = $todayJob;
                                }

                                if ($todayJob == 0) {
                                    $timePut = $nextTime;
                                    $jobPut = $frequenceTache;
                                }

                                // check if possible old "standard" possability
                                // but not possible with extra cost ( min-max annuaire )
                                if (!empty($hidden_writer_today_tasks) && $webmasterCan){
                                    $count_possibility = 0;

                                    foreach($hidden_writer_today_tasks as $annuaire_id=>$cost){
                                        if ($owner_solde > $cost)
                                            $count_possibility++;
                                    }

                                    // update project "status `On pause`" - not enough money
                                    if ($count_possibility == 0){
                                        $webAffected = $dbh->query('UPDATE projets SET `not_enough_money` = 1 WHERE id=' . $getCommande['id']);
                                        $webmasterCan = false;
                                    }else{
                                        $webAffected = $dbh->query('UPDATE projets SET `not_enough_money` = 0 WHERE id=' . $getCommande['id']);
                                    }
                                }

                                //var_dump('TOTAL = '.$jobPut.'<br/>');
                                if (!$webmasterCan) {
                                    if ($timePut < $timeRounded) {
                                        $timePut = $timeRounded;
                                    }
                                } else {
                                    $raportQ = Functions::webmasterPossibilities($getCommande['proprietaire']);

                                    //var_dump('MAX possible ='.$raportQ.'<br/>');
                                    if ($raportQ <= $pointAffaire) {
                                        $jobPut = $raportQ;
                                    }
                                }
                                //                                echo $jobPut;

                                $varJobs = $timePut . "|" . $jobPut;

                                $setter = false;
                                //                                echo $varJobs;
                                $setter = $dbh->query('UPDATE projets SET email = "' . $varJobs . '",
                                                                          writer_delay_step = '.$first_days_delay.'
                                                                      WHERE id=' . $getCommande['id']);

                                if ($setter) {
                                    $getCommande['email'] = $varJobs;
                                }
                            }
                        } else {
                            $nnu = Functions::getAnnuaireNameById($getCommande['annuaire']);
                            echo "<h4>
                        Le repertoire d'annuaires associ� � ce projet est vide. <br/>Pensez � ajouter des annuaires � la liste : " . $nnu['libelle'] . "
                        </h4>";
                            $noAnnuaire = 1;
                        }
                    } else {
                        if (!isset($_GET['from'])) {
                            header('location:./details.html?from=over&id=' . $getCommande['id']);
                        }
                    }

                    if ($getCommande['over'] == 1) {
                        echo "<h4 style='color:red;'>Ce projet est � pr�sent termin�. </h4>";
                    }

                    //var_dump($array_frequence_checker);
                    // BUG ? TimePut

                    $current_midnight = strtotime('today midnight');
                    $timePut = $current_midnight;

                    if (isReferer() && $timePut > time() && $addOne == 0) {
                       $pointAffaire = 0;
                    }

                    if (isReferer() && !isSuperReferer()) {

                        if (!$BONUS_WRITER_ACCESS) {
                        //                        echo $pointAffaire;
                            if ($getCommande['affectedTO'] != $idUser) {
                                $_SESSION['alertLogincontent'] = "Ce projet n'est pas/plus � vous .Attention, Ceci est un avertissement.<br/>";
                                header('location:./accueil.html?statut=error');
                            }


                            if ($getCommande['affectedTO'] == $idUser) {
                                $_SESSION['alertLogincontent'] = "Plus aucune soumission en attente pour ce projet.<br/>";
                            //                            header('location:./accueil.html?statut=success');
                            }

                        }
                    }

                    if (isWebmaster()) {
                        if ($getCommande['proprietaire'] != $idUser) {
                            $_SESSION['alertLogincontent'] = "Ce projet ne vous appartient pas. Attention, ceci est un avertissement.<br/>";
                            header('location:./accueil.html?statut=error');
                        }
                    }


                    $count = count($tableau);
                    if (isReferer()) {
                        $pagination = 200;
                    }
                    include("pages.php");



                    if ($countAnnuaires > 0 && $getCommande['over'] == 0) {
                        if ($getCommande['adminApprouveTime'] <= time()) {
                            if ($webmasterCan) {
                                if (isReferer($idTypeUser)) {
                                    if ($specialAssignement == 0) {
                                        if ($retardTampon > 0) {
                                            ?>
                                            <p style="color:red;">
                                                Vous avez <?PHP echo $retardTampon; ?> jour(s) de retard pour une soumission. ( Maximum : <?PHP echo $joursRetards; ?> jour(s) )
                                            </p>
                                            <?PHP
                                        } else {
                                            if ($pointAffaire == 0) {
                                                ?>
                                                <p style="color:green;">
                                                    Plus aucune soumission sur ce projet. Vous serez inform� si c'est le cas. Merci
                                                </p>
                                                <?PHP
                                            } else {
                                                ?>
                                                <p style="color:green;">
                                                    Le retard maximum pour les soumissions est de <?PHP echo $joursRetards; ?> jour(s). Veuillez toujours faire les soumissions � temps.
                                                </p>
                                                <?PHP
                                            }
                                        }
                                    }
                                }
                            } else {
                                ?>
                                <p style="color:orangered;">
                                    Le projet est actuellement mis en pause.Veuillez r�essayer ult�rieurement.
                                </p>
                                <?PHP
                                if (isReferer()) {
                                    ?>
                                    <p style="color:red;">
                                        Aucune p�nalit� de retard ne sera appliqu�e � ce projet jusqu'� la r�solution du probl�me.
                                    </p>
                                    <?PHP
                                }
                                if (isWebmaster()) {
                                    ?>
                                    <p style="color:red;">
                                        Le solde de votre compte est insuffisant pour effectuer les soumissions restantes. Veuillez recharger votre compte.
                                    </p>
                                    <?PHP
                                }
                                if (isAdmin() || isSu()) {
                                    ?>
                                    <p style="color:red;">
                                        Le solde de ce webmaster est insuffisant pour effectuer les soumissions restantes. Veuillez l'aviser.
                                    </p>
                                    <?PHP
                                }
                            }
                        } else {
                            ?>
                            <p style="color:red;">
                                Ce projet n'a pas encore d�marrer ou � �t� repousser dans le temps.
                            </p>
                            <?PHP
                        }
                    }

                    if ($noAnnuaire == 0 && $count > 0 && $countAnnuaires > 0 && (isAdmin() || isWebmaster() || isSu() || (isReferer() && $webmasterCan && $pointAffaire > 0))) {
                        if (isSu() || isAdmin() || isWebmaster()) {
                            if (isSu() || isAdmin()) {
                                ?>
                                <a class="button color small round" href="./compte/modifier/referenceur.html?id=<?PHP echo $getCommande['id']; ?>" style="color:white;float:right;margin:0px 0px 5px 10px;"><i class="icon-user" style="color:white;" ></i> R�affacter le projet �</a>
                                <?PHP
                            }
                            //                            if (isWebmaster()) {
                            ?>
                            <a class="button color small round" href="./compte/modifier/description.html?id=<?PHP echo $getCommande['id']; ?>" style="color:white;float:right;margin:0px 0px 5px 10px;"><i class="icon-info" style="color:white;" ></i> R�cup�rer Descriptions</a>

                            <?PHP
                            //                            }
                            if ($getCommande['over'] == 0) {
                                ?>
                                <a class="button color small round" href="./compte/modification/stopperprojet.html?id=<?PHP echo $getCommande['id']; ?>" style="color:white;float:right;margin:0px 0px 5px 10px;"><i class="icon-remove-sign" style="color:white;" ></i> Arr�ter le projet</a>
                                <a class="button color small round" href="./compte/accueil.html" style="color:white;float:right;margin:0px 0px 5px 5px;"><i class="icon-list" style="color:white;" ></i> Retour � Liste des projets</a>
                                <?PHP
                            }
                            if ($getCommande['over'] == 1) {
                                ?>
                                <a class="button color small round" href="./compte/modification/reprendreprojet.html?id=<?PHP echo $getCommande['id']; ?>" style="color:white;float:right;margin:0px 0px 5px 10px;"><i class="icon-refresh" style="color:white;" ></i> Reprendre le projet</a>
                                <a class="button color small round" href="./compte/accueil.html?section=over" style="color:white;float:right;margin:0px 0px 5px 5px;"><i class="icon-list" style="color:white;" ></i> Retour � Liste des projets</a>

                                <?PHP
                            }

                            if ($getCommande['over'] >= 0) {
                                ?>
                                <a class="button color small round" href="./compte/post/csv.html" style="color:white;float:right;margin:0px 0px 5px 10px;"><i class="icon-download" style="color:white;" ></i> T�l�charger le rapport</a>

                                <?PHP
                            }
                        }
                        if (isWebmaster() || isSu()) {

                            echo '<p style="font-weight:bold;color:red;clear:both;">';
                            echo '<br/>';
                            echo 'ATTENTION! M�me si l\'annuaire ne met que quelques jours � valider votre site, notre robot peut mettre plusieurs mois ';
                            echo '� detecter le lien sur certains annuaires! Il est donc normal d\'avoir de nombreux liens en attente. Toutes les soumissions seront statu�es en lien trouv� ou en refus.';
                            echo '</p><br/>';
                            ?>

                            <style>
                                span.selectedListing{
                                    background:green !important;
                                }
                            </style>
                            <span class="" style="float:left;margin:5px 0px 5px 0px;font-size:13px;font-weight:bold;">Soumissions: </span> 
                            <span class="button color small round displaySoumissions" data-action="succeedLine" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-check" style="color:white;" ></i> Trouv�es</span>
                            <span class="button color small round displaySoumissions" data-action="inProcess" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-search" style="color:white;" ></i> Pas encore trouv�es</span>
                            <span class="button color small round displaySoumissions" data-action="noSucceedLine" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-remove-sign" style="color:white;" ></i> Refus�es</span>
                            <!--<span class="button color small round displaySoumissions" data-action="successLine" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-check" style="color:white;" ></i> Envoy�es</span>-->
                            <span class="button color small round displaySoumissions" data-action="failedLine" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-remove" style="color:white;" ></i> Impossibles</span>
                            <span class="button color small round displaySoumissions" data-action="Waiting" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-pause" style="color:white;" ></i> Restantes </span>
                            <!--<span class="button color small round displaySoumissions" data-action="Current" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-play" style="color:white;" ></i> A faire</span>-->
                            <span class="button color small round displaySoumissions selectedListing" data-action="CorrectLine" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-play" style="color:white;" ></i> R�capitulatif</span>
                            <span class="button color small round displaySoumissions" data-action="all" style="color:white;float:left;margin:0px 0px 5px 10px;"><i class="icon-list" style="color:white;" ></i> Toutes</span>

                            <?PHP
                        }
                        ?>

                        <!-- voodoo417 -->
                        <div style="display:none;" id="tooltip_status" class="tooltip" >
                            <div class="tooltip_header">Il existe 3 diff�rents statuts :</div>
                            <div class="tooltip_body">
                                <p><b>Lien trouv� </b>: Votre site a �t� valid� et le lien a �t� trouv� sur la page indiqu�e.</p>
                                <p><b>Recherche du Lien en cours </b>: Notre robot n'a pas encore trouv� votre lien. Ce dernier
                                    peut cependant mettre jusqu'� 1 mois pour d�tecter la validation de votre site.</p>
                                <p><b>Lien non trouv� </b>: Soit votre site est refus�, soit votre site est
                                    toujours en attente de validation 7 mois plus tard.</p>
                            </div>
                        </div>


                        <table class="simple-table" id="detailsTable">
                            <tbody>
                                <tr class="principalTR">
                                    <th>
                                        Annuaire
                                    </th>
                                    <th>
                                        A faire le :
                                    </th>
                                    <th>
                                        Soumis le :
                                    </th>

                                    <th>
                                        Commentaires
                                    </th>

                                    <th>
                                        Statut <span id="statut_what_is" class="what_is_this">(Qu'est ce que c'est?)</span>
                                    </th>

                                </tr>
                                <?PHP
                                $countage = 1;
                                $longMot = 50;
                                //                                print_r($tableau);
                                //                                echo $limit2;
                                $array_to_csv = array();


                                // project has rejected tasks
                                // sort by 1) done-state 2) by today

                                if (isReferer()){
                                    //var_dump($rejected_task_annuaires);
                                    if (count($rejected_task_annuaires) == 0){
                                       $project_has_rejected_tasks = false;
                                    }
                                }

                                if ($project_has_rejected_tasks){

                                    usort($tableau,function($a,$b){
                                        //$date_compare = ($a['soumissibleTimestamp']==$b['soumissibleTimestamp']);
                                        //if ($date_compare === false){
                                        $status_compare = ($a['todayJob']==$b['todayJob']);

                                        if ($status_compare){
                                            $op1_date = date('d/m/Y',$a['soumissibleTimestamp']);
                                            $op2_date = date('d/m/Y',$b['soumissibleTimestamp']);
                                            if ($a['todayJob'] == 1){
                                                if ($op1_date == $opt2_date)
                                                   return 0;
                                                return ($a['soumissibleTimestamp'] > $b['soumissibleTimestamp'])? -1: 1;
                                            }
                                            //if ( $op1_date == $opt2_date )
                                                //return 0;
                                            return ($a['soumissibleTimestamp'] > $b['soumissibleTimestamp'])? 1: -1;
                                        }
                                        //if ($a['todayJob']==$b['todayJob'])
                                        //return 0;
                                        //return $a['todayJob'] < $b['todayJob']?1:-1;

                                        //}
                                        return ($a['todayJob'] < $b['todayJob'])?1:-1;
                                    });
                                }

                                // define writer`s price per 100 words

                                // decrement fucking limits because keys are not saved
                                if ($project_has_rejected_tasks) {
                                    $limit1--;
                                    $limit2--;
                                }


                               // die;
                                while ($limit1 <= $limit2) {

                                    /*
                                    if ( ($tableau[$limit1]["todayJob"]==0) || ($tableau[$limit1]["todayJob"]==0)){
                                        $limit1++;
                                        continue;
                                    }*/


                                    $lienExterne = $tableau[$limit1]['annuaire'];
                                    $classeSPe = "";


                                    if (!preg_match("#http://#", $tableau[$limit1]['annuaire'])) {
                                        $lienExterne = "http://" . $tableau[$limit1]['annuaire'];
                                    }

                                    $urlTrouvee = '';
                                    $Reponse = "";
                                    $ReponseColor = "";
                                    $CommentairesAssocies = "Aucune description";
                                    if (!empty($ComCom[3]['commentaire'])) {
                                        $CommentairesAssocies = "";
                                        $CommentairesAssocies = $ComCom[3]['commentaire'];
                                    }


                                    $failedLine = false;
                                    if (isAdmin() || isSu() || isWebmaster() || isSuperReferer()) {
                                        $defaultLabel = "description";
                                        $classeSPe2 = "";
                                        $classeSPe3 = "";
                                        $annuaire_id = $tableau[$limit1]['annuaireID'];

                                        if ($tableau[$limit1]["todayJob"] == 3) {
                                            $classeSPe = "failedLine";
                                            $defaultLabel = "raison";
                                            $failedLine = true;
                                            $countImpossible++;
                                            //$sum_paid += $tableau[$limit1]['tarifW'];
                                        }

                                        if ($tableau[$limit1]["todayJob"] == 2) {
                                            $classeSPe = "successLine";

                                            $sum_paid += $tableau[$limit1]['coutWebmaster'];
                                            // @deprecated
                                            //$sum_paid += $tableau[$limit1]['tarifW'];



                                            $Reponse = "";
                                        }
                                        if ($tableau[$limit1]["todayJob"] == 1) {
                                            $classeSPe = "Current";
                                        }
                                        if ($tableau[$limit1]["todayJob"] == 0) {
                                            //var_dump('id = '.$tableau[$limit1]['id']);
                                            $classeSPe = "Waiting";
                                        }
                                        if ($project_backlinks[$annuaire_id]['status'] == 'found') {
                                            $Reponse .= "Lien Accept� ";
                                            $ReponseColor = "1";
                                            $classeSPe2 = "succeedLine";
                                        }
                                        if ($project_backlinks[$annuaire_id]['status'] == 'not_found_yet') {
                                            $Reponse .= "Lien en Attente ";
                                            $ReponseColor = "2";
                                            $classeSPe2 = "inProcess";
                                        }
                                        if ($project_backlinks[$annuaire_id]['status'] == 'not_found') {
                                            $Reponse .= "Lien Refus� ";
                                            $ReponseColor = "3";
                                            $classeSPe2 = "noSucceedLine";


                                        }
                                        if ($project_backlinks[$annuaire_id]['status'] == 'found' || ($tableau[$limit1]["todayJob"] >= 0 && $tableau[$limit1]["todayJob"] <= 2)) {
                                            $classeSPe3 = "CorrectLine";
                                        }

                                        // show Impossible tasks
                                        if (!isset($project_backlinks[$annuaire_id]['status']) && $failedLine){
                                            //$classeSPe3 = "CorrectLine";
                                        }
                                    }



                                    if (isset($tableau[$limit1]['annuaire']) && !empty($tableau[$limit1]['annuaire']) && ((isReferer() && ($tableau[$limit1]["todayJob"] == 1 && ($specialAssignement == 0 || ($specialAssignement == 1 && $tableau[$limit1]["dayJour"] > 0)) )) || (isSuperReferer() && $tableau[$limit1]["todayJob"] > 0 && $tableau[$limit1]["affectedTO"] != $idUser) || isWebmaster() || isAdmin() || isSu())) {
                                         //                                   echo $tableau[$limit1]["todayJob"];
                                        //                                   echo $limit1;
                                        $afficher++;
                                        ?>


                                        <?PHP

                                        // calculations task`s cost
                                        $bonusAdded = 0;
                                        $bonusAdded_Web = 0;

                                        if ($getSupps[$projet_id] && in_array($tableau[$limit1]["annuaireID"], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                            $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                                            $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                                            if ($bonusAdded_Web == 0) {
                                                $bonusAdded_Web = $ComptePersoWebmaster;
                                            }
                                        }
                                        //$owner_solde = 3.0;

                                        //$owner_task_cost =  ($tableau[$limit1]["tarifW"] + Functions::getRemunuerationWebmaster($tableau[$limit1]["proprietaire"]) + $bonusAdded_Web);
                                        $owner_task_cost =  $tableau[$limit1]['owner_task_cost'];
                                        $admin_show_task_not_enough_money = ( (isSu() || isAdmin()) && ($owner_task_cost > $owner_solde) && ( ($tableau[$limit1]["todayJob"] == 1) || ($tableau[$limit1]["todayJob"] == 0) ) );
                                        ?>



                                        <tr  class="secondaireTR <?PHP echo $classeSPe . " " . $classeSPe2 . " " . $classeSPe3; ?>" >
                                            <td  <?php if ($admin_show_task_not_enough_money) {?> style="background:yellow;" <?php } ?>>
                                                <?php //echo $owner_task_cost;?>
                                                <?Php //echo $tableau[$limit1]['tarifW'].'<br/>';     ?>

                                                <?php //echo 'Min words ='.$tableau[$limit1]['min_words_count'].'<br/>';?>
                                                <?php //echo 'Max words ='.$tableau[$limit1]['max_words_count'].'<br/>';?>

                                                <?PHP
                                                echo Functions::longeur($tableau[$limit1]['annuaire'], $longMot);
                                                if ($tableau[$limit1]['annuaire'] != $erreurLienAnnuaire) {
                                                    ?> 
                                                    <a href="<?PHP echo $lienExterne; ?>" target="_blank" title="Ouvrir l'annuaire"><i class="icon-external-link"></i></a>
                                                    <?PHP
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?PHP
                                                if ($tableau[$limit1]['soumission'] == "Indisponible" && $getCommande['over'] == 1 && isset($_GET['from']) && $_GET['from'] == "over" && $tableau[$limit1]["todayJob"] < 2) {
                                                    $tableau[$limit1]['soumissible'] = "Indisponible";
                                                }
                                                // echo $tableau[$limit1]["todayJob"].'    ';
                                                // echo '<b>'.$tableau[$limit1]['id'].'  </b>';
                                                echo $tableau[$limit1]['soumissible'];
                                                ?>
                                            </td>
                                            <td>
                                                <?PHP echo $tableau[$limit1]['soumission']; ?>
                                                <?PHP if ((isAdmin() || isSu() || isWebmaster()) && $tableau[$limit1]['affectedTO'] != 0 && $tableau[$limit1]['statut'] == "OK" && $tableau[$limit1]['affectedTO'] != $getCommande['affectedTO']) { ?>
                                                <?PHP } ?>
                                                <?PHP
                                                //                                                print_r($getRestarted[$getCommande['id'].$tableau[$limit1]['annuaireID']]);
                                                if ($getRestarted[$getCommande['id'] . $tableau[$limit1]['annuaireID']]['LatestRestart']) {
                                                    echo '<br/><span style="color:green;font-weight:bold;"><u style="color:green;font-weight:bold;">Repropos� le</u>:<br/>' . date('d/m/Y', $getRestarted[$getCommande['id'] . $tableau[$limit1]['annuaireID']]['LatestRestart']) . "</span>";
                                                }
                                                ?>
                                            </td>

                                            <td style="line-height:17px;">

                                                <?PHP
                                                if ($tableau[$limit1]["today"] >= 2 && $tableau[$limit1]['affectedTO'] != $getCommande['affectedTO'] && (isAdmin() || isSu())) {
                                                    echo 'T�che �ffectu�e par :<br/>' . Functions::getfullname($tableau[$limit1]['affectedTO']) . "<br/>";
                                                }

                                                $ComCom = Functions::getAllCommentaire($getCommande['id'], $tableau[$limit1]['annuaireID']);
                                                if ($ComCom && !empty($ComCom['common']['totalcom'])) {
                                                    echo '<span class="activeComment" data-title="la ' . $defaultLabel . '" rel="' . $tableau[$limit1]['id'] . '"><i class="icon-eye-open"></i> <span class="">Voir la ' . $defaultLabel . '</span></span><br/>';
                                                    echo '<div id="commentaireslide' . $tableau[$limit1]['id'] . '" style="display:none;">';

                                                    $writer_id = 0;

                                                    foreach ($ComCom as $keys => $values) {

                                                        if ($keys != "common") {
                                                            if (isset($ComCom[$keys]) && $ComCom[$keys]['commentaire'] != "") {
                                                                // get writer_id
                                                                if ($keys == 3){
                                                                   $writer_id = $ComCom[$keys]['user'];
                                                                }

                                                                echo "<strong><u>" . $arrayLvl_[$keys] . "</u></strong>";
                                                                echo nl2br(PHP_EOL . stripslashes($ComCom[$keys]['commentaire']) . PHP_EOL);
                                                                if ($writer_id && isWebmaster()) {

                                                                    // check liked
                                                                    $like_state = -1; // item isn`t liked yet
                                                                    $annuaire_id = $tableau[$limit1]['annuaireID'];
                                                                    $unliked_text = '';
                                                                    $unliked_text_edition = false;
                                                                    if (isset($writers_likes[$annuaire_id][$writer_id])){
                                                                        $like_state   = $writers_likes[$annuaire_id][$writer_id]['value'];
                                                                        $unliked_text = $writers_likes[$annuaire_id][$writer_id]['unliked_text'];
                                                                        $is_unliked_text = empty($unliked_text)? false : true;
                                                                    }
                                                                    ?>
                                                                    <div class="like_buttons"
                                                                         data-writer_id="<?php echo $writer_id;?>"
                                                                         data-annuaire_id="<?php echo $tableau[$limit1]['annuaireID'];?>"
                                                                         data-project_id="<?php echo $getCommande['id'];?>"


                                                                    >
                                                                        <span class="like_button like <?php echo ($like_state==1)?' active ':''; ?>  <?php echo ($like_state==0)?'suspended':''; ?>" data-state="1">J'aime ce texte, confier plus de t�ches � ce r�dacteur &nbsp;<i class="icon-thumbs-up"></i></span><br/>
                                                                        <span class="like_button unlike <?php echo ($like_state==0)?' active ':''; ?> <?php echo ($like_state==1)?'suspended':''; ?>" data-state="0" href="#">Je n'aime pas ce texte, lui confier moins de t�ches &nbsp;<i class="icon-thumbs-down"></i></span>
                                                                        <div id="unlike_<?php echo $writer_id.'_'.$tableau[$limit1]['annuaireID']; ?>" class="unlike_block" <?php if ( ($like_state==0) ) { ?>style="display:block;" <?php } ?> >
                                                                            <div class="unlike_comment_edit" <?php if(!empty($unliked_text)){ ?>style="display:block;" <?php } ?> ><?php echo utf8_decode($unliked_text); ?></div>

                                                                            <textarea class="unlike_comment" style="display:none" ><?php echo utf8_decode($unliked_text); ?></textarea>

                                                                            <div class="like_button button small round color unlike_comment_button"
                                                                                 data-state="0" data-unlike_comment_action="1"
                                                                                 <?php if($is_unliked_text){ ?> data-edition="1" style="display: inline-block;"<?php } ?>
                                                                                <?php if(!$is_unliked_text){ ?> style="display: none;"<?php } ?>
                                                                                >
                                                                               <?php echo (!$is_unliked_text)? 'Envoyer' : 'Modifier'; ?>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                <?}

                                                                if ($getCommande['over'] == 0 && ($keys == $idTypeUser || (isAdmin() || isSu()))) {
                                                                    echo '<a href="./compte/modifier/commentaire.html?id=' . $ComCom[$keys]['id'] . '"><i class="icon-edit"></i> Modifier </a>&nbsp;&nbsp;';
                                                                    echo '<a href="./compte/supprimer/commentaire.html?id=' . $ComCom[$keys]['id'] . '" onclick="return confirmation();"><i class="icon-trash"></i> Supprimer </a><br/>';
                                                                }
                                                            } else {
                                                                if ($keys == $idTypeUser && $getCommande['over'] == 0) {
                                                                    echo '<a href="./compte/ajout/commentaire.html?annuaire=' . $tableau[$limit1]["annuaireID"] . '&projet=' . $getCommande["id"] . '"><i class="icon-plus"></i> Ajouter un commentaire </a><br/>';
                                                                }
                                                            }
                                                        }
                                                    }

                                                 }

                                                echo "</div>";
                                                if (!isset($ComCom[$idTypeUser]) && $getCommande['over'] == 0) {
                                                    if (isWebmaster() || isAdmin() || isSu()) {
                                                        echo '<a href="./compte/ajout/commentaire.html?annuaire=' . $tableau[$limit1]["annuaireID"] . '&projet=' . $getCommande["id"] . '"><i class="icon-plus"></i> Ajouter un commentaire </a><br/>';
                                                    } else {
                                                        echo "Aucun Commentaire";
                                                    }
                                                }
                                                ?>

                                            </td>

                                            <td>

                                                <?PHP
                                                $annuaire_id = $tableau[$limit1]['annuaireID'];
                                                //if (!empty($project_backlinks[$annuaire_id]) ){
                                                $status_exist = (!(empty($project_backlinks[$annuaire_id]) && !$project_backlinks[$annuaire_id]['first_checked'] ));



                                                if ($tableau[$limit1]['statut'] == "OK" && $tableau[$limit1]["todayJob"] == 2) {
                                                    if (isWebmaster() && !$status_exist) {
                                                        echo '<a href="#" style="color:green;" ><i class="icon-check icon-1x" style="color:green;"></i> T�che Effectu�e </a><br/>';
                                                    }
                                                    if (!isWebmaster()) {
                                                        echo '<a href="#" style="color:green;" ><i class="icon-check icon-1x" style="color:green;"></i> T�che Effectu�e </a><br/>';
                                                    }
                                                } else if ($tableau[$limit1]['statut'] == "OK" && $tableau[$limit1]["todayJob"] == 3) {
                                                    echo '<a href="#" style="color:#4480BC;"><i class="icon-remove-sign" style="color:#4480BC;"></i> Soumission Impossible</a><br/>';
                                                } else if ($tableau[$limit1]['statut'] != "OK" && $tableau[$limit1]["todayJob"] == 1) {
                                                    if ($getCommande['over'] == 0 && $tableau[$limit1]["todayJob"] == 1 && !isReferer()) {
                                                        echo '<a href="#" ><i class="icon-play"></i> T�che du jour</a><br/>';
                                                    } else {
                                                        if ($getCommande['over'] == 1) {
                                                            echo '<a href="#" style="color:red;"><i class="icon-remove-sign" style="color:red;"></i> Non effectu�</a><br/>';
                                                        }
                                                    }
                                                } else if ($tableau[$limit1]['statut'] != "OK" && $tableau[$limit1]["todayJob"] == 0) {
                                                    if (!isReferer() && $getCommande['over'] == 0) {
                                                        echo '<a href="./#" ><i class="icon-pause"></i> En attente</a>';
                                                    } else {
                                                        if ($getCommande['over'] == 1) {
                                                            echo '<a href="#" style="color:red;"><i class="icon-remove-sign" style="color:red;"></i> Non effectu�</a><br/>';
                                                        }
                                                    }
                                                }
                                                ?>
                                                <?PHP
                                                if ($tableau[$limit1]['statut'] == "OK" && $tableau[$limit1]["todayJob"] == 2) {
                                                    ?>
                                                    <?PHP if ((isSU($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer())) { ?>
                                                        <a href="compte/supprimer/revoquerjobs.html?id=<?PHP echo $tableau[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-legal"></i> Rejeter la T�che </a><br/>
                                                       <!--<a href="compte/modification/tacheeffectueeadmin.html?id=<?PHP echo $tableau[$limit1]['id']; ?>" ><i class="icon-external-link"></i> Approuver<br/>la t�che</a><br/>-->
                                                    <?PHP } ?>
                                                    <?PHP
                                                } else if ($tableau[$limit1]["todayJob"] == 3) {
                                                    if ((isWebmaster())) {
                                                        ?>
                                                        <a href="compte/supprimer/revoquerjobs.html?id=<?PHP echo $tableau[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-refresh"></i> Relancer la t�che </a><br/> <i class="" style="font-size:11px;color:red;font-weight:normal;">(Vous n'�tes pas factur�)</i><br/>
                                                        <?PHP
                                                    }
                                                    if ((isSU($idTypeUser) || isAdmin($idTypeUser) || (isSuperReferer() && isset($_GET['typeRef'])))) {
                                                        ?>
                                                        <a href="compte/supprimer/revoquerjobs.html?id=<?PHP echo $tableau[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-refresh"></i> Relancer la t�che</a><br/>
                                                        <?PHP
                                                    }
                                                } else {
                                                    if ($tableau[$limit1]["todayJob"] == 1 && (isSu($idTypeUser) || ( (isReferer() && ($idUser == $tableau[$limit1]["affectedTO"])) || $BONUS_WRITER_ACCESS ))) {
                                                        if ($tableau[$limit1]["todayJob"] == 1 && $tableau[$limit1]['statut'] != "OK" && $retardataire <= $joursRetards && $webmasterCan) {
                                                            $encodage = urlencode(Functions::encodage(CLEINTERNE, base64_encode(serialize($tableau[$limit1]['annuaireID'] . "|" . $tableau[$limit1]['soumissibleTimestamp'] . "|" . $tableau[$limit1]["todayJob"] . "|" . $getCommande['id']))));

                    //                                      $decodage = utf8_decode(unserialize(base64_decode(Functions::decodage(CLEINTERNE, $encodage))));
                    //                                      echo $decodage;
                    //                                      echo $encodage;
                                                            if (1 == 1) {
                                                                ?>
                                                                <a href="#ancres<?PHP echo $tableau[$limit1]["id"]; ?>" class="ancresDisplay" onclick=""><i class="icon-eye-open"></i> Voir la t�che </a><br/>
                                                                <div id="ancres<?PHP echo $tableau[$limit1]["id"]; ?>" class="ancres">
                                                                    <h5 style="color:maroon;">
                                                                        <?PHP
                                                                        $bonusAdded = 0;
                                                                        $bonusAdded_Web = 0;

                                                                        $link_submission = !empty($tableau[$limit1]['link_submission'])? $tableau[$limit1]['link_submission'] : $tableau[$limit1]["annuaire"]  ;
                                                                        echo "<a href='" . $getCommande["lien"] . "' target='_blank'>" . $getCommande["lien"] . "</a> � r�f�rencer sur <a href='" . $link_submission . "' target='_blank'>" . $tableau[$limit1]["annuaire"] . "</a>";
                                                                        echo "<br>";
                                                                        echo "<br>";
                                                                        if (isSu()) {
                                                                            echo "<span style='font-size:12px !important'>Formule: Co�t de base  + Tarif Extra annuaire + Tarif Compte Membre + R�daction</span><br>";
                                                                        }

                                                                        if ($getSupps[$projet_id] && in_array($tableau[$limit1]["annuaireID"], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                                                            $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                                                                            $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                                                                            if ($bonusAdded == 0) {
                                                                                $bonusAdded = $ComptePersoReferenceur;
                                                                            }
                                                                            if ($bonusAdded_Web == 0) {
                                                                                $bonusAdded_Web = $ComptePersoWebmaster;
                                                                            }
                                                                        }

                                                                        //echo $bonusAdded_Web.'<br/>';
                                                                        //echo $ComptePersoWebmaster.'<br/>';

                                                                        //echo ' Annuaire MIN words = '.$tableau[$limit1]['min_words_count'].'<br/>';
                                                                        //echo ' Annuaire MAX words = '.$tableau[$limit1]['max_words_count'].'<br/>';
                                                                        //echo ' Writer tarif = '.$writer_tarif_redaction.'<br/>';

                                                                        //echo 'R�mun�ration = '.($tableau[$limit1]["tarifR"] + $remuneration + $bonusAdded).'<br/>';

                                                                        //if ( )
                                                                        $writer_extra_cost = 0;
                                                                        $webmaster_extra_cost = 0;
                                                                        if ( ($LIST_ANNUAIRE_WORDS_COUNT > 0) && (isSu() || isReferer()) ){
                                                                            $annuaire_counts['min_words_count'] = $tableau[$limit1]['min_words_count'];
                                                                            $annuaire_counts['max_words_count'] = $tableau[$limit1]['max_words_count'];
                                                                            $writer_extra_cost = $annuaireModel->calculateExtraPriceForWriter($annuaire_counts,$LIST_ANNUAIRE_WORDS_COUNT,$writer_tarif_redaction);
                                                                            $webmaster_extra_cost = $annuaireModel->calculateExtraPrice($annuaire_counts,$LIST_ANNUAIRE_WORDS_COUNT);
                                                                        }

                                                                        //echo $writer_extra_cost;

                                                                        if (isReferer()) {
                                                                            // writer extra cost

                                                                            //echo 'Writer Extra Cost = '.$writer_extra_cost.'<br/><br/>';
                                                                            //echo 'TarifR ='.$tableau[$limit1]["tarifR"].'<br/>';

                                                                            if ( ($tableau[$limit1]["tarifR"] > 0) || ($writer_extra_cost > 0)) {
                                                                                $tarifR = $tableau[$limit1]["tarifR"];
                                                                                if ($writer_extra_cost>0){
                                                                                    $tarifR += $writer_extra_cost;
                                                                                }
                                                                                echo "R�mun�ration : " . ($tarifR + $remuneration + $bonusAdded) . " <i class='icon-dollar'></i>  (Bonus: +" .$tarifR. "<i class='icon-dollar'></i>)";
                                                                            } else {
                                                                                echo "R�mun�ration : " . ($remuneration + $bonusAdded) . " <i class='icon-dollar'></i> ";
                                                                            }
                                                                        }

                                                                        if (isSu()) {
                                                                            $webmaster_task_cost = ($tableau[$limit1]["tarifW"] + Functions::getRemunuerationWebmaster($tableau[$limit1]["proprietaire"]) + $bonusAdded_Web);
                                                                            $writer_task_cost = ($tableau[$limit1]["tarifR"] + Functions::getRemunueration($tableau[$limit1]["affectedTO"]) + $bonusAdded) ;

                                                                            // add extra costs
                                                                            if ($writer_extra_cost > 0){
                                                                                $writer_task_cost += $writer_extra_cost;
                                                                            }
                                                                            if ($webmaster_task_cost > 0){
                                                                                $webmaster_task_cost += $webmaster_extra_cost;
                                                                            }
                                                                            //var_dump('cnt = ',$LIST_ANNUAIRE_WORDS_COUNT);
                                                                            echo "Co�t Webmaster: " . $webmaster_task_cost . " <i class='icon-euro'></i> (" . Functions::getRemunuerationWebmaster($tableau[$limit1]["proprietaire"]) . " + " . $tableau[$limit1]["tarifW"] . " + " . $bonusAdded_Web . " + " . $webmaster_extra_cost . ")<br/>";
                                                                            echo "Co�t R�f�renceur: " . $writer_task_cost . " <i class='icon-dollar'></i>  (Bonus non inclus)  (" . Functions::getRemunueration($tableau[$limit1]["affectedTO"]) . " + " . $tableau[$limit1]["tarifR"] . " + " . $bonusAdded . " + " .$writer_extra_cost . " )";
                                                                       }
                                                                        ?>
                                                                    </h5>

                                                                    <?php if( (isReferer() || isSu()) && ($LIST_ANNUAIRE_WORDS_COUNT > 0)){
                                                                            $task_words_count = $LIST_ANNUAIRE_WORDS_COUNT;
                                                                            if ($tableau[$limit1]['max_words_count'] < $LIST_ANNUAIRE_WORDS_COUNT ){
                                                                                $task_words_count = $tableau[$limit1]['max_words_count'];
                                                                            }

                                                                            if ( $LIST_ANNUAIRE_WORDS_COUNT < $tableau[$limit1]['min_words_count']){
                                                                                $task_words_count = $tableau[$limit1]['min_words_count'];
                                                                            }

                                                                            if ($task_words_count > 0 ){ ?>
                                                                        <div class="annuaire_words_count">NOMBRE DE MOTS MINIMUM: <?php echo $task_words_count; ?></div>
                                                                    <?php }
                                                                        } ?>
                                                                    <?PHP
                                                                    $ancreContent = "";
                                                                    $ancreSpecial = "";
                        //                                                                    echo  $tableau[$limit1]["AnnuaireDisplayAncre"];
                        //                                                                    print_r( $ancreAnnuaireAll[$getCommande["id"]]["269"]);
                                                                    if (trim($ancreAnnuaireAll[$getCommande["id"]][$tableau[$limit1]["annuaireID"]]['ancre']) != "" && $tableau[$limit1]["AnnuaireDisplayAncre"] == 1) {
                                                                        $ancreContent = $ancreAnnuaireAll[$getCommande["id"]][$tableau[$limit1]["annuaireID"]]['ancre'];
                                                                        $ancreSpecial = "font-weight:bold;color:red;font-size:17px;";
                                                                    } else {
                                                                        if (trim($getCommande["ancres"]) != "" && $tableau[$limit1]["AnnuaireDisplayAncre"] == 1) {
                                                                            $ancreContent = $getCommande["ancres"];
                                                                        }
                                                                    }
                                                                    if ($bonusAdded > 0 && !isWebmaster() && !empty($getSupps[$projet_id]['url']) && !empty($getSupps[$projet_id]['username']) && !empty($getSupps[$projet_id]['password'])) {
                                                                        ?>
                                                                        <h5 style="color:red;font-size : 15px;">ATTENTION!!! L'inscription est � r�aliser avec l'adresse email du webmaster accessible avec les informations ci-dessous:</h5>
                                                                        <span style='font-size:14px;'>
                                                                            <?PHP
                                                                            echo "<u style='font-size:14px;font-weight:bold;'>URL d'acc�s Web</u>:  " . $getSupps[$projet_id]['url'] . "<br/>";
                                                                            echo "<u style='font-size:14px;font-weight:bold;'>Adresse Email</u>: " . $getSupps[$projet_id]['username'] . "<br/>";
                                                                            echo "<u style='font-size:14px;font-weight:bold;'>Mot de passe</u>: " . $getSupps[$projet_id]['password'] . "<br/>";
                                                                            ?>
                                                                        </span>
                                                                        <br/>
                                                                        <hr/>
                                                                        <br/>
                                                                        <?PHP
                                                                    }
                                                                    if (trim($ancreContent) != "") {
                                                                        ?>
                                                                        <h4><u>Ancres � utiliser</u>:</h4>
                                                                        <span style="<?PHP echo $ancreSpecial; ?>">
                                                                            <?PHP
                                                                            echo nl2br(stripslashes($ancreContent));
                                                                            ?>
                                                                        </span>
                                                                        <br/>
                                                                        <hr/>
                                                                        <br/>
                                                                        <?PHP
                                                                    }
                                                                    if (trim($tableau[$limit1]["consignesAnnuaire"]) != "") {
                                                                        ?>
                                                                        <h4><u>Consignes de l'annuaire</u>:</h4>
                                                                        <?PHP
                                                                        echo nl2br(stripslashes($tableau[$limit1]["consignesAnnuaire"]));
                                                                        ?>
                                                                        <br/>
                                                                        <hr/>
                                                                        <br/>
                                                                        <?PHP
                                                                    }
                                                                    if (trim($getCommande["consignes"]) != "") {
                                                                        ?>
                                                                        <h4><u>Consignes du projet</u>:</h4>
                                                                        <?PHP
                                                                        echo nl2br(stripslashes($getCommande["consignes"]));
                                                                        ?>
                                                                        <br/>
                                                                        <hr/>
                                                                        <br/>
                                                                        <?PHP
                                                                    }
                                                                    if (isReferer()) {
                                                                        ?>
                                                                        <div align="center" style="width:450px;font-size:16px;margin:auto;">
                                                                            <br/><br/>
                                                                            <a href="compte/modification/tacheeffectuee.html?param=<?PHP echo $encodage; ?>" onclick="return confirmationReferer();" style="color:green;display:block;width:200px;float:left;margin-right:20px;"><i class="icon-ok-sign" style="color:green"></i> Soumission effectu�e </a>
                                                                            <a href="compte/modification/tacheeffectuee.html?cant=1&param=<?PHP echo $encodage; ?>" onclick="return confirmationReferererNo();"  style="color:red;display:block;width:200px;float:right;"><i class="icon-remove-sign" style="color:red"></i> Soumission impossible </a>
                                                                            <br/><br/>
                                                                        </div>
                                                                    <?PHP }
                                                                    ?>
                                                                </div>
                                                            <?PHP } else {
                                                                ?>
                                                                <a href="compte/modification/tacheeffectuee.html?param=<?PHP echo $encodage; ?>" onclick="return confirmationReferer();" style="color:green"><i class="icon-ok-sign" style="color:green"></i> Soumission effectu�e </a><br/>
                                                                <a href="compte/modification/tacheeffectuee.html?cant=1&param=<?PHP echo $encodage; ?>" onclick="return confirmationReferererNo();" style="color:red"><i class="icon-remove-sign"  style="color:red"></i> Soumission impossible </a>
                                                                <?PHP
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>

                                                <?PHP if ((isSU($idTypeUser) || isAdmin($idTypeUser)) == "OK") { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                                            <a href="compte/modification/tacheeffectueeadmin.html?id=<?PHP echo $tableau[$limit1]['id']; ?>" ><i class="icon-external-link"></i> Approuver<br/>la t�che</a><br/>-->
                                                <?PHP } ?>

                                                <?php
                                                if ($tableau[$limit1]["todayJob"] == 2 && !$failedLine && !empty($project_backlinks[$annuaire_id]) && $project_backlinks[$annuaire_id]['first_checked']) {

                                                    $status_info = $project_backlinks[$annuaire_id];

                                                    if ($status_info['status'] == 'found') {
                                                        $countFound++;
                                                        $status_color = '#298A08';
                                                        $status_text = 'Lien trouv�!';
                            //                                                        if ($status_info['status_type'] == 'cron' && $status_info['date_found'] == '0000-00-00 00:00:00') {
                                                        if ($status_info['date_found'] == '0000-00-00 00:00:00') {
                            //                                                            $updateBackLinks = $dbh->query('UPDATE annuaires2backlinks SET date_found = "' . date("Y-m-d H:i:s") . '" WHERE site_id = ' . $project_id . ' AND annuaire_id = ' . $annuaire_id);
                                                        }
                                                    }

                                                    if ($status_info['status'] == 'not_found_yet') {
                                                        $status_color = '#FF8000';
                                                        $status_text = 'Recherche du Lien en cours';
                                                        $countNotFoundYet++;
                                                    }

                                                    if ($status_info['status'] == 'not_found') {
                                                        $status_color = '#FF0000';
                                                        $status_text = "Lien non trouv�";
                                                        $countNotFound++;
                                                    }

                                                    if (!empty($status_info['backlink']) && !substr_count($status_info['backlink'], 'http:')) {
                                                        $status_info['backlink'] = str_replace('www.', '', $status_info['backlink']);
                                                        $status_info['backlink'] = 'http://www.' . $status_info['backlink'];
                                                    }
                                                    ?>
                                                    <div style="color:<?php echo $status_color; ?>;text-align: left;">
                                                        <?php echo $status_text; ?>
                                                        <?php
                                                        if (!empty($status_info['backlink'])) {
                                                            $urlTrouvee = $status_info['backlink'];
                                                            if (!strpos($lienExterne,'www')){ // fix
                                                                $status_info['backlink'] = str_replace('www.','',$status_info['backlink']);
                                                            }
                                                            ?>
                                                            <a target="_blank" style="color:<?php echo $status_color; ?>;" href="<?php echo $status_info['backlink']; ?>"> Voir le lien</a>

                                                        <?php } ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </td>



                                        </tr>
                                        <?PHP
                                    }

                            //                                    if ($tableau[$limit1]['todayJob'] == 2 && $tableau[$limit1]['annuaire'] != $erreurLienAnnuaire) {
                                    if ($tableau[$limit1]['todayJob'] == 2) {
                                        $array_to_csv[] = Array(
                                            $tableau[$limit1]['annuaire'],
                                            $tableau[$limit1]['soumission'],
                                            $Reponse,
                                            $urlTrouvee,
                                            $CommentairesAssocies,
                                            $ReponseColor
                                        );
                                    }

                                    $countage++;
                                    $limit1++;
                                }
                                ?>
                            </tbody>
                        </table>

                        <!-- Modal - For unliked comment -->
                        <div class="modal" id="details_unliked_modal"  style="display:none;">
                            <div class="header">Souhaitez-vous indiquer ce que vous n'avez pas aim� ?</div>
                            <textarea class="unlike_comment" ></textarea>
                            <div class="footer">
                                <div class="button color big send_comment">Envoyer</div>
                                <div class="button color big send_comment without_comment" data-finish="1">Non merci !</div>
                            </div>
                        </div>

                        <?PHP

                        $totalResult = $countFound + $countNotFoundYet + $countNotFound;
                        //                        echo "ff" . $countNotFound;
                        $PourcentFound = Functions::getPourcentage($countFound, $totalResult);
                        $PourcentNotFoundYet = Functions::getPourcentage($countNotFoundYet, $totalResult);
                        //                        $PourcentNotFound = Functions::getPourcentage($countNotFound, $totalResult);
                        $PourcentNotFound = round((100 - ($PourcentFound + $PourcentNotFoundYet)), 1);
                        //                        $PourcentImpossible = round((100 - ($PourcentFound + $PourcentNotFoundYet + $PourcentNotFound)), 1);
                        ?>
                        <script>
                            (function ($) {

                                // modal processing unliked comment
                                $('#details_unliked_modal .send_comment').on('click',function(){
                                    var modal = $('#details_unliked_modal');
                                    var comment = $(modal).find('.unlike_comment');

                                    $(comment).removeClass('error_empty');


                                   // close modal/ without unliked comment
                                   if ($(this).data('finish')){
                                       $("#details_unliked_modal").hide();
                                       $(document).find('.blocker').hide();
                                       $(document).find('body').css('overflow','');
                                       $(comment).val('');
                                       return;
                                   }


                                    // check if webmaster inputs comment
                                    var unliked_text = $(comment).val();
                                    if (!unliked_text.length) {
                                       $(comment).addClass('error_empty');
                                       return;
                                    }else{

                                        var data = {
                                            writer_id: $(modal).data('writer_id'),
                                            annuaire_id: $(modal).data('annuaire_id'),
                                            project_id: $(modal).data('project_id'),
                                            value: $(modal).data('like_state'),
                                            unliked_comment: unliked_text,
                                            type: 'writer_likes'
                                        };

                                        // current unliked block at table
                                        var unlike_block = $('#unlike_'+data.writer_id+'_'+data.annuaire_id);
                                        console.log('#unlike_'+data.writer_id+'_'+data.annuaire_id);

                                        var unliked_comment = $(unlike_block).find('.unlike_comment');
                                        $(unliked_comment).removeClass('error_empty');
                                        var like_btn = $(unlike_block).find('.like_button');

                                        $.post("files/includes/ajax/update.php", data, function (repsonse) {
                                            if (repsonse) {
                                                var unlike_comment_btn = $(unlike_block).find('.unlike_comment_button');

                                                // like-unliked action
                                                $(unlike_comment_btn).text('Modifier').show();
                                                $(unlike_block).find('.unlike_comment_edit').text(unliked_text).show();
                                                $(unliked_comment).val(unliked_text).hide();
                                                $(like_btn).data('edition',true);
                                                $(unlike_block).show();


                                                $("#details_unliked_modal").hide();
                                                $(document).find('.blocker').hide();
                                                $(document).find('body').css('overflow','');
                                                $(comment).val('');

                                            }
                                        });



                                    }

                                });



                                // likes writers-text
                                $('.like_button').click(function(e){
                                    e.preventDefault();

                                    var like_btn = $(this);
                                    var like_state = $(this).data('state');
                                    var parent = $(this).parents('.like_buttons'); // like group container

                                    // unliked
                                    var unlike_block = $(parent).find('.unlike_block');
                                    var unliked_comment = $(unlike_block).find('.unlike_comment');
                                    $(unliked_comment).removeClass('error_empty');

                                    var unlike_comment_btn = $(unlike_block).find('.unlike_comment_button');

                                    var unliked_comment_action = false;
                                    if ($(this).data('unlike_comment_action')){
                                        unliked_comment_action = true;
                                    }

                                    // edition unliked text
                                    if (unliked_comment_action && $(this).data('edition')){
                                        $(unlike_comment_btn).text('Envoyer');
                                        $(parent).find('.unlike_comment_edit').hide();
                                        $(unliked_comment).show();
                                        $(this).data('edition',false);
                                        return;
                                    }


                                    console.warn('fire');

                                    if (!unliked_comment_action) {
                                        if (like_state) {
                                            $(unlike_block).slideUp('fast');
                                            $(unliked_comment).val('');
                                            $(parent).find('.unlike_comment_edit').hide();
                                            $(unliked_comment).show();
                                            $(unlike_comment_btn).data('edition',false).text('Envoyer');
                                        } else {
                                            var modal = $("#details_unliked_modal");

                                            $(modal).data('like_state',like_state);
                                            $(modal).data('writer_id',$(parent).data('writer_id'));
                                            $(modal).data('annuaire_id',$(parent).data('annuaire_id'));
                                            $(modal).data('project_id',$(parent).data('project_id'));

                                            $(modal).modal({
                                                    escapeClose: false,
                                                    clickClose: false,
                                                    showClose: false
                                            });

                                            // deprecated
                                            //$(unlike_block).slideDown('fast');
                                        }
                                    }

                                    var unliked_text = $(unliked_comment).val();
                                    if (unliked_comment_action){
                                        console.log('Text =',unliked_text);
                                        if (!unliked_text.length){
                                            $(unliked_comment).addClass('error_empty');
                                            return;
                                        }else{
                                            // bugfix
                                            // unliked_text.replace(new RegExp('?', 'g'), 'oe');
                                        }
                                    }



                                    var data = {
                                        writer_id: $(parent).data('writer_id'),
                                        annuaire_id: $(parent).data('annuaire_id'),
                                        project_id: $(parent).data('project_id'),
                                        value: like_state,
                                        unliked_comment: unliked_text,
                                        type: 'writer_likes'
                                    };

                                    $(this).data('ajax_processing',true);

                                    $.post("files/includes/ajax/update.php", data, function (repsonse) {
                                        if (repsonse) {
                                            // like-unliked action
                                            if (!unliked_comment_action) {
                                                $(parent).find('.like_button').removeClass('suspended').removeClass('active');
                                                if (like_state) {
                                                    $(parent).find('.like').addClass('active');
                                                    $(parent).find('.unlike').addClass('suspended');
                                                } else {
                                                    $(parent).find('.like').addClass('suspended');
                                                    $(parent).find('.unlike').addClass('active');
                                                }
                                            }

                                            // send unliked action
                                            if (unliked_comment_action){
                                                $(unlike_comment_btn).text('Modifier');
                                                $(parent).find('.unlike_comment_edit').text(unliked_text).show();
                                                $(unliked_comment).hide();
                                                $(like_btn).data('edition',true);
                                            }
                                        }
                                    });

                                });

                                // totals|counters
                                $('.project_costs_total').eq(0).find('span').text("<?Php echo round($sum_paid,2); ?>");
                                $('.project_costs_total').eq(1).find('span').text("<?Php echo round($sum_not_paid,2); ?>");

                                $('.nbrFound').find('span:first').text(" <?Php echo intval($countFound); ?>");
                                $('.nbrFound').find('span:last').text("( <?Php echo $PourcentFound . "%"; ?> )");

                                $('.nbrNotFoundYet').find('span:first').text(" <?Php echo intval($countNotFoundYet); ?>");
                                $('.nbrNotFoundYet').find('span:last').text("( <?Php echo $PourcentNotFoundYet . "%"; ?> )");

                                $('.nbrNotFound').find('span:first').text(" <?Php echo intval($countNotFound); ?>");
                                $('.nbrNotFound').find('span:last').text("( <?Php echo $PourcentNotFound . "%"; ?> )");

                                $('.nbrImpossible').find('span:first').text(" <?Php echo $countImpossible; ?>");
                                $('.nbrImpossible').find('span:last').text("( Non Factur�es<?Php // echo $PourcentImpossible . "%";                                                   ?> )");

                            })(jQuery);

                        </script>
                        <!-- Pagination -->
                        <?PHP
                        if (!isReferer()) {
                            ?>

                            <script>
                                (function ($) {
                                    $('#detailsTable tr.secondaireTR').fadeOut(10, function () {
                                        $('#detailsTable tr.CorrectLine').fadeIn();
                                    });
                                })(jQuery);
                            </script>
                            <?PHP
                        }
                        //var_dump( $remuneration);
                    }
                    if ($nbre_result > $pagination) {
                        echo $list_page;
                    }
                } else {

                    //var_dump('redirect'); die;
                    header('location: ./accueil.html');
                }
            } else {
                $_SESSION['alertLogincontent'] = "Aucune soumission pr�vue pour vous ce jour!<br/>";
                header('location: ./accueil.html?statut=warning');
            }
            ?>
            <!-- End pagination -->
        </div>




    </div>
<?php


?>

</div>
<?PHP
//if (isset($_GET['mode']) && $_GET['mode'] == "csv" && $getCommande['over'] == 1 && $array_to_csv) {
if ($getCommande['over'] >= 0 && $array_to_csv) {
    unset($_SESSION['Print_details_projet']);
    $_SESSION['Print_details_projet']['contents'][0]['content'] = $array_to_csv;
    $_SESSION['Print_details_projet']['contents'][0]['nameSheet'] = $getCommande['lien'];
    $_SESSION['Print_details_projet']['nom'] = '' . Functions::nameCrypt($getCommande['lien']);
    $_SESSION['Print_details_projet']['titre'] = 'Rapport projet ' . $getCommande['lien'];

}
include("files/includes/bottomBas.php");
?>