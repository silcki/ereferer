<?PHP
@session_start();

$a = 1;
$b = 11;
$page = 1;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        ?>
        <div style="position:absolute;top:0px;left:0;margin-left:220px;margin-top:50px;z-index: 9999">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
            }
            ?>
        </div>
        <div class="three-fourth">
            <?PHP
            $annuairesTous = Functions::getAnnuaireAll();
            $idAnnuaireParent = intval($_GET['id']);
            $currentAnnuaireList = Functions::getOneAnnuaireListe($idAnnuaireParent);

            // checking access
            Functions::checkAccess(isWebmaster(),$idUser,$currentAnnuaireList['proprietaire']);

            if ($currentAnnuaireList && ((isSu() || isAdmin()) || (isWebmaster() && $currentAnnuaireList['proprietaire'] == $idUser))) {

                if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                    $annuairesFromListNew = explode(";", $currentAnnuaireList['annuairesList']);
                } else {
                    $annuairesFromListNew = array();
                }
                $annuairesFromListNew = array_filter($annuairesFromListNew);

                $aEdit = 0;
                foreach ($annuairesFromListNew as $annuaireThatId) {
                    if (!isset($annuairesTous[$annuaireThatId]) || (isset($annuairesTous[$annuaireThatId]) && (!Functions::checkUrlFormat($annuairesTous[$annuaireThatId]['annuaire']) || $annuairesTous[$annuaireThatId]['active'] == 0))) {
                        $aEdit++;
//                    unset($annuairesFromListNew[$annuaireThatId]);
                        unset($annuairesFromListNew[array_search($annuaireThatId, $annuairesFromListNew)]);
                    }
                }
//            echo count($annuairesFromListNew);
                $contentThatListImploded = "";
                if ($aEdit > 0) {
                    $contentThatListImploded = implode(";", $annuairesFromListNew);
                    $contentThatListImploded .= ";";
                    if ($contentThatListImploded != ";") {
                        $requete = "UPDATE annuaireslist SET annuairesList = '" . $contentThatListImploded . "' WHERE id=" . intval($idAnnuaireParent);
                        $execution = $dbh->query($requete);
                        $currentAnnuaireList['annuairesList'] = $contentThatListImploded;
                    }
                }

                $annuairesFromListNew = array_filter($annuairesFromListNew);
                $count = count($annuairesFromListNew);
//            echo $count;
//            print_r($annuairesFromListNew);
                include("pages.php");
                ?>

                <h4>Liste des annuaires de la liste : <?PHP echo $count; ?> annuaire(s) .</h4>
                <?PHP
                if ($count > 0) {
                    ?>
                    <a href="./compte/modifier/listannuairecontent.html?id=<?PHP echo $idAnnuaireParent; ?>" class="button color small round" style="color:white;float:right;margin:5px 0px 5px 10px;">Modifier cette liste</a>
                    <table class="simple-table">
                        <tbody>
                            <tr>
                                <th style="width:20px   ">
                                    N�
                                </th>
                                <th>
                                    Annuaire
                                </th>
                            </tr>
                            <?PHP
                            while ($limit1 <= $limit2) {
                                if ($annuairesFromListNew[$limit1 - 1]) {
                                    $url = Functions::getAnnuaireName($annuairesFromListNew[$limit1 - 1]);
                                    $link = Functions::getAnnuaireName($annuairesFromListNew[$limit1 - 1]);
                                    if ($url == "") {
                                        $url = 'Annuaire Introuvable';
                                        $link = "#";
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <?PHP echo ($limit1); ?>
                                        </td>
                                        <td>
                                            <a href="<?PHP echo $link; ?>" target="_blank"><?PHP echo $url; ?></a>
                                        </td>
                                    </tr>
                                    <?PHP
                                }
                                $limit1++;
                            }
                            ?>
                        </tbody>
                    </table>
                    <!-- Pagination -->
                    <?PHP
                    if ($nbre_result > pagination) {
                        echo $list_page;
                    }
                } else {
                    ?>
                    <div align="center" width="500" style="width:700px;margin:auto;">
                        <h1>Aucun r�sultat � afficher.</h1>
                        <p class="p_menu" style="width:150px;"><a href="./compte/ajout/listannuaire.html" class="a_menu button color <?PHP
                                                                  if ($b == 1) {
                                                                      echo "active";
                                                                  }
                                                                  ?>"> Ajouter une liste</a></p>
                    </div>

                    <?PHP
                }
            } else {
                echo '<h1 style="color:red">Attention! Cette liste ne vous appartient pas.</h1>';
            }
            ?>
            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>