<?PHP
@session_start();

$a = 1;
$b = 1;
$page = 1;
include('files/includes/topHaut.php');
isRight('3');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">


        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        if (!isset($_GET['action']) || empty($_GET['action'])) {
            $_GET['action'] = "modification";
        } else {
            $_GET['action'] = "ajouter";
        }
//        echo $_GET['action'];

        if (isWebmaster()) {
            $getCommande = Functions::getCommande($_GET['id'], $idUser);

            // not owner
            if (empty($getCommande)){
                Functions::redirectJS('/');
            }
        }
        if (isAdmin() || isSu()) {
            $getCommande = Functions::getCommande($_GET['id'], 0, 1);
        }
        $idUserPropList = $getCommande['annuaire'];
        if (isSu() || isAdmin()) {
            $currentAnnuaireList = Functions::getOneAnnuaireListe($idUserPropList, 0);
        } else {
            $currentAnnuaireList = Functions::getOneAnnuaireListe($idUserPropList, $idUser);
        }

        if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
            $annuairesFromListNew = explode(";", $currentAnnuaireList['annuairesList']);
        } else {
            $annuairesFromListNew = array();
        }

        $annuairesFromListNew = array_filter($annuairesFromListNew);
        $projet_id = intval($getCommande['id']);
        $getSupps = Functions::getPersoWebmasterCompte($projet_id);
        $isYes = 0;
        $costReferenceur = $ComptePersoReferenceur;
        $costWebmaster = $ComptePersoWebmaster;
        $url = '';
        $username = '';
        $password = '';
        $annuairesSelected = array();

        if ($getSupps && $getSupps[$projet_id]['annuairesList']) {
            $isYes = $getSupps[$projet_id]['active'];
            $costReferenceur = $getSupps[$projet_id]['costReferenceur'];
            $costWebmaster = $getSupps[$projet_id]['costWebmaster'];
            $url = $getSupps[$projet_id]['url'];
            $username = $getSupps[$projet_id]['username'];
            $password = $getSupps[$projet_id]['password'];
            $annuairesSelected = $getSupps[$projet_id]['annuairesList'];
        }

        $idutilisateur = $getCommande['proprietaire'];

//        $dbh->query('UPDATE annuaires SET ComptePersoWebmaster = 1 WHERE id IN  (' . implode(',', $annuairesSelected) . ')');
//        print_r($annuairesFromListNew);

        if ($_GET['action'] == "modification") {
            
        }

        $isYes = isset($_SESSION['tampon']['poss']) ? $_SESSION['tampon']['poss'] : $isYes;
        $url = isset($_SESSION['tampon']['url']) ? $_SESSION['tampon']['poss'] : "";
        $username = isset($_SESSION['tampon']['username']) ? $_SESSION['tampon']['username'] : "";
        $password = isset($_SESSION['tampon']['password']) ? $_SESSION['tampon']['password'] : "";
        $password = isset($_SESSION['tampon']['password']) ? $_SESSION['tampon']['password'] : "";
        $annuairesSelected = isset($_SESSION['tampon']['annuaires']) ? $_SESSION['tampon']['annuaires'] : $annuairesSelected;
        ?>

        <div class="three-fourth">
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:600px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertLogincontent']; ?>
                    </p>
                </div>
            <?PHP } ?>

            <h4>Soumissions personnalis�es, projet : 
                <?PHP echo $getCommande['lien']; ?>
                <?PHP
                if (isSu() || isAdmin()) {
//                    echo " du webmaster " . Functions::getfullname($getCommande['proprietaire']);
                }
                ?>
            </h4>
            <?PHP
            $count = 0;
            $count = count($annuairesFromListNew);
            ?>

            <?PHP
            $countAuthorized = 0;
            $countAuthorizedChecked = 0;
            $countNoAuthorized = 0;
            $sum_to_pay = 0;
            $ContentAuthorizeAnnuaire = "";
            $ContentAuthorizeAnnuaireEffectuee = "";
            $ContentNoAuthorizeAnnuaire = "";
//            $ContentAuthorizeAnnuaire .= "<br/><input name='zero' id='checkkall' rel='' class='' checked='' type='checkbox' title='Cocher tous'/> <label for='checkkall' class='checkkall' style='display:inline;color:green;'> Cocher tous</label><br/>";

            foreach ($annuairesFromListNew as $idAnnuaire) {
                $annuaireInfos = Functions::getAnnuaire($idAnnuaire);
                if ($annuaireInfos && $annuaireInfos['ComptePersoWebmaster'] == 1) {
                    $countAuthorized++;

                    $checked = "";
                    if (in_array($idAnnuaire, $annuairesSelected)) {
                        $isJobbed = Functions::getJobLine($projet_id, $idAnnuaire);
//                        print_r($isJobbed);
                        if ($isJobbed['id']) {
                            $checked = "none ";
                        } else {
                            $sum_to_pay += $costWebmaster;
                            $countAuthorizedChecked++;
                            $checked = "checked='checked' ";
                        }
                    }
                    if ($checked == "none ") {
                        $ContentAuthorizeAnnuaireEffectuee .= "- <span style='font-weight:bold;color:green;'> " . $annuaireInfos['annuaire'] . " : T�che d�j� effectu�e</span><br/>";
                    } else {
                        $ContentAuthorizeAnnuaire .= "<input name='annuaires[]' id='annuaire" . $idAnnuaire . "' "
                                . "rel='" . $costWebmaster . "' class='checkboxs' " . $checked
                                . "type='checkbox' value='" . $idAnnuaire . "'/> "
                                . "<label for='annuaire" . $idAnnuaire . "' style='display:inline;'>" . $annuaireInfos['annuaire'] . "</label><br/>";
                    }
                } else {
                    $ContentNoAuthorizeAnnuaire .= "- <span style=''>" . $annuaireInfos['annuaire'] . "</span><br/>";
                    $countNoAuthorized++;
                }
            }
            $ContentAuthorizeAnnuaire .= $ContentAuthorizeAnnuaireEffectuee;
            if (empty($ContentAuthorizeAnnuaire)) {
                $ContentAuthorizeAnnuaire = "<span style='color:red;'>Aucun annuaire dans cette liste.</span>";
            }
            if (empty($ContentNoAuthorizeAnnuaire)) {
                $ContentNoAuthorizeAnnuaire = "Aucun annuaire dans cette liste.</span>";
            }
            ?>

            <form class="simple-table" name="" action="./compte/<?PHP echo $_GET['action']; ?>/emailpro.html?id=<?PHP echo $_GET['id']; ?>" method="POST">


                <input type="submit" class="button color small round" value="Terminer" style="color:white;float:right;"/>
                <?PHP
                if ($count > 0) {
                    ?>
                    <br/>   
                    <div class="clear"> <br/><br/> </div>

                    <b>Souhaitez vous que les soumissions soient effectu�es depuis votre adresse email?</b>
                    <p><i>Vous aurez alors le contr�le des soumissions et pourrez alors modifier les textes et les ancres par vous m�me ult�rieurement.</i></p>
                    <b>Le co�t suppl�mentaire est de <?PHP echo $costWebmaster; ?> <i class='icon-euro'></i> par compte cr��.</b>

                    <fieldset class="one" style="clear:both">
                        <br/>
                        <!--<label style="width:300px;">Possibilit� de cr�er un compte au webmaster:</label>-->
                        <div style="width:100px;float:left;margin-right:10px;">
                            <input name="poss" class="search-field reponseNeed" type="radio" value='0' style="float:left;" maxlength="300" id="possnonCompte"  <?PHP
                            if ($isYes == 0) {
                                echo 'checked="checked"';
                            }
                            ?>>
                            <label for="possnonCompte" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                        </div>

                        <div style="width:50px;float:left;margin-right:10px;">
                            <input name="poss" class="search-field reponseNeed" type="radio" value='1' style="float:left;" maxlength="300" id="possouiCompte" <?PHP
                            if ($isYes == 1) {
                                echo 'checked="checked"';
                            }
                            ?>>
                            <label for="possouiCompte" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                        </div>
                        <br/>
                        <br/>
                    </fieldset>

                    <div id="showUp" style="display:none;">
                        <?PHP
                        if ($_GET['action'] == "modification" && isSu()) {
                            ?>
                            <input type="hidden" value="<?PHP echo $idutilisateur; ?>" name="user_id"/>
                            <fieldset style="clear:both;">
                                <label class="color" for="costWebmaster">Tarif Webmaster:</label>
                                <input name="costWebmaster" id="costWebmaster" class="search-field" value="<?PHP echo $costWebmaster; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-euro"></i> (Euro)                        <br>
                            </fieldset>
                            <fieldset style="clear:both;">
                                <label class="color" for="costReferenceur">Tarif R�ferenceur:</label>
                                <input name="costReferenceur" id="costReferenceur" class="search-field" value="<?PHP echo $costReferenceur; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-dollar"></i> (Dollar)                        <br>
                            </fieldset>
                            <?PHP
                        }
                        ?>
                        <div>
                            <br/>
                            <fieldset class="one-fourth" style="clear:both">
                                <label><span> </span>URL pour se connecter au serveur email: </label>
                                <input name="url" class="search-field" type="text" value='<?PHP echo $url; ?>' style="margin-bottom:5px;width:350px;" maxlength="300"><br/>
                            </fieldset>
                            <fieldset class="one-fourth" style="clear:both">
                                <label><span> </span>Identifiant (Adresse email): </label>
                                <input name="username" class="search-field" type="text" value='<?PHP echo $username; ?>' style="margin-bottom:5px;width:350px;" maxlength="300"><br/>
                            </fieldset>
                            <fieldset class="one-fourth" style="clear:both">
                                <label><span> </span>Mot de passe: </label>
                                <input name="password" class="search-field" type="text" value='<?PHP echo $password; ?>' style="margin-bottom:5px;width:350px;" maxlength="300"><br/>
                            </fieldset>
                        </div>
                        <br/>

                        <div class="cleared" style="font-size:14px;font-weight:bold;">
                            <br/>
                            <p>Estimation du co�t suppl�mentaire pour <span id="TotalAnnuaires"><?PHP echo $countAuthorizedChecked; ?></span> annuaire(s) : <span style='font-weight:bold;color:red;'> <span id="costTotal"><?PHP echo ($sum_to_pay); ?></span> <i class='icon-euro'></i></span></p>
                        </div>
                        <div id='listConcernee'>
                            <strong><u>Liste des annuaires concern�s</u></strong>
                            <p><?PHP echo $ContentAuthorizeAnnuaire; ?></p>
                        </div>
                        <div id='listNonConcernee'>
                            <strong><u>Liste des annuaires non concern�s</u></strong> (<i> Aucun compte ne peut �tre cr��, les soumissions ne pourront donc �tre reprises, aucun co�t suppl�mentaire n'est donc compliqu�.</i>)
                            <br/>
                            <br/>
                            <p style="font-weight:bold;"><?PHP echo $ContentNoAuthorizeAnnuaire; ?></p>
                        </div>

                    </div>

                    <div class="clear"> <br/><br/> </div>
                    <input type="submit" class="button color small round" value="Terminer" style="color:white;float:right;"/>
                    <?PHP
                }
                ?>
            </form>
            <!-- End pagination -->
        </div>

        <script>
            //            $(document).ready(function ($) {
            (function ($) {
                if ($('#possouiCompte').is(':checked')) {
                    $('#showUp').slideDown("slow");
                }
                //                })
            })(jQuery);

        </script>

    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>