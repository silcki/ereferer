<?PHP
@session_start();

$a = 1;
$page = 4;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">
        <div class="blog-post">
            <p>
            <h2>1. INFORMATIONS L�GALES</h2>

            Conform�ment aux dispositions des articles 6-III et 19 de la loi pour la Confiance dans l'�conomie Num�rique, nous vous informons que :<br/><br/>
            * Le directeur de la publication du site web est Higel Emmanuel.<br/>
            * Le prestataire assurant le stockage direct et permanent est :<br/>
            OVH<br/>
            SAS au capital de 5 000 000 &euro;<br/>
            RCS Roubaix ? Tourcoing 424 761 419 00045<br/>
            Code APE 6202A<br/>
            N� TVA : FR 22 424 761 419<br/>
            Si�ge social : 2 rue Kellermann - 59100 Roubaix - France.<br/>
            Directeur de la publication : Octave KLABA<br/><br/>
            * Le pr�sent site web est �dit� par EURL CADOMAX au capital de 500&euro;.<br/>
            SIRET 52281558800010<br/>
            RCS Nantes B 522 815 588<br/>
            Cordonn�es de contact:<br/>
            26 rue des ifs<br/>
            44240 La Chapelle sur erdre<br/>
            T�l�phone: 09 70 44 98 55<br/>
            <br/>
            <h2>2. LIMITATION DE RESPONSABILIT�</h2>

            L'utilisateur reconna�t avoir pris connaissance des pr�sentes conditions d'utilisation et s'engage &agrave; les respecter.<br/><br/>
            L'utilisateur du site internet reconna�t disposer de la comp�tence et des moyens n�cessaires pour acc�der et utiliser ce site.<br/><br/>
            L'utilisateur du site internet reconna�t avoir v�rifi� que la configuration informatique utilis�e ne contient aucun virus et qu'elle est en parfait �tat de fonctionnement.<br/><br/>
            Emmanuel Higel met tout en oeuvre pour offrir aux utilisateurs des informations et/ou des outils disponibles et v�rifi�s mais ne saurait �tre tenue pour responsable des erreurs, d'une absence de disponibilit� des fonctionnalit�s et/ou de la pr�sence de virus sur son site.<br/><br/>
            Les informations fournies par Emmanuel Higel le sont &agrave; titre indicatif et ne sauraient dispenser l'utilisateur d'une analyse compl�mentaire et personnalis�e.<br/><br/>
            Emmanuel Higel ne saurait garantir l'exactitude, la compl�tude, l'actualit� des informations diffus�es sur son site.<br/><br/>
            En cons�quence, l'utilisateur reconna�t utiliser ces informations sous sa responsabilit� exclusive.<br/><br/>
            <br/><br/>
            <h2>3. INFORMATIQUE ET LIBERT�S</h2>

            Les traitements automatis�s de donn�es nominatives r�alis�s &agrave; partir de ce site web ont �t� trait�s en conformit� avec les exigences de la loi n�78-17 du 6 janvier 1978 modifi�e, relative &agrave; l'informatique, aux fichiers et aux libert�s.<br/><br/>
            L'utilisateur est notamment inform� que conform�ment &agrave; l'article 32 de la loi n�78-17 du 6 janvier 1978 modifi�e, relative &agrave; l'informatique, aux fichiers et aux libert�s, les informations qu'il communique par le biais des formulaires pr�sents sur le site sont n�cessaires pour r�pondre &agrave; sa demande et sont destin�es &agrave; Emmanuel Higel, en tant que responsable du traitement &agrave; des fins de gestion administrative et commerciale.<br/><br/>
            L'utilisateur est inform� qu'il dispose d'un droit d'acc�s, d'interrogation et de rectification qui lui permet, le cas �ch�ant, de faire rectifier, compl�ter, mettre &agrave; jour, verrouiller ou effacer les donn�es personnelles le concernant qui sont inexactes, incompl�tes, �quivoques, p�rim�es ou dont la collecte, l'utilisation, la communication ou la conservation est interdite.<br/><br/>
            L'utilisateur dispose �galement d'un droit d'opposition au traitement de ses donn�es pour des motifs l�gitimes ainsi qu'un droit d'opposition &agrave; ce que ces donn�es soient utilis�es &agrave; des fins de prospection commerciale.<br/><br/>
            L'ensemble de ces droits s'exerce aupr�s de Emmanuel Higel par courrier accompagn� d'une copie d'un titre d'identit� comportant une signature par l'interm�diaire de notre formulaire de courrier �lectronique.<br/><br/>
            <br/><br/>
            <h2>4. COOKIES</h2>

            L'utilisateur est inform� que lors de ses visites sur le site, un cookie peut s'installer automatiquement sur son logiciel de navigation.<br/><br/>
            Le cookie est un bloc de donn�es qui ne permet pas d'identifier les utilisateurs mais sert &agrave; enregistrer des informations relatives &agrave; la navigation de celui-ci sur le site.<br/><br/>
            L'utilisateur dispose de l'ensemble des droits susvis�s s'agissant des donn�es &agrave; caract�re personnel communiqu�es par le biais des cookies dans les conditions indiqu�es ci-dessus.<br/><br/>
            Les utilisateurs du site internet sont tenus de respecter les dispositions de la loi n�78-17 du 6 janvier 1978 modifi�e, relative &agrave; l'informatique, aux fichiers et aux libert�s, dont la violation est passible de sanctions p�nales.<br/><br/>
            Ils doivent notamment s'abstenir, s'agissant des informations &agrave; caract�re personnel auxquelles ils acc�dent ou pourraient acc�der, de toute collecte, de toute utilisation d�tourn�e d'une mani�re g�n�rale, de tout acte susceptible de porter atteinte &agrave; la vie priv�e ou &agrave; la r�putation des personnes.<br/><br/>
            Nous faisons appel &agrave; des entreprises de publicit� tierces pour la diffusion d'annonces sur notre site Web. Ces entreprises peuvent utiliser les donn�es relatives &agrave; votre navigation sur notre site Web ou d'autres sites (&agrave; l'exception de vos nom, adresse postale, adresse e-mail ou num�ro de t�l�phone) afin de vous proposer des annonces de produits ou services adapt�es &agrave; vos centres d'int�r�t. Pour en savoir plus sur cette pratique ou sur la possibilit� d'interdire l'utilisation de ces donn�es par ces entreprises, <a href="http://www.google.com/privacy_ads.html" target="_blank">cliquez ici</a>.<br/><br/>
            <br/><br/>
            <h2>5. PROPRI�T� INTELLECTUELLE</h2>

            La structure g�n�rale ainsi que les logiciels, textes, images anim�es ou non, son savoir-faire et tous les autres �l�ments composant le site sont la propri�t� exclusive de Emmanuel Higel.<br/><br/>
            Toute repr�sentation totale ou partielle de ce site par quelle que personne que ce soit, sans l'autorisation expresse de Emmanuel Higel est interdite et constituerait une contrefa�on sanctionn�e par les articles L. 335-2 et suivants du Code de la propri�t� intellectuelle.<br/><br/>
            Il en est de m�me des bases de donn�es figurant, le cas �ch�ant sur le site web qui sont prot�g�es par les dispositions de la loi du 1er juillet 1998 portant transposition dans le Code de la propri�t� intellectuelle de la directive du 11 mars 1996 relative &agrave; la protection juridique des bases de donn�es, et dont Emmanuel Higel est producteur.<br/><br/>
            Les marques de Emmanuel Higel et de ses partenaires, ainsi que les logos figurant sur le site sont des marques d�pos�es.<br/><br/>
            Toute reproduction totale ou partielle de ces marques ou de ces logos effectu�s &agrave; partir des �l�ments du site sans l'autorisation expresse de Emmanuel Higel est donc prohib�e au sens du Code de la propri�t� intellectuelle.<br/><br/>
            Emmanuel Higel ne saurait �tre responsable de l'acc�s par les utilisateurs via les liens hypertextes mis en place dans le cadre du site internet en direction d'autres ressources pr�sentes sur le r�seau.<br/><br/>

            </p>
        </div>


        <!-- Pagination -->

        <!-- End pagination -->
    </div>

</div>
<?PHP
include("files/includes/bottomBas.php")
?>