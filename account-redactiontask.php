<?PHP
@session_start();

if (!isset($_GET['block']) || empty($_GET['block'])) {
    //header('location:' . $_SERVER['HTTP_REFERER']);
}

if (!isset($_GET['id']) || empty($_GET['id'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

$a = 40;
$b = 11;

$page = 1;

include_once('files/includes/topHaut.php');

// "models"
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/EchangesProposition.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/EchangeSites.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Partners.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Logger.php';

// helpers
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionViewHelper.php';

// validation
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionValidation.php';


$project_id = intval($_GET['id']);
$RedactionProjects = new RedactionProjects($dbh);
$reportsModel = new RedactionReports($dbh);
$project = $RedactionProjects->getProject($project_id);

$partnersModel = new Partners($dbh);


$is_modification = false;

if ( ($project['status']=='modification') || (($project['status']=='review')) ){
    $is_modification = true;
}

$viewHelper = new RedactionViewHelper();

// set action
$action = 'task.html';

// unset by default
if (  !isset($_SESSION['post_response']) ) {

    if (isset($_SESSION['redaction_task_text'])) {
        unset($_SESSION['redaction_task_text']);
    }

    if (isset($_SESSION['category'])) {
        unset($_SESSION['category']);
    }

    if (isset($_SESSION['meta_title'])) {
        unset($_SESSION['meta_title']);
    }

    if (isset($_SESSION['meta_desc'])) {
        unset($_SESSION['meta_desc']);
    }

    if (isset($_SESSION['redaction_task_errors'])) {
        unset($_SESSION['redaction_task_errors']);
    }

    if (isset($_SESSION['redaction_post_no_errors'])){
        unset($_SESSION['redaction_post_no_errors']);
    }

    if (isset($_SESSION['declined_conditions'])){
        unset($_SESSION['declined_conditions']);
    }

}


$is_postback = isset($_SESSION['post_response'])? true: false;


// check "is post"
if (isset($_SESSION['post_response'])){
    unset($_SESSION['post_response']);
}

// review task
if (isAdmin() || isSu() || isSuperReferer() || (isReferer() && $is_modification) ){
   
    $report = $reportsModel->getReport($project_id);

    $redactionValidation = new RedactionValidation($dbh);

    // Not override post-data from project`s report
    $admin_reject_post_action = false;
    if ($is_postback && !empty($report) && !empty($report['admin_comment']))
        $admin_reject_post_action = true;

    // modification is required!
    // Not override post-data from project`s report
    $writer_modify_action = false;

    if ($is_postback && (isReferer() && $is_modification))
        $writer_modify_action = true;

    // admin accessing
    $admin_access = false;
    if ($is_postback && (isAdmin() || isSu()) ){
        $admin_access = true;
    }


    if (!empty($report) && !$admin_access && !$admin_reject_post_action && !$writer_modify_action){
        $_SESSION['meta_title'] = $report['meta_title'];
        $_SESSION['meta_desc'] = $report['meta_desc'];
        $_SESSION['redaction_task_text'] = $report['text'];
        $_SESSION['image_sources'] = $report['image_sources'];
        $_SESSION['front_image'] = $report['front_image'];


        $report_data = unserialize($report['report']);

        // get declined conditions from report
        // if they exitst => project has errors
        if (isset($report_data['declined_conditions'])){
            $_SESSION['declined_conditions']= $report_data['declined_conditions'];

            // validate data from report for getting error-list
            $validate_data = array();
            $validate_data['meta_title'] = $report['meta_title'];
            $validate_data['meta_desc']  = $report['meta_desc'];
            $validate_data['text'] = $report['text'];
            $validate_data['project_id'] = $report['project_id'];
            $report_errors = $redactionValidation->validateText($validate_data,true);

            // set errors from report
            if (!empty($report_errors)){
                $_SESSION['redaction_task_errors'] = $report_errors;
            }
        }

    }
}

// using js for redirect - fucking code-architecture
if ( (isReferer() && ($idUser!=$project['affectedTO'] && !isSuperReferer()) || isWebmaster()) ) {
    echo "<script type='text/javascript'> document.location = '/'; </script>";
}

?>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="one">
        <script type="text/javascript">
            (function($){

                $(document).ready(function(){

                    tinyMCE.PluginManager.add('stylebuttons', function(editor, url) {
                        [ 'h1', 'h2', 'h3'].forEach(function(name){
                            editor.addButton("style-" + name, {
                                tooltip: "Toggle " + name,
                                text: name.toUpperCase(),
                                onClick: function() { editor.execCommand('mceToggleFormat', false, name); },
                                onPostRender: function() {
                                    var self = this, setup = function() {
                                        editor.formatter.formatChanged(name, function(state) {
                                            self.active(state);
                                        });
                                    };
                                    editor.formatter ? setup() : editor.on('init', setup);
                                }
                            })
                        });
                    });

                    tinymce.init({
                        mode: "specific_textareas",
                        selector: '#seo_text',
                        language_url : 'js/tinymce/fr_FR.js',
                        height: 600,
                        menubar: false,
                        statusbar: false,
                        entity_encoding : "raw",
                        mode : "exact",
                        entities: "",
                        plugins: [
                            'advlist autolink lists link image charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'media paste code stylebuttons'
                        ],
                        formats: {
                            bold : {inline : 'b' },
                        },
                        forced_root_block : false,
                        force_br_newlines : false,
                        convert_newlines_to_brs : false,
                        remove_linebreaks : false,
                        toolbar: 'undo redo | style-h1 style-h2 style-h3 bold | alignleft aligncenter alignright alignjustify | bullist | image link',
                        content_css: [
                        
                            '//www.tinymce.com/css/codepen.min.css'
                        ],
                        extended_valid_elements : "b/strong|title",
                        paste_auto_cleanup_on_paste : true,
                        paste_postprocess : function(pl, o) {
                            // remove extra line breaks
                            o.node.innerHTML = o.node.innerHTML.replace(/&nbsp;/ig, " ");
                        }
                    });

                    $("#check_text").click(function(e){
                        e.preventDefault();
                        $("input[name=checking]").val(1);

                        if(add_declined_texts('task_form')){
                           $("#task_form").submit();
                        }

                    });

                    $("#confirm").click(function(e){
                        e.preventDefault();
                        if (confirm('En �tes-vous s�r?')) {
                            if (add_declined_texts('task_form')) {
                                $("input[name=finishing]").val(1);
                                $("#task_form").submit();
                            }
                        }
                    });

                    // admin&super writer comment to modification
                    $("#modify_btn").click(function(){
                        $("#modify_block").show();
                    });

                    $("#modify_send_btn").click(function(e){
                        e.preventDefault();
                        var comment = $('#admin_comment').val();
                        if (!comment.length){
                            $('#admin_comment').addClass('error_input');
                        }else{
                            if (confirm('En �tes-vous s�r?')) {
                                if (add_declined_texts('admin_form')){
                                    $("#admin_form").submit();
                                }
                            }
                        }
                    });


                    // Rejection user`s conditions

                    $(".error_decline_btn").on('click',function(e){
                        var type = $(this).data('type');
                        $(this).next().show(); // show
                    });

                    $(".confirm_reject").on('click',function(e){
                        e.preventDefault();

                        var error_inputs = $(this).parents('.error_inputs');
                        var input = $(error_inputs).find('.error_input');
                        var text = $(input).val();

                        var modify_action = $(input).data('modify');
                        var declined_text =  $(error_inputs).find('.declined_text');

                        // modification existing text
                        if (modify_action){
                           $(declined_text).hide();
                           $(input).data('modify',false);
                           $(input).show();
                           $(this).text('OK');
                        }else{ // set declined text/state

                            $(input).removeClass('error_empty');

                            if (!text.length){
                                $(input).addClass('error_empty');
                                $(input).data('declined',false)
                            }else{
                                $(input).data('modify',true).hide();
                                $(input).data('declined',true);
                                $(declined_text).text(text).show();
                                $(this).parents('.error_decline_block').find('.error_header').addClass('error_declined');
                                $(this).text('Modifier');
                            }

                        }

                    });

                    $(".remove_reject").on('click',function(e){
                        e.preventDefault();

                        if (confirm('En �tes-vous s�r?')){
                            var error_inputs= $(this).parents('.error_inputs');
                            var input = $(error_inputs).find('.error_input');
                            var declined_text =  $(error_inputs).find('.declined_text');

                            $(this).prev().text('OK');
                            $(declined_text).text('').hide();
                            $(input).val('').removeData('declined').removeData('modify');
                            $(error_inputs).hide();
                            $(input).show();
                            $(this).parents('.error_decline_block').find('.error_header').removeClass('error_declined');
                        }
                    });

                    // add to form couplse reject-texts

                    function add_declined_texts(form_name){
                        var declined_items = $('.error_input');

                        console.log('add_declined_texts');
                        console.log(declined_items.length);

                        $.each(declined_items,function(i,item){

                            if ($(item).data('declined') && $(item).val().length){
                                type = escape_trim($(item).data('type'));
                                text = $(item).val();
                                hidden_input = '<input type="hidden" name="declined_conditions['+type+']" value="'+text+'" />';
                                console.log(hidden_input);
                                $('#'+form_name).append(hidden_input);
                            }
                        });
                        return true;
                    }

                    function escape_trim(str) {
                        str = str.replace(/^\s+/, '');
                        for (var i = str.length - 1; i >= 0; i--) {
                            if (/\S/.test(str.charAt(i))) {
                                str = str.substring(0, i + 1);
                                break;
                            }
                        }
                        return str;
                    }

                });
            })(jQuery);
        </script>

        <div class="redaction_task">
            <div><b>Nom du projet : </b><?php echo utf8_decode($project['title']);?></div>
            <div><b>Nombre de mots : </b><?php echo $project['words_count'];?></div>

            <?php if (!empty($project['article_title'])) { ?>
                <div><b>Titre de l'article : </b><?php echo utf8_decode($project['article_title']);?></div>
            <?php } ?>

            <?php if (!empty($project['instructions'])) { ?>
                <div><b>Consignes relatives � l'article : </b><br/>
                    <blockquote><pre><?php echo utf8_decode($project['instructions']);?></pre></blockquote>
                </div>
            <?php } ?>

            <div><b>Consignes du projet : </b><br/>
                <blockquote><pre><?php echo utf8_decode($project['desc']);?></pre></blockquote>
            </div>

            <div class="instructions">
                <b>Consignes : </b><br/>
                
               

                 <? if ($project['prop_id']) {  
                    $prop = Functions::getProp($project['prop_id']);
                    $site = Functions::getSite($prop['to_site_id']);
                    ?>
                    Article pour : <?=$site['url']?>
                    <?
                 } ?>

                  <?php $viewHelper->show_task_rules($project);?>

                <? if ($project['img_count']) { ?>
                <div class="rule"><?php echo utf8_decode($project['img_count']);?> image(s) minimum.</div>
                <? } ?>
               
                <? if ($project['links_array']) {  ?>
                 <b>Placer les liens suivant : </b><br/>
                <?
                    $las=unserialize($project['links_array']);
                   
                    foreach($las as $key=>$la){
              
                ?>

                <div class="rule">Ancre : <?=utf8_decode($la['anchor']) ?> Lien : <?=utf8_decode($la['url']) ?></div>

                <? }} ?>
            </div>

            <?php
                //var_dump($_SESSION['redaction_task_errors']);
                //var_dump($_SESSION['declined_conditions']);
            ?>

            <?php if (isset($_SESSION['redaction_task_errors']) ){?>
                <?php
                    // check if errors are empty. Related with declined conditions
                    $checking_errors_state = true;
                    $errors = $_SESSION['redaction_task_errors'];
                    $declined_conditions = isset($_SESSION['declined_conditions']) ? $_SESSION['declined_conditions'] : array();

                    if (!empty($declined_conditions)){
                        $errors_diff = array_diff_key($errors,$declined_conditions);

                        //var_dump($errors_diff);
                        // test
                        //$errors_diff = array();
                        if(empty($errors_diff)){
                            $checking_errors_state = false;
                            $_SESSION['redaction_post_no_errors'] = true;
                            $_SESSION['declined_conditions'] = $declined_conditions;
                        }
                    }

                    // test
                    //$_SESSION['redaction_post_no_errors'] = true;
                 ?>
             <div class="errors <?php echo (!$checking_errors_state)? 'errors_declined': '' ;?>">
                 <?php
                    $viewHelper->show_task_errors($project,$errors,$declined_conditions);
                 ?>
             </div>
            <?php } ?>

            <!-- Webmaster Comment -->
            <?php if($is_modification && !empty($report['webmaster_comment'])){ ?>
                <div id="writer_comment">
                    <h3>MESSAGE DU CLIENT:</h3>
                    <blockquote>
                        <?php echo nl2br($report['webmaster_comment']);?>
                    </blockquote>
                </div>
            <?php } ?>

            <!-- Admin Comment -->
            <?php if($is_modification && !empty($report['admin_comment']) && isReferer() ){ ?>
                <div id="writer_comment">
                    <h3>MESSAGE DE L'ADMIN:</h3>
                    <blockquote>
                        <?php echo nl2br($report['admin_comment']);?>
                    </blockquote>
                </div>
            <?php } ?>


            <div class="form_wrapper">
                <form action="./compte/redaction_post/<?php echo $action;?>" id="task_form" method="post" style="margin-top:0px;float:left;width:100%;">

                <?
                $categories = $partnersModel->isCategoriesSet($site['id']);
                $checkedCategories = $partnersModel->getCheckedCategory((int)$project_id);
                if ($partnersModel->isPluginSet($site['id']) && !empty($categories)) { ?>
                    <div class="front-image">
                        <b>Images � la une:</b>
                        <input type="text" name="front_image" value="<?php echo isset($_SESSION['front_image']) ? $_SESSION['front_image'] : ''; ?>"  style="margin-bottom:5px;width:350px;"/>
                    </div>
                    <div class="categories">
                        <b>Cat�gorie:</b>
                        <ul class="non-list-ul">
                            <?
                            foreach ($categories as $key=>$category) {
                                ?>
                                <li>
                                    <label for="lbl-<? echo $category ?>">
                                        <input type="checkbox" name="category[<? echo $key ?>]" id="lbl-<? echo $category ?>" value="<? echo $category ?>" <? if(in_array($key, $checkedCategories) || (isset($_SESSION['category']) && in_array($category, $_SESSION['category'])) ) echo 'checked'; ?>/>
                                        <? echo $category ?>
                                    </label>
                                </li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                <? } ?>

                    <input type="hidden" name="project_id" value="<?php echo $project_id;?>"/>

                    <?php if (!empty($project['meta_title'])) : ?>
                        <div class="meta_title">
                            <b><?php echo htmlentities('<title>');?></b><input type="text" name="meta_title" value="<?php echo isset($_SESSION['meta_title']) ? $_SESSION['meta_title'] : ''; ?>" maxlength="65"/> <b><?php echo htmlentities('</title>');?></b>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($project['meta_desc'])) : ?>
                        <div class="meta_title">
                            <b><?php echo htmlentities('<meta name="description" content="');?></b><input type="text" name="meta_desc" value="<?php echo isset($_SESSION['meta_desc']) ? $_SESSION['meta_desc'] : ''; ?>" maxlength="150"/><b>"/></b>
                        </div>
                    <?php endif; ?>

                     <textarea name="text" id="seo_text">
                        <?php
                            if (isset($_SESSION['redaction_task_text'])){
                                echo $_SESSION['redaction_task_text'];
                            }
                        ?></textarea>
                    <input type="hidden" name="checking" value="0" />
                    <input type="hidden" name="finishing" value="0" />

                    <?php /*
                    <?php if (isReferer() || isSuperReferer()){ ?>
                        <input type="hidden" name="modification_review" value="1" />
                    <?php } ?> */ ?>

                    <input type="hidden" name="project_id" value="<?php echo $project_id;?>" />
                    
                    <? if ($project['prop_id']) {  ?>
                    <br/>
                      Source des images (une URL par ligne) :
                      <textarea name="image_sources" row="3" style="width:98%"> <?php
                            if (isset($_SESSION['image_sources'])){
                                echo $_SESSION['image_sources'];
                            }
                        ?></textarea>
                    <? } ?>

                    <div class="action_buttons">
                        <input type="submit" class="button color big round" id="check_text" value="V�rifier le texte" />
                        <?php if (isset($_SESSION['redaction_post_no_errors']) && $is_postback){ ?>
                            <input type="submit" class="button color big round" id="confirm" value="Terminer" />
                        <?php } ?>

                        <?php if (isSuperReferer() || isSu()) : ?>
                             <div id="modify_btn">Je souhaite des modifications</div>
                        <?php endif; ?>

                    </div>

                    <div class="clearfix"></div>
                </form>
                    <!-- Admin & SuperWriter(s) Comment to Modification-->
                    <div id="modify_block">
                        <header>Afin que nous puissions apporter les corrections n�cessaires, merci de nous signaler ce que ne vous convient pas:</header>
                        <form action="./compte/redaction_post/task.html" id="admin_form" method="post" style="margin-top:0px;float:left;width:100%;">
                            <input type="hidden" name="project_id" value="<?php echo $project_id;?>" />
                            <input type="hidden" name="modification" value="1" />
                            <input type="hidden" name="admin_reject" value="1" />
                            <textarea name="comment" id="admin_comment"></textarea>
                            <div class="action_buttons">
                                <input type="submit" class="button color big round" id="modify_send_btn" value="Envoyer" />
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>

                    <div class="clearfix"></div>
                </div>
       </div>
    </div>
</div>
<?PHP
include("files/includes/bottomBas.php");
