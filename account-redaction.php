<?PHP
@session_start();

if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:/');
}

//if (!isset($_GET['id']) || empty($_GET['id']) && (isset($_GET['action']))) {
   // header('location:/');
//}

if (isset($_GET['id']))
    $project_id = intval($_GET['id']);

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

$a = 40;
$b = 2;

$page = 1;
include('files/includes/topHaut.php');

$edition = false;
if (isset($_GET['action']) && ($_GET['action']=='modify')){
    $edition = true;
}


//models
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';


$RedactionProjects = new RedactionProjects($dbh);
$client_templates = $RedactionProjects->getTemplates($idUser);

// "TESTS"
/*
$ids = array(28,36,37,38,41,42,43,44,45,47);
foreach($ids as $id)
    $RedactionProjects->finishProccessing($id,206,'finishing');
die;*/

//$RedactionProjects->finishProccessing(58,643,$step='finishing');
//die;

// check access

if ($edition){
    $project = $RedactionProjects->getProject($project_id);


    if ( (isWebmaster() && ($project['webmaster_id']==$idUser)) || (isSu() || isAdmin() || isSuperReferer() ) ){
        // decline
        if (isWebmaster() && ($project['status']!='waiting')) {
            Functions::redirectJS('/');
        }

    }else{
        Functions::redirectJS('/');
        //header('location:' . $_SERVER['HTTP_REFERER']); die;
    }
}


//$price_100_words = 1.3;

if (isWebmaster()){
    if (isset($_SESSION['connected']['tarif_redaction']) && !empty($_SESSION['connected']['tarif_redaction'])){
        $price_100_words = $_SESSION['connected']['tarif_redaction'];
    }
}else{
    $price_100_words = $RedactionProjects->getPriceForWordsByUserId($project['webmaster_id']);
}

if (!$price_100_words){
    $price_100_words = $RedactionProjects->getPriceForWords();
}

//var_dump($price_100_words);

?>

<!--breadcrumbs ends -->
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="files/js/jquery.serialize-object.js"></script>
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="one">

        <?php if (isWebmaster() && ($section=='waiting')){?>
            <?php
                $words_capacity = $_SESSION['allParameters']['delay_words_count']['valeur'];
                $days_starting = $RedactionProjects->getDaysCountWaiting($idUser,$words_capacity);
                if ($days_starting>=1){
            ?>
                <div class="notification warning">
                    D�lai d'attente actuellement estim� � <b><?php echo  $days_starting;?></b> jour(s).
                </div><br/>
            <?php } ?>
        <?php } ?>


        <script type="text/javascript">
            (function($){


                jQuery.noConflict();

                // THERE IS MUST BE ANGULAR... BUT TOO LATE...
                // TODO
                // REFACTORING ALL!!!!!!!!!!!

                // digits_input only inputs
                jQuery.fn.ForceNumericOnly =
                    function()
                    {
                        return this.each(function()
                        {
                            $(this).keydown(function(e)
                            {
                                var key = e.charCode || e.keyCode || 0;
                                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                                // home, end, period, and numpad decimal
                                return (
                                key == 8 ||
                                key == 9 ||
                                key == 13 ||
                                key == 46 ||
                                key == 110 ||
                                key == 190 ||
                                (key >= 35 && key <= 40) ||
                                (key >= 48 && key <= 57) ||
                                (key >= 96 && key <= 105));
                            });
                        });
                    };

                $(document).ready(function(){
                    var ajax_url = 'files/includes/ajax/update.php';
                    var ajax_data_url = 'files/includes/ajax/data.php';

                    var edition = <?php echo json_encode($edition);?>;
                    var edit_project_id = <?php echo json_encode($project_id);?>;
                    var amount_per_100 = parseFloat(<?php echo json_encode($price_100_words);?>);

                    // load project for edition

                    if (edition) {

                        var data = {
                            type: 'redaction_project_edition',
                            project_id: edit_project_id
                        };

                        $.post(ajax_data_url, data)
                            .done(function (response) {

                               <?php /* var response = <?php echo json_encode($project);?>; */?>

                                console.log(response);
                                clearForm();
                                setForm(response,true);
                                estimation();
                                // check showing related seo-data
                                show_seo_related_block();
                          })
                            .fail(function (error) {
                                console.log(error);
                            })
                            .always(function () {
                        });

                    }



                    // set digit input
                    $('.digits_input').ForceNumericOnly();

                    var validator = $("#creating_form").validate({
                        onsumbit: false,
                        invalidHandler: function(event, validator) {
                            var errors = validator.numberOfInvalids();
                            if (errors) {
                                $(".error_inputing").show();
                            } else {
                                $(".error_inputing").hide();
                            }
                        }
                    });

                    // text counts & words
                    $("#add_counts,.attention_btn").click(function(){
                        var texts_count = $("#texts_count").val();
                        var words_count = $("#words_count").val();

                        if (texts_count.length && words_count.length) {
                            addCounts(texts_count, words_count);
                        }

                        var counters_cnt = $("#counters .list div").length;
                        if (counters_cnt){
                            $(".attention_text").show();
                            // remove validation requiring
                            $("#texts_count").removeProp('required');
                            $("#words_count").removeProp('required');
                        }
                        estimation();
                    });

                    function addCounts(texts_count,words_count){
                        var $list =  $("#counters .list");
                        var list_row = '<div class="counts"><b>Nombre de textes: </b><span class="texts_count">'+texts_count+'</span> , <b>Nombre de mots par articles: </b><span class="words_count">'+words_count+'</span>  <i class="icon-remove-sign remove_counter"></i></div>';
                        $list.append(list_row);
                        $("#texts_count").val('');
                        $("#words_count").val('');
                    }

                    // removing seo word
                    $(document).on('click','.remove_counter',function(){
                        $(this).parent().remove();

                        var counters_cnt = $("#counters .list div").length;
                        if (!counters_cnt){
                            $(".attention_text").hide();
                            $("#texts_count").prop('required',true);
                            $("#words_count").prop('required',true);
                        }
                        estimation();
                    });

                    // updating estimation
                    $(document).on('blur','input[name=texts_count],input[name=words_count]',function(){
                        var parent = $(this).parents('.counters');
                        var texts_count = $(parent).find("input[name=texts_count]").val();
                        var words_count = $(parent).find("input[name=words_count]").val();
                        if (texts_count.length && words_count.length) {
                            estimation();
                            $(parent).find('.validate_counters').eq(0).removeData('click_confirmed');
                        }
                    });

                    $("#words_count").blur(function(){
                        caclulateSeoPercents();
                    });

                    $(".required_field").blur(function(){
                        var errors = validator.numberOfInvalids();
                        if (!errors){
                            $(".error_inputing").hide();
                        }
                    });

                    // SEO WORDS
                    // add seo expression
                    $("#add_seo_word").click(function(){
                       var word = $("#seo_word").val();
                       if (word.length){
                            $('.error_words_list').hide();
                            addWord(word);
                       }
                    });

                    function addWord(word){
                        var $list =  $(".seo_words .list");
                        var list_row = '<div class="word"><span>'+word+'</span>  <i class="icon-remove-sign remove_seo_word"></i></div>';
                        $list.append(list_row);
                        $("#seo_word").val('');
                    }

                    // removing seo word
                    $(document).on('click','.remove_seo_word',function(){
                         $(this).parent().remove();
                    });

                    // IMAGES
                    $("#add_image").click(function(){
                        var image_url = $("#image_url").val();
                        var image_alt = $("#image_alt").val();
                        if (image_url.length){
                            $('.error_images_list').hide();
                            addImage(image_url,image_alt);
                        }
                    });

                    function addImage(image_url,image_alt){
                        var $list =  $("#images .list");
                        var list_row = '<div class="image"><span class="url">'+image_url+'</span>';
                        if (image_alt.length){
                            list_row +=' (<b>ALT</b>: <span class="alt">'+image_alt+'</span> )';
                        }
                        list_row +='  <i class="icon-remove-sign remove_image"></i></div>';
                        $list.append(list_row);
                        $("#image_url").val('');
                        $("#image_alt").val('');
                    }

                    $(document).on('click','.remove_image',function(){
                        $(this).parent().remove();
                    });

                    // seo related
                    $('[data-check="1"]').on('click focusout',function(){
                        var checked_elements = $('[data-check="1"]');
                        var related_item = $(this).data('related');

                        if ($(this).is(':checkbox')){
                            var show_related = $(this).is(':checked');
                            console.log(show_related,' ',related_item);
                            if (show_related)
                                $('[data-seo_check="'+related_item+'"]').show();
                            else
                                $('[data-seo_check="'+related_item+'"]').hide();
                        }

                        if ($(this).is(':text')){
                            if ($(this).val().length){
                                $('[data-seo_check="'+related_item+'"]').show();
                            }else{
                                $('[data-seo_check="'+related_item+'"]').hide();
                            }
                        }

                        // check show related box
                        show_seo_related_block();
                    });

                    function show_seo_related_block(){
                        var related_items = $('[data-check="1"]');
                        var show_related = false;
                        $.each(related_items,function(i,item){
                            if ( ($(item).is(':checked')) || ($(this).is(':text') && $(this).val().length) ){
                                show_related = true;
                                return;
                            }
                        });
                        $(".seo_related").show(show_related);

                        var related_items = $('[data-check="1"]');

                        $.each(related_items,function(){
                            var related_item = $(this).data('related');

                            if ($(this).is(':checkbox')){
                                var show_related = $(this).is(':checked');
                                console.log(show_related,' ',related_item);
                                if (show_related)
                                    $('[data-seo_check="'+related_item+'"]').show();
                                else
                                    $('[data-seo_check="'+related_item+'"]').hide();
                            }

                            if ($(this).is(':text')){
                                if ($(this).val().length){
                                    $('[data-seo_check="'+related_item+'"]').show();
                                }else{
                                    $('[data-seo_check="'+related_item+'"]').hide();
                                }
                            }
                        });

                    }

                    // TODO
                    // REFACTORING
                    // unit in one
                    // template&creating projects

                    // save as template
                    $("#template_btn").click(function(){

                        $("#redaction_not_optimized_modal").data('action_btn','template_btn');
                        $("#redaction_not_optimized_modal").data('is_template', true);
                        $("#status_success").hide();
                        $("#status_error").hide();

                        var button = $(this);
                        // reject sending ajax
                        if($(button).data('ajax'))
                            return false;

                        if ($("#creating_form").valid()) {
                            // check ranges
                            if (!validateRanges())
                                return;

                            // check images & seo words
                            if (!validateLists())
                                return;

                            if (confirm('En �tes-vous s�r?')){
                                $(".error_inputing").hide();

                                if (checkNotOptimizedArticles())
                                    sendAjaxProject(button,true);
                            }
                        }else{
                            $(button).data('ajax',false);
                            $(".error_inputing").show();
                        }

                    });


                    // creating project
                    $("#submit_btn").click(function(e){
                        e.preventDefault();

                        $("#redaction_not_optimized_modal").data('action_btn','submit_btn');
                        $("#redaction_not_optimized_modal").data('is_template', false);
                        $("#status_success").hide();
                        $("#status_error").hide();
                        var button = $(this);

                        // reject sending ajax
                        if($(button).data('ajax'))
                            return false;

                        if ($("#creating_form").valid()) {

                            // check ranges
                            if (!validateRanges())
                                return;

                            // check images & seo words
                            if (!validateLists())
                                return;

                            $(".error_inputing").hide();
                            if (confirm('En �tes-vous s�r?')){
                                $(".solde_error").hide();
                                var amount = $('.estimation_total_sum').text();
                                var webmaster_id = $('input[name="webmaster_id"]').val();
                                var checking = false;

                                var data = {
                                    type : 'redaction_check_solde',
                                    amount: amount,
                                    webmaster_id: webmaster_id,
                                    edition: edition
                                };

                                // add project id
                                if (edition){
                                    data.project_id = edit_project_id;
                                }

                                //console.log(data);

                                $(button).data('ajax',true);
                                $.post(ajax_data_url,data)
                                    .done(function(response){
                                        checking = parseInt(response.checking);
                                        console.log(checking);
                                        if (checking){
                                            $(".solde_error").hide();

                                            if (checkNotOptimizedArticles())
                                                sendAjaxProject(button,false);

                                        }else{
                                            $(".solde_error").show();
                                        }
                                    })
                                    .fail(function(error){
                                        console.log(error);
                                    })
                                    .always(function(){
                                        $(button).data('ajax',false)
                                    });
                            }else{
                                $(button).data('ajax',false);
                            }
                        }else{
                            $(button).data('ajax',false)
                            $(".error_inputing").show();
                        }
                    });


                    function sendAjaxProject(button,is_template){
                        var form_data = prepareFormDataRequest(is_template,false);

                        // set single flag

                        var is_single = isSingleText();
                        form_data.is_single = is_single;


                        if (is_single) {
                            var article_index = 1;
                            article_input = $('#articles_data .article_title').eq(0);
                            parent = $(article_input).parent();

                            //article_block = $(article_input).parents('.articles_block');

                            article_title = $(article_input).val();
                            instructions = $(parent).find('[data-name="instructions"]').val();

                            form_data.article_title = article_title.replace(new RegExp('�', 'g'), 'oe');

                            form_data.instructions = instructions.replace(new RegExp('�', 'g'), 'oe');
                            form_data.optimized = 1;
                            form_data.words_count = $('#words_count').val();
                        }

                        // replace
                        form_data.title = form_data.title.replace(new RegExp('�', 'g'), 'oe');
                        form_data.desc = form_data.desc.replace(new RegExp('�', 'g'), 'oe');

                        var post_data = {
                            projects: JSON.stringify(ARTICLES),
                            form_data: form_data,
                            type: 'redaction_creating_project'
                        };

                        // POST

                        //console.log(post_data);
                        //return;

                        $.post(ajax_url, post_data)
                            .done(function (response) {
                                if (response == 'ok') {
                                    $("#status_success").css('display','inline-block').delay(2000).fadeOut('slow');
                                } else {
                                    $("#status_error").css('display','inline-block').delay(2000).fadeOut('slow');
                                }

                                console.log(response);
                            })
                            .fail(function (error) {
                                console.log(error);
                            })
                            .always(function(){
                                $(button).data('ajax',false);
                            });
                    }

                    // load template
                    $("#load_project_btn").click(function(){
                        var project_id =  parseInt($("select[name=id]").val());
                        var button = $(this);

                        // empty list
                        if (project_id==-1) return false;

                        // reject sending ajax
                        if(!$(button).data('ajax')){
                            $(button).data('ajax',true);
                        }else return false;

                        var data = {
                           type : 'redaction_project_template',
                           project_id: project_id
                        };

                        if (confirm('En �tes-vous s�r?')) {
                            $.post(ajax_data_url,data)
                                .done(function(response){
                                    clearForm();
                                    // @setForm - deprecated

                                    setFormExtented(response);
                                    estimation();


                                    // check showing related seo-data
                                    //show_seo_related_block();
                                })
                                .fail(function(error){
                                    console.log(error);
                                })
                                .always(function(){
                                    $(button).data('ajax',false);
                                });
                        }else{
                            $(button).data('ajax',false);
                        }
                    });

                    // for single ( edition -action )
                    // old code
                    function setForm(data){
                        console.log('setForm',data);


                        console.log('data =',data);



                        for(var key in data){
                            if (data.hasOwnProperty(key)){

                                // @deprecated field
                                // fix
                                if (key == 'texts_count')
                                    continue;

                                //console.log(key,' ',data[key]);
                                if (!$.isArray(data[key])){


                                   console.log(key,' ',data[key]);

                                    var input = $('#creating_form input[name="'+key+'"]');

                                    if ($(input).is(':checkbox')){

                                        if (data[key] == 'on'){
                                            checked = true;
                                        }else{
                                            checked = !!+data[key];
                                        }

                                        console.log(key,' ',checked,' ',data[key]);
                                        $(input).prop('checked',checked);
                                        if (checked){
                                            $(input).parent().show();
                                        }
                                    }

                                    if ($(input).is(':text')){
                                        $(input).val(data[key]);
                                    }

                                    if (key=='desc'){
                                        $("#desc").val(data[key]);
                                    }
                                }

                                if ($.isArray(data[key])){
                                    // list of seo words
                                    if (key=='words'){
                                        var words = data[key];
                                        for(var i=0;i<words.length;i++){
                                            word = words[i];
                                            addWord(word);
                                        }
                                    }
                                    // list of images
                                    if (key=='images'){
                                        var images = data[key];
                                        for(var i=0;i<images.length;i++){
                                            image_url = images[i].image_url;
                                            image_alt = images[i].alt;
                                            addImage(image_url,image_alt);
                                        }
                                    }
                                    // list of counts
                                    if (key=='counts'){
                                        var counts = data[key];
                                        // without listing
                                        if (counts.length == 1){
                                            texts_count = counts[0].texts_count;
                                            words_count = counts[0].words_count;
                                            $("#texts_count").val(texts_count);
                                            $("#words_count").val(words_count);
                                        }else{
                                            for(var i=0;i<counts.length;i++){
                                                texts_count = counts[i].texts_count;
                                                words_count = counts[i].words_count;
                                                addCounts(texts_count,words_count);
                                            }
                                        }
                                    }

                                }

                            }
                        }

                        // set 1 for edition
                        if(edition){

                             /*
                             $(article).find('.article_title').val(project.article_title);

                             // show/set article instructions if it`s not empty
                             if (project.instructions.length) {
                             $(article).find('.instructions_action').trigger('click');
                             $(article).find('textarea').val(project.instructions);
                             }*/

                            $("#texts_count").val(1).prop('disabled',true);

                            //return;
                            if (data.article_title.length){
                                $('.counters').eq(0).find('.validate_counters').trigger('click');// trigger validate
                                var article_block  = $('#creating_form .articles_block').eq(0);

                                $(article_block).find('.article_title').val(data.article_title);

                                // show/set article instructions if it`s not empty
                                if (data.instructions.length) {
                                    $(article_block).find('.instructions_action').trigger('click');
                                    $(article_block).find('textarea').val(data.instructions);
                                }

                            }
                        }
                    }


                    // REDACTION UPDATE
                    var ARTICLES_POST_NUMBER = 1;
                    var ARTICLES = [];
                    var ARTICLES_TEMPLATE = [];


                    function clearFormExtended(){

                        // clear-reset forms before loading
                        resetArticleForm();
                        //$("#articles_data").empty();
                        //$('.validate_counters').removeData('hash');

                        ARTICLES_POST_NUMBER = 1;
                        ARTICLES = [];
                        ARTICLES_TEMPLATE = [];

                    }

                    function setFormExtented(data){
                        console.log('setFormExtended');

                        // clear-reset forms before loading
                        resetArticleForm();
                        $("#articles_data").empty();
                        $('.validate_counters').removeData('hash');

                        ARTICLES_POST_NUMBER = 1;
                        ARTICLES = [];
                        ARTICLES_TEMPLATE = [];


                        // TODO
                        //

                        // is single
                        if (!data.multiple){
                           var project = data.project;
                           var common_title = project.title;
                           var common_desc = project.desc;
                           $('#title').val(common_title);
                           $('#desc').val(common_desc);

                           var texts_count = 1;
                           var words_count = project.words_count; // get words count from first cat`s child

                           var counters_index = 0; // first
                           // set counters
                           var counters_row = $('#creating_form .counters').eq(counters_index);

                            $(counters_row).find('#texts_count').val(texts_count);
                            $(counters_row).find('#words_count').val(words_count);

                            $(counters_row).find('.validate_counters').trigger('click');// trigger validate

                            // get current article block
                            var article_block = $('#creating_form .articles_block').eq(counters_index);

                            var article = $(article_block).find('.article').eq(0);

                            // set real project_id
                            $(article).data('project_id', project.id);
                            $(article).find('.article_title').val(project.article_title);

                            // show/set article instructions if it`s not empty
                            if (project.instructions.length) {
                                $(article).find('.instructions_action').trigger('click');
                                $(article).find('textarea').val(project.instructions);
                            }

                            // reset is_template
                            project.is_template = 0;
                            setForm(project);
                            $(".details").show();
                            //$("#optimize_action").trigger('click');
                        }else{
                            var counters_index = 0;

                            var categories = data.categories;
                            var category_cnt = Object.keys(categories.children).length;

                            // get/set common params
                            var first_category = categories.children[Object.keys(categories.children)[0]];
                            var first_project = first_category[0];

                            var common_title = first_project.title;
                            var common_desc = first_project.desc;
                            $('#title').val(common_title);
                            $('#desc').val(common_desc);


                            var project_index = 0;

                            //console.log('cats = ',categories);
                            $.each(categories.children, function (i, projects) {
                                var texts_count = projects.length;
                                var words_count = projects[0].words_count; // get words count from first cat`s child

                                // set counters
                                var counters_row = $('#creating_form .counters').eq(counters_index);

                                $(counters_row).find('#texts_count').val(texts_count);
                                $(counters_row).find('#words_count').val(words_count);

                                $(counters_row).find('.validate_counters').trigger('click');// trigger validate

                                // get current article block
                                var article_block = $('#creating_form .articles_block').eq(counters_index);



                                // set real project (s)
                                $.each(projects, function (i, project) {
                                    //console.log($(article_block).html());
                                    var article = $(article_block).find('.article').eq(i);

                                    // set real project_id
                                    $(article).data('project_id', project.id);
                                    $(article).find('.article_title').val(project.article_title);

                                    // show/set article instructions if it`s not empty
                                    if (project.instructions.length) {
                                        $(article).find('.instructions_action').trigger('click');
                                        $(article).find('textarea').val(project.instructions);
                                    }

                                    // reset is_template
                                    project.is_template = 0;
                                    ARTICLES_TEMPLATE.push(project);
                                    project_index++;
                                });

                                // add extra article block
                                if (counters_index < category_cnt) {
                                    $('#creating_form .attention_btn').eq(counters_index).trigger('click');
                                }
                                counters_index++;
                            });

                            // console
                            //console.log(' Template`s projects  = ',ARTICLES_TEMPLATE);

                            // trigger optimization
                            $("#optimize_action").trigger('click');
                        }
                    }

                    // generate project tree
                    function buildProjectTree(nodes){
                        var map = {}, node, roots = [];
                        for (var i = 0; i < nodes.length; i += 1) {
                            node = nodes[i];
                            node.children = [];
                            map[node.id] = i; // use map to look-up the parents
                            if (node.parent_id !== "0") {
                                nodes[map[node.parent_id]].children.push(node);
                            } else {
                                roots.push(node);
                            }
                        }
                        return roots;
                    }

                    function estimation(){
                        var counts_list =  $(".counters");
                        var amount = 0.0;

                        couple_amount = 0.0;
                        $.each(counts_list,function(i,item){
                            texts_count = $(item).find('input[name=texts_count]').val();
                            words_count = $(item).find('input[name=words_count]').val();

                            if (texts_count.length && words_count.length) {
                                console.log(texts_count, ' ', words_count);
                                couple_amount = (amount_per_100 * parseFloat(words_count) / 100) * parseFloat(texts_count);
                                amount += couple_amount;
                            }
                        });

                        // @deprecated
                        /*
                        if ($(counts_list).size()==0){

                            var texts_count = $("#texts_count").val();
                            var words_count = $("#words_count").val();
                            //con
                            if (texts_count.length && words_count.length) {
                                amount = (amount_per_100 * parseFloat(words_count) / 100) * parseFloat(texts_count);
                            }
                        }else{
                            console.log('many',' ',$(counts_list).size());
                            couple_amount = 0.0;

                            $.each(counts_list,function(i,item){
                                texts_count = $(item).find('.texts_count').text();
                                words_count = $(item).find('.words_count').text();
                                //console.log(texts_count,' ',words_count);
                                couple_amount = (amount_per_100*parseFloat(words_count)/100)*parseFloat(texts_count);
                                amount+=couple_amount;
                                //console.log(amount,' ',couple_amount);
                            });
                        }*/

                        amount = (amount).toFixed(2);
                        $(".estimation_total_sum").text(amount);
                    }

                    function clearForm(){
                        $("#creating_form").find('.list').empty();
                        $("#creating_form").find(':checkbox').prop('checked',false);
                        $("#creating_form").find(':text').val('');
                        $("#creating_form #desc").val('');
                        $(".error_inputing").hide();
                        validator.resetForm();
                    }

                    function prepareFormDataRequest(is_template,is_single){
                        var is_single = is_single || false;

                        var form = $("#creating_form");

                        // removing
                        $("#creating_form .temp_data").remove();

                        // save as template
                        if(is_template){
                            var hidden_row = '<input type="hidden" class="temp_data" name="is_template" value="1" />';
                            $(form).append(hidden_row);
                        }else{ // as project
                            var amount = $('.estimation_total_sum').text();
                            var hidden_row = '<input type="hidden" class="temp_data" name="amount" value="'+amount+'" />';
                            $(form).append(hidden_row);
                        }

                        if (edition){
                            var hidden_row = '<input type="hidden" class="temp_data" name="edition" value="1" />';
                            $(form).append(hidden_row);
                            var hidden_row = '<input type="hidden" class="temp_data" name="project_id" value="'+edit_project_id+'" />';
                            $(form).append(hidden_row);
                        }

                        // seo words
                            var seo_words = $('.seo_words .list .word span');
                            $.each(seo_words,function(i,item){
                                var word = $(item).text();
                                var hidden_row = '<input type="hidden" class="temp_data" name="seo_words[]" value="'+word+'" />';
                                $(form).append(hidden_row);
                            });

                            // images
                            var index = 0;
                            var images = $('#images .list .image');
                            $.each(images,function(i,item){
                                url = $(item).find('span.url').text();
                                alt = $(item).find('span.alt').text();
                                hidden_url = '<input type="hidden" class="temp_data" name="images['+index+'][image_url]" value="'+url+'" />';
                                hidden_alt = '<input type="hidden" class="temp_data" name="images['+index+'][alt]" value="'+alt+'" />';
                                $(form).append(hidden_url);
                                $(form).append(hidden_alt);
                                index++;
                        });


                        // counters
                        var index = 0;
                        var counters = $('#counters .list .counts');
                        $.each(counters,function(i,item){
                            texts_count = $(item).find("span.texts_count").text();
                            words_count = $(item).find("span.words_count").text();
                            hidden_texts = '<input type="hidden" class="temp_data" name="counts['+index+'][texts]" value="'+texts_count+'" />';
                            hidden_words = '<input type="hidden" class="temp_data" name="counts['+index+'][words]" value="'+words_count+'" />';
                            $(form).append(hidden_texts);
                            $(form).append(hidden_words);
                            index++;
                        });

                        return $(form).serializeObject();
                    }


                    $("input[name=seo_percent_start],input[name=seo_percent_end]").on('blur',function(){
                        console.warn('blur');
                        caclulateSeoPercents();
                    });


                    $("input[name=seo_percent_end]").on('focus',function(e){
                        e.preventDefault();
                        console.warn('focus');

                        //caclulateSeoPercents();
                    });

                    function caclulateSeoPercents(){
                        var seo_start = $("input[name=seo_percent_start]").val();
                        var seo_end = $("input[name=seo_percent_end]").val();

                        var words_count = $("#words_count").val();

                        //console.log(seo_start,' ',seo_end);

                        // start
                        if (words_count.length && seo_start.length){
                            val = parseInt(seo_start);
                            count = parseInt(words_count);
                            percent = (100*val/count).toFixed(0);
                            console.log('start=',percent);
                            $('#seo_percent_start').text(percent);
                        }else{
                            $('#seo_percent_start').text('X');
                        }

                        // end
                        if (words_count.length && seo_end.length){
                            val = parseInt(seo_end);
                            count = parseInt(words_count);
                            percent = (100*val/count).toFixed(0);
                            console.log('end=',percent);
                            $('#seo_percent_end').text(percent);
                        }else{
                            $('#seo_percent_end').text('X');
                        }
                    }

                    // REDACTION update
                    // re-generate
                    function refresh_optimization_picker(){
                        var blocks = $('.articles_block');

                        // reset problems
                        if (!edition)
                            resetArticleForm();

                        var is_single = false;
                        // check single text
                        if ( isSingleText()){
                            var is_single = true;
                            // hide hmtl-part for multiple
                            $("#optimization").hide();
                            $('.register_block').hide();
                        }


                        // is_template
                        var current_template_cnt = ARTICLES_TEMPLATE.length;
                        var is_template = (current_template_cnt > 0);


                        if (!is_single) {
                            // show hmtl-parts for multiple
                            $("#optimization").show();
                            $('.register_block').show();

                            var opti_select = '<select id="optimization_picker"  multiple="multiple">';
                            var statuses_block = '';

                            var group_index = 1;
                            var option_index = 0;

                            // multiple
                            $.each(blocks, function (i, block) {
                                texts = $(block).data('texts');
                                words = $(block).data('words');
                                hash = $(block).data('hash');
                                group_title = 'Les ' + texts + ' textes de ' + words + ' mots';


                                article_type_option = '<optgroup label="'+group_title+'" >';
                                statuses_group = '<div class="header">'+group_title+'</div><div class="group">';

                                opti_select += article_type_option;
                                articles = $(block).find('.article_title');
                                var article_index = 1;
                                $.each(articles, function (i, article) {
                                    // select`s options generation
                                    number = group_index+'-'+article_index;
                                    real_project_id = $(article).data('project_id');
                                    option = '<option data-project_id = "'+real_project_id+'" data-option_index="'+option_index+'" class="article_single" value="' + number + '">' + $(article).val() + '</option>';
                                    opti_select += option;

                                    // articles statuses generation

                                    status_text = '( Non optimis� )';
                                    status_class = 'not_optimized';

                                    // if is template - override standard behaviour
                                    if (is_template){

                                        //console.log('templates_projects =  ',ARTICLES_TEMPLATE);
                                        //console.log('index = '+option_index);
                                        //console.log(ARTICLES_TEMPLATE[0].optimized);
                                        //return;

                                        //console.log('project_index = ', option_index);
                                        if ( current_template_cnt > option_index) {
                                            if (ARTICLES_TEMPLATE[option_index].optimized > 0) {
                                                status_text = '( Optimis� )';
                                                status_class = 'optimized';
                                            }
                                        }
                                    }else{

                                    }

                                    article_status = '<div class="article_status">\
                                    <span class="title">'+$(article).val()+'</span> : <span data-article_index="'+option_index+'" class="'+status_class+'">'+status_text+'</span></div>';
                                    statuses_group  += article_status;


                                    article_index++;
                                    option_index++;
                                    if (option_index>0){
                                        return;
                                    }
                                });

                                statuses_group += '</div>';
                                statuses_block += statuses_group;

                                opti_select += '</optgroup>';
                                group_index++;
                            });

                            opti_select += '</select>';

                            //console.log(opti_select);
                            $("#optimization .container").html(opti_select);
                            $(".articles_statuses").html(statuses_block);
                        }

                        // define array-data for post
                        init_articles();
                    }


                    function init_articles(){
                        var articles = $("#articles_data [data-number]");
                        var webmaster_id =  $('input[name="webmaster_id"]').val();

                        ARTICLES = []; // reset articles

                        // is_template
                        var current_template_cnt = ARTICLES_TEMPLATE.length;
                        var is_template = (current_template_cnt > 0);

                        // @deprecated
                        if (isSingleText()){
                            // @deprecated
                        }else{

                            // generate array of "projects" based on generated select
                            var options = $('#optimization_picker').find('option');

                            $.each(options,function(i,option){
                                index = $(option).data('option_index');

                                data = {
                                    article_title: $(option).text(),
                                    optimized: 0,
                                    form_data: '',
                                    selected_id: index
                                };

                                //
                                if (is_template){
                                    data.form_data = ARTICLES_TEMPLATE[index];
                                }

                                if ($(option).hasClass('article_single')) {
                                   article_index = index;

                                   //article_index++;
                                   article_input = $('#articles_data .article_title').eq(article_index);
                                   parent = $(article_input).parent();

                                   article_block =  $(article_input).parents('.articles_block');
                                   article_block_index = $(article_block).data('block_index');
                                   article_title = $(article_input).val();

                                   instructions = $(parent).find('[data-name="instructions"]').val();
                                   data.instructions = instructions;

                                   data.words_count =  $('.counters').eq(article_block_index).find('#words_count').val();


                                   data.article_title = data.article_title.replace(new RegExp('�', 'g'), 'oe');
                                   data.instructions = data.article_title.replace(new RegExp('�', 'g'), 'oe');

                                   console.log('index = ', index);
                                   console.log('data = ', data);
                                }

                                ARTICLES[index] = data;
                            });

                        }
                    }

                    // check project with single text
                    function isSingleText(){
                        var blocks = $('.articles_block');
                        var check = ( (blocks.length==1) && (parseInt($(blocks).data('texts'))==1) );
                        if (!check){ // edition,single load
                            check = ( (blocks.length==0) && (parseInt($('#texts_count').val())==1)  );
                        }
                        return check;
                    }

                    /// add new text with new text_counts&words_counts
                    $("#creating_form").on('click','.validate_counters',function(){
                        $(this).data('click_confirmed',true).removeClass('button_error');

                        if (!edition){
                            $(".details").hide();
                        }

                        add_articles_block($(this));
                        estimation();
                        refresh_optimization_picker();
                    });


                    function add_articles_block(el){
                        var parent = $(el).parent();

                        var texts_elem = $(parent).find("#texts_count");
                        var words_elem = $(parent).find("#words_count");

                        $(texts_elem).removeClass('error');
                        $(words_elem).removeClass('error');

                        var texts_count = $(texts_elem).val();
                        var words_count = $(words_elem).val();

                        var article_block_index = $("#creating_form").find('.validate_counters').index(el);
                        console.log('block index =',article_block_index);


                        var articles_container = $('#articles_data');
                        //$(articles_container).empty();

                        if (texts_count.length && words_count.length && (texts_count > 0) && (words_count > 0)) {

                                // if edition - remove standart behaviour
                                //if (edition) return;

                                var refreshed_action = false;
                                var articles_block = '';
                                var existed_hash = '';

                                // prevent creating duplicate
                                if ($(el).data('hash')){
                                    console.log(el);
                                    console.log('refresh_action');
                                    //console.log($(el).data('hash'));
                                    refreshed_action = true;
                                    existed_hash = $(el).data('hash');

                                    // // suppress standard behaviour on edition
                                    if (!edition)
                                        $('[data-hash="'+$(el).data('hash')+'"]').empty();

                                    articles_container = $('[data-hash="'+$(el).data('hash')+'"]');
                                    $('[data-hash="'+$(el).data('hash')+'"]').data('texts',texts_count);
                                    $('[data-hash="'+$(el).data('hash')+'"]').data('words',words_count);

                                }

                                if (!refreshed_action) { // new article block
                                    // generate unique hash
                                    var hash_id = randomHash(5);
                                    $(el).data('hash', hash_id); // set button as related with articles block
                                    articles_block = '<div class="articles_block" data-block_index="'+article_block_index+'" data-hash="' + hash_id + '" data-texts="' + texts_count + '" data-words="' + words_count + '" >';
                                }else{
                                    $('[data-hash="'+existed_hash+'"]').data('texts',texts_count);
                                    $('[data-hash="'+existed_hash+'"]').data('words',words_count);
                                }

                                for (i = 1; i <= texts_count; i++) {
                                    // suppress standard behaviour on edition
                                    if (refreshed_action && edition) break;

                                    articles_block += add_article(articles_container, i, ARTICLES_POST_NUMBER);
                                    ARTICLES_POST_NUMBER++;
                                }


                                if (!refreshed_action) {
                                    articles_block += '</div>';
                                }

                                $(articles_container).append(articles_block);

                                // add form for extra texts&words
                                if (!refreshed_action && !edition)
                                    add_header_texs_words_form($('#articles_data'));
                        }else{

                            if (!texts_count.length || (texts_count <= 0) ){
                                $(texts_elem).addClass('error');
                            }

                            if (!words_count.length || (words_count <= 0) ){
                                $(words_elem).addClass('error');
                            }
                        }

                    }

                    function init_multiselect() {

                        $('#optimization_picker').multiselect({
                            enableClickableOptGroups: true,

                            buttonText: function (options, select) {
                                if (options.length === 0) {
                                    return 'Tous les textes � la fois';
                                }
                                else if (options.length > 1) {
                                    return 'Vous avez s�lectionn� '+options.length+' articles';
                                }
                                else if (options.length == 1) {
                                    var labels = [];
                                    options.each(function () {
                                        if ($(this).attr('label') !== undefined) {
                                            labels.push($(this).attr('label'));
                                        }
                                        else {
                                            labels.push($(this).html());
                                        }
                                    });
                                    return labels.join(', ') + '';
                                }
                            },
                            onChange: function (option, checked) {
                                console.log('projects = '+ARTICLES);

                                // set behaviour that user can pick only one option of group
                                var values = [];
                                var optGroupLabel = $(option).parent().attr('label');
                                $('#optimization_picker optGroup[label="'+optGroupLabel+'"] option').each(function() {
                                    if ($(this).val() !== option.val()) {
                                        values.push($(this).val());
                                    }
                                });
                                $('#optimization_picker').multiselect('deselect', values);

                                // check-load selected value(s)
                                var options = $('#optimization_picker option:selected');

                                var ids = [];
                                $(options).each(function(index, option){
                                    ids.push($(this).data('option_index'));
                                });

                                if (ids.length == 1){
                                    var single_id = ids[0];

                                    // check state for specified article
                                    //console.log('single = '+single_id);
                                    //console.log(ARTICLES[single_id]);
                                    if( !$.isEmptyObject(ARTICLES[single_id].form_data)){
                                        // get current project`s params
                                        var data = ARTICLES[single_id].form_data;
                                        resetArticleForm();
                                        setForm(data);
                                    }else{
                                        resetArticleForm();
                                    }
                                }else{
                                    resetArticleForm();
                                }

                            }

                        });
                    }

                    // test
                    // init_multiselect();

                    function checkValidateButtons(){
                        var buttons = $('.validate_counters');
                        var checking = true;

                        $.each(buttons,function(i,btn){
                            parent = $(btn).parent();

                            texts_elem = $(parent).find("#texts_count");
                            words_elem = $(parent).find("#words_count");

                            texts_count = $(texts_elem).val();
                            words_count = $(words_elem).val();

                            if (texts_count.length && words_count.length && (texts_count > 0) && (words_count > 0)) {
                                if (!$(btn).data('click_confirmed')){
                                    checking = false;
                                    $(btn).addClass('button_error');
                                }
                            }
                        });

                        return checking;
                    }


                    // show optimization block & validate project`s info
                    $("#optimize_action").click(function(){


                        if ($("#creating_form").valid()) {
                            if (!checkValidateButtons())
                                return;
                            else{
                                $(".error_inputing").hide();
                            }

                            $('.details').show();
                            refresh_optimization_picker();
                            init_multiselect();
                        }else{
                            // TODO
                            // ERROR
                        }
                    });


                    $(".register_article_data").on('click',function(){

                        // check ranges
                        if (!validateRanges())
                            return;

                        // check images & seo words
                        if (!validateLists())
                            return;



                        if (confirm('En �tes-vous s�r?')) {
                            var options = $('#optimization_picker option:selected');

                            var ids = [];
                            $(options).each(function(index, option){
                                ids.push($(this).data('option_index'));
                            });

                            if (ids.length>0){

                                var serialazed_data = prepareFormDataRequest(false,false);

                                for (i=0; i<ids.length;i++){
                                    number = ids[i];

                                    ARTICLES[number].form_data = serialazed_data;
                                    ARTICLES[number].optimized = 1; // set flag as optimized

                                    // change status-title
                                    status_row = $('[data-article_index="'+number+'"]');
                                    ///console.log($(status_row).text());
                                    $(status_row).removeClass('not_optimized').addClass('optimized').text('( Optimis� )');
                                }

                            }else{

                                var options = $('#optimization_picker option');

                                var ids = [];
                                $(options).each(function(index, option){
                                    ids.push($(this).data('option_index'));
                                });

                                var serialazed_data = prepareFormDataRequest(false,false);

                                for (i=0; i<ids.length;i++){
                                    number = ids[i];

                                    ARTICLES[number].form_data = serialazed_data;
                                    ARTICLES[number].optimized = 1; // set flag as optimized

                                    // change status-title
                                    status_row = $('[data-article_index="'+number+'"]');
                                    ///console.log($(status_row).text());
                                    $(status_row).removeClass('not_optimized').addClass('optimized').text('( Optimis� )');
                                }
                            }

                            // show success status
                            $('.register_success').css('display','inline-block').delay(1000).fadeOut('slow');
                        }

                    });


                    // saving all not optimized project`s
                    function fillProjectWithoutOptimization(is_template){
                        var options = $('#optimization_picker option');

                        var ids = [];
                        $(options).each(function(index, option){
                            ids.push($(this).data('option_index'));
                        });

                        var serialazed_data = prepareFormDataRequest(is_template,false);

                        for (i=0; i<ids.length;i++){
                            number = ids[i];

                            ARTICLES[number].form_data = serialazed_data;
                            //ARTICLES[number].optimized = 1; // set flag as optimized

                        }
                    }


                    // check if user try save multiple withou optimization
                    function checkWithOptimization(is_single){
                        // check optimized state(s)


                        if (is_single){
                            //  something is checked
                            var checked_inputs = $('.details :checked').map(function() {
                                return this.value;
                            }).get();

                            // seo words count
                            var words_list_cnt = $('.seo_words .list .word').length;
                            // images count
                            var images_list_cnt = $('#images .list .image').length;

                            // checking rages
                            var ranges =  $('[data-range_max]');

                            var not_zero_values_cnt = 0;
                            $.each(ranges,function(i,item){
                                min_val = parseInt($(item).val());
                                max_el = $(item).next();
                                max_val = parseInt($(max_el).val());

                                if (max_val > 0 || (min_val > 0)){
                                    not_zero_values_cnt++;
                                }

                            });

                            /*
                            console.log('checked ',checked_inputs.length);
                            console.log('words_list_cnt ',words_list_cnt);
                            console.log('images_list_cnt ',images_list_cnt);
                            console.log('not_zero_values_cnt ',not_zero_values_cnt);*/

                            return ( (checked_inputs.length == 0) &&
                                     (words_list_cnt == 0) &&
                                     (images_list_cnt == 0) &&
                                     (not_zero_values_cnt == 0) );

                        }else{

                            var options = $('#optimization_picker option');

                            var ids = [];
                            $(options).each(function(index, option){
                                ids.push($(this).data('option_index'));
                            });

                            var total_optimized = 0;

                            for (i=0; i<ids.length;i++){
                                number = ids[i];

                                if (ARTICLES[number].optimized > 0)
                                    total_optimized++;
                            }

                            return (total_optimized == ids.length);
                        }
                    }


                    // set default article-title for
                    //$(".register_article_data .title").text($('#optimization_picker').find('option:selected').text());

                    function resetArticleForm(){
                        $('#layout input[type=checkbox]').prop('checked',false);
                        $('#layout input[type=text]').val('');

                        $('#seo input[type=text]').val('');
                        $('#seo .list').empty();
                        $('#seo input[type=checkbox]').prop('checked',false);
                        $('#seo .seo_related').hide();

                        $('#images input[type=text]').val('');
                        $('#images .list').empty();
                    }

                    // show instructions for specified article-project
                    $("#articles_data").on('click','.instructions_action',function(){
                        $(this).parent().find('.instructions').show();
                    });


                    function add_extra_texs_words_form(element,format){
                        var format = format || '';

                        var form = '<fieldset class="counters" data-format="'+format+'">\
                            <div class="rows">\
                            <div class="row_counter">\
                            <label for="texts_count" class="text_count">Nombre de textes :</label>\
                            <input type="text" id="texts_count" name="texts_count" class="digits_input required_fields" required />\
                            </div>\
                            <div class="row_counter">\
                            <label for="words_count">Nombre de mots par articles :</label>\
                            <input type="text" id="words_count" name="words_count"  class="digits_input required_fields" required />\
                            </div>\
                            <div class="button color big round validate_counters">\
                            VALIDER\
                            </div>\
                            </div>\
                             <div class="clearfix"></div>\
                             </fieldset>';

                        $(element).after(form);
                    }

                    // add extra text&count form
                    function add_header_texs_words_form(el){

                        var form = '<div class="attention">\
                                        <i class="icon-plus plus_btn" id="add_counts"></i> <label class="attention_btn">Ajouter d\'autres textes d\'une longueur diff�rente</label>\
                                    </div>';
                        $(el).append(form);
                     }


                    $("#articles_data").on('click','.attention_btn',function(){
                        if (!$(this).data('disabled')){
                            var parent = $(this).parent();
                            add_extra_texs_words_form(parent);
                            var inner_text = $(this).text();
                            inner_text += ':';
                            $(this).text(inner_text);
                            $(this).data('disabled',true);
                        }
                    });


                    // add article-form
                    function add_article(element,number,post_number,text,instructions){
                        text = text || '';
                        instructions = instructions || '';

                        var fake_name = 'fake_articles['+number+']['+post_number+']';
                        if (edition){
                            fake_name = '';
                        }

                        var article_div = '<div class="article" >\
                        <label>Titre du texte N�'+number+' :</label><input type="text" data-number="'+post_number+'" data-name="article_title" name="'+fake_name+'" class="required_field article_title" value="'+text+'" required/>\
                        <div  class="instructions_action" >Ajouter une consigne sp�cifique � ce texte. <span class="optional">(facultatif)</span></div>\
                        <div class="clearfix"></div>\
                            <div class="instructions">\
                               <label for="desc"> Consignes sp�cifiques :</label>\
                               <textarea data-name="instructions" >'+instructions+'</textarea>\
                            </div>\
                        </div>';

                        return article_div;
                    }

                    // info-tooltips

                    var tags_description = {
                      'meta_title': "Il s'agit du titre de la page qui sera affich� en haut de votre navigateur et, g�n�ralement, par les moteurs de recherche sur leurs pages de r�sultats.",
                      'meta_desc' : "Les moteurs de recherche utilisent essentiellement cette tr�s courte description et la retranscrivent sur leurs pages de r�sultats. (Description d'une 20aine de mots)",
                      'h1' : "Il s'agit du titre principal de votre article. Ce titre est capital pour une bonne structure de votre article et pour une optimisation en terme de SEO. Cette balise est donc indispensable � tout article.",
                      'h2' : "Il s'agit d'un sous-titre � votre article et, par d�finition, � votre titre H1. Il est indispensable pour une bonne mise en forme si votre article est particuli�rement long.",
                      'h3' : "Il s'agit d'un sous-sous-titre � votre article ou d'un sous-titre � votre titre H3. Il est conseill� pour une bonne mise en forme si votre article est particuli�rement long.",
                    };

                    $("[data-type=html_tooltip]").on('mouseenter', function (e) {
                        var tag = $(this).data("tag");
                        var pos = $(this).position();
                        if (tag in tags_description){
                            var info = tags_description[tag];
                            $(".tooltip .tooltip_body p").text(info);
                            $(".tooltip").css({top: pos.top + 25, left: pos.left + 10}).fadeIn('fast');
                        }
                    }).on('mouseleave', function (e) {
                        $(".tooltip").hide();
                    });


                    // tool
                    function randomHash(L){
                        var s= '';
                        var randomchar=function(){
                            var n= Math.floor(Math.random()*62);
                            if(n<10) return n; //1-10
                            if(n<36) return String.fromCharCode(n+55); //A-Z
                            return String.fromCharCode(n+61); //a-z
                        }
                        while(s.length< L) s+= randomchar();
                        return s;
                    }

                    // validate images and seo-words lists
                    // if user have added
                    function validateLists(){
                        $(".error_list").hide();
                        $('.seo_inputs input[type=text]').removeClass('error');
                        $('.images_inputs input[type=text]').removeClass('error');

                        // check seo count words
                        var error_state = true;
                        var seo_words_max = parseInt($("input[name=seo_percent_end]").val());
                        var seo_words_min = parseInt($("input[name=seo_percent_start]").val());
                        var words_list_cnt = $('.seo_words .list .word').length;

                        seo_words_min = seo_words_min || 0;
                        seo_words_max = seo_words_max || 0;

                        if ( (seo_words_max > 0) && (words_list_cnt == 0) ){
                            error_state  = false;
                            $(".error_words_list").show();
                        }

                        // user add  item without setting range "from-to"

                        //console.log(words_list_cnt, ' ', seo_words_min, ' ', seo_words_max);
                        if ( ( words_list_cnt > 0 ) && ( ( seo_words_min == 0 ) && ( seo_words_max == 0) )){
                            $("input[name=seo_percent_end]").addClass('error');
                            $("input[name=seo_percent_start]").addClass('error');
                            error_state  = false;
                        }

                        // check images count
                        var images_min = parseInt($("input[name=images_count_from]").val());
                        var images_max = parseInt($("input[name=images_count_to]").val());

                        var images_list_cnt = $('#images .list .image').length;
                        images_min = images_min || 0;
                        images_max = images_max || 0;

                        if ( (images_max > 0) && (images_list_cnt == 0) ){
                            error_state  = false;
                            $(".error_images_list").show();
                        }

                        // user add  item without setting range "from-to"

                        // console.log(words_list_cnt, ' ', seo_words_min, ' ', seo_words_max);
                        if ( ( images_list_cnt > 0 ) && ( ( images_min == 0 ) && ( images_max == 0) )){
                            $("input[name=images_count_from]").addClass('error');
                            $("input[name=images_count_to]").addClass('error');
                            error_state  = false;
                        }

                        return error_state;
                    }

                    // validate H2,H3, seo_words, images_words count
                    //
                    function validateRanges(){
                        var min_ranges = $('[data-range_min]');
                        var errors_count = 0;
                        $(min_ranges).removeClass('error');
                        $('[data-range_max]').removeClass('error');

                        $.each(min_ranges,function(i,item){
                            min_val = parseInt($(item).val());
                            max_el = $(item).next();
                            max_val = parseInt($(max_el).val());

                            if (max_val<min_val){
                                $(item).addClass('error');
                                $(max_el).addClass('error');
                                errors_count++;
                            }
                        });

                        return (errors_count==0);
                    }

                    // MODAL

                    // check not optimized articles
                    // show hide/not
                    function checkNotOptimizedArticles(){
                        var action_checking = true;

                        var modal = $('#redaction_not_optimized_modal');
                        var modal_event = $(modal).data('event');

                        var table = $(modal).find('.items');
                        $(table).empty(); // clear

                        if (isSingleText()){
                            //console.log('single');

                            checking = checkWithOptimization(true);
                            if (checking) {
                                // show if hide element
                                $(modal).find('.single_header').show();
                                $(modal).find('.multiple_header').hide();
                                $(modal).find('.opt_block').hide();

                                article_input = $('#articles_data .article_title').eq(0);
                                parent = $(article_input).parent();
                                article_title = $(article_input).val();

                                tr_row =  '<tr>\
                                               <td class="title">'+article_title+'</td>\
                                               <td class="article_not_optimized">Non optimis�</td>\
                                           </tr>';
                                $(table).append(tr_row);


                                $("#redaction_not_optimized_modal").modal({
                                    escapeClose: false,
                                    clickClose: false,
                                    showClose: false
                                });

                                action_checking = false;
                            }


                        }else{ // multiple
                            checking = checkWithOptimization(false);

                            //console.log('checking = ',checking);

                            // if something isn`t optimized
                            if (!checking){

                                // show if hide element
                                $(modal).find('.single_header').hide();
                                $(modal).find('.multiple_header').show();
                                $(modal).find('.opt_block').show();

                                // get/set not optimized articles
                                var options = $('#optimization_picker option');

                                var ids = [];
                                $(options).each(function(index, option){
                                    ids.push($(this).data('option_index'));
                                });

                                var total_optimized = 0;

                                for (i=0; i<ids.length;i++){
                                    number = ids[i];
                                    tr_row = '';
                                    if (ARTICLES[number].optimized == 0){
                                        tr_row =  '<tr>\
                                                    <td class="title">'+ARTICLES[number].article_title+'</td>\
                                                    <td class="article_not_optimized">Non optimis�</td>\
                                                   </tr>';

                                        $(table).append(tr_row);
                                    }
                                }

                                $("#redaction_not_optimized_modal").modal({
                                    escapeClose: false,
                                    clickClose: false,
                                    showClose: false
                                });
                                action_checking = false;
                            }

                        }
                        ///console.log('action_checking',action_checking);
                        return action_checking;
                    }


                    $('.modal_not_optimized_btn').on('click',function(){
                        var continue_action = $(this).data('continue');
                        var action_btn = $("#redaction_not_optimized_modal").data('action_btn');
                        var is_template = $("#redaction_not_optimized_modal").data('is_template');

                        // if user confirm without optimization
                        // send ajax
                        if (continue_action){
                            sendAjaxProject($("#"+action_btn),is_template);
                        }
                        // scratch
                        $("#redaction_not_optimized_modal").hide();
                        $(document).find('.blocker').hide();
                        $(document).find('body').css('overflow','');
                    });



                });
            })(jQuery);
        </script>

        <div style="display:none;width:380px;" class="tooltip" >
            <div class="tooltip_header">Qu'est-ce que c'est?</div>
            <div class="tooltip_body">
                <p></p>
            </div>
        </div>


        <div class="three-fourth">
            <div class="one service_redaction_creating_project">


                <!-- Modal - Not Optimize Articles Errors -->
                <div class="modal" id="redaction_not_optimized_modal"  style="display:none;">
                    <div class="header">ATTENTION !</div>
                    <div class="list_items">
                        <div class="multiple_header"><b>Tous vos articles ne sont pas optimis�s : </b></div>
                        <div class="single_header" style="display:none;"><b>Votre article n'est pas optimis� : </b></div>
                        <table class="items"></table> <!-- Multiple Items -->
                        <div class="opt_block">
                            Pour enregistrer votre optimisation, pensez � cliquer sur le lien rouge
                            <span class="register">ENREGISTRER L'OPTIMISATION</span> situ� juste au dessus du bouton <b>"TERMINER"</b>
                        </div>

                    </div>
                    <div class="footer">
                        <p> �tes-vous s�r de vouloir continuer ? </p>
                        <div class="button color big submit_form modal_not_optimized_btn" data-continue="1">OUI</div>
                        <div class="button color big submit_form modal_not_optimized_btn" >NON</div>
                    </div>
                </div>


                <?php if (isWebmaster()){?>
                    <?php
                    $words_capacity = $_SESSION['allParameters']['delay_words_count']['valeur'];

                    $days_starting = $RedactionProjects->getDaysCountWaiting($idUser,$words_capacity);
                    if ($days_starting>=1){
                        ?>
                        <div class="notification warning">
                            D�lai d'attente actuellement estim� � <b><?php echo  $days_starting;?></b> jour(s).
                        </div><br/>
                    <?php } ?>
                <?php } ?>



                <form action="compte/redaction_post/<?PHP echo $_GET['block']; ?>.html" id="creating_form" method="post" style="margin-top:0px;float:left;">
                    <input type="hidden" name="type" value="redaction_creating_project" />
                    <input type="hidden" name="webmaster_id" value="<?php echo $idUser;?>" />



                    <div class="required_fields">
                        <fieldset>
                             <?php if (!empty($client_templates)) { ?>
								 <label for="id"> Charger un projet :</label>
								 <select name="id" class="select">
								 <?php
								   if (count($client_templates)>0){
									   foreach($client_templates as $option){?>
										<option value="<?php echo $option['id'];?>"><?php echo utf8_decode($option['title']);?></option>
								  <?php }
								   }else{
								 ?>
									   <option value="-1">Votre liste est vide</option>
								 <?php } ?>
								 </select>
								<div id="load_project_btn">Charger</div>
							<?php } ?>
                        </fieldset>


                        <fieldset>
                            <label for="title"> Nom du projet :</label>
                            <input type="text" id="title" class="required_field"  name="title" required/>
                        </fieldset>

                    </div>


                    <fieldset class="counters" data-format="">
                        <div class="rows">
                            <div class="row_counter">
                                <label for="texts_count" class="text_count">Nombre de textes :</label>
                                <input type="text" id="texts_count" name="texts_count" class="digits_input required_field" required />
                            </div>
                            <div class="row_counter">
                                <label for="words_count">Nombre de mots par articles :</label>
                                <input type="text" id="words_count" name="words_count"  class="digits_input required_field" required />
                            </div>
                            <div class="button color big round validate_counters">
                                VALIDER
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </fieldset>

                    <!-- Start Point for articles -->
                    <div id="articles_data" data-format=""></div>

                    <!--
                        <div class="attention">
                        <i class="icon-plus plus_btn" id="add_counts"></i> <label class="attention_btn">Ajouter d'autres textes d'une longueur diff�rente</label>
                        <div class="list"></div>
                        <div class="clearfix"></div>
                        <div class="attention_text">
                            (<b>Attention!</b> Si la taille diff�re sensiblement d'un lot � l'autre, vous risquez de vous retrouver en<br/>suroptimisation ou
                            sous-optimisation en compl�tant les crit�res ci-dessous. Il est alors plus prudent de<br/> cr�er un autre projet.)
                        </div>
                    </div>-->

                    <fieldset id="project_main_info">
                        <div class="desc_note">
                            <label for="desc"> Consignes g�n�rales du projet :</label>
                            <div class="note">Ces consignes s'appliqueront � tous les textes</div>
                        </div>
                        <textarea id="desc" name="desc" class="required_field" required ></textarea>
                    </fieldset>


                    <?php if (!$edition){ ?>
                        <div class="clearfix"></div>
                        <div class="error_inputing optimize_errors"  >Attention, un ou des champs obligatoires sont manquants!</div>
                        <div id="optimize_action" class="button color big round">
                            Passer � l'�tape 2 :<br/>
                            OPTIMISATION
                        </div>
                    <?php } ?>

                    <?php
                        $hide_details_part = false;

                        if (!$edition)
                            $hide_details_part = true;

                        // test
                        //$hide_details_part = false;
                    ?>
                    <div class="details" <?php if ($hide_details_part) { ?> style="display:none;" <?php } ?> >
                        <div class="details_header">
                            <h1>D�tails de votre projet</h1><span class="details_note">(tous les champs sont facultatifs)</span>
                        </div>

                        <?php if (!$edition){ ?>
                            <div id="optimization">
                                <label>Optimisation du projet:</label>

                                <div class="desc">
                                    <h4>ATTENTION !</h4>
                                    <p>Il est possible d'optimiser vos textes un par un ! Pour cela, s�lectionnez dans un premier temps un texte � optimiser dans la liste d�roulante,
                                        enregistrez votre optimisation, puis res�lectionnez un autre article dans la liste d�roulante, et ainsi de suite!</p>
                                </div>

                                <div class="container">
                                    <select id="optimization_picker" multiple="multiple"></select>
                                </div>

                                <div class="articles_statuses"></div>
                            </div>
                        <?php } ?>


                        <div id="layout">
                            <h3>1) La mise en page</h3>
                            <div class="row">
                                <!--<input type="hidden" name="meta_title" value="0" />-->
                                <label>Meta Title :</label> Afficher une balise meta title en en-t�te de l'article <input type="checkbox" data-check="1"  name="meta_title" data-related="seo_meta_title" />
                                <span class="what_is_this" data-type="html_tooltip" data-tag="meta_title"> (qu'est-ce que c'est?)</span>
                            </div>
                            <div class="row">
                                <!--<input type="hidden" name="meta_title" value="0" />-->
                                <label>Meta Description :</label> Ajouter une meta description en en-t�te de l'article <input type="checkbox" name="meta_desc" />
                                <span class="what_is_this" data-type="html_tooltip"  data-tag="meta_desc"> (qu'est-ce que c'est?)</span>
                            </div>
                            <div class="row">
                                <!--<input type="hidden" name="H1_set" value="0" />-->
                                <label>H1:</label> Utilisation d'une balise H1 <input type="checkbox" data-check="1" name="H1_set"  data-related="seo_H1_set" />
                                <span class="what_is_this" data-type="html_tooltip"  data-tag="h1"> (qu'est-ce que c'est?)</span>
                            </div>
                            <div class="row">
                                <label>H2:</label> Utilisation de <input type="text" data-check="1" class="digits_input"  name="H2_start" data-related="seo_H2_set" data-range_min="1" /> � <input class="digits_input" data-check="1" type="text" name="H2_end" data-related="seo_H2_set" data-range_max="1" /> balises H2 dans votre texte
                                <span class="what_is_this" data-type="html_tooltip"  data-tag="h2"> (qu'est-ce que c'est?)</span>
                            </div>
                            <div class="row">
                                <label>H3:</label> Utilisation de <input type="text" class="digits_input" data-check="1" name="H3_start" data-related="seo_H3_set" data-range_min="1" /> � <input class="digits_input" type="text" data-check="1" name="H3_end" data-related="seo_H3_set" data-range_max="1" /> balises H3 dans votre texte
                                <span class="what_is_this" data-type="html_tooltip"  data-tag="h3"> (qu'est-ce que c'est?)</span>
                            </div>
                            <div class="row">
                                <label>Mots en gros :</label> Mettre quelques expressions importantes en gras <input type="checkbox" name="bold_text" />
                            </div>
                            <div class="row">
                                <label>Puces :</label> Utilisation de puces dans votre texte (votre texte contiendra alors une lise d'�num�ration ) <input type="checkbox" name="UL_set" />
                            </div>
                        </div>

                        <div id="extra_project_settings" style="display: block;">

                            <div id="seo">
                                <h3>2) Optimisation SEO</h3>
                                <div class="row">
                                    <label>Mots et expressions pouvant �tre utilis�s dans votre texte :</label>
                                    <div class="seo_words">
                                        <div class="seo_words_input" ><input type="text" name="seo_word" id="seo_word"  />  <div class="button color big round add_button" id="add_seo_word">Valider le mot cl�</div> </div>
                                        <div class="error_list error_words_list">Attention! Vous devez ajouter des mots cl�s avant.</div>
                                        <div class="list"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="row seo_stats">
                                    <div class="seo_inputs">
                                        <i class="icon-circle-arrow-right"></i> Utiliser entre <input type="text" name="seo_percent_start" class="digits_input" data-range_min="1" /> et <input type="text" class="digits_input" name="seo_percent_end" data-range_max="1" /> expressions SEO par article
                                    </div>
                                    <div class="notes">
                                        <b>Remarque !</b> Vous pouvez, par exemple, demander l'utilisation de 10 � 15 expressions SEO par article et<br/> n'avoir renseign� que 5 expressions SEO. Dans ce cas, chaque expression reviendra 2 � 3 fois par article.
                                        <div class="explanation">
                                            <b>Votre taux de densit� sera alors entre <span id="seo_percent_start" style="padding:0px;" >X</span>% et <span style="padding:0px;" id="seo_percent_end">X</span>%</b> <span>(le taux de densit� id�al est compris entre 3% et 8%)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row seo_related">
                                    <label>Expression SEO dans le titre</label>
                                    <div data-seo_check="seo_meta_title">
                                        Une expression SEO doit �tre utilis�e dans la meta-title <input type="checkbox" name="seo_meta_title" />
                                    </div>
                                    <div data-seo_check="seo_H1_set">
                                        Une expression SEO doit �tre utilis�e dans la balise H1 <input type="checkbox" name="seo_H1_set" />
                                    </div>
                                    <div data-seo_check="seo_H2_set">
                                        Une expression SEO doit �tre utilis�e dans une balises H2 <input type="checkbox" name="seo_H2_set" />
                                    </div>
                                    <div data-seo_check="seo_H3_set">
                                        Une expression SEO doit �tre utilis�e dans une balises H3 <input type="checkbox" name="seo_H3_set" />
                                    </div>
                                </div>
                        </div>

                        <section id="images">
                            <h3>3) Les images</h3>
                            <div class="row">
                                <header><label>Images :</label> Url des images pouvant �tre utilis�es dans l'article </header>
                                <div>
                                    <div>
                                        <label>URL : </label><input type="text" name="image" class="image_url" id="image_url"  />
                                        <label>Attribut Alt : </label><input type="text" name="image_alt"  class="image_alt" id="image_alt" />
                                        <div class="button color big round add_button" id="add_image">Valider l'image</div>
                                    </div>
                                    <div class="error_list error_images_list">Attention! Vous devez ajouter des images au pr�alable.</div>
                                    <div class="list"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row image_stats">
                                <div class="images_inputs">
                                    <i class="icon-circle-arrow-right"></i>Afficher entre <input type="text" class="digits_input" name="images_count_from" data-range_min="1" /> et <input class="digits_input" type="text" name="images_count_to" data-range_max="1" /> images par article
                                </div>
                            </div>
                        </section>
                    </div>

                    <?php  if (!$edition) {?>
                        <div class="register_block">
                            <div class="register_article_data" data-number="">
                                Enregistrer l'optimisation
                            </div>
                            <div class="register_info">
                                Apr�s avoir cliqu� sur ce lien, vous pourrez toujours modifier ou<br/>optimiser d'autres articles.
                            </div>
                            <div class="notification success register_success" style="">
                                Enregistrement effectu�.
                            </div>
                        </div>
                    <?php } ?>

                    <footer>
                        <div>
                            <div class="error_inputing">Attention, un ou des champs obligatoires sont manquants!</div>
                        </div>
                        <div id="template_btn">Sauvegarder ce projet en tant que mod�le <span class="action_result"></span></div>
                        <input type="submit" id="submit_btn" class="button color big round" value="TERMINER" style="color:white;"/>
                        <div class="solde_error">Votre solde est insuffisant pour lancer ce projet</div>
                        <div class="clearfix"></div>
                    </footer>


                    <div class="notification success" id="status_success" style="display: none;">
                        Success Op�ration effectu�e avec succ�s.
                    </div>

                    <div class="notification error" id="status_error" style="display: none;">
                        Erreur: le projet n'a pas �t� cr��
                    </div>

                    <div id="estimation_box_fixed">ESTIMATION: &nbsp;<span class="estimation_total_sum">0.0</span> &euro;</div>
                </form>
            </div>
       </div>
    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>