<?PHP
@session_start();

$a = 1;
$b = 20;
$page = 1;
include('files/includes/topHaut.php');

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Backlinks.php';


if (!empty($_GET['page'])) {
    $page = intval($_GET['page']);
} else {
    $page = 1;
}

?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        ?>
        <div style="position:absolute;top:0px;left:0;margin-left:220px;z-index: 9999;">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
            }
            ?>
        </div>
        <div class="three-fourth">
            <br/>
            <br/>
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:700px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertActioncontent']; ?>
                    </p>
                </div>
            <?PHP } ?>

<!--            <h4>D�tails du site : <?PHP echo $getCommande['lien']; ?></h4>-->
            <?PHP
            $DEV_IP = '178.120.201.2';
            $before = microtime(true);

            list($links_ratio_writers,$count) = Backlinks::getWritersRatio($page,$pagination);

            //$count = count($links_ratio_writers);
            include("pages.php");
            ?>

            <table class="simple-table">
                <tr>
                    <th>
                        Annuaire
                    </th>
                    <th>
                        Writers
                    </th>
                </tr>
                <?php foreach($links_ratio_writers as $link_ratio): ?>
                <tr>
                    <td><?php //echo $link_ratio['id'];?>  <?php echo $link_ratio['link'];?></td>
                    <td>
                       <?php if (isset($link_ratio['writers']) && ($link_ratio['writers']>0)) {

                              usort($link_ratio['writers'],function($a,$b){
                                 if ($a['ratio'] == $b['ratio']) {
                                     return 0;
                                 }
                                 return ($a['ratio'] < $b['ratio']) ? 1 : -1;
                             });

                             foreach( $link_ratio['writers'] as $writer){
                                 $writer_name = ucfirst(strtolower($writer['first_name'])).' '.ucfirst(strtolower($writer['last_name']));

                                 // old code
                                 $styleTaux = 'font-weight:bold;font-size:12px;';
                                 if ($writer['ratio'] >= 80) {
                                    $styleTaux .="color:#008000;";
                                 } else if ($writer['ratio'] >= 65 && $writer['ratio'] < 80) {
                                     $styleTaux .="color:#CDCD0D";
                                 } else if ($writer['ratio'] >= 50 && $writer['ratio'] < 65) {
                                    $styleTaux .="color:orange";
                                 } else if ($writer['ratio'] > 0 && $writer['ratio'] < 50) {
                                    $styleTaux .="color:red";
                                 }?>

                                <?php echo $writer_name;?>
                                 <span style="<?php echo $styleTaux;?>"><?php echo $writer['ratio'];?> %</span>  &nbsp;(<?php echo 'f:'.$writer['total_found']; ?>,<?php echo 'nf:'.$writer['total_not_found']; ?>,<?php echo 'nfy:'.$writer['total_not_found_yet']; ?>)  <br/>
                            <?php
                             }
                       }?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>

            <?php

            include("pages.php");

            if ($nbre_result > pagination) {
                echo $list_page;
            }

            ?>
            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>