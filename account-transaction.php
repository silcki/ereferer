<?PHP
@session_start();

// The main menu is not displayed on the page without these magic numbers
$a = 1;
$b = 70;
$page = 1;
include('files/includes/topHaut.php');
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';

?>
    <div class="container">
        <div class="one">

            <?PHP
            include("files/includes/menu.php");
            if (!isset($_GET['id']) || empty($_GET['id'])) {
                $_GET['id'] = 0;
            }
            ?>
            <div style="position:absolute;top:0px;left:0;margin-left:220px;z-index: 10000;">
            </div>
            <div class="three-fourth">
            <?PHP
            $transaction = new Transaction($dbh, $idUser);
            $transactionItem = $transaction->getTransactionForTable();
            $count = count($transactionItem);

            $pagination = 10;     // maximum number of entries per page
            include("pages.php"); // for pagination

                ?>
                    <br>
                    <br>
                    <table class="simple-table">
                        <tbody>
                        <tr>
                            <th>
                                Date
                            </th>
                            <th>
                                Details
                            </th>
                            <th>
                                Debit
                            </th>
                            <th>
                                Credit
                            </th>
                            <th>
                                Solde
                            </th>
                        </tr>
                        <?php
                        if ($count > 0) {
//                            for ($i = 0; $i <= $count; $i++){
//                            $limit1 - first number on the page
//                            $limit2 - last number on the page
                            $limit1--; // The array starts with zero, and the existing pagination works starts with one
                            $limit2 =  ($count < $limit2) ? ($count-1) : $limit2;
                            while ($limit1 <= $limit2) {
                                ?>
                                <tr>
                                    <td>
                                        <?PHP echo $transactionItem[$limit1]['date']; ?>
                                    </td>
                                    <td>
                                        <?PHP echo $transactionItem[$limit1]['details']; ?>
                                    </td>
                                    <td>
                                        <?PHP echo $transactionItem[$limit1]['debit']; ?>
                                    </td>
                                    <td>
                                        <?PHP echo $transactionItem[$limit1]['credit']; ?>
                                    </td>
                                    <td>
                                        <?PHP echo $transactionItem[$limit1]['solde']; ?>
                                    </td>
                                </tr>
                                <?php
                                $limit1++;
                            }
                        ?>

                        </tbody>
                    </table>
            <?php
                //            <!-- Pagination -->
                if ($nbre_result > $pagination) {
                    echo $list_page;
                }
                ?>
            </div>
        <?php
            }
            // if no results to display
            else {
            ?>
            <div align="center" width="500" style="width:700px;margin:auto;">
                <h1>Aucun r�sultat � afficher.</h1>

            </div>

            <?PHP
            }
            ?>
        </div>
    </div>
<?PHP
//include("files/includes/bottomBas.php")
//?>