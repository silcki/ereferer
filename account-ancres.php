<?PHP
@session_start();

$a = 1;
$b = 1;
$page = 1;
include('files/includes/topHaut.php');
isRight('3');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        if (!isset($_GET['action']) || empty($_GET['action'])) {
            $_GET['action'] = "modification";
        } else {
            $_GET['action'] = "ajouter";
        }
//        echo $_GET['action'];

        if (isWebmaster()) {
            $getCommande = Functions::getCommande($_GET['id'], $idUser);

            // check accessing
            if ($idUser != $getCommande['proprietaire']){
                Functions::redirectJS('/');
            }
        }

        if (isAdmin() || isSu()) {
            $getCommande = Functions::getCommande($_GET['id'], 0, 1);
        }
        $idUserPropList = $getCommande['annuaire'];
        if (isSu() || isAdmin()) {
            $currentAnnuaireList = Functions::getOneAnnuaireListe($idUserPropList, 0);
        } else {
            $currentAnnuaireList = Functions::getOneAnnuaireListe($idUserPropList, $idUser);
        }

        if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
            $annuairesFromListNew = explode(";", $currentAnnuaireList['annuairesList']);
        } else {
            $annuairesFromListNew = array();
        }

//        print_r($annuairesFromListNew);

        if ($_GET['action'] == "modification") {
            
        }
        ?>

        <div class="three-fourth">
            <br/>
            <br/>
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:700px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertActioncontent']; ?>
                    </p>
                </div>
            <?PHP } ?>


            <?php

             $multiple = false;
             $domains = explode(PHP_EOL, $getCommande['lien']);

              if (count($domains)>1) $multiple=true;

              $json = '';
              // check edition and get temp ancres
              if ($multiple){
                if (isset($_GET['id'])){
                    $project_id = intval($_GET['id']);
                    $ancres = Functions::getTempAncres($project_id);
                    if (isset($ancres['json'])){
                        $json = $ancres['json'];
                    }
                }
              }
            ?>

            <?php if ($multiple){ ?>
            <script>
                (function($){
                    $(document).ready(function(){
                        var domains = [];

                        // set anchors on change domain
                        $("#multiple_domains").change(function(){
                            var current_domain = $(this).val();
                            console.log(current_domain);
                            var form_inputs = $('form.simple-table input[type=text]');
                            // get values for current domains
                            for (i=0;i<domains.length;i++){

                                if(domains[i].url == current_domain){
                                    var items = domains[i].items;

                                    $.each(form_inputs,function(i,item){
                                        annuaire_id = $(item).data('annuaire_id');
                                        anchor = '';
                                        exists = false;
                                        for (i=0;i<items.length;i++){
                                            if (items[i].annuaire_id == annuaire_id){
                                                anchor = items[i].anchor;
                                                exists = true;
                                            }
                                        }
                                        // on changing domain show|hide annuaire input|span
                                        if (exists) {
                                            var parent_div =  $(item).parent();

                                            console.log('found',anchor);
                                            parent_div.hide();
                                            parent_div.next().show();
                                            parent_div.next().find('span').eq(0).text(anchor);
                                            var buttons_div = $(parent_div).parent().next();

                                            $(buttons_div).find('.okAnnuaireProjet').hide();
                                            $(buttons_div).find('.editAnnuaireProjet').show();
                                        }else{
                                            $(item).val('');
                                            var parent_div =  $(item).parent();
                                            parent_div.show();
                                            parent_div.next().hide();
                                            var buttons_div = $(parent_div).parent().next();
                                            $(buttons_div).find('.okAnnuaireProjet').show();
                                            $(buttons_div).find('.editAnnuaireProjet').hide();
                                        }
                                    });
                                }
                            }

                            console.log(domains);

                        });


                        <?php if (!empty($json)){ ?>
                            var json = <?php echo json_encode($json);?>;
                            domains = JSON.parse(json);
                            $("#multiple_domains").change();
                        <?php } ?>

                        var multiple = <?php echo json_encode($multiple);?>;
                        $(".form_submit").click(function(e){
                            e.preventDefault();
                            if (multiple){
                                var json_data = JSON.stringify(domains);
                                //console.log(json_data);
                                $("input[name=multiple_domains_ancres]").val(json_data);
                            }
                            $('form.simple-table').submit();
                        });

                        <?php if (empty($json)){ ?>
                        // prepare domains
                        var domains_urls = $("#multiple_domains").find('option');
                        $.each(domains_urls,function(i,item){
                            var domain_url = $(item).val();
                            var domain = {
                                url: domain_url,
                                items: []
                            };
                            domains.push(domain);
                        });
                        <?php } ?>


                        $(".okAnnuaireProjet").click(function(e){
                            e.preventDefault();
                            var input = $(this).parent().prev().find('input[type=text]');

                            var anchor = $(input).val();
                            var project_id = $(input).data('project_id');
                            var annuaire_id = $(input).data('annuaire_id');
                            var user_id = $(input).data('user_id');

                            var current_domain = $("#multiple_domains").val();

                            // push new item
                            for (i=0;i<domains.length;i++){
                                if(domains[i].url == current_domain){

                                    // check for duplicate and remove
                                    for(j=0;j<domains[i].items.length;j++){
                                        item = domains[i].items[j];
                                        if (item.annuaire_id==annuaire_id){
                                            domains[i].items.splice(j,1);
                                        }
                                    }

                                    domains[i].items.push({
                                        anchor: anchor,
                                        project_id: project_id,
                                        user_id: user_id,
                                        annuaire_id:  annuaire_id
                                    });

                                }
                            }
                            //console.log(domains);
                        });

                        $(".editAnnuaireProjet").click(function(e){
                            var current_domain = $("#multiple_domains").val();
                            var form_inputs = $('form.simple-table input[type=text]');
                            var current_annuaire_id = $(this).data('annuaire_id');

                            // get values for current domains
                            for (i=0;i<domains.length;i++){
                                if(domains[i].url == current_domain) {
                                    var items = domains[i].items;
                                    // set anchor for modification for current couple annuaire/
                                    $.each(form_inputs,function(i,item) {
                                        annuaire_id = $(item).data('annuaire_id');
                                        if (annuaire_id == current_annuaire_id){
                                            for (i=0;i<items.length;i++){
                                                if (items[i].annuaire_id == current_annuaire_id){
                                                    anchor = items[i].anchor;
                                                    console.log(anchor);
                                                    $(item).val(anchor);
                                                    break;
                                                }
                                            }
                                        }
                                    });
                                }
                            }

                        });

                    });
                })(jQuery);
            </script>
            <?php } ?>

            <h4 style="display: inline-block;">Ancres du projet :
                <?php if ($multiple){?>
                    <select id="multiple_domains" style="display: inline-block;">
                        <?php foreach($domains as $domain):?>
                            <option name="<?php echo $domain;?>"><?php echo $domain;?></option>
                        <?php endforeach; ?>
                    </select>
                <?php } else {  ?>
                    <?PHP echo $getCommande['lien']; ?>
                <?php } ?>
                <?PHP
                if (isSu() || isAdmin()) {
                    echo " du webmaster ".Functions::getfullname($getCommande['proprietaire']);
                }
                ?>
            </h4>
            <?PHP
            $count = 0;
            $count = count($annuairesFromListNew);
            ?>

            <form class="simple-table" name="" action="./compte/<?PHP echo $_GET['action']; ?>/ancres.html?id=<?PHP echo $_GET['id']; ?>" method="POST">
                    <input type="submit" class="button form_submit color small round" value="Suivant >" style="color:white;float:right;"/>
                    <?php if ($multiple){?>
                        <input type="hidden" name="multiple_domains_ancres" value=""/>
                        <input type="hidden" name="project_id" value="<?php echo $_GET['id'];?>"/>
                    <?php } ?>
                    <?PHP
                                if ($count > 0) {
                    foreach ($annuairesFromListNew as $annuaire) {

                        if (!empty($annuaire)) {

                            $valueDeduite = "";
                            $thisAnnuaireInfos = Functions::getAnnuaire($annuaire);

                            if ($_GET['action'] == "modification") {
                                $getAncreContent = Functions::getAncre($_GET['id'], $annuaire);
                                if ($getAncreContent[$_GET['id']][$annuaire]["ancre"]) {
                                    $valueDeduite = ($getAncreContent[$_GET['id']][$annuaire]["ancre"]);
                                }
                            }
                            if (substr($thisAnnuaireInfos['annuaire'], -1) == "/") {
                                $thisAnnuaireInfos['annuaire'] = substr($thisAnnuaireInfos['annuaire'], 0, -1);
                            }
                            $thisAnnuaireInfos['annuaire'] = preg_replace("#http://#", "", $thisAnnuaireInfos['annuaire']);
                            ?>

                            <fieldset class="one cleared" style="border-bottom:1px solid #dcdcdc;width:100%;padding:5px 0 0 10px;margin-bottom:10px;">
                                <label style="">
                                    <?PHP echo $thisAnnuaireInfos['annuaire']; ?>  &nbsp;&nbsp; <span class="" style="color:black;font-size: 12px;"> <?php /*PR <?PHP echo $thisAnnuaireInfos['page_rank']; ?>, */?>TF <?PHP echo $thisAnnuaireInfos['tf']; ?> </span>
                                    <?PHP
                                    $noWayShowing = 0;
                                    if ($_GET['action'] == "modification" && Functions::getDoublon("jobs", "siteID", $_GET['id'], "annuaireID", intval($annuaire))) {
                                        $noWayShowing = 1;
                                        ?>
                                        <span class="tips" style="color:green !important;font-weight:bold;">Cette t�che a d�j� �t� ex�cut�e !</span> 
                                        <?PHP
                                    } else {
                                        if ($thisAnnuaireInfos['webmasterAncre'] == 0) {
                                            ?>
                                            <span class="tips" style="color:red;">L'ancre ne peut �tre definie pour cet annuaire!</span> 
                                            <?PHP
                                        }
                                    }
                                    ?>
                                </label>
                                <?PHP
                                if ($thisAnnuaireInfos['webmasterAncre'] == 1 && $noWayShowing == 0) {
//                                    echo $valueDeduite;
                                    ?>
                                    <div style="width:700px;float:left;">
                                        <div style="width:auto;float:left;">
                                            <div id="Edit<?PHP echo $_GET['id'] . "_" . $annuaire; ?>">
                                                <label style="float:left;color:black;font-weight:normal;">Votre ancre: &nbsp;</label>
                                                <input name="<?PHP echo $_GET['id'] . "_" . $annuaire . "_" . $getCommande['proprietaire']; ?>" id="content<?PHP echo $_GET['id'] . "_" . $annuaire; ?>" class="search-field" type="text" data-annuaire_id="<?php echo $annuaire;?>" data-project_id="<?php echo $_GET['id'];?>" data-user_id="<?php echo $getCommande['proprietaire'];?>" value="<?PHP echo $valueDeduite; ?>" style="width:350px;margin-right:5px;float:left;margin-left:0px;" charset="UTF-8">
                                            </div>  
                                            <div id="Edited<?PHP echo $_GET['id'] . "_" . $annuaire; ?>" style="display:none;width:auto;">
                                                <b> Ancre d�sir�e:</b>&nbsp; <span id="inputValue<?PHP echo $_GET['id'] . "_" . $annuaire; ?>"></span>
                                            </div>
                                        </div>  
                                        <div style="width:100px;margin-left:10px;float:left;">
                                            <button class="okAnnuaireProjet" id="ok<?PHP echo $_GET['id'] . "_" . $annuaire; ?>" data-annuaire_id="<?php echo $annuaire;?>" rel="<?PHP echo $_GET['id'] . "_" . $annuaire; ?>" onclick="return false;">ok</button>
                                            <button class="editAnnuaireProjet" id="editBtn<?PHP echo $_GET['id'] . "_" . $annuaire; ?>" data-annuaire_id="<?php echo $annuaire;?>"  rel="<?PHP echo $_GET['id'] . "_" . $annuaire; ?>" onclick="return false;" style="display:none;">Modifier</button>
                                        </div>
                                    </div>

                                    <?PHP
                                }
                                ?>
                                <p class="cleared"></p>
                                <?PHP
                                if (!empty($thisAnnuaireInfos['webmasterConsigne'])) {
                                    ?>
                                    <p style="font-weight:bold;color:red;font-size:13px;" id="Alert<?PHP echo $_GET['id'] . "_" . $annuaire; ?>">
                                        <?PHP echo $thisAnnuaireInfos['webmasterConsigne']; ?>
                                    </p>

                                    <?PHP
                                }
                                ?>
                            </fieldset>    

                            <?PHP
                        }
                    }
                    ?>
                    <div class="clear">  </div>
                    <input type="submit" class="button form_submit color small round" value="Suivant >" style="color:white;float:right;"/>
                    <?PHP
                } else {
                    ?>
                    <div align="center" width="500" style="width:700px;margin:auto;">
                        <h1>Aucun annuaire � afficher.</h1>

                    </div>

                    <?PHP
                }
                ?>
            </form>
            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>