<?PHP
@session_start();

$a = 1;
$b = 7;
$page = 1;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        ?>
        <div style="position:absolute;top:0px;left:0;margin-left:220px;z-index: 9999;">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
            }
            ?>
        </div>
        <div class="three-fourth">
            <br/>
            <br/>
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:700px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertActioncontent']; ?>
                    </p>
                </div>
            <?PHP } ?>

<!--            <h4>D�tails du site : <?PHP echo $getCommande['lien']; ?></h4>-->
            <?PHP

            $DEV_IP = '178.120.201.2';
            $before = microtime(true);

            if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                $annuaires = Functions::getAnnuaireList(1, '2015-03-20');
            } else {
                $annuaires = Functions::getAnnuaireList($idUser);
            }



            if ( ($_SERVER['REMOTE_ADDR']==$DEV_IP) || ($_SERVER['REMOTE_ADDR']=='127.0.0.1')  ){
                echo (microtime(true)-$before).'<br/>';
            }

            $count = 0;
            $count = count($annuaires);

            if (isset($_GET['by']) && !empty($_GET['by'])) {

                if ($_GET['by'] == 'temps') {

                    function cmp($a, $b) {
//                        return strcmp($a["temps"], $b["temps"]);
                        if ($a["temps"] == $b["temps"]) {
                            return 0;
                        }
                        return ($a["temps"] < $b["temps"]) ? -1 : 1;
                    }

                }

                if ($_GET['by'] == 'taux') {

                    function cmp($a, $b) {
                        if ($a["taux"] == $b["taux"]) {
                            return 0;
                        }
                        return ($a["taux"] < $b["taux"]) ? -1 : 1;
                    }

                }
                usort($annuaires, "cmp");
                $annuaires = array_reverse($annuaires);
            }

            include("pages.php");


            if ($count > 0) {
                ?>
                <a href="compte/ajout/ajouterannuaire.html" class="button color small round" style="color:white;float:right;margin:5px 0px 5px 10px;">Ajouter un annuaire</a>
                <a href="compte/importer.html" class="button color small round" style="color:white;float:right;margin:5px 0px 5px 10px;">Importer annuaires</a>
                <a href="compte/importer.html?type=excel" class="button color small round" style="color:white;float:right;margin:5px 0px 5px 10px;">Importer Excel</a>

                <table class="simple-table">
                    <tbody>
                        <tr>
                            <th>
                                Annuaire
                            </th>
                            <?PHP if (isAdmin() || isSu()) { ?>
                                <th>
                                    <a href="./compte/annuaires.html?by=temps" title="Organiser par temps" style="color:white !important;"> Temps </a>
                                </th>
                                <th>
                                    <a href="./compte/annuaires.html?by=taux" title="organiser par taux" style="color:white !important;"> Taux </a>
                                </th>
                            <?PHP } else { ?>
                            <?PHP } ?>
                            <th>
                                Ajout
                            </th>


                            <th>
                                Actions
                            </th>


                        </tr>
                        <?PHP

                        while ($limit1 <= $limit2) {
                            if ($annuaires[$limit1]['annuaire'] != "") {
                                $styleTemps = 'font-weight:bold;';

                                $styleTaux = 'font-weight:bold;font-size:12px;';
                                if ($annuaires[$limit1]['taux'] >= 80) {
                                    $styleTaux .="color:#008000;";
                                } else if ($annuaires[$limit1]['taux'] >= 65 && $annuaires[$limit1]['taux'] < 80) {
                                    $styleTaux .="color:#CDCD0D";
                                } else if ($annuaires[$limit1]['taux'] >= 50 && $annuaires[$limit1]['taux'] < 65) {
                                    $styleTaux .="color:orange";
                                } else if ($annuaires[$limit1]['taux'] > 0 && $annuaires[$limit1]['taux'] < 50) {
                                    $styleTaux .="color:red";
                                }
                                ?>
                                <tr id="annuaire<?PHP echo $annuaires[$limit1]['id']; ?>">
                                    <td style="<?PHP echo $styleTaux; ?>">
                                        <?PHP echo $annuaires[$limit1]['annuaire']; ?>
                                    </td>

                                    <?PHP if (isAdmin() || isSu()) { ?>
                                        <td style="<?PHP echo $styleTemps; ?>">
                                            <?PHP
                                            echo $annuaires[$limit1]['temps'] . " jour(s)";
                                            if ($annuaires[$limit1]['temps'] != $annuaires[$limit1]['tv']) {
                                                $dbh->query('UPDATE annuaires SET tv = "'.$annuaires[$limit1]['temps'].'" WHERE id='.$annuaires[$limit1]['id']);
                                            }
                                            ?>
                                        </td>
                                        <td style="width:80px;font-weight:bold;">
                                            <span style="<?PHP echo $styleTaux; ?>">Taux: <?PHP echo $annuaires[$limit1]['taux'] . " %"; ?></span><br/>
                                            <span style="font-size:11px;font-weight:normal;">
                                                <span style="">Found: </span><?PHP echo $annuaires[$limit1]['reussi']; ?><br/>
                                                <span style="">Not Found: </span><?PHP echo $annuaires[$limit1]['echoue']; ?><br/>
                                                <span style="">Not Yet: </span><?PHP echo $annuaires[$limit1]['echoueyet']; ?>
                                            </span>
                                            <?PHP
                                            if ($annuaires[$limit1]['taux'] != $annuaires[$limit1]['tav']) {
                                                $dbh->query('UPDATE annuaires SET tav = "'.$annuaires[$limit1]['taux'].'" WHERE id='.$annuaires[$limit1]['id']);
                                            }
                                            ?>
                                        </td>
                                                            <!--                                    <td>
                                        <?PHP
                                        if ($annuaires[$limit1]['importedBy'] == 0 || $annuaires[$limit1]['importedBy'] == 1) {
                                            echo $_SESSION['tampon']['logoText'];
                                        } else {
                                            echo Functions::getfullname($annuaires[$limit1]['importedBy']);
                                        }
                                        ?>
                                                                                        </td>-->
                                    <?PHP } else { ?>
                                    <?PHP } ?>
                                    <td style="width:65px;">
                                        <?PHP echo date("d-m-Y", strtotime($annuaires[$limit1]['created'])); ?>
                                    </td>


                                    <td style="width:60px">
                                        <a href="compte/modifier/annuaire.html?id=<?PHP echo $annuaires[$limit1]['id']; ?>"  title="Modifier cet annuaire" ><i class="icon-edit"></i> Edit</a><br/>
                                        <a href="compte/supprimer/annuaire.html?id=<?PHP echo $annuaires[$limit1]['id']; ?>" onclick="return confirmation();" title="Supprimer cet annuaire" ><i class="icon-trash"></i> Delete</a><br/>
                                        <?PHP if ($annuaires[$limit1]['active'] == 1) { ?>
                                            <a href="compte/modification/desactiverannuaire.html?id=<?PHP echo $annuaires[$limit1]['id']; ?>" onclick="return confirmation();" title="D�sactiver cet annuaire" style="color:red"><i class="icon-unchecked"></i> Disable</a>
                                        <?PHP } else { ?>
                                            <a href="compte/modification/activerannuaire.html?id=<?PHP echo $annuaires[$limit1]['id']; ?>" onclick="return confirmation();" title="Activer cet annuaire" style="color:green;"><i class="icon-check-sign"></i> Activer</a>
                                        <?PHP } ?>
                                    </td>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--                            <td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <a href="" ></a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </td>-->

                                </tr>
                                <?PHP
                            }
                            $limit1++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- Pagination -->
                <?PHP

                if ( ($_SERVER['REMOTE_ADDR']==$DEV_IP) || ($_SERVER['REMOTE_ADDR']=='127.0.0.1')  ){
                    echo (microtime(true)-$before).'<br/>';
                }


                if ($nbre_result > pagination) {
                    echo $list_page;
                }
            } else {
                ?>
                <div align="center" width="500" style="width:700px;margin:auto;">
                    <h1>Aucun r�sultat � afficher.</h1>

                </div>

                <?PHP
            }
            ?>
            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>