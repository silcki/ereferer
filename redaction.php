<?PHP
@session_start();

$a = 1;
$page = 5;
include('files/includes/topHaut.php');
$requete = "SELECT valeur FROM parametres WHERE id='26'";
$execution = $dbh->query($requete);
$retour = $execution->fetch();
$remunu = $retour['valeur'];
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <p style="font-size:15px;">
		<h1>Ereferer: La plateforme de r�daction N�1</h1>
            
<h3>Pour 1�65 les 100 mots, Ereferer vous propose :</h3>
- <b>La mise � disposition des meilleurs r�dacteurs</b> de la plateforme pour vous proposer des textes et articles pour votre site internet. Ereferer ne travaille qu'avec les r�dacteurs ayant re�u plus de 85% d'appr�ciations positives.<br/>
- <b>Des articles relus et, si besoin, corrig�s.</b> <br/>
- <b>Des articles optimis�s comme il vous si�ra ! </b>En effet, vous pourrez d�finir le nombre de titres H1, H2, H3, vos mots cl�s et leur densit� dans le texte, le nombre de mots dans le texte, etc. Notre logiciel analysera le texte du r�dacteur et ce dernier ne pourra l'envoyer que lorsque tous vos crit�res sont pr�sents ! <br/>
- <b>La mise en page des articles</b> avec notamment l'insertion de vos images.<br/>
- <b>La possibilit� de r�cup�rer le code source</b> pour une publication rapide sur des CMS comme Wordpress<br/><br/>


<P ALIGN=CENTER>
Exemple d'un article re�u par le client :<br/>
Capture d'�cran de l'interface client lors de la r�ception de l'article command�<br/>
    <a target="_blank" href="http://www.ereferer.fr/files/images/ex-redac.jpg">
<img src="http://www.ereferer.fr/files/images/ex-redac-petit.jpg" alt="screenshot module r�daction ereferer" />
    </a><br/><i>Cliquez pour agrandir !</i><br/>
</P>
<br/><b>Autres exemples :</b><br/>
- <a target="_blank" href="http://www.ereferer.fr/files/images/ex-redac2.png">Exemple de r�daction num�ro 2</a><br/>
- <a target="_blank" href="http://www.ereferer.fr/files/images/ex-redac3.png">Exemple de r�daction n�3</a><br/>
- <a target="_blank" href="http://www.ereferer.fr/files/images/ex-redac4.JPG">Exemple 4</a><br/>
- <a target="_blank" href="http://www.ereferer.fr/files/images/ex-redac5.JPG">Exemple 5</a><br/><br/>






<h2>Des articles SEO-optimis�s suivant vos crit�res</h2>
Comme vous pouvez le constater sur l'image ci-dessus, les articles r�pondent exactement � vos crit�res d'optimisations d�finis lors de la cr�ation de votre projet. En effet, les r�dacteurs ne peuvent envoyer leur article que lorsque tous les crit�res sont au vert.<br/><br/>

<h2>Qu'en est-il de la qualit� des articles ?</h2>
Comme cit� plus haut, seuls les meilleurs r�dacteurs sont conserv�s. Par "meilleurs r�dacteurs", nous entendons ceux ayant re�u plus de 85% d'appr�ciations positives de la part des clients.
D'autre part, les textes sont relus et corrig�s par un relecteur avant de vous �tre envoy�s. Ce dernier est �quipe d'Antidote, le meilleur logiciel de correction grammaticale, afin d'�tre vraiment s�re qu'aucune faute ne passe !

        </p>
        <!-- Pagination -->

        <!-- End pagination -->

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>