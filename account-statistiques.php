<?PHP
@session_start();

$a = 5;

$page = 1;
include('files/includes/topHaut.php');

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';

if (isSu() || isSuperReferer()) {
    if (!isset($_GET['id']) || empty($_GET['id'])) {
        $_GET['id'] = 0;
    }


    $start = microtime(true);


    isRight("4");
    $months = array('', 'Janv', 'F�vr', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Ao�t', 'Sept', 'Oct', 'Nov', 'D�c');
    $monthsLong = array('', 'Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre', 'D�cembre');

    // "FR_EN mirror" for months
    $months_FR_TO_ENG = array(
        'Janv' => 'Jan',
        'F�vr' => 'Feb',
        'Mars' => 'Mar',
        'Avr'  => 'Apr',
        'Mai'  => 'May',
        'Juin' => 'Jun',
        'Juil' => 'Jul',
        'Ao�t' => 'Aug',
        'Sept' => 'Sep',
        'Oct'  => 'Oct',
        'Nov'  => 'Nov',
        'D�c'  => 'Dec'
    );



    ?>
    <style>
        #container{
            width:1200px;
        }
        .container{
            margin-left:30px;
        }
        #main-navigation ul {
            margin-left:-30px;
        }
        .earned, .earnedThisMonth, .earnedThisUser, .earnedThisYear{
            display:block;
            min-width:60px;
            width:auto;
            font-weight:bold;
        }
        .earned{
            color:red;
        }
        .earnedThisUser{
            color:blue;
        }
        .earnedThisMonth{
            color:white;
        }
        .earnedThisYear{
            color:white;
        }
    </style>

    <!--breadcrumbs ends -->
    <div class="container">
        <div class="one">
            <?PHP
            if (!isset($_GET['annee']) || empty($_GET['annee']) || intval($_GET['annee']) == 0) {
                $_GET['annee'] = 2015;
            }
            $strtotimeStart = strtotime("" . intval($_GET['annee']) . "-1-1");
            $strtotimeEnd = strtotime("" . intval($_GET['annee']) . "-12-31 23:59:59");
            ?>
            <h4>Statistiques des Soumissions: Ann�e <span style="font-size:14px;"><?PHP echo $_GET['annee']; ?></span></h4>

            <div class="one">

                <a href="compte/statistiques.html?annee=<?PHP echo (intval($_GET['annee']) - 1); ?>" class="button color small round" style="color:white;float:left;margin:5px 0px 5px 0px;">< Ann�e <?PHP echo (intval($_GET['annee']) - 1); ?></a>
                <a href="compte/statistiques.html?annee=<?PHP echo (intval($_GET['annee'])); ?>" class="button color small round" style="color:white;float:left;margin:5px 10px 5px 10px;background:green;"> Ann�e En cours : <?PHP echo (intval($_GET['annee'])); ?></a>
                <a href="compte/statistiques.html?annee=<?PHP echo (intval($_GET['annee']) + 1); ?>" class="button color small round" style="color:white;float:left;margin:5px 0px 5px 0px;">Ann�e <?PHP echo (intval($_GET['annee']) + 1); ?> ></a>

                <table class="simple-table">
                    <tbody>

                        <tr>
                            <th>
                                R�f�renceur
                            </th>
                            <?PHP
                            foreach ($months as $mois) {
                                if (!empty($mois)) {
                                    echo "<th>" . $mois . "</th>";
                                }
                            }
                            ?>
                            <th>
                                Total
                            </th>
                        </tr>
                        <?PHP
                        $totalTotaux = 0;
                        $totalEarned = 0;
                        $totauxMois = array();
                        $totauxEarnedMois = array();
                        $execution = $dbh->query("SELECT * FROM utilisateurs WHERE typeutilisateur = '3' AND active = 1 AND annuaire_writer=1 ORDER BY nom ASC");
                        $current = 0;

                        // refactoring

                        $userModel = new User($idUser);
                        $current_year = intval($_GET['annee']);

                        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                            $thisRemuneration = Functions::getRemunueration($retour['id']);

                            // refactoring
                            $countByMonths = $userModel->getWritersEarnStatsMonthsByYear($current_year,$retour['id']);

//                        echo date('d/m/Y', $retour['joinTime']);
                            ?>
                            <tr>
                                <td>
                                    <?PHP echo '<span  style="font-weight:bold;"> ' . $retour['nom'] . " " . $retour['prenom'] . '</span>'; ?>
                                    <?PHP echo "<br/>Join in : " . $monthsLong[intval(date('m', $retour['joinTime']))] . ' ' . date('Y', $retour['joinTime']); ?>
                                </td>
                                <?PHP
                                $total = 0;
                                $totalEarnedThisUser = 0;
                                $counting = 1;

                                // Refactoring

                                $step = 0;

                                foreach ($months as $key => $mois) {
                                    if (!empty($mois)) {

                                        $counting = $key;
                                        $countingEnd = $counting + 1;
                                        if ($counting == 12) {
                                            $countingEnd = $counting;
                                        }

                                        if (strlen($counting) == 1) {
                                            $counting = '0' . $counting;
                                        }
                                        $countingFormatStart = strlen($counting) == 1 ? "0" . $counting : $counting;
                                        $countingFormatEnd = strlen($counting + 1) == 1 ? "0" . ($counting + 1) : ($counting + 1);

                                        // @deprecated
                                        /*
                                        $dateStart = "01-" . $countingFormatStart . "-" . intval($_GET['annee']) . " 00:00:00";
                                        $dateEnd = date('t', strtotime('01-' . ($countingFormatStart) . '-' . intval($_GET['annee']))) . "-" . ($countingFormatStart) . "-" . intval($_GET['annee']) . " 23:59:59";
//                                    $dateEnd = date('t', strtotime('01-' . ($countingFormatEnd) . '-' . intval($_GET['annee']))) . "-" . ($countingFormatEnd) . "-" . intval($_GET['annee']) . " 23:59:59";
//                                    echo "</br>".$dateStart . " to " . $dateEnd . "</br>";

                                        $strtotimeStart = strtotime($dateStart);
                                        $strtotimeEnd = strtotime($dateEnd);


                                        $sql = "SELECT COUNT(*)
                                        FROM jobs AS j WHERE j.affectedto= '" . $retour['id'] . "' AND j.adminApprouved=2  AND j.adminApprouvedTime >= '" . $strtotimeStart . "' AND j.adminApprouvedTime <= '" . $strtotimeEnd . "'";

                                        $count = $dbh->query($sql)->fetchColumn(0);*/

                                        $current_month = $months_FR_TO_ENG[$mois];
                                        if (isset($countByMonths[$current_month])){
                                            $count = $countByMonths[$current_month]['tasks_count'];
                                        }else{
                                            $count = 0;
                                        }

                                        $totalEarnedThisMonth = ($count * $thisRemuneration);
                                        if (date('Y') >= 2016 || (date('Y') == 2015 && $countingFormatStart >= 10)) {
                                            // @deprecated
                                            /*$sql_Plus = "SELECT SUM(coutReferer) FROM jobs AS j WHERE j.affectedto= '" . $retour['id'] . "' AND j.adminApprouved=2  AND j.adminApprouvedTime >= '" . $strtotimeStart . "'  AND j.adminApprouvedTime <= '" . $strtotimeEnd . "'";
                                            $totalEarnedThisMonth = $dbh->query($sql_Plus)->fetchColumn(0);*/

                                            if (isset($countByMonths[$current_month])){
                                                $totalEarnedThisMonth = $countByMonths[$current_month]['month_sum'];
                                            }else{
                                                $totalEarnedThisMonth = 0.0;
                                            }
                                            $totalEarnedThisMonth = floatval(round($totalEarnedThisMonth, 2));
                                        }

                                        echo "<td>";
                                        echo $count;
                                        if (isSu()) {
                                            echo '<span class="earned">' . $totalEarnedThisMonth . ' <i class="icon-dollar"></i></span>';
                                        }
                                        echo "</td>";
                                        $total += $count;
                                        $totalEarnedThisUser += $totalEarnedThisMonth;
                                        $counting++;

                                        $totauxMois[$counting]['total'] = intval($totauxMois[$counting]['total']);
                                        $totauxMois[$counting]['total'] += $count;

                                        $totauxMois[$counting]['earned'] = floatval($totauxMois[$counting]['earned']);
                                        $totauxMois[$counting]['earned'] += $totalEarnedThisMonth;
                                    }
                                }
                                $totalTotaux += $total;
                                $totalEarned += $totalEarnedThisUser;
                                ?>
                                <td>
                                    <?PHP echo $total; ?>
                                    <?PHP
                                    if (isSu()) {
                                        echo '<span class="earned earnedThisUser">' . $totalEarnedThisUser . ' <i class="icon-dollar"></i></span>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?PHP
                            $current++;

                            //if ($current == 3){
                              //die;
                            //s}
                        }
//                    print_r($totauxMois);
                        ?>
                        <tr style="font-size:16px;">
                            <td style="font-weight:bold;color:white;background:green;text-align:right;" colspan="">
                                TOTAL :
                            </td>
                            <?PHP
                            foreach ($totauxMois as $key => $totauxMoisUnit) {
                                echo '<td  style="text-align:left;font-weight:bold;color:white;background:green" >';
                                echo intval($totauxMoisUnit['total']);
                                if (isSu()) {
                                    echo '<span class="earned earnedThisMonth">' . $totauxMoisUnit['earned'] . ' <i class="icon-dollar"></i>';
                                }
                                echo '</td>';
                            }
                            ?>
                            <td style="text-align:left;font-weight:bold;color:white;background:green" colspan="">
                                <?PHP echo $totalTotaux; ?>        
                                <?PHP
                                if (isSu()) {
                                    echo '<span class="earned earnedThisYear">' . $totalEarned . ' <i class="icon-dollar"></i></span>';
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <a href="compte/statistiques.html?annee=<?PHP echo (intval($_GET['annee']) - 1); ?>" class="button color small round" style="color:white;float:left;margin:5px 0px 5px 0px;">< Ann�e <?PHP echo (intval($_GET['annee']) - 1); ?></a>
                <a href="compte/statistiques.html?annee=<?PHP echo (intval($_GET['annee'])); ?>" class="button color small round" style="color:white;float:left;margin:5px 10px 5px 10px;background:green;"> Ann�e En cours : <?PHP echo (intval($_GET['annee'])); ?></a>
                <a href="compte/statistiques.html?annee=<?PHP echo (intval($_GET['annee']) + 1); ?>" class="button color small round" style="color:white;float:left;margin:5px 0px 5px 10px;">Ann�e <?PHP echo (intval($_GET['annee']) + 1); ?> ></a>

                <!-- Pagination -->
                <!-- End pagination -->
            </div>


        </div>

        <?php //$end_time = (microtime(true) - $start );?>

        <h1><?php echo $end_time; ?></h1>

    </div>
    <?PHP
}
include("files/includes/bottomBas.php")
?>