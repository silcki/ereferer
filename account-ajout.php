<?PHP
@session_start();

if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

$a = 1;

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}



if ($_GET['block'] == "site") {
    $b = 1;
} else if ($_GET['block'] == "ajouterannuaire") {
    $b = 6;
} else if ($_GET['block'] == "categorie") {
    $a = 4;
    $b = 5;
} else if ($_GET['block'] == "contrat") {
    $a = 4;
    $b = 2;
} else if ($_GET['block'] == "listannuaire") {
    $a = 1;
    $b = 9;
} else if ($_GET['block'] == "utilisateur") {
    $a = 5;
    $b = 5;
} else {
    $b = 10;
}


$page = 1;
include('files/includes/topHaut.php');


require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';

$label = "Ajouter";
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

    <?php

        $show_error_modal = false;
        $show_error_legal_modal = false;

        //var_dump($_SESSION['error_inner_pages_subdomains']); die;

        if (isset($_SESSION['tamponForm']['site']) && ($_SESSION['alertLogincontent']!='new_project_duplicates_error')) {

                // inner pages subdomains
                if (isset($_SESSION['error_inner_pages_subdomains'])){

                    $show_error_modal = true;
                    $modal_items = $_SESSION['error_inner_pages_subdomains_data'];
                    //$modal_items = array_slice($modal_items,0,2);
                }

                if(isset($_SESSION['error_legal_content'])){

                    $show_error_legal_modal = true;
                    $modal_legal_items = $_SESSION['error_legal_content_data'];
                    //$modal_legal_items = array_slice($modal_legal_items,0,2);
                }


        }

    ?>

       <script>
           (function($){
               $(document).ready(function(){

                   var show_error_modal = <?php echo json_encode($show_error_modal);?>;
                   var show_error_legal_modal = <?php echo json_encode($show_error_legal_modal);?>;

                   if (show_error_modal) {
                       $(".inner_pages_subdomain_modal").modal({
                           escapeClose: false,
                           clickClose: false,
                           showClose: false
                       });
                   }

                   if (!show_error_modal && show_error_legal_modal){
                       $(".legal_modal").modal({
                           escapeClose: false,
                           clickClose: false,
                           showClose: false
                       });
                   }

                    $('.link_status').click(function(e){
                        e.preventDefault();
                        $(this).parent().parent().find('a').removeClass('picked');
                        $(this).addClass('picked');
                        var type = $(this).data('type');
                        checkPickedLinks(type);
                    });


                    $('.submit_form').click(function(){
                        var form = $("#form_creating_project");
                        var type = $(this).data('type');

                        var hidden_ids_name = type + '_ids';
                        var hidden_cheked_name = type + '_checked';
                        var remove_type_classname = type + '_temp_data';

                        // remove hidden data
                        $('.'+remove_type_classname).remove();

                        var items = [];

                        if (type=='subdomain'){
                           items =  $(".inner_pages_subdomain_modal .items a.picked");
                        }

                        if (type=='legal'){
                           items =  $(".legal_modal .items a.picked");
                        }

                        if (confirm('En �tes-vous s�r?')) {

                            $.each(items, function (i, item) {
                                var id = $(item).data('id');
                                var value = $(item).data('value');

                                var hidden_row = '<input type="hidden" class="'+remove_type_classname+'" name="'+hidden_ids_name+'[' + id + ']" value="' + value + '" />';
                                console.log(hidden_row);
                                $(form).append(hidden_row);
                            });

                            var hidden_row = '<input type="hidden" class="'+remove_type_classname+' '+hidden_cheked_name+'" name="'+hidden_cheked_name+'" value="1" />';

                            console.log(hidden_row);
                            $(form).append(hidden_row);

                            // send subdomains & inner pages annuaires

                            //alert('show_error_legal_modal = ',show_error_legal_modal);

                            if (!show_error_legal_modal){
                                $(form).submit();
                            }else{
                                // sending legal data
                                $(".inner_pages_subdomain_modal").modal("hide");
                                $("#error_modal_creating_project .footer").hide();

                                if (!$(form).has('.legal_checked').length){
                                    $(".legal_modal").modal({
                                        escapeClose: false,
                                        clickClose: false,
                                        showClose: false
                                    });
                                }else{
                                    $(".legal_modal").modal("hide");
                                    $(form).submit();
                                }

                            }
                        }
                    });

                   function checkPickedLinks(type){
                      var items_type = '.'+type+'_items';

                      var total_picked =  $(items_type+" a.picked").size();
                      var total = $(items_type+" .domain").size();

                      if (total_picked == total){
                          $("#error_modal_creating_project .footer").show();
                      }else{
                          $("#error_modal_creating_project .footer").hide();
                      }

                   }


               });

           })(jQuery);

       </script>

        <?PHP
        include("files/includes/menu.php")
        ?>

        <?php if ($show_error_modal){ ?>
            <div class="modal inner_pages_subdomain_modal" id="error_modal_creating_project" style="display:none;">
                <div class="header">ATTENTION !</div>

                <?php
                    $error_types = array();
                    if (isset($_SESSION['error_subdomain_type']) && !empty($_SESSION['error_subdomain_type'])){
                        $error_types[] = '<b>sous-domaine</b>';
                    }

                    if (isset($_SESSION['error_inner_pages_type']) && !empty($_SESSION['error_inner_pages_type'])){
                        $error_types[] = '<b>pages internes</b>';
                    }
                    $str_error_types = implode(' / ',$error_types);

                ?>

                Dans votre liste d'annuaires <b><?php echo $_SESSION['error_annuaire'];?></b>, vous avez selectionn� des annuaires n'acceptant pas les <?php echo $str_error_types;?>.
                <br/> <br/>
                <div id="list_items" >
                    <b>Souhaitez-vous tout de m�me que ces soumissions soient effectu�es ?</b><br/><br/>
                    <table class="items subdomain_items">
                      <?php foreach($modal_items as $item):?>
                        <tr data-id="<?php echo $item['id'];?>" >
                            <td class="domain" ><?php echo str_replace('http://','',$item['annuaire']); ?> </td>
                            <td class="button"> <a href="#" class="link_status" data-type="subdomain" data-id="<?php echo $item['id'];?>" data-value="1">Faire</a></td>
                            <td class="no_button"> <a href="#" class="link_status" data-type="subdomain" data-id="<?php echo $item['id'];?>" data-value="0">Ne pas faire</a></td>
                        </tr>
                      <?php endforeach; ?>
                    </table>
                    <div class="footer">
                        <div class="button color big submit_form" id="submit_form" data-type="subdomain">Terminer</div>
                    </div>
                </div>


            </div>
        <?php } ?>


        <?php if ($show_error_legal_modal){ ?>
            <div class="modal legal_modal" id="error_modal_creating_project" style="display:none;">
                <div class="header">ATTENTION !</div>

                Dans votre liste d'annuaires <b><?php echo $_SESSION['error_annuaire'];?></b>, vous avez selectionn� des annuaires n'acceptant pas les sites sans <b>mentions l�gales</b>.

                <br/> <br/>
                <div id="list_items" >
                    <b>Souhaitez-vous tout de m�me que ces soumissions soient effectu�es ?</b><br/><br/>
                    <table class="items legal_items">
                        <?php foreach($modal_legal_items as $item):?>
                            <tr data-id="<?php echo $item['id'];?>" >
                                <td class="domain" ><?php echo str_replace('http://','',$item['annuaire']); ?> </td>
                                <td class="button"> <a href="#" class="link_status" data-type="legal" data-id="<?php echo $item['id'];?>" data-value="1">Faire</a></td>
                                <td class="no_button"> <a href="#" class="link_status" data-type="legal" data-id="<?php echo $item['id'];?>" data-value="0">Ne pas faire</a></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <div class="footer">
                        <div class="button color big submit_form"  id="submit_form" data-type="legal">Terminer</div>
                    </div>
                </div>
            </div>
        <?php } ?>



    <div class="three-fourth annnuaire_create_project">
            <div class="one">
                <?PHP
                if (isset($_GET['statut']) && $_GET['statut'] != "") {

                    // hide "standart" error
                    if ( ($_SESSION['alertLogincontent']!='new_project_duplicates_error')
                      && ($_SESSION['alertLogincontent']!='error_inner_pages_subdomains')
                      && ($_SESSION['alertLogincontent']!='error')
                      && (!isset($_SESSION['error_legal_content']))
                    ){
                    ?>
                    <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:600px">
                        <p>
                            <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>-->
                            <?php
                                // hide from top error-block

                            ?>
                            <?PHP echo $_SESSION['alertLogincontent']; ?>
                        </p>
                    </div>
                <?PHP }
                }?>
                <form action="compte/ajouter/<?PHP echo $_GET['block']; ?>.html" method="post" id="form_creating_project" style="margin-top:0px;float:left;">

                    <?PHP
                    if ($_GET['block'] == "site" && (isWebmaster($idTypeUser) || isAdmin($idTypeUser) || isSu($idTypeUser))) {
                        $label = "Suivant";
                        ?>
                        <script>
                            (function($){
                                $(document).ready(function(){

                                    var annuairelist_modify_btn = $('.annuairelist_count_link[data-action_type="modify"]');
                                    var annuairelist_setup_btn = $('.annuairelist_count_link[data-action_type="setup"]');

                                    $("select[name=annuaire]").on('change',function() {
                                        var selected_id = $(this).val();
                                        var words_count = $(this).children('option').filter(':selected').data('words_count');

                                        var list_annuiare_url = '/compte/modifier/listannuairecontent.html?id=';

                                        console.log(selected_id);

                                        // reset
                                        $(annuairelist_modify_btn).hide();
                                        $(annuairelist_setup_btn).hide();
                                        $("#annuaire_list_words_count").html('');

                                        if (selected_id > 0) {
                                            $(".modify_annuaire").show();

                                            var href = list_annuiare_url+selected_id;

                                            if (words_count>0){
                                                $("#annuaire_list_words_count").html(words_count);
                                                $(annuairelist_modify_btn).find('a').prop('href',href);
                                                $(annuairelist_modify_btn).show();
                                            }else{
                                                $(annuairelist_setup_btn).find('a').prop('href',href);
                                                $(annuairelist_setup_btn).show();
                                            }
                                        }else{
                                            $(".modify_annuaire").hide();
                                        }
                                    });

                                    $("select[name=annuaire]").trigger('change');


                                    $("#project_urls").on('blur input',function(){
                                        var text = $(this).val();

                                        // block mobilitedouce.fr
                                        /*if (text.indexOf('mobilitedouce.fr')>=0){
                                            $('.https_error').eq(1).show();
                                            $(".annnuaire_create_project input[type=submit]").hide();
                                        }else{
                                            $('.https_error').eq(1).hide();
                                            $(".annnuaire_create_project input[type=submit]").show();
                                        }*/

                                        // block https
                                        if (text.indexOf('https')>=0){
                                            $('.https_error').eq(0).show();
                                        }else{
                                            $('.https_error').eq(0).hide();
                                        }
                                    });


                                });
                            })(jQuery);
                        </script>

                        <?php
                        // Showing "Expecting time to start a project"-message
                        $show_estimation_start_days = false;


                        if (isWebmaster()){
                            $projectsModel = new Projects($dbh);
                            $estimation_start_days = $projectsModel->getEstimatedStartDaysCount();
                            if ($estimation_start_days > 0){
                                $show_estimation_start_days = true;
                            }
                        }


                        /*
                         @ deprecated
                        if (isset($_SESSION['allParameters']['estimation_start_days'])){
                            $estimation_start_days = $_SESSION['allParameters']['estimation_start_days']['valeur'];

                            if (isWebmaster() && ($estimation_start_days>0)){
                                $show_estimation_start_days = true;
                            }
                        }*/


                        if ($show_estimation_start_days){
                            $days_label = ($estimation_start_days==1)? 'jour' : 'jours';
                            ?>
                            <div class="notification notice" >
                                Le temps d'attente pour le traitement des nouveaux projets est estim� � <b><?php echo $estimation_start_days;?></b> <?php echo $days_label; ?>.
                            </div>
                        <?php } ?>


                        <h4><a nohref="#">Ajouter un nouveau site � mon compte</a></h4>

                        <!--<button class="search-btn"></button>-->
                        <fieldset style="">
                            <!--<legend class="color">Connexion</legend>-->
                            <?PHP
                            if (isSU() || isAdmin()) {
                                ?>
                                <select name="proprietaire" class="select">
                                    <option value="0">Choisir propri�taire</option>
                                    <?PHP
                                    echo Functions::getUserOptionList(4, 1);
                                    ?>
                                </select>
                                <?PHP
                            }
                            ?>
                        </fieldset>
                        <fieldset class="one-fourth">
                            <label><span>* </span>URL du ou des site(s) 
                                <br/> <i class="title">Si vous avez plusieurs sites, veuillez les s�parer d'un saut � la ligne.</i>
                            </label>
                            <textarea rows="10" cols="" name="site" id="project_urls" class="search-field" style="margin-bottom:5px;width:400px;height:150px;font-size:11px;"><?PHP
                                if (isset($_SESSION['tamponForm']['site'])) {
                                    echo $_SESSION['tamponForm']['site'];
                                }
                                ?></textarea>
                            <div class="https_error">
                                Attention, la majorit� des annuaires refusent le protocope HTTPS ! Pour ces annuaires, si la soumission n'est pas possible en HTTPS, elle sera alors r�alis�e en HTTP.
                            </div>
                            <div class="https_error">
                                Par mesure de s�curit�, votre site a �t� bloqu�, merci de me contacter par la messagerie priv�e afin de le d�verrouiller !
                            </div>
                        </fieldset>

                        <div class="clearfix"></div>
                        <?php if (isset($_SESSION['new_project_duplicates_error'])): ?>
                            <div class="notification error" style="width:600px">
                                <?php
                                    $duplicates_domains =  implode(' , ',$_SESSION['new_project_duplicates']);
                                    //$list_id = $_SESSION['tamponForm']['annuaire'];
                                    $list_id = array_shift(array_keys($_SESSION['new_project_duplicates']));
                                ?>
                                Attention, vous avez d�j� un projet cr�� pour  <b><?php echo $duplicates_domains;?></b> . Il est conseill� d'�diter la liste d'annuaires de ce dernier si vous souhaitez le poursuivre.
                                C'est � dire, vous devez ajouter de nouveaux annuaires � cette liste et ensuite pourrez "reprendre" le projet :
                                <a target="_blank" href="/compte/modifier/listannuairecontent.html?id=<?php echo $list_id;?>">http://www.ereferer.fr/compte/modifier/listannuairecontent.html?id=<?php echo $list_id;?></a>
                            </div>
                            <?php unset($_SESSION['new_project_duplicates_error']);?>
                        <?php endif;?>

                        <?PHP
                        if ($result['envoyer'] == 0 || ($result['envoyer'] == 1 && (isAdmin() || isSU()))) {
                            ?>
                            <!--                            <fieldset class="one-fourth cleared">
                                                            <label><span>* </span>Budget Pr�vu</label>
                                                            <input name="budget" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['budget'])) {
                                echo $_SESSION['tamponForm']['budget'];
                            }
                            ?>' style="margin-bottom:5px;width:50px;float:left;margin-right: 5px" onkeyup="ve(this);" maxlength="6"><?PHP echo $_SESSION['devise']; ?><br/>
                                                        </fieldset>-->

                        <?PHP } ?>
                        <!--                        <fieldset class="one-fourth cleared">
                                                    <label><span>* </span>Cat�gorie du site</label>
                                                    <select name="categorie" class="select cleared">
                                                        <option value="0">Choisir la cat�gorie</option>
                        <?PHP echo Functions::getCategorieList(isset($_SESSION['tamponForm']['categorie']) ? $_SESSION['tamponForm']['categorie'] : ""); ?>
                                                    </select>
                                                </fieldset>-->

                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span>* </span>Liste d'annuaires � utiliser</label>
                            <?PHP echo Functions::getUserAnnuairelist(isset($_SESSION['tamponForm']['annuaire']) ? $_SESSION['tamponForm']['annuaire'] : "",-1,1); ?>
                        </fieldset>

                        <fieldset class="two-third modify_annuaire cleared" style="display:none;">
                            <br/>
                            <label>Nombre de mots minimum souhait�s :</label>
                            <div>
                                 <div class="annuairelist_count_link annuairelist_count_link_modify" data-action_type="modify" ><div id="annuaire_list_words_count"></div> ( <a href="" >Cliquez-ici</a> pour modifier ce seuil associ� � votre liste )</div>
                                 <div class="annuairelist_count_link" data-action_type="setup"> Pas de minimum souhait�. <a href="" >Modifier votre liste d'annuaires</a> ici pour le configurer. </div>
                             </div>
                        </fieldset>

                        <fieldset class="one-fourth cleared" style="width:500px;">
                            <br/>
                            <label><span>* </span>Fr�quence de soumission 
                                <br/> <i class="title">Exemple : 1 annuaire(s) tous les 2 jour(s)</i>
                            </label>
                            <div style="display:inline-block;margin-bottom:5px;width:170px;float:left;">
                                <input name="annuaireFrequence" class="search-field" type="text" value='<?PHP
                                if (isset($_SESSION['tamponForm']['annuaireFrequence'])) {
                                    echo $_SESSION['tamponForm']['annuaireFrequence'];
                                }
                                ?>' style="float:left;width:20px;" onkeyup="ve(this);" maxlength="2"> &nbsp;Annuaire(s) tous les  
                            </div>
                            <div style="display:inline-block;margin-bottom:5px;width:90px;margin-right: 5px;margin-left:0px;float:left;">
                                <input name="jourFrequence" class="search-field" type="text" value='<?PHP
                                if (isset($_SESSION['tamponForm']['jourFrequence'])) {
                                    echo $_SESSION['tamponForm']['jourFrequence'];
                                }
                                ?>' style="width:20px;float:left;" onkeyup="ve(this);" maxlength="2">&nbsp; Jour(s)
                            </div>
                            <br/>

                        </fieldset>
                        <!--                        <fieldset class="one-fourth cleared">
                                                    <br/>
                                                    <label><span></span>Ancres pour le r�f�rencement, 1 par ligne - Facultatif 
                                                        <br/><i class="title">( Laissez cette case vide si vous n'avez pas de pr�f�rence )</i>
                                                    </label>
                                                    <textarea rows="15" cols="" name="ancres" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP
                        if (isset($_SESSION['tamponForm']['ancres'])) {
                            echo $_SESSION['tamponForm']['ancres'];
                        }
                        ?></textarea><br/>
                                                </fieldset>-->
                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span></span>Quelques conseils ou instructions � nous donner ? Facultatif
                                <!--<br/> <i class="title">Pr�cisez une adresse email et son mot de passe si vous souhaitez que le r�f�renceur confirme les liens de soumissions � l'int�rieur des emails</i>-->
                            </label>
                            <textarea rows="15" cols="" name="commentaire" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP
                                if (isset($_SESSION['tamponForm']['commentaire'])) {
                                    echo $_SESSION['tamponForm']['commentaire'];
                                }
                                ?></textarea><br/>
                        </fieldset>


                        <?PHP
                    } else if ($_GET['block'] == "ajouterannuaire") {
                        $label = "Ajouter annuaire";
                        ?>
                        <h4>Ajouter un annuaire</h4>
                        <p>
                            Veuillez entrer l'url de l'annuaire dans le champ ci-dessous au format suivant : www.annuaire.com
                        </p>

                        <fieldset style="">
                            <!--<legend class="color">Connexion</legend>-->
                            <?PHP
                            if (isSU() || isAdmin()) {
                                ?>
                                <select name="webmasterPartenaire" class="select" style="width:400px;">
                                    <option value="0">S�lectionner le responsable de l'annuaire</option>
                                    <?PHP
                                    echo Functions::getUserOptionList(4, 1);
                                    ?>
                                </select>
                                <?PHP
                            }
                            ?>
                        </fieldset>
                        <fieldset class="one" style="clear:both">
                            <br/>
                            <label style="width:300px;">Possibilit� de cr�er un compte au webmaster:</label>
                            <div style="width:100px;float:left;margin-right:10px;">
                                <input name="poss" class="search-field" type="radio" value='0' style="float:left;" maxlength="300" id="possnon" checked="checked">
                                <label for="possnon" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                            </div>

                            <div style="width:50px;float:left;margin-right:10px;">
                                <input name="poss" class="search-field" type="radio" value='1' style="float:left;" maxlength="300" id="possoui">
                                <label for="possoui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                            </div>
                            <br/>
                            <br/>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Comission par validation:</label>
                            <input name="WebPartenairePrice" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-euro"></i> (Euro)                        <br>
                            <br/>

                        </fieldset>

                        <fieldset class="one-fourth" style="clear:both">
                            <label style="width:300px;">Le webmaster peut-il choisir son ancre? :</label>
                            <div style="width:100px;float:left;margin-right:10px;">
                                <input name="ancre" class="search-field ancreDisplaY" type="radio" value='0' style="float:left;" maxlength="300" id="ancrenon" checked="checked">
                                <label for="ancrenon" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                            </div>

                            <div style="width:50px;float:left;margin-right:10px;">
                                <input name="ancre" class="search-field ancreDisplaY" type="radio" value='1' style="float:left;" maxlength="300" id="ancreoui">
                                <label for="ancreoui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                            </div>
                            <br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared" id="ancreConsigneDisplay" style="display:block;">
                            <label><span></span>Consigne de l'ancre (facultatif):

                            </label>
                            <input class="search-field" type="text" style="float:left;width:400px" name="consignesAncre" value="<?PHP
                            if (isset($_SESSION['tamponForm']['consignesAncre'])) {
                                echo $_SESSION['tamponForm']['consignesAncre'];
                            }
                            ?>"/>
                            <br/>
                            <br/>
                        </fieldset>

                        <fieldset class="one-fourth" style="clear:both">
                            <br/>
                            <label style="width:300px;">Type d'annuaire:</label>
                            <div style="width:100px;float:left;margin-right:10px;">
                                <input name="cp" class="search-field" type="radio" value='0' style="float:left;" maxlength="300" id="cpnon" checked="checked">
                                <label for="cpnon" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Annuaire</label>
                            </div>

                            <div style="width:50px;float:left;margin-right:10px;">
                                <input name="cp" class="search-field" type="radio" value='1' style="float:left;" maxlength="300" id="cpoui">
                                <label for="cpoui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">CP</label>
                            </div>
                            <br/>
                            <br/>
                        </fieldset>
                        <fieldset class="one-fourth" style="clear:both">
                            <label><span> </span>URL de l'annuaire: </label>
                            <input name="annuaire" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['annuaire'])) {
                                echo $_SESSION['tamponForm']['annuaire'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;" maxlength="300"><br/>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Tarif extra Webmaster:</label>
                            <input name="tarifW" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-euro"></i> (Euro)                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Tarif extra R�f�renceur:</label>
                            <input name="tarifR" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-dollar"></i> (Dollar)                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Date de cr�ation:</label>
                            <input name="age" id="datedemarrage" class="search-field" value="" type="text" style="margin-bottom:5px;width:100px;float:left;margin-right:5px;" onkeyup="">                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Trust Flow:</label>
                            <input name="tf" class="search-field" value="0" type="text" style="margin-bottom:5px;width:80px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                        </fieldset>
                        <!--                        
                                                <fieldset style="clear:both;">
                                                    <label class="color">AlexaRank:</label>
                                                    <input name="ar" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                                                </fieldset>-->

                        <fieldset class="one-fourth" style="clear:both;">
                            <label>PageRank:</label>
                            <input name="page_rank" class="search-field" type="text" style="margin-bottom:5px;width:80px;float:left" value="0">
                        </fieldset>
                        <!--                        <fieldset style="clear:both;">
                                                    <label class="color">MozRank:</label>
                                                    <input name="mr" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                                                </fieldset>-->
                        <fieldset style="clear:both;">
                            <label class="color">Total Domaine r�f�rent:</label>
                            <input name="tdr" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Total Backlink:</label>
                            <input name="tb" class="search-field" value="0" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                        </fieldset>
                        <!--                        <fieldset class="one-fourth">
                                                    <br/>
                        
                                                    <label style="width:300px;">Afficher les ancres du projet:</label>
                        
                                                    <div style="width:50px;float:left;margin-right:10px;">
                                                        <input name="display" class="search-field" type="radio" value='1' style="float:left;" maxlength="300" id="oui">
                                                        <label for="oui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                                                    </div>
                        
                                                    <div style="width:50px;float:left;margin-right:10px;">
                                                        <input name="display" class="search-field" type="radio" value='0' style="float:left;" maxlength="300" id="non" checked="checked">
                                                        <label for="non" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                                                    </div>
                                                </fieldset>-->

                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span></span>Consignes de l'annuaire :
                                <br/>
                            </label>
                            <textarea rows="15" cols="" name="consignesTotale" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP
                                if (isset($_SESSION['tamponForm']['consignes'])) {
                                    echo $_SESSION['tamponForm']['consignes'];
                                }
                                ?></textarea><br/>
                        </fieldset>
                        <!--                        <fieldset class="one-fourth">
                                                    <label><span> </span>Lien retour ? </label>
                                                    <div style="width:50px;float:left;">
                                                        <input name="retour" checked="checked" class="search-field" type="radio" value='1' style="" maxlength="300" id="oui"><label for="oui" style="width:15px;margin-top:-5px;">Oui</label>
                                                    </div>
                                                    <div style="width:50px;float:left;margin-left:20px;">
                                                        <input name="retour" class="search-field" type="radio" value='0' style="" maxlength="300" id="non"><label for="non" style="width:15px;margin-top:-5px;">Non</label>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                </fieldset>-->

                        <fieldset class="one-fourth cleared" style="width:500px;">
                            <br/>
                            <br/>
                            <label><span>* </span>Ajouter aux listes suivantes :</label>
                            <?PHP echo Functions::getUserAnnuairelistCheckCase(); ?>
                        </fieldset>

                        <?PHP
                    } else if ($_GET['block'] == "listannuaire") {
                        $label = "Cr�er ma liste";
                        ?>
                        <h4>Cr�er un r�pertoire d'annuaire</h4>
                        <p>
                            Veuillez entrer le nom de cette liste
                        </p>

                        <fieldset class="one-fourth">
                            <label><span> </span>Libell� de la liste: </label>
                            <input name="listannuaire" class="search-field" type="text" value="<?PHP
                            if (isset($_SESSION['tamponForm']['listannuaire'])) {
                                // purify
                                $HTML = new HTML;
                                $_SESSION['tamponForm']['listannuaire'] = $HTML->purifyParams($_SESSION['tamponForm']['listannuaire']);

                                echo $_SESSION['tamponForm']['listannuaire'];
                            }
                            ?>" style="margin-bottom:5px;width:350px;" maxlength="300"><br/>
                        </fieldset>

                        <?PHP
                    } else if ($_GET['block'] == "commentaire") {
                        $label = "Ajouter commentaire";
                        if (!isset($_GET['annuaire'])) {
                            header('location:' . $_SERVER['HTTP_REFERER']);
                        }
                        if (!isset($_GET['projet'])) {
                            header('location:' . $_SERVER['HTTP_REFERER']);
                        }
                        $titreok = 0;

                        if (isset($_GET['soumission'])) {
                            $titreok = 1;
                        }
                        ?>
                        <h4>
                            <?PHP
                            if ($titreok == 1) {
                                if ($_GET['soumission'] == "no") {
                                    echo "Pour quelle raison la soumission est-elle impossible ?";
                                } else {
                                    echo "Ajouter une description a cette soumission";
                                }
                            } else {
                                echo 'Ajouter un commentaire';
                            }
                            ?>
                        </h4>
                        <p>
                            <?PHP
                            if ($titreok == 1) {
                                if ($_GET['soumission'] == "no") {
                                    echo "Veuillez saisir les raisons de l'echec de la soumission";
                                } else {
                                    echo "Veuillez saisir la description";
                                }
                            } else {
                                echo 'Veuillez saisir le commentaire';
                            }
                            ?>

                        </p>

                        <input name="annuaire" value="<?PHP echo intval($_GET['annuaire']); ?>" type="hidden"/>
                        <input name="projet" value="<?PHP echo intval($_GET['projet']); ?>" type="hidden"/>
                        <input name="redir" value="<?PHP echo urlencode($_SERVER['HTTP_REFERER']); ?>" type="hidden"/>
                        <fieldset class="one-fourth">
                            <label><span> </span>
                                <?PHP
                                if ($titreok == 1) {
                                    if ($_GET['soumission'] == "no") {
                                        echo "Raisons de l'echec de la soumission";
                                    } else {
                                        echo "Contenu de la description";
                                    }
                                } else {
                                    echo 'Contenu du commentaire';
                                }
                                ?>
                            <!--<i class="title">Si vous avez plusieurs sites, veuillez les s�parer d'un saut � la ligne.</i>-->
                            </label>
                            <textarea rows="10" cols="" name="commentaire" class="search-field" style="margin-bottom:5px;width:400px;height:150px;font-size:11px;"><?PHP
                                if (isset($_SESSION['tamponForm']['commentaire'])) {
                                    echo $_SESSION['tamponForm']['commentaire'];
                                }
                                ?></textarea><br/>
                        </fieldset>
                        <div class="clearfix"></div>
                        <?php if (isset($_SESSION['ajout_commentaire_error_words_count'])){ ?>
                            <div class="notification error" style="display:inline-block;" >
                                <p>Erreur, ce texte ne fait que <b><?php echo $_SESSION['ajout_commentaire_passed_words'];?></b> et doit faire environ <b><?php echo $_SESSION['ajout_commentaire_required_words_count'];?></b> mots!</p>
                            </div>
                        <?php } ?>

                        <?PHP
                    } else if ($_GET['block'] == "utilisateur") {
                        $label = "Ajouter utilisateur";
                        ?>

                        <script>
                            (function($){
                                $(document).ready(function() {

                                    $("#user_type_id").on('change',function(){
                                       var type_id = parseInt($(this).val());
                                        if (type_id == 3){
                                            $(".writer_types").show();
                                        }else{
                                            $(".writer_types").hide();
                                        }
                                    });

                                    $('#user_type_id').trigger('change');
                                });
                            })(jQuery);
                        </script>

                        <fieldset class="one-fourth cleared">
                            <label><span> </span>Type utilisateur: </label>
                            <select name="typeutilisateur" id="user_type_id">
                                <option value="0">S�lectionner groupe</option>
                                <option value="2">Administrateur</option>
                                <option value="3">R�f�renceur</option>
                                <option value="4">Webmaster</option>
                            </select>
                        </fieldset>
                        <div class="clearfix"></div><br/>

                        <div class="writer_types" style="display: none;">
                            <fieldset>
                                <label style="display:inline-block;">Annuaire</label> <input type="checkbox" name="annuaire_writer" />
                            </fieldset>
                            <fieldset>
                                <label style="display:inline-block;">Redaction</label> <input type="checkbox" name="redaction_writer" />
                            </fieldset>
                        </div>


                        <fieldset class="one-fourth cleared">
                            <label><span>* </span>Nom </label>
                            <input name="nom" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['nom'])) {
                                echo $_SESSION['tamponForm']['nom'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;"><br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span>* </span>Pr�nom(s) </label>
                            <input name="prenom" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['prenom'])) {
                                echo $_SESSION['tamponForm']['prenom'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;"><br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span> </span>T�l�phone </label>
                            <input name="telephone" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['telephone'])) {
                                echo $_SESSION['tamponForm']['telephone'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;" onkeyup="ve(this);"><br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span> </span>Adresse </label>
                            <input name="adresse" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['adresse'])) {
                                echo $_SESSION['tamponForm']['adresse'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;"><br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span> </span>Soci�t� </label>
                            <input name="societe" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['societe'])) {
                                echo $_SESSION['tamponForm']['societe'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;margin-right:100px;"><br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span> </span>Site Web Personnel </label>
                            <input name="siteweb" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['siteweb'])) {
                                echo $_SESSION['tamponForm']['siteweb'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;"><br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span>* </span>Adresse Email </label>
                            <input name="email" class="search-field" type="text" value='<?PHP
                            if (isset($_SESSION['tamponForm']['email'])) {
                                echo $_SESSION['tamponForm']['email'];
                            }
                            ?>' style="margin-bottom:5px;width:350px;margin-right:100px;"><br/>
                        </fieldset>
                        <fieldset class="one-fourth cleared">
                            <label><span>* </span>Mot de passe: </label>
                            <input name="pass" class="search-field" type="password" value='' style="margin-bottom:5px;width:400px;"><br/>
                        </fieldset>
                        <fieldset class="one-fourth cleared">
                            <label><span>* </span>Confirmer mot de passe: </label>
                            <input name="confpass" class="search-field" type="password" value='' style="margin-bottom:5px;width:400px;"><br/>
                        </fieldset>

                        <div class="cleared"></div>
                        <?PHP
                    } else if ($_GET['block'] == "contrat") {
                        $label = "Ajouter contrat";
                        ?>
                        <h4>Ajouter un contrat</h4>
                        <fieldset class="one-fourth">
                            <label><span> </span>Utilisateurs concern�s: </label>
                            <select name="typeUser">
                                <option value="0" selected="selected">S�lectionner groupe</option>
                                <option value="2">Administrateur</option>
                                <option value="3">R�f�renceur</option>
                                <option value="4">Webmasteur</option>
                                <option value="5">Affiliation</option>
                                <option value="6">Partenariat</option>
                            </select>
                        </fieldset>
                        <fieldset class="one-fourth">
                            <label><span> </span>Module: </label>
                            <select name="module">
                                <option value="0" selected="selected">S�lectionner module</option>
                                <option value="redaction">Redaction</option>
                            </select>
                        </fieldset>
                        <br/><br/>

                        <div class="clearfix"></div>
                        <fieldset class="one-fourth">
                            <label><span> </span>Contenu du contrat: </label>
                            <textarea name="content" id="content" style="width:700px;height:300px;float:left !important;"><?PHP
                                if (isset($_SESSION['tamponForm']['content'])) {
                                    echo $_SESSION['tamponForm']['content'];
                                }
                                ?></textarea>

                        </fieldset>
                        <div class="clear"></div>
                        <?PHP
                    }
                    ?>
                    <div class="clear">  </div><br/>
                    <input type="submit" class="button color small round" value="<?PHP echo $label; ?>" style="color:white;"/>

                </form>
            </div>


            <!-- Pagination -->

            <!-- End pagination -->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>