<?PHP
@session_start();

$a = 1;
$page = 4;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">
        <div class="blog-post">
            <p>
           <h1>Conditions g�n�rales de vente</h1> <h2>Clause n� 1 : Objet </h2>

Les conditions g�n�rales de vente d�crites ci-apr�s d�taillent les droits et obligations de la soci�t� CADOMAX et de son client dans le cadre d'une prestation de r�f�rencement dans des annuaires de sites Internet.  
Toute prestation accomplie par la soci�t� CADOMAX implique donc l'adh�sion sans r�serve de l'acheteur aux pr�sentes conditions g�n�rales de vente. 

<h2>Clause n� 2 : Prix</h2>  le prix de la soumission dans un annuaire est de 1.85� TTC. Ce tarif sous-entend: 
- La description du texte n�cessaire � la soumission dans un annuaire. Cette description est unique pour chaque soumission. 
- La soumission du site Internet dans l'annuaire. 
- L'acc�s � la plateforme Ereferer.  

Le client choisit les annuaires dans lesquels il souhaite �tre r�f�renc�. Ces annuaires sont ind�pendants et ne sont pas la propri�t� de la soci�t� CADOMAX.  

Le client cr�dite sur son compte le montant de son choix. Chaque soumission lui est ensuite d�bit�e au prix �num�r� ci-dessus.  

La soci�t� CADOMAX s'accorde le droit de modifier ses tarifs � tout moment. 

<h2>Clause n� 3 : Retrait et modification d'une soumission</h2> La modification ou le retrait d'une soumission dans un annuaire peut-�tre effectu�e dans la mesure du possible. Pour se faire, le client devra cr�er un projet comme � l'accoutum�e en s�lectionnant les annuaires pour lesquels des modifications pour ses soumissions doivent �tre apport�es. Le client devra ensuite bien sp�cifier dans les instructions du projet qu'il s'agit de modification ou de suppression de liens. Ses modifications ou retraits seront d�bit�s au prix de la soumission soit 1�85. La soci�t� CADOMAX ne pourra pas modifier les soumissions sur les annuaires pour lesquels elle ne dispose pas de compte. 

<h2>Clause n� 4 : Rabais et ristournes</h2> Les tarifs propos�s comprennent les rabais et ristournes que la soci�t� CADOMAX serait amen�e � octroyer compte tenu de ses r�sultats ou de la prise en charge par l'acheteur de certaines prestations. 

<h2>Clause n� 5 : Escompte</h2> Aucun escompte ne sera consenti en cas de paiement anticip�. 

<h2>Clause n� 6 : Modalit�s de paiement</h2> Le r�glement des commandes s'effectue :    soit par virement paypal;   
soit par ch�que ;   
soit par virement bancaire ;   

Lors de l'enregistrement de la commande, l'acheteur devra verser l'int�gralit� du montant global de la facture afin que commence la prestation de r�f�rencement. 

<h2>Clause n� 7 : Retard de paiement</h2> En cas de d�faut de paiement total ou partiel, la prestation de r�f�rencement n'aura pas lieu. 

<h2>Clause n� 8</h2> : La prestation La prestation est effectu�e suivant les exigences du client lors de la cr�ation de son projet de r�f�rencement. 

la soci�t� CADOMAX ne peut pas �tre tenue pour responsable de l'acceptation ou non de votre site dans un annuaire. Par ailleurs en aucun cas la soci�t� CADOMAX ne peut garantir le positionnement d'un site internet dans les moteurs de recherche 

<h2>Clause n� 9 : Force majeure</h2> La responsabilit� de la soci�t� CADOMAX ne pourra pas �tre mise en oeuvre si la non-ex�cution ou le retard dans l'ex�cution de l'une de ses obligations d�crites dans les pr�sentes conditions g�n�rales de vente d�coule d'un cas de force majeure. � ce titre, la force majeure s'entend de tout �v�nement ext�rieur, impr�visible et irr�sistible au sens de l'article 1148 du Code civil. 

<h2>Clause n� 10 : Tribunal comp�tent</h2> Tout litige relatif � l'interpr�tation et � l'ex�cution des pr�sentes conditions g�n�rales de vente est soumis au droit fran�ais.   
� d�faut de r�solution amiable, le litige sera port� devant le Tribunal de commerce de Nantes.  
          </p>  </p>
        </div>


        <!-- Pagination -->

        <!-- End pagination -->
    </div>

</div>
<?PHP
include("files/includes/bottomBas.php")
?>