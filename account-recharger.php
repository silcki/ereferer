<?PHP
@session_start();

$a = 6;
$b = 1;
$page = 1;
include('files/includes/topHaut.php');
?>


    <!--breadcrumbs ends -->
    <div class="container">
        <div>

            <?PHP
            include("files/includes/menu.php");
            ?>
            <div class="three-fourth">
                <div>

                    <?php
                    // Showing "Expecting time to start a project"-message
                    $estimation_start_days = 0;
                    if (isWebmaster()){
                        $projectsModel = new Projects($dbh);
                        $estimation_start_days = $projectsModel->getEstimatedStartDaysCount();
                    }
                    if ($estimation_start_days > 0){
                        $days_label = ($estimation_start_days==1)? 'jour' : 'jours';
                        ?>
                        <div class="notification notice" style="display:inline-block;">
                            Le temps d'attente pour le traitement des nouveaux projets est estim� � <b><?php echo $estimation_start_days;?></b> <?php echo $days_label; ?>.
                        </div>
                    <?php } ?>



                    <div class="permalink">
                        <h4><a nohref="#">Faire une demande de rechargement</a></h4>
                    </div>

                    <?PHP
                    if (isset($_GET['statut']) && $_GET['statut'] != "") {
                        ?>
                        <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                            <p>
                                <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>-->
                                <?PHP echo $_SESSION['alertLogincontent']; 
                                ?>
                            </p>
                        </div>
                    <?PHP } ?>
                    <?PHP
                    $videsChamp = "";
                    $userGetFreshInfos = Functions::getUserInfos($idUser);
                    if (isWebmaster()) {
                        if ($userGetFreshInfos['nom'] == "") {
                            $videsChamp .= '<li>- Nom</li>';
                        }
                        if ($userGetFreshInfos['prenom'] == "") {
                            $videsChamp .= '<li>- Pr�nom(s)</li>';
                        }
                        if ($userGetFreshInfos['adresse'] == "") {
                            $videsChamp .= '<li>- Adresse Compl�te
                        </li>';
                        }
                        if ($userGetFreshInfos['codepostal'] == "") {
                            $videsChamp .= '<li>- Code Postal</li>';
                        }
                        if ($userGetFreshInfos['ville'] == "") {
                            $videsChamp .= '<li>- Ville</li>';
                        }
                    }

                    if ($videsChamp == "") {
                        ?>
                        <p>
                            Veuillez saisir le montant que vous souhaitez recharger dans votre compte.<br/>
                        </p>
                        <form action="/compte/ajouter/recharger.html" method="post" style="margin-top:0px;float:left;" enctype="multipart/form-data">
                            <fieldset style="">
                                <input name="montant" class="search-field" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"/>
                                <?PHP echo $_SESSION['devise']; ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <strong style="color:red;">(Attention une TVA de <?PHP echo $_SESSION['allParameters']['tax']["valeur"]; ?>% s'applique pour les particuliers et les entreprises fran�aises. Si vous �tes hors-France, vous devez le mentionnez dans <a href="/compte/modifier/profil.html">votre profil</a>)</strong>
                                <br/>
                            </fieldset>
                            <!--
                            <fieldset class="one">
                                <input name="tax" type="checkbox" value='1' style="margin-top:5px;float:left;margin-right:10px;dislpay:inline;" id="tax" >
                                <label style="display:inline;color:green" for="tax">TVA non applicable ( entreprise hors France ).</label>
                                <br/>
                                <br/>
                            </fieldset>
                            -->
                            <input type="submit" class="button color small round" value="Recharger" style="color:white;"/>

                        </form>
                        <div style="clear:both;"></div>
                        <br/>
                        La facture se trouvera dans votre rubrique "<b>Mon Compte > Mes Factures</b>".<br> Merci de bien v�rifier que vos coordonn�es de facturation sont � jour.<br/>
                        <br/>
                        Les modes de paiement possibles:<br/>

                        <strong>- Paypal</strong><br/>
                        <strong>- Virement bancaire</strong><br/>

                        N'h�sitez pas � m'ajouter sur Skype : <strong>emmanuel.higel</strong>
                        
                    <?PHP
                    } else {
                        ?>
                        <p style="">
                            <br/>
                            Votre adresse de facturation est incompl�te. <br/>
                            Veuillez compl�tez les champs suivants sur votre <a href="./compte/modifier/profil.html">profil</a> :<br/>
                        <ul style="color:red;">
                            <?PHP
                            echo $videsChamp;
                            ?>
                        </ul>
                        <br/>
                        <a href="/compte/modifier/profil.html">Cliquez ici pour compl�ter votre profil</a>
                        </p>
                    <?PHP
                    }
                    ?>
                </div>
                <br/>
                <hr/>
                <br/>

                <div style="margin-top:50px;">
                    <?PHP
                    $requests = Functions::getPaiementsListMe();
                    $count = 0;

                    $count = count($requests);
                    include("pages.php");
                    if ($count > 0) {

                        if (isWebmaster()) {
                            echo '<h4>Vos demandes de rechargements en attente.</h4>';
                        } else {
                            echo '<h4>Vos demandes de paiement en attente.</h4>';
                        }
                        ?>
                        <table class="simple-table">
                            <tbody>
                            <tr>
                                <th>
                                    Montant
                                </th>
                                <th>
                                    Date de la requete
                                </th>
                                <th>
                                    Statut
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            <?PHP
                            while ($limit1 <= $limit2) {
                                ?>
                                <tr>
                                    <td>
                                        <?PHP
                                        echo $requests[$limit1]['requestAmount'];

                                        if ($requests[$limit1]['type'] == 4) {
                                            echo " <i class='icon-euro'></i>";
                                        } else {
                                            echo " <i class='icon-dollar'></i>";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?PHP echo date("d/m/Y � H:m:s", $requests[$limit1]['time']); ?>
                                    </td>
                                    <td>
                                        En attente
                                    </td>
                                    <td>
                                        <a href="/compte/supprimer/requete.html?id=<?PHP echo $requests[$limit1]['id']; ?>" onclick="return confirmation();" ><i class="icon-remove"></i> Annuler</a>
                                    </td>
                                </tr>
                                <?PHP
                                $limit1++;
                            }
                            ?>
                            </tbody>
                        </table>
                        <!-- Pagination -->
                    <?PHP
                    }
                    ?>
                    <!-- End pagination -->
                    <!-- Pagination -->

                    <!-- End pagination -->
                </div>
            </div>

        </div>
    </div>
<?PHP
include("files/includes/bottomBas.php")
?>