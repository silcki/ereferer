<?PHP
@session_start();

if (isset($_SESSION['connected'])) {
    header("location:compte/accueil.html");
}

$a = 2;
$page = 2;
include('files/includes/topHaut.php');

if (!empty($_POST['login-input']) && Functions::checkEmailFormat($_POST['login-input'])) {

    /* @depricated
    $requete = "SELECT * FROM utilisateurs WHERE username='" . addslashes($_POST['login-input']) . "'";
    $execution = $dbh->query($requete);
    $retour = $execution->fetch();*/

    // prepared
    $stmt = $dbh->prepare("SELECT * FROM utilisateurs
                           WHERE username = :username ");
    $stmt->bindParam(':username', $_POST['login-input']);
    if ($stmt->execute())
        $retour = $stmt->fetch(PDO::FETCH_ASSOC);

    if (isset($retour['email'])) {
        if ($retour['active'] == 1) {
            unset($_SESSION['alertLogincontent']);
            $newPassGen = time();
            $newPass = Functions::getCryptePassword($newPassGen);
            $setPass = false;
            $name = $_SESSION['allParameters']['logotext']["valeur"];


            /* @deprecated
            $setPass = $dbh->query('UPDATE utilisateurs SET password="' . $newPass . '" WHERE id=' . $retour['id']);
            */

            // prepared
            $stmt = $dbh->prepare("UPDATE utilisateurs SET password = :password
                                   WHERE id = :id");

            $stmt->bindParam(':password', $newPass );
            $stmt->bindParam(':id',$retour['id']);

            $setPass = $stmt->execute();


            if ($setPass) {
                @require_once('./files/includes/phpmailer/class.phpmailer.php');
                $body = '';
                $body .= 'Bonjour,<br/><br/>';
                $body .= 'Votre nouveau mot de passe pour Ereferer est: <strong>' . $newPassGen.'</strong><br/><br/>';
                $body .= 'Pensez &agrave; le changer si possible.<br/><br/>';
                $body .= 'Cordialement,<br/>';
                $body .= 'Emmanuel';
                
                $mail = new PHPMailer();
                $mail->Timeout = '30';
                $mail->CharSet = 'UTF-8';
                $mail->From = "noreply@ereferer.fr";
                $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
                $mail->Subject = "R&eacute;initialisation de votre mot de passe sur Ereferer.";
                $mail->AltBody = "";
                $mail->IsHTML(true);
                $mail->MsgHTML($body);
                $mail->AddAddress($retour['username'], "");
                $mail->Send();
            }
        } else {
            $_SESSION['alertLogincontent'] = "Votre compte est d�sactiv�. Contactez un administrateur.";
        }
    } else {
        $_SESSION['alertLogincontent'] = "Ce compte est introuvable. Veuillez r�essayer svp.";
    }
} else {
    $_SESSION['alertLogincontent'] = "Format de l'adresse email incorrecte.";
}

if (isset($_SESSION['alertLogincontent']) && $_SESSION['alertLogincontent'] != "") {
    header('location:reset.html?statut=error');
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <div class="permalink">
            <h4><a nohref="#">R�initialisation d�roul�e avec succ�s.</a></h4>
        </div>

        <div class="notification success" style="width:500px;">
            <p>
                <span>Succ�s</span> 
                Votre demande de r�initialisation de mot de passe est r�ussie. <br/>
                Un email contenant votre nouveau mot de passe vous � �t� envoyer.<br/><br/>
                
               <strong> Veuillez consultez votre bo�te de r�ception ou votre dossier spam.</strong><br/><br/>

                Merci.
            </p>
        </div>



        <!-- Pagination -->

        <!-- End pagination -->

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>