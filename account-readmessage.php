<?PHP
@session_start();

$a = 3;
$b = 1;
$page = 1;
include('files/includes/topHaut.php');
?>

<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">
            <?PHP
            $getMessage = Functions::getMessageList($_SESSION['connected']['id']);


            $count = count($getMessage);

            include("pages.php");

            if ($count > 0) {
                ?>

                <!--<h4>Jquery Accordion</h4>-->
                <ul class="accordion">
                    <?PHP
                    $style = "";
                    while ($limit1 <= $limit2) {
                        if ($getMessage[$limit1]['viewReceiver'] == 1) {
                            $style = "";
                            $lienShow = "nohref=''";
                            $selected = "";
                            $selected2 = "display: none;";
                            if (isset($_GET['messageID']) && $_GET['messageID'] == $getMessage[$limit1]['id']) {
                                $selected = "selected";
                                $selected2 = "display: block;";
                            }

                            if ($getMessage[$limit1]['lu'] == 1) {
                                $style = "font-weight:bold;color:red;";
//                            $lienShow = 'href="' . $_SERVER['REDIRECT_URL'] . "?messageID=" . $getMessage[$limit1]['id'] . '"';
                                $lienShow = 'nohref="" id="' . $getMessage[$limit1]['id'] . '"';
                            }
                            ?>
                            <li class="accordion-item <?PHP echo $selected; ?>" >
                                <div class="accordion-switch">
                                    <span class="togglegfx"></span>
            <!--                                <a <?PHP echo $lienShow; ?> class="Messagerie">-->
                                    <?PHP
                                    echo '<span class="message_subject" data-id="'.$getMessage[$limit1]['id'].'"><span style="' . $style . '" class="Messagerie" id="' . $getMessage[$limit1]['id'] . '">' . $getMessage[$limit1]['sender'] . '</span> : ' . stripslashes($getMessage[$limit1]['objet']) . '</span>';
                                    ?>
                                    <!--</a>-->
                                    <div style="float:right;">
                                        <a href="compte/writemessage.html?id=<?PHP echo $getMessage[$limit1]['id']; ?>" style="margin-right:5px;" onclick="">
                                            <i class="icon-reply"></i> R�pondre
                                        </a>
                                        <a href="compte/supprimer/message.html?id=<?PHP echo $getMessage[$limit1]['id']; ?>" style="margin-right:5px;" onclick="return confirmation();">
                                            <i class="icon-trash"></i> Supprimer
                                        </a>
                                    </div>
                                </div>

                                <div class="accordion-content">
                                    <p>
                                        <?PHP echo nl2br(stripslashes($getMessage[$limit1]['content'])); ?>
                                    </p>
                                </div>

                            </li>
                            <?PHP
                        }
                        $limit1++;
                    }
                    ?>
                </ul>
            </div>

            <!-- Pagination -->
            <?PHP
            if ($nbre_result > pagination) {
                echo $list_page;
            }
        } else {
            ?>
            <div align="center" width="500" style="width:700px;margin:auto;">
                <br/>
                <br/>
                <h1>
                    Aucun message dans ce dossier.
                </h1>
            </div>

            <?PHP
        }
        ?>
        <!-- End pagination -->


    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>