<?PHP

@session_start();

//if (isset($_GET['facture_id']) && !empty($_GET['facture_id']) && intval($_GET['facture_id']) > 0) {
//    require_once("files/includes/config/configuration.php");
//    require_once("files/includes/functions.php");
//    require_once("./files/includes/fpdf/fpdf.php");


define('EURO', chr(128));

class PDF_HTML extends FPDF {

    var $B = 0;
    var $I = 0;
    var $U = 0;
    var $HREF = '';
    var $ALIGN = '';
    var $widths;
    var $aligns;

    function WriteHTML($html) {
        //Parseur HTML
        $html = str_replace("\n", ' ', $html);
        $a = preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($a as $i => $e) {
            if ($i % 2 == 0) {
                //Texte
                if ($this->HREF)
                    $this->PutLink($this->HREF, $e);
                elseif ($this->ALIGN == 'center')
                    $this->Cell(0, 5, $e, 0, 1, 'C');
                else
                    $this->Write(5, $e);
            }
            else {
                //Balise
                if ($e[0] == '/')
                    $this->CloseTag(strtoupper(substr($e, 1)));
                else {
                    //Extraction des attributs
                    $a2 = explode(' ', $e);
                    $tag = strtoupper(array_shift($a2));
                    $prop = array();
                    foreach ($a2 as $v) {
                        if (preg_match('/([^=]*)=["\']?([^"\']*)/', $v, $a3))
                            $prop[strtoupper($a3[1])] = $a3[2];
                    }
                    $this->OpenTag($tag, $prop);
                }
            }
        }
    }

    function OpenTag($tag, $prop) {
        //Balise ouvrante
        if ($tag == 'B' || $tag == 'I' || $tag == 'U')
            $this->SetStyle($tag, true);
        if ($tag == 'A')
            $this->HREF = $prop['HREF'];
        if ($tag == 'BR')
            $this->Ln(5);
        if ($tag == 'P')
            $this->ALIGN = $prop['ALIGN'];
        if ($tag == 'HR') {
            if (!empty($prop['WIDTH']))
                $Width = $prop['WIDTH'];
            else
                $Width = $this->w - $this->lMargin - $this->rMargin;
            $this->Ln(2);
            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetLineWidth(0.4);
            $this->Line($x, $y, $x + $Width, $y);
            $this->SetLineWidth(0.2);
            $this->Ln(2);
        }
    }

    function CloseTag($tag) {
        //Balise fermante
        if ($tag == 'B' || $tag == 'I' || $tag == 'U')
            $this->SetStyle($tag, false);
        if ($tag == 'A')
            $this->HREF = '';
        if ($tag == 'P')
            $this->ALIGN = '';
    }

    function SetStyle($tag, $enable) {
        //Modifie le style et s�lectionne la police correspondante
        $this->$tag+=($enable ? 1 : -1);
        $style = '';
        foreach (array('B', 'I', 'U') as $s)
            if ($this->$s > 0)
                $style.=$s;
        $this->SetFont('', $style);
    }

    function PutLink($URL, $txt) {
        //Place un hyperlien
        $this->SetTextColor(0, 0, 255);
        $this->SetStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->SetStyle('U', false);
        $this->SetTextColor(0);
    }

    function SetWidths($w) {
        //Tableau des largeurs de colonnes
        $this->widths = $w;
    }

    function SetAligns($a) {
        //Tableau des alignements de colonnes
        $this->aligns = $a;
    }

    function Row($data) {
        //Calcule la hauteur de la ligne
        $nb = 0;
        for ($i = 0; $i < count($data); $i++)
            $nb = max($nb, $this->NbLines($this->widths[$i], $data[$i]));
        $h = 5 * $nb;
        //Effectue un saut de page si n�cessaire
        $this->CheckPageBreak($h);
        //Dessine les cellules
        for ($i = 0; $i < count($data); $i++) {
            $w = $this->widths[$i];
            $a = isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Sauve la position courante
            $x = $this->GetX();
            $y = $this->GetY();
            //Dessine le cadre
            $this->Rect($x, $y, $w, $h);
            //Imprime le texte
            $this->MultiCell($w, 5, $data[$i], 0, $a);
            //Repositionne � droite
            $this->SetXY($x + $w, $y);
        }
        //Va � la ligne
        $this->Ln($h);
    }

    function CheckPageBreak($h) {
        //Si la hauteur h provoque un d�bordement, saut de page manuel
        if ($this->GetY() + $h > $this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w, $txt) {
        //Calcule le nombre de lignes qu'occupe un MultiCell de largeur w
        $cw = &$this->CurrentFont['cw'];
        if ($w == 0)
            $w = $this->w - $this->rMargin - $this->x;
        $wmax = ($w - 2 * $this->cMargin) * 1000 / $this->FontSize;
        $s = str_replace("\r", '', $txt);
        $nb = strlen($s);
        if ($nb > 0 and $s[$nb - 1] == "\n")
            $nb--;
        $sep = -1;
        $i = 0;
        $j = 0;
        $l = 0;
        $nl = 1;
        while ($i < $nb) {
            $c = $s[$i];
            if ($c == "\n") {
                $i++;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
                continue;
            }
            if ($c == ' ')
                $sep = $i;
            $l+=$cw[$c];
            if ($l > $wmax) {
                if ($sep == -1) {
                    if ($i == $j)
                        $i++;
                }
                else
                    $i = $sep + 1;
                $sep = -1;
                $j = $i;
                $l = 0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

    function Finish() {
        exit();
    }

}

$data = Functions::getFacturesByid(intval($_GET['facture_id']));

// definition de la TVA. Exemple : TVA = 2 equivaut a 2%;
//$tva = 0;

if ($data && !empty($data) && (isSu() || $data['user'] == $idUser)) {

    $userInfos = Functions::getUserInfos($data['user']);
    $text0 = "FACTURE";
//
        $text1 = "EURL CADOMAX - SIRET : 52281558800010";
        $text2 = "Emmanuel Higel";
        $text3 = "26 Rue des ifs";
        $text4 = "44240 La chapelle sur Erdre, France";


//    $text1 = "Netaudit Limited";
//    $text2 = "Office 2611, Office Tower Langham Place";
//    $text3 = "8, Argyle Street, Kowloon";
//    $text4 = "Hong Kong";

    $texte4_ = "RCS Nantes B 522 815 588";
    $texte4__ = "FR22424761419";

//************************************************

    $text5 = "                                              ";
    $text5 .= "DESIGNATION";
    $text5 .= "                                                                                       ";


    $text6 = "          ";
    $text6 .= "MONTANT HT";


    //var_dump(' Facture_id '.$_GET['facture_id'].' tva = '.$data['tva']);

    //return;

    if (!empty($data['tva'])){
        $tva = 20;
    }else{
        $tva = 0;
    }


    $text7 = "TVA applicable au taux de : " . $tva . "%";
//    $text8 = "Dispens� d'immatriculation  au r�gistre de commerce et des soci�t�s (RCS) et au r�pertoires des";
//    $text9 = "m�tiers (RM) TVA non applicable, art. 293 B du CGI";
    
    if($tva==0 && $userInfos['country']!="France"){
        $text8 = "Vente d'une prestation de service intracommunautaire � un pays membre de l'UE";
        $text9 = "Autoliquidation par le preneur (Art. 283-2 du CGI) - N� de TVA : ".$userInfos['numtva'];
    }

    $text8 = "";
    $text9 = "";

    $texte10 = "IBAN: FR76 3000 3020 3300 0203 1287 679";
    $texte11 = "BIC/SWIFT: SOGEFRPP";
//    $texte11_ = "ACCOUNT NUMBER : 817-456452-838";

    //$data['amount'] = 37;


    if (!empty($data['tva'])){
        $tvaAmount = (20*$data['amount']/100);
        $tvaAmount = round($tvaAmount,2);
    }else{
        $tva = 0;
        $tvaAmount = (($tva * $data['amount']) / 100);
    }

    $netAmount = $data['amount'] + $tvaAmount;

//********************************************************

    $client1 = $userInfos['nom'] . " " . $userInfos['prenom'];
    $client2 = $userInfos['societe'];
    $client3 = $userInfos['adresse'];
    $client4 = $userInfos['codepostal'] . "," . $userInfos['ville'].",".$userInfos['country'];

    $numFacture = "00" . date('Y') . "" . $data['id'];
    $textFacture = "FACTURE N� " . $numFacture;
    $textDate = "DATE: " . date("d/m/Y", $data['time']);

    $objet = "OBJET:";
    $objetSuite = $data['amount'] . EURO . " de rechargement sur ereferer.";

//    $ContentFacture1 = $data['amount'] . EURO . " de credit sur ereferer\n";
    $ContentFacture1 = "Credit de rechargement sur ereferer\n";
    $ContentFacture1 .= "TVA";
    $ContentFacture1 .= "\n\n\n\n\n\n\n";
    $ContentFacture1 .= $text7;
    $ContentFacture1 .= "\n";
    $ContentFacture1 .= $text8;
    $ContentFacture1 .= $text9;
    $ContentFacture1 .= "\n\n";
    $ContentFacture2 = $data['amount'] . EURO . "\n";
    $ContentFacture2 .= $tvaAmount . EURO . " ";
    $ContentFacture3 = "                                                                            ";
    $ContentFacture3 .= "                                                                       ";
    $ContentFacture3 .= "TOTAL TTC";
    $ContentFacture4 = $netAmount . EURO . " ";
    $ContentFacture5 = "Avec nos remerciements !";

//    ob_get_clean();
    $pdf = new PDF_HTML();
    $pdf->AddPage();
    $pdf->SetFont('Arial');
    $pdf->WriteHTML($text1 . '');
    $pdf->SetFont('Arial', 'B', '19');
    $pdf->SetTextColor('145', '145', '145');
    $pdf->WriteHTML('                               ');
    $pdf->WriteHTML('' . $text0 . '<br>');
    $pdf->SetTextColor('0', '0', '0');
    $pdf->SetFont('Arial', '', '12');
    $pdf->WriteHTML($text2 . '<br>');
    $pdf->WriteHTML($text3 . '<br>');
    $pdf->WriteHTML($text4 . '');
    $pdf->SetFont('Arial', '', '8');
    $pdf->WriteHTML('                                                   ');
    $pdf->WriteHTML('                                                   ');
    $pdf->WriteHTML('' . $textFacture . '');
    $pdf->WriteHTML('                                                   ');
    $pdf->WriteHTML('                                                   ');
    $pdf->WriteHTML('                                                   ');
    $pdf->WriteHTML('                                                   ');
    $pdf->WriteHTML('' . $textDate . '<br>');

    $pdf->SetFont('Arial', 'B', '8');
    $pdf->WriteHTML('                                                           ');
    $pdf->WriteHTML('                                                           ');
    $pdf->WriteHTML('' . $objet . '<br>');
    $pdf->SetFont('Arial', '', '8');
    $pdf->WriteHTML('                                                           ');
    $pdf->WriteHTML('                                                           ');
    $pdf->WriteHTML('' . $objetSuite . '<br><br>');

    $pdf->SetFont('Arial', 'B', '13');
    $pdf->WriteHTML($client1 . '<br>');
    $pdf->WriteHTML($client2 . '<br>');
    $pdf->WriteHTML($client3 . '<br>');
    $pdf->WriteHTML($client4 . '<br>');
    $pdf->WriteHTML('<br>');
    $pdf->WriteHTML('<br>');
    $pdf->WriteHTML('<br>');

    $pdf->SetWidths(array(135, 50));
    $pdf->SetFont('Arial', 'B', '11');
    $pdf->Row(array($text5, $text6));
    $pdf->SetFont('Arial', '', '8');
    $pdf->Row(array('' . $ContentFacture1 . '', '' . $ContentFacture2 . ''));

    $pdf->SetFont('Arial', '', '8');
    $pdf->Row(array($ContentFacture3, $ContentFacture4));
    $pdf->WriteHTML('<br>');
    $pdf->WriteHTML('<br>');
    $pdf->SetFont('Arial', '', '8');
    $pdf->WriteHTML($texte10 . '<br>');
    $pdf->WriteHTML($texte11 . '<br>');
    $pdf->WriteHTML($texte11_ . '<br>');
    $pdf->WriteHTML('<br>');
    $pdf->WriteHTML('                                               ');
    $pdf->WriteHTML('                                               ');
    $pdf->SetFont('Arial', 'B', '11');
    $pdf->WriteHTML($ContentFacture5 . '<br>');

//        $dir = "./files/factures/";
//        $fileName = $dir . "ereferer-facture-" . $numFacture . ".pdf";
//        $pdf->Output($dirFactures.$fileName, "F");
}
//    else {
//        echo '<h2> Ce fichier est introuvable..</h2>';
//    }
//} else {
//    echo '<h2> Ce fichier est introuvable.</h2>';
//}


?>