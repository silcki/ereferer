<?PHP
@session_start();
$redirectURL = str_replace('&statut=error', '', @$_SERVER['HTTP_REFERER']);
$redirectURL = str_replace('?statut=error', '', $redirectURL);



$a = 0;
$b = 0;

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

if (isset($_GET['block']) && ($_GET['block'] == "profil" || $_GET['block'] == "password")) {
//    $z = 8;
//    $a = 5;
}

if (isset($_GET['block']) && ($_GET['block'] == "redaction_affectationgroupee") ) {
    $a = 40;
    $b = 30;
}

// all true
$_GET['filtrageavance'] = 1;

$page = 1;
include('files/includes/topHaut.php');
						//error_reporting(E_ALL);

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Likes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionViewHelper.php';

//echo "ddd".$_SESSION['b'];

$label = "Modifier";
$specialWidth = "";
$oneNot = "three-fourth";

if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

if (isset($_GET['block']) && in_array($_GET['block'], array('password', 'profil', 'parametres', 'affectationgroupee', 'redaction_affectationgroupee' ,'repartition'))) {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $_GET['id'] = isset($_GET['id']) ? intval($_GET['id']) : $idUser;
    } else {
        $_GET['id'] = $idUser;
    }
    $currentUser = Functions::getUserInfos($_GET['id']);
}
if (empty($_GET['id'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
    exit;
}
if (isset($_GET['block']) && ($_GET['block'] == "listannuairecontent" )) {
    $specialWidth = "width:950px;";
    $oneNot = "one";
}

$idMod = $_GET['id'];

?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        if (isset($_GET['block']) && ($_GET['block'] != "listannuairecontent" )) {
            include("files/includes/menu.php");
        }
        ?>

        <?PHP
        if (isset($_GET['block']) && ($_GET['block'] == "listannuairecontent" )) {
            ?>
            <style>
                #container{
                    width:1200px;
                }
                .container{
                    margin-left:30px;
                }
                #main-navigation ul {
                    margin-left:-30px;
                }
            </style>
            <?PHP
        }
        ?>

        <div class="<?PHP echo $oneNot; ?>">
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:600px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>-->
                        <?PHP echo $_SESSION['alertLogincontent']; ?>
                    </p>
                </div>
            <?PHP } ?>

            <form action="compte/modification/<?PHP echo $_GET['block']; ?>.html?id=<?PHP echo $idMod; ?>&iduser=<?PHP echo isset($_GET['iduser']) ? intval($_GET['iduser']) : 0; ?>" method="post" style="margin-top:0px;float:left;<?PHP echo $specialWidth; ?>" enctype="multipart/form-data">
                <?PHP
                if ($_GET['block'] == "annuaire") {
                    $page = $_SERVER['HTTP_REFERER'];
                    $result = Functions::getAnnuaire($idMod);

                    if ($result["importedBy"] == $idUser || isAdmin() || isSu()) {
//                        $result = "";
                        ?>
                        <h4>Modification de l'annuaire : <?PHP echo $result["annuaire"]; ?></h4>
                        <fieldset style="">
                            <!--<legend class="color">Connexion</legend>-->
                            <?PHP
                            if (isSU() || isAdmin()) {
                                ?>
                                <select name="webmasterPartenaire" class="select" style="width:400px;">
                                    <option value="0">S�lectionner le responsable de l'annuaire</option>
                                    <?PHP
                                    echo Functions::getUserOptionList(4, 1, $result['webmasterPartenaire']);
                                    ?>
                                </select>
                                <?PHP
                            }
                            ?>
                        </fieldset>
                        <?php
                        if (isSU() || isAdmin()) {
                            ?>
                        <fieldset class="one" style="clear:both" class="">
                            <br/>
                            <div class="accept_option" style="clear:both">
                                <label  style="float:left;" >Accepte les sous-domaines:</label>
                                <div style="float:left;margin-left:10px;" >
                                    <input <?php echo ($result["accept_inner_pages"] == 1)? 'checked="checked"': '';  ;?> value="1" name="accept_inner_pages" type="radio"/> Oui&nbsp;&nbsp;&nbsp;
                                    <input <?php echo ($result["accept_inner_pages"] == 0)? 'checked="checked"': '';  ;?> name="accept_inner_pages" value="0" type="radio"/> Non
                                </div>
                            </div>

                            <div class="accept_option" style="clear:both">
                                <label  style="float:left;" >Accepte les sites sans mentions l�gales:</label>
                                <div style="float:left;margin-left:10px;" >
                                    <input <?php echo ($result["accept_legal_info"] == 1)? 'checked="checked"': '';  ;?> value="1" name="accept_legal_info" type="radio"/> Oui&nbsp;&nbsp;&nbsp;
                                    <input <?php echo ($result["accept_legal_info"] == 0)? 'checked="checked"': '';?> name="accept_legal_info"  value="0"  type="radio"/> Non
                                </div>
                            </div>

                            <div class="accept_option" style="clear:both">
                                <label  style="float:left;" >N'accepte que les entreprises:</label>
                                <div style="float:left;margin-left:10px;" >
                                    <input <?php echo ($result["accept_company_websites"] == 1)? 'checked="checked"': '';?>  value="1"  name="accept_company_websites" type="radio"/> Oui&nbsp;&nbsp;&nbsp;
                                    <input <?php echo ($result["accept_company_websites"] == 0)? 'checked="checked"': '';?>  value="0"  name="accept_company_websites" type="radio"/> Non
                                </div>
                            </div>
                        </fieldset>

                         <?php } ?>


                        <fieldset class="one" style="clear:both">
                            <br/>
                            <label style="width:300px;">Possibilit� de cr�er un compte au webmaster:</label>
                            <div style="width:100px;float:left;margin-right:10px;">
                                <input name="poss" class="search-field" type="radio" value='0' style="float:left;" maxlength="300" id="possnon"  <?PHP
                                if ($result["ComptePersoWebmaster"] == 0) {
                                    echo 'checked="checked"';
                                }
                                ?>>
                                <label for="possnon" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                            </div>

                            <div style="width:50px;float:left;margin-right:10px;">
                                <input name="poss" class="search-field" type="radio" value='1' style="float:left;" maxlength="300" id="possoui" <?PHP
                                if ($result["ComptePersoWebmaster"] == 1) {
                                    echo 'checked="checked"';
                                }
                                ?>>
                                <label for="possoui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                            </div>

                            <br/>
                            <br/>
                        </fieldset>

                        <fieldset style="clear:both;">
                            <label class="color">Comission par validation:</label>
                            <input name="WebPartenairePrice" class="search-field" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);" value="<?PHP
                            echo $result["WebPartenairePrice"];
                            ?>"><i class="icon-euro"></i> (Euro)                        
                            <br>
                            <br/>

                        </fieldset>
                        <fieldset class="one-fourth" style="clear:both">
                            <label style="width:300px;">Le webmaster peut-il choisir son ancre? :</label>
                            <div style="width:100px;float:left;margin-right:10px;">
                                <input name="ancre" class="search-field ancreDisplaY" type="radio" value='0' style="float:left;" maxlength="300" id="ancrenon" <?PHP
                                if ($result["webmasterAncre"] == 0) {
                                    echo 'checked="checked"';
                                }
                                ?>>
                                <label for="ancrenon" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                            </div>

                            <div style="width:50px;float:left;margin-right:10px;">
                                <input name="ancre" class="search-field ancreDisplaY" type="radio" value='1' style="float:left;" maxlength="300" id="ancreoui"  <?PHP
                                if ($result["webmasterAncre"] == 1) {
                                    echo 'checked="checked"';
                                }
                                ?>>
                                <label for="ancreoui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                            </div>
                            <br/>
                        </fieldset>

                        <fieldset class="one-fourth cleared" id="ancreConsigneDisplay" style="display:block;">
                            <br/>
                            <label><span></span>Consigne de l'ancre (facultatif):

                            </label>
                            <input class="search-field" type="text" style="float:left;width:400px" name="consignesAncre" value="<?PHP
                            echo $result["webmasterConsigne"];
                            ?>"/>
                            <br/>
                            <br/>
                        </fieldset>
                        <fieldset class="one-fourth" style="clear:both">

                            <label style="width:300px;">Type d'annuaire:</label>
                            <div style="width:100px;float:left;margin-right:10px;">
                                <input name="cp" class="search-field" type="radio" value='0' style="float:left;" maxlength="300" id="cpnon" <?PHP
                                if ($result["cp"] == 0) {
                                    echo 'checked="checked"';
                                }
                                ?>>
                                <label for="cpnon" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Annuaire</label>
                            </div>

                            <div style="width:50px;float:left;margin-right:10px;">
                                <input name="cp" class="search-field" type="radio" value='1' style="float:left;" maxlength="300" id="cpoui" <?PHP
                                if ($result["cp"] == 1) {
                                    echo 'checked="checked"';
                                }
                                ?>>
                                <label for="cpoui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">CP</label>
                            </div>
                            <br/>
                            <br/>
                        </fieldset>

                        <fieldset style="clear:both;">
                            <input name="annuaire" class="search-field" type="text" style="margin-bottom:5px;width:300px;float:left" value="<?PHP echo $result["annuaire"]; ?>">
                            <br/>
                            <br/>
                        </fieldset>


                        <!-- <Parsing> -->
                        <fieldset style="clear:both;">
                            <label>NDDCible:</label>
                            <?php
                            $nddcible = empty($result["nddcible"]) ? $result["annuaire"] : $result["nddcible"];
                            ?>
                            <input name="nddcible" class="search-field" type="text" style="margin-bottom:5px;width:300px;float:left" value="<?PHP echo $nddcible; ?>">
                            <br/>
                            <br/>
                        </fieldset>
						
						<fieldset style="clear:both;">
                            <label>Page de soumission:</label>
                            <?php
                            $link_submission= $result["link_submission"];
                            ?>
                            <input name="link_submission" class="search-field" type="text" style="margin-bottom:5px;width:500px;float:left" value="<?PHP echo $link_submission; ?>">
                            <br/>
                            <br/>
                        </fieldset>



                        <fieldset style="clear:both;"> 
                            <label style="width:175px; float:left;">Pages count for checking:</label>
                            <input name="page_count" class="search-field" type="text" style="margin-bottom:5px;width:50px;float:left" value="<?PHP echo $result['page_count']; ?>">
                            <br/><br/>
                        </fieldset>

                        <fieldset style="clear:both;">
                            <label class="color">Tarif extra Webmaster:</label>
                            <input name="tarifW" class="search-field" value="<?PHP echo $result['tarifW']; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-euro"></i> (Euro)                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Tarif extra R�f�renceur:</label>
                            <input name="tarifR" class="search-field" value="<?PHP echo $result['tarifR']; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"><i class="icon-dollar"></i> (Dollar)                        <br>
                        </fieldset>
						
						<fieldset class="one-fourth cleared">
                            <label><span></span>Nombre de mots minimum :
                            </label>
                            <input name="min_words_count" class="search-field"  value="<?PHP echo $result["min_words_count"]; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">
						</fieldset>

                        <fieldset class="one-fourth cleared">
                            <label><span></span>Nombre de mots maximum :
                            </label>
                            <input name="max_words_count" class="search-field"  value="<?PHP echo $result["max_words_count"]; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        
						</fieldset>	
						
                        <fieldset style="clear:both;">
                            <label class="color">Date de cr�ation:</label>
                            <?PHP
                            $age = "";
                            if (intval($result["age"]) > 0) {
                                $age = date('d-m-Y', $result["age"]);
                            } else {
                                
                            }
                            ?>
                            <input name="age" id="datedemarrage" class="search-field"  value="<?PHP echo $age; ?>" type="text" style="margin-bottom:5px;width:100px;float:left;margin-right:5px;" onkeyup="">                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Trust Flow:</label>
                            <input name="tf" class="search-field"  value="<?PHP echo $result["tf"]; ?>" type="text" style="margin-bottom:5px;width:80px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                        </fieldset>
                        <!--                        
                                                <fieldset style="clear:both;">
                                                    <label class="color">AlexaRank:</label>
                                                    <input name="ar" class="search-field" value="0" type="text" style="margin-bottom:5px;width:80px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                                                </fieldset>-->
						<?php /*
                        <fieldset class="one-fourth" style="clear:both;">
                            <label>PageRank:</label>
                            <input name="page_rank" class="search-field" type="text" style="margin-bottom:5px;width:80px;float:left"  value="<?PHP echo $result['page_rank']; ?>">
                        </fieldset> */ ?>
						
						
                        <!--                        <fieldset style="clear:both;">
                                                    <label class="color">MozRank:</label>
                                                    <input name="mr" class="search-field"  value="<?PHP echo $result["mr"]; ?>" type="text" style="margin-bottom:5px;width:80px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                                                </fieldset>-->
                        <fieldset style="clear:both;">
                            <label class="color">Total Domaine r�f�rent:</label>
                            <input name="tdr" class="search-field"  value="<?PHP echo $result["tdr"]; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                        </fieldset>
                        <fieldset style="clear:both;">
                            <label class="color">Total Backlink:</label>
                            <input name="tb" class="search-field"  value="<?PHP echo $result["tb"]; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);">                        <br>
                        </fieldset>
                        <!-- </Parsing> -->


                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span> </span>Statut de l'annuaire: </label>
                            <div style="width:80px;float:left;">
                                <input name="retour" <?PHP
                                if ($result["active"] == 1) {
                                    echo 'checked="checked"';
                                }
                                ?> class="search-field" type="radio" value='1' style="float:left" maxlength="300" id="oui">
                                <label for="oui" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Activ�</label>
                            </div>
                            <div style="width:90px;float:left;margin-left:20px;">
                                <input name="retour" <?PHP
                                if ($result["active"] == 0) {
                                    echo 'checked="checked"';
                                }
                                ?> class="search-field" type="radio" value='0' style="float:left" maxlength="300" id="non">
                                <label for="non" style="width:15px;margin-top:-5px;margin-left:5px;float:left;">D�sactiv�</label>
                            </div>
                            <br/>
                        </fieldset>


                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span> </span>Avantages VIP?: </label>
                            <div style="float:left;margin-left:10px;" >
                                <input <?php echo ($result["vip_state"] == 1)? 'checked="checked"': '';?>  value="1"  name="vip_state" type="radio"/> Oui&nbsp;&nbsp;&nbsp;
                                <input <?php echo ($result["vip_state"] == 0)? 'checked="checked"': '';?>  value="0"  name="vip_state" type="radio"/> Non
                            </div>
                            <br/>
                        </fieldset>


                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span></span>Texte avantage :
                            </label>
                            <textarea rows="15" cols="" name="vip_text" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP echo stripslashes($result["vip_text"]); ?></textarea><br/>
                        </fieldset>


                        <!--                        <fieldset class="one-fourth cleared">
                                                    <br/>
                                                    <label><span> </span>Afficher les ancres : </label>
                                                    <div style="width:50px;float:left;margin-right:10px;">
                                                        <input name="display" <?PHP
                        if ($result["display"] == 1) {
                            echo 'checked="checked"';
                        }
                        ?> class="search-field" type="radio" value='1' style="float:left" maxlength="300" id="oui_">
                                                        <label for="oui_" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Oui</label>
                                                    </div>
                                                    <div style="width:50px;float:left;margin-right:10px;">
                                                        <input name="display" <?PHP
                        if ($result["display"] == 0) {
                            echo 'checked="checked"';
                        }
                        ?> class="search-field" type="radio" value='0' style="float:left" maxlength="300" id="non_">
                                                        <label for="non_" style="width:10px;margin-top:-5px;margin-left:5px;float:left;">Non</label>
                                                    </div>
                                                    <br/>
                                                </fieldset>-->


                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span></span>Consignes de l'annuaire :
                            </label>
                            <textarea rows="15" cols="" name="consignes" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP echo stripslashes($result["consignes"]); ?></textarea><br/>
                        </fieldset>
		
						<div class="clearfix"></div>

                        <input name="redir" value="./../annuaires.html#annuaire<?PHP echo $idMod; ?>" type="hidden"/>
                        <fieldset class="one-fourth" style="width:500px;">
                            <br/>
                            <br/>

                            <label><span>* </span>Modifier les listes parent:</label>
                            <?PHP
//                            $ids = 0;
                            echo Functions::getUserAnnuairelistCheckCase($idMod);
                            ?>
                        </fieldset>

                        <?PHP
                    }
                } else if ($_GET['block'] == "affectationgroupeez") {



                    $result = Functions::getCategorieName($idMod);
                    echo $_POST['okChecked'];
                    if (!isset($_POST['okChecked'])) {
                        $_SESSION['alertLogincontent'] .= "Vous n'avez cocher aucune case<br/>";
                        header("location:" . $redirectURL . "&statut=error");
                    }
                    ?>
                    <h4>Modification de la cat�gorie : <?PHP echo $result; ?></h4>

                    <input name="categorie" class="search-field" type="text" style="margin-bottom:5px;width:300px;float:left" value="<?PHP echo $result; ?>"><br/>

                    <?PHP
                } else if ($_GET['block'] == "commentaire") {
                    $result = Functions::getCommentaire($idMod);
                    if (!$result) {
                        $result['commentaire'] = "";
                    }
//                    print_r($result);
                    ?>
                    <h4>Modifier un commentaire : </h4>

                    <textarea rows="10" cols="" name="commentaire" class="search-field" style="margin-bottom:5px;width:400px;height:150px;font-size:11px;"><?PHP echo stripslashes($result['commentaire']); ?></textarea><br/>
                    <input name="redir" value="<?PHP echo urlencode($_SERVER['HTTP_REFERER']); ?>" type="hidden"/>

                    <?PHP
                } else if ($_GET['block'] == "password" && (isAdmin() || isSu() || (isReferer() && $idUser == $idMod) || (isWebmaster() && $idUser == $idMod))) {
//                    $result = Functions::getCategorieName($idMod);
                    ?>
                    <h4>Modification du mot de passe de : <?PHP echo Functions::getfullname($idMod); ?></h4>
                    <?PHP if (isWebmaster() || isReferer() || ((isSu() || isAdmin()) && $idMod == $idUser)) { ?>
                        <fieldset class="one-fourth">
                            <label><span>* </span>Ancien mot de passe: </label>
                            <input name="oldpass" class="search-field" type="password" value='' style="margin-bottom:5px;width:400px;"><br/>
                        </fieldset>
                    <?PHP } ?>
                    <fieldset class="one-fourth">
                        <label><span>* </span>Nouveau mot de passe: </label>
                        <input name="newpass" class="search-field" type="password" value='' style="margin-bottom:5px;width:400px;"><br/>
                    </fieldset>
                    <fieldset class="one-fourth">
                        <label><span>* </span>Confirmer nouveau mot de passe: </label>
                        <input name="confnewpass" class="search-field" type="password" value='' style="margin-bottom:5px;width:400px;"><br/>
                    </fieldset>
                    <?PHP
                } else if ($_GET['block'] == "profil" && (isAdmin() || isSu() || (isReferer() && $idUser == $idMod) || (isWebmaster() && $idUser == $idMod))) {
                    if (isSuperReferer() && $idUser != $idMod) {
                        
                    }
                    $result = Functions::getUserInfos($idMod);
                    ?>
                    <?PHP if ((isSu() || isAdmin()) && $idMod != $idUser) { ?>
                        <a class="button color small round" href="./compte/modifier/solde.html?id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 15px 5px;">Solde</a>
                        <a class="button color small round" href="./compte/accueil.html?section=projects&iduser=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Projets</a>
                        <?PHP if ($result['lastIP'] == 111111) { ?> 
                            <a class="button color small round" href="./compte/utilisateurs.html?section=affiliation&byuser=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Affili�s</a>
                        <?PHP } ?> 
                        <a class="button color small round" href="./compte/modifier/password.html?id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Mot de passe</a>
                        <?PHP if ($result['active'] == "1") { ?> 
                            <a class="button color small round" href="./compte/modification/utilisateurdisable.html?id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">D�sactiver</a>
                        <?PHP } else { ?> 
                            <a class="button color small round" href="./compte/modification/utilisateurenable.html?id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Activer</a>
                        <?PHP } ?> 
                        <a class="button color small round" href="./compte/supprimer/utitlisateur.html?id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Supprimer</a>
						
                        <a class="button color small round" href="./compte/writemessage.html?id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Message</a>
                        
                        <? $sites = Functions::getSitesList(array('user-id'=>$idMod)); if(count($sites)){ ?>
                            <a class="button color small round" href="./compte/echangesgestion/liste.html?user-id=<?PHP echo $idMod; ?>" style="color:white;float:right;margin:0px 0px 5px 5px;">Voir ses sites</a>
                        <? } ?>
                    <?PHP } ?>

					
					<?php /*		
                    <fieldset class="one-fourth">
                        <label><span>* </span>Modifier Avatar </label>
                        <?PHP
                        if ($result['avatar'] == "") {
                            $result['avatar'] = 'images/default.png';
                        }
                        ?>

                        <img src="<?PHP echo $result['avatar']; ?>" width="100" height="100"/>
                        <input name="avatar" class="search-field" type="hidden" value='<?PHP echo $result['avatar']; ?>' style="margin-bottom:5px;width:350px;"><br/>
                        <input name="avatar" class="search-field" type="file" style="margin-bottom:5px;width:400px;">

                    </fieldset> */ ?>

                    <?PHP if ((isSu() || isAdmin()) && $idMod != $idUser) { ?>
                        <fieldset class="one-fourth cleared">
                            <label><span> </span>Type utilisateur: </label>
                            <select name="typeutilisateur">
                                <option value="0">S�lectionner groupe</option>
                                <option value="<?PHP echo $result['typeutilisateur']; ?>" selected="selected"><?PHP echo $arrayLvl[$result['typeutilisateur']]; ?></option>
                                <option value="2">Administrateurs</option>
                                <option value="3">R�f�renceurs</option>
                                <option value="4">Webmasters</option>
                            </select>
                        </fieldset>
                    <?PHP } ?>
                    <fieldset class="one-fourth cleared">
                        <label><span>* </span>Nom </label>
                        <input name="nom" class="search-field" type="text" value='<?PHP echo stripslashes($result['nom']); ?>' style="margin-bottom:5px;width:350px;"><br/>
                    </fieldset>

                    <fieldset class="one-fourth">
                        <label><span>* </span>Pr�nom(s) </label>
                        <input name="prenom" class="search-field" type="text" value='<?PHP echo stripslashes($result['prenom']); ?>' style="margin-bottom:5px;width:350px;"><br/>
                    </fieldset>

                    <fieldset class="one-fourth">
                        <label><span> </span>T�l�phone </label>
                        <input name="telephone" class="search-field" type="text" value='<?PHP echo stripslashes($result['telephone']); ?>' style="margin-bottom:5px;width:350px;" onkeyup="ve(this);"><br/>
                    </fieldset>
                    <fieldset class="one-fourth cleared">
                            <label><span> </span>Pays </label>
                            <select name="country" id="country" style="margin-bottom:5px;width:370px;">
                                <option value="France">France </option>
                                <option value="Afghanistan">Afghanistan </option>
                                <option value="Afrique_Centrale">Afrique_Centrale </option>
                                <option value="Afrique_du_sud">Afrique_du_Sud </option> 
                                <option value="Albanie">Albanie </option>
                                <option value="Algerie">Algerie </option>
                                <option value="Allemagne">Allemagne </option>
                                <option value="Andorre">Andorre </option>
                                <option value="Angola">Angola </option>
                                <option value="Anguilla">Anguilla </option>
                                <option value="Arabie_Saoudite">Arabie_Saoudite </option>
                                <option value="Argentine">Argentine </option>
                                <option value="Armenie">Armenie </option> 
                                <option value="Australie">Australie </option>
                                <option value="Autriche">Autriche </option>
                                <option value="Azerbaidjan">Azerbaidjan </option>
                                <option value="Bahamas">Bahamas </option>
                                <option value="Bangladesh">Bangladesh </option>
                                <option value="Barbade">Barbade </option>
                                <option value="Bahrein">Bahrein </option>
                                <option value="Belgique">Belgique </option>
                                <option value="Belize">Belize </option>
                                <option value="Benin">Benin </option>
                                <option value="Bermudes">Bermudes </option>
                                <option value="Bielorussie">Bielorussie </option>
                                <option value="Bolivie">Bolivie </option>
                                <option value="Botswana">Botswana </option>
                                <option value="Bhoutan">Bhoutan </option>
                                <option value="Boznie_Herzegovine">Boznie_Herzegovine </option>
                                <option value="Bresil">Bresil </option>
                                <option value="Brunei">Brunei </option>
                                <option value="Bulgarie">Bulgarie </option>
                                <option value="Burkina_Faso">Burkina_Faso </option>
                                <option value="Burundi">Burundi </option>
                                <option value="Caiman">Caiman </option>
                                <option value="Cambodge">Cambodge </option>
                                <option value="Cameroun">Cameroun </option>
                                <option value="Canada">Canada </option>
                                <option value="Canaries">Canaries </option>
                                <option value="Cap_vert">Cap_Vert </option>
                                <option value="Chili">Chili </option>
                                <option value="Chine">Chine </option> 
                                <option value="Chypre">Chypre </option> 
                                <option value="Colombie">Colombie </option>
                                <option value="Comores">Colombie </option>
                                <option value="Congo">Congo </option>
                                <option value="Congo_democratique">Congo_democratique </option>
                                <option value="Cook">Cook </option>
                                <option value="Coree_du_Nord">Coree_du_Nord </option>
                                <option value="Coree_du_Sud">Coree_du_Sud </option>
                                <option value="Costa_Rica">Costa_Rica </option>
                                <option value="Cote_d_Ivoire">C�te_d_Ivoire </option>
                                <option value="Croatie">Croatie </option>
                                <option value="Cuba">Cuba </option>
                                <option value="Danemark">Danemark </option>
                                <option value="Djibouti">Djibouti </option>
                                <option value="Dominique">Dominique </option>
                                <option value="Egypte">Egypte </option> 
                                <option value="Emirats_Arabes_Unis">Emirats_Arabes_Unis </option>
                                <option value="Equateur">Equateur </option>
                                <option value="Erythree">Erythree </option>
                                <option value="Espagne">Espagne </option>
                                <option value="Estonie">Estonie </option>
                                <option value="Etats_Unis">Etats_Unis </option>
                                <option value="Ethiopie">Ethiopie </option>
                                <option value="Falkland">Falkland </option>
                                <option value="Feroe">Feroe </option>
                                <option value="Fidji">Fidji </option>
                                <option value="Finlande">Finlande </option>
                                <option value="France">France </option>
                                <option value="Gabon">Gabon </option>
                                <option value="Gambie">Gambie </option>
                                <option value="Georgie">Georgie </option>
                                <option value="Ghana">Ghana </option>
                                <option value="Gibraltar">Gibraltar </option>
                                <option value="Grece">Grece </option>
                                <option value="Grenade">Grenade </option>
                                <option value="Groenland">Groenland </option>
                                <option value="Guadeloupe">Guadeloupe </option>
                                <option value="Guam">Guam </option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guernesey">Guernesey </option>
                                <option value="Guinee">Guinee </option>
                                <option value="Guinee_Bissau">Guinee_Bissau </option>
                                <option value="Guinee equatoriale">Guinee_Equatoriale </option>
                                <option value="Guyana">Guyana </option>
                                <option value="Guyane_Francaise ">Guyane_Francaise </option>
                                <option value="Haiti">Haiti </option>
                                <option value="Hawaii">Hawaii </option> 
                                <option value="Honduras">Honduras </option>
                                <option value="Hong_Kong">Hong_Kong </option>
                                <option value="Hongrie">Hongrie </option>
                                <option value="Inde">Inde </option>
                                <option value="Indonesie">Indonesie </option>
                                <option value="Iran">Iran </option>
                                <option value="Iraq">Iraq </option>
                                <option value="Irlande">Irlande </option>
                                <option value="Islande">Islande </option>
                                <option value="Israel">Israel </option>
                                <option value="Italie">italie </option>
                                <option value="Jamaique">Jamaique </option>
                                <option value="Jan Mayen">Jan Mayen </option>
                                <option value="Japon">Japon </option>
                                <option value="Jersey">Jersey </option>
                                <option value="Jordanie">Jordanie </option>
                                <option value="Kazakhstan">Kazakhstan </option>
                                <option value="Kenya">Kenya </option>
                                <option value="Kirghizstan">Kirghizistan </option>
                                <option value="Kiribati">Kiribati </option>
                                <option value="Koweit">Koweit </option>
                                <option value="Laos">Laos </option>
                                <option value="Lesotho">Lesotho </option>
                                <option value="Lettonie">Lettonie </option>
                                <option value="Liban">Liban </option>
                                <option value="Liberia">Liberia </option>
                                <option value="Liechtenstein">Liechtenstein </option>
                                <option value="Lituanie">Lituanie </option> 
                                <option value="Luxembourg">Luxembourg </option>
                                <option value="Lybie">Lybie </option>
                                <option value="Macao">Macao </option>
                                <option value="Macedoine">Macedoine </option>
                                <option value="Madagascar">Madagascar </option>
                                <option value="Mad�re">Mad�re </option>
                                <option value="Malaisie">Malaisie </option>
                                <option value="Malawi">Malawi </option>
                                <option value="Maldives">Maldives </option>
                                <option value="Mali">Mali </option>
                                <option value="Malte">Malte </option>
                                <option value="Man">Man </option>
                                <option value="Mariannes du Nord">Mariannes du Nord </option>
                                <option value="Maroc">Maroc </option>
                                <option value="Marshall">Marshall </option>
                                <option value="Martinique">Martinique </option>
                                <option value="Maurice">Maurice </option>
                                <option value="Mauritanie">Mauritanie </option>
                                <option value="Mayotte">Mayotte </option>
                                <option value="Mexique">Mexique </option>
                                <option value="Micronesie">Micronesie </option>
                                <option value="Midway">Midway </option>
                                <option value="Moldavie">Moldavie </option>
                                <option value="Monaco">Monaco </option>
                                <option value="Mongolie">Mongolie </option>
                                <option value="Montserrat">Montserrat </option>
                                <option value="Mozambique">Mozambique </option>
                                <option value="Namibie">Namibie </option>
                                <option value="Nauru">Nauru </option>
                                <option value="Nepal">Nepal </option>
                                <option value="Nicaragua">Nicaragua </option>
                                <option value="Niger">Niger </option>
                                <option value="Nigeria">Nigeria </option>
                                <option value="Niue">Niue </option>
                                <option value="Norfolk">Norfolk </option>
                                <option value="Norvege">Norvege </option>
                                <option value="Nouvelle_Caledonie">Nouvelle_Caledonie </option>
                                <option value="Nouvelle_Zelande">Nouvelle_Zelande </option>
                                <option value="Oman">Oman </option>
                                <option value="Ouganda">Ouganda </option>
                                <option value="Ouzbekistan">Ouzbekistan </option>
                                <option value="Pakistan">Pakistan </option>
                                <option value="Palau">Palau </option>
                                <option value="Palestine">Palestine </option>
                                <option value="Panama">Panama </option>
                                <option value="Papouasie_Nouvelle_Guinee">Papouasie_Nouvelle_Guinee </option>
                                <option value="Paraguay">Paraguay </option>
                                <option value="Pays_Bas">Pays_Bas </option>
                                <option value="Perou">Perou </option>
                                <option value="Philippines">Philippines </option> 
                                <option value="Pologne">Pologne </option>
                                <option value="Polynesie">Polynesie </option>
                                <option value="Porto_Rico">Porto_Rico </option>
                                <option value="Portugal">Portugal </option>
                                <option value="Qatar">Qatar </option>
                                <option value="Republique_Dominicaine">Republique_Dominicaine </option>
                                <option value="Republique_Tcheque">Republique_Tcheque </option>
                                <option value="Reunion">Reunion </option>
                                <option value="Roumanie">Roumanie </option>
                                <option value="Royaume_Uni">Royaume_Uni </option>
                                <option value="Russie">Russie </option>
                                <option value="Rwanda">Rwanda </option>
                                <option value="Sahara Occidental">Sahara Occidental </option>
                                <option value="Sainte_Lucie">Sainte_Lucie </option>
                                <option value="Saint_Marin">Saint_Marin </option>
                                <option value="Salomon">Salomon </option>
                                <option value="Salvador">Salvador </option>
                                <option value="Samoa_Occidentales">Samoa_Occidentales</option>
                                <option value="Samoa_Americaine">Samoa_Americaine </option>
                                <option value="Sao_Tome_et_Principe">Sao_Tome_et_Principe </option> 
                                <option value="Senegal">Senegal </option> 
                                <option value="Seychelles">Seychelles </option>
                                <option value="Sierra Leone">Sierra Leone </option>
                                <option value="Singapour">Singapour </option>
                                <option value="Slovaquie">Slovaquie </option>
                                <option value="Slovenie">Slovenie</option>
                                <option value="Somalie">Somalie </option>
                                <option value="Soudan">Soudan </option> 
                                <option value="Sri_Lanka">Sri_Lanka </option> 
                                <option value="Suede">Suede </option>
                                <option value="Suisse">Suisse </option>
                                <option value="Surinam">Surinam </option>
                                <option value="Swaziland">Swaziland </option>
                                <option value="Syrie">Syrie </option>
                                <option value="Tadjikistan">Tadjikistan </option>
                                <option value="Taiwan">Taiwan </option>
                                <option value="Tonga">Tonga </option>
                                <option value="Tanzanie">Tanzanie </option>
                                <option value="Tchad">Tchad </option>
                                <option value="Thailande">Thailande </option>
                                <option value="Tibet">Tibet </option>
                                <option value="Timor_Oriental">Timor_Oriental </option>
                                <option value="Togo">Togo </option> 
                                <option value="Trinite_et_Tobago">Trinite_et_Tobago </option>
                                <option value="Tristan da cunha">Tristan de cuncha </option>
                                <option value="Tunisie">Tunisie </option>
                                <option value="Turkmenistan">Turmenistan </option> 
                                <option value="Turquie">Turquie </option>
                                <option value="Ukraine">Ukraine </option>
                                <option value="Uruguay">Uruguay </option>
                                <option value="Vanuatu">Vanuatu </option>
                                <option value="Vatican">Vatican </option>
                                <option value="Venezuela">Venezuela </option>
                                <option value="Vierges_Americaines">Vierges_Americaines </option>
                                <option value="Vierges_Britanniques">Vierges_Britanniques </option>
                                <option value="Vietnam">Vietnam </option>
                                <option value="Wake">Wake </option>
                                <option value="Wallis et Futuma">Wallis et Futuma </option>
                                <option value="Yemen">Yemen </option>
                                <option value="Yougoslavie">Yougoslavie </option>
                                <option value="Zambie">Zambie </option>
                                <option value="Zimbabwe">Zimbabwe </option>
                                </select>
                        </fieldset>
                        
                        <fieldset class="one-fourth cleared" style="display:none;">
                            <label><span> </span>N� de TVA Intracommunautaire (Obligatoire pour �tre exon�r� de TVA) </label>
                            <input id="numtva" name="numtva" class="search-field" type="text" value='<?PHP echo stripslashes($result['numtva']); ?>' style="margin-bottom:5px;width:350px;"><br/>
                        </fieldset>
                         <script type="text/javascript">

                 var Europe=["Allemagne",
                "Autriche",
                "Belgique",
                "Bulgarie",
                "Chypre",
                "Croatie",
                "Danemark",
                "Espagne",
                "Estonie",
                "Finlande",
                "Grece",
                "Hongrie",
                "Irlande",
                "Italie",
                "Lettonie",
                "Lituanie",
                "Luxembourg",
                "Malte",
                "Pays_Bas",
                "Pologne",
                "Portugal",
                "Replublique_Tcheque",
                "Roumanie",
                "Royaume_Uni",
                "Slovaquie",
                "Slovenie",
                "Suede"];

                 jQuery("#country").val("<?PHP echo stripslashes($result['country']); ?>");;

                   var r=jQuery("#country").val();
                    if(Europe.indexOf(r) !== -1){
                        
                        jQuery("#numtva").parent().show("fast");

                    } else {
                        
                        jQuery("#numtva").parent().hide("fast");
                    
                    }

               jQuery("#country").change(function(){
                
            
                    var r=jQuery("#country").val();
                    if(Europe.indexOf(r) !== -1){
                        
                        jQuery("#numtva").parent().show("fast");

                    } else {
                        
                        jQuery("#numtva").parent().hide("fast");
                    
                    }

               });


               </script>
    
                    <fieldset class="one-fourth cleared">
                        <label><span> </span>Adresse </label>
                        <input name="adresse" class="search-field" type="text" value='<?PHP echo stripslashes($result['adresse']); ?>' style="margin-bottom:5px;width:350px;"><br/>
                    </fieldset>

                    <fieldset class="one-fourth cleared">
                        <label><span> </span>Code Postal </label>
                        <input name="codepostal" class="search-field" type="text" value='<?PHP echo stripslashes($result['codepostal']); ?>' style="margin-bottom:5px;width:350px;"><br/>
                    </fieldset>

                    <fieldset class="one-fourth cleared">
                        <label><span> </span>Ville </label>
                        <input name="ville" class="search-field" type="text" value='<?PHP echo stripslashes($result['ville']); ?>' style="margin-bottom:5px;width:350px;"><br/>
                    </fieldset>

                    <fieldset class="one-fourth">
                        <label><span> </span>Site Web Personnel </label>
                        <input name="siteweb" class="search-field" type="text" value='<?PHP echo $result['siteweb']; ?>' style="margin-bottom:5px;width:350px;"><br/>
                    </fieldset>

                    <fieldset class="one-fourth">
                        <label><span>* </span>Adresse Email </label>
                        <input name="email" class="search-field" type="text" value='<?PHP echo $result['email']; ?>' style="margin-bottom:5px;width:350px;margin-right:100px;"><br/>
                    </fieldset>

                    <?PHP if (isWebmaster() || ((isSu() || isAdmin()) && $idMod != $idUser && isWebmaster($result['typeutilisateur']))) { ?>
                        <fieldset class="one-fourth">
                            <label><span> </span>Soci�t� </label>
                            <input name="societe" class="search-field" type="text" value='<?PHP echo stripslashes($result['societe']); ?>' style="margin-bottom:5px;width:350px;margin-right:100px;"><br/>
                        </fieldset>
                    <?PHP } ?>



                    <?PHP
                    if ((isSu($idTypeUser) || isAdmin()) && $idMod != $idUser) {
                        $show = $result['frais'];
                        if (isWebmaster($result['typeutilisateur'])) {
                            $show = Functions::getRemunuerationWebmaster() . ' <i class="icon-euro style=""></i>';
                        }
                        if (isReferer($result['typeutilisateur'])) {
                            $show = Functions::getRemunueration() . ' <i class="icon-dollar" style=""></i>';
                        }
                        ?>



                        <fieldset class="one-fourth">
                            <label>Tarif par soumission :<br/>
                                <span class="tips">Remettre ce champ � "0" pour appliquer le tarif par d�faut en vigeur: <strong><?PHP echo $show; ?></strong> </span> 
                            </label>
                            <input name="frais" class="search-field" type="text" value='<?PHP echo $result['frais']; ?>' style="margin-bottom:5px;width:30px;float:left;margin-right:5px;">
                            <?PHP
                            if (isReferer($result['typeutilisateur'])) {
                                ?>
                                <i class="icon-dollar" style=""></i>
                                <?PHP
                            }
                            if (isWebmaster($result['typeutilisateur'])) {
                                ?>
                                <i class="icon-euro" style=""></i>
                                <?PHP
                            }
                            ?>
                            <br/>

                        </fieldset>
                        <?PHP
                        if (isWebmaster($result['typeutilisateur'])) {
                            $checkedF = "";
                            if ($result['lastIP'] == 111111) {
                                $checkedF = "checked='checked'";
                            }
                            ?>
                            <hr/>
                            <br/>
                            <br/>
                            <div class="cleared" style="">
                                <fieldset class="one-fourth">
                                    <input name="lastIP" type="checkbox" value='111111' style="margin-top:5px;float:left;margin-right:10px;dislpay:inline;" id="affiliation" <?PHP echo $checkedF; ?>>
                                    <label style="display:inline;color:orangered" for="affiliation">Compte Affiliation</label>
                                    <br/>
                                </fieldset>  

                                <fieldset class="one-fourth cleared" style="">
                                    <label style="color:orangered">Tarif par Affiliation  :
                                        <span class="tips">Remettre ce champ � "0" pour appliquer le tarif par d�faut en vigeur: <strong><?PHP echo $_SESSION['allParameters']['affiliation']["valeur"]; ?> <i class="icon-euro" style=""></i></strong> </span> 
                                    </label>
                                    <input name="styleAdmin" class="search-field" type="text" value='<?PHP echo intval($result['styleAdmin']); ?>' style="margin-bottom:5px;width:30px;float:left;margin-right:5px;">
                                    <i class="icon-euro" style=""></i>
                                    <br/>
                                </fieldset>    
                            </div>


                            <?PHP
                        }
                        ?>

                        <?PHP if ((isSu() || isAdmin())) { ?>
                            
                            <div class="accept_option" style="clear:both">
                                <br/>
                                <label  style="float:left;" >Webmaster de confiance</label>
                                <div style="float:left;margin-left:10px;" >
                                    <input <?php echo ($result["trusted"] == 1)? 'checked="checked"': '';  ;?> value="1" name="trusted" type="checkbox"/>
                                </div>
                            </div>

                            <div class="accept_option" style="clear:both">
								<br/>
                                <label  style="float:left;" >Projet cach� au r�dacteur:</label>
                                <div style="float:left;margin-left:10px;" >
                                    <input <?php echo ($result["writers_choosing"] == 1)? 'checked="checked"': '';  ;?> value="1" name="writers_choosing" type="checkbox"/>
                                </div>
                            </div>
							
							<div class="accept_option" style="clear:both">
								<br/>
                                <label  style="float:left;" >Projet bonus:</label>
                                <div style="float:left;margin-left:10px;" >
                                    <input <?php echo ($result["bonus_projects"] == 1)? 'checked="checked"': '';  ;?> value="1" name="bonus_projects" type="checkbox"/>
                                </div>
                            </div>


							
                            <div class="accept_option" style="clear:both">
								<br/>
                                <label  style="float:left;" >Tarif r�daction:</label>
                                <div style="float:left;margin-left:10px;" >
                                        <input name="tarif_redaction" class="search-field" type="text" value='<?PHP echo floatval($result['tarif_redaction']); ?>' style="margin-bottom:5px;width:30px;float:left;margin-right:5px;">
										 <?PHP
										if (isReferer($result['typeutilisateur'])) {
											?>
											<i class="icon-dollar" style=""></i>
											<?PHP
										}
										if (isWebmaster($result['typeutilisateur'])) {
											?>
											<i class="icon-euro" style=""></i>
											<?PHP
										}
										?>		
								</div>
                            </div>		

							<?php if (isReferer($result['typeutilisateur'])){ ?>
								<div class="accept_option" style="clear:both">
									<label  style="float:left;" >R�daction:</label>
									<div style="float:left;margin-left:10px;" >
										<input <?php echo ($result["redaction_writer"] == 1)? 'checked="checked"': '';  ;?> value="1" name="redaction_writer" type="checkbox"/>
									</div>
								</div>		

								<div class="accept_option" style="clear:both">
									<label  style="float:left;" >Annuaire:</label>
									<div style="float:left;margin-left:10px;" >
										<input <?php echo ($result["annuaire_writer"] == 1)? 'checked="checked"': '';  ;?> value="1" name="annuaire_writer" type="checkbox"/>
									</div>
								</div>
							<?php } ?>		
							
                        <?PHP } ?>

                        <?PHP
                        if (isReferer($result['typeutilisateur'])) {
                            $checkedF = "";
                            if ($result['lastIP'] == 111111) {
                                $checkedF = "checked='checked'";
                            }
                            ?>
                            <hr/>
                            <br/>
                            <br/>
                            <div class="cleared" style="">
                                <fieldset class="one-fourth">
                                    <input name="lastIP" type="checkbox" value='111111' style="margin-top:5px;float:left;margin-right:10px;dislpay:inline;" id="affiliation" <?PHP echo $checkedF; ?>>
                                    <label style="display:inline;color:orangered" for="affiliation">Compte R�f�renceur-Admin</label>
                                    <br/>
                                </fieldset>  

                                <fieldset class="one-fourth cleared" style="">
                                    <label style="color:orangered">
                                        <!--Tarif par Affiliation  :-->
                                        <!--<span class="tips">Remettre ce champ � "0" pour appliquer le tarif par d�faut en vigeur: <strong><?PHP echo $_SESSION['allParameters']['affiliation']["valeur"]; ?> <i class="icon-euro" style=""></i></strong> </span>--> 
                                    </label>
                                    <!--<input name="styleAdmin" class="search-field" type="hidden" value='<?PHP echo intval($result['styleAdmin']); ?>' style="margin-bottom:5px;width:30px;float:left;margin-right:5px;">-->
                                    <!--<i class="icon-euro" style=""></i>-->
                                    <br/>
                                </fieldset>    
                            </div>

                            <?PHP
                        }
                        ?>
                    <?PHP } ?>

                    <?PHP
                } else if ($_GET['block'] == "site" && (isAdmin() || isSu() || isWebmaster())) {
                    $label = "Suivant >";

                    if (isSU() || isAdmin()) {
                        $result = Functions::getCommande($_GET['id'], 0, 1);
                    } else {
                        $result = Functions::getCommande($_GET['id'], $idUser);
                    }
					
					// redirect 
					if (empty($result) && isWebmaster()){
						Functions::redirectJS('/');
						die;					
					}
			
                    ?>
					<script>
                            (function($){
							
                                $(document).ready(function(){

                                    var annuairelist_modify_btn = $('.annuairelist_count_link[data-action_type="modify"]');
                                    var annuairelist_setup_btn = $('.annuairelist_count_link[data-action_type="setup"]');

                                    $("select[name=annuaire]").on('change',function() {
                                        var selected_id = $(this).val();
                                        var words_count = $(this).children('option').filter(':selected').data('words_count');

                                        var list_annuiare_url = '/compte/modifier/listannuairecontent.html?id=';

                                        console.log(selected_id);

                                        // reset
                                        $(annuairelist_modify_btn).hide();
                                        $(annuairelist_setup_btn).hide();
                                        $("#annuaire_list_words_count").html('');

                                        if (selected_id > 0) {
                                            $(".modify_annuaire").show();

                                            var href = list_annuiare_url+selected_id;

                                            if (words_count>0){
                                                $("#annuaire_list_words_count").html(words_count);
                                                $(annuairelist_modify_btn).find('a').prop('href',href);
                                                $(annuairelist_modify_btn).show();
                                            }else{
                                                $(annuairelist_setup_btn).find('a').prop('href',href);
                                                $(annuairelist_setup_btn).show();
                                            }
                                        }else{
                                            $(".modify_annuaire").hide();
                                        }
                                    });

                                    $("select[name=annuaire]").trigger('change');
									
								});	
									
						    })(jQuery);
                    </script>			
					
                    <h4>Modification du projet : <?PHP echo $result['lien']; ?></h4>
						

                    <fieldset style="">
                        <!--<legend class="color">Connexion</legend>-->
                        <?PHP
                        if (isSU() || isAdmin()) {
                            ?>
                            <select name="proprietaire" class="select">
                                <option value="0">Choisir propri�taire</option>
                                <?PHP
                                echo Functions::getUserOptionList(4, 1, $result['proprietaire']);
                                ?>
                            </select>
                            <?PHP
                        }
                        ?>
                    </fieldset>
					
					
                    <?PHP /*
                    if ($result['envoyer'] == 0) {
                        ?>
                        <fieldset class="one-fourth">
                            <label><span>* </span>URL du ou des site(s) 
                                <br/> <i class="title">Si vous avez plusieurs sites, veuillez les s�parer d'un saut � la ligne.</i>
                            </label>
                            <textarea rows="10" cols="" name="site" class="search-field" style="margin-bottom:5px;width:400px;height:150px;font-size:11px;"><?PHP echo $result['lien']; ?></textarea><br/>
                        </fieldset>
                    <?PHP } else { ?>
                        <fieldset class="one-fourth">
                            <br/>
                            <label><span>* </span>URL du  site 
                                <!--<br/> <i class="title">Si vous avez plusieurs sites, veuillez les s�parer d'un saut � la ligne.</i>-->
                            </label>
                            <input name="site" class="search-field" type="text" value='<?PHP echo $result['lien']; ?>' style="float:left;width:400px">
                        </fieldset>
                    <?PHP } */ ?>



                    <?PHP
                    if ($result['envoyer'] == 0 || ($result['envoyer'] == 1 && (isAdmin() || isSU()))) {
                        ?>
                        <!--                        <fieldset class="one-fourth cleared">
                                                    <label><span>* </span>Budget Pr�vu</label>
                                                    <input name="budget" class="search-field" type="text" value='<?PHP echo $result['budget']; ?>' style="margin-bottom:5px;width:50px;float:left;margin-right: 5px" onkeyup="ve(this);" maxlength="6"><?PHP echo $_SESSION['devise']; ?><br/>
                                                </fieldset>-->

                    <?PHP } ?>
                    <!--                    <fieldset class="one-fourth cleared">
                                            <label><span>* </span>Cat�gorie du site</label>
                                            <select name="categorie" class="select cleared">
                                                <option value="0">Choisir la cat�gorie</option>
                    <?PHP echo Functions::getCategorieList($result['categorieID']); ?>
                                            </select>
                                        </fieldset>-->
                    <?PHP
                    if ((isWebmaster() && $result['proprietaire'] == $idUser)) {
                        ?>
                        <fieldset class="one-fourth cleared">
                            <br/>
                            <label><span>* </span>Liste d'annuaires � utiliser</label>
                            <?PHP echo Functions::getUserAnnuairelist($result['annuaire'], $result['proprietaire'],-1,1); ?>

                        </fieldset>
                    <?PHP } ?>

					<fieldset class="two-third modify_annuaire cleared" style="display: none;">
							<br/>
							<label>Nombre de mots minimum souhaites :</label>
							<div>
								 <div class="annuairelist_count_link annuairelist_count_link_modify" data-action_type="modify" ><div id="annuaire_list_words_count"></div> ( <a href="" >Cliquez-ici</a> pour modifier ce seuil associe a votre liste )</div>
								 <div class="annuairelist_count_link" data-action_type="setup"> Pas de minimum souhaite. <a href="" >Modifier votre liste d'annuaires</a> ici pour le configurer. </div>
							 </div>
					</fieldset>

					
                    <?PHP
                    $freq = Functions::getFrequence($result['frequence']);
                    $jour = $freq[1];
                    $Annu = $freq[0];
                    ?>
                    <fieldset class="one-fourth cleared" style="width:500px;">
                        <br/>
                        <label><span>* </span>Fr�quence de soumission 
                            <br/> <i class="title">Exemple : 1 annuaire(s) tous les 2 jour(s)</i>
                        </label>
                        <div style="display:inline-block;margin-bottom:5px;width:170px;float:left;">
                            <input name="annuaireFrequence" class="search-field" type="text" value='<?PHP echo $Annu; ?>' style="float:left;width:20px;" onkeyup="ve(this);" maxlength="2"> &nbsp;Annuaire(s) tous les  
                        </div>
                        <div style="display:inline-block;margin-bottom:5px;width:90px;margin-right: 5px;margin-left:0px;float:left;">
                            <input name="jourFrequence" class="search-field" type="text" value='<?PHP echo $jour; ?>' style="width:20px;float:left;" onkeyup="ve(this);" maxlength="2">&nbsp; Jour(s)
                        </div>
                        <br/>

                    </fieldset>
                    <!--                    <fieldset class="one-fourth cleared">
                                            <br/>
                                            <label><span></span>Ancres pour le r�f�rencement, 1 par ligne - Facultatif 
                                            </label><i class="title">( Laissez cette case vide si vous n'avez pas de pr�f�rence )</i>
                                            <textarea rows="15" cols="" name="ancres" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP echo stripslashes($result['ancres']); ?></textarea><br/>
                                        </fieldset>-->
                    <fieldset class="one-fourth cleared">
                        <br/>
                        <label><span></span>Quelques conseils ou instructions � nous donner ? Facultatif
                            <!--<br/> <i class="title">Pr�cisez une adresse email et son mot de passe si vous souhaitez que le r�f�renceur confirme les liens de soumissions � l'int�rieur des emails</i>-->
                        </label>
                        <textarea rows="15" cols="" name="commentaire" class="search-field" style="margin-bottom:5px;width:500px;height:250px"><?PHP echo stripslashes($result['consignes']); ?></textarea><br/>
                    </fieldset>


                    <?PHP
                } else if ($_GET['block'] == "rejeterprojet" && (isAdmin($idTypeUser) || isSu($idTypeUser))) {
                    $label = "Rejeter le projet";
                    ?>
                    <h4>Rejeter un projet</h4>
                    <p>
                        Vous tentez de rejeter un projet, veuillez svp entrer les cause du rejet.<br/>
                        T�chez d'�tre bref et pr�cis svp.
                    </p>
                    <fieldset class="one-fourth">
                        <label><span> </span>Raison du rejet: </label>
                        <input name="raison" class="search-field" type="text" value='' style="margin-bottom:5px;width:350px;" maxlength="500"><br/>
                    </fieldset>
                    <?PHP
                } else if ($_GET['block'] == "modifiercontrat") {
                    $contrat = Functions::getcontratById($idMod);
                    $label = "Modifier contrat";
					
                    ?>
                    <h4>Modifier le contrat <?PHP echo $arrayLvl[$contrat['typeUser']] ?></h4>
                    <div class="clear"></div>                     <div class="clear"></div>
                    <fieldset class="one-fourth">
                        <label><span> </span>Utilisateurs concern�s: </label>
                        <select name="typeUser">
                            <option value="0">S�lectionner groupe</option>
                            <option value="<?PHP echo $contrat['typeUser']; ?>" selected="selected"><?PHP echo $arrayLvl[$contrat['typeUser']]; ?></option>
                            <option value="2">Administrateur</option>
                            <option value="3">R�f�renceur</option>
                            <option value="4">Webmasteur</option>
                            <option value="5">Affiliation</option>
                        </select>
                    </fieldset>
                    <br/>
                    <br/>
					<fieldset class="one-fourth">
						<label><span> </span>Module: </label>
						<select name="module">
							<option value="0" selected="selected">Selectionner module</option>
							<option value="redaction" <?php if (!empty($contrat['module']) && ($contrat['module']=='redaction')) {?>selected="selected"<?php } ?>>Redaction</option>
						</select>
					</fieldset>
					<br/><br/>

                    <fieldset class="one-fourth">
                        <label><span> </span>Contenu du contrat: </label>
                        <textarea name="content" id="content" style="width:700px;height:400px;"><?PHP echo stripslashes($contrat['contrat']); ?></textarea>

                    </fieldset>
                    <div class="clear"></div>
                    <div class="clear"></div>
                    <div class="clear"></div>

                    <?PHP
                } else if (($_GET['block'] == "referenceur" || $_GET['block'] == "affectationgroupee") && ( (isAdmin() || isSu() || isSuperReferer()) )) {
                    $label = "Attribuer R�f�renceur";
                    $affectedTO = 0;
                    $oChecked = "";
				?>
                    <h4>
                        Affecter un r�f�renceur au(x) projet(s) 
                        <?PHP
                        if ($_GET['block'] == "affectationgroupee" && (!isset($_SESSION["PostProjets"]) || (isset($_SESSION["PostProjets"]) && (count($_SESSION["PostProjets"]) == 0 || empty($_SESSION["PostProjets"][0])))) && (!isset($_POST['projets']) || (isset($_POST['projets']) && (count($_POST['projets']) == 0 || empty($_POST['projets'][0]))))) {
                            $_SESSION['alertLogincontent'] = "Veuillez cocher les cases auxquelles appliquer cette action.<br/>";
                            header('location:' . $redirectURL . '?statut=error');
                        }

                        if ((isset($_POST['okChecked']) && !empty($_POST['okChecked'])) || (isset($_SESSION["PostProjets"]) && !empty($_SESSION["PostProjets"][0]))) {
                            if (isset($_SESSION["PostProjets"]) && !isset($_POST['projets']) && count($_SESSION["PostProjets"]) > 0) {
                                $oChecked = $_SESSION["okChecked"];
                                $projetsList = (array) $_SESSION["PostProjets"];
                            } else {
                                if (count($_POST['projets']) < 0) {
                                    $_SESSION['alertLogincontent'] = "Veuillez cocher les cases auxquelles appliquer cette action.<br/>";
                                    header('location:' . $redirectURL . '&statut=error');
                                }
                                $oChecked = $_POST['okChecked'];
                                $projetsList = (array) $_POST['projets'];
                            }


                            $oChecked = $_POST['okChecked'];
                            if (isset($_POST['envoyer2']) && !empty($_POST['envoyer2']) && isset($_POST['okChecked2']) && !empty($_POST['okChecked2'])) {
                                $oChecked = $_POST['okChecked2'];
                            }
                            if (isset($_POST['envoyer3']) && !empty($_POST['envoyer3']) && isset($_POST['okChecked3']) && !empty($_POST['okChecked3'])) {
                                $oChecked = $_POST['okChecked3'];
                            }


                            if ($oChecked == "suppressionGroupeeProjets") {
                                if (count($_POST['projets']) > 0 && !empty($_POST['projets'][0])) {

                                    foreach ($_POST['projets'] as $value) {
                                        if ($value != "" && $value != 0) {
                                            if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                                                $dbh->query('DELETE FROM projets WHERE id=' . intval($value));
                                            }
                                            if (isWebmaster()) {
                                                $dbh->query('DELETE FROM projets WHERE proprietaire = "' . $idUser . '" AND id=' . intval($value));
                                            }
                                        }
                                    }
                                    $_SESSION['alertLogincontent'] = "Op�ration effectu�e avec succ�s.<br/>";
                                    header('location:' . $redirectURL . '&statut=success');
                                } else {
                                    $_SESSION['alertLogincontent'] = "Veuillez cocher les cases auxquelles appliquer cette action.<br/>";
                                    header('location:' . $redirectURL . '&statut=error');
                                }
                            }

                            if ($oChecked == "arretGroupeeProjets") {
                                foreach ($_POST['projets'] as $value) {
                                    if ($value != "" && $value != 0 && (isAdmin() || isSu() || isWebmaster())) {
                                        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                                            $dbh->query('UPDATE projets SET over = "1", overTime="' . time() . '" WHERE id=' . intval($value));
                                        }
                                        if (isWebmaster()) {
                                            $dbh->query('UPDATE projets SET over = "1", overTime="' . time() . '" WHERE proprietaire = "' . $idUser . '" AND id=' . intval($value));
                                        }
                                    }
                                }
                                $_SESSION['alertLogincontent'] = "Les projets ont �t�s arr�ter avec succ�s.<br/>";
                                header('location:' . $redirectURL . '?statut=success');
                            }

//                            print_r($_POST);
                            $projetsName = "";
                            $nbreProjet = 0;
                            $sameTime = 0;
                            foreach ($projetsList as $projetsID) {
                                if ($projetsID != "" && $projetsID != 0) {
                                    $commande = Functions::getCommandeByID($projetsID);
                                    if ($oChecked == "HeureDemarrageGroupeeProjets" && ($commande['budget'] > $commande['adminApprouveTime'] || $commande['adminApprouveTime'] < $commandeCurrent['affectedTime'])) {
//                                        $_SESSION['alertLogincontent'] = "Un des projets s�lectionn�s est d�ja en cours d'ex�cution. La date de d�marrage ne peut �tre modifi�e.<br/>";
//                                        header('location: ./accueil.html?statut=warning');
                                    }
//                                    $projetsName .= ", ";
//                                    $projetsName .= $commande['lien'];
                                    ?>  
                                    <input type="hidden" name="projets[]" value="<?PHP echo $projetsID; ?>"/>
                                    <?PHP
                                    $nbreProjet++;
                                }
                            }
                            if ($nbreProjet > 0) {
                                if ($nbreProjet == 1) {
                                    $commande = Functions::getCommandeByID($projetsList[0]);
                                    $timeDemarrage = $commande['adminApprouveTime'];
                                    $projetsName = $commande['lien'];
                                    $affectedTO = $commande['affectedTO'];
                                    $timeAffected = $commande['affectedTime'];
                                    $timeSoumission = $commande['budget'];
                                    $affectedSec = explode(";", $commande['affectedSec']);
                                    $affectedSec = array_filter($affectedSec);
									$is_fixed_writer = $commande['is_fixed_writer'];
                                } else {
                                    $timeDemarrage = time();
                                    $projetsName = $nbreProjet . " projets s�lectionn�s";
                                    $affectedSec = array(0, 0, 0);
                                }
                            }

                            echo ": " . $projetsName;
                        } else {

                            $commande = Functions::getCommandeByID($idMod);
                            $affectedTO = $commande['affectedTO'];
                            $timeDemarrage = $commande['adminApprouveTime'];
                            $timeAffected = $commande['affectedTime'];
                            $timeSoumission = $commande['budget'];
                            $affectedSec = explode(";", $commande['affectedSec']);
                            $affectedSec = array_filter($affectedSec);
                            $is_fixed_writer = $commande['is_fixed_writer'];

                            ?>

                            : <?PHP echo $commande['lien']; ?>
                            <?PHP
//                            print_r($commande['affectedSec']);
                        }
//                        echo $affectedTO;
                        ?>
                    </h4>

                    <input type="hidden" name="okChecked" value="<?PHP echo $oChecked; ?>"/>
                    <?PHP if (($oChecked == "affectationGroupeeProjets" && $_GET['block'] == "affectationgroupee") || $_GET['block'] == "referenceur") { ?>
                        <fieldset class="one-fourth">
                            <label><span> </span>R�f�renceur principal: </label>
                            <select data-placeholder="Choisir R�f�renceur" name="affectedTO" class="select affected_select">

                                <?PHP
                                if (isSU() || isAdmin() || isSuperReferer()) {
                                    echo Functions::getUserOptionList(3, 1, $affectedTO);
                                }
                                ?>
                            </select>
                        </fieldset>

                        <?PHP
                        $secondary = 1;
                        while ($secondary <= $refsecNbre) {
                            ?>
                            <fieldset class="one-fourth">
                                <label style="color:orange"><span> </span>R�f�renceur Secondaire <?PHP echo $secondary; ?>: </label>
                                <select data-placeholder="Choisir R�f�renceur" name="affectedTO<?PHP echo $secondary; ?>" class="select writer_secondary">
                                    <?PHP
                                    if (isSU() || isAdmin() || isSuperReferer()) {
                                        echo Functions::getUserOptionList(3, 1, $affectedSec[($secondary - 1)]);
                                    }
                                    ?>
                                </select>
                            </fieldset>
                            <?PHP
                            $secondary++;
                        }
                        ?>
                        <div class="clearfix"></div><br/>
                        <label style="display:inline-block;">Fixed writer <input type="checkbox" name="is_fixed_writer" <?php echo (isset($is_fixed_writer) && ($is_fixed_writer))?'checked':'';?> value="1" id="is_fixed_writer"  /> </label>
					<?PHP } ?>
					<br/>
                    <?PHP
                    if ((isSu() || isAdmin()) && (($oChecked == "HeureDemarrageGroupeeProjets" && $_GET['block'] == "affectationgroupee") || ($_GET['block'] == "referenceur"))) {
                        if ($label == "") {
                            $label = "Modifier Date de d�marrage";
                        }
                        ?>
                        <fieldset style="" class="cleared one-fourth">
                            <br/>
                            <?PHP
                            $champDate = date('d-m-Y', $timeDemarrage);
                            if ($timeDemarrage < time()) {
//                                $champDate = date('d-m-Y', time());
                            }
                            ?>
                            <!--<legend class="color">Connexion</legend>-->
                            <label><span> </span>Date de d�marrage projet(s): </label>
                            <input name="datedemarrage" id="datedemarrage" class="search-field" value ="<?PHP echo $champDate; ?>" type="text" style="float:left;width:100%" onkeyup=""/>
                            <br/>
                        </fieldset>
                    <?PHP } ?>


                    <?PHP
                } else if ($_GET['block'] == "redaction_affectationgroupee") { ?>

					<?php  if (($_GET['block'] == "redaction_affectationgroupee") && ( (isAdmin() || isSu() || isSuperReferer()) )) { ?>
						<?php
							$label = "Attribuer R�f�renceur";
							$project_ids = $_POST['projets'];
							$redactionProjects  = new RedactionProjects($dbh);
							$viewHelper = new RedactionViewHelper;
							
							$project_id = $project_ids[0];
							
							$project = $redactionProjects->getProject($project_id);
							$title = $project['title'];
							//error_reporting(E_ALL);
						?>
						<input type="hidden" name="okChecked" value="redaction_affectationgroupee"/>
						<input type="hidden" name="project_id" value="<?php echo $project_id;?>"/>
					    <h4> Affecter un r�f�renceur au(x) projet(s): <?php echo utf8_decode($title);?></h4>
						
						<fieldset class="one-fourth">
                            <label><span> </span>R�f�renceur: </label>
                            <select data-placeholder="Choisir R�f�renceur" name="affectedTO" class="select affected_select">

                                <?PHP

                                if (isSU() || isAdmin() || isSuperReferer()) {
                                    echo  $viewHelper->getWritersOptionList($project_id);
                                }
                                ?>
                            </select>
                        </fieldset>
						<?php 
						?>
					
					
						
						
					<?php } ?>
				<?php
				}else if ($_GET['block'] == "repartition") {
//                    $commande = Functions::getCommandeByID($idMod);
                    $label = "R�partir les projets";
                    ?>
                    <h4>R�partir les projets</h4>
                    <fieldset class="one-fourth">
                        <label><span> </span>R�partition � partir du : </label>
                        <input name="dateinitiale" id="datedemarrage" class="search-field" value ="<?PHP echo date('d-m-Y', time()); ?>" type="text" style="float:left;width:100%" onkeyup=""/>
                        <br/>
                        <br/>
                    </fieldset>
                    <fieldset class="one-fourth cleared" style="width:500px;">
                        <label><span>* </span>Fr�quence d'ex�cution des t�ches
                            <br/> <i class="title">Exemple : 1 projet(s) tous les 2 jour(s) par r�f�renceur</i>
                        </label>
                        <div style="display:inline-block;margin-bottom:5px;width:170px;float:left;">
                            <input name="projetFrequence" class="search-field" type="text" value='1' style="float:left;width:20px;" onkeyup="ve(this);" maxlength="2"> &nbsp;projet(s) tous les  
                        </div>
                        <div style="display:inline-block;margin-bottom:5px;width:90px;margin-right: 5px;margin-left:0px;float:left;">
                            <input name="jourFrequence" class="search-field" type="text" value='1' style="width:20px;float:left;" onkeyup="ve(this);" maxlength="2">&nbsp; Jour(s)
                        </div>
                        <br/>

                    </fieldset>

                    <?PHP
                } else if ($_GET['block'] == "description") {
                    $projet = Functions::getCommandeByID($idMod);
                    $commande = Functions::getListCommentaire(3, $idMod);
                    $jobs = Functions::getJob($idMod);
//                    print_r($jobs);
                    $label = "";
                    $descriptions = "";
                    foreach ($jobs as $ligne) {
                        if (!empty($ligne['annuaireID'])) {
                            if (isSu() || isAdmin()) {
                                $descriptions .= "<u>R�f�renceur</u> : " . Functions::getfullname($ligne['affectedto']) . "\n";
                            }
                            $color = "red";
                            $texte = "Impossible";
                            echo $ligne['adminApprouve'];
                            if ($ligne['adminApprouved'] == 2) {
                                $color = "green";
                                $texte = "Effectu�e";
                            }
                            $annuaireNamed = "Annuaire Inexistant ou supprim�";
                            $conAnu = Functions::getAnnuaireName($ligne['annuaireID']);
//                            $conAnu = $ligne['annuaireID'];
                            if ($conAnu && !empty($conAnu)) {
                                $annuaireNamed = $conAnu;
                            } else {
                                $annuaireNamed = "URL de l'Annuaire Introuvable";
                            }
                            $descriptions .= "<u>Statut</u> : <span style='color:" . $color . "'>Soumission " . $texte . " le " . date("d/m/Y \� H\h : i\m\i\\n", $ligne['soumissionTime']) . "</span>\n";
                            $descriptions .= "<u>Annuaire</u> : " . $annuaireNamed . " \n";
                            $descriptions .= "<u>Description</u> : \n";
                            if ($commande[$ligne['annuaireID']]['commentaire'] != "") {
                                $descriptions .= "" . stripslashes($commande[$ligne['annuaireID']]['commentaire']) . "";
                            } else {
                                $descriptions .= "Aucune description fournie.";
                            }
                            $descriptions .= "\n";
                            $descriptions .= "--------------------------------------------------------------------------";
                            $descriptions .= "\n\n";
                        }
                    }
                    if ($descriptions == "") {
                        $descriptions = "Aucune Description pour ce projet";
                    }
                    ?>
                    <h4>R�cup�rer les descriptions du projet : <?PHP echo $projet['lien']; ?></h4>
                    <fieldset class="one-fourth">
                        <!--<label><span> </span>R�partition � partir du : </label>-->
                        <pre style="width:750px;height:500px;font-size:13px;text-overflow:ellipsis;word-wrap:break-word; padding:5px 10px 5px 5px;"><?PHP echo $descriptions; ?></pre>
                    </fieldset>

                    <?PHP
                } else if ($_GET['block'] == "solde") {
                   
                    $infosUser = Functions::getUserInfos($idMod);

                    $devises = Functions::getDevises($infosUser['devise']);
                    $deviseUser = "<i class='icon-" . $devises["libelle"] . "'></i> (" . ucfirst($devises["libelle"]) . ")";
                   
                    $label = "Modifier le solde";
                    ?>
                    <h4>Changer le solde/cr�dit de  : <?PHP echo Functions::getfullname($idMod); ?></h4>
                    <fieldset style="">
                       
                        <input name="solde" class="search-field" value ="<?PHP echo $infosUser['solde']; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"/><?PHP echo $deviseUser; ?>
                        <br/>
                    </fieldset><br/>
                    <?
                     $label = "Modifier le cr�dit";
                    ?>
                    
                    <fieldset style="">
                       
                        <input name="credit" class="search-field" value ="<?PHP echo $infosUser['credit']; ?>" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"/> Cr�dits
                        <br/>
                    </fieldset>

                    <?PHP
                } else if ($_GET['block'] == "listannuaire") {
                    $result = Functions::getListAnnuaireName($idMod);
					
					if (isWebmaster()){
						$current_list = Functions::getAnnuaireNameById($idMod);
						if ($current_list['proprietaire'] !=$idUser ){
							Functions::redirectJS('/');
							die;
						}
					}

                    $label = "Modifier le libell�";
                    ?>
                    <h4>Modifier le libell� de la liste d'annuaires : <?PHP echo $result; ?></h4>
                    <p>
                        Veuillez entrer le nouveau nom de cette liste
                    </p>
                    <fieldset class="one-fourth">
                        <label><span> </span>Libell� de la liste: </label>
                        <input name="listannuaire" class="search-field" type="text" style="margin-bottom:5px;width:350px;float:left" maxlength="300" value="<?PHP echo $result; ?>"><br/>
                    </fieldset>

                    <?PHP
                } else if ($_GET['block'] == "annuaire__") {
                    $result = Functions::getAnnuaire($idMod);
                    $label = "Modifier le libell�";
                    ?>
                    <h4>Modifier un annuaire :</h4>
                    <p>
                        Veuillez entrer la nouvelle URL:
                    </p>
                    <fieldset class="one-fourth">
                        <label><span> </span>URl de l'annuaire: </label>
                        <input name="annuaire" class="search-field" type="text" style="margin-bottom:5px;width:350px;float:left" maxlength="300" value="<?PHP echo $result['annuaire']; ?>"><br/>
                    </fieldset>

                    <?PHP
                } else if ($_GET['block'] == "listannuairecontent") {
						
					// get current annuairelist info
					$currentAnnuaireList = Functions::getOneAnnuaireListe(intval($idMod));
							
					// check access 
					if (isWebmaster()){
						$list_owner_id = $currentAnnuaireList['proprietaire']; 
					
						if ($list_owner_id != $idUser){
							Functions::redirectJS('/');
							die;
						}
					}
					
					$filter_config = $currentAnnuaireList['filter_config'];
					$last_viewed = $currentAnnuaireList['last_viewed'];
					
					
					
					
					// update last viewed time
					$current_time = time();
					if (isWebmaster()){
						 $dbh->query('UPDATE `annuaireslist` 
									  SET last_viewed = '.$current_time.' 
									  WHERE id=' . intval($idMod).' AND proprietaire = '.$idUser);
					}		
					
					
					// loading saved filter-config 
					if (isWebmaster() || isSU() || isAdmin() ){
						if (!empty($filter_config)){
							// parsing manually
							$filter_parts = explode('&',$filter_config);
							$query_params = array();
							foreach($filter_parts as $part){
								list($parsed_param,$parsed_value) = explode('=',$part,2);
								$query_params[$parsed_param] = $parsed_value;
							}	
							// push saved filter config to $_GET
							if (isset($query_params['filtrageavance']) && isset($query_params['filtre'])){
								foreach($query_params as $param=>$value){
									$_GET[$param]=$value;
								}					
							}		
						}
					}

					//var_dump($currentAnnuaireList);
							
				
                    if (!isset($_GET['filtrageavance']) || empty($_GET['filtrageavance'])) {
                        $_GET['filtrageavance'] = 0;
                    } else {
                        $_GET['filtrageavance'] = intval($_GET['filtrageavance']);
                    }
		
 
                    if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                        $annuairesFromList = explode(";", $currentAnnuaireList['annuairesList']);
                    } else {
                        $annuairesFromList = array();
                    }
                    $annuairesFromList = array_filter($annuairesFromList);
										
                    //var_dump($annuairesFromList);
//                    print_r($annuairesFromList);
                    $totalDepart = 1;
                    $annuairesPoint = array();
                    $label = "Enregistrer la liste";
                    ?>
                    <h4 style="width:900px;">Modifier : <?PHP echo $currentAnnuaireList['libelle'] . " [ " . count($annuairesFromList) . " annuaire(s) ]"; ?></h4>
                    <?PHP
                    $anuuLister = "";
//                    $resultMesAnnuaires = Functions::getListAnnuaireByUser($idUser);

                    if (isset($_GET['byAnu']) && intval($_GET['byAnu']) > 0) {
                        $idUserPropList = 0;
                        if (isWebmaster()) {
                            $idUserPropList = "webmasterAdmin";
                        }
                        $annuInfos = intval($_GET['byAnu']);
                        $currentAnnuaireListAnnuInfos = Functions::getOneAnnuaireListe($annuInfos, $idUserPropList);
                        if ($currentAnnuaireListAnnuInfos) {
                            $anuLister = $currentAnnuaireListAnnuInfos['annuairesList'];
                        } else {
                            $anuLister = "";
                        }
                    } else {
                        if ($_GET['byAnu'] == 'page_rank') {
                            $anuLister = "page_rank";
                        } else {
                            $anuLister = "";
                        }
                    }

                    $hauteur = "120";
                    if (isset($_GET['filtrageavance']) && $_GET['filtrageavance'] == 1) {
                        $hauteur = "305";
                    }

//                    print_r($resultAnnuaireAdmin);
//                        print_r($resultMesAnnuaires);
                    ?>
                    <style>
                        fieldset{
                            height:70px;
                        }
                    </style>
                    <div style="display:block;border:1px solid #dcdcdc;padding:10px;width:1100px;height:<?PHP echo $hauteur; ?>px;">
                        <h6 style="color:green;">Filtrer les annuaires:</h6>
                        <?PHP
                        $cp = "";
                        $liste = 0;
                        $otherData = "";

                        $ExtraOtherData = "";
                        $annuaireSelected = "";

                        if ($_GET['filtrageavance'] == 1) {
                            $Cond1 = 0;
                            $Cond2 = 0;
                            $Cond3 = 0;
                            $Cond4 = 0;
                            $Cond5 = 0;
                            $Cond6 = 0;

                            $tCond = 1;
                            $tVal = 0;

                            $prCond = 1;
                            $prVal = 0;

                            $tfCond = 1;
                            $tfVal = 0;

                            $aCond = 1;
                            $aVal = 0;

                            $tvCond = 1;
                            $tvVal = 0;

                            $tavCond = 1;
                            $tavVal = 0;

                            $tbCond = 1;
                            $tbVal = 0;
                        }

                        $arrayOrder = array("ASC", "DESC");
                        $arrayCond = array("AND", "AND");
                        $arrayComp = array("<=", ">=");
                        $arrayComp_ = array(">=", "<=");

                        $adminListes = Functions::getListAnnuaireDataByUser(0);
                        $requesteSPe = "";

                        if (isset($_GET['filtre']) && !empty($_GET['filtre'])) {
                            $filtre_infos = base64_decode($_GET['filtre']);
                            $filtre_infos = explode("|", $filtre_infos);
							
								
									
							//var_dump($filtre_infos);

                            $annuaireSelected = isset($filtre_infos[0]) ? $filtre_infos[0] : "";

							
                            $cp = isset($filtre_infos[1]) ? $filtre_infos[1] : "";
                            $liste = isset($filtre_infos[2]) ? intval($filtre_infos[2]) : 0;

                            $otherData = isset($filtre_infos[3]) ? $filtre_infos[3] : "";

                            $_GET['filtrageavance'] = isset($filtre_infos[4]) ? $filtre_infos[4] : 0;

                            $ExtraOtherData = isset($filtre_infos[5]) ? $filtre_infos[5] : "";

//                            $requesteSPe = " ORDER BY tarifW DESC" . $arrayOrder[$tarifW];

                            $firstStep = 0;
                            $otherDataSend = "";
                            $colonne = "";

                            if (trim($cp) != "" && ($cp == 0 || $cp == 1)) {
                                if ($_GET['filtrageavance'] == 0) {
                                    $requesteSPe .=" ORDER BY cp " . $arrayOrder[intval($cp)];
                                }
                                if ($_GET['filtrageavance'] == 1) {
                                    $otherDataSend = "cp-" . $cp;
                                }
                                $firstStep = 1;
                            }

                            if ($firstStep == 1 && intval($liste) != 0) {
                                $otherDataSend = "cp-" . $cp;
                            } else {
                                $otherDataSend = $otherData;
                            }


                            if ($otherData != "") {
                                $otherDataEx = explode("-", $otherData);
                                if ($otherDataEx && $_GET['filtrageavance'] == 0) {
                                    if ($firstStep == 1) {
                                        $requesteSPe .= ",";
                                    } else {
                                        $requesteSPe .=" ORDER BY ";
                                    }
                                    if ($otherDataEx[0] == "age") {
//                                        echo "ok";
//                                        $arrayOrder = array("ASC", "DESC");
                                    }
                                    $requesteSPe .= "" . $otherDataEx[0] . " " . $arrayOrder[$otherDataEx[1]];
                                }
                                $colonne = "" . $otherDataEx[0];
                            }

                            if ($ExtraOtherData != "") {

                                if ($_GET['filtrageavance'] == 1) {
								
									

                                    $otherDataExX = explode(";", $ExtraOtherData);
									
									//var_dump($otherDataExX);
//                                    print_r($otherDataExX);
                                    // WTF this code!!!!!!!!
                                    if (is_array($otherDataExX)) {
//                                         print_r( $otherDataExX);
                                        $Cond1 = intval($otherDataExX[2]);
                                        $Cond2 = intval($otherDataExX[5]);
                                        $Cond3 = intval($otherDataExX[8]);
                                        $Cond4 = intval($otherDataExX[11]);
                                        $Cond5 = intval($otherDataExX[14]);
                                        $Cond6 = intval($otherDataExX[17]);

                                        $tCond = intval($otherDataExX[0]);
                                        $tVal = floatval($otherDataExX[1]);
										
										//var_dump($tVal);

//                                        echo $otherDataEx[0];

                                        $prCond = intval($otherDataExX[3]);
                                        $prVal = intval($otherDataExX[4]);

                                        $tfCond = intval($otherDataExX[6]);
                                        $tfVal = intval($otherDataExX[7]);

                                        $aCond = intval($otherDataExX[9]);
                                        $aVal = intval($otherDataExX[10]);

                                        $tvCond = intval($otherDataExX[12]);
                                        $tvVal = intval($otherDataExX[13]);

                                        $tavCond = intval($otherDataExX[15]);
                                        $tavVal = intval($otherDataExX[16]);

                                        $tbCond = intval($otherDataExX[18]);
                                        $tbVal = intval($otherDataExX[19]);
										
										
										
										//var_dump($otherDataExX);
										
										// accept annuaires conditions 
										// FUCKING Code
										$accept_inner_pages = intval($otherDataExX[20]);
										$accept_legal_info = intval($otherDataExX[21]);		
										$accept_inner_pages = intval($otherDataExX[22]);
										
										//var_dump($accept_inner_pages,$accept_legal_info,$accept_inner_pages);
										
										//die;
										
                                        $requesteSPe .=" AND ";

                                        if (trim($cp) != "" && ($cp == 0 || $cp == 1)) {
                                            $requesteSPe .=" cp = " . intval($cp);
                                            $requesteSPe .=" AND ";
                                        }

                                        if ($tVal > 0) {
                                            $requesteSPe .= "tarifW " . $arrayComp[$tCond] . " '" . (floatval($tVal) - ($remuneration)) . "'";
                                        } else {
                                            $requesteSPe .= "tarifW " . $arrayComp[$tCond] . " " . intval(0);
                                        }
                                        $requesteSPe .= " " . $arrayCond[$Cond1] . " ";


                                        // PAGE_RANK
                                        $requesteSPe .= "page_rank " . $arrayComp[$prCond] . " " . $prVal;
										//var_dump($requesteSPe);
                                        $requesteSPe .= " " . $arrayCond[$Cond2] . " ";
										//var_dump($requesteSPe);


                                        $requesteSPe .= "tf " . $arrayComp[$tfCond] . " " . $tfVal;
                                        $requesteSPe .= " " . $arrayCond[$Cond3] . " ";

                                        // bug
                                        if ($aVal > 0) {
                                            $requesteSPe .= "age " . $arrayComp_[$aCond] . " " . intval(time() - (31556926 * $aVal)); // WTF??
                                            // crutch
                                            //$requesteSPe .= "age >= 0 ";
                                        } else {
                                            // $requesteSPe .= "age " . $arrayComp_[$aCond] . " " . intval(0);
                                            $requesteSPe .= " 1 ";
                                            // crutch
                                            //$requesteSPe .= "age >= 0 ";
                                        }										
										
										// Je veux referencer un sous-domaine ou une page profonde.		
										if (isset($_GET['accept_inner_pages'])){
											$requesteSPe.=' AND accept_inner_pages = 1 ';										
     									}
										
										// Je n'ai pas de mention legale sur mon site.
										if (isset($_GET['accept_legal_info'])){
											$requesteSPe.=' AND accept_legal_info  = 1 ';											
										}
										
										// Je referencie le site d'une entreprise.
										if (isset($_GET['accept_company_websites'])){
											$requesteSPe.=' AND accept_company_websites IN (0,1) ';
										}else{
										    $requesteSPe.=' AND accept_company_websites <> 1 ';
										}		
                                        
										// Possibilite de creer un compte membre.
										if (isset($_GET['is_member_account'])){
											$requesteSPe.=' AND ComptePersoWebmaster = 1 ';								
										}										

                                        $requesteSPe .= " " . $arrayCond[$Cond4] . " ";

                                        $requesteSPe .= "tv " . $arrayComp[$tvCond] . " " . $tvVal;
                                        $requesteSPe .= " " . $arrayCond[$Cond5] . " ";


                                        $requesteSPe .= "tav " . $arrayComp[$tavCond] . " " . $tavVal;
                                        $requesteSPe .= " " . $arrayCond[$Cond6] . " ";

                                        $requesteSPe .= "tb " . $arrayComp[$tbCond] . " " . $tbVal;

                                        if ($otherData != "") {
                                            $requesteSPe .= " ORDER BY " . $otherDataEx[0] . " " . $arrayOrder[$otherDataEx[1]];
                                        }
										
										//var_dump($requesteSPe);
                                    }
                                }
                            }
                        }
//                        echo $remuneration."";
//                        echo $requesteSPe;
//                    echo $liste;
//                    echo $otherDataSend;
//                    echo $ExtraOtherData;


						// BUG? ANNUAIRE FROM FILTER
                        if ($annuaireSelected != "") {                            
							//$annuairesFromList = array_filter(explode(';', $annuaireSelected));
                        }

						
                        ?>
                        <style>
    <?PHP
    if ($colonne != "") {
        echo "#" . $colonne . "{";
        echo "background:#7BB30E;";
        echo "}";

        echo "." . $colonne . "," . "." . $colonne . " > tr:hover{";
        echo "background:#E6FAE6;";
        echo "}";
    }
    ?>
                                                                                                
                        </style>

                        <div>
                            <fieldset style="float:left;width:100px;">
                                <label class="color" for="cp">Type:</label>
                                <select name="cp" id="cp">
                                    <?PHP
                                    $selectedd = "";
                                    $selectedd_ = "";
                                    if ($cp == 0 && $cp != "") {
                                        $selectedd = "selected='selected'";
                                    }
                                    if ($cp == 1) {
                                        $selectedd_ = "selected='selected'";
                                    }
                                    ?>
                                    <option value="" >Tous</option>
                                    <option value="0" <?PHP echo $selectedd; ?>>Annuaire</option>
                                    <option value="1" <?PHP echo $selectedd_; ?>>CP</option>
                                </select>
                            </fieldset>
                            <fieldset style="float:left;width:210px;margin-right:10px;">
                                <label class="color" for="liste">Liste d'annuaire:</label>
                                <select name="liste" id="liste">
                                    <option value="0">Tous</option>
                                    <?PHP
                                    foreach ($adminListes as $adminListeLine) {
                                        $selecteddd = "";
                                        if ($liste == $adminListeLine['id']) {
                                            $selecteddd = "selected='selected'";
                                        }
                                        ?>
                                        <option value='<?PHP echo $adminListeLine['id']; ?>' <?PHP echo $selecteddd; ?>>
                                            <?PHP echo ucfirst(preg_replace("#Annuaires#", "", $adminListeLine['libelle'])); ?>
                                        </option>
                                        <?PHP
                                    }
                                    ?>
                                </select>
                            </fieldset>


                            <fieldset style="float:left;width:310px;">
                                <label class="color" for="otherData">R�organiser Par :</label>
                                <select name="otherData" id="otherData" style="width:300px;">
                                    <option value="">Aucune Option</option>
                                    <?PHP
                                    ?>
                                    <optgroup label="Tarif">
                                        <option value="tarifW-0" <?PHP
                                        if ($otherData == "tarifW-0") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Tarif Croissant</option>
                                        <option value="tarifW-1" <?PHP
                                        if ($otherData == "tarifW-1") {
                                            echo "selected='selected'";
                                        };
                                        ?> >Tarif D�croissant</option>
                                    </optgroup>

								
                                    <optgroup style="display:none;" label="Page Rank">
                                        <option value="page_rank-0" <?PHP
                                        if ($otherData == "page_rank-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Page Rank Croissant</option>
                                        <option value="page_rank-1" <?PHP
                                        if ($otherData == "page_rank-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Page Rank D�croissant</option>
                                    </optgroup>
									
								

                                    <optgroup label="Age">
                                        <option value="age-1" <?PHP
                                        if ($otherData == "age-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>�ge : Ordre Croissant</option>
                                        <option value="age-0" <?PHP
                                        if ($otherData == "age-0") {
                                            echo "selected='selected'";
                                        };
                                        ?>>�ge : Ordre D�croissant</option>
                                    </optgroup>

                                    <optgroup label="Trust Flow">
                                        <option value="tf-0" <?PHP
                                        if ($otherData == "tf-0") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Trust Flow Croissant</option>
                                        <option value="tf-1" <?PHP
                                        if ($otherData == "tf-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Trust Flow D�croissant</option>
                                    </optgroup>
                                    <!--                                    <optgroup label="Citation Flow">
                                                                            <option value="cf-0" <?PHP echo $selectedddd_; ?>>Ordre Croissant</option>
                                                                            <option value="cf-1" <?PHP echo $selectedddd_; ?>>Ordre D�croissant</option>
                                                                        </optgroup>-->
                                    <!--                                    <optgroup label="Total Domaine R�f�rent">
                                                                            <option value="tdr-0" <?PHP
                                    if ($otherData == "tdr-1") {
                                        echo "selected='selected'";
                                    };
                                    ?>>Total Domaine R�f�rent : Ordre Croissant</option>
                                                                            <option value="tdr-1" <?PHP
                                    if ($otherData == "tdr-1") {
                                        echo "selected='selected'";
                                    };
                                    ?>>Total Domaine R�f�rent : Ordre D�croissant</option>
                                                                        </optgroup>-->
                                    <optgroup label="Total Domaine r�f�rents">
                                        <option value="tb-0" <?PHP
                                        if ($otherData == "tb-0") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Total Domaine r�f�rents : Ordre Croissant</option>
                                        <option value="tb-1" <?PHP
                                        if ($otherData == "tb-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Total Domaine r�f�rents : Ordre D�croissant</option>
                                    </optgroup>
                                    <optgroup label="Temps de validation">
                                        <option value="tv-0" <?PHP
                                        if ($otherData == "tv-0") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Temps de validation : Ordre Croissant</option>
                                        <option value="tv-1" <?PHP
                                        if ($otherData == "tv-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Temps de validation : Ordre D�croissant</option>
                                    </optgroup>
                                    <optgroup label="Taux de validation">
                                        <option value="tav-0" <?PHP
                                        if ($otherData == "tav-0") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Taux de validation : Ordre Croissant</option>
                                        <option value="tav-1" <?PHP
                                        if ($otherData == "tav-1") {
                                            echo "selected='selected'";
                                        };
                                        ?>>Taux de validation : Ordre D�croissant</option>
                                    </optgroup>
                                </select>
                            </fieldset>


                            <?PHP
                            if (isset($_GET['filtrageavance']) && $_GET['filtrageavance'] == 1) {
                                ?>
                            </div>
                            <div style="clear:both;">                                
                                <style>
                                    .advandInput{
                                        width:50px;display:inline-block;float:left;height:17px;
                                    }
                                    .condition{
                                        border:1px solid black;
                                        /*                                        height:25px;
                                                                                font-size:11px;
                                                                                margin-top:3px;
                                                                                width:50px;*/
                                    }
                                </style>


                                <!--                                <fieldset style="float:left;width:80px;">
                                                                    <label class="color" for="listeC">&nbsp;</label>
                                                                    <select name="advancedSearch[]" id="listeC" style="float:left;" class="condition">
                                                                        <option value="0" <?PHP
                                if ($Cond1 == 0) {
                                    echo "selected='selected'";
                                }
                                ?>>ET</option>
                                                                        <option value="1" <?PHP
                                if ($Cond1 == 1) {
                                    echo "selected='selected'";
                                }
                                ?>>OU</option>
                                                                    </select>
                                                                </fieldset>-->

                                <fieldset style="float:left;width:140px;">
                                    <label class="color" for="listeT">Tarif :</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeT" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($tCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>><=</option>
                                            <option value="1" <?PHP
                                            if ($tCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $tVal; ?>" style="width:30px;" onkeyup="ve1(this);"/> 
                                        &nbsp;<i class="icon-euro color" style="display:inline-block;margin-top:10px;" ></i> 
                                    </div>
                                </fieldset>

                                <input type="hidden" name="advancedSearch[]" value="<?PHP echo $Cond1; ?>"/>
								
						
                                <fieldset style="float:left;width:150px;display:none;">
                                    <label class="color" for="listeP">Page Rank:</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeP" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($prCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>><=</option>
                                            <option value="1" <?PHP
                                            if ($prCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $prVal; ?>" style="" onkeyup="ve(this);"/>
                                    </div>
                                </fieldset>
								
                                <!--
                                                                <fieldset style="float:left;width:80px;">
                                                                    <label class="color" for="listeCC">&nbsp;</label>
                                                                    <select name="advancedSearch[]" id="listeCC" style="float:left;" class="condition">
                                                                        <option value="0" <?PHP
                                if ($Cond2 == 0) {
                                    echo "selected='selected'";
                                }
                                ?>>ET</option>
                                                                        <option value="1" <?PHP
                                if ($Cond2 == 1) {
                                    echo "selected='selected'";
                                }
                                ?>>OU</option>
                                                                    </select>
                                                                </fieldset>-->
                                <input type="hidden" name="advancedSearch[]" value="<?PHP echo $Cond2; ?>"/>


                                <fieldset style="float:left;width:150px;">
                                    <label class="color" for="listeTF">Trust Flow:</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeTF" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($tfCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>><=</option>
                                            <option value="1" <?PHP
                                            if ($tfCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $tfVal; ?>" style="" onkeyup="ve(this);"/>
                                    </div>
                                </fieldset>
                                <!--
                                                                <fieldset style="float:left;width:80px;">
                                                                    <label class="color" for="listeCCC">&nbsp;</label>
                                                                    <select name="advancedSearch[]" id="listeCCC" style="float:left;" class="condition">
                                                                        <option value="0" <?PHP
                                if ($Cond3 == 0) {
                                    echo "selected='selected'";
                                }
                                ?>>ET</option>
                                                                        <option value="1" <?PHP
                                if ($Cond3 == 1) {
                                    echo "selected='selected'";
                                }
                                ?>>OU</option>
                                                                    </select>
                                                                </fieldset>-->
                                <input type="hidden" name="advancedSearch[]" value="<?PHP echo $Cond3; ?>"/>

                                <fieldset style="float:left;width:160px;">
                                    <label class="color" for="listeA">Age :</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeA" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($aCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>>=<</option>
                                            <option value="1" <?PHP
                                            if ($aCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $aVal; ?>" style="width:20px;" onkeyup="ve(this);"/>
                                        &nbsp;<span class="color" style="display:inline-block;margin-top:5px;font-weight:bold;" >An(s)</span> 
                                    </div>
                                </fieldset>

                                <!--                                <fieldset style="float:left;width:80px;">
                                                                    <label class="color" for="listeCCCC">&nbsp;</label>
                                                                    <select name="advancedSearch[]" id="listeCCCC" style="float:left;" class="condition">
                                                                        <option value="0" <?PHP
                                if ($Cond4 == 0) {
                                    echo "selected='selected'";
                                }
                                ?>>ET</option>
                                                                        <option value="1" <?PHP
                                if ($Cond4 == 1) {
                                    echo "selected='selected'";
                                }
                                ?>>OU</option>
                                                                    </select>
                                                                </fieldset>-->
                                <input type="hidden" name="advancedSearch[]" value="<?PHP echo $Cond4; ?>"/>

                                <fieldset style="float:left;width:150px;">
                                    <label class="color" for="listeTV">Temps Validation:</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeTV" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($tvCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>><=</option>
                                            <option value="1" <?PHP
                                            if ($tvCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $tvVal; ?>" style="width:15px;" onkeyup="ve(this);" maxlength="2"/>
                                        &nbsp;<span class="color" style="display:inline-block;margin-top:5px;font-weight:bold;" >Jr(s)</span> 

                                    </div>
                                </fieldset>
                                <input type="hidden" name="advancedSearch[]" value="<?PHP echo $Cond5; ?>"/>

                                <fieldset style="float:left;width:150px;">
                                    <label class="color" for="listeTAV">Taux Validation:</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeTAV" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($tavCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>><=</option>
                                            <option value="1" <?PHP
                                            if ($tavCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $tavVal; ?>" style="width:15px;" onkeyup="ve(this);" maxlength="2"/>
                                        &nbsp;<span class="color" style="display:inline-block;margin-top:5px;font-weight:bold;" >%</span> 

                                    </div>
                                </fieldset>

                                <!--                                <fieldset style="float:left;width:80px;">
                                                                    <label class="color" for="liste">&nbsp;</label>
                                                                    <select name="advancedSearch[]" id="liste" style="float:left;" class="condition">
                                                                        <option value="0" <?PHP
                                if ($Cond5 == 0) {
                                    echo "selected='selected'";
                                }
                                ?>>ET</option>
                                                                        <option value="1" <?PHP
                                if ($Cond5 == 1) {
                                    echo "selected='selected'";
                                }
                                ?>>OU</option>
                                                                    </select>
                                                                </fieldset>-->
                                <input type="hidden" name="advancedSearch[]" value="<?PHP echo $Cond6; ?>"/>

                                <fieldset style="float:left;width:150px;">
                                    <label class="color" for="listeTB">Total Backlinks:</label>
                                    <div>
                                        <select name="advancedSearch[]" id="listeTB" style="float:left;margin-right:5px;">
                                            <option value="0" <?PHP
                                            if ($tbCond == 0) {
                                                echo "selected='selected'";
                                            }
                                            ?>><=</option>
                                            <option value="1" <?PHP
                                            if ($tbCond == 1) {
                                                echo "selected='selected'";
                                            }
                                            ?>>>=</option>
                                        </select>
                                        <input type="text" name="advancedSearch[]" class="search-field advandInput" value="<?PHP echo $tbVal; ?>" style="" onkeyup="ve(this);"/>
                                    </div>
                                </fieldset>
                                <?PHP
                            }
                            ?>
							
							<div style="float:left;">
								<label class="color"><input value="1" type="checkbox" <?php if (isset($_GET['accept_inner_pages'])):?> checked <?php endif; ?> name="accept_inner_pages"/> &nbsp;Je veux r�f�rencer un sous-domaine ou une page profonde.</label>
								
								<label class="color"><input  value="1" type="checkbox" <?php if (isset($_GET['accept_legal_info'])):?> checked <?php endif; ?> name="accept_legal_info"/> &nbsp;Je n'ai pas de mention l�gale sur mon site.</label>
								
								<?php /*<label class="color"><input  value="1" type="checkbox" <?php if (isset($_GET['accept_company_websites'])):?> checked <?php endif; ?> name="accept_company_websites"/> &nbsp;Je r�f�rencie le site d'une entreprise.</label>
								
								<label class="color"><input  value="1" type="checkbox" <?php if (isset($_GET['is_member_account'])):?> checked <?php endif; ?> name="is_member_account"/> &nbsp;Possibilit� de cr�er un compte membre.</label> */ ?>
                            </div>
							<div class="clearfix"></div>
							<?php
								$user_min_words_count = $currentAnnuaireList['words_count'];
								
								if (isset($_GET['min_words_count']) && !empty($_GET['min_words_count']) ){
									$user_min_words_count = intval($_GET['min_words_count']);
								}
								
								//var_dump($user_min_words_count);
								//var_dump($currentAnnuaireList['words_count']);
							?>
							<?php 
								$redaction_price_per_100 = isset($_SESSION['allParameters']['price_100_words']['valeur'])? $_SESSION['allParameters']['price_100_words']['valeur'] : 1.65;
								$redaction_price_per_100_euro = str_replace('.','&euro;',$redaction_price_per_100);
							?>							
							<div class="list_annuaires_size_block">
								<div class="row" >
									<label class="color">Nombre de mots minimum souhait�s  : </label>
									<input name="min_words_count" class="input_size" value="<?php  echo  $user_min_words_count ? $user_min_words_count: '';?>" type="text" onkeyup="ve1(this);" >
									<span class="explanation_tooltip" data-type="minimum" >(Qu'est-ce que c'est)</span>
									<div style="display:none;">
                                        En compl�tant ce champ, le prix indiqu� sera ajust� en fonction du nombre de mots suppl�mentaires que nous aurons � �crire, au prix de <?php echo $redaction_price_per_100;?>&euro;/100 mots. Ainsi, si vous demandez 300 mots en minimum, et l'annuaire n'exige que 200 mots,
										un compl�ment de <?php echo $redaction_price_per_100_euro;?> vous sera appliqu� pour les 100 mots suppl�mentaires. <br/>
                                        Laissez vide pour avoir les soumissions au tarif minimum.
                                    </div>
								</div>
								<!--
								<div class="row" >
									<label class="color">Nombre de mots maximum souhait�s  : </label>
									<input name="max_words_count" class="input_size" value="<?php  echo  (isset($_GET['max_words_count']))? $_GET['max_words_count']: '';?>" type="text" >
								</div>
                                <span class="explanation_tooltip" data-type="minimum" >(Qu'est-ce que c'est)</span>
                                <div style="display:none;">
                                    En compl�tant ce champ, le prix indiqu� sera ajust� en fonction du nombre de mots suppl�mentaires que nous aurons � �crire, au prix de 1.65&euro;/100 mots. Ainsi, si vous demandez 300 mots en minimum, et l'annuaire n'exige que 200 mots,
                                    un compl�ment de 1 &euro; 65 vous sera appliqu� pour les 100 mots suppl�mentaires. <br/>
                                    Laissez vide pour avoir les soumissions au tarif minimum.
                                </div>-->
							</div>

							
					    	<input value="<?PHP echo $_GET['filtrageavance']; ?>" name="filtrageavance" type="hidden"/>
							<div class="clearfix"></div>
                            <fieldset style="float:left;margin-top: 0px;margin-bottom:10px;">
						                                 <input type="submit" name="action" class="button color small round " value="Filtrer" style="color:white;"/>
                                <!--
                                <a href="./compte/modifier/listannuairecontent.html?id=<?PHP echo $_GET['id']; ?>" class="button small color round">Reset</a>
                                
                                &nbsp;-->
                                <?php /* ?>
                                  <span style="font-size:14px;color:orangered;">
                                  <?PHP
                                  if (isset($_GET['filtrageavance']) && $_GET['filtrageavance'] == 0) {
                                  ?>
                                  <!--<br/>-->
                                  <a href="./compte/modifier/listannuairecontent.html?id=<?PHP echo $_GET['id']; ?>&filtrageavance=1" style="font-weight:bold;" class="button small color round">Filtrage avanc�</a>
                                  <?PHP
                                  }

                                  if (isset($_GET['filtrageavance']) && $_GET['filtrageavance'] == 1) {
                                  ?>
                                  <a href="./compte/modifier/listannuairecontent.html?id=<?PHP echo $_GET['id']; ?>&filtrageavance=0" style="font-weight:bold;" class="button small color round">Filtrage simple</a>
                                  <?PHP
                                  }
                                  ?>
                                  </span>
                                  <?php */ ?>
                            </fieldset>

                        </div>

                    </div>

                    <!-- voodoo417 -->
                    <div id="estimation_box_fixed">Estimation du projet: &nbsp;<span class="estimation_total_sum">0.0</span>&euro;</div>


                    <?PHP
                    if ($_GET['filtrageavance'] == 'no') {
                        foreach ($annuairesFromList as $idSelected) {
                            ?>
                            <input type="hidden" value="<?PHP echo $idSelected; ?>" name="check[]" />
                            <?PHP
                        }
                    }
                    ?>
                    <?PHP
//                    echo $requesteSPe;
//                    echo $liste;
//                    echo $otherDataSend;

                    //var_dump($requesteSPe);die;
					
					//error_reporting(0);

                    $resultAnnuaireAdmin = Functions::getListAnnuaireByUser(0, $requesteSPe, $liste, $otherDataSend, $ExtraOtherData);
										
					//$last_viewed = 1450792744; 
					
					if (!empty($last_viewed)){		
						$sort_by_last_viewed = false; 
						
						foreach($resultAnnuaireAdmin as &$annuaire){
							$annuaire_created = strtotime($annuaire['created']);
							if ($annuaire_created >= $last_viewed){
								$annuaire['new'] = true;		
								if (!$sort_by_last_viewed)
									$sort_by_last_viewed = true;
							}		
						}
						unset($annuaire); // reset 
						
						// sorting for new
						if ($sort_by_last_viewed){
							usort($resultAnnuaireAdmin, function($a, $b) use($last_viewed) {
								$annuaire_created = strtotime($a['created']);
								return ( $last_viewed > $annuaire_created)? 1 : -1;
							});
						}					
					}
					
					
					//var_dump($resultAnnuaireAdmin);
					
                    // voodoo417
                    $show_not_filtered = false;
                    $totalActiveAnnuaires = Functions::getTotalActiveAnnuaires();
                    if (count($resultAnnuaireAdmin) != $totalActiveAnnuaires) {
//                        echo 0;
                        $show_not_filtered = true;
                        $filtered_ids = array();

                        foreach ($resultAnnuaireAdmin as $item) {
                            $filtered_ids[] = $item['id'];
                        }
                        $not_filtered_annuiares = Functions::getNotFilteredAnnuaires($filtered_ids, $requesteSPe);

                        // get not filtered params for colored colums
                        $condition = array_shift(explode('ORDER BY', $requesteSPe)); // removing order
                        $conditions = explode('AND', $condition);
                        $not_filtered_params = array();
                        foreach ($conditions as $couple) {
                            $data = array();
                            if (strpos($couple, '<=')) {
                                list($field, $value) = explode("<=", $couple);
                                $not_filtered_params[trim($field)] = array('val' => trim($value), 'op' => '<=');
                            }

                            if (strpos($couple, '>=')) {
                                list($field, $value) = explode(">=", $couple);
                                $not_filtered_params[trim($field)] = array('val' => trim($value), 'op' => '>=');
                            }
                        }
//                        echo '<pre>';
//                        var_dump($not_filtered_params);
//                        echo '</pre>';
                    }

//                    print_r($resultAnnuaireAdmin);
                    if (count($resultAnnuaireAdmin) > 0) {

                        // tv N/A sort
						
		

                        if ($otherData == 'tarifW-1') {
	
		
							usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tarifW = floatval($a['tarifW']);
                                $b_tarifW = floatval($b['tarifW']);
								
								if ($a_tarifW == $b_tarifW)
									return 0;
								
								return ($a_tarifW < $b_tarifW)? 1 : -1;
								/*
                                if (($a_tarifW == 0) || ($b_tarifW == 0)) { // check N/A
                                    return $b_tarifW - $a_tarifW;
                                } else {
                                    return $b_tarifW - $a_tarifW;
                                }*/
                            });
							
                        } elseif ($otherData == 'tarifW-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tarifW = floatval($a['tarifW']);
                                $b_tarifW = floatval($b['tarifW']);
								
								if ($a_tarifW == $b_tarifW)
									return 0;
								
								return ($a_tarifW < $b_tarifW)? -1 : 1;														
								
								/*
                                if (($a_tarifW == 0) || ($b_tarifW == 0)) { // check N/A
                                    return $b_tarifW - $a_tarifW;
                                } else {
                                    return $a_tarifW - $b_tarifW;
                                }*/
								
								
                            });
                        }
						
						

                        if ($otherData == 'age-1') {
                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_age = intval($a['age']);
                                $b_age = intval($b['age']);
                                if (($a_age == 0) || ($b_age == 0)) { // check N/A
                                    return $b_age - $a_age;
                                } else {
                                    return $b_age - $a_age;
                                }
                            });
                        } elseif ($otherData == 'age-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_age = intval($a['age']);
                                $b_age = intval($b['age']);
                                if (($a_age == 0) || ($b_age == 0)) { // check N/A
                                    return $b_age - $a_age;
                                } else {
                                    return $a_age - $b_age;
                                }
                            });
                        }

                        if ($otherData == 'page_rank-1') {
                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_page_rank = intval($a['page_rank']);
                                $b_page_rank = intval($b['page_rank']);
                                if (($a_page_rank == 0) || ($b_page_rank == 0)) { // check N/A
                                    return $b_page_rank - $a_page_rank;
                                } else {
                                    return $b_page_rank - $a_page_rank;
                                }
                            });
                        } elseif ($otherData == 'page_rank-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_page_rank = intval($a['page_rank']);
                                $b_page_rank = intval($b['page_rank']);
                                if (($a_page_rank == 0) || ($b_page_rank == 0)) { // check N/A
                                    return $b_page_rank - $a_page_rank;
                                } else {
                                    return $a_page_rank - $b_page_rank;
                                }
                            });
                        }

                        if ($otherData == 'tf-1') {
                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tf = intval($a['tf']);
                                $b_tf = intval($b['tf']);
                                if (($a_tf == 0) || ($b_tf == 0)) { // check N/A
                                    return $b_tf - $a_tf;
                                } else {
                                    return $b_tf - $a_tf;
                                }
                            });
                        } elseif ($otherData == 'tf-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tf = intval($a['tf']);
                                $b_tf = intval($b['tf']);
                                if (($a_tf == 0) || ($b_tf == 0)) { // check N/A
                                    return $b_tf - $a_tf;
                                } else {
                                    return $a_tf - $b_tf;
                                }
                            });
                        }
                        
						// changed filter from "Backlinks" to "Domaine Referents"
						// field "tb" => field "tdr"
						// 		
                        if ($otherData == 'tb-1') {
                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tb = intval($a['tdr']);
                                $b_tb = intval($b['tdr']);
                                if (($a_tb == 0) || ($b_tb == 0)) { // check N/A
                                    return $b_tb - $a_tb;
                                } else {
                                    return $b_tb - $a_tb;
                                }
                            });
                        } elseif ($otherData == 'tb-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tb = intval($a['tdr']);
                                $b_tb = intval($b['tdr']);
                                if (($a_tb == 0) || ($b_tb == 0)) { // check N/A
                                    return $b_tb - $a_tb;
                                } else {
                                    return $a_tb - $b_tb;
                                }
                            });
                        }

                        // tv N/A sort

                        if ($otherData == 'tv-1') {
                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tv = intval($a['tv']);
                                $b_tv = intval($b['tv']);
                                if (($a_tv == 0) || ($b_tv == 0)) { // check N/A
                                    return $b_tv - $a_tv;
                                } else {
                                    return $b_tv - $a_tv;
                                }
                            });
                        } elseif ($otherData == 'tv-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tv = intval($a['tv']);
                                $b_tv = intval($b['tv']);
                                if (($a_tv == 0) || ($b_tv == 0)) { // check N/A
                                    return $b_tv - $a_tv;
                                } else {
                                    return $a_tv - $b_tv;
                                }
                            });
                        }
                        // tav N/A sort

                        if ($otherData == 'tav-1') {
                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tav = intval($a['tav']);
                                $b_tav = intval($b['tav']);
                                if (($a_tav == 0) || ($b_tav == 0)) { // check N/A
                                    return $b_tav - $a_tav;
                                } else {
                                    return $b_tav - $a_tav;
                                }
                            });
                        } elseif ($otherData == 'tav-0') {

                            usort($resultAnnuaireAdmin, function($a, $b) {
                                $a_tav = intval($a['tav']);
                                $b_tav = intval($b['tav']);
                                if (($a_tav == 0) || ($b_tav == 0)) { // check N/A
                                    return $b_tav - $a_tav;
                                } else {
                                    return $a_tav - $b_tav;
                                }
                            });
                        }
	
                        ?>
                        <div style="clear:both;width: 100%;float:left;">
                            <br/><br/>
                            <input type="submit" id="top_save_list" name="action" class="button color small round " value="<?PHP echo $label; ?> " style="color:white;display:inline-block;"/>
                            <div id="estimation_box">
                                Estimation du projet: &nbsp;<span class="estimation_total_sum">0.0</span>&euro;
                            </div>
                            <br/><br/>
                        </div>
						
							<div style="display:none;width:380px;" class="tooltip">
							   <div class="tooltip_header"></div>
							   <div class="tooltip_body"></div>
							</div>
							
									
							
							<div class="clearfix"></div>
							<br/>	
							<br/>
							
								
							<!-- Scroll Down Table  -->
							<table class="simple-table tableAnnuaires " id="scroll_table_header" style="z-index:9999;display:none; width:1140px;margin-right:0px;position:fixed; top:0;left:0;" >	
								<tbody>
								<tr>
                                        <th>
                                            <input id="checkClasseses2222" class="checkClasseses" name="checkClasseses" type="checkbox" value="0" title="Cocher Tout" rel="2222">&nbsp;
                                            <label class="sort_field" id="annuaire" style="color:white;display:inline-block;" class="sortable" >Nos Annuaires</label>
                                        </th>
                                        <th id="cp" class="sort_field" >
                                            Type
                                        </th>
								
                                        <th id="vip_state" class="sort_field" >
											Partenariat?
										</th>
										
                                        <th id="tarifW" class="sort_field" >
                                            Tarif
                                        </th>
                                        <th id="ar" class="sort_field" >
                                            Alexa Rank
                                        </th>
                                        <th id="age" class="sort_field" >
                                            �ge (ans)
                                        </th>
                                        <th id="tf" class="sort_field" >
                                            Trust Flow
                                        </th>
                                        <th id="tv" style="font-size:10px;width:40px;" class="sort_field">
                                            Temps de validation
                                        </th>
                                        <th id="tav" style="font-size:9px;width:30px;" class="sort_field">
                                            Taux de validation
                                        </th>
                                        <th id="td" class="sort_field" style="font-size:9px;width: 40px;">
                                            Domaine r�f�rents
                                        </th>
                                        <th id="liste" style="min-width:80px;">
                                            Cat�gories
                                        </th>

                                </tr>
								</tbody>		
							</table>	



												<!-- Extra Scroll Down Table  -->
							<table class="simple-table tableAnnuaires " id="extra_scroll_table_header" style="z-index:9999;display:none; width:1140px;margin-right:0px;position:fixed; top:0;left:0;" >	
								<tbody>
								<tr>
                                        <th >
                                            <input id="checkClasseses3333" class="checkClasseses" name="checkClasseses" type="checkbox" value="0" title="Cocher Tout" rel="3333">&nbsp;
                                            <label class="sort_field" data-not_filter="1" id="annuaire" style="color:white;display:inline-block;" class="sortable" >Nos Annuaires</label>
                                        </th>
                                        <th id="cp" class="sort_field" data-not_filter="1" >
                                            Type
                                        </th>
								
                                        <th id="vip_state" class="sort_field" data-not_filter="1" >
											Partenariat?
										</th>
										
                                        <th id="tarifW" class="sort_field" data-not_filter="1" >
                                            Tarif
                                        </th>
                                        <th id="ar" class="sort_field" data-not_filter="1" >
                                            Alexa Rank
                                        </th>
                                        <th id="age" class="sort_field" data-not_filter="1" >
                                            �ge (ans)
                                        </th>
                                        <th id="tf" class="sort_field" data-not_filter="1" >
                                            Trust Flow
                                        </th>
                                        <th id="tv" style="font-size:10px;width:40px;" class="sort_field" data-not_filter="1">
                                            Temps de validation
                                        </th>
                                        <th id="tav" style="font-size:9px;width:30px;" class="sort_field" data-not_filter="1">
                                            Taux de validation
                                        </th>
                                        <th id="td" class="sort_field" style="font-size:9px;width: 40px;" data-not_filter="1">
                                            Domaine r�f�rents
                                        </th>
                                        <th style="min-width:80px;" id="liste">
                                            Cat�gories
                                        </th>

                                </tr>
								</tbody>		
							</table>				

                        <div class="<?php if ($show_not_filtered) { ?>filtered_outside<?php } ?>">
							
							<script type="text/javascript">
								(function($){
									$(document).ready(function(){
									
										$.fn.toTopTd = function() {
											var $el;
											return this.each(function() {
												$el = $(this);
												var newDiv = $("<div />", {
													"class": "new_annuaire",
													"css"  : {
														"height"  : $el.height(),
														"width"   : "100%",
														"position": "relative"
													}
												});
												$el.wrapInner(newDiv);    
											});
										};
										
										
										//$("#tableClasses2222 td.has_new_annuaire").toTopTd();
									
									
										// Min & Max words-counts 
										
										$('.explanation_tooltip').on('mouseenter', function (e) {
											var type = $(this).data("type");
											var text = $(this).next().html();
											var pos = $(this).position();
											$(".tooltip .tooltip_body").html(text);
											$(".tooltip").css({top: pos.top + 25, left: pos.left + 10}).fadeIn('fast');
										}).on('mouseleave', function (e) {
											$(".tooltip").hide();
										});
									
										
										// FIXED_TOP header for annuaires-table		
										
										// flags of tables - "activity" 
										var main_table_active = false;		
										var extra_table_active = false;		
										
										// main annuaires table
										var table =  $("#tableClasses2222");
										var header_pos = $(table).offset();
										var header_tr = $(table).find('tr').eq(0);		
										var top_offset = header_pos.top + $(header_tr).height();
										var left_offset = $(table)[0].getBoundingClientRect().left   + $(window)['scrollLeft']();
			
										
										// set/calculate offsets & widths for scroll table`s td
										$("#scroll_table_header").css("left",left_offset);
										
										var header_tds = $(header_tr).find('th');
										var scroll_tds = $("#scroll_table_header").find("th");
										
										$(header_tds).each(function(i,item){
											el_width = $(item).width();
											$(scroll_tds).eq(i).width(el_width);											
										});
										
							  		    var table_offset_height = top_offset+$(table).height() - $("#scroll_table_header").height() - $(table).find('tr:last');			
										
										
										// Extra table 
										var extra_table =  $("#tableClasses3333");
										var extra_table_exists = false;
										
										
										if ($(extra_table).length){
											extra_table_exists = true;

											extra_header_pos = $(extra_table).offset();
											extra_header_tr = $(extra_table).find('tr').eq(0);		
											extra_top_offset = extra_header_pos.top + $(extra_header_tr).height();
											extra_left_offset = $(extra_table)[0].getBoundingClientRect().left   + $(window)['scrollLeft']();
											extra_table_height = $(extra_table).height();		


											// set/calculate offsets & widths for scroll table`s td
											$("#extra_scroll_table_header").css("left",extra_left_offset);
											$("#extra_scroll_table_header").width($(extra_table).width());
											
											extra_header_tds = $(extra_header_tr).find('th');
											extra_scroll_tds = $("#extra_scroll_table_header").find("th");
											
											$(extra_header_tds).each(function(i,item){
												el_width = $(item).width();
												$(extra_scroll_tds).eq(i).width(el_width);											
											});
											
											//extra_table_offset_height = top_offset+$(extra_table).height() - $("extra_scroll_table_header").height();												
										}
											
										
										
										$(window).scroll(function(){
											var pos = $(document).scrollTop();
											
										
											// catch scrolling for main table
											if ( (pos>top_offset)){
												if (pos > table_offset_height){
													main_table_active = false;
													$("#scroll_table_header").hide();
												}else{
													$("#scroll_table_header").show();
												}
												//console.warn('Show');
											}else{
												$("#scroll_table_header").hide();																							
												//console.warn('Hide');
											}
											
											// catch scrolling for extra table
																						
											// catch scrolling for main table
											if (extra_table_exists){
												if ( (pos>extra_top_offset)){
													$("#extra_scroll_table_header").show();
												}else{
													$("#extra_scroll_table_header").hide();																							
												}											
											}
																						
											//console.log('extra_top_offset = '+top_offest);		
											console.log(extra_table_exists);									
										});										
										
										
										
										// Sorting annuaires-tables
										$(".tableAnnuaires .sort_field").on('click', function(){
											// order by : 0 - ASC, 1 - DESC
											var order = 1;
										    var sort_field = $(this).prop('id');
											//var alexa_bigest = 99999999;
											//  disable alexa-rank sorting
											//if ( sort_field == 'ar' )
												//return;
																						
											
											if (parseInt($(this).data('order')) > 0){
												$(this).data('order',-1);
											}else{
												$(this).data('order',1);
												order = -1;
											}
											
											// set specified sort-class
											if ( sort_field == 'td' ){
												sort_field = 'tb';
											}
											
											console.warn(sort_field,' ',order);
											
											var $table,$rows;
											
											if (!$(this).data('not_filter')){
												$table  = $('#tableClasses2222'); // main table 								
											}else{
												$table  = $('#tableClasses3333'); // extra table     												
											}
											
											$rows = $($table).find('tr:not(:eq(0))');     												

											
											
											//return;

											$rows.sort(function(a, b) {
												var valueA = $(a).find('.'+sort_field).data('sort_value');
												var valueB = $(b).find('.'+sort_field).data('sort_value');
												
												
												/*
												if ( (sort_field == 'ar') && (valueA == 0)){
													valuaA = alexa_bigest;
													console.warn(alexa_bigest);
												}												
												
												if ( (sort_field == 'ar') && (valueB == 0)){
													valuaB = alexa_bigest;
												}*/												
												
												
												if( valueA < valueB ) {
													return 1*order;
												}
												
												if( valueA > valueB ) {
													return -1*order;
												}
												return 0;																								
											});

											$rows.each(function(index, row){
											  $table.append(row);                 
											});											
										
										});
										
										
									})}
								)(jQuery);
							</script>
							
						
							<!-- Main Annuiare Table --> 
                            <table class="simple-table tableAnnuaires " id="tableClasses2222" style="float:left;width:1140px;margin-right:0px;">
                <!--                                <caption class="titreCaption"> <?PHP echo $titreListe; ?></caption>-->
                                <tbody>               


                                    <tr>
                                        <th>
                                            <input id="checkClasseses2222" class="checkClasseses" name="checkClasseses" type="checkbox" value="0" title="Tout Cocher" rel="2222"/>&nbsp;
                                            <label class="sort_field"  id="annuaire" style="color:white;display:inline-block;">
												<?PHP echo "Nos Annuaires"; ?>
											</label>
                                        </th>
                                        <th id="cp" class="sort_field" >
                                            Type
                                        </th>
								
                                        <th id="vip_state" class="sort_field">
											Partenariat?
										</th>
										
                                        <th id="tarifW" class="sort_field">
                                            Tarif
                                        </th>
                                        <th id="ar" class="sort_field">
                                            Alexa Rank
                                        </th>
                                        <!--<th id="page_rank">
                                            Page Rank
                                        </th> -->
                                        <th id="age" class="sort_field">
                                            �ge (ans)
                                        </th>
                                        <th id="tf" class="sort_field">
                                            Trust Flow
                                        </th>
                    <!--                                    <th>
                                                            Citation Flow
                                                        </th>-->
                                        <th id="tv" class="sort_field" style="font-size:10px;width:40px;">
                                            Temps de validation
                                        </th>
                                        <th id="tav" class="sort_field" style="font-size:9px;width:30px;">
                                            Taux de validation
                                        </th>
                                        <th id="td" class="sort_field" style="font-size:9px;width: 40px;" >
                                            Domaine r�f�rents
                                        </th>
                                        <th style="min-width:80px;" id="liste">
                                            Cat�gories
                                        </th>

                                    </tr>
                                    <?PHP
//                                print_r($annuairesFromList);

									$annuaireModel = new Annuaire;
								
                                    foreach ($resultAnnuaireAdmin as $lineNosAnnuaires) {
															
										//var_dump($lineNosAnnuaires); die;
                                        if ($lineNosAnnuaires['annuaire'] != "") {
											
											$checked = "";
                                            if (in_array($lineNosAnnuaires['id'], $annuairesFromList)) {
                                                $checked = 'checked="checked"';
                                            }
                                            ?>
                                            <tr>
												<?php $annuaire_title = Functions::longeur(Functions::urlWithoutHash($lineNosAnnuaires['annuaire'], 1), 100);?>
                                                <?php $annuaire_title = str_replace('www.','',$annuaire_title);?>
												<td class="annuaire <?php if ($lineNosAnnuaires['new']) {?> has_new_annuaire <?php } ?>" data-sort_value="<?php echo $annuaire_title;?>"  style="position:relative;" >
                                                    <?php if ($lineNosAnnuaires['new']){ ?>
														<div class="new_annuaire">Nouveau</div>
													<?php } ?>
													
													<div class="relative">
														<input type="checkbox" value="<?PHP echo $lineNosAnnuaires['id']; ?>" id="check<?PHP echo $lineNosAnnuaires['id']; ?>" name="check[]" <?PHP echo $checked; ?> class="checkedLine"/>
														&nbsp; &nbsp;
														<label style="display:inline-block;font-size:12px;" for="check<?PHP echo $lineNosAnnuaires['id']; ?>"><?PHP echo Functions::longeur(Functions::urlWithoutHash($lineNosAnnuaires['annuaire'], 1), 100); ?></label>
														<?PHP if (((isAdmin() || isSu()) && $lineNosAnnuaires['importedBy'] == 0) || (isWebmaster() && $lineNosAnnuaires['importedBy'] == $idUser)) { ?>
															<a style="display:block;float:right;margin:0px 5px;" href="compte/supprimer/annuaire.html?id=<?PHP echo $lineNosAnnuaires['id']; ?>" title="Supprimer Annuaire" onclick="return confirmation();"><i class="icon-trash"></i> </a>        
															<a style="display:block;float:right;" href="compte/modifier/annuaire.html?id=<?PHP echo $lineNosAnnuaires['id']; ?>" title="Modifier Annuaire"><i class="icon-edit"></i> </a>
														<?PHP } ?>
														
														<br/>
													</div>
												</td>

                                                <td class="cp" data-sort_value="<?php echo $lineNosAnnuaires['cp'];?>" >
                                                    <?PHP
                                                    $typeAnnuaire = array('Annuaire', 'CP');
                                                    echo $typeAnnuaire[$lineNosAnnuaires['cp']];
                                                    ?> 
                                                </td>
									
												<?php $vip_state = (!empty($lineNosAnnuaires['vip_state']))? true: false; ?>												
												<td class="vip_state" data-sort_value="<?php echo intval($vip_state);?>"  >
													<?php $vip_text = $lineNosAnnuaires['vip_text']; ?>
													<? if ($vip_state){?>
														OUI<br/>
														<span class="vip_text" data-notice="<?php echo $lineNosAnnuaires['vip_text']; ?>" >Voir l'avantage ?</span>
													<?php } else { ?>
														NON
													<?php }?>
												</td>
												
												<?php 
													$annuaire_price =  $lineNosAnnuaires['tarifW'] + $remuneration;
													
													//$test_price = 'Base price ='.$annuaire_price.' ';
													// calculate specified price 
													if ($user_min_words_count){
														$extra_price = $annuaireModel->calculateExtraPrice($lineNosAnnuaires,$user_min_words_count);
														if ($extra_price){
															$annuaire_price += $extra_price;
														}
														//$test_price .=' +  Extra = '.$extra_price;
													}
												?>
												
                                                <td style="font-size:11px;" class="tarifW" data-sort_value="<?php echo $annuaire_price;?>">
                                                    <?php //echo 'Rem ='.$remuneration; ?>
                                                    <?php // 'Tar ='.$lineNosAnnuaires['tarifW'];  ?>
													<?php echo $test_price;?><br/>														
                                                    <?PHP echo $annuaire_price; ?> <i class="icon-euro"></i>
													<?php if ( ($user_min_words_count > $lineNosAnnuaires['max_words_count']) && ( $lineNosAnnuaires['max_words_count'] > 0) ){?>
														<div class="max_limit_annuaire">L'annuaire n'accepte que <b><?php echo $lineNosAnnuaires['max_words_count']; ?></b> mots max!</div>
													<?php } ?>
                                                </td>


												<?php $alexa_rank = intval($lineNosAnnuaires['ar']); ?>
                                                 <td class="ar" data-sort_value="<?php echo ($alexa_rank)? $alexa_rank : 99999999;?>">
                                                    <?PHP
                                                    $lienExterne = $lineNosAnnuaires['annuaire'];

                                                    $lienExterne = preg_replace("#http://#", "", $lienExterne);
                                                    $lienExterne = preg_replace("#www.#", "", $lienExterne);
//                                                        echo $lienExterne;
//                                                        echo $annuaires[$limit1]['annuaire'];
                                                    //echo "<div style='overflow:hidden;height:15px;width:100px;display:inline-block;'><div style='margin-top:-25px;'>" . Functions::getAlexaRank($lienExterne) . "</div></div>";
														?>
													<?php echo ($alexa_rank)? $alexa_rank: '---';?>
                                                </td>
												
												<?php /*
                                                <td style="font-weight:normal;" title="<?PHP echo $lineNosAnnuaires['page_rank']; ?>" class="page_rank">
                                                    <?PHP
                                                    echo "" . Functions::getGoogleRank($lineNosAnnuaires['annuaire']);
//                                                echo "" . $lineNosAnnuaires['page_rank'];
                                                    ?>
                                                </td> */ ?>

												<?php 
													$annuaire_age = 0; 
													 if ($lineNosAnnuaires['age'] > 0) {
														$annuaire_age = floor((time() - intval($lineNosAnnuaires['age'])) / (31536000));
													}												
												?>												
                                                <td class="age" data-sort_value="<?php echo $annuaire_age;?>" >
                                                    <?PHP
                                                    if ($lineNosAnnuaires['age'] > 0) {
                                                        echo floor((time() - intval($lineNosAnnuaires['age'])) / (31536000));
                                                    } else {
                                                        echo 0;
                                                    }
//                                                echo ' an(s)';
                                                    ?> 
                                                </td>
                                                <td class="tf" data-sort_value="<?php echo intval($lineNosAnnuaires['tf']);?>" >
                                                    <?PHP echo intval($lineNosAnnuaires['tf']); ?>
                                                </td>
                    <!--                                            <td>
                                                <?PHP //echo intval($lineNosAnnuaires['cf']); ?>
                                                </td>-->
                                                <td class="tv" data-sort_value="<?php echo intval($lineNosAnnuaires['tv']);?>" >
                                                    <?php
                                                    $tv = intval($lineNosAnnuaires['tv']);
                                                    $tv_value = '';
                                                    if (!$tv) {
                                                        $tv_value = 'N/A';
                                                    } else {
                                                        $tv_value = $tv . " Jr(s)";
                                                    }
                                                    ?>
                                                    <?php echo $tv_value; ?>
                                                </td>
                                                <td class="tav" data-sort_value="<?php echo intval($lineNosAnnuaires['tav']);?>" >
                                                    <?php
                                                    $tav = intval($lineNosAnnuaires['tav']);

                                                    $styleTaux = 'font-weight:bold;font-size:12px;';
                                                    if ($tav >= 80) {
                                                        $styleTaux .="color:#008000;";
                                                    } else if ($tav >= 65 && $tav < 80) {
                                                        $styleTaux .="color:#CDCD0D";
                                                    } else if ($tav >= 50 && $tav < 65) {
                                                        $styleTaux .="color:orange";
                                                    } else if ($tav > 0 && $tav < 50) {
                                                        $styleTaux .="color:red";
                                                    }

                                                    $tav_value = '';
                                                    if (!$tav) {
                                                        $tav_value = 'N/A';
                                                    } else {
                                                        $tav_value = $tav . " %";
                                                    }
                                                    ?>
                                                    <span style="<?PHP echo $styleTaux; ?>"><?PHP echo $tav_value; ?></span>
                                                </td>
                                                <td class="tb" data-sort_value="<?php echo intval($lineNosAnnuaires['tdr']);?>" >
                                                    <?PHP echo intval($lineNosAnnuaires['tdr']) . ""; ?>
                                                </td>

                                                <td style="font-size:11px;text-transform:capitalize" class="liste">
                                                    <?PHP
                                                    $checklist = "";
                                                    $affichList = 0;
                                                    foreach ($adminListes as $adminListeLine) {

                                                        if (in_array($lineNosAnnuaires['id'], $adminListeLine['annuairesListArray'])) {
                                                            $checklist = $adminListeLine['libelle'];
                                                            $style = "";
                                                            if ($liste == $adminListeLine['id']) {
                                                                $style = "font-weight:bold;";
                                                            }
                                                            if ($checklist != "") {
                                                                $annuaireLName = preg_replace("#Annuaires#", "", $checklist);
                                                                $annuaireLName = preg_replace("#Accepte la#", "", $annuaireLName);
                                                                $annuaireLName = preg_replace("#acceptant#", "", $annuaireLName);
                                                                $annuaireLName = preg_replace("#Les#", "", $annuaireLName);

                                                                echo "- <span style='" . $style . "'>" . $annuaireLName . "</span><br/>";
                                                                $affichList++;
                                                            }
//                                                        break;
                                                        }
                                                    }



                                                    if ($affichList == 0) {
                                                        echo "- Non-Class�";
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?PHP
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>


                        <?php if ($show_not_filtered && count($not_filtered_annuiares) > 0): ?>
						
							<?php

							if (!empty($last_viewed)){		
								$sort_by_last_viewed = false; 
								
								foreach($not_filtered_annuiares as &$annuaire){
									$annuaire_created = strtotime($annuaire['created']);
									if ($annuaire_created >= $last_viewed){
										$annuaire['new'] = true;		
										if (!$sort_by_last_viewed)
											$sort_by_last_viewed = true;
									}		
								}
								unset($annuaire); // reset 
								
								
								// sorting for new
								if ($sort_by_last_viewed){
									usort($not_filtered_annuiares, function($a, $b) use($last_viewed) {
										$annuaire_created = strtotime($a['created']);
										return ( $last_viewed > $annuaire_created)? 1 : -1;
									});
								}
							}
							?>						
						
						
                            <!-- voodoo417 -->
                            <div class="clear"></div>
                            <div id="not_filtered_block">
                                <h2>Autres annuaires ne correspondant pas � vos crit�res de recherche</h2>


                                <div class="not_filtered_outside">
                                    <table id="tableClasses3333" class="simple-table tableAnnuaires" id="tableClasses-not-filtered" style="float:left;width:1140px;max-width:1140px;margin-right:0px;">
                                        <!--                                <caption class="titreCaption"> <?PHP echo $titreListe; ?></caption>-->
                                        <tbody>


                                            <tr>
                                                <th>
                                                    <input id="checkbox_not_filtered" class="checkClasseses" name="checkClasseses" type="checkbox" value="0" title="Tout Cocher" rel="3333"/>&nbsp;
                                                    <label class="sort_field" data-not_filter="1" id="annuaire" style="color:white;display:inline-block;">
														<?PHP echo "Nos Annuaires"; ?></label>
							     				    </label>
                                                </th>
                                                <th id="cp" class="sort_field" data-not_filter="1"  >Type</th>
												<th id="vip_state" class="sort_field" data-not_filter="1" >
													Partenariat?
												</th>												
                                                <th id="tarifW" class="sort_field" data-not_filter="1" >Tarif</th>
                                                <th id="ar" class="sort_field" data-not_filter="1" >Alexa Rank</th>
                                               <!-- <th id="page_rank">Page Rank</th> -->
                                                <th id="age" class="sort_field" data-not_filter="1" >�ge (ans)</th>
                                                <th id="tf" class="sort_field" data-not_filter="1" >Trust Flow</th>
                                                <!-- th>
                                                                                        Citation Flow
                                                                                    </th>-->
                                                <th id="tv" class="sort_field" data-not_filter="1" style="font-size:10px;width:40px;" >Temps de validation</th>
                                                <th id="tav" class="sort_field" data-not_filter="1" style="font-size:9px;width:30px;">Taux de validation</th>
                                                <th id="tb" class="sort_field" data-not_filter="1" >Domaine r�f�rents</th>
                                                <th style="min-width:80px;" id="liste">Cat�gories</th>

                                            </tr>

                                            <?php
											
											// changed filter from "Backlinks" to "Domaine Referents"
											// field "tb" => field "tdr"
											// 		
											if ($otherData == 'tb-1') {
												usort($not_filtered_annuiares, function($a, $b) {
													$a_tb = intval($a['tdr']);
													$b_tb = intval($b['tdr']);
													if (($a_tb == 0) || ($b_tb == 0)) { // check N/A
														return $b_tb - $a_tb;
													} else {
														return $b_tb - $a_tb;
													}
												});
											} elseif ($otherData == 'tb-0') {

												usort($not_filtered_annuiares, function($a, $b) {
													$a_tb = intval($a['tdr']);
													$b_tb = intval($b['tdr']);
													if (($a_tb == 0) || ($b_tb == 0)) { // check N/A
														return $b_tb - $a_tb;
													} else {
														return $a_tb - $b_tb;
													}
												});
                        }
											
											
                                            // tv N/A sort

                                            if ($otherData == 'tv-1') {

                                                usort($not_filtered_annuiares, function($a, $b) {
                                                    $a_tv = intval($a['tv']);
                                                    $b_tv = intval($b['tv']);
                                                    if (($a_tv == 0) || ($b_tv == 0)) { // check N/A
                                                        return $b_tv - $a_tv;
                                                    } else {
                                                        return $b_tv - $a_tv;
                                                    }
                                                });
                                            } elseif ($otherData == 'tv-0') {

                                                usort($not_filtered_annuiares, function($a, $b) {
                                                    $a_tv = intval($a['tv']);
                                                    $b_tv = intval($b['tv']);
                                                    if (($a_tv == 0) || ($b_tv == 0)) { // check N/A
                                                        return $b_tv - $a_tv;
                                                    } else {
                                                        return $a_tv - $b_tv;
                                                    }
                                                });
                                            }

                                            if ($otherData == 'tav-1') {

                                                usort($not_filtered_annuiares, function($a, $b) {
                                                    $a_tav = intval($a['tav']);
                                                    $b_tav = intval($b['tav']);
                                                    if (($a_tav == 0) || ($b_tav == 0)) { // check N/A
                                                        return $b_tav - $a_tav;
                                                    } else {
                                                        return $b_tav - $a_tav;
                                                    }
                                                });
                                            } elseif ($otherData == 'tav-0') {

                                                usort($not_filtered_annuiares, function($a, $b) {
                                                    $a_tav = intval($a['tav']);
                                                    $b_tav = intval($b['tav']);
                                                    if (($a_tav == 0) || ($b_tav == 0)) { // check N/A
                                                        return $b_tav - $a_tv;
                                                    } else {
                                                        return $a_tav - $b_tav;
                                                    }
                                                });
                                            }


                                            foreach ($not_filtered_annuiares as $lineNosAnnuaires) {
                                                if ($lineNosAnnuaires['annuaire'] != "") {
                                                    $checked = "";
                                                    if (in_array($lineNosAnnuaires['id'], $annuairesFromList)) {
                                                        $checked = 'checked="checked"';
                                                    }
                                                    ?>
                                                    <tr>
														<?php $annuaire_title = Functions::longeur(Functions::urlWithoutHash($lineNosAnnuaires['annuaire'], 1), 100);?>
														<?php $annuaire_title = str_replace('www.','',$annuaire_title);?>	                                          
														
														
														<td class="annuaire <?php if ($lineNosAnnuaires['new']) {?> has_new_annuaire <?php } ?>" data-sort_value="<?php echo $annuaire_title;?>" style="position:relative;"  >
															
															<?php if ($lineNosAnnuaires['new']){ ?>
																<div class="new_annuaire">Nouveau</div>
															<?php } ?>
															
															<div class="relative">		
																<input type="checkbox" value="<?PHP echo $lineNosAnnuaires['id']; ?>" id="check<?PHP echo $lineNosAnnuaires['id']; ?>" name="check[]" <?PHP echo $checked; ?> class="checkedLine"/>
																&nbsp; &nbsp;
																<label style="display:inline-block;font-size:12px;" for="check<?PHP echo $lineNosAnnuaires['id']; ?>"><?PHP echo Functions::longeur($lineNosAnnuaires['annuaire'], 100); ?></label>
																<?PHP if (((isAdmin() || isSu()) && $lineNosAnnuaires['importedBy'] == 0) || (isWebmaster() && $lineNosAnnuaires['importedBy'] == $idUser)) { ?>
																	<a style="display:block;float:right;margin:0px 5px;" href="compte/supprimer/annuaire.html?id=<?PHP echo $lineNosAnnuaires['id']; ?>" title="Supprimer Annuaire" onclick="return confirmation();"><i class="icon-trash"></i> </a>
																	<a style="display:block;float:right;" href="compte/modifier/annuaire.html?id=<?PHP echo $lineNosAnnuaires['id']; ?>" title="Modifier Annuaire"><i class="icon-edit"></i> </a>
																<?PHP } ?>
															</div>
															
                                                        </td>

                                                        <td class="cp" data-sort_value="<?php echo $lineNosAnnuaires['cp'];?>"  >
                                                            <?PHP
                                                            $typeAnnuaire = array('Annuaire', 'CP');
                                                            echo $typeAnnuaire[$lineNosAnnuaires['cp']];
                                                            ?>
                                                        </td>
														
														
														<?php $vip_state = (!empty($lineNosAnnuaires['vip_state']))? true: false; ?>												
														<td class="vip_state" data-sort_value="<?php echo intval($vip_state);?>"  >
															<?php $vip_text = $lineNosAnnuaires['vip_text']; ?>
															<? if ($vip_state){?>
																OUI<br/>
																<span class="vip_text" data-notice="<?php echo $lineNosAnnuaires['vip_text']; ?>" >Voir l'avantage ?</span>
															<?php } else { ?>
																NON
															<?php }?>
														</td>														

														
														
											

                                                        <?php

														$annuaire_price =  $lineNosAnnuaires['tarifW'] + $remuneration;
														
														//$test_price = 'Base price ='.$annuaire_price.' ';
														// calculate specified price 
														if ($user_min_words_count){
															$extra_price = $annuaireModel->calculateExtraPrice($lineNosAnnuaires,$user_min_words_count);
															if ($extra_price){
																$annuaire_price += $extra_price;
															}
															//$test_price .=' +  Extra = '.$extra_price;
														}														
														
                                                        $marker_current = false;
                                                        if (isset($not_filtered_params['tarifW']) && Functions::compareFieldWithFilter($not_filtered_params['tarifW'], $annuaire_price))
                                                            $marker_current = true;
                                                        ?>
														
														<?php 
															//var_dump($not_filtered_params['tarifW']);	
														?>
												
														<td style="font-size:11px;" class="tarifW  <?php if ($marker_current) { ?>not_filter<?php } ?>" data-sort_value="<?php echo $annuaire_price;?>">
															<?php //echo 'Rem ='.$remuneration; ?>
															<?php // 'Tar ='.$lineNosAnnuaires['tarifW'];  ?>
															<?php echo $test_price;?><br/>														
															<?PHP echo $annuaire_price; ?> <i class="icon-euro"></i>
															<?php if ( ($user_min_words_count > $lineNosAnnuaires['max_words_count']) && ( $lineNosAnnuaires['max_words_count'] > 0) ){?>
																<div class="max_limit_annuaire">L'annuaire n'accepte que <b><?php echo $lineNosAnnuaires['max_words_count']; ?></b> mots max!</div>
															<?php } ?>
														</td>
														
														<?php /*
															  <td style="font-size:11px;" class="tarifW  <?php if ($marker_current) { ?>not_filter<?php } ?>" data-sort_value="<?php echo ($lineNosAnnuaires['tarifW'] + $remuneration);?>">
																<?PHP echo ($lineNosAnnuaires['tarifW'] + $remuneration); ?> <i class="icon-euro"></i>
															  </td> 
															  */ 
														?>
	
														<?php $alexa_rank = intval($lineNosAnnuaires['ar']); ?>														
                                                        <td class="ar" data-sort_value="<?php echo ($alexa_rank)? $alexa_rank : 99999999;?>">
                                                            <?PHP
                                                            $lienExterne = $lineNosAnnuaires['annuaire'];

                                                            $lienExterne = preg_replace("#http://#", "", $lienExterne);
                                                            $lienExterne = preg_replace("#www.#", "", $lienExterne);
                                                            //                                                        echo $lienExterne;
                                                            //                                                        echo $annuaires[$limit1]['annuaire'];
                                                            //echo "<div style='overflow:hidden;height:15px;width:100px;display:inline-block;'><div style='margin-top:-25px;'>" . Functions::getAlexaRank($lienExterne) . "</div></div>";
                                                            ?>
															
															<?php echo ($alexa_rank)? $alexa_rank: '---';?>

                                                        </td>
                                                        <?php
                                                        $marker_current = false;
                                                        if (isset($not_filtered_params['page_rank']) && Functions::compareFieldWithFilter($not_filtered_params['page_rank'], $lineNosAnnuaires['page_rank']))
                                                            $marker_current = true;
                                                        ?>
														
														<?php /*
                                                        <td style="font-weight:normal;" title="<?PHP echo $lineNosAnnuaires['page_rank']; ?>"
                                                            class="page_rank  <?php if ($marker_current) { ?>not_filter<?php } ?>">
                                                                <?PHP
                                                                echo "" . Functions::getGoogleRank($lineNosAnnuaires['annuaire']);
                                                                //    echo "" . $lineNosAnnuaires['page_rank'];
                                                                ?>
                                                        </td> */ ?>
														
                                                        <?php

														$annuaire_age = 0; 
														 if ($lineNosAnnuaires['age'] > 0) {
															$annuaire_age = floor((time() - intval($lineNosAnnuaires['age'])) / (31536000));
														}				
														
                                                        $marker_current = false;
                                                        if (isset($not_filtered_params['age']) && Functions::compareFieldWithFilter($not_filtered_params['age'], $lineNosAnnuaires['age']))
                                                            $marker_current = true;
                                                        ?>
                                                        <td class="age  <?php if ($marker_current) { ?>not_filter<?php } ?>" data-sort_value="<?php echo $annuaire_age;?>">
                                                            <?PHP
                                                            if ($lineNosAnnuaires['age'] > 0) {
                                                                echo floor((time() - intval($lineNosAnnuaires['age'])) / (31536000));
                                                            } else {
                                                                echo 0;
                                                            }
                                                            //                                                echo ' an(s)';
                                                            ?>
                                                        </td>
														
                                                        <?php
                                                        $marker_current = false;
                                                        if (isset($not_filtered_params['tf']) && Functions::compareFieldWithFilter($not_filtered_params['tf'], $lineNosAnnuaires['tf']))
                                                            $marker_current = true;
                                                        ?>
                                                        <td class="tf <?php if ($marker_current) { ?>not_filter<?php } ?>" data-sort_value="<?php echo intval($lineNosAnnuaires['tf']);?>">
                                                            <?PHP echo intval($lineNosAnnuaires['tf']); ?>
                                                        </td>
														
                                                        <!--                                            <td>
                                                        <?PHP echo intval($lineNosAnnuaires['cf']); ?>
                                                                    </td>-->

                                                        <?php
                                                        $marker_current = false;
                                                        if (isset($not_filtered_params['tv']) && Functions::compareFieldWithFilter($not_filtered_params['tv'], intval($lineNosAnnuaires['tv'])))
                                                            $marker_current = true;
                                                        ?>											
                                                        <td class="tv  <?php if ($marker_current) { ?>not_filter<?php } ?>" data-sort_value="<?php echo intval($lineNosAnnuaires['tv']);?>" >
                                                            <?php
                                                            $tv = intval($lineNosAnnuaires['tv']);
                                                            $tv_value = '';
                                                            if (!$tv) {
                                                                $tv_value = 'N/A';
                                                            } else {
                                                                $tv_value = $tv . " Jr(s)";
                                                            }
                                                            ?>
                                                            <?php echo $tv_value; ?>
                                                        </td>

                                                        <td class="tav" data-sort_value="<?php echo intval($lineNosAnnuaires['tav']);?>">
                                                            <?php
                                                            $tav = intval($lineNosAnnuaires['tav']);

                                                            $styleTaux = 'font-weight:bold;font-size:12px;';
                                                            if ($tav >= 80) {
                                                                $styleTaux .="color:#008000;";
                                                            } else if ($tav >= 65 && $tav < 80) {
                                                                $styleTaux .="color:#CDCD0D";
                                                            } else if ($tav >= 50 && $tav < 65) {
                                                                $styleTaux .="color:orange";
                                                            } else if ($tav > 0 && $tav < 50) {
                                                                $styleTaux .="color:red";
                                                            }

                                                            $tav_value = '';
                                                            if (!$tav) {
                                                                $tav_value = 'N/A';
                                                            } else {
                                                                $tav_value = $tav . " %";
                                                            }
                                                            ?>
                                                            <span style="<?PHP echo $styleTaux; ?>"><?PHP echo $tav_value; ?></span>
                                                        </td>

                                                        <?php
                                                        $marker_current = false;
                                                        if (isset($not_filtered_params['tdr']) && Functions::compareFieldWithFilter($not_filtered_params['tdr'], intval($lineNosAnnuaires['tdr'])))
                                                            $marker_current = true;
                                                        ?>										
                                                        <td class="tb  <?php if ($marker_current) { ?>not_filter<?php } ?>" data-sort_value="<?php echo intval($lineNosAnnuaires['tdr']);?>">
                                                            <?PHP echo intval($lineNosAnnuaires['tdr']) . ""; ?>
                                                        </td>

                                                        <td style="font-size:11px;text-transform:capitalize" class="liste">
                                                            <?PHP
                                                            $checklist = "";
                                                            $affichList = 0;
                                                            foreach ($adminListes as $adminListeLine) {

                                                                if (in_array($lineNosAnnuaires['id'], $adminListeLine['annuairesListArray'])) {
                                                                    $checklist = $adminListeLine['libelle'];
                                                                    $style = "";
                                                                    if ($liste == $adminListeLine['id']) {
                                                                        $style = "font-weight:bold;";
                                                                    }
                                                                    if ($checklist != "") {
                                                                        $annuaireLName = preg_replace("#Annuaires#", "", $checklist);
                                                                        $annuaireLName = preg_replace("#Accepte la#", "", $annuaireLName);
                                                                        $annuaireLName = preg_replace("#acceptant#", "", $annuaireLName);
                                                                        $annuaireLName = preg_replace("#Les#", "", $annuaireLName);

                                                                        echo "- <span style='" . $style . "'>" . $annuaireLName . "</span><br/>";
                                                                        $affichList++;
                                                                    }
//                                                        break;
                                                                }
                                                            }



                                                            if ($affichList == 0) {
                                                                echo "- Non-Class�";
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                }
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        <?php endif; ?> <!-- Not Filtered Annuaires -->



                        <?PHP
                        if ($nbre_result > pagination) {
                            echo $list_page;
                        }
                    } else {
                        ?>
                        <br/>
                        <br/>
                        <div align="center" width="900" style="width:900px;margin:auto;">
                            <h5>Aucun annuaire trouv�.</h5>
                        </div>

                        <?PHP
                    }
                    ?>


                    <?PHP
                } else {
                    
                }
                ?>

                <div class="clear">  
                </div><br/>
                <?PHP
                if ($label != "") {
                    ?>
                    <input type="submit"

                           <?php
                           if ($_GET['block'] == "listannuairecontent") {
                               echo 'id="bottom_save_list"';
                           }
                           ?>
                           class="button color small round " value="<?PHP echo $label; ?> " style="color:white;"/>
                           <?PHP
                       }
                       ?>

            </form>
            <?PHP
            if ($_GET['block'] == "listannuairecontentt") {
                ?>

                <div style="float:right;width:300px;">
                    <h4>Ajouter un annuaire</h4>
                    <p>
                        Si votre annuaire ne se trouve pas dans la liste, vous pouvez l'ajouter via le champ suivant.
                    </p>
                    <form action="compte/ajouter/ajouterannuaire_.html" method="post" style="">
                        <fieldset class="one-fourth">
                            <label><span> </span>URL de l'annuaire: </label>
                            <input name="annuaire" class="search-field" type="text" value='' style="margin-bottom:5px;width:250px;" maxlength="300"><br/>
                        </fieldset>
                        <fieldset class="one-fourth">
                            <label><span> </span>Lien retour ? </label>
                            <div style="width:50px;float:left;">
                                <input name="retour" checked="checked" class="search-field" type="radio" value='1' style="" id="oui"><label for="oui" style="width:15px;margin-top:-5px;">Oui</label>
                            </div>
                            <div style="width:50px;float:left;margin-left:20px;">
                                <input name="retour" class="search-field" type="radio" value='0' style="" id="non"><label for="non" style="width:15px;margin-top:-5px;">Non</label>
                            </div>
                            <br/>
                            <br/>
                        </fieldset>
                        <fieldset class="one-fourth" style="width:300px;">
                            <label><span>* </span>Ajouter � autre(s) liste(s) :</label>
                            <?PHP
                            $ids = 0;
                            if (!empty($_GET['id'])) {
                                $ids = intval($_GET['id']);
                            }
                            echo Functions::getUserAnnuairelistCheckCase($ids);
                            ?>
                        </fieldset>
                        <div class="clear">  </div>
                        <input type="submit" class="button color small round" value="Ajouter l'annuaire" style="color:white;"/>
                    </form>
                    <div>
                        <br/>
                        <strong style="color:red;">- ou -</strong>
                    </div>

                    <a href="./compte/importer.html?id=<?PHP echo $idMod; ?>" class="button color small round" style="color:white;float:left;margin:10px 0px 5px 00px;">Importer mes annuaires</a>


                </div>

                <?PHP
            }
            ?>
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>