<?PHP
@session_start();

if (!isset($_GET['block']) || empty($_GET['block'])) {
    //header('location:' . $_SERVER['HTTP_REFERER']);
}

if (!isset($_GET['id']) || empty($_GET['id'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}

$page = 1;



include_once('files/includes/topHaut.php');

// models
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionLikes.php';
// helpers
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionViewHelper.php';

$project_id = intval($_GET['id']);
$RedactionProjects = new RedactionProjects($dbh);
$reportsModel = new RedactionReports($dbh);
$likesModel = new RedactionLikes();
$project = $RedactionProjects->getProject($project_id);

$viewHelper = new RedactionViewHelper();

// set action
$action = 'task.html';

// review task
$report = $reportsModel->getReport($project_id);
$report_data = unserialize($report['report']);

$like_state = -1; // not liked
$likes = $likesModel->getWritersLikesByProjectId($project_id);
if (!empty($likes)){
    $like_state = $likes[0]['value'];
    $unliked_text = $likes[0]['webmaster_comment'];
    $is_unliked_text = empty($unliked_text)? false : true;
}

// show only for owner
// using js for redirect - fucking code-architecture

if (!(isSuperReferer() || isAdmin() || isSu())) {

    if ($project['status'] != 'finished') {
        echo "<script type='text/javascript'> document.location = '/'; </script>";
    }

    if (isWebmaster()) {
        if ($idUser != $project['webmaster_id']) {
            echo "<script type='text/javascript'> document.location = '/'; </script>";
        }
    }
}

// has been viewed by client
if (isWebmaster() && ($project['status']=='finished')){
    $RedactionProjects->setViewed($project_id,1);
}

?>
<!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> -->
<div class="container" xmlns="http://www.w3.org/1999/html">
    <div class="one">
        <script type="text/javascript">
            (function($){
                $(document).ready(function(){

                    $('#view_source').click(function(e){
                        e.preventDefault();
                        $(".hidden_source_block").show();
                        var s_height = document.getElementById('source').scrollHeight;
                        //s_height = 500;
                        //document.getElementById('source').setAttribute('style','height:'+s_height+'px');
                        //document.getElementById('source').setAttribute('style','width:100%');
                    });

                    $("#modify_btn").click(function(){
                        $("#modify_block").show();
                    });

                    $("#modify_send_btn").click(function(e){
                        e.preventDefault();
                        var comment = $('#webmaster_comment').val();
                        if (!comment.length){
                            $('#webmaster_comment').addClass('error_input');
                        }else{
                            if (confirm('En �tes-vous s�r?')) {
                                $("#comment_form").submit();
                            }
                        }
                    });


                    // Dialog unliked comment

                    // modal processing unliked comment
                    $('#redaction_unliked_modal .send_comment').on('click',function(){
                        var modal = $('#redaction_unliked_modal');
                        var comment = $(modal).find('.unlike_comment');

                        $(comment).removeClass('error_empty');


                        // close modal/ without unliked comment
                        if ($(this).data('finish')){
                            $("#redaction_unliked_modal").hide();
                            $(document).find('.blocker').hide();
                            $(document).find('body').css('overflow','');
                            $(comment).val('');
                            return;
                        }


                        // check if webmaster inputs comment
                        var unliked_text = $(comment).val();
                        if (!unliked_text.length) {
                            $(comment).addClass('error_empty');
                            return;
                        }else{

                            var data = {
                                writer_id: $(modal).data('writer_id'),
                                project_id: $(modal).data('project_id'),
                                value: $(modal).data('like_state'),
                                unliked_comment: unliked_text,
                                type: 'redaction_writer_likes'
                            };

                            // current unliked block at table
                            var unlike_block = $('#unlike_'+data.writer_id+'_'+data.project_id);
                            console.log('#unlike_'+data.writer_id+'_'+data.project_id);

                            var unliked_comment = $(unlike_block).find('.unlike_comment');
                            $(unliked_comment).removeClass('error_empty');
                            var like_btn = $(unlike_block).find('.like_button');

                            $.post("files/includes/ajax/update.php", data, function (repsonse) {
                                if (repsonse) {
                                    var unlike_comment_btn = $(unlike_block).find('.unlike_comment_button');

                                    // like-unliked action
                                    $(unlike_comment_btn).text('Modifier').show();
                                    $(unlike_block).find('.unlike_comment_edit').text(unliked_text).show();
                                    $(unliked_comment).val(unliked_text).hide();
                                    $(like_btn).data('edition',true);
                                    $(unlike_block).show();

                                    // remove modal
                                    $("#redaction_unliked_modal").hide();
                                    $(document).find('.blocker').hide();
                                    $(document).find('body').css('overflow','');
                                    $(comment).val('');

                                }
                            });
                        }

                    });





                    // likes writers-text
                    $('.like_button').click(function(e){
                        e.preventDefault();

                        var like_btn = $(this);
                        var like_state = $(this).data('state');
                        var parent = $(this).parents('.like_buttons'); // like group container

                        // unliked
                        var unlike_block = $(parent).find('.unlike_block');
                        var unliked_comment = $(unlike_block).find('.unlike_comment');
                        $(unliked_comment).removeClass('error_empty');

                        var unlike_comment_btn = $(unlike_block).find('.unlike_comment_button');

                        var unliked_comment_action = false;
                        if ($(this).data('unlike_comment_action')){
                            unliked_comment_action = true;
                        }

                        // edition unliked text
                        if (unliked_comment_action && $(this).data('edition')){
                            $(unlike_comment_btn).text('Envoyer');
                            $(parent).find('.unlike_comment_edit').hide();
                            $(unliked_comment).show();
                            $(this).data('edition',false);
                            return;
                        }


                        console.warn('fire');

                        if (!unliked_comment_action) {
                            if (like_state) {
                                $(unlike_block).slideUp('fast');
                                $(unliked_comment).val('');
                                $(parent).find('.unlike_comment_edit').hide();
                                $(unliked_comment).show();
                                $(unlike_comment_btn).data('edition',false).text('Envoyer');
                            } else {
                                var modal = $("#redaction_unliked_modal");

                                $(modal).data('like_state',like_state);
                                $(modal).data('writer_id',$(parent).data('writer_id'));
                                $(modal).data('annuaire_id',$(parent).data('annuaire_id'));
                                $(modal).data('project_id',$(parent).data('project_id'));

                                $(modal).modal({
                                    escapeClose: false,
                                    clickClose: false,
                                    showClose: false
                                });
                            }
                        }

                        var unliked_text = $(unliked_comment).val();
                        if (unliked_comment_action){
                            console.log('Text =',unliked_text);
                            if (!unliked_text.length){
                                $(unliked_comment).addClass('error_empty');
                                return;
                            }else{
                                // bugfix
                                // unliked_text.replace(new RegExp('�', 'g'), 'oe');
                            }
                        }

                        var data = {
                            writer_id: $(parent).data('writer_id'),
                            project_id: $(parent).data('project_id'),
                            value: like_state,
                            unliked_comment: unliked_text,
                            type: 'redaction_writer_likes'
                        };


                        $(this).data('ajax_processing',true);

                        $.post("files/includes/ajax/update.php", data, function (repsonse) {
                            if (repsonse) {
                                // like-unliked action
                                if (!unliked_comment_action) {
                                    $(parent).find('.like_button').removeClass('suspended').removeClass('active');
                                    if (like_state) {
                                        $(parent).find('.like').addClass('active');
                                        $(parent).find('.unlike').addClass('suspended');
                                    } else {
                                        $(parent).find('.like').addClass('suspended');
                                        $(parent).find('.unlike').addClass('active');
                                    }
                                }

                                // send unliked action
                                if (unliked_comment_action){
                                    $(unlike_comment_btn).text('Modifier');
                                    $(parent).find('.unlike_comment_edit').text(unliked_text).show();
                                    $(unliked_comment).hide();
                                    $(like_btn).data('edition',true);
                                }
                            }
                        });

                    });

                });
            })(jQuery);
        </script>

        <div class="redaction_finished_task">
            <div><b>Nom du projet : </b><?php echo utf8_decode($project['title']);?></div>

            <?php if (!empty($project['article_title'])) { ?>
                <div><b>Titre de l'article : </b><?php echo utf8_decode($project['article_title']);?></div>
            <?php } ?>

            <?php if (!empty($project['instructions'])) { ?>
                <div><b>Consignes relatives � l'article : </b><br/>
                    <blockquote><pre><?php echo utf8_decode($project['instructions']);?></pre></blockquote>
                </div>
            <?php } ?>

            <div><b>Consignes du projet : </b><br/>
                <blockquote><pre><?php echo utf8_decode($project['desc']);?></pre></blockquote>
            </div>

            <div class="instructions">
                <b>Vous avez demand�: </b><br/>
                <?php $viewHelper->show_report($project,$report_data);?>
            </div>

            <div class="action_buttons">
                <b class="header">Votre texte: </b><a href="" id="view_source">Voir le code source de l'article</a><a href="/compte/redaction_post/get_text.html?id=<?php echo $project_id;?>" id="download_source">T�l�charger le code source de l'article</a>
            </div>

            <?php if (!empty($report['meta_title']) || (!empty($report['meta_desc'])) ){?>
                <div class="meta_block">
                   <?php if (!empty($report['meta_title'])){?>
                   <div class="meta_title">
                       <b class="header">Meta-title: </b> <?php echo  $report['meta_title'];?>
                   </div>
                   <?php } ?>
                   <?php if (!empty($report['meta_desc'])){?>
                        <div>
                            <b class="header">Meta-description: </b> <?php echo  $report['meta_desc'];?>
                        </div>
                   <?php } ?>
                </div>
            <?php } ?>

            <?php
                $meta_text = '';
                if (!empty($report['meta_title'])){
                    $meta_text .= '<title>'.$report['meta_title'].'</title>';
                }

                if (!empty($report['meta_desc'])){
                    $meta_text .= '<meta name="description" content="'.$report['meta_desc'].'" />';
                }
                $report['text']= $meta_text.$report['text'];
            ?>

            <div class="hidden_source_block" style="display: none;;">
                 <textarea id="source" style="width:100%;min-height: 400px;" ><?php echo $report['text'];?></textarea> <br/>
            </div>


            <!-- Modal - Not Optimize Articles Errors -->
            <div class="modal" id="redaction_unliked_modal"  style="display:none;">
                <div class="header">Souhaitez-vous indiquer ce que vous n'avez pas aim� ?</div>
                <textarea class="unlike_comment" ></textarea>
                <div class="footer">
                    <div class="button color big send_comment">Envoyer</div>
                    <div class="button color big send_comment without_comment" data-finish="1">Non merci !</div>
                </div>
            </div>

            <div class="source_block"">
                <div><?php echo $report['text'];?></div>
                <div class="like_buttons"
                     data-writer_id="<?php echo $project['affectedTO'];?>"
                     data-webmaster_id="<?php echo  $project['webmaster_id'];?>"
                     data-project_id="<?php echo  $project['id'];?>"
                    >
                    <span class="like_button like <?php echo ($like_state==1)?' active ':''; ?>  <?php echo ($like_state==0)?'suspended':''; ?>" data-state="1">J'aime ce texte, confier plus de t�ches � ce r�dacteur &nbsp;<i class="icon-thumbs-up"></i></span><br/>
                    <span class="like_button unlike <?php echo ($like_state==0)?' active ':''; ?> <?php echo ($like_state==1)?'suspended':''; ?>" data-state="0" href="#">Je n'aime pas ce texte, lui confier moins de projets &nbsp;<i class="icon-thumbs-down"></i></span>

                    <div id="unlike_<?php echo $project['affectedTO'].'_'.$project['id']; ?>"  class="unlike_block" <?php if ( ($like_state==0) ) { ?>style="display:block;" <?php } ?> >
                        <!--<span class="header">N'h�sitez pas � dire ce que vous n'avez pas aim�! (facultatif)</span>-->
                        <div class="unlike_comment_edit" <?php if(!empty($unliked_text)){ ?>style="display:block;" <?php } ?> ><?php echo utf8_decode($unliked_text); ?></div>

                        <textarea class="unlike_comment" style="display:none" ><?php echo utf8_decode($unliked_text); ?></textarea>

                        <div class="like_button button small round color unlike_comment_button"
                             data-state="0" data-unlike_comment_action="1"
                            <?php if($is_unliked_text){ ?> data-edition="1" style="display: inline-block;"<?php } ?>
                            <?php if(!$is_unliked_text){ ?> style="display: none;"<?php } ?>
                            >
                            <?php echo (!$is_unliked_text)? 'Envoyer' : 'Modifier'; ?>
                        </div>

                    </div>

                </div>
                <div id="modify_btn">Je souhaite des modifications</div>
                <div id="modify_block">
                    <header>Afin que nous puissions apporter les corrections n�cessaires, merci de nous signaler ce que ne vous convient pas:</header>
                    <form action="./compte/redaction_post/<?php echo $action;?>" id="comment_form" method="post" style="margin-top:0px;float:left;width:100%;">
                        <input type="hidden" name="project_id" value="<?php echo $project_id;?>" />
                        <input type="hidden" name="modification" value="1" />
                        <textarea name="comment" id="webmaster_comment"></textarea>
                        <div class="action_buttons">
                            <input type="submit" class="button color big round" id="modify_send_btn" value="Envoyer" />
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
       </div>
    </div>
</div>
<?PHP
include("files/includes/bottomBas.php");
