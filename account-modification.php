<?PHP


@session_start();
if (isset($_SERVER['HTTP_REFERER'])) {
    $redirectURL = str_replace('?statut=error', '', $_SERVER['HTTP_REFERER']);
    $redirectURL = str_replace('&statut=error', '', $redirectURL);
    $redirectURL = str_replace('&statut=success', '', $redirectURL);
    $redirectURL = str_replace('?statut=success', '', $redirectURL);
}
$special = 0;
$specialMessage = "";

$a = 1;
$b = 0;

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}


$page = 1;
$pageTilte = "modif";
include('files/includes/topHaut.php');

// user
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Projects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';

$_SESSION['alertLogincontent'] = "";
$_SESSION['tamponForm'] = "";


if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}


if (isset($_GET['block']) && in_array($_GET['block'], array('password', 'notifications', 'profil', 'parametres', 'actiongroupee', 'affectationgroupee', 'redaction_affectationgroupee', 'tacheeffectuee'))) {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $_GET['id'] = isset($_GET['id']) ? intval($_GET['id']) : $idUser;
    } else {
        $_GET['id'] = $idUser;
    }
}


if (!isset($_POST['contratReponse'])) {
    if (!isset($_GET['id']) || empty($_GET['id'])) {
        header('location:' . $_SERVER['HTTP_REFERER']);
    } else {
        $_GET['id'] = intval($_GET['id']);
    }
}


if ($_GET['block'] == "site") {


    if (isSU() || isAdmin()) {
        $result = Functions::getCommande($_GET['id'], 0, 1);
    } else {
        $result = Functions::getCommande($_GET['id'], $idUser);
    }


    /*if (empty($_POST['site']) ) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer l'url du ou des site(s) � r�f�rencer<br/>";
    } else {

        if (isMultiple($_POST['site']) && $result['envoyer'] == 0) {

            $listURL = explode(PHP_EOL, $_POST['site']);
            $error = "";
            foreach ($listURL as $value) {
                $value = trim($value);
                if ($value != "" && !Functions::checkUrlFormat($value)) {
                    $error = "Le format d'une des URL entr�es est incorrecte<br/>";
                }
            }
            if ($error != "") {
                $_SESSION['alertLogincontent'] .= $error;
            }
        } else {
            $_POST['site'] = trim($_POST['site']);
            if (!Functions::checkUrlFormat($_POST['site'])) {
                $_SESSION['alertLogincontent'] .= "Le format de l'URL est incorrecte<br/>";
            }
        }
    } */

    //
    //    if (empty($_POST['email']) || Functions::checkEmailFormat($_POST['email']) == false) {
    //        $_SESSION['alertLogincontent'] .= "Format de l'email Incorrect<br/>";
    //    }
    //    if ($result['envoyer'] == 0 || ($result['envoyer'] == 1 && (isAdmin() || isSU()))) {
    //        if (empty($_POST['budget'])) {
    //            $_SESSION['alertLogincontent'] .= "Entrez un budget pour ce projet.<br/>";
    //        }
    //    } else {
    //        $_POST['budget'] = $result['budget'];
    //    }
    //    if (empty($_POST['categorie'])) {
    //        $_SESSION['alertLogincontent'] .= "Choisissez une cat�gorie pour votre site<br/>";
    //    }

    if ((isWebmaster() && $result['proprietaire'] == $idUser)) {
        if (empty($_POST['annuaire'])) {
            $_SESSION['alertLogincontent'] .= "S�lectionnez la liste d'annuaires � utiliser<br/>";
        }
    } else {
        $_POST['annuaire'] = $result['annuaire'];
    }

    if (empty($_POST['annuaireFrequence']) || $_POST['annuaireFrequence'] <= 0 || empty($_POST['jourFrequence']) || $_POST['jourFrequence'] <= 0) {
        $_SESSION['alertLogincontent'] .= "Choissisez la fr�quence de soumission de votre site.<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {
        $frequence = "1";

        $frequence = intval($_POST['annuaireFrequence']) . "x" . intval($_POST['jourFrequence']);

        // check changing frequence
        $current_frequence = $result['frequence'];

        $frequence_changed = false;
        if ($frequence !== $current_frequence){
            $frequence_changed = true;
        }

        /*
            NOTE:
            lien = ' . $dbh->quote($_POST['site']) . ', REMOVED
        */

        // if frequence has been changed
        if ($frequence_changed){
           // $sql.= ', is_reaffected = 1, reaffected_time = ' . $dbh->quote(time()) . ',  adminApprouveTime = ' . $dbh->quote(time()) . ', affectedTime = ' . $dbh->quote(time()) ;

            if (isAdmin() || isSu()) {
                $dbh->query('UPDATE projets SET annuaire = ' . $dbh->quote($_POST['annuaire']) . ',frequence = ' . $dbh->quote($frequence) . ',consignes = ' . $dbh->quote(($_POST['commentaire'])) . ',
                             is_reaffected = 1, reaffected_time = ' . $dbh->quote(time()) . ',  adminApprouveTime = ' . $dbh->quote(time()) . ', affectedTime = ' . $dbh->quote(time()).'
                             WHERE id=' . $dbh->quote(intval($_GET['id'])));
            } else if (isWebmaster($idTypeUser)) {
                $dbh->query('UPDATE projets SET annuaire = ' . $dbh->quote($_POST['annuaire']) . ',frequence = ' . $dbh->quote($frequence) . ',consignes = ' . $dbh->quote(($_POST['commentaire'])) . ',
                             is_reaffected = 1, reaffected_time = ' . $dbh->quote(time()) . ',  adminApprouveTime = ' . $dbh->quote(time()) . ', affectedTime = ' . $dbh->quote(time()).'
                             WHERE id=' . $dbh->quote(intval($_GET['id'])) . ' AND proprietaire =' . $dbh->quote($idUser));
            }
            //var_dump('CHANGED'); die;

        }else{


            if (isAdmin() || isSu()) {
                $dbh->query('UPDATE projets SET annuaire = ' . $dbh->quote($_POST['annuaire']) . ',frequence = ' . $dbh->quote($frequence) . ',consignes = ' . $dbh->quote(($_POST['commentaire'])) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
            } else if (isWebmaster($idTypeUser)) {
                $dbh->query('UPDATE projets SET annuaire = ' . $dbh->quote($_POST['annuaire']) . ',frequence = ' . $dbh->quote($frequence) . ',consignes = ' . $dbh->quote(($_POST['commentaire'])) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])) . ' AND proprietaire =' . $dbh->quote($idUser));
            }

        }


        if ($frequence != $result['frequence'] && $result['affectedTO'] != 0 && $result['adminApprouve'] == 1) {
//            header("location:./ancres.html?id=" . $_GET['id']);
        }
//        if (isWebmaster()) {
        header("location:../ancres.html?id=" . $_GET['id']);
//        }
    }
}

if ($_GET['block'] == "ancres") {
    $action = "";
    if (isWebmaster() || isSu() || isAdmin()) {

        // check
        $multiple_project = false;
        // multiple_domain_project
        if (isset($_POST['multiple_domains_ancres'])){
            $multiple_project = true;
            $data = $_POST['multiple_domains_ancres'];
            $project_id = intval($_POST['project_id']);

            // check if exists temp ancres-data
            $result = $dbh->query('SELECT * FROM ancres_projects_temp WHERE project_id='.$dbh->quote($project_id));
            $row = $result->fetch();


            if ($row){
                //var_dump('updating');
                $stmt = $dbh->prepare('UPDATE ancres_projects_temp
                                       SET json=:json
                                       WHERE project_id=:project_id');
                $json = $_POST['multiple_domains_ancres'];

                $stmt->bindParam(':project_id',$project_id);
                $stmt->bindParam(':json',$_POST['multiple_domains_ancres']);
                $status = $stmt->execute();
            }else{
                //var_dump('inseting');
                $stmt = $dbh->prepare('INSERT INTO ancres_projects_temp
                                   VALUES(:project_id, :json)');

                $stmt->bindParam(':project_id',$project_id);
                $stmt->bindParam(':json',$_POST['multiple_domains_ancres']);
                $status = $stmt->execute();
            }
        }

        // single domain project
        if (!$multiple_project) {
            foreach ($_POST as $key => $value) {
                $keys = explode("_", $key);
                if (!empty($value) || $value != NULL) {
                    $value = addslashes($value);
                    $p_id = $keys[0];
                    $mod = false;
                    if (Functions::getAncre($keys[0], $keys[1])) {
                        $mod = $dbh->query('UPDATE ancres SET ancre = ' . $dbh->quote($value) . ', updated = ' . $dbh->quote(time()) . ' WHERE projetID=' . $dbh->quote(intval($keys[0])) . ' AND annuaireID=' . $dbh->quote(intval($keys[1])) . ' AND webmasterID =' . $dbh->quote(intval($keys[2])) . '');
                    } else {
//                    $action = "&action=emailpro";
                        $stmt = $dbh->prepare('INSERT INTO ancres
                                   VALUES(null, :projet, :annuaire, :ancre, :webmasterID, :updated, :created)');
                        $stmt->bindParam(':projet', $keys[0]);
                        $stmt->bindParam(':annuaire', $keys[1]);
                        $stmt->bindParam(':ancre', $value);
                        $stmt->bindParam(':webmasterID', $keys[2]);
                        $stmt->bindParam(':updated', time());
                        $stmt->bindParam(':created', time());
                        $status = $stmt->execute();
                    }
                }
            }
        }


    }
    header("location:../emailpro.html?id=" . $_GET['id'] . $action);
    exit;
}

if ($_GET['block'] == "emailpro") {

    $_GET['id'] = isset($_GET['id']) ? intval($_GET['id']) : 0;

    if (isWebmaster() || isSu() || isAdmin() && $_GET['id'] > 0) {
        if (isset($_POST['poss']) && intval($_POST['poss']) == 1) {
            $_SESSION['tampon']['poss'] = intval($_POST['poss']);
            $_SESSION['tampon']['url'] = $_POST['url'];
            $_SESSION['tampon']['username'] = $_POST['username'];
            $_SESSION['tampon']['password'] = $_POST['password'];
            $_SESSION['tampon']['annuaires'] = $_POST['annuaires'];

            $email = $_POST['username'];
            if (strpos($email,'@gmail.com')!== false ){
                $_SESSION['alertLogincontent'] .= "Merci de s�l�ctionner une adresse Email autre que Gmail. Nous ne pouvons pas nous y connecter en raison d'une s�curit�!<br/>";
            }

            if (empty($_POST['url'])) {
                $_SESSION['alertLogincontent'] .= "Veuillez saisir une URL<br/>";
            }
            if (empty($_POST['username'])) {
                $_SESSION['alertLogincontent'] .= "Veuillez saisir un identifiant ou une adresse email<br/>";
            }
            if (empty($_POST['password'])) {
                $_SESSION['alertLogincontent'] .= "Veuillez saisir un mot de passe<br/>";
            }
            if (count($_POST['annuaires']) == 0) {
                $_POST['annuaires'] = array();
                $_SESSION['alertLogincontent'] .= "Veuillez s�lectionner au moins un annuaire.<br/>";
            }
        }

        if ($_SESSION['alertLogincontent'] == "") {

            $costWebmaster = isset($_POST['costWebmaster']) ? floatval($_POST['costWebmaster']) : $ComptePersoWebmaster;
            $costReferenceur = isset($_POST['costReferenceur']) ? floatval($_POST['costReferenceur']) : $ComptePersoReferenceur;
            $active = isset($_POST['poss']) ? intval($_POST['poss']) : 0;
//            echo $_GET['id'];

            if (!Functions::getPersoWebmasterCompte($_GET['id'])) {

                $user_id = isset($_POST['user_id']) ? intval($_POST['user_id']) : $idUser;

                if (isset($_POST['poss']) && intval($_POST['poss']) == 1 && $_GET['id'] > 0) {
                    $stmt = $dbh->prepare('INSERT INTO emailsforprojects
                                   VALUES("", :project_id, :user_id, :url, :username, :password, :annuaires, :costWebmaster, :costReferenceur, :active, :updated, :time)');

                    $stmt->bindParam(':project_id', $_GET['id']);
                    $stmt->bindParam(':user_id', $user_id);
                    $stmt->bindParam(':url', $_POST['url']);
                    $stmt->bindParam(':username', $_POST['username']);
                    $stmt->bindParam(':password', $_POST['password']);
                    $stmt->bindParam(':annuaires', implode(';', $_POST['annuaires']));
                    $stmt->bindParam(':costWebmaster', $costWebmaster);
                    $stmt->bindParam(':costReferenceur', $costReferenceur);
                    $stmt->bindParam(':active', $active);
                    $stmt->bindParam(':updated', time());
                    $stmt->bindParam(':time', time());
                    $status = $stmt->execute();
                    if ($status) {
                        $query = $dbh->query("SELECT id FROM projets WHERE proprietaire = " . $dbh->quote($user_id) . " AND id = " . $dbh->quote($_GET['id']) . " AND adminApprouve = 0 AND envoyer = 0 AND over = 0")->fetch();
                        if ($query['id'] == $_GET['id']) {
                            header("location:../ajouter/emailpro.html?id=" . $_GET['id']);
                        }
                    }
                } else {
                    
                }
            } else {
                if (isset($_POST['poss']) && intval($_POST['poss']) == 1 && $_GET['id'] > 0) {
                    $requteFinale = 'UPDATE emailsforprojects '
                            . 'SET url = ' . $dbh->quote($_POST['url']) . ', '
                            . 'username = ' . $dbh->quote($_POST['username']) . ', '
                            . 'password = ' . $dbh->quote($_POST['password']) . ', '
                            . 'annuaires = ' . $dbh->quote(implode(';', $_POST['annuaires'])) . ', '
                            . 'costWebmaster = ' . $dbh->quote($costWebmaster) . ', '
                            . 'costReferenceur = ' . $dbh->quote($costReferenceur) . ', '
                            . 'active = ' . $dbh->quote($active) . ', '
                            . 'updated = ' . $dbh->quote(time()) . ' ';
                } else {
                    $requteFinale = 'UPDATE emailsforprojects SET '
                            . 'active = "' . $active . '", '
                            . 'updated = "' . time() . '" ';
                }
                if (isSu()) {
                    $requteFinale .= ', user_id = ' . $dbh->quote($_POST['user_id']) . ' WHERE project_id = ' . $dbh->quote(intval($_GET['id']));
                }
                if (isWebmaster()) {
                    $requteFinale .= 'WHERE project_id = ' . $dbh->quote(intval($_GET['id'])) . ' AND user_id = ' . $dbh->quote($idUser);
                }
//                echo $requteFinale;
                $mod = $dbh->query($requteFinale);
            }
            unset($_SESSION['tampon']);
        }
    }
}



if ($_GET['block'] == "utilisateurenable") {

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $active = $dbh->query('UPDATE utilisateurs SET active = "1" WHERE id=' . $dbh->quote($_GET['id']));
            if ($active) {
                $requete = "SELECT * FROM utilisateurs WHERE id =" . $dbh->quote($_GET['id']);
                $execution = $dbh->query($requete);
                $retour = $execution->fetch();
                if ($retour && $retour['lastIP'] > 0 && $retour['lastIP'] != 111111) {
                    if (!Functions::getDoublon("affiliation_affilies", "parrain", $retour['lastIP'], "affilie", $retour['id'])) {
                        $tarifWebmaster = Functions::getRemunuerationAffiliation($retour['lastIP']);
                        $setupAffiliation = $dbh->query('INSERT INTO affiliation_affilies VALUES("", ' . $dbh->quote($retour['lastIP']) . ', ' . $dbh->quote($retour['id']) . ', ' . $dbh->quote($tarifWebmaster) . ', ' . $dbh->quote(date('m')) . ', ' . $dbh->quote(date('Y')) . ', ' . $dbh->quote(time()) . ')');
                        if ($setupAffiliation) {
                            $soldeWebmaster = Functions::getSolde($retour['lastIP']);
                            $newBalance = $tarifWebmaster + $soldeWebmaster;
                            $reSo = $dbh->query('UPDATE utilisateurs SET solde = ' . $dbh->quote($newBalance) . ' WHERE id=' . $dbh->quote($retour['lastIP']));
                        }
                    }
                }
            }
        }
    }
}


if ($_GET['block'] == "desactiverannuaire") {

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $active = $dbh->query('UPDATE annuaires SET active = "0" WHERE id=' . $dbh->quote(intval($_GET['id'])));
            if ($active) {
                $_SESSION['alertActioncontent'] = "Annuaire d�sactiv� avec succ�s.";
                header('location:./../annuaires.html?statut=success#annuaire' . intval($_GET['id']));
            }
        }
    }
}


if ($_GET['block'] == "activerannuaire") {

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $active = $dbh->query('UPDATE annuaires SET active = "1" WHERE id=' . $dbh->quote(intval($_GET['id'])));
            if ($active) {
                $_SESSION['alertActioncontent'] = "Annuaire activ� avec succ�s.";
                header('location:../../compte/annuaires.html?statut=success#annuaire' . intval($_GET['id']));
            }
        }
    }
}

if ($_GET['block'] == "solde") {


    if ($_POST['solde']==="") {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer le nouveau solde.<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $user = Functions::getUserInfos($_GET['id']);
            $_POST['solde'] = floatval($_POST['solde']);

            $transaction = new Transaction($dbh, $_GET['id']);
            if (($_POST['solde'] - $user['solde']) != 0) {
                $dbh->query('UPDATE utilisateurs SET solde = ' . $dbh->quote($_POST['solde']) . ', lastPayment = ' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($user['id']));
                @$dbh->query('INSERT INTO requestpayment (id,type,requesterID,requestAmount,answer,answerTime,time) VALUES("", ' . $dbh->quote($user['typeutilisateur']) . ', ' . $dbh->quote($user['id']) . ',  ' . $dbh->quote(floatval($difference)) . ', "1", ' . $dbh->quote(time()) . ', ' . $dbh->quote(time()) . ')');
                if ($_POST['solde'] > $user['solde']){
                    $diff = $_POST['solde'] - $user['solde'];
                    $transaction->addTransaction("Ajout de " . $diff . " euros à votre compte", $diff, 0);
                } else {
                    $diff = $user['solde'] - $_POST['solde'];
                    $transaction->addTransaction("Retrait de " . $diff . " euros à votre compte", 0, $diff);
                }
            }
        }
    }

    if ($_POST['credit']==="") {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer le nouveau credit.<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {

            $user = Functions::getUserInfos($_GET['id']);
            $_POST['credit'] = floatval($_POST['credit']);
            
            $transaction = new Transaction($dbh, $_GET['id']);
            if (($_POST['credit'] - $user['credit']) != 0) {
                $dbh->query('UPDATE utilisateurs SET credit = ' . $dbh->quote($_POST['credit']) . ' WHERE id=' . $dbh->quote($user['id']));
                if ($_POST['credit'] > $user['credit']){
                    $diff = $_POST['credit'] - $user['credit'];
                    $transaction->addTransaction("Ajout de " . $diff . " credit à votre compte", $diff, 0);
                } else {
                    $diff = $user['credit'] - $_POST['credit'];
                    $transaction->addTransaction("Retrait de " . $diff . " credit à votre compte", 0, $diff);
                }
            }
        }

    }



}

if ($_GET['block'] == "validerrequete") {
//    print_r($_SERVER['HTTP_REFERER']) ;

    $projectsModel = new Projects($dbh);

    @$request = Functions::getPaiementById($_GET['id']);
    $wbePayer = 0;

    if (isset($_GET['hash']) && Functions::personalEncodage($request['requestAmount']) == $_GET['hash']) {
        $wbePayer = 1;
    }
    if ($_SESSION['alertLogincontent'] == "" && isset($request['requesterID'])) {
        $userInfos = Functions::getUserInfos($request['requesterID']);
        if ((isSu($idTypeUser) || isAdmin($idTypeUser) || (isWebmaster() && $request['requesterID'] == $idUser && $wbePayer == 1 && (!isset($_SERVER['HTTP_REFERER']) || (isset($_SERVER['HTTP_REFERER']) && preg_match("#paypal.com#", $_SERVER['HTTP_REFERER'])))))) {
            $setUser = false;
            $witch = 0;
            if (isWebmaster($userInfos['typeutilisateur'])) {
                $newSolde = $userInfos['solde'] + $request['requestAmount'];
            } else {
                $newSolde = $userInfos['solde'] - $request['requestAmount'];
            }
            $setUserFirst = false;

            // get "paused" projects
            $paused_project_ids = $projectsModel->getPausedProjectsIds($userInfos['id']);

            $setUserFirst = $dbh->query('UPDATE utilisateurs SET solde=' . $dbh->quote($newSolde) . ', AmountLastPayment = ' . $dbh->quote($request['requestAmount']) . ', lastPayment =' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($request['requesterID']));

            $transaction = new Transaction($dbh, $request['requesterID']);
            $transaction->addTransaction("Ajout manuel de " . $request['requestAmount'] . " euros à votre compte", $request['requestAmount'], 0);

            // reset paused projects on balance increasing
            if (!empty($paused_project_ids)){
                $projectsModel->resetPausedProjects($paused_project_ids,$userInfos['id']);
            }

            if ($setUserFirst) {
                //$facture = $dbh->query('INSERT INTO factures (`id`,`user`,`amount`,`tva`,`reference_paypal`,`reference_ereferer`,`time`) VALUES ("","' . $userInfos['id'] . '","' . $request['requestAmount'] . '","' . $_SESSION['allParameters']['tva']["valeur"] . '","","ERF' . time() . '","' . time() . '")');
                $tva = $_SESSION['allParameters']['tva']["valeur"];

                // bugfix tva
                if ( (isSu($idTypeUser) || isAdmin($idTypeUser))){
                    $tva = $request['tva'];
                }

                $facture = $dbh->query('INSERT INTO factures (`id`,`user`,`amount`,`tva`,`reference_paypal`,`reference_ereferer`,`time`) VALUES ("",' . $dbh->quote($userInfos['id']) . ',' . $dbh->quote($request['requestAmount']) . ',' . $dbh->quote($tva) . ',"","ERF' . time() . '",' . $dbh->quote(time()) . ')');

                $setUser = $dbh->query('UPDATE requestpayment SET answer = "1", answerTime=' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($_GET['id']));
            }
            if ($setUser) {
                $name = $_SESSION['allParameters']['logotext']["valeur"];
                $urlSite = $_SESSION['allParameters']['url']["valeur"];
                $limitepaiement = $_SESSION['allParameters']['limitepaiement']["valeur"];
                $EmailSite = $_SESSION['allParameters']['email']["valeur"];

                @require_once('./files/includes/phpmailer/class.phpmailer.php');
                $body = '<br/>';
                $body = '';
                $body .= 'Cher(e) ' . $userInfos['nom'] . ' ' . $userInfos['prenom'] . ',<br/><br/>';
                if (isWebmaster($userInfos['typeutilisateur'])) {
                    $body .= "Votre compte a &eacute;t&eacute; recharger de " . $request['requestAmount'] . " Euro(s)<br/><br/>";

                    $mail->Subject = "Votre compte a &eacute;t&eacute; recharger de " . $request['requestAmount'] . " Euro(s)";
                } else {
                    $body .= "Votre compte a &eacute;t&eacute; d&eacute;biter de " . $request['requestAmount'] . " Dollar(s)<br/><br/>";
                    $body .= "Vous pourrez faire une autre demande de paiement dans " . $limitepaiement . " jour(s).<br/><br/>";

                    $mail->Subject = "Demande de paiement de " . $request['requestAmount'] . " Dollar(s) approuv&eacute;e";
                }

//                $body .= "Merci de faire confiance � " . $name . ", <br/>";
                $body .= "Bien Cordialement, <br/>";
                $body .= 'Emmanuel';

                $mail = new PHPMailer();
                $mail->Timeout = '30';
                $mail->CharSet = 'UTF-8';
                $mail->From = "noreply@ereferer.fr";
                $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
                $mail->AltBody = "";
                $mail->IsHTML(true);
                $mail->MsgHTML($body);
                $mail->AddAddress($userInfos['email'], "");
                $mail->AddAddress($EmailSite, "");
                $mail->Send();
            }
            if (isWebmaster()) {
                header('location:../../compte/paypal.html?statut=success');
            }
        }
    }
}

if ($_GET['block'] == "utilisateurdisable") {

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE utilisateurs SET active = "0" WHERE id=' . $dbh->quote($_GET['id']));
        }
    }
}

if ($_GET['block'] == "reprendreprojet") {
    $condition = 0;

    $projet = Functions::getCommandeByID(intval($_GET['id']));
    $currentAnnuaireList = Functions::getOneAnnuaireListe($projet['annuaire']);

    // checking access
    Functions::checkAccess(isWebmaster(),$idUser,$projet['proprietaire']);

    if ($projet['annuaire'] > 0 && !empty($projet['annuaire'])) {
        //$currentAnnuaireList = Functions::getOneAnnuaireListe($projet['annuaire']);

        if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
            $annuairesFromList = explode(";", $currentAnnuaireList['annuairesList']);
        } else {
            $annuairesFromList = array();
        }

        // bug-fix
        $annuairesFromList = array_filter($annuairesFromList);
        $done = Functions::getCountTaskDone($projet['id'],true);



//        $countDoned = count($done);
//        $fusion = array_merge($annuairesFromList, $done);
        $fusion = array_diff($annuairesFromList, $done);
        $fusion = array_filter($fusion);

        if (count($fusion) > 0) {
            $condition = 1;
        }
        // $condition = 0;
    }


    if ($_SESSION['alertLogincontent'] == "" && $condition == 1) {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE projets SET over = "0", from_pause = 1, is_reaffected = 1, reaffected_time = ' . $dbh->quote(time()) . ',  adminApprouveTime = ' . $dbh->quote(time()) . ', affectedTime = ' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($_GET['id']));
        }
        if (isWebmaster()) {
            $dbh->query('UPDATE projets SET over = "0", from_pause = 1, is_reaffected = 1, reaffected_time = ' . $dbh->quote(time()) . ',  adminApprouveTime = ' . $dbh->quote(time()) . ', affectedTime = ' . $dbh->quote(time()) . ' WHERE proprietaire = ' . $dbh->quote($idUser) . ' AND id=' . $dbh->quote($_GET['id']));
        }
    }


    if (!empty($projet) && ($condition == 0) ) {
        $list_name = $currentAnnuaireList['libelle'];
        $list_id =  $currentAnnuaireList['id'];
        $_SESSION['alertLogincontent'] .= 'Tous les annuaires li�s � votre liste <b>'.$list_name.'</b> ont �t� trait�s.
                                           Vous devez ajouter de nouveaux annuaires � cette liste pour pouvoir reprendre le projet.<br/> <a target="_blank" href="http://www.ereferer.fr/compte/modifier/listannuairecontent.html?id='.$list_id.'">http://www.ereferer.fr/compte/modifier/listannuairecontent.html?id='.$list_id.'</a><br/>';
        $_SESSION['alertLogincontent'] .= "<br/>Vous pouvez �galement changer la liste d'annuaires associ�e � votre projet : <br/> <a target=\"_blank\" href=\"http://www.ereferer.fr/compte/modifier/site.html?id=".$projet['id'].'">http://www.ereferer.fr/compte/modifier/site.html?id='.$projet['id'].'</a>';
    }

}

if ($_GET['block'] == "stopperprojet") {
    $getCommande = Functions::getCommandeByID(intval($_GET['id']));

    // checking access
    Functions::checkAccess(isWebmaster(),$idUser,$getCommande['proprietaire']);

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE projets SET over = "1", overTime=' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($_GET['id']));
        }
        if (isWebmaster()) {
            $dbh->query('UPDATE projets SET over = "1", overTime=' . $dbh->quote(time()) . ' WHERE proprietaire = ' . $dbh->quote($idUser) . ' AND id=' . $dbh->quote($_GET['id']));
        }
    }
}


if ($_GET['block'] == "accepterprojet") {

    $projet = Functions::getCommandeByID($_GET['id']);

    // checking access
    Functions::checkAccess(isWebmaster(),$idUser,$projet['proprietaire']);

//    $result = Functions::getUserInfos($projet['proprietaire']);
//    $newSolde = $result['solde'] - $projet['budget'];
//    if ($result['solde'] < $projet['budget']) {
//        $_SESSION['alertLogincontent'] .= "Le client n'a pas assez de fonds sur son compte.<br/>";
//    }
    if ($projet && $projet['proprietaire']) {
        $webmasterCan = Functions::isWebmasterCan(intval($projet['proprietaire']));
    } else {
        $webmasterCan = false;
    }


    if (!$webmasterCan) {
        $_SESSION['alertLogincontent'] = "Solde insuffissant pour d�marrer ce(s) projet(s). Pensez � recharger votre compte.<br/>";
    }

    // restrict access
    if (isWebmaster() && ($idUser != $projet['proprietaire'])){
       header('location:./../../logout.html'); die;
    }

    if ($_SESSION['alertLogincontent'] == "" && isset($projet['id']) && $webmasterCan) {
        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isWebmaster($idTypeUser)) {
            $setUser = false;

            if (isMultiple($projet['lien'])) {
                $count = 0;
                $projetParent = $projet['id'];
                $arrayLien = explode(PHP_EOL, $projet['lien']);


                //error_reporting(E_ALL);

                // get ancres
                $result = $dbh->query("SELECT json FROM ancres_projects_temp WHERE project_id=".$dbh->quote($projetParent));
                $data_json = $result->fetchColumn();
                $data = json_decode($data_json,true);
                $domains_ancres = array();
                foreach($data as $item){
                    $domain_url = $item['url'];
                    if (is_array($item['items']) && (count($item['items']))){
                        foreach($item['items'] as $el){
                            $domains_ancres[$domain_url][] = $el;
                        }
                    }
                }


                error_reporting(E_ERROR);

                foreach ($arrayLien as $valueUrl){
                     if ($valueUrl != "") {
//                        print_r($valueUrl);
                        if ($count == 0) {
                            $setUser = $dbh->query('UPDATE projets SET lien =' . $dbh->quote($valueUrl) . ', parent =' . $dbh->quote($projetParent) . ', envoyer = "1", sendTime = ' . $dbh->quote(time()) . ', adminApprouve = "1", adminApprouveTime = ' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($_GET['id']));

                            // inserting ancres
                            if (isset($domains_ancres[$valueUrl])){
                                $project_id = $_GET['id'];
                                $ancres = $domains_ancres[$valueUrl];

                                foreach($ancres as $ancre) {
                                    $ancre['project_id'] = $project_id;
                                    Functions::insertAncre($ancre);
                                }
                            }

                        } else {
                            if (!Functions::getDoublon("projets", "lien", $valueUrl, "proprietaire", $projet['proprietaire'])) {

                                $dbh->query('INSERT INTO projets VALUES("", ' . $dbh->quote($projetParent) . ', ' . $dbh->quote($projet['categorieID']) . ', ' . $dbh->quote($valueUrl) . ', "", ' . $dbh->quote($projet['proprietaire']) . ', ' . $dbh->quote($projet['frequence']) . ', "0", ' . $dbh->quote($projet['annuaire']) . ', ' . $dbh->quote(addslashes($projet['consignes'])) . ', ' . $dbh->quote(addslashes($projet['ancres'])) . ', "1", ' . $dbh->quote(time()) . ', "1",' . $dbh->quote(time()) . ',"","0","0","0","0", "0","1", "","0","","","","","","",0)');

                                $project_id = $dbh->lastInsertId();
                                //var_dump($project_id);

                                // inserting ancres
                                if (isset($domains_ancres[$valueUrl])){
                                    $ancres = $domains_ancres[$valueUrl];
                                    foreach($ancres as $ancre) {
                                        $ancre['project_id'] = $project_id;
                                        Functions::insertAncre($ancre);
                                    }
                                }

                            }
                        }
                        $count++;
                    }
                }
                //die;
//                $dbh->query('UPDATE utilisateurs SET solde = "' . $newSolde . '" WHERE id=' . $projet['proprietaire']);
            } else {
                $setUser = $dbh->query('UPDATE projets SET envoyer = "1", sendTime = ' . $dbh->quote(time()) . ', adminApprouve = "1", adminApprouveTime = ' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote($_GET['id']));
//                $dbh->query('UPDATE utilisateurs SET solde = "' . $newSolde . '" WHERE id=' . $projet['proprietaire']);
            }

            if ($setUser) {
//                $userInfos = Functions::getUserInfos($_SESSION['connected']['id']);
                $name = $_SESSION['allParameters']['logotext']["valeur"];
                $urlSite = $_SESSION['allParameters']['url']["valeur"];
                $EmailSite = $_SESSION['allParameters']['email']["valeur"];

                @require_once('./files/includes/phpmailer/class.phpmailer.php');
                $body = '<br/>';
                $body .= 'Veuillez vous connecter pour affecter des r&eacute;f&eacute;renceur &agrave; ces projets svp.<br/><br/>';

                $mail = new PHPMailer();
                $mail->Timeout = '30';
                $mail->CharSet = 'UTF-8';
                $mail->From = "noreply@ereferer.fr";
                $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
                $mail->Subject = $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom'] . " a un ou des projet(s) en attente.";
                $mail->AltBody = "";
                $mail->IsHTML(true);
                $mail->MsgHTML($body);
                $mail->AddAddress($EmailSite, "");
                $mail->Send();
            }
        }
    }
}


// user notifications
if ($_GET['block'] == "notifications") {
    $userModel = new User($idUser);
    $user_settings = $_POST['user_settings'];
    $userModel->setParam('settings',$user_settings);
}

if ($_GET['block'] == "rejeterprojet") {

    if (empty($_POST['raison'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer les raisons du rejet. Soyez bref<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE projets SET adminApprouve = 2,adminApprouveTime=' . $dbh->quote(time()) . ', adminRaison =' . $dbh->quote($_POST['raison']) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
        }
    }
}

if ($_GET['block'] == "annuaire") {
    if (empty($_POST['annuaire'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir l'URL de l'annuaire<br/>";
    } else {
        if (!Functions::checkUrlFormat($_POST['annuaire'])) {
            $_SESSION['alertLogincontent'] .= "L'url de l'annuaire n'est pas valide.<br/>";
        }
    }

    //error_reporting(E_ALL);

    if ($_SESSION['alertLogincontent'] == "") {
        $retour = 1;
        if (isset($_POST['retour']) && $_POST['retour'] == 0) {
            $retour = 0;
        }

// <parsing>
        $consignes = !empty($_POST['consignes']) ? $_POST['consignes'] : '';
        $nddcible = !empty($_POST['nddcible']) ? $_POST['nddcible'] : '';
        $page_count = !empty($_POST['page_count']) ? intval($_POST['page_count']) : 100;
        $page_rank = !empty($_POST['page_rank']) ? intval($_POST['page_rank']) : 0;
        $display = !empty($_POST['ancre']) ? intval($_POST['ancre']) : 0;
        $displayAncre = !empty($_POST['ancre']) ? intval($_POST['ancre']) : 0;
        $ComptePersoWebmaster = !empty($_POST['poss']) ? intval($_POST['poss']) : 0;
        $consignesAncre = !empty($_POST['consignesAncre']) ? $_POST['consignesAncre'] : "";

        $webmasterPartenaire = !empty($_POST['webmasterPartenaire']) ? intval($_POST['webmasterPartenaire']) : 0;
        $WebPartenairePrice = !empty($_POST['WebPartenairePrice']) ? floatval($_POST['WebPartenairePrice']) : 0;

        $vip_text = !empty($_POST['vip_text'])? $_POST['vip_text'] : '';



        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {

            $dbh->query('UPDATE annuaires SET 
                         annuaire = ' . $dbh->quote($_POST['annuaire']) . ',
                         page_rank = ' . $dbh->quote($page_rank) . ',
                         tarifW = ' . $dbh->quote(floatval($_POST['tarifW'])) . ',
                         tarifR = ' . $dbh->quote(floatval($_POST['tarifR'])) . ',
                         active = ' . $dbh->quote($retour) . ',
                         display = ' . $dbh->quote($display) . ',
                         webmasterAncre = ' . $dbh->quote($displayAncre) . ',
                         webmasterConsigne = ' . $dbh->quote($consignesAncre) . ',
                         webmasterPartenaire = ' . $dbh->quote($webmasterPartenaire) . ',
                         WebPartenairePrice = ' . $dbh->quote($WebPartenairePrice) . ',
                         ComptePersoWebmaster = ' . $dbh->quote($ComptePersoWebmaster) . ',
                         consignes = ' . $dbh->quote($consignes) . ',
                         nddcible = ' . $dbh->quote($nddcible) . ',
                         page_count = ' . $dbh->quote($page_count) . ',
                         cf = ' . $dbh->quote(intval($_POST['cf'])) . ',
                         tf = ' . $dbh->quote(intval($_POST['tf'])) . ',
                         cp = ' . $dbh->quote(intval($_POST['cp'])) . ',
                         age = ' . $dbh->quote(strtotime($_POST['age'])) . ',
                         tdr = ' . $dbh->quote(intval($_POST['tdr'])) . ',
                         tb = ' . $dbh->quote(intval($_POST['tb'])) . ',
                         accept_inner_pages = ' . $dbh->quote(intval($_POST['accept_inner_pages'])) . ',
                         accept_legal_info = ' . $dbh->quote(intval($_POST['accept_legal_info'])) . ',
                         accept_company_websites = ' . $dbh->quote(intval($_POST['accept_company_websites'])) . ',
                         link_submission = "'.$_POST['link_submission'].'",
                         vip_state = "'.intval($_POST['vip_state']).'",
                         vip_text = "'.addslashes($vip_text).'",
                         min_words_count = ' . $dbh->quote(intval($_POST['min_words_count'])) . ',
                         max_words_count = ' . $dbh->quote(intval($_POST['max_words_count'])) . '
                         WHERE id=' . $dbh->quote(intval($_GET['id'])));
        } else {
            if (isWebmaster($idTypeUser)) {
//                $dbh->query('UPDATE annuaires SET annuaire = "' . $_POST['annuaire'] . '", active = "' . $retour . '", display = "' . $display . '", consignes = "' . addslashes($consignes) . '" WHERE importedBy ="' . $idUser . '" AND id=' . intval($_GET['id']));
            }
        }

        $annuairesListes = Functions::getAnnuaireListeList();
//        print_r($annuairesListes);
        foreach ($annuairesListes as $lineAnu) {
            $value = intval($lineAnu['id']);
            $annuaire = $lineAnu['annuairesList'];
            $annuairesFromList = explode(";", $lineAnu['annuairesList']);
            $annuairesFromListNew = $annuairesFromList;
            $annuairesFromListNew = array_filter($annuairesFromListNew);

//            print_r($_POST['repertoire']);
            if (isset($_POST['repertoire']) && in_array(intval($lineAnu['id']), $_POST['repertoire'])) {
//                echo "6";
                if (!in_array(intval($_GET['id']), $annuairesFromListNew) && intval($_GET['id']) != 0) {
                    $annuaire = $lineAnu['annuairesList'] . intval($_GET['id']) . ";";
                }
            } else {
                if (in_array(intval($_GET['id']), $annuairesFromListNew) && intval($_GET['id']) != 0) {
                    unset($annuairesFromListNew[array_search(intval($_GET['id']), $annuairesFromListNew)]);
                    $annuaire = implode(";", $annuairesFromListNew);
                    $annuaire .= ";";
                }
            }
            $dbh->query('UPDATE annuaireslist SET annuairesList = ' . $dbh->quote($annuaire) . ' WHERE id =' . $lineAnu['id']);
        }

//        echo $_POST['redir'];
        if (!empty($_POST['redir']) && !Functions::checkUrlFormat2($_POST['redir'])) {
            header('location: ' . $_POST['redir']);
        }
    }
}

if ($_GET['block'] == "commentaire") {
    if (!empty($_POST['redir'])) {
        $_POST['redir'] = urldecode($_POST['redir']);
    }

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $added = $dbh->query('UPDATE commentaires SET com = ' . $dbh->quote(addslashes($_POST['commentaire'])) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
        }
        if (isWebmaster() || (isReferer() && !isSuperReferer())) {
            $added = $dbh->query('UPDATE commentaires SET com = ' . $dbh->quote(addslashes($_POST['commentaire'])) . ', user=' . $dbh->quote($idUser) . ' WHERE typeuser =' . $dbh->quote($idTypeUser) . ' AND id=' . $dbh->quote(intval($_GET['id'])));
        }
        if (isSuperReferer()) {
            $added = $dbh->query('UPDATE commentaires SET com = ' . $dbh->quote(addslashes($_POST['commentaire'])) . ' WHERE typeuser =' . $dbh->quote($idTypeUser) . ' AND id=' . $dbh->quote(intval($_GET['id'])));
        }
        if (!empty($_POST['redir']) || !$added !== true) {
            header('location: ' . $_POST['redir']);
        }
    }
}

if ($_GET['block'] == "listannuaire") {

    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $who = 0;
    } else {
        $who = $idUser;
    }
    if (empty($_POST['listannuaire'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir le nom de la liste � cr��.<br/>";
    } else {
        if (Functions::getDoublon("annuaireslist", "libelle", $_POST['listannuaire'], "proprietaire", $who)) {
            $_SESSION['alertLogincontent'] .= "Vous avez d�ja une liste du m�me nom.<br/>";
        }
    }

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE annuaireslist SET libelle = ' . $dbh->quote($_POST['listannuaire']) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
        }
        if (isWebmaster()) {
            $dbh->query('UPDATE annuaireslist SET libelle = ' . $dbh->quote($_POST['listannuaire']) . ' WHERE proprietaire = ' . $dbh->quote($who) . ' AND id=' . $dbh->quote(intval($_GET['id'])));
        }
    }
}

if ($_GET['block'] == "listannuairecontent") {


//    print_r($_POST);
    $listSelected = "";
    if ($_SESSION['alertLogincontent'] == "") {

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        } else {
            $who = $idUser;
        }
        if (!empty($_POST['check'])) {
            $tamponArray = array();
            foreach ($_POST['check'] as $Key => $value) {
                if (!in_array($value, $tamponArray) && $value != "") {
                    $tamponArray[] = $value;
                    $listSelected .= $value . ";";
                }
            }
        } else {
            $listSelected = "";
        }
        if (isset($_POST['action'])) {
//$_POST['filtrageavance'] = 1; // always true

            // Words count for user`s annuaire`s list
            $min_words_count = !empty($_POST['min_words_count']) ? $_POST['min_words_count'] : 0;


            if ($_POST['action'] == "Filtrer" && isset($_POST['cp']) && isset($_POST['liste']) && (isset($_POST['otherData']) || isset($_POST['advancedSearch'])) && isset($_POST['filtrageavance'])) {
                $filtre_infos = "";
                $filtre_infos = $listSelected . "|" . $_POST['cp'] . "|" . intval($_POST['liste']) . "|" . $_POST['otherData'] . "|" . intval($_POST['filtrageavance']);
                if ($_POST['filtrageavance'] == 1) {
                    if (isset($_POST['advancedSearch'])) {
                        $filtre_infos .= "|" . implode(";", $_POST['advancedSearch']);
                    }
                }

                // checkboxes-filters
                $checkboxes_filters = array();
                if (isset($_POST['accept_inner_pages'])){
                    $checkboxes_filters[] = 'accept_inner_pages=1';
                }
                if (isset($_POST['accept_legal_info'])){
                    $checkboxes_filters[] = 'accept_legal_info=1';
                }
                if (isset($_POST['accept_company_websites'])){
                    $checkboxes_filters[] = 'accept_company_websites=1';
                }

                if (isset($_POST['is_member_account'])){
                    $checkboxes_filters[] = 'is_member_account=1';
                }

                if (isset($_POST['min_words_count']) && !empty($_POST['min_words_count'])){
                    $checkboxes_filters[] = 'min_words_count='.intval($_POST['min_words_count']);
                }


                $checkboxes_query = '';
                if (!empty($checkboxes_filters)){
                    $checkboxes_query = implode('&',$checkboxes_filters);
                }


                // prepare filter for saving
                $filter_config = 'filtrageavance=' . $_POST['filtrageavance'] . '&filtre=' . base64_encode($filtre_infos);

                // add checkboxes-filters for filter-query
                if (!empty($checkboxes_query)){
                    $filter_config .='&'.$checkboxes_query;
                }

                if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                    /*
                    $dbh->query('UPDATE annuaireslist
                                 SET annuairesList = ' . $dbh->quote($listSelected) . ',
                                     words_count = '.$min_words_count.'
                                 WHERE id=' . $dbh->quote(intval($_GET['id'])));*/

                    $stmt = $dbh->prepare('UPDATE annuaireslist
                                           SET    annuairesList = :list,
                                                  filter_config = :filter_config
                                           WHERE  id=:id');

                    $stmt->bindParam(':list',$listSelected);
                    $stmt->bindParam(':filter_config',$filter_config);
                    $stmt->bindParam(':id',intval($_GET['id']));
                    $status = $stmt->execute();
                }

                if (isWebmaster()) {
                    // @deprecated
                    //$dbh->query('UPDATE annuaireslist SET annuairesList = "' . $listSelected . '" WHERE proprietaire = "' . $who . '" AND id=' . intval($_GET['id']));

                    $stmt = $dbh->prepare('UPDATE annuaireslist
                                           SET    annuairesList = :list,
                                                  filter_config = :filter_config
                                           WHERE  proprietaire = :who AND id=:id');

                    $stmt->bindParam(':list',$listSelected);
                    $stmt->bindParam(':who',$who);
                    $stmt->bindParam(':filter_config',$filter_config);
                    $stmt->bindParam(':id',intval($_GET['id']));

                    $status = $stmt->execute();

                }

                $redirect_url = 'location:./../modifier/listannuairecontent.html?id=' . intval($_GET['id']) . '&filtrageavance=' . $_POST['filtrageavance'] . '&filtre=' . base64_encode($filtre_infos);

                // add checkboxes-filters url
                if (!empty($checkboxes_query)){
                    $redirect_url.='&'.$checkboxes_query;
                }
                header($redirect_url);
            } else {


                if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                    $dbh->query('UPDATE annuaireslist SET annuairesList = ' . $dbh->quote($listSelected) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
                }

                if (isWebmaster()) {

                    //var_dump($listSelected);
                    //die;


                    // @deprecated
                    //$dbh->query('UPDATE annuaireslist SET annuairesList = "' . $listSelected . '" WHERE proprietaire = "' . $who . '" AND id=' . intval($_GET['id']));

                    $stmt = $dbh->prepare('UPDATE annuaireslist
                                           SET annuairesList = :list,
                                               words_count = :words_count
                                           WHERE proprietaire = :who AND id=:id');

                    $stmt->bindParam(':list',$listSelected);
                    $stmt->bindParam(':who',$who);
                    $stmt->bindParam(':words_count',$min_words_count);
                    $stmt->bindParam(':id',intval($_GET['id']));

                    $status = $stmt->execute();
                }
            }
        }
    }
}

if ($_GET['block'] == "dupliquerannuairecontent") {
    $listSelected = "";
    if ($_SESSION['alertLogincontent'] == "") {

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        } else {
            $who = $idUser;
        }

        if (!empty($_POST['check'])) {
            $tamponArray = array();
            foreach ($_POST['check'] as $Key => $value) {
                if (!in_array($value, $tamponArray)) {
                    $tamponArray[] = $value;
                    $listSelected .= $value . ";";
                }
            }
        } else {
            $listSelected = "";
        }

        //error_reporting(E_ERROR);

        $getThisList = Functions::getOneAnnuaireListe($_GET['id']);

        // check accessing
        if (isWebmaster() && ($idUser != $getThisList['proprietaire']) ){
            Functions::redirectJS('/');
        }

        if ((isSu($idTypeUser) || isAdmin($idTypeUser)) && $getThisList) {
            $query = 'INSERT INTO annuaireslist  VALUES(null, ' . $dbh->quote($getThisList['libelle'] . " (Copie)").', ' . $who . ', '.$dbh->quote($getThisList['annuairesList']) . ', '. $dbh->quote(time()) .',0,'.$dbh->quote($getThisList['words_count']) .','.$dbh->quote($getThisList['filter_config']).',0)';
            $dbh->query($query);
        }

        if (isWebmaster() && $getThisList) {
            $query = 'INSERT INTO annuaireslist  VALUES(null, ' . $dbh->quote($getThisList['libelle'] . " (Copie)").', ' . $who . ', ' . $dbh->quote($getThisList['annuairesList']) . ', ' . $dbh->quote(time()) . ',0,'.$dbh->quote($getThisList['words_count']) .','.$dbh->quote($getThisList['filter_config']).',0)';
            $dbh->query($query);
        }
    }
}

if ($_GET['block'] == "contrat") {

    if (empty($_POST['contratReponse']) || $_POST['contratReponse'] != 1) {
        $_SESSION['alertLogincontent'] .= "Veuillez accepter le contact pour continuer.<br/>";
    }

    $is_redaction_module = isset($_POST['redaction_module']);

    if ($_SESSION['alertLogincontent'] == "") {
        if ($is_redaction_module){
            $contract = $dbh->query('UPDATE utilisateurs SET redaction_contract_accepted = "' . intval($_POST['contratReponse']) . '" WHERE id=' . $idUser);
            $_SESSION['connected']["redaction_contract_accepted"] = 1;
            $redirectURL = "./compte/redactionlisting/progress.html";

        }else{
            $contract = $dbh->query('UPDATE utilisateurs SET contractAccepted = "' . intval($_POST['contratReponse']) . '" WHERE id=' . $idUser);
            $_SESSION['connected']["contractAccepted"] = 1;
            $redirectURL = "./compte/accueil.html";
        }

    }


}

if ($_GET['block'] == "modifiercontrat") {

    if (empty($_POST['typeUser'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez selectionner le groupe auquel appliquer ce contrat.<br/>";
    }
    if (empty($_POST['content'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir le contenu du contrat.<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $sql = 'UPDATE contrats SET content = "' . $dbh->quote($_POST['content']).'"  WHERE typeUser=' . $dbh->quote(intval($_POST['typeUser']));
            if (!empty($_POST['module'])){
                $sql .=' AND module='.$dbh->quote($_POST['module']);
            }else{
                $sql .=' AND module IS NULL ';
            }

            //var_dump($sql); die;

            $dbh->query($sql);
        }
    }
}


if ($_GET['block'] == "message") {
    
}

if ($_GET['block'] == "password") {

    if (isWebmaster() || isReferer() || ((isSu() || isAdmin()) && $idMod == $idUser)) {
        if (empty($_POST['oldpass']) || Functions::getCryptePassword($_POST['oldpass']) != $_SESSION['connected']['password']) {
            $_SESSION['alertLogincontent'] .= "Veuillez saisir votre ancien mot de passe correctement<br/>";
        }
    }

    if (empty($_POST['newpass']) || empty($_POST['confnewpass'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer le nouveau mot de passe.<br/>";
    } else {
        if ($_POST['newpass'] != $_POST['confnewpass']) {
            $_SESSION['alertLogincontent'] .= "Les nouveaux mots de passe ne concordent pas.<br/>";
        }
    }

    if ($_SESSION['alertLogincontent'] == "") {
        $_POST['newpass'] = addslashes($_POST['newpass']);
        if ((isSu($idTypeUser) || isAdmin($idTypeUser)) && intval($_GET['id']) != $idUser) {
            $userpass = $dbh->query('UPDATE utilisateurs SET password = ' . $dbh->quote(Functions::getCryptePassword($_POST['newpass'])) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
        } else {
            $userpass = $dbh->query('UPDATE utilisateurs SET password = ' . $dbh->quote(Functions::getCryptePassword($_POST['newpass'])) . ' WHERE id=' . $dbh->quote($idUser));
        }
    }
}

if ($_GET['block'] == "soumettresite") {

    if ($projet['budget'] > $solde) {
        $_SESSION['alertLogincontent'] .= "Vous n'avez pas assez de fonds dans votre compte pour proposer ce projet.<br/>";
    }
    if ($_SESSION['alertLogincontent'] == "") {
        if (isWebmaster($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE projets SET envoyer = "1", adminApprouve = 0, sendTime=' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
        }
    }
}


if ($_GET['block'] == "parametres") {
    if ($_SESSION['alertLogincontent'] == "") {
        $format = "";
        $temoin = 0;


        // top header messages by user`s types
        $user_groups = isset($_POST['user_groups'])? $_POST['user_groups'] : array();
        $_POST['user_groups'] = serialize($user_groups);
        $_POST[44] = serialize($user_groups);
        unset($_POST['user_groups']);

        foreach ($_POST as $key => $value) {
            if (($value=='')|| $value == NULL) {
                $temoin++;
            } else {
                if (isAdmin() || isSu()) {
                    // redaction desc
                    if (($key==38) || ($key==43) || ($key==45)){
                        $value = htmlentities($value,ENT_COMPAT,'ISO-8859-1');
                    }
                    $dbh->query('UPDATE parametres SET valeur = ' . $dbh->quote($value) . ' WHERE id=' . $dbh->quote(intval($key)));
                }
            }
        }

        $_SESSION['allParameters'] = Functions::getAllParameters();

        if ($temoin > 0) {
            $_SESSION['alertLogincontent'] .= "Un ou plusieurs champ(s) sont rest�s vides. Veuillez verifier.<br/>";
        }
    }
}

if ($_GET['block'] == "profil") {

    if (empty($_POST['nom']) || !Functions::checkNameFormat($_POST['nom'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un nom valide.<br/>";
    }

    if (empty($_POST['prenom']) || !Functions::checkNameFormat($_POST['prenom'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir un ou des pr�nom(s) valide(s).<br/>";
    }

    if (isset($_POST['typeutilisateur']) && empty($_POST['typeutilisateur'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez obligatoirement choisir le type d'utilisateur.<br/>";
    }
//
    if (!isset($_POST['frais']) || empty($_POST['frais'])) {
        $_POST['frais'] = 0;
    }

    if (empty($_POST['email']) || !Functions::checkEmailFormat($_POST['email'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez obligatoirement saisir une adresse email valide.<br/>";
    }

    if (!isset($_POST['societe']) || empty($_POST['societe'])) {
        $_POST['societe'] = "";
    }


    if (!isset($_POST['ville']) || empty($_POST['ville'])) {
        $_POST['ville'] = "";
    }

    if (empty($_POST['telephone']) || strlen($_POST['telephone']) < 8) {
//        $_SESSION['alertLogincontent'] .= "Veuillez saisir un num�ro de t�l�phone valide<br/>";
    }

    if (isset($_POST['lastIP']) && $_POST['lastIP'] == 111111) {
        $lastIP = $_POST['lastIP'];
        if (isset($_POST['styleAdmin']) && $_POST['styleAdmin'] > 0) {
            $styleAdmin = $_POST['styleAdmin'];
        } else {
            $styleAdmin = 0;
        }
    } else {
        $lastIP = 0;
        $styleAdmin = 0;
    }

    if (!empty($_FILES['avatar']['size'])) {
        $extention = strrchr(basename($_FILES['avatar']['name']), ".");
        if ($extention != ".jpg" && $extention != ".jpeg" && $extention != ".png" && $extention != ".bmp") {
            $_SESSION['alertLogincontent'] .= "Le fichier a uploader doit �tre un fichier image.<br/>";
        } else {
            
        }
    }

    if (!empty($_POST['siteweb']) && !Functions::checkUrlFormat($_POST['siteweb'])) {
        $_SESSION['alertLogincontent'] .= "Veuillez saisir l'url correcte du site web<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {

        if (!empty($_FILES['avatar']['size'])) {
            $_POST['avatar'] = "./upload/" . intval($_GET['id']) . $extention;
            $_SESSION['connected']['avatar'] = $_POST['avatar'];
            $_SESSION['avatar'] = $_SESSION['connected']['avatar'];
            $image_sizes = getimagesize($_FILES['avatar']['tmp_name']);
            $nomimage = intval($_GET['id']) . $extention;
            $image = "files/upload/" . $nomimage;
            move_uploaded_file($_FILES['avatar']['tmp_name'], $image);
        }


        if ((isSu($idTypeUser) || isAdmin($idTypeUser)) && $idUser != $_GET['id']) {
            $writers_choosing = isset($_POST['writers_choosing'])? 1: 0;
            $tarif_redaction = floatval($_POST['tarif_redaction']);
            $annuaire_writer = isset($_POST['annuaire_writer'])? 1: 0;
            $redaction_writer = isset($_POST['redaction_writer'])? 1: 0;
            $bonus_projects = isset($_POST['bonus_projects'])? 1: 0;



            $dbh->query('UPDATE utilisateurs  SET prenom = ' . $dbh->quote(($_POST['prenom'])) . ',
                                avatar = ' . $dbh->quote($_POST['avatar']) . ',
                                ville = ' . $dbh->quote(($_POST['ville'])) . ',
                                nom = ' . $dbh->quote(($_POST['nom'])) . ',
                                telephone = ' . $dbh->quote(($_POST['telephone'])) . ',
                                email = ' . $dbh->quote($_POST['email']) . ',
                                societe = ' . $dbh->quote(($_POST['societe'])) . ',
                                frais = ' . $dbh->quote(floatval($_POST['frais'])) . ',
                                typeutilisateur = ' . $dbh->quote($_POST['typeutilisateur']) . ',
                                codepostal = ' . $dbh->quote(($_POST['codepostal'])) . ',
                                adresse = ' . $dbh->quote(($_POST['adresse'])) . ',
                                siteweb = ' . $dbh->quote($_POST['siteweb']) . ',
                                lastIP = ' . $dbh->quote(($lastIP)) . ',
                                styleAdmin = ' . $dbh->quote(floatval($styleAdmin)) . ',
                                writers_choosing = "'.$writers_choosing.'",
                                tarif_redaction = "'.$tarif_redaction.'",
                                annuaire_writer = "'.$annuaire_writer.'",
                                redaction_writer = "'.$redaction_writer.'",
                                bonus_projects = "'.$bonus_projects.'",
                                country = ' . $dbh->quote(($_POST['country'])) . ',
                                numtva = ' . $dbh->quote(($_POST['numtva'])) . ',
                                trusted = ' . $dbh->quote(($_POST['trusted'])) . '
                                
                                WHERE id=' . $dbh->quote(intval($_GET['id'])));
        } else {
            if ($idUser == $_GET['id']) {
                $dbh->query('UPDATE utilisateurs SET prenom = ' . $dbh->quote(($_POST['prenom'])) . ', avatar = ' . $dbh->quote($_POST['avatar']) . ', ville = ' . $dbh->quote(($_POST['ville'])) . ', nom = ' . $dbh->quote(($_POST['nom'])) . ', telephone = ' . $dbh->quote(($_POST['telephone'])) . ', email = ' . $dbh->quote($_POST['email']) . ', societe = ' . $dbh->quote(($_POST['societe'])) . ', adresse = ' . $dbh->quote(($_POST['adresse'])) . ', siteweb = ' . $dbh->quote($_POST['siteweb']) . ', codepostal = ' . $dbh->quote(($_POST['codepostal'])) . ',
                    country = ' . $dbh->quote(($_POST['country'])) . ',
                    numtva = ' . $dbh->quote(($_POST['numtva'])) . '
                 WHERE id=' . $dbh->quote(intval($_GET['id'])));
            }
        }

        if ($idUser == $_GET['id'] && $_POST['email'] != $_SESSION['connected']['email']) {
            header('location:./../../logout.html');
        } else {
            $special = 1;
            $_SESSION['alertLogincontent'] = "Modification effectu�e avec succ�s.<br/>";
            $redirectURL = $redirectURL . "&statut=success";
//            header('location:' . $redirectURL);
        }
    }
}




if ($_GET['block'] == "referenceur") {

    $commande = Functions::getCommandeByID($_GET['id']);
    $varNew = "";

    if (!empty($_POST['affectedTO']) && ($_POST['affectedTO'] <= 0)) {
        $_SESSION['alertLogincontent'] .= "Veuillez choisir un r�f�renceur existant pour ce projet.<br/>";
    }

//    if (!empty($_POST['datedemarrage']) && (!preg_match('#-#', $_POST['datedemarrage']) || (strtotime($_POST['datedemarrage']) < strtotime(date("d-m-Y", time())) && date("d-m-Y", strtotime($_POST['datedemarrage']) != $commande['adminApprouveTime'])))) {
    if (!empty($_POST['datedemarrage']) && (!preg_match('#-#', $_POST['datedemarrage']))) {
        $_SESSION['alertLogincontent'] .= "Veuillez selectionner une date de demarrage valide (dd-mm-yyyy).<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {

        if (!empty($_POST['datedemarrage']) && $commande['affectedTO'] != $_POST['affectedTO']) {
            $timer = Functions::timeToSet(strtotime($_POST['datedemarrage']));
            $timerAffected = $timer;

            $freq = Functions::getFrequence($commande['frequence']);
            $frequenceTache = $freq[0];
            $varNew = $timer . "|" . $frequenceTache;

            $dbh->query('UPDATE projets SET email = ' . $dbh->quote($varNew) . ', adminApprouveTime = ' . $dbh->quote($timer) . ', affectedTime = ' . $dbh->quote($timerAffected) . ' WHERE id=' . $dbh->quote($_GET['id']));
        }

        if ((isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer()) && !empty($_POST['affectedTO'])) {

            $timer = Functions::timeToSet(time());

            if ($varNew == "") {
                $freq = Functions::getFrequence($commande['frequence']);
                $frequenceTache = $freq[0];
                $varNew = $timer . "|" . $frequenceTache;
            }
            $secRefDec = 1;
            $secRef = "";
            $temoinREDO = array();
            while ($secRefDec <= $refsecNbre) {
                $ligneBew = intval($_POST['affectedTO' . $secRefDec]);
                if ($ligneBew != $commande['affectedTO'] && $ligneBew > 0 && !in_array($ligneBew, $temoinREDO)) {
                    $temoinREDO[] = $ligneBew;
                    $secRef .= intval($ligneBew) . ";";
                } else {
                    $secRef .= "0;";
                }
                $secRefDec++;
            }

            if ($secRef == $defaultHash) {
                $secRef = "";
            }

            if ($commande['affectedTO'] != $_POST['affectedTO']) {


                $dbh->query('UPDATE projets SET categories = ' . $dbh->quote($secRef) . ', email = ' . $dbh->quote($varNew) . ', affectedTO = ' . $dbh->quote($_POST['affectedTO']) . ', affectedBY = ' . $dbh->quote($idUser) . ', affectedTime = ' . $dbh->quote($timer) . ' WHERE id=' . $dbh->quote($_GET['id']));
            } else {
                $dbh->query('UPDATE projets SET categories = ' . $dbh->quote($secRef) . ' WHERE id=' . $dbh->quote(intval($_GET['id'])));
            }
            if (isSu() || isAdmin()) {
                header('location:./../details.html?id=' . $_GET['id']);
            }
        }
    }
}



if ($_GET['block'] == "repartition") {

    if (!isset($_POST['projetFrequence']) || empty($_POST['projetFrequence']) || (!empty($_POST['projetFrequence']) && ($_POST['projetFrequence'] <= 0))) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer le nombre de projet � ex�cuter.<br/>";
    }

    if (!isset($_POST['jourFrequence']) || empty($_POST['jourFrequence']) || (!empty($_POST['jourFrequence']) && ($_POST['jourFrequence'] <= 0))) {
        $_SESSION['alertLogincontent'] .= "Veuillez entrer le nombre de jours.<br/>";
    }


    if (!isset($_POST['dateinitiale']) || empty($_POST['dateinitiale']) || (!empty($_POST['dateinitiale']) && (!preg_match('#-#', $_POST['dateinitiale']) || strtotime($_POST['dateinitiale']) < strtotime(date("d-m-Y", time()))))) {
        $_SESSION['alertLogincontent'] .= "Veuillez selectionner une date initiale valide (dd-mm-yyyy).<br/>";
    }

    if ($_SESSION['alertLogincontent'] == "") {

        $firstDate = strtotime($_POST['dateinitiale']);
        $nbreProjet = $_POST['projetFrequence'];
        $espacement = $_POST['jourFrequence'];
        $control = 0;
        $timeToSet = 0;
        $timeToCalculate = 0;
        $referenceurTampon = 0;
        $iduserrs = isset($_GET['iduser']) ? intval($_GET['iduser']) : 0;

        if ($iduserrs > 0) {
            $tousProjets = Functions::getAllCommandeReferer($iduserrs, 6, " id ASC");
        } else {
            $tousProjets = Functions::getAllCommandeReferer(0, 1, " affectedTO ASC");
        }

        foreach ($tousProjets as $lineProject) {

            if ($referenceurTampon != $lineProject['affectedTO']) {
                $referenceurTampon = $lineProject['affectedTO'];
                $control = 0;
            }

            if ($control == 0) {
                $timeToSet = Functions::timeToSet($firstDate);
//                echo date('d-m-Y', $timeToSet);
            } else {
                if ($control % $nbreProjet == 0 && $control > 0) {
//                    echo $control.".";
                    $timeToCalculate = $timeToSet + ($dureeJournee * $espacement);
                    $timeToSet = Functions::timeToSet($timeToCalculate);
                }
            }


            $varNew = "";
            if ($varNew == "") {
                $freq = Functions::getFrequence($lineProject['frequence']);
                $frequenceTache = $freq[0];
                $varNew = $timeToSet . "|" . $frequenceTache;
            }

            if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                $dbh->query('UPDATE projets SET email = ' . $dbh->quote($varNew) . ', adminApprouveTime=' . $dbh->quote($timeToSet) . ' WHERE id=' . $dbh->quote($lineProject['id']));
            }
            $control++;
        }
    }
}




if (($_GET['block'] == "actiongroupee" || $_GET['block'] == "affectationgroupee")) {

    if (!isset($_POST['projets']) || count($_POST['projets']) < 0) {
        $_SESSION['alertLogincontent'] .= "Veuillez cocher les cases auxquelles appliquer cette action.<br/>";
    } else {
        if (isset($_POST['affectedTO']) && ($_POST['affectedTO'] == "" || intval($_POST['affectedTO']) == 0 )) {
            $_SESSION['alertLogincontent'] .= "Veuillez selectionner le r�f�renceur pour ce(s) projet(s).<br/>";
            $_SESSION["PostProjets"] = (array) $_POST['projets'];
        }
    }

    $oChecked = $_POST['okChecked'];
    if (isset($_POST['envoyer2']) && !empty($_POST['envoyer2']) && isset($_POST['okChecked2']) && !empty($_POST['okChecked2'])) {
        $oChecked = $_POST['okChecked2'];
    }
    if (isset($_POST['envoyer3']) && !empty($_POST['envoyer3']) && isset($_POST['okChecked3']) && !empty($_POST['okChecked3'])) {
        $oChecked = $_POST['okChecked3'];
    }

//    print_r($_POST);
//    echo $oChecked;

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isWebmaster($idTypeUser) || isReferer($idTypeUser) || isSuperReferer($idTypeUser)) {

            if ($oChecked == "archivageGroupeeProjets") {
                foreach ($_POST['projets'] as $value) {
                    if ($value != "" && $value != 0) {
                        $dbh->query('UPDATE projets SET showProprio = 0 WHERE proprietaire=' . $dbh->quote($idUser) . ' AND id=' . $dbh->quote(intval($value)));
                    }
                }
            }

            if ($oChecked == "suppressionGroupeeProjets") {

                foreach ($_POST['projets'] as $value) {
                    if ($value != "" && $value != 0) {
                        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                            $dbh->query('DELETE FROM projets WHERE id=' . $dbh->quote(intval($value)));
                        }
                        if (isWebmaster()) {
                            $dbh->query('DELETE FROM projets WHERE proprietaire = "' . $idUser . '" AND id=' . $dbh->quote(intval($value)));
                        }
                    }
                }
                unset($_SESSION["PostProjets"]);
            }

            if ($oChecked == "demarrageGroupeeProjets") {
                foreach ($_POST['projets'] as $value) {
                    if ($value != "" && $value != 0) {
                        $projet = Functions::getCommandeByID(intval($value));
                        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isWebmaster($idTypeUser)) {
                            if (isMultiple($projet['lien'])) {
                                $count = 0;
                                $projetParent = $projet['id'];
                                $arrayLien = explode(PHP_EOL, $projet['lien']);
                                foreach ($arrayLien as $valueUrl) {
                                    if ($valueUrl != "") {
                                        if ($count == 0) {
                                            $dbh->query('UPDATE projets SET lien =' . $dbh->quote($valueUrl) . ', parent =' . $dbh->quote($projetParent) . ', envoyer = "1", sendTime = ' . $dbh->quote(time()) . ', adminApprouve = "1", adminApprouveTime = ' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote(intval($value)));
                                        } else {
                                            $dbh->query('INSERT INTO projets VALUES("", ' . $dbh->quote($projetParent) . ', ' . $dbh->quote($projet['categorieID']) . ', ' . $dbh->quote($valueUrl) . ', ' . $dbh->quote($projet['email']) . ', ' . $dbh->quote($projet['proprietaire']) . ', ' . $dbh->quote($projet['frequence']) . ', "0", ' . $dbh->quote($projet['annuaire']) . ', ' . $dbh->quote($projet['commentaire']) . ', "1", ' . $dbh->quote(time()) . ', "1",' . $dbh->quote(time()) . ',"","0","0","0","0", "0","1", "")');
                                        }
                                        $count++;
                                    }
                                }
                            } else {
                                $dbh->query('UPDATE projets SET envoyer = "1", sendTime = ' . $dbh->quote(time()) . ', adminApprouve = "1", adminApprouveTime = ' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote(intval($value)));
                            }
                        }
                    }
                }
            }

            if ($oChecked == "repriseGroupeeProjets") {
                foreach ($_POST['projets'] as $value) {
                    if ($value != "" && $value != 0 && (isAdmin() || isSu() || isWebmaster())) {
                        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                            $dbh->query('UPDATE projets SET affectationgroupee = "0", overTime=' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote(intval($value)));
                        }
                        if (isWebmaster()) {
                            $dbh->query('UPDATE projets SET over = "0", overTime=' . $dbh->quote(time()) . ' WHERE proprietaire = ' . $dbh->quote($idUser) . ' AND id=' . $dbh->quote(intval($value)));
                        }
                    }
                }
            }

            if ($oChecked == "arretGroupeeProjets") {
                foreach ($_POST['projets'] as $value) {
                    if ($value != "" && $value != 0 && (isAdmin() || isSu() || isWebmaster())) {
                        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
                            $dbh->query('UPDATE projets SET over = "1", overTime=' . $dbh->quote(time()) . ' WHERE id=' . $dbh->quote(intval($value)));
                        }
                        if (isWebmaster()) {
                            $dbh->query('UPDATE projets SET over = "1", overTime=' . $dbh->quote(time()) . ' WHERE proprietaire = ' . $dbh->quote($idUser) . ' AND id=' . $dbh->quote(intval($value)));
                        }
                    }
                }
                unset($_SESSION["PostProjets"]);
            }

            if ($oChecked == "affectationGroupeeProjets") {
                $timers = time();
                $timer = Functions::timeToSet(time());
                $varNew = "";

                // if is writers
                if (isReferer()|| isSuperReferer()){
                    $_POST['affectedTO'] = $idUser;
                }

                if (!empty($_POST['affectedTO'])) {

                    $_POST['affectedTO'] = ltrim($_POST['affectedTO'], '0');
//                    print_r($_POST['projets']);


                    foreach ($_POST['projets'] as $value) {
                        if ($value != "" && $value != 0 && (isAdmin() || isSu() || isReferer() || isSuperReferer())) {
                            $commande = Functions::getCommandeByID($value);

                            if ($commande['affectedTime'] == 0) {
                                $timer = $commande['adminApprouveTime'];
                            }

                            if ($varNew == "") {
                                if ($commande) {
                                    $freq = Functions::getFrequence($commande['frequence']);
                                    $frequenceTache = $freq[0];
                                    $varNew = $timer . "|" . $frequenceTache;
                                }
                            }

                            $secRefDec = 1;
                            $secRef = "";
                            $temoinREDO = array();
                            while ($secRefDec <= $refsecNbre) {
                                if (isset($_POST['affectedTO' . $secRefDec])) {
                                    $ligneBew = intval($_POST['affectedTO' . $secRefDec]);
                                    if ($ligneBew != $_POST['affectedTO'] && $ligneBew != 0 && !in_array($ligneBew, $temoinREDO)) {
                                        $temoinREDO[] = $ligneBew;
                                        $secRef .= intval($ligneBew) . ";";
                                    }
                                }
                                $secRefDec++;
                            }

                            if ($secRef == $defaultHash) {
                                $secRef = "";
                            }

                            // set fixed_writer
                            $is_fixed_writer = isset($_POST['is_fixed_writer'])? 1 : 0;

                            // timer fix
                            // set current time istead strange Functions::timeToSet
                            $timer = time();

                            $resEff = false;
                            if ($commande['affectedTO'] == $_POST['affectedTO']) {
                                $resEff = $dbh->query('UPDATE projets SET categories = ' . $dbh->quote($secRef) . ', is_fixed_writer = '.$dbh->quote($is_fixed_writer).' WHERE id=' . $dbh->quote(intval($value)) . '');
                            } else {
                                $resEff = $dbh->query('UPDATE projets SET categories = ' . $dbh->quote($secRef) . ', is_fixed_writer = '.$dbh->quote($is_fixed_writer).', email = ' . $dbh->quote($varNew) . ', affectedTO = ' . $dbh->quote($_POST['affectedTO']) . ', affectedBY = ' . $dbh->quote($idUser) . ', affectedTime = ' . $dbh->quote($timer) . ', adminApprouveTime = UNIX_TIMESTAMP()  WHERE id=' . $dbh->quote(intval($value)) . '');
//                                $resEff = $dbh->query('UPDATE projets SET affectedTO = "158" WHERE id=' . intval($value));
                            }
                            if ($resEff == true) {
//                                echo $value . "/";
                            }
                        }
                    }
                }
                unset($_SESSION["PostProjets"]);
            }

            if ($oChecked == "HeureDemarrageGroupeeProjets") {

                $timerAffected = "";
                $timer = "";
                if (!empty($_POST['datedemarrage']) && strtotime($_POST['datedemarrage']) >= strtotime(date("d-m-Y", time())) && preg_match('#-#', $_POST['datedemarrage'])) {
                    $timer = Functions::timeToSet(strtotime($_POST['datedemarrage']));
                    $timerAffected = $timer;
                }

                if ($timerAffected != "" && $timer != "") {
                    foreach ($_POST['projets'] as $value) {
                        if ($value != "" && $value != 0 && (isAdmin() || isSu())) {
                            $commandeCurrent = Functions::getCommandeByID($value);
                            if (!empty($_POST['datedemarrage'])) {
                                $freq = Functions::getFrequence($commandeCurrent['frequence']);
                                $frequenceTache = $freq[0];
                                $varNew = $timer . "|" . $frequenceTache;

                                $dbh->query('UPDATE projets SET email = ' . $dbh->quote($varNew) . ',  adminApprouveTime = ' . $dbh->quote($timer) . ', affectedTime = ' . $dbh->quote($timerAffected) . ' WHERE id=' . $dbh->quote(intval($value)));
                            }
                        }
                    }
                    unset($_SESSION["PostProjets"]);
                }
            }
            unset($_SESSION["PostProjets"]);
        }
    }
}






if (($_GET['block'] == "redaction_actiongroupee" || $_GET['block'] == "redaction_affectationgroupee")) {


    //if (!isset($_POST['projets']) || count($_POST['projets']) < 0) {
      //  $_SESSION['alertLogincontent'] .= "Veuillez cocher les cases auxquelles appliquer cette action.<br/>";
    //} else {
    if (isset($_POST['affectedTO']) && ($_POST['affectedTO'] == "" || intval($_POST['affectedTO']) == 0 )) {
            $_SESSION['alertLogincontent'] .= "Veuillez selectionner le r�f�renceur pour ce(s) projet(s).<br/>";
            $_SESSION["PostProjets"] = (array) $_POST['projets'];
    }

    $oChecked = $_POST['okChecked'];
  //    print_r($_POST);
//    echo $oChecked;

    if ($_SESSION['alertLogincontent'] == "") {
        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer($idTypeUser)) {

            // set writer for project(s)
            if ($oChecked == "redaction_affectationgroupee") {
                $project_id = $_POST['project_id'];
                $writer_id = $_POST['affectedTO'];
                $status = 'progress';
                $affected_time = time();


                $sql = 'UPDATE redaction_projects
                        SET affectedTO = '.$dbh->quote($writer_id).',
                            affected_time = FROM_UNIXTIME('.$dbh->quote($affected_time).'),
                            status = "progress"
                        WHERE id ='.$dbh->quote($project_id);

                $result = $dbh->query($sql);
            }


        }
        if (isReferer()){

            if ($oChecked == "redaction_affectationgroupee") {
                $projects_ids = $_POST['projets'];
                $writer_id = $_POST['writer_id'];
                $affected_time = time();

                foreach ($projects_ids as $id) {
                    $sql = 'UPDATE redaction_projects
                        SET affectedTO = ' . $dbh->quote($writer_id) . ',
                            affected_time = FROM_UNIXTIME(' . $dbh->quote($affected_time) . '),
                            status = "progress"
                        WHERE id =' . $dbh->quote($id);

                    $result = $dbh->query($sql);
                }
            }
        }

    }
}


if ($_GET['block'] == "tacheeffectuee") {

    $cant = 0;
    if (empty($_GET['param'])) {

        $_SESSION['alertLogincontent'] = "Un probl�me est survenu. Veuillez r�essayer.<br/>";
    } else {

        if (isset($_GET['cant']) && $_GET['cant'] == 1) {
            $cant = 1;
        }


        if (isReferer()){
            $show_bonus_projects = false;

            $show_bonus_projects = !empty($_SESSION['connected']['bonus_projects'])? true : false;

            // test
            // $enCours = 0;
            // $show_bonus_projects = true;

            $BONUS_WRITER_ACCESS = ( isReferer() && ($enCours==0) &&  $show_bonus_projects);

            if (!$BONUS_WRITER_ACCESS){
                $BONUS_WRITER_ACCESS = ( isReferer() && isset($_SESSION['connected']['BONUS_ACTIVE']) &&  $show_bonus_projects);
            }

        }

        $params = urldecode(unserialize(base64_decode(Functions::decodage(CLEINTERNE, $_GET['param']))));
        $params = explode('|', $params);




        if (!isset($params[3]) || empty($params[3]) || !isset($params[2]) || empty($params[2]) || !isset($params[1]) || empty($params[1]) || !isset($params[0]) || empty($params[0]) || (!empty($params[2]) && intval($params[2]) != 1)) {
            $_SESSION['alertLogincontent'] = "Un probl�me est survenu. Veuillez r�essayer.<br/>";
        } else {
            $getCommande = Functions::getCommandeByID($params[3]);

            $idUserReal = $idUser;
            if ($getCommande['affectedSec'] != "" && $getCommande['affectedSec'] != $defaultHash) {
                $specialAssignement = 0;
                $secondaireRef = explode(";", $getCommande['affectedSec']);
                $secondaireRef = array_filter($secondaireRef);
                if (in_array($idUser, $secondaireRef)) {
                    $idUser = $getCommande['affectedTO'];
                    $specialAssignement = 1;
                }
            }

            if (!isset($getCommande['id']) || $getCommande['affectedTO'] != $idUser) {
                if (!$BONUS_WRITER_ACCESS) {
                    $_SESSION['alertLogincontent'] .= "Attention � l'action que tentez de faire. Ceci est un avertissement de l'administrateur...<br/>";
                }
            } else {
                $getLastJob = Functions::getLastJob($getCommande['id']);
                $createdSoum = 0;

                if (!isset($getLastJob['soumissibleTime']) || $getLastJob['soumissibleTime'] == NULL) {
                    $getLastJob['soumissibleTime'] = intval($params[1]);
                    $createdSoum = 1;
                }

//        print_r($params);
//                if (($createdSoum == 0 && intval($params[1]) > $getLastJob['soumissibleTime'])) {
//                    $_SESSION['alertLogincontent'] .= "Attention � l'action que tentez d'ex�cuter. Ceci est un avertissement de l'administrateur....<br/>";
//                }
            }
        }
    }


    if ($_SESSION['alertLogincontent'] == "") {
        if (isset($getCommande['id'])) {
            $currentTime = time();

            $jobsModel = new Jobs();
            $annuaireModel = new Annuaire();

            // access for bonus projects
            if ( isReferer($idTypeUser) && $BONUS_WRITER_ACCESS ){
                $getCommande['affectedTO'] = $idUserReal;
            }


            if ( isReferer($idTypeUser) && ($getCommande['affectedTO'] == $idUser)) {

                unset($_SESSION['tacheTampon']);
                if ((!isset($_SESSION['tacheTampon']) || (isset($_SESSION['tacheTampon']) && $_SESSION['tacheTampon'] != $params[0] . $getCommande['affectedTO'] . $getCommande['id']))) {

                    // set writer tarif redaction for calculations
                    $writer_tarif_redaction = floatval($_SESSION['tarif_redaction']);
                    if (!$writer_tarif_redaction){ // common tarif
                        $writer_tarif_redaction = floatval($_SESSION['allParameters']['writer_price_100_words']['valeur']);
                    }

                    //  User annuaire-list words count
                    $LIST_ANNUAIRE_WORDS_COUNT = 0;
                    $nnu = Functions::getAnnuaireNameById($getCommande['annuaire']);
                    if ($nnu['words_count']) {
                        $LIST_ANNUAIRE_WORDS_COUNT = $nnu['words_count'];
                    }

                    $getAnnuaireInfos = Functions::getAnnuaire($params[0]);

                    $AnnuaireAuthoriser = explode(";", $getCommande['adminRaison']);

                    if ($getAnnuaireInfos && in_array($getAnnuaireInfos['id'], $AnnuaireAuthoriser)) {

                        $projet_id = $getCommande['id'];
                        $refererSolde = Functions::getSolde($idUserReal);
                        $soldeWebmaster = Functions::getSolde($getCommande['proprietaire']);
                        $tarifWebmaster = Functions::getRemunuerationWebmaster($getCommande['proprietaire']);

                        // calcuate extra-sums
                        // if user set words count for annuaire-list
                        $webmaster_extra_cost = 0.0;
                        $writer_extra_earn = 0.0;
                        if ($LIST_ANNUAIRE_WORDS_COUNT > 0){
                            $webmaster_extra_cost = $annuaireModel->calculateExtraPrice($getAnnuaireInfos,$LIST_ANNUAIRE_WORDS_COUNT);
                            $writer_extra_earn = $annuaireModel->calculateExtraPriceForWriter($getAnnuaireInfos,$LIST_ANNUAIRE_WORDS_COUNT,$writer_tarif_redaction);
                        }

                        // add extra cost for task-cost
                        if ($webmaster_extra_cost > 0){
                            $tarifWebmaster += $webmaster_extra_cost;
                        }

                        if ($soldeWebmaster >= $tarifWebmaster) {
                            $getSupps = Functions::getPersoWebmasterCompte($projet_id);

                            //var_dump($getSupps);
                            $bonusAdded = 0;
                            $bonusAdded_Web = 0;
                            if ($getSupps && in_array($params[0], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                                $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                                if ($bonusAdded == 0) {
                                    $bonusAdded = $ComptePersoReferenceur;
                                }
                                if ($bonusAdded_Web == 0) {
                                    $bonusAdded_Web = $ComptePersoWebmaster;
                                }
                            }

                            //var_dump($bonusAdded);
                            //var_dump($bonusAdded_Web);

                            //var_dump($remuneration);
                            $solde = $refererSolde;
                            $CoutWebmaster = $tarifWebmaster + $getAnnuaireInfos['tarifW'] + $bonusAdded_Web;
                            $CoutReferenceur = $remuneration + $getAnnuaireInfos['tarifR'] + $bonusAdded + $bonusNew;

                            // add extra earn for writer
                            if ( $writer_extra_earn > 0.0){
                                $CoutReferenceur += $writer_extra_earn;
                            }

                            // writer has malus project(s)
                            $malus_project_count = Functions::getBacklinkCouplesCount('not_found_yet',0, $idUserReal,0,1);
                            //var_dump($malus_project_count,$idUserReal);
                            //var_dump($newSolde);
                            if ($malus_project_count){
                                $default_malus = 0.15;
                                $malus = isset($_SESSION['allParameters']['malus_writer']["valeur"])? floatval($_SESSION['allParameters']['malus_writer']["valeur"]): $default_malus;
                                $CoutReferenceur = $CoutReferenceur - $malus;
                            }

                            //var_dump($solde);
                            $newSolde = $solde + $CoutReferenceur; // increasing writer balance
                            $newSoldeWebmaster = $soldeWebmaster - ($CoutWebmaster); // reduce webmaster balance
                            $repnSend = "yes";


                            //var_dump($newSolde,$_SESSION['allParameters']['malus_writer']["valeur"]);

                            if (!Functions::getDoublon("jobs", "siteID", $params[3], "annuaireID", intval($params[0]))) {
                                $adminApprouved = 2;
                                if ($cant == 1) {
                                    $adminApprouved = 3;
                                    $repnSend = "no";
                                }

                                //var_dump($CoutReferenceur);
                                //die;

                                $return = $dbh->query('INSERT INTO jobs VALUES(null, ' . $dbh->quote($CoutReferenceur) . ', ' . $dbh->quote($CoutWebmaster) . ', ' . $dbh->quote($params[3]) . ', ' . $dbh->quote(intval($params[0])) . ', ' . $dbh->quote($getAnnuaireInfos['annuaire']) . ', ' . $dbh->quote($getCommande['email']) . ', ' . $dbh->quote($idUserReal) . ', ' . $dbh->quote($getCommande['affectedTime']) . ', ' . $dbh->quote($currentTime) . ', ' . $dbh->quote(intval($params[1])) . ', ' . $dbh->quote($adminApprouved) . ', ' . $dbh->quote($currentTime) . ',"1",' . $dbh->quote($currentTime) . ')');

                                if ($cant == 0) {
                                    if ($return) {
                                        $jobsModel->deleteRejectedJob($getCommande['id'],intval($params[0]));

                                        $setLastSoumission = $dbh->query('UPDATE projets SET budget = ' . $dbh->quote($currentTime) . ' WHERE id=' . $dbh->quote($getCommande['id']));
                                        $_SESSION['tacheTampon'] = $params[0] . $getCommande['affectedTO'] . $getCommande['id'];
                                        if ($adminApprouved == 2) {
                                            $setWebmaster = $dbh->query('UPDATE utilisateurs SET solde = ' . $dbh->quote($newSoldeWebmaster) . ' WHERE id=' . $dbh->quote($getCommande['proprietaire']));

                                            $annuaireName = $annuaireModel->getAnnuaireNameById($getCommande['annuaire']);
                                            // save transaction when a task is done by writer  (for webmaster)
                                            $transaction = new Transaction($dbh, $getCommande['proprietaire']);
                                            $transaction->addTransaction("R�alisation de la t�che " . $getCommande['lien'] . " sur " . $annuaireName, 0, $CoutWebmaster, $getCommande['id']);
                                            
                                            if ($setWebmaster) {
                                                $reSo = $dbh->query('UPDATE utilisateurs SET solde = ' . $dbh->quote($newSolde) . ' WHERE id=' . $dbh->quote($idUserReal));
                                                // save transaction when a task is done by writer (for writer)
                                                $transaction = new Transaction($dbh, $idUserReal);
                                                $transaction->addTransaction("R�alisation de la t�che " . $getCommande['lien'] . " sur " . $annuaireName, $CoutReferenceur, 0, $getCommande['id']);
                                                
                                                if ($reSo) {
                                                    $_SESSION['connected']['solde'] = $newSolde;
                                                }
                                            }

                                            $dbh->query('INSERT IGNORE INTO annuaires2backlinks (`site_id`, `annuaire_id`, `backlink`,`date_checked`,`date_checked_first`,`status`,`status_type`) VALUES(' . $dbh->quote($params[3]) . ', ' . $dbh->quote($params[0]) . ', "", ' . $dbh->quote(date("Y-m-d H:i:s")) . ', ' . $dbh->quote(date("Y-m-d H:i:s")) . ', "not_found_yet", "cron")');
                                            $dbh->query('INSERT IGNORE INTO soumissionsage VALUES(null, ' . $dbh->quote($params[3]) . ', ' . $dbh->quote(intval($params[0])) . ', ' . $dbh->quote($idUserReal) . ',"0","1",' . $dbh->quote(time()) . ')');
                                        }
                                    } else {
                                        $_SESSION['alertLogincontent'] = "Une erreur est survenue. Veuillez r�essayer ou contacter un administrateur.<br/>";
                                    }
                                }
                                $special = 1;
                                $_SESSION['alertLogincontent'] = "R�sultat de la soumission enregistr� avec succ�s.<br/>";
                                $redirectURL = $redirectURL . "&statut=success";
//                            header('location:' . $redirectURL);
//                            echo '../ajout/commentaire.html?soumission=' . $repnSend . '&projet=' . $params[3] . '&annuaire=' . $params[0];
                                header('location: ../ajout/commentaire.html?soumission=' . $repnSend . '&projet=' . $params[3] . '&annuaire=' . $params[0]);
                                exit;
                            } else {
                                $_SESSION['alertLogincontent'] = "Cette t�che a d�ja �t� effectu�e.<br/>";
                            }
                        } else {
                            $_SESSION['alertLogincontent'] = "Le client ne dipose plus d'assez de fonds pour financer cette soumission. Informez un administrateur.<br/>";
                        }
                    } else {
                        $_SESSION['alertLogincontent'] = "Cet annuaire n'est pas pr�vu dans la liste des t�che de ce projet. Un rapport sera envoyer � un administrateur.<br/>";
                    }
                }
            }
        }
    }
}


if ($_SESSION['alertLogincontent'] != "") {
    if (!strpos($redirectURL, "?")) {
        $redirectURL = $redirectURL . "?statut=error";
    } else {
        $redirectURL = $redirectURL . "&statut=error";
    }

    if ($special == 0) {
        header('location:' . $redirectURL);
    }
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">

            <div class="permalink">
                <h4><a nohref="#">Statut de l'op�ration en cours</a></h4>
            </div>

            <div class = "notification success">
                <p>
                    <?PHP
                    if ($specialMessage != "") {
                        ?>
                        <span>Success</span> <?PHP echo $specialMessage; ?>.
                        <?PHP
                    } else {
                        // override basic success message for creating project
                        if (strpos($_SERVER['HTTP_REFERER'],'emailpro')) {
                        ?>
                            Merci pour votre projet, n'oubliez pas de le d�marrer maintenant !
                        <?php
                        }else{?>
                            <span>Success</span> Op�ration effectu�e avec succ�s.
                        <?php
                            }
                        ?>
                        <?PHP
                    }
                    ?>
                </p>
            </div>
            <?PHP echo isButton(); ?>

            <!--Pagination-->

            <!--End pagination-->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php");
?>