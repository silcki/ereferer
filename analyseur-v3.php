<?php

set_time_limit(1000);
error_reporting(0);
// http://analyseur_v3.php?nddcible=lesite.fr&liens=lelien.com


global $_hostname;
global $_bdd;
global $_login;
global $_pwd;
global $_connexionPDO;
include_once 'bdd_config.php';


$_connexionPDO = new PDO('mysql:host=' . $_hostname . ';dbname=' . $_bdd, $_login, $_pwd);

global $liens;

$liens = trim($_REQUEST['liens']);
$nomDomaine = trim($_REQUEST['nddcible']);
$maxIteration = 40;
$pageDeb = trim("");

$liensArray = array();
if (preg_match("#;#", $liens)) {
    $liensArray = explode(";", $liens);
    foreach ($liensArray as $chaqueLien) {
//        $chaqueLien = preg_replace('#http://#', '', $chaqueLien);
//        $chaqueLien = preg_replace('#https://#', '', $chaqueLien);
//        $chaqueLien = preg_replace('#www.#', '', $chaqueLien);
        $liensArray[] = $chaqueLien;
    }
} else {
    $liensArray[] = $liens;
}



global $find;
$find = '';

function runner($urlgo, $ndd, $monlien) {

    global $_connexionPDO;
    global $find;
//    global $liensArray;
//    $monlien = 'http://www.' . $monlien;

    $urlCnt = urldecode($urlgo);
    $optionsCnt = array(
        CURLOPT_URL => $urlCnt,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => false,
        CURLOPT_FAILONERROR => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13',
        CURLOPT_REFERER => 'http://www.google.fr/search?hl=fr&source=hp&q=' . $ndd . '&aq=f&aqi=g2&aql=&oq=',
    );
    $CURLCnt = curl_init();
    if (empty($CURLCnt)) {
        die("ERREUR curl_init : Il semble que cURL ne soit pas disponible.");
    }
    curl_setopt_array($CURLCnt, $optionsCnt);
    $cnt = curl_exec($CURLCnt);
    if (curl_errno($CURLCnt)) {
//        echo "ERREUR curl_exec : " . curl_error($CURLCnt);
    }
    curl_close($CURLCnt);

    //echo ($cnt);
    if (preg_match_all("/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU", $cnt, $matches)) {
        /*
          echo '<br/>';
          print_r($matches[2]);
         */

        foreach ($matches[2] as $link) {
            $urlpropre = '';
            if ('http://www.' . $ndd == substr($link, 0, (11 + strlen($ndd)))) {
                $urlpropre = $link;
            } else {
                if ('http://' . $ndd == substr($link, 0, (7 + strlen($ndd)))) {
                    $urlpropre = $link;
                } else {
                    if ('tel:' != substr($link, 0, 4) && 'https://' != substr($link, 0, 8) && 'http://' != substr($link, 0, 7) && 'javascript' != substr($link, 0, 10) && substr($link, 0, 1) != "'") {
                        $urlpropre = $link;
                        if (strlen($link) < 7 || 'http://' != substr($link, 0, 7)) {
                            if ($link != '') {
                                if (substr($link, 0, 1) != '/') {
                                    $urlpropre = 'http://www.' . str_replace('//', '/', $ndd . '/' . $link);
                                } else {
                                    $urlpropre = 'http://www.' . str_replace('//', '/', $ndd . $link);
                                }
                            } else {
                                $urlpropre = 'http://www.' . $ndd;
                            }
                        }
                    }
                }
            }

            if ($urlpropre != '') {
                $nb = 0;
                $req = "SELECT count(1) nb FROM bot_urls WHERE url_analysee = '" . $urlpropre . "' "; // echo $req . '<br/>';
                $resultats2 = $_connexionPDO->query($req);
                while ($resultat2 = $resultats2->fetch(PDO::FETCH_OBJ)) {
                    $nb = intval($resultat2->nb);
                }

                if ($nb < 1) {
                    $req = "INSERT INTO `bot_urls` (`url_analysee`, `url_recherchee`, `est_traitee`) VALUES('" . $urlpropre . "', '" . $monlien . "', 0)";
                    $_connexionPDO->exec($req);
                }
            }

            if (substr($link, 0, strlen($monlien)) == $monlien) {
                $find = $urlgo;
                $reqA = "DELETE FROM `bot_urls` WHERE `est_traitee` = 0 AND `url_recherchee` = '" . $monlien . "')";
//                $_connexionPDO->exec($reqA);
                break;
            }
        }
    }

    $_connexionPDO->exec("UPDATE `bot_urls` SET `est_traitee` = 1 WHERE `url_analysee` = '" . $urlgo . "' ");
}

/* * ****************************************************************************************** */
/* * ****************************************************************************************** */

function install_bdd($nomDomaine, $pageDeb) {

    global $_connexionPDO;
    global $liensArray;

    $_connexionPDO->exec("TRUNCATE TABLE bot_urls");

    $_connexionPDO->exec("CREATE TABLE IF NOT EXISTS `bot_urls` (
							  `id_url` int(11) NOT NULL AUTO_INCREMENT,
							  `url_analysee` varchar(300) DEFAULT NULL,
							  `url_recherchee` varchar(300) DEFAULT NULL,
							  `est_traitee` int(11) DEFAULT 0,
							  PRIMARY KEY (`id_url`)
							) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");

    foreach ($liensArray as $lienUnik) {
        if ($lienUnik != "") {
            $_connexionPDO->exec("INSERT INTO `bot_urls` (`url_analysee`, `url_recherchee`, `est_traitee`) VALUES('" . urlDebut($nomDomaine, $pageDeb) . "', '" . $lienUnik . "', 0)");
        }
    }
}

/* * ****************************** */

function urlDebut($ndd, $pageDebut = "") {
    $urlgo = $pageDebut;
    if (strlen($pageDebut) < 7 || 'http://' != substr($pageDebut, 0, 7)) {
        if ($pageDebut != '') {
            if (substr($pageDebut, 0, 1) != '/') {
                $urlgo = 'http://www.' . str_replace('//', '/', $ndd . '/' . $pageDebut);
            } else {
                $urlgo = 'http://www.' . str_replace('//', '/', $ndd . $pageDebut);
            }
        } else {
            $urlgo = 'http://www.' . $ndd;
        }
    }

    return trim($urlgo);
}

/* * ****************************** */

function findInterneLink() {
    global $_connexionPDO;
    $result = array();

    $resultats = $_connexionPDO->query("SELECT url_analysee, url_recherchee FROM bot_urls WHERE est_traitee = 0 ORDER BY url_recherchee LIMIT 1");
    while ($resultat = $resultats->fetch(PDO::FETCH_OBJ)) {
        $result['urlAnalysee'] = $resultat->url_analysee;
        $result['urlRecherchee'] = $resultat->url_recherchee;
    }

    return $result;
}

/* * ****************************************************************************************** */
/* * ****************************************************************************************** */

install_bdd($nomDomaine, $pageDeb);

$iterator = 0;

$afficherResultat = '';
echo '<ol style="">';
while (($urlEnAnalyse = findInterneLink()) !== '' && $iterator < $maxIteration) {
    $iterator++;
    runner($urlEnAnalyse['urlAnalysee'], $nomDomaine, $urlEnAnalyse['urlRecherchee']);

    global $find;
    echo '<li style="font-size:10px;color:';
    if ($find != '') {
        echo '#46B525;';
        $iterator = $maxIteration + 1;
    } else {
        echo '#dd0000;';
    }
//    echo '"> ' . $urlEnAnalyse['urlRecherchee'] . ' ===> ' . $urlEnAnalyse['urlAnalysee'] . "";
    echo '"> ' . $urlEnAnalyse['urlAnalysee'] . "";
    echo '</li>';
}
echo '<ol>';
?>