<?PHP
@session_start();

$a = 1;
$page = 5;
include('files/includes/topHaut.php');
$requete = "SELECT valeur FROM parametres WHERE id='26'";
$execution = $dbh->query($requete);
$retour = $execution->fetch();
$remunu = $retour['valeur'];
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <p style="font-size:15px;">
		<h1>Ereferer: La plateforme d'�change, d'achat et de vente d'articles N�1</h1>
            

Ce service d'�change d'articles fonctionne sous forme de cr�dits. Pour chaque publication, vous gagnez un certain nombre de cr�dits variable suivant la qualit� de votre site. Vous pourrez ensuite d�penser vos cr�dits pour publier � votre tour sur n'importe quel autre site partenaire. <b>Il n'y a donc aucun �change r�ciproque ni triangulaire</b>.</br></br>
Exemple :</br>
Un webmaster d�cide de publier un article sur mon site. Ce dernier me rapporte 3 cr�dits.</br>
Avec ses 3 cr�dits, je d�cide de publier 2 articles sur 2 autres sites diff�rents et ind�pendants. La premi�re publication me coute 1 cr�dit tandis que la seconde me coute 2 cr�dits.</br></br>
Les webmasters ne souhaitant pas rentrer dans le jeu des �changes auront la possibilit� d'<b>acheter des cr�dits, de les vendre</b>, ou de les �changer contre du solde Ereferer destin� au r�f�rencement dans les annuaires ou au module de r�daction.</br></br>

Pr�sentation de l'interface pour trouver des partenaires (Ces derniers ont la possibilit� de masquer leurs URL !) :</br>
<img src="http://www.ereferer.fr/files/images/echange.JPG" alt="�change d'articles SEO" /></br></br>

<h3>Combien coute ce service ?</h3>
<ul>
<li><b>- Les �changes :</b> Pour toutes les personnes jouant le jeu des �changes, ce service est gratuit. Vous gagnerez par exemple 1 cr�dit en recevant une publication et, � votre tour, il vous coutera 1 cr�dit pour publier chez un autre partenaire.</li></br>
<li><b>- L'achat d'articles :</b> Pour ceux pr�f�rant acheter directement des cr�dits pour publier chez des partenaires, ce dernier co�te 10�HT.</li></br>
<li><b>- La vente d'articles : </b>Pour ceux ne souhaitant pas r�utiliser leurs cr�dits gagn�s et d�cidant de les vendre, Ereferer vous les ach�te au prix de 6� HT</li></br>
<li>Enfin, les webmasters qui d�cideront d'�changer leurs cr�dits contre du solde Ereferer pour les annuaires ou le module de r�daction pourront �changer 1 cr�dit contre 10�.</li></ul></br></br>
Pour savoir combien de cr�dits peut rapporter votre site, testez le ici :</br>
<!--test credits-->
<br/>
 <label>URL � tester</label>
  <input id="mc_url" name="url" type="text" value="http://" style="float:left;width:500px;" />&nbsp;
  <input id="mc_test" type="submit" class="button color small round" value="Tester mon site" style="color:white;">

  Nombre de cr�dits maximum : <span id="credmax">0</span>

<script type="text/javascript">
		
	jQuery("#mc_test").click(function(){
	
		jQuery("#credmax").html('Chargement...');
		
		var urla=jQuery("#mc_url").val();
		
		jQuery.post( "files/includes/ajax/test_credits.php", { url: urla }).done(function( data ) {

			jQuery("#credmax").html(data);

		});;

	});
		 
</script>
<!--fin test credits-->
</br></br>


<h3>Fonctionnement du module</h3>
Pour chaque site que vous souhaitez ajouter, Ereferer plafonne un nombre de cr�dits maximum que vous pouvez gagner. Cet algorithme d�pend notamment de l'�ge du domaine, de son Trust Flow, et de son nombre de domaines r�f�rents. </br></br>Vous avez la <b>possibilit� de cacher votre URL</b> sur la plateforme. Dans ce cas, seules les metrics seront affich�es aux autres webmasters.</br></br>
Vous proposerez directement votre article via la plateforme. Le partenaire, apr�s lecture de votre article, est libre de l'accepter ou non. L'article sera cependant affich� <b>sous forme d'une image</b> afin d'�viter le vol de ce dernier. Il ne sera converti en .doc que si le partenaire accepte sa publication.
</br></br>
Vous disposez de 15 jours pour accepter ou non une proposition d'articles. Si vous ne r�pondez pas sous ce d�lai, votre site passera inactif et ne vous ne pourrez plus recevoir de nouvelles propositions. Ceci afin de toujours garantir une liste de sites actifs.</br></br>

<h3>Le Plugin Wordpress</h3>
Le Plugin, toujours en cours de construction, vous permettra de recevoir les articles directement sur votre site Internet et de les valider ou non sans avoir � passer via l'interface Ereferer.
        </p>
        <!-- Pagination -->

        <!-- End pagination -->




    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>