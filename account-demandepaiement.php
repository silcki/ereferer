<?PHP
@session_start();

$a = 1;
$b = 1;
$page = 1;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        ?>
        <div class="three-fourth">
            <div class="one">

                <div class="permalink">
                    <h4><a nohref="#">Faire une demande de paiement</a></h4>
                </div>

                <?PHP
                if (isset($_GET['statut']) && $_GET['statut'] != "") {
                    ?>
                    <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                        <p>
                            <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                            <?PHP echo $_SESSION['alertLogincontent']; ?>
                        </p>
                    </div>
                <?PHP } if ($solde >= $limitePaiement && !refererHavePay() && refererCanAsk()) { ?>
                    <p>
                        Veuillez saisir le montant que vous souhaiter retirer de votre compte.<br/>
                        PS : Vous ne pouvez faire qu'une seule demande de paiement par mois.<br/>
                    </p>
                    <form action="compte/ajouter/demandepaiement.html" method="post" style="margin-top:0px;float:left;" enctype="multipart/form-data">
                        <!--<button class="search-btn"></button>-->
                        <fieldset style="">
                            <!--<legend class="color">Connexion</legend>-->
                            <input name="montant" class="search-field" type="text" style="margin-bottom:5px;width:50px;float:left;margin-right:5px;" onkeyup="ve1(this);"/><?PHP echo $_SESSION['devise']; ?>
                            <br/>
                        </fieldset>


                        <input type="submit" class="button color small round" value="Envoyer" style="color:white;"/>

                    </form>
                    <?PHP
                } else {
                    if (refererHavePay()) {
                        ?>
                        <p style="color:red;">
                            Vous avez d�j� une demande de paiement en attente de validation. 
                        </p>
                        <?PHP
                    } else {
                        if (!refererCanAsk()) {
                            $jours = refererCanAsk(1) . " Jour(s)";
                            ?>
                            <p style="color:red;">
                                Vous ne pouvez plus faire de demande de paiement avant <?PHP echo $jours; ?>; 
                            </p>
                            <?PHP
                        } else {
                            ?>
                            <p style="color:red;">
                                Vous ne pouvez faire une demande de paiement que lorsque le solde de votre compte est d'au moins : <?PHP echo $limitePaiement . " " . $_SESSION['devise']; ?>
                            </p>
                            <?PHP
                        }
                    }
                }
                ?>
            </div>


            <!-- Pagination -->

            <!-- End pagination -->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>