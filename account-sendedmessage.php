<?PHP
@session_start();

$a = 3;
$b = 3;
$page = 1;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">
            <?PHP
            $getMessage = Functions::getMessageSended($_SESSION['connected']['id']);
            $count = count($getMessage);

            include("pages.php");

            if ($count > 0) {
                ?>

                <!--<h4>Jquery Accordion</h4>-->
                <ul class="accordion">
                    <?PHP
                    $style = "";
                    while ($limit1 <= $limit2) {
                        if ($getMessage[$limit1]['viewSender'] == 1) {
                            $style = "";
                            ?>
                            <li class="accordion-item">
                                <div class="accordion-switch">
                                    <span class="togglegfx"></span>
                                    <?PHP
                                    echo '<span style="' . $style . '">' . $getMessage[$limit1]['receiver'] . '</span>';
                                    ?>

                                    <a href="compte/supprimer/message.html?id=<?PHP echo $getMessage[$limit1]['id']; ?>" style="float:right;margin-right:5px;" onclick="return confirmation();">
                                        <i class="icon-trash"></i> Supprimer
                                    </a>
                                </div>
                                <div class="accordion-content" style="display: none;">
                                    <p>
                                        <?PHP echo nl2br(stripslashes($getMessage[$limit1]['content'])); ?>
                                    </p>
                                </div>
                            </li>
                            <?PHP
                        }
                        $limit1++;
                    }
                    ?>
                </ul>
            </div>

            <!-- Pagination -->
            <?PHP
            if ($nbre_result > pagination) {
                echo $list_page;
            }
        } else {
            ?>
            <div align="center" width="500" style="width:700px;margin:auto;">
                <br/>
                <br/>
                <h1>
                    Aucun message dans ce dossier.
                </h1>
            </div>

            <?PHP
        }
        ?>
        <!-- End pagination -->


    </div>
</div>


<?PHP
include("files/includes/bottomBas.php")
?>