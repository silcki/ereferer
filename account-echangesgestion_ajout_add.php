<?php

if (isset($inside) && $inside == 1) {
    $errors = [];
    if (empty($_POST['url'])) {
        $errors[] = "Veuillez entrer une URL !";
    } else {
        if (!filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
            $errors[] = "L'URL renseign�e n'est pas valide !";
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $_POST['url']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        if (!$result = curl_exec($ch)) {
            $errors[] = "L'URL est inaccessible";
        }

        curl_close($ch);
    }

    $arr_url = parse_url($_POST['url']);
    $domain = str_ireplace("www.", "", $arr_url["host"]);
    $accessKeyId = "AKIAINKJPEMKDY4GD3HA";
    $secretAccessKey = "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";
    $urlInfo = new UrlInfo($accessKeyId, $secretAccessKey, $domain, $_POST['url']);
    $infos = $urlInfo->getUrlInfo();

    $dc = $infos['domain_creation'];
    $age = floatval(dateDifference(date("Y-m-d"), $dc, '%y.%m'));

    $domref = $infos['dom_ref'];
    $tf = $infos['tf'];

    $retour = $echangeSites->isDomainExists($domain, $_SESSION['connected']['id']);

    if ($retour) {
        $errors[] = "Vous avez d�j� ajout� ce site, veuillez entrer une autre URL !";
    }

    //check file (screenshot)

    $target_dir = "files/screenshot/";
    $target_file = $target_dir . md5($domain) . ".jpg";
    $uploadOk = 1;
    $imageFileType = pathinfo($_FILES["screenshot"]["name"], PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image

    if ($_FILES["screenshot"]["tmp_name"] != "") {
        $check = getimagesize($_FILES["screenshot"]["tmp_name"]);
        if ($check !== false) {

            $uploadOk = 1;
        } else {
            $errors[] = "Le fichier n'est pas une image";
            $uploadOk = 0;
        }


        if ($_FILES["fileToUpload"]["size"] > 1024000) {
            $errors[] = "L'image envoy�e est trop volumineuse (1 mo max)";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "JPG" && $imageFileType != "JPEG") {
            $errors[] = "L'image envoy�e n'est pas au format jpeg (.jpg / .jpeg)";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {

        } else {
            if (move_uploaded_file($_FILES["screenshot"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/" . $target_file)) {
                error_reporting(E_ALL);

                function resizeImage($filename, $max_width, $max_height)
                {
                    list($orig_width, $orig_height) = getimagesize($filename);

                    $width = $orig_width;
                    $height = $orig_height;

                    # taller
                    if ($height > $max_height) {
                        $width = ($max_height / $height) * $width;
                        $height = $max_height;
                    }

                    # wider
                    if ($width > $max_width) {
                        $height = ($max_width / $width) * $height;
                        $width = $max_width;
                    }

                    $image_p = imagecreatetruecolor($width, $height);

                    $image = imagecreatefromjpeg($filename);

                    imagecopyresampled($image_p, $image, 0, 0, 0, 0,
                        $width, $height, $orig_width, $orig_height);

                    //return $image_p;
                    //file_put_contents($filename, $image_p);
                    imagejpeg($image_p, $filename);
                }

                resizeImage($_SERVER['DOCUMENT_ROOT'] . "/" . $target_file, 300, 400);

                error_reporting(0);

            } else {
                $errors[] = "Il y'a eu une erreur � l'envoi de votre fichier image.";
            }

        }
    }

    $execution = $echangeSites->getUserSites($_SESSION['connected']['id']);
    $sitesbycat = array();

    foreach ($execution as $retour) {
        if ($retour['cat_id1']) $sitesbycat[$retour['cat_id1']]++;

        if ($retour['cat_id2'] != $retour['cat_id1'] && $retour['cat_id2']) {
            $sitesbycat[$retour['cat_id2']]++;
        }
    }

    $maxcred = Functions::creditAlgo($tf, $domref, $age);

    if (empty($_POST['credit'])) {
        $errors[] = "Veuillez entrer un nombre de cr�dit !";
    } else {
        if ($_POST['credit'] > $maxcred) {
            $errors[] = "Veuillez entrer un nombre de cr�dit inferieur ou �gal � " . Functions::creditAlgo($tf, $domref, $age);
        }
    }

    if ($_POST['cat_id1'] == 0) {
        $errors[] = "Veuillez choisir au minimum une cat�gorie !";
    }

    if ($sitesbycat[$_POST['cat_id1']] >= 5 || $sitesbycat[$_POST['cat_id2']] >= 5) {
        $errors[] = "Vous ne pouvez pas inscrire plus de 5 sites dans la m�me cat�gorie.";
    }

    if (empty($_POST['tags'])) {
        $errors[] = "Veuillez entrer un ou plusieurs tags !";
    }

    if (empty($_POST['nb_mots_max'])) {
        $errors[] = "Veuillez entrer un nombre de mots minimum";
    }

    if (empty($_POST['nb_liens_max']) && $_POST['nb_liens_max'] < '1') {
        $errors[] = "Veuillez entrer un nombre de liens maximum (superieur � 1)";
    }

    if ($_POST['nb_images_min'] === '0') {
    } else {
        if (empty($_POST['nb_images_min'])) $errors[] = "Veuillez entrer un nombre d'images minimum";
    }

    if (empty($errors)) {
        //GET URL INFOS :
        $show = false;

        //GET ALL THE METRICS !

        //process add

        if (isset($_POST['masquer'])) {
            $masquer = 1;
        } else {
            $masquer = 0;
        }

        if (isset($_POST['accept_eref'])) {
            $eref = 1;
        } else {
            $eref = 0;
        }
        if (isset($_POST['accept_web'])) {
            $web = 1;
        } else {
            $web = 0;
        }
        if (isset($_POST['accept_self'])) {
            $self = 1;
        } else {
            $self = 0;
        }


        if (isSu()) {
            $ageM = !empty($_POST['age_m']) ? $_POST['age_m']:date('Y-m-d');
            $stmt = $dbh->prepare('INSERT INTO echange_sites (`id`, `user_id`, `url`, `urlshow`, `domain`, `tf`, `tf_m`, `alexa_m`, `age_m`, `dom_ref_m`, `alexa`, `age`, `dom_ref`, `masquer`, `credit`, `status`, `active`, `cat_id1`, `cat_id2`, `tags`, `nb_mots_max`, `nb_liens_max`, `nb_images_min`, `regle`,`accept_eref`,`accept_web`,`accept_self`, `api_key`)
                                            VALUES(null,
                                                    :user_id,
                                                    :url,
                                                    :urlshow,
                                                    :domain,
                                                    ' . $dbh->quote($infos['tf']) . ',
                                                    :tf_m,
                                                    :alexa_m,
                                                    ' . $dbh->quote($ageM) . ',
                                                    :dom_ref_m,
                                                    :alexa,
                                                    ' . $dbh->quote($infos['domain_creation']) . ',
                                                    ' . $dbh->quote($infos['dom_ref']) . ',
                                                    :masquer,
                                                    :credit,
                                                    1,
                                                    1,
                                                    :cat_id1,
                                                    :cat_id2,
                                                    :tags,
                                                    :nb_mots_max,
                                                    :nb_liens_max,
                                                    :nb_images_min,
                                                    :regle,
                                                    ' . $dbh->quote($eref) . ',
                                                    ' . $dbh->quote($web) . ',
                                                    ' . $dbh->quote($self) . ',
                                                    :api_key
                                                    )');
        } else {
            $stmt = $dbh->prepare('INSERT INTO echange_sites (`id`, `user_id`, `url`, `urlshow`, `domain`, `tf`, `alexa`, `age`, `dom_ref`, `masquer`, `credit`, `status`, `active`, `cat_id1`, `cat_id2`, `tags`, `nb_mots_max`, `nb_liens_max`, `nb_images_min`, `regle`,`accept_eref`,`accept_web`,`accept_self`, `api_key`)
                                            VALUES(null,
                                                    :user_id,
                                                    :url,
                                                    :urlshow,
                                                    :domain,
                                                    ' . $dbh->quote($infos['tf']) . ',
                                                    :alexa,
                                                    ' . $dbh->quote($infos['domain_creation']) . ',
                                                    ' . $dbh->quote($infos['dom_ref']) . ',
                                                    :masquer,
                                                    :credit,
                                                    1,
                                                    1,
                                                    :cat_id1,
                                                    :cat_id2,
                                                    :tags,
                                                    :nb_mots_max,
                                                    :nb_liens_max,
                                                    :nb_images_min,
                                                    :regle,
                                                    ' . $dbh->quote($eref) . ',
                                                    ' . $dbh->quote($web) . ',
                                                    ' . $dbh->quote($self) . ',
                                                    :api_key
                                                    )');
        }
        if (isSu()) {
            $user_id = $_POST['user_id'];
        } else {
            $user_id = $_SESSION['connected']['id'];
        }

        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':url', $_POST['url']);
        $stmt->bindParam(':urlshow', $_POST['urlshow']);
        $stmt->bindParam(':domain', $domain);
        $stmt->bindParam(':masquer', $masquer);
        $stmt->bindParam(':credit', $_POST['credit']);
        $stmt->bindParam(':cat_id1', $_POST['cat_id1']);
        $stmt->bindParam(':cat_id2', $_POST['cat_id2']);
        $stmt->bindParam(':tags', $_POST['tags']);
        $stmt->bindParam(':nb_mots_max', $_POST['nb_mots_max']);
        $stmt->bindParam(':nb_liens_max', $_POST['nb_liens_max']);
        $stmt->bindParam(':nb_images_min', $_POST['nb_images_min']);
        $stmt->bindParam(':regle', $_POST['regle']);
        $stmt->bindParam(':alexa', $infos['alexa']);
        $stmt->bindParam(':api_key', $_POST['api_key']);


        if (isSu()) {
            $tfM = !empty($_POST['tf_m']) ? $_POST['tf_m']:0;
            $alexaM = !empty($_POST['alexa_m']) ? $_POST['alexa_m']:0;
            $domRefM = !empty($_POST['dom_ref_m']) ? $_POST['dom_ref_m']:0;

            $stmt->bindParam(':tf_m', $tfM);
            $stmt->bindParam(':alexa_m', $alexaM);
            $stmt->bindParam(':dom_ref_m', $domRefM);
        }

        $stmt->execute();


        //ajout cr�dit initiaux :

        $client = Functions::getUserInfos($_SESSION['connected']['id']);

        if (!$client['free_credits']) {
            if ($maxcred > 2) {
                $maxcred = 2;
            }

            $dbh->query("UPDATE `utilisateurs` SET  `free_credits` = 1, `credit` = (`credit` + " . $maxcred . ") WHERE `id` = " . $_SESSION['connected']['id']);
        }

        $notices[] = 'Le site � �t� ajout�.';


    } else {
        $show = true;
    }
}
?>