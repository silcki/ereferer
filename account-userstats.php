<?PHP
@session_start();


if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}


$a = 40;
$b = 8;

//var_dump($a,$b);

$page = 1;
include('files/includes/topHaut.php');

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Jobs.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Likes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionLikes.php';

$jobsModel = new Jobs();
$reportsModel = new RedactionReports($dbh);

$likesModel = new Likes();
$redactionLikesModel = new RedactionLikes();

if ( isReferer()) {
    if (!isset($_GET['id']) || empty($_GET['id'])) {
        $_GET['id'] = 0;
    }

    $months = array('Janv', 'F�vr', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Ao�t', 'Sept', 'Oct', 'Nov', 'D�c');
    $monthsLong = array('Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre', 'D�cembre');

    $monthsEngToFrench = array(
       'Jan' => 'Janv',
       'Feb' => 'F�vr',
       'Mar' => 'Mars',
       'Apr' => 'Avr',
       'May' => 'Mai',
       'Jun' => 'Juin',
       'Jul' => 'Juil',
       'Aug' => 'Ao�t',
       'Sep' => 'Sept',
       'Oct' => 'Oct',
       'Nov' => 'Nov',
       'Dec' => 'D�c'
    );

    ?>
    <style>
        #container{
            width: 1090px;
        }
        #main-navigation ul {
            margin-left:-30px;
        }
        .earned, .earnedThisMonth, .earnedThisUser, .earnedThisYear{
            display:block;
            min-width:60px;
            width:auto;
            font-weight:bold;
        }
        .earned{
            color:red;
        }
        .earnedThisUser{
            color:blue;
        }
        .earnedThisMonth{
            color:white;
        }
        .earnedThisYear{
            color:white;
        }
    </style>
    <!--breadcrumbs ends -->
    <div class="container user_stats_page">
        <div >
            <?PHP
             $writer_id = $idUser;

            // writer_status
            $is_annuaire_writer = $_SESSION['connected']['annuaire_writer'];
            $is_redaction_writer = $_SESSION['connected']['redaction_writer'];
            $is_corrector = isSuperReferer();

            // calculates last 12 months times
            $periods = array();
            $current_time = "Y-m-01"; // format for getting current months

            // ordered months (+French) labels
            $months_labels = array();

            for ($i = 11; $i >= 0; $i--) {
                $periods[$i]['start'] = date("Y-m-01", strtotime( date( $current_time )." -$i months")).' 00:00:00';
                $last_month_day = date('Y-m-t',strtotime($periods[$i]['start']));
                $periods[$i]['end'] = $last_month_day." 23:59:59";
                $month_name = date('M',strtotime($periods[$i]['start']));
                $months_labels[] = utf8_encode($monthsEngToFrench[$month_name]);
            }

            /*echo '<pre>';
            var_dump($months_labels);
            echo '</pre>';*/

            // stats for annuaire-module
            $annuaire_data = array();
            $annuaire_data_earnings = array();
            if ($is_annuaire_writer){
                $annuaire_data = $jobsModel->getWriterStatsData($periods,$writer_id);
                $annuaire_data_earnings = array_map(function($item){ return $item['writer_earn'];},$annuaire_data);
            }

            // stats for redaction-module
            $redaction_data = array();
            $redaction_data_earnings = array();
            if ($is_redaction_writer){
                $redaction_data = $reportsModel->getWriterStatsData($periods,$writer_id);
                $redaction_data_earnings = array_map(function($item){ return $item['writer_earn'];},$redaction_data);
           }

            // corrector earnings
            $corrector_data = array();
            $corrector_data_earnings = array();

            if ($is_corrector){
                $corrector_data = $reportsModel->getAdminWriterStatsData($periods,$writer_id);
                $corrector_data_earnings = array_map(function($item){ return $item['corrector_earn'];},$corrector_data);
            }

            // total sums
            $total_data_earnings = array();
            // annuaire
            if (!empty($annuaire_data_earnings)){
                $total_data_earnings = $annuaire_data_earnings;
            }

            // redaction as writer
            if (!empty($redaction_data_earnings)){
                foreach($redaction_data_earnings as $index=>$per_month){
                    $total_data_earnings[$index] = round($total_data_earnings[$index] + $per_month,2);
                }
            }

            // redaction as corrector
            if ($is_corrector && !empty($corrector_data_earnings)){
                foreach($corrector_data_earnings as $index=>$per_month){
                    $total_data_earnings[$index] = round($total_data_earnings[$index] + $per_month,2);
                }
            }

            /* Scracth (?) */
            /*
            array_unshift($total_data_earnings,0);
            array_unshift($months_labels,'');

            $total_data_earnings[] = 0;
            $months_labels[] = '';*/


            // check if it`s only one type writer
            $single_type_writer = false;
            if (!$is_corrector){
                $single_type_writer = ( ($is_annuaire_writer && !$is_redaction_writer) || (!$is_annuaire_writer  && $is_redaction_writer) );
            }

            ?>
            <h4><i class="icon-calendar"></i> &nbsp;Statistiques des 12 derniers mois</h4>
            <div class="chart_wrapper">
                <div class="ct-chart" id="writer_earn_chart"></div>
            </div>
            <div>
                <table class="total_counts">
                   <tr>
                      <th></th>
                      <th></th>
                      <?php foreach($months_labels as $month) : ?>
                           <td><?php echo utf8_decode($month);?></td>
                      <?php endforeach; ?>
                   </tr>

                   <?php if (!$single_type_writer) { ?>
                       <tr class="total_earns">
                        <td class="show_state"><input type="checkbox" class="checker" checked="checked" data-type="totals" /></td>
                        <td class="header">
                           Total Gains
                        </td>
                        <?php foreach($total_data_earnings as $total) : ?>
                           <td><?php echo number_format($total,2);?></td>
                        <?php endforeach; ?>
                       </tr>
                   <?php } ?>

                   <?php if ($is_annuaire_writer) { ?>
                        <tr class="annuaire_earns  header_top">
                            <td class="show_state"><input type="checkbox" class="checker" checked="checked" data-type="annuaire" /></td>
                            <td class="header">
                            Annuaire Gains
                            </td>
                        <?php foreach($annuaire_data_earnings as $total) : ?>
                            <td><?php echo number_format($total,2);;?></td>
                        <?php endforeach; ?>
                        </tr>
                        <tr class="annuaire_earns counts_row">
                            <td></td>
                            <td class="header">
                                Taches
                            </td>
                            <?php foreach($annuaire_data as $data) : ?>
                                <td><?php echo $data['task_done'];?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php } ?>
                    <?php if ($is_redaction_writer) { ?>
                        <tr class="redaction_earns header_top">
                            <td class="show_state"><input type="checkbox" class="checker" checked="checked" data-type="redaction_writer" /></td>
                            <td class="header">
                                Redaction Gains
                            </td>
                            <?php foreach($redaction_data_earnings as $total) : ?>
                                <td><?php echo number_format($total,2);?></td>
                            <?php endforeach; ?>
                        </tr>
                        <tr class="redaction_earns counts_row">
                            <td></td>
                            <td class="header">
                                Textes r�dig�s
                            </td>
                            <?php foreach($redaction_data as $data) : ?>
                                <td><?php echo $data['texts_count'];?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php } ?>
                    <?php if ($is_corrector) { ?>
                        <tr class="corrector_earns header_top">
                            <td class="show_state"><input type="checkbox" class="checker" checked="checked" data-type="corrector" /></td>
                            <td class="header">
                                Corrector Gains
                            </td>
                            <?php foreach($corrector_data_earnings as $total) : ?>
                                <td><?php echo number_format($total,2);?></td>
                            <?php endforeach; ?>
                        </tr>
                        <tr class="corrector_earns counts_row">
                            <td></td>
                            <td class="header">
                                Textes corrig�s
                            </td>
                            <?php foreach($corrector_data as $data) : ?>
                                <td><?php echo $data['texts_count'];?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>

        <?php
            // check current section
            $redaction_section = ( isset($_GET['redactor']) );
            $section_url =  '/compte/userstats.html';
            if (!$redaction_section){
                $section_url .= "?redactor=1";
            }

            $selected_like_state = (isset($_POST['like_state']))? intval($_POST['like_state']): -1;

            $annuaire_estimated_cnt = 0;
            if ($is_annuaire_writer){
                 $annuaire_like_state = $selected_like_state;
                 if ($redaction_section){
                    $annuaire_like_state = -1;
                 }
                 $annuaire_estimated_cnt = $likesModel->getEstimatedProjectsForWriter($writer_id,true,$annuaire_like_state);
            }
            $redaction_estimated_cnt = 0;
            if ($is_redaction_writer){
                $redaction_like_state = $selected_like_state;
                if (!$redaction_section){
                    $redaction_like_state = -1;
                }

                $redaction_estimated_cnt = $redactionLikesModel->getEstimatedProjectsForWriter($writer_id,true,$redaction_like_state);
            }

            $both_types_active = ( $is_annuaire_writer && $is_redaction_writer && ($annuaire_estimated_cnt > 0) && ($redaction_estimated_cnt > 0));
       ?>

        <?php if( $both_types_active ) { ?>
            <!-- "switch" -->
            <div class="section_url">
                <?php if (!$redaction_section){ ?>
                       <a class="redaction" href="<?php echo $section_url;?>">&nbsp;<i class="icon-file-text"></i> &nbsp;Textes de r�daction not�es par les clients (<?php echo $redaction_estimated_cnt;?>)</a>

                <?php }else{ ?>
                       <a class="annuaire"  href="<?php echo $section_url;?>">&nbsp;<i class="icon-pencil"></i> &nbsp;Soumissions not�es par les clients (<?php echo $annuaire_estimated_cnt;?>)</a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>

        <?php } ?>


        <?php
            // checking current showing
            $show_annuaire_section = ( ($is_annuaire_writer && ($annuaire_estimated_cnt > 0) && !$both_types_active )
                                       || ($both_types_active && !$redaction_section) );


            $show_redaction_section = ( ($is_redaction_writer  && ($redaction_estimated_cnt > 0) && !$both_types_active)
                                         || ($both_types_active && $redaction_section) );


            $pagination = 50;
            if ($show_annuaire_section){
                $count = $annuaire_estimated_cnt;
            }

            if ($show_redaction_section){
                $count = $redaction_estimated_cnt;
            }

            //if ($selected_like_state == 0){
                //$count = 0;
            //}

        ?>


        <?php if ( ($show_annuaire_section) || ($show_redaction_section) ) { ?>

            <div class="select_like_state">
                <select name="like_state" id="like_state">
                    <option value="-1" >Tous</option>
                    <option value="1" <?php if ($selected_like_state==1) {?> selected <?php } ?> >Aim�s</option>
                    <option value="0" <?php if ($selected_like_state==0) {?> selected <?php } ?> <?php ?>>Non aim�s</option>
                </select>
            </div>


        <?php
        // fucking code!!!
        include("pages.php");

        if ($nbre_result > $pagination) {
            echo $list_page . "<br/>";
        }

        // set current section url for js
        if ($show_annuaire_section)
            $current_json_url = '/compte/userstats.html';

        if ($show_redaction_section) {
            $current_json_url = '/compte/userstats.html?redactor=1&';
        }

        ?>
        <form action="" id="likes_form" method="post">
        <?php if ($show_annuaire_section){ ?>
            <?php
                $estimated_projects = $likesModel->getEstimatedProjectsForWriter($writer_id,false,$selected_like_state,$page,$pagination);
            ?>

            <div class="estimated">
                <table class="simple-table">
                    <tr>
                        <th>
                            <i class="icon-pencil"></i> &nbsp;Soumissions not�es par les clients
                        </th>
                    </tr>
                    <?php foreach($estimated_projects as $project){ ?>
                        <?php
                            $like_status = $project['like_status'];

                        ?>
                        <tr>
                           <td>
                                <div class="like_status <?php echo ($like_status)? 'liked' : 'unliked';?>">
                                    <i class="<?php echo ($like_status)? 'icon-thumbs-up' : 'icon-thumbs-down';?>"></i>
                                </div>
                               <div>
                                   <span class="title">Projet:</span>
                                   <?php echo str_replace(array('http://','/'),'',$project['lien']);?>
                               </div>
                               <div>
                                   <span class="title">Annuaire:</span>
                                   <?php echo str_replace(array('http://','/'),'',$project['annuaire']);?>
                               </div>
                               <div>
                                   <span class="title">Date:</span>
                                   <?php echo date('d-m-Y H:i:s',$project['writer_comment_created']);?>
                               </div>

                               <div>
                                   <span class="title">Votre texte:</span>
                                   <blockquote>
                                       <?php echo $project['writer_comment'];?>
                                   </blockquote>
                               </div>

                               <?php if ( !$like_status && !empty($project['webmaster_comment']) ){ ?>
                               <div class="unliked_comment">
                                   <span class="title">Commentaire du client:</span>
                                   <blockquote>
                                       <?php echo $project['webmaster_comment'];?>
                                   </blockquote>
                               </div>
                               <?php } ?>

                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>

        <?php } ?>

        <?php if ($show_redaction_section){ ?>
            <?php
            $estimated_projects = $redactionLikesModel->getEstimatedProjectsForWriter($writer_id,false,$selected_like_state,$page,$pagination);
            ?>
            <div class="redaction_estimated">
                <table class="simple-table">
                    <tr>
                        <th>
                            <i class="icon-file-text"></i> &nbsp;Textes de r�daction not�es par les clients
                        </th>
                    </tr>
                    <?php foreach($estimated_projects as $project){ ?>
                        <?php
                        $like_status = $project['like_status'];

                        ?>
                        <tr>
                            <td>
                                <div class="like_status <?php echo ($like_status)? 'liked' : 'unliked';?>">
                                    <i class="<?php echo ($like_status)? 'icon-thumbs-up' : 'icon-thumbs-down';?>"></i>
                                </div>

                                <div>
                                    <span class="title">Nom du projet</span> : <?php echo utf8_decode($project['title']);?>
                                </div>

                                <div>
                                    <span class="title">Consigne g�n�rale li�e au projet</span> :
                                    <blockquote><pre><?php echo utf8_decode($project['desc']);?></pre></blockquote>
                                </div>

                                <?php if (!empty($project['article_title'])){ ?>
                                    <div>
                                        <span class="title">Nom de l'article</span> : <?php echo utf8_decode($project['article_title']);?>
                                    </div>
                                <?php } ?>

                                <?php if (!empty($project['instructions'])){ ?>
                                    <div>
                                        <span class="title">Consigne sp�cifique � l'article</span> :
                                        <blockquote>
                                            <pre><?php echo utf8_decode($project['instructions']);?></pre>
                                        </blockquote>
                                    </div>
                                <?php } ?>

                                <div>
                                    <span class="title">Votre texte</span> :
                                    <span class="show_text"  data-project_id="<?php echo $project['id'];?>">
                                         <i class="icon-circle-arrow-down"></i> Voir
                                    </span>
                                    <div class="report_text" data-report_text_id="<?php echo $project['id'];?>">
                                        <blockquote><?php echo $project['report_text'];?></blockquote>
                                        <span class="hide_text"  data-project_id="<?php echo $project['id'];?>">
                                          <i class="icon-chevron-sign-up"></i> Masquer
                                        </span>
                                    </div>
                                </div>


                                <?php if ( !$like_status && !empty($project['webmaster_comment']) ){ ?>
                                    <div class="unliked_comment">
                                        <span class="title">Commentaire du client:</span>
                                        <blockquote>
                                            <?php echo $project['webmaster_comment'];?>
                                        </blockquote>
                                    </div>
                                <?php } ?>

                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>

        <?php } ?>
        </form>

        <?php
            if ($nbre_result > $pagination) {
                echo $list_page . "<br/>";
            }
        ?>

        <?php } ?>
    </div>



    <script>
        (function($) {

            $(document).ready(function(){

                // select like-state
                $('#like_state').chosen({
                    "disable_search": true
                });

                var finished_base_url = <?php echo json_encode($current_json_url);?>;
                var finished_like_state = <?php echo json_encode(intval($selected_like_state));?>;

                // filter finished by writer
                $('#like_state').on('change',function(){
                    var selected_state = $(this).val();
                    var current_url = window.location.href;

                    var hidden_writer_id = '<input type="hidden" name="like_state" value="'+selected_state+'" />';
                    $("#likes_form").append(hidden_writer_id);

                    if (selected_state != finished_like_state){
                        current_url = finished_base_url;
                    }

                    $('#likes_form').prop('action',current_url).submit();
                    console.warn(selected_state);
                });

                $('nav.pagination a').on('click',function(e){
                    e.preventDefault();
                    var selected_state = $('#like_state').val();
                    var href = $(this).prop('href');
                    var hidden_writer_id = '<input type="hidden" name="like_state" value="'+selected_state+'" />';
                    $("#likes_form").append(hidden_writer_id);

                    if (selected_state != finished_like_state){
                        href = finished_base_url;
                    }

                    $('#likes_form').prop('action',href).submit();
                });


                $('#like_state').on('change',function(){
                    var like_state = $(this).val();
                    console.log(like_state);
                });

                // chart with earnings
                var months_labels = <?php echo json_encode($months_labels);?>;

                var is_annuaire_writer = <?php echo json_encode($is_annuaire_writer);?>;
                var is_redaction_writer = <?php echo json_encode($is_redaction_writer);?>;
                var is_corrector = <?php echo json_encode($is_corrector);?>;

                var is_single_type_writer = <?php echo json_encode($single_type_writer);?>;


                var annuaire_data = [];
                var redaction_data = [];
                var corrector_data = [];
                var total_data = [];

                // show common if only writer has both type
                if (!is_single_type_writer){
                    total_data = <?php echo json_encode($total_data_earnings);?>;
                }

                if (is_annuaire_writer){
                    annuaire_data = <?php echo json_encode($annuaire_data_earnings);?>;
                }

                if (is_redaction_writer){
                    redaction_data = <?php echo json_encode($redaction_data_earnings);?>;
                }

                if (is_corrector){
                    corrector_data =  <?php echo json_encode($corrector_data_earnings);?>;
                }

                var data = {
                    labels: months_labels,
                    series: [{
                        name: 'annuaire_sum',
                        data:  annuaire_data
                    }, {
                        name: 'redaction_sum',
                        data:  redaction_data
                    }, {
                        name: 'corrector_sum',
                        data:  corrector_data
                    },{
                        name: 'total_sum',
                        data:  total_data
                    }]
                };

                var options = {
                    fullWidth: true,
                    width: 920,
                    //width: 300,
                    height: 450,
                    // Disable line smoothing
                    lineSmooth: false,
                    responsive: true,
                    padding: 0,
                    spanGaps: false,
                    chartPadding: {
                        left: 0,
                        right: 30,
                        bottom: 0,
                        top: 30
                    },
                    fontSize: 14,
                    axisX:{
                        offset: 50,
                        showLabeb: true,
                        showGrid: true,
                        scaleMinSpace: 40
                    },
                    axisY: {
                        offset: 50,
                        labelInterpolationFnc: function(value) {
                            return  value + ' $';
                        },
                        scaleMinSpace: 30
                    }
                };
                var chart_earnings = new Chartist.Line('.ct-chart',data,options);

                // show/hide type-earnings

                $('.checker').on('click',function(){
                    var state = $(this).is(':checked');
                    var type = $(this).data('type');

                    if (type=='annuaire'){
                        (state) ?  $('.ct-series-a').show() : $('.ct-series-a').hide();
                    }

                    if (type=='redaction_writer'){
                        (state) ?  $('.ct-series-b').show() : $('.ct-series-b').hide();
                    }

                    if (type=='corrector'){
                        (state) ?  $('.ct-series-c').show() : $('.ct-series-c').hide();
                    }

                    if (type=='totals'){
                        (state) ?  $('.ct-series-d').show() : $('.ct-series-d').hide();
                    }

                    console.log(state,' ',type);
                });


                // likes stats

                $(".redaction_estimated .show_text").on('click',function(){
                    var project_id = $(this).data('project_id');
                    $('[data-report_text_id="'+project_id+'"]').slideDown('fast');
                });

                $(".redaction_estimated .hide_text").on('click',function(){
                    var project_id = $(this).data('project_id');
                    $('[data-report_text_id="'+project_id+'"]').slideUp('fast');
                });

            });

        })(jQuery);
    </script>


<?PHP
}
include("files/includes/bottomBas.php")
?>