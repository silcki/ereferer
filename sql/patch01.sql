ALTER TABLE `utilisateurs` CHANGE `adresse` `adresse` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `telephone` `telephone` INT(11) NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `societe` `societe` VARCHAR(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `ville` `ville` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `siteweb` `siteweb` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `codepostal` `codepostal` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `telephone` `telephone` VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `avatar` `avatar` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `tentatives` `tentatives` INT(20) NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `naissance` `naissance` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `connected` `connected` INT(11) NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `credit_auto` `credit_auto` INT(11) NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `lastPayment` `lastPayment` INT(30) NULL DEFAULT NULL, CHANGE `AmountLastPayment` `AmountLastPayment` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `free_credits` `free_credits` INT(11) NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `trusted` `trusted` INT(11) NULL DEFAULT NULL;