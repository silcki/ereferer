ALTER TABLE `redaction_projects` CHANGE `img_count` `img_count` TINYINT(4) NOT NULL DEFAULT '0';
ALTER TABLE `redaction_projects` CHANGE `links_array` `links_array` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `redaction_projects` CHANGE `prop_id` `prop_id` INT(11) NOT NULL DEFAULT '0';