ALTER TABLE `echange_sites` ADD `website_url` TEXT NULL DEFAULT NULL AFTER `api_key`, ADD `plugin_url` TEXT NULL DEFAULT NULL AFTER `website_url`;

DROP TABLE IF EXISTS `echange_sites_categories`;
CREATE TABLE IF NOT EXISTS `echange_sites_categories` (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `echange_sites_id` int(11) UNSIGNED NOT NULL,
  `ext_id` int(12) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `echange_sites_id` (`echange_sites_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `echange_sites_categories` CHANGE `echange_sites_id` `echange_sites_id` INT(11) NOT NULL;
ALTER TABLE `echange_sites_categories` ADD FOREIGN KEY (`echange_sites_id`) REFERENCES `echange_sites`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
