ALTER TABLE `requestpayment` CHANGE `answerTime` `answerTime` INT(20) NOT NULL DEFAULT '0';
ALTER TABLE `requestpayment` CHANGE `facture` `facture` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `paypal_address` `paypal_address` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `lastIP` `lastIP` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `styleAdmin` `styleAdmin` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `country` `country` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `utilisateurs` CHANGE `numtva` `numtva` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;