DROP TABLE IF EXISTS `redaction_reports_categories`;
CREATE TABLE IF NOT EXISTS `redaction_reports_categories` (
  `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_id` int(11) UNSIGNED NOT NULL,
  `ext_id` int(12) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `redaction_reports_categories` CHANGE `project_id` `project_id` INT(11) NOT NULL;
ALTER TABLE `redaction_reports_categories` ADD FOREIGN KEY (`project_id`) REFERENCES `redaction_projects`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;