<?PHP
@session_start();

$a = 1;
$b = 11;
$page = 1;
include('files/includes/topHaut.php');

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/AnnuaireViewHelper.php';

$viewHelper = new AnnuaireViewHelper();
?>

<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        ?>
        <div style="position:absolute;top:0px;left:0;margin-left:220px;margin-top:50px;z-index: 9999">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
            }
            ?>
        </div>
        <div class="three-fourth" style="">

            <h4>R�pertoires d'annuaires.</h4>
            <?PHP
            $annuairesTous = Functions::getAnnuaireAll();
            $annuairesUserList = Functions::getAnnuaireListeList();

            // hide soft-deleted rows
            $annuairesList = array();
            $cnt_all = count($annuairesUserList);
            $pos = 1;
            for($i=1;$i<=$cnt_all;$i++){
                if($annuairesUserList[$i]['is_deleted'])
                    continue;
                else{
                    $annuairesList[$pos] = $annuairesUserList[$i];
                    $pos++;
                }
            }
            $count = count($annuairesList);

            include("pages.php");


            if ($count > 0) {
                ?>
                <a href="./compte/ajout/listannuaire.html" class="button color small round" style="color:white;float:right;margin:5px 0px 5px 10px;">Cr�er une liste</a>
                <table class="simple-table list_annuaire_table">
                    <tbody>
                        <tr>
                            <th>
                                Libell�
                            </th>
                            <th class="words_count">
                                Nombre<br/>de mots
                            </th>
                            <th>
                                Cr�ation
                            </th>
                            <th>
                                Annuaires
                            </th>
                            <th>
                                Actions
                            </th>


                        </tr>
                        <?PHP
                        while ($limit1 <= $limit2) {
                            $aEdit = 0;
                            foreach ($annuairesFromListNew as $annuaireThatId) {
                                if (!isset($annuairesTous[$annuaireThatId]) || (isset($annuairesTous[$annuaireThatId]) && (!Functions::checkUrlFormat($annuairesTous[$annuaireThatId]['annuaire']) || $annuairesTous[$annuaireThatId]['active'] == 0))) {
                                    $aEdit++;
                                    unset($annuairesFromListNew[array_search($annuaireThatId, $annuairesFromListNew)]);
                                }
                            }
                            $contentThatListImploded = "";
                            if ($aEdit > 0) {
                                $contentThatListImploded = implode(";", $annuairesFromListNew);
                                $contentThatListImploded .= ";";
                                if ($contentThatListImploded != ";") {
                                    $requete = "UPDATE annuaireslist SET annuairesList = '" . $contentThatListImploded . "' WHERE id=" . intval($annuairesList[$limit1]['id']);
                                    $execution = $dbh->query($requete);
                                    $annuairesList[$limit1]['annuairesList'] = $contentThatListImploded;
                                }
                            }

                            $filter_config = $annuairesList[$limit1]['filter_config'];

                            ?>
                            <tr>
                                <td class="<?php if (!empty($filter_config)){ ?> has_filter_config <?php } ?>">
                                    <div class="list_title"><?PHP echo $annuairesList[$limit1]['libelle']; ?></div>
                                    <?php if (!empty($filter_config) && isWebmaster()) { ?>
                                        <?php
                                            $filter_config_html = $viewHelper->annuaireListFilterConfig($filter_config);
                                            if (!empty($filter_config)){
                                        ?>
                                            <div class="list_config">
                                                <?php echo $filter_config_html;?>
                                            </div>
                                            <?php } ?>
                                    <?php } ?>

                                </td>
                                <td class="words_count">
                                    <?php $words_count = $annuairesList[$limit1]['words_count'];  ?>
                                    <?php if ($words_count){?>
                                            <?php echo $words_count;?>
                                    <?php }else {?>
                                            ---
                                    <?php } ?>
                                </td>
                                <td>
                                    <?PHP echo date("d/m/Y", $annuairesList[$limit1]['created']); ?>
                                </td>
                                <td>
                                    <?PHP echo count(explode(';', $annuairesList[$limit1]['annuairesList'])) - 1; ?> Annuaire(s)
                                </td>
                                <td>
                                    <a href="compte/modifier/listannuaire.html?id=<?PHP echo $annuairesList[$limit1]['id']; ?>"  ><i class="icon-edit"></i> Modifier le Libell�</a><br/>
                                    <a href="compte/voirliste.html?id=<?PHP echo $annuairesList[$limit1]['id']; ?>"  ><i class="icon-eye-open"></i> Voir la liste</a><br/>
                                    <a href="compte/modifier/listannuairecontent.html?id=<?PHP echo $annuairesList[$limit1]['id']; ?>"  ><i class="icon-edit"></i> Modifier la liste</a><br/>
                                    <a href="compte/modification/dupliquerannuairecontent.html?id=<?PHP echo $annuairesList[$limit1]['id']; ?>"  ><i class=" icon-copy"></i> Dupliquer la liste</a><br/>
                                    <a href="compte/supprimer/listannuaire.html?id=<?PHP echo $annuairesList[$limit1]['id']; ?>" onclick="return confirmation();" ><i class="icon-trash"></i> supprimer la liste</a>
                                </td>
                                                                                                           <!--                            <td>
                                                                                                                                            <a href="" ></a>
                                                                                                                                        </td>-->

                            </tr>
                            <?PHP
                            $limit1++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- Pagination -->
                <?PHP
                if ($nbre_result > pagination) {
                    echo $list_page;
                }
            } else {
                ?>
                <div align="center" width="500" style="width:700px;margin:auto;">
                    <h1>Aucun r�sultat � afficher.</h1>
                    <p class="p_menu" style="width:150px;"><a href="./compte/ajout/listannuaire.html" class="a_menu button color <?PHP
                                                              if ($b == 1) {
                                                                  echo "active";
                                                              }
                                                              ?>"> Ajouter une liste</a></p>
                </div>

                <?PHP
            }
            ?>
            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>