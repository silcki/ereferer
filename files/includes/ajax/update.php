<?php

include("../config/configuration.php");
include("../functions.php");


require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Projects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/MailProvider.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Likes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionLikes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/HTML.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';


$error = "";
$return = "";

if (isset($_POST['type']) && !empty($_POST['type'])) {





    /*
    *  Update webmaster start-dates REDACTION waiting-projects - Drag-n-drop sortable behaviour
    */

    if ($_POST['type'] == "redaction_projects_webmaster_start_dates") {
        if (isset($_POST['projects']) && (!empty($_POST['projects'])) ){
            $projects = $_POST['projects'];
            $projectModel = new RedactionProjects($dbh);
            $response = $projectModel->updateProjectStartDates($projects);
        }

        header('Content-Type: application/json');
        $response = json_encode($response);
        echo $response;
        die;
    }


    /*
     *  Update webmaster start-dates waiting-projects - Drag-n-drop sortable behaviour
     */

    if ($_POST['type'] == "projects_webmaster_start_dates") {
        if (isset($_POST['projects']) && (!empty($_POST['projects'])) ){
            $projects = $_POST['projects'];
            $projectModel = new Projects($dbh);
            $response = $projectModel->updateProjectStartDates($projects);
        }

        header('Content-Type: application/json');
        $response = json_encode($response);
        echo $response;
        die;
    }

    /*
     *  Writers/admin update project state
     */
    if ($_POST['type'] == "project_update_current_state") {
        $project_id = intval($_POST['project_id']);
        $user_id = intval($_POST['user_id']);
        $user_type = $_POST['user_type'];
        if ($project_id){
            $projectModel = new Projects($dbh);
            $response = $projectModel->updateProjectState($project_id,$user_id,$user_type,true);

            if (isset($response['writer_task_status'])){
                $response['writer_task_status'] = utf8_encode($response['writer_task_status']);
            }
        }

        header('Content-Type: application/json');
        $response =json_encode($response);
        echo $response;
        die;
    }



    // redaction
    // set project as not viewed by webmaster
    if ($_POST['type'] == "redaction_project_not_viewed") {
        $project_id = intval($_POST['project_id']);
        if ($project_id){
            $redactionProjects = new RedactionProjects($dbh);
            $redactionProjects->setViewed($project_id,0);
            $return = 'ok';
        }
    }

    // readaction
    if ($_POST['type'] == "redaction_creating_project") {
        $RedactionProjects = new RedactionProjects($dbh);
        unset($_POST['type']);

        if (isset($_POST['form_data']['edition'])){
            $data = $_POST['form_data'];
            $result = $RedactionProjects->insert($data); // insert with flag "edition" => updating
        } else {
            $result = $RedactionProjects->createProject($_POST);
        }

        if ($result) {
            // save transaction when creating "green" project
            $transaction = new Transaction($dbh, $_POST['form_data']['webmaster_id']);
            $transaction->addTransaction("Achat de ". $_POST['form_data']['texts_count'] ." article(s) : Nom du projet : " . $_POST['form_data']['title'], 0, $_POST['form_data']['amount']);
        }

        $return = $result ? 'ok':'fail';
    }

    if ($_POST['type'] == "datedemarrage") {
        $error = "";
        $currentProject = Functions::getCommandeByID($_POST['projet']);

        if ($currentProject) {
            if (!empty($_POST['datedemarrage']) && (!preg_match('#-#', $_POST['datedemarrage']) || strtotime($_POST['datedemarrage']) < strtotime(date("d-m-Y", time())))) {
                $error .= "Veuillez selectionner une date de demarrage valide (dd-mm-yyyy).<br/>";
            }

            if ($error == "") {

                $timer = Functions::timeToSet(strtotime($_POST['datedemarrage']));
                $timerAffected = $timer;

                $freq = Functions::getFrequence($currentProject['frequence']);
                $frequenceTache = $freq[0];
                $varNew = $timer . "|" . $frequenceTache;

                $ok = $dbh->query('UPDATE projets SET email = "' . $varNew . '", budget = "' . $timer . '", adminApprouveTime = "' . $timer . '", affectedTime = "' . $timerAffected . '" WHERE id=' . $currentProject['id']);
                if ($ok) {
                    $return = "1";
                }
            } else {
                $return = date('d-m-Y', $currentProject['adminApprouveTime']);
            }
        }
    }

    if ($_POST['type'] == "referenceur") {
        $error = "";
        $currentProject = Functions::getCommandeByID($_POST['projet']);

        if ($currentProject) {
            if (empty($_POST['affectedTOO']) || $_POST['affectedTOO'] == 0) {
                $error .= "Veuillez choisir un r�f�renceur Valide pour ce projet.<br/>";
            }

            if ($error == "") {
                $timers = time();
                $timer = Functions::timeToSet($timers);

//                if ($currentProject['affectedTO'] != $_POST['affectedTOO']) {

                $freq = Functions::getFrequence($currentProject['frequence']);
                $frequenceTache = $freq[0];
                $varNew = $timer . "|" . $frequenceTache;

                $ok = $dbh->query('UPDATE projets SET email = "' . $varNew . '", budget = "' . $timer . '",  affectedTO = "' . $_POST['affectedTOO'] . '", affectedBY = "1", affectedTime = "' . $timer . '" WHERE id=' . $currentProject['id']);
                if ($ok) {
                    if (isSuperReferer()) {
                        $return = '<a href="#">' . Functions::getfullname($_POST['affectedTOO']) . '</a>';
                    } else {
                        $return = '<a href="compte/modifier/profil.html?id=' . $_POST['affectedTOO'] . '">' . Functions::getfullname($_POST['affectedTOO']) . '</a>';
                    }
                }
//                }
            }
        }
    }

    // reaffect redaction projects
    if ($_POST['type'] == "redaction_referenceur") {
        $error = "";
        $project_id = intval($_POST['project_id']);
        $model = new RedactionProjects($dbh);
        $project = $model->getProject($project_id);
        if (!empty($project)) {

            if (empty($_POST['affectedTOO']) || $_POST['affectedTOO'] == 0) {
                $error .= "Veuillez choisir un référenceur Valide pour ce projet.<br/>";
            }

            if (empty($error)) {
                $writer_id = intval($_POST['affectedTOO']);
                $now = time();

                $ok = $dbh->query('UPDATE redaction_projects
                         SET affectedTO = '.$writer_id.',
                             affected_time = FROM_UNIXTIME('.$now.'),
                             status = "progress"
                         WHERE id='.$project_id);
                if (isSuperReferer()) {
                    $return = '<a href="#">' . Functions::getfullname($_POST['affectedTOO']) . '</a><br/>';
                } else {
                    $return = '<a target="_blank" href="compte/modifier/profil.html?id=' . $_POST['affectedTOO'] . '">' . Functions::getfullname($_POST['affectedTOO']) . '</a><br/>';
                }

                if (!$ok) {
                    header('HTTP/1.1 400 Bad request', true, 400);
                    header('Content-Type: application/json');
                    $response =json_encode([
                        'result' => false,
                        'error' => 'Not updated',
                        'pdo_error' => $dbh->errorInfo(),
                    ]);
                    echo $response;
                    die;
                }

            } else {
                header('HTTP/1.1 400 Bad request', true, 400);
                header('Content-Type: application/json');
                $response =json_encode([
                    'result' => false,
                    'error' => $error
                ]);
                echo $response;
                die;
            }
        }
    }


    if ($_POST['type'] == "pagination") {

        $ok = $dbh->query('UPDATE parametres SET valeur = "' . $_POST['paginationExpress'] . '" WHERE id=27');
        if ($ok) {
            $return = "1";
        }
    }


    /*
     *  Annuaire Like/unlike
     *
     */

    if ($_POST['type'] == "writer_likes") {
        // purifier
        $hmtlHelper = new HTML;

        $data = $_POST;

        if (isset($data['unliked_comment']) && !empty($data['unliked_comment'])){
            $unliked_comment = $hmtlHelper->purifyParams($data['unliked_comment']);
        }

        //error_reporting(E_ERROR);

        $result = $dbh->query('DELETE FROM `writers_likes` WHERE project_id ='.$data['project_id'].' AND
                                                       annuaire_id ='.$data['annuaire_id'].' AND
                                                       writer_id='.$data['writer_id']);

        if ($result){

            $sql ='INSERT INTO `writers_likes` VALUES (:project_id,
                                                       :annuaire_id,
                                                       :writer_id,
                                                       :value,
                                                       :unliked_comment,
                                                       :clicked_time)';

            $clicked_time = time();
            $stmt = $dbh->prepare($sql);
            $stmt->bindParam(":project_id",$data['project_id'],PDO::PARAM_INT);
            $stmt->bindParam(":annuaire_id",$data['annuaire_id'],PDO::PARAM_INT);
            $stmt->bindParam(":writer_id",$data['writer_id'],PDO::PARAM_INT);
            $stmt->bindParam(":value",$data['value'],PDO::PARAM_INT);
            $stmt->bindParam(":unliked_comment",$unliked_comment,PDO::PARAM_STR);
            $stmt->bindParam(":clicked_time",$clicked_time,PDO::PARAM_INT);

            $ok = $stmt->execute();
        }
        if ($ok) {
            // sending message to writers about not liked text
            if(!$data['value']){
                $type_user = 3; // writer
                $comments = Functions::getAllCommentaire($data['project_id'],$data['annuaire_id']);
                $text =  $comments[$type_user]['commentaire'];
                $subject = "Le client n'aime pas votre texte";
                $message = "Juste pour vous informer que le client n'aime pas la description suivante : <br/>".$text;
                $writer_id = $data['writer_id'];
                $setUser = $dbh->query('INSERT INTO messages VALUES("", "' . addslashes($subject) . '", "' . addslashes($message) . '", "1", "' . $writer_id . '", "1", "1", "1", "' . time() . '")');
            }
            $return = "1";
        }


        // checking worse writer & changing writers
        if ($ok && !$data['value']){
            $project_id = $_POST['project_id'];
            $writer_id = $_POST['writer_id'];

            $likesModel = new Likes();
            $worse_ids = $likesModel->getWorseWritersByProjectID($project_id);
            //var_dump('writer_id',$writer_id);
            //var_dump('worse_ids',$worse_ids);

            if(!empty($worse_ids) && in_array($writer_id,$worse_ids)){
                $new_writer_id = 0;

                // check best writer
                $best_writers_ids = $likesModel->getBestWritersByProjectID($project_id);
                //var_dump('best_ids',$best_writers_ids);
                if(!empty($best_writers_ids)){
                    $new_writer_id = $best_writers_ids[array_rand($best_writers_ids)];
                }

                // get random writer
                if(!$new_writer_id){
                    $not_in_sql = implode(',',$worse_ids);
                    $result = $dbh->query('SELECT id FROM utilisateurs WHERE typeutilisateur = 3 AND id NOT IN ('.$not_in_sql.')');
                    $writers_ids = $result->fetchAll(PDO::FETCH_COLUMN,0);
                    $new_writer_id = $writers_ids[array_rand($writers_ids)];

                }

                //var_dump('new_writer_id',$new_writer_id);
                // setting new writer_id
                $currentProject = Functions::getCommandeByID($project_id);

                $timers = time();
                $timer = Functions::timeToSet($timers);

                $freq = Functions::getFrequence($currentProject['frequence']);
                $frequenceTache = $freq[0];
                $varNew = $timer . "|" . $frequenceTache;

                $ok = $dbh->query('UPDATE projets SET email = "' . $varNew . '", budget = "' . $timer . '",  affectedTO = "' . $new_writer_id. '", affectedBY = "1", affectedTime = "' . $timer . '" WHERE id=' . $currentProject['id']);

                if($ok){
                    $return = "1";
                }

            }
        }
    }


    /*
     * Redaction like/unkike
     *
     */
    if ($_POST['type'] == "redaction_writer_likes") {
        $likesModel = new RedactionLikes();

        // purifier
        $hmtlHelper = new HTML;
        $data = $_POST;
        if (isset($data['unliked_comment']) && !empty($data['unliked_comment'])){
            $data['unliked_comment'] = $hmtlHelper->purifyParams($data['unliked_comment']);
        }

        if ($likesModel->setLike($data)){
            $return = "1";
        }
    }

    if ($_POST['type'] == "backlink_status") {
        $couples_ids = !empty($_POST['couple_ids']) ? $_POST['couple_ids'] : null;

        if ($couples_ids) {
            list($site_id, $annuaire_id) = explode('_', $couples_ids);
            $backlink = !empty($_POST['backlink']) ? $_POST['backlink'] : null;
            $status = !empty($_POST['action']) ? $_POST['action'] : null;


//            if (!Functions::getDoublon('annuaires2backlinks', 'site_id', $site_id, 'annuaire_id', $annuaire_id, 'backlink', $backlink)) {
                $sql = 'UPDATE `annuaires2backlinks` SET backlink = "' . $backlink . '",
                                                                date_found = "' . date("Y-m-d H:i:s") . '",
                                                                status = "' . $status . '",
                                                                status_type = "manually"
                                WHERE site_id=' . $site_id . ' AND annuaire_id=' . $annuaire_id;

                $ok = $dbh->query($sql);

                // sending notice for project`s owner
                if ($ok && ($status=='found') && ($backlink)){
                    $mailer = new MailProvider();
                    $mailer->NoticeBacklinkFound($site_id,$annuaire_id,$backlink);
                }

               if ($ok) {
                    echo 1;
                    die;
               }
        
//            } else {
//                echo 2;
//                die;
//            }
        }
    }

    if ($_POST['type'] == "referenceur") {
        //header();
    }
    echo $return;
}
?>