<?PHP

include("../topMinimal.php");

//isRight("3,4");

//verify rights

global $dbh;

@session_start();
$prop = Functions::getProp($_POST['id']);
$site = Functions::getSite($prop['to_site_id']);

/* test purpose
$mailProvider = new MailProvider();
$mailProvider->NoticeNewProp($_SESSION['connected']['id']);
*/

if ($_SESSION['connected']['id'] == $site['user_id'] || isSu()) {

} else {
    die();
}

if ($_POST['action'] == "toggle_mod_refuse") {

    // prepared
    $id = $_POST['id'];

    if ($_POST['definitive'] == "true") {
        $def = "`mod_close`='1' ,";
    }

    $stmt = $dbh->query("UPDATE echanges_proposition  SET $def `mod_status`='3', `mod_refuse`=" . $dbh->quote($_POST['comment']) . ", `viewed`='0' WHERE id='" . $id . "'");

    $output = array(
            "replace_text" => "Votre refus à été pris en compte"
    );

    echo json_encode($output);
} elseif ($_POST['action'] == "toggle_mod_accept") {


    // prepared
    $id = $_POST['id'];

    $stmt = $dbh->query("UPDATE echanges_proposition  SET `mod_status`='2', `viewed`='0' WHERE id='" . $id . "'");

    $output = array(
        "replace_text" => "OK"
    );

    echo json_encode($output);


} else if ($_POST['action'] == "toggle_refuse") {

    // prepared
    $id = $_POST['id'];

    if ($prop['ech_status'] != '100') {

        $stmt = $dbh->query("UPDATE echanges_proposition  SET `ech_status`='100', `comment`=" . $dbh->quote($_POST['comment']) . ", `datetime_accepted`=" . $dbh->quote(time()) . ", `viewed`='0' WHERE id='" . $id . "'");

        //rendre les crédits :

        $dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` + " . $prop['credit_cost'] . ") WHERE `id` = " . $prop['from_user_id']);

        $output = array("replace_text" => "Le texte à bien été refusé");

    }

    echo json_encode($output);


} elseif ($_POST['action'] == "toggle_mod") {

    // prepared


    $id = $_POST['id'];

    //si rédigé ereferer :
    if ($prop['redac'] && $prop['ech_status'] != "10") {

        $stmt = $dbh->query("UPDATE echanges_proposition SET `ech_status`='10', `comment`=" . $dbh->quote("Une demande de modification à été envoyée au redacteur") . ", `viewed`='0' WHERE id='" . $id . "'");

        //get project by prop id and change status :

        $requete = "SELECT * FROM redaction_projects WHERE prop_id=" . $id;
        $execution = $dbh->query($requete);
        $proj = $execution->fetch(PDO::FETCH_ASSOC);


        //UPDATE `redaction_projects` SET `status` = 'modification' WHERE `redaction_projects`.`id` = 8259;
        $dbh->query("UPDATE `redaction_projects` SET `status`= 'modification', `viewed`='0' WHERE id='" . $proj['id'] . "'");


        //change redaction reports : webmaster_comment
        $dbh->query("UPDATE `redaction_reports` SET `webmaster_comment`= " . utf8_decode($dbh->quote($_POST['comment'])) . " WHERE project_id='" . $proj['id'] . "'");

        //change balance of user :

        $projectModel = new RedactionProjects($dbh);
        $reportModel = new RedactionReports($dbh);
        $userModel = new User($_SESSION['connected']['id']);

        $project = $projectModel->getProject($proj['id']);
        $report = $reportModel->getReport($proj['id']);

        $writer_earn = $report['writer_earn'];
        $writer_id = $project['affectedTO'];

        $corrector_earn = $report['corrector_earn'];
        $corrector_id = $report['user_approved_id'];

        $userModel->changeBalance($corrector_id, $corrector_earn, false);
        $userModel->changeBalance($writer_id, $writer_earn, false);


    } else {


        $stmt = $dbh->query("UPDATE echanges_proposition SET `ech_status`='50', `comment`=" . $dbh->quote($_POST['comment']) . ", `viewed`='0' WHERE id='" . $id . "'");

    }
    $output = array("replace_text" => "La demande de modification à été prise en compte");

    echo json_encode($output);

} elseif ($_POST['action'] == "toggle_accept") {


    // prepared
    $id = $_POST['id'];

    $stmt = $dbh->query("UPDATE echanges_proposition SET `ech_status`='200', `comment`=" . $dbh->quote($_POST['comment']) . ", `viewed`='0' WHERE id='" . $id . "'");

    ob_start();
    ?>


    <input type="hidden" id="prop" value="zob"/>

    <h4>Valider la publication de l'article</h4>

    Vous avez accepté de publier cet article, vous pouvez désormais le télécharger au format document<br/></br>

    <a href="./files/docs/<?= $prop['doc_link'] ?>" class="button color small round"
       style="color:white;float:right;margin:5px 0px 0px 10px;" name="envoyer">Télécharger</a>
    <br/><br/><br/>

    <fieldset>
        <label><span>* </span>URL de la page sur lequel l'article à été publié</label>
        <input id="mc_url_validate_ok" name="url" type="text" value="" style="float:left;width:calc(100% - 20px);"/>
    </fieldset>


    <br/>
    <a id="mc_validate" class="button color small round" style="color:white;float:right;margin:5px 0px 0px 10px;"
       name="envoyer">Valider</a>
    <br/>

    <script type="text/javascript">


        jQuery(document).ready(function () {


            jQuery("#mc_validate").click(function () {

                this_url = jQuery("#mc_url_validate_ok").val();

                jQuery("#form_text_accept").html("Veuillez patienter, l'article est en cours de verification et de validation...");

                jQuery.post("files/includes/ajax/echanges_propositions_recues.php", {
                    action: 'toggle_validate',
                    url: this_url,
                    id: jQuery("#prop_id_accept").val()
                }).done(function (data) {

                    jQuery("#form_overlay_accept").css("display", "block");

                    jQuery("#form_text_accept").html(data);

                });
            });
        });

    </script>

    <?
    $replace = ob_get_contents();
    ob_end_clean();
    $output = array(

        "replace_text" => $replace
    );

    echo json_encode($output);

} elseif ($_POST['action'] == "toggle_validate") {


    if ($_POST['url']) {


    } else {
        $error++;
        $errors[] = "Veuillez renseigner le champ URL.";
    }

    //check links on article with simpleDOM
    $arr_url = parse_url($_POST['url']);
    $domain = str_ireplace("www.", "", $arr_url["host"]);

    $arr_url2 = parse_url($site['url']);
    $domain2 = str_ireplace("www.", "", $arr_url2["host"]);

    if ($domain != $domain2) {
        $error++;
        $errors[] = "Le domaine de cette URL ($domain) ne correspond pas à l'URL du site ($domain2)";
    }

    if ($error) {

        echo "<h4>La publication n'a pas pu être validée, erreurs :</h4>";

        foreach ($errors as $err) {
            echo $err;
        }

    } else {


        //validate

        include(__DIR__ . "../simple_html_dom.php");

        //$ct=file_get_contents($_POST['url']);


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $_POST['url']);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $ct = curl_exec($ch);

        if ($ct === false) {
            $error++;
            $errors[] = 'Erreur Curl : ' . curl_error($ch);
        }

        curl_close($ch);

        $html = str_get_html($ct);
        $links = 0;

        if ($html) {
            foreach ($html->find('a') as $element) {
                $links++;
                $results['links_href'][] = trim(html_entity_decode($element->href), "/");
            }

            //
            $check = unserialize($prop['check_links']);

            foreach ($check as $anchor => $link) {


                if (in_array(trim($link, "/"), $results['links_href'])) {


                } else {

                    $error++;
                    $errors[] = "Le lien '$link' n'a pas été trouvé sur cette page<br/>";

                }

            }

        } else {


            $error++;
            $errors[] = "Impossible d'acceder à l'URL.";


        }


        if ($error) {

            echo "<h4>La publication n'a pas pu être validée, erreurs :</h4>";

            foreach ($errors as $err) {
                echo $err;
            }

        } else {

            if ($prop['ech_status'] == 200) {
                //change status, add credits
                $id = $_POST['id'];
                $dbh->query("UPDATE echanges_proposition SET `ech_status`='201', `comment`=" . $dbh->quote('<a target="_blank" href="' . $_POST['url'] . '">' . $_POST['url'] . '</a>') . ", `page_publish`=" . $dbh->quote($_POST['url']) . ", `datetime_accepted`=" . $dbh->quote(time()) . ",  `viewed`='0' WHERE id='" . $id . "'");

                //add credits
                $add = $prop['credit_cost'];
                $to = $site['user_id'];

                $dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` + " . $add . ") WHERE `id` = " . $to);

                echo "<h4>La publication à été validée, vous avez été crédité de $add crédits.</h4><br/>Veuillez patienter pendant la redirection...<br/>";


                echo '<script type="text/javascript">

				setTimeout(
				  function() 
				  {
				    location.reload();
				  }, 4000);

			</script>';
            }
        }
    }

} ?>