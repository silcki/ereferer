<?php

include("../config/configuration.php");
include("../functions.php");


require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';

$error = "";
$return = "";

if (isset($_POST['type']) && !empty($_POST['type'])) {

    $redactionProjects = new RedactionProjects($dbh);
    $project_id = intval($_POST['project_id']);

    // readaction - load template
    if ($_POST['type'] == "redaction_project_template") {
        $response = $redactionProjects->getProject($project_id,true);
    }

    // redatction - project for edition
    if ($_POST['type'] == "redaction_project_edition") {
        $response = $redactionProjects->getProject($project_id,false);
        $response['desc'] = utf8_encode(utf8_decode($response ['desc']));
    }

    // readaction - check balance
    if ($_POST['type'] == "redaction_check_solde") {
        $webmaster_id = intval($_POST['webmaster_id']);
        $amount = floatval($_POST['amount']);
        $is_edition = $_POST['edition'];

        $solde = Functions::getSolde($webmaster_id); // get balance

        // new project
        if (!$is_edition){
            $checking = $solde - $amount;
            $checking = ($checking>0.0)?1:0;
            $response = array('checking'=>$checking);
        }else{ //edition
            $project = $redactionProjects->getProject($project_id);
            $current_amount = $project['amount'];
            if ($amount<=$current_amount){
                $checking = 1;
            }else{
                $diff_plus = $amount-$current_amount;
                $checking = $solde - $diff_plus;
                $checking = ($checking>0.0)?1:0;
            }
            $response = array('checking'=>$checking);
        }

    }

    //header("content-type:text/html;charset=ISO-8859-1");
    header('Content-Type: application/json');

    $response =json_encode($response);
    //var_dump($response);
    echo $response;

    if ($_SERVER['REMOTE_ADDR']=='37.45.247.157'){
        //var_dump("JSON  error ",json_last_error());
    }

    die;
}