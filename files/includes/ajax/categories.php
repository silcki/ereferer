<?PHP

include("../topMinimal.php");

isRight("3,4");

global $dbh;



function show_cat_list(){
			ob_start();
			$categories = Functions::getCategoriesListv2();
			$count = count($categories);

			if ($count > 0) { ?>

			                <table id="mc_cat_list" class="simple-table">
			                    <tbody>
			                        <tr>
			                            <th>
			                                Cat�gories 
			                            </th>
			                           
			                            <th>
			                                Actions
			                            </th>


			                        </tr>
			                        <?PHP
			                       for ($inc=1;$inc<=$count;$inc++) {
			                            ?>
			                            <tr>
			                                <td>
			                                    <?PHP if($categories[$inc]['level']>0){ echo "&nbsp;&nbsp; > &nbsp;&nbsp;"; } echo $categories[$inc]['value']; ?>
			                                </td>
			                                
			                                <td>
			                                     <a class="mc_del_cat" rel="<?PHP echo $categories[$inc]['id']; ?>"><i class="icon-trash"></i> supprimer</a>
			                                </td>

			                            </tr>
			                            <?PHP
			                        }
			                        ?>
			                    </tbody>
			                </table>
			              
			                <?PHP
			               
            } else {
                ?>
                <div align="center" width="500" style="width:700px;margin:auto;">
                    <h1>Aucun r�sultat � afficher.</h1>

                </div>

            <?PHP
            }
            
            $out = ob_get_contents();
			ob_end_clean();
			return $out;
}

function show_select_cat_list(){
			
			$categories = Functions::getCategoriesListv2();
			$count = count($categories);
			

			$ret='<option value="0">-</option>';

        for ($inc=1;$inc<=$count;$inc++) {

                $value=$categories[$inc]['value'];
                $id= $categories[$inc]['id'];
                if($categories[$inc]['level']==0){$ret.="<option value=\"$id\">$value</option>"; }
         }
	return $ret;
}


if($_POST['action']=="add"){
		
		if(!empty($_POST['value'])){

		    // prepared
		    $stmt = $dbh->prepare('INSERT INTO  categories_v2  (`id`, `value`, `level`, `parent`, `active`)
		                                        VALUES("",
		                                                :value,
		                                                :level,
		                                                :parent,
		                                               	1
		                                                )');

		    $stmt->bindParam(':value', utf8_decode($_POST['value']));
		    $stmt->bindParam(':level', $_POST['cat_id']==0?$level=0:$level=1);
		    $stmt->bindParam(':parent', $_POST['cat_id']);

		    $stmt->execute();

		    $output=array("message"=>"La cat�gorie '".utf8_decode($_POST['value'])."' � �t� ajout�e<br/><br/>",
						  "table"=>show_cat_list(),
						  "select"=>show_select_cat_list());

		   	echo json_encode(cast_data_types($output));
		    
		}

	 	show_cat_list();

		} elseif($_POST['action']=="delete"){
			
			
			$id=$_POST['id'];
			$dbh->query("DELETE FROM categories_v2 WHERE id='".$id."' ");
			$dbh->query("DELETE FROM categories_v2 WHERE parent='".$id."' ");

			$output=array("message"=>"La cat�gorie � �t� supprim�e<br/><br/>",
						  "table"=>show_cat_list(),
						  "select"=>show_select_cat_list());

			echo json_encode(cast_data_types($output));
		}
	
?>