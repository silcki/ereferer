<?php

include("../topMinimal.php");
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';
global $dbh;
@session_start();

$client = Functions::getUserInfos($_SESSION['connected']['id']); 

if($_POST['prop_id']){
	
	$prop = Functions::getProp($_POST['prop_id']);
	$site = Functions::getSite($prop['to_site_id']);

	if( $_SESSION['connected']['id'] == $prop['from_user_id'] || isSu()){
	} else {
		die();
	}
} else {
	$site = Functions::getSite($_POST['site_id']);
}

//print_r($_POST);
$error=0;

require_once '../third/MsWordToImageConvert.php';

// Set the uplaod directory
$uploadDir = '/files/docs/';

// Set the allowed file extensions
$fileTypes = array('docx','odt', 'doc'); // Allowed file extensions

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile   = $_FILES['Filedata']['tmp_name'];
	$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
	
	// Validate the filetype
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	
	$ts=md5(time());
	$ts2=md5(time()."jpg");

	$targetFile = $uploadDir . $ts.".".$fileParts['extension'];

	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

		// Save the file
		move_uploaded_file($tempFile, $targetFile);
		

		if ($fileParts['extension'] == "odt") {
			//convert odt to docx

            $ara = file_get_contents($_SESSION['allParameters']['url']['valeur']."files/services/onlineconvert/oc.php?key=OIZfionef5ef5651ze54gEzeg845&file=".$_SESSION['allParameters']['url']['valeur'].'files/docs/'.$ts.".".$fileParts['extension']);

            $ara = unserialize($ara);
         	
            rename($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$ara['file_id']."/".$ts.".docx",$_SERVER['DOCUMENT_ROOT']."/files/docs/".$ts.".docx");
 
            unlink($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$ara['file_id']."/".$ts.".odt");

		} elseif ($fileParts['extension']=="doc") {

			$ara = file_get_contents($_SESSION['allParameters']['url']['valeur']."files/services/onlineconvert/oc.php?key=OIZfionef5ef5651ze54gEzeg845&file=".$_SESSION['allParameters']['url']['valeur'].'files/docs/'.$ts.".".$fileParts['extension']);

            $ara = unserialize($ara);
         
            rename($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$ara['file_id']."/".$ts.".docx",$_SERVER['DOCUMENT_ROOT']."/files/docs/".$ts.".docx");
 
            unlink($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$ara['file_id']."/".$ts.".doc");

		}


		//Create image file
		/*
		$convert = new MsWordToImageConvert("3227716289", "6341013409217433156189417");
		$convert->fromURL($_SESSION['allParameters']['url']['valeur'].'files/docs/'.$ts.".docx");
		$convert->toFile($uploadDir.$ts2."."."jpg");
		*/


		//Unzip & count images
		$zip = new ZipArchive;
		if ($zip->open( $uploadDir . $ts.".docx") === TRUE) {
		    $zip->extractTo($uploadDir."/".$ts);
		    $zip->close();
		   
		} else {
		   $errors[]='La décompression du document a recontré un problème.';
		   $error++;
		}

	
		$img_count=0;

		foreach (new GlobIterator($uploadDir."/".$ts.'/word/media/*.jpg') as $fileinfo) {
		    $img_count++;
		}


		foreach (new GlobIterator($uploadDir."/".$ts.'/word/media/*.jpeg') as $fileinfo) {
		    $img_count++;
		}

		foreach (new GlobIterator($uploadDir."/".$ts.'/word/media/*.png') as $fileinfo) {
		    $img_count++;
		}

		$dir=$uploadDir."/".$ts;

		if(is_dir($dir)){
		    $it = new RecursiveDirectoryIterator($dir);
		    $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
		    foreach($it as $file) {
		        if ('.' === $file->getBasename() || '..' ===  $file->getBasename()) continue;
		        if ($file->isDir()) rmdir($file->getPathname());
		        else unlink($file->getPathname());
		    }
		    rmdir($dir);
		}

	    //convert to html and extract values
	    //$ar=unserialize(file_get_contents($_SESSION['allParameters']['url']['valeur']."files/services/phpword/service/ereferer.php?key=OIZfionef5ef5651ze54gEzeg845&file=files/docs/".$ts.".docx"));
		
		$ar = unserialize(file_get_contents($_SESSION['allParameters']['url']['valeur']."files/services/onlineconvert/ochtml.php?key=OIZfionef5ef5651ze54gEzeg845&file=".$_SESSION['allParameters']['url']['valeur'].'files/docs/'.$ts.".docx"."&filename=".$ts));

		//print_r($_SESSION['allParameters']['url']['valeur']."files/services/onlineconvert/ochtml.php?key=OIZfionef5ef5651ze54gEzeg845&file=".$_SESSION['allParameters']['url']['valeur'].'files/docs/'.$ts.".docx"."&filename=".$ts);
		

		$coutcredit = $_SESSION['allParameters']['prix_achat_credit']['valeur'];
        $credits_restants = $client['credit'];
    
        if($client['credit_auto']){
			$credits_restants += $client['solde']/$coutcredit;
		}

		if($site['credit'] > $credits_restants && empty($_POST['prop_id'])){
			$error++;
			$errors[]="Vous n'avez pas assez de crédits pour soumettre une proposition sur ce site. (Il vous reste $credits_restants crédits.)<br/><br/>";
		}


		if($_POST['nb_mots_min'] <= $ar['words']){
			$valid[] = "Votre document comporte ".$ar['words']." mots.<br/><br/>";
		} else {
			$error++;
			$errors[] = "Votre document comporte ".$ar['words']." mot(s). Il faut un minimum de ".$_POST['nb_mots_min']." mot(s).<br/><br/>";
		}

		if($_POST['nb_liens_max'] >= $ar['links']){
			$valid[] = "Votre document comporte ".$ar['links']." lien(s).<br/><br/>";
		} else {
			$error++;
			$errors[] = "Votre document comporte ".$ar['links']." lien(s). Il faut un maximum de ".$_POST['nb_liens_max']." lien(s).<br/><br/>";
		}

		if($_POST['nb_images_min'] <= $img_count){
			$valid[] = "Votre document comporte ".$img_count." image(s).<br/><br/>";
		} else {
			$error++;
			$errors[] = "Votre document comporte ".$img_count." image(s). Il faut un minimum de ".$_POST['nb_images_min']." image(s).<br/><br/>";
		}


		if($_POST['prop_id']){$where='AND id != '.$dbh->quote($_POST['prop_id']);}

		$sQuery = "SELECT * FROM `echanges_proposition` WHERE ech_status!='100' $where";
		$rResult = $dbh->query( $sQuery);
		
		while ( $record = $rResult->fetch(PDO::FETCH_ASSOC) )
		{
			$x=similar_text($ar['plaintext'], $record['plaintext'],$percent);
			if($percent >= 90){
				$error++;
				$errors[]="Votre document est trop similaire à un autre déjà posté. (".$percent."% de similarité)";
				break;
			}
		}


	} else {

		$error++;
		// The file type wasn't allowed
		$errors[] = 'Type de fichier invalide.';

	}

	if ($error) {

		echo "<h4>Votre document n'a pas été accepté, erreurs :</h4><div style=\"color:red;\">";
		foreach($errors as $err){
			echo $err;
		}
		echo "</div>";

		if($valid){
			echo "<h4>Voici les points valides :</h4><div style=\"color:green;\">";
			foreach($valid as $err){
				echo $err;
			}
		}
		echo "</div>";
		
		echo ' <a href="javascript:reset_form();" class="button color small round disabled" style="color:white;float:right;margin:5px 0px 0px 10px;" name="envoyer">Retour</a>';

	} else {

		echo "<h4>Votre document a été accepté, voici ses caractéristiques :</h4>";
		foreach($valid as $err){
			echo $err;
		}

		//ADD IN DATABASE :


	    $stmt = $dbh->prepare('INSERT INTO echanges_proposition (`id`, `from_user_id`, `to_site_id`,  `redac`, `doc_link`, `doc_img`, `ech_status`, `datetime_created`, `page_publish`, `nb_mots`, `nb_images`, `nb_liens`, `plaintext`, `check_links`, `credit_cost`)
	                                        VALUES(null,
	                                                :from_user_id,
	                                                :to_site_id,
	                                               	0,
	                                               	:doc_link,
	                                               	:doc_img,
	                                               	0,
	                                               	:dt,
	                                               	0,
	                                               	:nb_mots,
	                                               	:nb_images,
	                                               	:nb_liens,
	                                               	:texte,
	                                               	:check_links,
	                                               	:credit_cost
	                                                )');

	    $stmt->bindParam(':from_user_id', $_SESSION['connected']['id']);
	    $stmt->bindParam(':to_site_id', $_POST['site_id']);
	    $doclink=$ts.".docx";
	    $stmt->bindParam(':doc_link', $doclink);

	    $docimg=$ts."/index.html";

	    $stmt->bindParam(':doc_img', $docimg);
	    $stmt->bindParam(':nb_mots', $ar['words']);
	    $stmt->bindParam(':check_links', serialize($ar['links_href']));
	    $time=time();
	    $stmt->bindParam(':dt', $time);

	    $stmt->bindParam(':nb_images', $img_count);
	    $stmt->bindParam(':nb_liens', $ar['links']);
	    $stmt->bindParam(':texte', $ar['plaintext']);
	    $stmt->bindParam(':credit_cost', $site['credit']);

	    $stmt->execute();

	    //send notification :
	     $mailProvider = new MailProvider();
	   	$mailProvider->NoticeNewProp($site['user_id']);

	    if($_POST['prop_id']){

	    	//delete old prop :

	    	$where='WHERE id = '.$dbh->quote($_POST['prop_id']);
	    	$sQuery = "DELETE FROM `echanges_proposition` $where";
			$rResult = $dbh->query( $sQuery);

	    } else {

	    	if($site['credit'] <= $client['credit']){
	    		$dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` - ".$site['credit'].") WHERE `id` = ".$_SESSION['connected']['id']);
	    	} else {
	    		$reste=$site['credit']-$client['credit'];
	    		$dbh->query("UPDATE `utilisateurs` SET `credit` = 0 WHERE `id` = ".$_SESSION['connected']['id']);
	    		
	    		//retirer le reste au solde
	    		$coutcredit = $_SESSION['allParameters']['prix_achat_credit']['valeur'];
	    		$retrait = $reste * $coutcredit;

	    		$dbh->query("UPDATE `utilisateurs` SET `solde` = (`solde` - ".$retrait.") WHERE `id` = ".$_SESSION['connected']['id']);
				
				$transaction = new Transaction($dbh, $_SESSION['connected']['id']);
				$transaction->addTransaction("Publication sur le blog " . $site['url'], 0, $retrait);
	    	}
	    }
	}
}
?>