<?PHP
include("../topMinimal.php");

global $dbh;

@session_start();

$aColumns = array( 'url', 'credit','age','alexa','tf','dom_ref','avis','pref','temps','actions');

/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "id";




//ORDERS :
if($_GET['sSortDir_0']=="asc"){$ascdesc="ASC";}
if($_GET['sSortDir_0']=="desc"){$ascdesc="DESC";}
if(intval($_GET['iSortCol_0'])==0){ $order="ORDER BY domain ".$ascdesc; }
if(intval($_GET['iSortCol_0'])==1){ $order="ORDER BY credit ".$ascdesc; }
if(intval($_GET['iSortCol_0'])==2){ $order="ORDER BY IF(AGE_M!='0000-00-00', AGE_M, AGE) ".$ascdesc; }
if(intval($_GET['iSortCol_0'])==3){ $order="ORDER BY IF(TF_M!='0', TF_M, TF) ".$ascdesc; }
if(intval($_GET['iSortCol_0'])==4){ $order="ORDER BY IF(DOM_REF_M!='0', DOM_REF_M, DOM_REF) ".$ascdesc; }

//FILTERS :

if($_GET['cat_id']!=0){
	$where="AND (cat_id1 = ".$dbh->quote($_GET['cat_id'])." OR cat_id2 = ".$dbh->quote($_GET['cat_id']).")";
}

if($_GET['tag']!=''){
	$where.=" AND (tags LIKE ".$dbh->quote('%'.utf8_decode($_GET['tag']).'%').")";
}

if($_GET['dont_show_mask']=='true'){
	$where.=" AND masquer='0' ";
}

$what='credit'; if($_GET[$what]!=''){ if($_GET[$what.'_sign']=="<="){$sign="<=";} else {$sign=">=";} $where.=" AND (".$what." ".$sign." ".$dbh->quote($_GET[$what]).")"; }
$what='tf'; if($_GET[$what]!=''){ if($_GET[$what.'_sign']=="<="){$sign="<=";} else {$sign=">=";} $where.=" AND (IF(TF_M!='0', TF_M, TF) ".$sign." ".$dbh->quote($_GET[$what]).")"; }
$what='alexa'; if($_GET[$what]!=''){ if($_GET[$what.'_sign']=="<="){$sign="<=";} else {$sign=">=";} $where.=" AND (IF(ALEXA_M!='0', ALEXA_M, ALEXA) ".$sign." ".$dbh->quote($_GET[$what]).")"; }

$what='dom_ref'; if($_GET[$what]!=''){ if($_GET[$what.'_sign']=="<="){$sign="<=";} else {$sign=">=";} $where.=" AND (IF(DOM_REF_M!='0', DOM_REF_M, DOM_REF) ".$sign." ".$dbh->quote($_GET[$what]).")"; }

$what='age'; if($_GET[$what]!=''){ 
	
	if($_GET[$what.'_sign']=="<="){

		$where.=" AND (IF(AGE_M!='0000-00-00', AGE_M, AGE) >= DATE_ADD(CURDATE(), INTERVAL - ".$dbh->quote($_GET[$what])." YEAR))"; 

	} else {
		$where.=" AND (IF(AGE_M!='0000-00-00', AGE_M, AGE) <= DATE_ADD(CURDATE(), INTERVAL - ".$dbh->quote($_GET[$what])." YEAR))"; 
	} 
	
	

}

//$what=''; if($_GET[$what]!=''){ if($_GET[$what.'_sign']=="<="){$sign="<=";} else {$sign=">=";} $where.=" AND (".$what." ".$sign." ".$dbh->quote($_GET[$what]).")"; }



$table="echange_sites";
$sQuery = "SELECT SQL_CALC_FOUND_ROWS * FROM `$table` WHERE id=id $where AND `user_id`!= '".$_SESSION['connected']['id']."' AND active=1 $order";

$rResult = $dbh->query( $sQuery);
	

/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS() as found
	";
	$rResultFilterTotal = $dbh->query( $sQuery);
	$aResultFilterTotal = $rResultFilterTotal->fetch(PDO::FETCH_ASSOC);
	$iFilteredTotal = intval($aResultFilterTotal['found']);

	/* Total data set length */
	$sQuery = "
		SELECT COUNT(*) as count
		FROM  $table
	";
	$rResultTotal = $dbh->query( $sQuery);
	$aResultTotal = $rResultTotal->fetch(PDO::FETCH_ASSOC);
	$iTotal = $aResultTotal['count'];
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	function secondsToTime($inputSeconds) {

	    $secondsInAMinute = 60;
	    $secondsInAnHour  = 60 * $secondsInAMinute;
	    $secondsInADay    = 24 * $secondsInAnHour;

	    // extract days
	    $days = floor($inputSeconds / $secondsInADay);

	    // extract hours
	    $hourSeconds = $inputSeconds % $secondsInADay;
	    $hours = floor($hourSeconds / $secondsInAnHour);

	    // extract minutes
	    $minuteSeconds = $hourSeconds % $secondsInAnHour;
	    $minutes = floor($minuteSeconds / $secondsInAMinute);

	    // extract the remaining seconds
	    $remainingSeconds = $minuteSeconds % $secondsInAMinute;
	    $seconds = ceil($remainingSeconds);

	    // return the final array
	    $obj = array(
	        'd' => (int) $days,
	        'h' => (int) $hours,
	        'm' => (int) $minutes,
	        's' => (int) $seconds,
	    );
	    return $obj;
	}
	
	while ( $record = $rResult->fetch(PDO::FETCH_ASSOC) )
	{

		$table2="echanges_proposition";
		$sQuery2 = "SELECT* FROM `$table2` WHERE `from_user_id` = '".$record['user_id']."' AND datetime_accepted != '0'";
		$rResult2 = $dbh->query( $sQuery2 );
		$count=0;$temps=0;

		while ( $record2 = $rResult2->fetch(PDO::FETCH_ASSOC) ){
			$count++;
			$temps+=$record2['datetime_accepted']-$record2['datetime_created'];
		}
		
		$moyennetemps=$temps/$count;

		$client = Functions::getUserInfos($_SESSION['connected']['id']);

		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{

			if ( $aColumns[$i] == "url" ){ 
				
				$table3="echanges_proposition";
				$sQuery3 = "SELECT* FROM `$table3` WHERE `from_user_id` = '".$_SESSION['connected']['id']."' AND to_site_id = '".$record['id']."' AND ech_status IN(201)";
				$rResult3 = $dbh->query( $sQuery3 );
				
				$catname1=Functions::getCategorybyID($record['cat_id1']);

				$x=Functions::getCategorybyID($record['cat_id2']);
				
				if(trim($x)!=""){
					$catname2=", ".$x;
				} else {
					$catname2="";
				}

				$cats="<span style=\"font-size:10px\">Cat�gories : $catname1[value] $catname2[value]<br/><span>";
				$tags="<span style=\"font-size:10px\">Tags : ".$record['tags']."</span>";

				if(file_exists($_SERVER['DOCUMENT_ROOT']."/files/screenshot/".md5($record['domain']).".jpg")){
					$screen='<br/><a href="files/screenshot/'.md5($record['domain']).'.jpg" class="colorbox">Aper�u</a>';
				} else {
					$screen="";
				}

				if($record3 = $rResult3->fetch(PDO::FETCH_ASSOC)){
						$row[] =  $record['url']."<br/>".$cats.$tags.$screen.'<br/><a style="font-size:11px;" href="./files/includes/ajax/history.php?site_id='.$record['id'].'" rel="modal:open" >Voir l\'historique de vos �changes</a>';
				} else {
						if( ( $client['trusted'] && $record['showurltotrusted'] ) || ( isSu($_SESSION['connected']['id']) ) ) {
							$row[] =  $record['url']."<br/>".$cats.$tags.$screen;
						} else {
							$row[] =  $record['urlshow']."<br/>".$cats.$tags.$screen;
						}
				}




			}

			elseif ( $aColumns[$i] == "credit" ){ 
				
				if(!$record['accept_web'] && !$record['accept_eref'] && $record['accept_self']){
					$selfonly=1;
					$row[] =  $record['credit']+1;
				} else {
					$selfonly=0;
					$row[] =  $record['credit'];
				}
				
			}
			elseif ( $aColumns[$i] == "age" ){
				
				if($record['age_m']!="0000-00-00"){ $row[]=dateDifference(date("Y-m-d"),$record['age_m'],'%y an(s) %m mois'); 
					
				} else {

					if($record['age']!="0000-00-00"){ 

						if($record['age']!="0000-00-00") $row[]=dateDifference(date("Y-m-d"),$record['age'],'%y an(s) %m mois');

					} else {
						$row[]="-";
					}

				} 

			}
			elseif ( $aColumns[$i] == "alexa" ){ 
				$record['alexa_m'] ? $row[]=$record['alexa_m'] : $row[]=$record['alexa'];  }
			elseif ( $aColumns[$i] == "tf" ){ 
				$record['tf_m'] ? $row[]=$record['tf_m'] : $row[]=$record['tf'];  }
			elseif ( $aColumns[$i] == "dom_ref" ){  $record['dom_ref_m'] ? $row[]=$record['dom_ref_m'] : $row[]=$record['dom_ref']; }
			elseif ( $aColumns[$i] == "avis" ){ 

				$table4="echanges_proposition";
				$sQuery4 = "SELECT* FROM `$table4` WHERE `to_site_id` = '".$record['id']."' AND avis_note != '0'";
				$rResult4 = $dbh->query( $sQuery4 );
				$ctn=0;$note=0;$com=0;
				while ( $record4 = $rResult4->fetch(PDO::FETCH_ASSOC) ){
					$ctn++;
					$note+=$record4['avis_note'];
					if($record4['avis_com']) $com++;
				}
				
				if($ctn==0){
					$row[] = "-";
				} else {
					
					$nb=round($note/$ctn,1)/5*85;

					$stars='<div class="center"><div class="stars_rating_wrapper"><div class="stars_rating_empty"></div><div class="stars_rating" style="width:'.$nb.'px;"></div></div>';

					$row[] =  $stars."(Sur $ctn avis)<br/><span style=\"font-size:9px\"><a href=\"./files/includes/ajax/comments.php?id=".$record['id']."\" rel=\"modal:open\">Commentaires ($com)</a></span></div>";
				}
				

			}
			elseif ( $aColumns[$i] == "pref" ){ 
				$row[]='<span class="Notes">Mots</span> : '.$record['nb_mots_max'].'<br/>
                        <span class="Notes">Images</span> : '.$record['nb_images_min'].'<br/>
                        <span class="Notes">Liens</span> : '.$record['nb_liens_max'].'<br/>';

                        }
			elseif ( $aColumns[$i] == "temps" ){ 

				if($moyennetemps!="0"){
				
					$stt=secondsToTime($moyennetemps); 
					
					if($stt['h']>0 || $stt['m']>0 ){$stt['d']++;}

					$row[] = $stt['d']." jour(s)";
					

				} else {
					
					$row[] = "-";
				
				}
			}
			elseif ( $aColumns[$i] == "actions" ){ 
				unset($pro);

				if($record['accept_web']) $pro.='<a onClick="reset_form();" class="modal-prop" href="#modal-prop" rel="modal:open" credit="'.$record['credit'].'" site="'.$record['id'].'" nb_mots_min="'.$record['nb_mots_max'].'" nb_liens_max="'.$record['nb_liens_max'].'" nb_images_min="'.$record['nb_images_min'].'" regle="'.$record['regle'].'">Proposer votre article</a><br/>';

				if($record['accept_eref']) $pro.='<a onClick="reset_form_red();" class="modal-red" href="#modal-red" rel="modal:open" credit="'.$record['credit'].'" site="'.$record['id'].'" nb_liens_max="'.$record['nb_liens_max'].'" nb_mots_min="'.$record['nb_mots_max'].'" >R�daction par ereferer</a><br/>';
				
				if($record['accept_self']) $pro.='<a onClick="reset_form_self();" class="modal-self" href="#modal-self" rel="modal:open" selfonly="'.$selfonly.'" credit="'.($record['credit']+1).'" site="'.$record['id'].'" nb_liens_max="'.$record['nb_liens_max'].'" nb_mots_min="'.$record['nb_mots_max'].'" >R�daction par le webmaster</a><br/>';

				
				$row[]=$pro;
			
			

			 }

		}
		
		$output['aaData'][] = $row;
	}

	echo json_encode( cast_data_types($output ));
?>