<?php

include("../topMinimal.php");
global $dbh;


@session_start();

$error=0;


$client = Functions::getUserInfos($_SESSION['connected']['id']); 


// Set the uplaod directory

if($client['societe']){

	$uploadDir = '/dl-files/factures-envoyees/'.date("Y")."/Societe/";

} else {

	$uploadDir = '/dl-files/factures-envoyees/'.date("Y")."/Particuliers/";

}
$baseuploadDir=$uploadDir;

if (!mkdir($_SERVER['DOCUMENT_ROOT'].$uploadDir,0755,true)){
	
}

if(!$_POST['credits']){
		$error++;
		$errors.="Le champ cr�dit doit �tre rempli <br/><br/>";
} else {

    if($_POST['credits']>$client['credit']){
         
        $error++;
		$errors.="Votre solde est insuffisant <br/><br/>";
        
    } 

}

if($_POST['credits']<($_SESSION['allParameters']['seuil_paiement']['valeur']/$_SESSION['allParameters']['prix_vente_credit']['valeur'])){
	$error++;
	$errors.="Le nombre de cr�dits doit �tre superieur � 20 <br/><br/>";
}


if(!$_POST['paypal']){
		$error++;
		$errors.="Le champ adresse paypal doit �tre rempli <br/><br/>";
}



// Set the allowed file extensions
$fileTypes = array('pdf'); // Allowed file extensions

$verifyToken = md5('unique_salt' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile   = $_FILES['Filedata']['tmp_name'];
	$uploadDir  = $_SERVER['DOCUMENT_ROOT'] . $uploadDir;
	
	// Validate the filetype
	$fileParts = pathinfo($_FILES['Filedata']['name']);
	
	$ts=$client['nom']."-".date("Y-m-d");

	$targetFile = $uploadDir . $ts.".".$fileParts['extension'];

	if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

		// Save the file
		move_uploaded_file($tempFile, $targetFile);

	} else {
		$error++;
		// The file type wasn't allowed
		$errors.='Type de fichier invalide.';

	}



	if($error){

		$output=array("errors"=>$errors);

	} else {

		//ADD IN DATABASE :


	    $stmt = $dbh->prepare('INSERT INTO payments (`id`, `user_id`, `amount`,  `credits`, `file`, `paypal_address`, `status`, `created`)
	                                        VALUES("",
	                                                :user_id,
	                                                :amount,
	                                               	:credits,
	                                               	:file,
	                                               	:paypal_address,
	                                               	0,
	                                               	:thistime
	                                                )');

	    $stmt->bindParam(':user_id', $_SESSION['connected']['id']);
	    
	    $montant=$_SESSION['allParameters']['prix_vente_credit']['valeur']*$_POST['credits'];
	    $stmt->bindParam(':amount', $montant);
	    $stmt->bindParam(':credits', $_POST['credits']);

	    $doclink=$baseuploadDir.$ts.".".$fileParts['extension'];
	    $stmt->bindParam(':file', $doclink);
	    $stmt->bindParam(':paypal_address', $_POST['paypal']);
	    $stmt->bindParam(':thistime', time());
	    
	    $stmt->execute();


	    //remove cr�dits
   
        $dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` - ".$_POST['credits'].") WHERE `id` = ".$_SESSION['connected']['id']);

 


	   $output=array("message"=>"ok"
						 );

	
	  

	}
		echo json_encode(cast_data_types($output));
}
?>