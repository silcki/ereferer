<?php
include("../topMinimal.php");
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';
global $dbh;
@session_start();


$client = Functions::getUserInfos($_SESSION['connected']['id']); 

if($_POST['prop_id']){
	
	$prop = Functions::getProp($_POST['prop_id']);
	$site = Functions::getSite($prop['to_site_id']);

	if( $_SESSION['connected']['id'] == $prop['from_user_id'] || isSu()){

	} else {
		die();
	}

} else {

	$site = Functions::getSite($_POST['site_id']);

}

$coutmot = $_SESSION['allParameters']['price_100_words']['valeur']/100;
$cout_red = round($site['nb_mots_max'] * $coutmot,2);


$coutcredit = $_SESSION['allParameters']['prix_achat_credit']['valeur'];                      
$credits_restants = $client['credit'];

if($client['credit_auto']){$credits_restants += ($client['solde'] - $cout_red)/$coutcredit;}

if($site['credit'] > $credits_restants){
	$error++;
	$errors[]="Vous n'avez pas assez de crédits pour soumettre une proposition sur ce site.<br/><br/>";
}



if($cout_red > $client['solde']){
	$error++;
	$errors[]="Votre solde est insuffisant pour demander une rédaction. (Il vous reste un solde de $client[solde] €.)<br/><br/>";
}



if($error){
	echo "<h4>Votre demande n'a pas été accepté, erreurs :</h4>";
	foreach($errors as $err){
		echo $err;
	}

	echo ' <a href="javascript:reset_form_red();" class="button color small round disabled" style="color:white;float:right;margin:5px 0px 0px 10px;" name="envoyer">Retour</a>';
} else {
		echo "<h4>Votre demande à été acceptée</h4>";

		//remove credits

		if($site['credit'] <= $client['credit']){
            $dbh->query("UPDATE `utilisateurs` SET `credit` = (`credit` - ".$site['credit'].") WHERE `id` = ".$_SESSION['connected']['id']);
    	} else {
    		$reste = $site['credit'] - $client['credit'];
    		$dbh->query("UPDATE `utilisateurs` SET `credit` = 0 WHERE `id` = ".$_SESSION['connected']['id']);
    		
    		//retirer le reste au solde
    		$coutcredit = $_SESSION['allParameters']['prix_achat_credit']['valeur'];
    		$retrait = $reste * $coutcredit;

    		$dbh->query("UPDATE `utilisateurs` SET `solde` = (`solde` - " . $retrait.") WHERE `id` = ".$_SESSION['connected']['id']);

			$transaction = new Transaction($dbh, $_SESSION['connected']['id']);
			$transaction->addTransaction("Rédaction d’un article pour le blog " . $site['url'] . " (crédits)", 0, $retrait);
    	}

    	//remove sold :
    	$dbh->query("UPDATE `utilisateurs` SET `solde` = (`solde` - " . $cout_red.") WHERE `id` = ".$_SESSION['connected']['id']);

		$transaction = new Transaction($dbh, $_SESSION['connected']['id']);
		$transaction->addTransaction("Rédaction d’un article pour le blog " . $site['url'], 0, $cout_red);

   		//ADD IN DATABASE :


	    $stmt = $dbh->prepare('INSERT INTO echanges_proposition (`id`, `from_user_id`, `to_site_id`,  `redac`, `ech_status`, `datetime_created`, `page_publish`, `nb_mots`, `nb_images`, `nb_liens`, `plaintext`, `credit_cost`)
	                                        VALUES(null,
	                                                :from_user_id,
	                                                :to_site_id,
	                                               	1,
	                                               	10,
	                                               	:dt,
	                                               	0,
	                                               	0,
	                                               	0,
	                                               	0,
	                                               	\'\',
	                                               	:credit_cost
	                                               )');

	    $stmt->bindParam(':from_user_id', $_SESSION['connected']['id']);
	    $stmt->bindParam(':to_site_id', $_POST['site_id']);
	    $doclink=$ts.".".$fileParts['extension'];
	    $time=time();
	    $stmt->bindParam(':dt', $time);
	    $stmt->bindParam(':credit_cost', $site['credit']);

	    $stmt->execute();

	    $desc="Règle du site : \n".$site['regle']."\n\n";
	    $desc.="Consigne du client : \n".$_POST['consignes']."\n\n";
	    
	    //$desc.="Liens à effectuer :\n";

	    $links_array=array();
	    for($x=0; $x<=$site['nb_liens_max']; $x++){
	    	if($_POST['url_'.$x]){
	    		//$desc.="Un lien vers : ".$_POST['url_'.$x]." avec l'ancre suivante : ".$_POST['anchor_'.$x]."\n";
	    	$links_array[] = array("url" => $_POST['url_'.$x],"anchor"=>$_POST['anchor_'.$x]);
	    	}

	    }
		$la=serialize($links_array);

	    $stmt = $dbh->query("INSERT INTO `redaction_projects` (`id`, `parent_id`, `webmaster_id`, `affectedTO`, `desc`, `article_title`, `instructions`, `words_count`,`img_count`,`links_array`, `title`, `meta_title`, `meta_desc`, `H1_set`, `H2_start`, `H2_end`, `H3_start`, `H3_end`, `bold_text`, `UL_set`, `seo_percent_start`, `seo_percent_end`, `seo_meta_title`, `seo_H1_set`, `seo_H2_set`, `seo_H3_set`, `is_template`, `amount`, `images_count_from`, `images_count_to`, `status`, `viewed`, `adminApproved`, `affected_time`, `created_time`, `admin_approved_time`, `optimized`, `prop_id`) VALUES (null, '0', '".$_SESSION['connected']['id']."', '0', ".$dbh->quote($desc).", '', '', ".$dbh->quote($site['nb_mots_max']).", ".$dbh->quote($site['nb_images_min']).", ".$dbh->quote($la).", ".$dbh->quote("Proposition échange d'article").", 1, 1, 1, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$cout_red', '0', '0', 'waiting', '0', '0', NULL, NOW(), NULL, '1',LAST_INSERT_ID())");

}

?>