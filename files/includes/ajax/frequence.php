<optgroup label="En fonction de jours">
    <option value="1">1 annuaire par jour</option>
    <option value="2">1 annuaire tous les 2 jours</option>
    <option value="3">1 annuaire tous les 3 jours</option>
    <option value="4">1 annuaire tous les 4 jours</option>
    <option value="5">1 annuaire tous les 5 jours</option>
    <option value="6">1 annuaire tous les 6 jours</option>

    <option value="7">1 annuaire par semaine</option>
    <option value="14">1 annuaire Chaque 2 semaines</option>
    <option value="21">1 annuaire Chaque 3 semaines</option>

    <option value="30">1 annuaire par mois</option>
    <option value="60">1 annuaire Chaque 2 mois</option>
    <option value="90">1 annuaire Chaque 3 mois</option>

</optgroup>
<optgroup label="Nombre de fois par jour">
    <?PHP
    $lim = 2;
    while ($lim <= 5) {
        ?>
        <option value="x<?PHP echo $lim; ?>"><?PHP echo $lim; ?> annuaire(s) par jour</option>
        <?PHP
        $lim++;
    }
    ?>

</optgroup>
