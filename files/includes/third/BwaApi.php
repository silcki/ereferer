<?php

class BwaApi
{

        const API_URL = "http://api.bulk-whois-api.com/api/";

        private $key;
        private $secret;

        public function __construct($key, $secret)
        {
                $this->key = $key;
                $this->secret = $secret;
        }

        /**
         * @param $action
         * @param array $params
         * @return array
         * @throws Exception
         */
        private function sendRequest($action, array $params = array())
        {
                $result = null;

                $postFields = http_build_query($params);
                $dateTime = new \DateTime('now', new \DateTimeZone('UTC'));
                $time = $dateTime->format('Y-m-d H:i:s');

                $message = $this->key . $time . $postFields;
                $signature = hash_hmac('sha512', $message, $this->secret);

                // generate extra headers
                $headers = array(
                        'Sign: ' . $signature,
                        'Time: ' . $time,
                        'Key: ' . $this->key
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_URL, self::API_URL . $action);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                $res = curl_exec($ch);

                if ($res === false) {
                        throw new \Exception('Could not get a reply: ' . curl_error($ch));
                } else {
                        $result = json_decode($res, true);

                        if ($result === null) {
                                throw new \Exception('Invalid response received.');
                        }
                }

                return $result;
        }

        public function authTest()
        {
                $response = $this->sendRequest("authTest");
                $result = new BwaApiAuthTest($response);

                return $result;
        }

        public function accountInfo()
        {
                $response = $this->sendRequest("info");
                $result = new BwaApiInfo($response);

                return $result;
        }

        public function whoisQuerySynchronous($query)
        {
                $response = $this->sendRequest("query", array('query' => $query));
                $result = new BwaApiWhoisQuery($response);

                return $result;
        }

        public function whoisQueryAsynchronous($query, $callbackUrl)
        {
                $response = $this->sendRequest("query", array('query' => $query, "asyncCallback" => $callbackUrl));
                $result = new BwaApiResponse($response);

                return $result;
        }

        /**
         * @param string $query
         * @return bool|BwaApiWhoisQuery  false on error
         * @throws Exception
         */
        public function whoisQueryPolling($query)
        {
                $result = false;

                $response = $this->sendRequest("query", array('query' => $query, 'polling' => "1"));
                $pollingResponse = new BwaApiPollResponse($response); // may throw exception
                $url = $pollingResponse->resultUrl;

                $done = false;
                while (!$done) {

                        sleep(5);
                        $json = file_get_contents($url);
                        $response = json_decode($json, true);

                        if ($response === null) {
                                throw new \Exception("Invalid JSON.");
                        } else {
                                $result = new BwaApiWhoisQuery($response);

                                if (!$result->success && $result->message === "Pending.") {
                                        # Not finished yet.
                                } else {
                                        $done = true;
                                }
                        }
                }

                return $result;
        }

}

class BwaApiResponse
{
        /** @var int */
        public $success;
        /** @var string|null */
        public $message;

        public function __construct(array $data)
        {
                foreach ($data as $key => $value) {

                        if (!property_exists($this, $key)) {
                                throw new \Exception("Invalid property {$key}.");
                        }

                        $this->{$key} = $value;
                }
        }

}

class BwaApiPollResponse extends BwaApiResponse
{
        /** @var string */
        public $resultUrl;
}

class BwaApiAuthTest extends BwaApiResponse
{

}

class BwaApiInfo extends BwaApiResponse
{
        /** @var string */
        public $email;
        /** @var int */
        public $requestsLeft;
}

class BwaApiWhoisQuery extends BwaApiResponse
{
        /** @var BwaApiWhoisOutput */
        public $output;
        /** @var string[] */
        public $rawOutput;

        public function __construct(array $data)
        {
                parent::__construct($data);

                $output = array_key_exists('output', $data) ? $data['output'] : array();
                $this->output = new BwaApiWhoisOutput($output);
        }

}

class BwaApiWhoisOutput
{
        /** @var string */
        public $domain;
        /** @var string */
        public $domain_id;
        /** @var string[] */
        public $status;
        /** @var bool */
        public $registered;
        /** @var bool */
        public $available;
        /** @var string */
        public $created_on;
        /** @var string */
        public $updated_on;
        /** @var string */
        public $expires_on;
        /** @var BwaApiWhoisRegistrar|null */
        public $registrar;
        /** @var BwaApiWhoisContact|null */
        public $registrant_contact;
        /** @var BwaApiWhoisContact|null */
        public $admin_contact;
        /** @var BwaApiWhoisContact|null */
        public $technical_contact;
        /** @var BwaApiWhoisNameServer[] */
        public $nameservers = array();

        public function __construct(array $data)
        {
                foreach ($data as $key => $value) {

                        if (!property_exists($this, $key)) {
                                throw new \Exception("Invalid property {$key}.");
                        }

                        $this->{$key} = $value;
                }

                if (!empty($data['registrar'])) {
                        $this->registrar = new BwaApiWhoisRegistrar($data['registrar']);
                }

                foreach (array('registrant_contact', 'admin_contact', 'technical_contact') as $contactType) {
                        if (!empty($data[$contactType])) {
                                $this->{$contactType} = new BwaApiWhoisContact($data[$contactType]);
                        }
                }

                if (!empty($data['nameservers'])) {
                        $this->nameservers = array();
                        foreach ($data['nameservers'] as $nameserver) {
                                $this->nameservers[] = new BwaApiWhoisNameServer($nameserver);
                        }
                }
        }
}

class BwaApiWhoisRegistrar
{
        /** @var string */
        public $id;
        /** @var string */
        public $name;
        /** @var string */
        public $organization;
        /** @var string */
        public $url;

        public function __construct(array $data)
        {
                foreach ($data as $key => $value) {

                        if (!property_exists($this, $key)) {
                                throw new \Exception("Invalid property {$key}.");
                        }

                        $this->{$key} = $value;
                }
        }
}

class BwaApiWhoisContact
{
        /** @var string */
        public $id;
        /** @var string */
        public $name;
        /** @var string */
        public $organization;
        /** @var string */
        public $address;
        /** @var string */
        public $city;
        /** @var string */
        public $zip;
        /** @var string */
        public $state;
        /** @var string */
        public $country;
        /** @var string */
        public $country_code;
        /** @var string */
        public $phone;
        /** @var string */
        public $fax;
        /** @var string */
        public $email;
        /** @var string */
        public $created_on;
        /** @var string */
        public $updated_on;
        /** @var string */
        public $url;

        public function __construct(array $data)
        {
                foreach ($data as $key => $value) {

                        if (!property_exists($this, $key)) {
                                throw new \Exception("Invalid property {$key}.");
                        }

                        $this->{$key} = $value;
                }
        }
}

class BwaApiWhoisNameServer
{
        /** @var string|null */
        public $name;
        /** @var string|null */
        public $ipv4;
        /** @var string|null */
        public $ipv6;

        public function __construct(array $data)
        {
                foreach ($data as $key => $value) {

                        if (!property_exists($this, $key)) {
                                throw new \Exception("Invalid property {$key}.");
                        }

                        $this->{$key} = $value;
                }
        }
}

