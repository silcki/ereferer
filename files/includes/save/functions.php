<?php

Class Functions {

    public static function getAlexaRank($url = '') {
//        $domain = 'siteduzero.com';
//        $data = file_get_contents("http://www.alexa.com/siteinfo/$domain");
//        preg_match('/<a href="\/siteowners\/certify[^\"]*">([0-9, ]+)<\/a>/i', $data, $results);
//        print_r($results[0]);
        if ($url != "") {
            return '<a href="http://www.alexa.com/siteinfo/' . $url . '" target="_blank"><script type="text/javascript" src="http://xslt.alexa.com/site_stats/js/t/a?url=' . $url . '"></script></a>';
        }
    }

//    public static function getAlexaRank($url = '') {
//        $xml = simplexml_load_string(file_get_contents("http://data.alexa.com/data?cli=10&url=" . urlencode($url)));
//        return $xml ? $xml->SD->POPULARITY['TEXT'] : 'None';
//    }

    public static function getGoogleRank($url = '') {
        $url = urlencode($url);
////        require_once("google/google_pagerank.class.php");
//        define('CACHE_DIR', dirname(__FILE__));
//
//        $gpr = new GooglePageRank($url);
//        return $gpr->pagerank;
        if ($url != "") {
            return '<a href="#"><img src="http://images.supportduweb.com/prggl.gif?site=' . $url . '&style=7" alt="PageRank" /></a>';
        }
    }

    public static function timeToSet($timeCheck = 0) {
        $return = "";
        $time = time();
        $timer = strtotime(date("d-m-Y", $time));

        if ($timeCheck < $timer) {
            $rapport = ($time - $timeCheck) / 3600;
            if ($rapport > 16 && $rapport <= 24) {
                $newDate = ($timeCheck + (3600 * 24));
                $return = strtotime(date("d-m-Y", $newDate));
            } else {
                $return = $timer;
            }
        } else {
            $return = $timeCheck;
        }

        return $return;
    }

    public static function checkNameFormat($name) {
// V�rifie si la cha�ne ressemble � une URL
        if (strlen($name) > 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function backup_tables($host, $user, $pass, $name, $tables = '*') {

        $link = mysql_connect($host, $user, $pass);
        mysql_select_db($name, $link);

        //get all of the tables
        if ($tables == '*') {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while ($row = mysql_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        //cycle through
        foreach ($tables as $table) {
            $result = mysql_query('SELECT * FROM ' . $table);
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE ' . $table . ';';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
            $return.= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysql_fetch_row($result)) {
                    $return.= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return.= '"' . $row[$j] . '"';
                        } else {
                            $return.= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $return.= ',';
                        }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

        //save file
        $handle = fopen('db-backup-' . time() . '-' . (md5(implode(',', $tables))) . '.sql', 'w+');
        fwrite($handle, $return);
        fclose($handle);
    }

    public static function checkUrlFormat2($url) {
// V�rifie si la cha�ne ressemble � une URL
        if (preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $url)) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkUrlFormat_($url) {
// V�rifie si la cha�ne ressemble � une URL
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkUrlFormat($URL) {
//        $counturl = strlen($URL);
//        $pos = stripos($URL, "/");
        if ($pos !== false && (substr_count($URL, "/") == 3 || substr_count($URL, "/") == 1)) {
//            $URL = substr($URL, 0, $pos);
        }

        if (!function_exists(filter_var) || !filter_var($URL, FILTER_VALIDATE_URL)) {
            $pattern_1 = "/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.([a-z]{2,6}))(:(\d+))?\/?/i";
            $pattern_2 = "/^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.([a-zA-Z]{2,6}))(:(\d+))?\/?/i";
            $pattern_3 = "/^(([A-Z0-9][A-Z0-9_-]*)+.([a-zA-Z]{2,6}))(:(\d+))?\/?/i";
            if (preg_match($pattern_1, $URL) || preg_match($pattern_2, $URL) || preg_match($pattern_3, $URL)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static function mepd($date) {
        if (intval($date) == 0)
            return $date;

        $tampon = time();
        $diff = $tampon - $date;

        $dateDay = date('d', $date);
        $tamponDay = date('d', $tampon);
        $diffDay = $tamponDay - $dateDay;

        if ($diff < 60 && $diffDay == "�") {
            return 'Il y a ' . $diff . 's';
        } else if ($diff < 600 && $diffDay == 0) {
            return 'Il y a ' . floor($diff / 60) . 'mn et ' . floor($diff % 60) . 's';
        } else if ($diff < 3600 && $diffDay == 0) {
            return 'Il y a ' . floor($diff / 60) . 'mn';
        } else if ($diff < 7200 && $diffDay == 0) {
            return ' Il y a ' . floor($diff / 3600) . 'h et ' . floor(($diff % 3600) / 60) . 'mn';
        } else if ($diff < 24 * 3600 && $diffDay == 0) {
            return 'Aujourd\'hui � ' . date('H\h i', $date) . ' min';
        } else if ($diff < 48 * 3600 && $diffDay == 1) {
            return 'Hier � ' . date('H\h i', $date) . ' min';
        } else {
            return 'Le ' . date('d/m/Y', $date);
        }
    }

    public static function mepd__($date) {
        if (intval($date) == 0)
            return $date;

        $tampon = time();
        $diff = $date - $tampon;
        $dateDay = date('d', $date);
        $tamponDay = date('d', $tampon);
        $diffDay = $tamponDay - $dateDay;

        if ($diff < (24 * 3600)) {
            return 'aujourd\'hui ';
        } else {
            return 'le ' . date('d/m/Y', $date);
        }
    }

    public static function checkEmailFormat($email) {
// V�rifie si la cha�ne ressemble � une URL
        if (preg_match('/([\w\-]+\@[\w\-]+\.[\w\-]+)/', $email)) {
            return true;
        } else {
            return false;
        }
    }

    public static function longeur($string = "", $size = 25) {
        $newString = "";
        if (strlen($string) > $size) {
            $newString = substr($string, 0, $size);
            $newString .= "...";
        } else {
            $newString = $string;
        }

        return $newString;
    }

    public static function getCountTaskDone($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT annuaireID FROM jobs WHERE siteID ='" . $id . "'";
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[] = $retour['annuaireID'];
        }
        return $tableau;
    }

    public static function getNumberAnnuaire($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT COUNT(*) AS nbre FROM annuaires WHERE importedBy='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountRequest($id = 0, $typeUser = 3, $answer = 0) {
        global $dbh;

        $tableau = array();
        if ($id != 0) {
            $requete = "SELECT COUNT(*) AS nbre FROM requestpayment WHERE requesterID = '" . $id . "' AND type = '" . $typeUser . "' AND answer = " . $answer;
        } else {
            $requete = "SELECT COUNT(*) AS nbre FROM requestpayment WHERE type = '" . $typeUser . "' AND answer = " . $answer;
        }
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountUserByType($typeUser = 3, $active = 0) {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE typeutilisateur = " . $typeUser;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getNewUsers() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE joinTime > 0 AND active = 0";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getRecharges() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM requestpayment WHERE answer = 0";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountProjetByUser($id = 0, $isAdmin = 3, $over = 0, $adminApprouve = 1, $envoyer = 1, $affected = 1, $showAdmin = 1) {
        global $dbh;

        $tableau = array();
        if ($isAdmin == 1) {
            if ($affected == 0) {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE affectedTO =0 AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = " . $over;
            } else {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND affectedTO <> 0 AND over = " . $over;
            }
        } else if ($isAdmin == 3) {
            $requete = "SELECT * FROM projets WHERE affectedTO ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = " . $over;
        } else if ($isAdmin == 4) {
            if ($affected == 0) {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE proprietaire ='" . $id . "' AND affectedTO =0 AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = " . $over;
            } else if ($affected == 2) {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE proprietaire ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND showProprio = '" . $showAdmin . "' AND over = " . $over;
            } else {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE proprietaire ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND affectedTO <> 0 AND over = " . $over;
            }
        }
        $execution = $dbh->query($requete);
        if ($isAdmin == 3) {
            $nbrr = 0;

            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                if (strpos($retour['email'], "|")) {
                    list($timePret, $countTask) = explode("|", $retour['email']);
                    if ($countTask > 0 && $timePret <= time() && $retour['adminApprouveTime'] <= time() && Functions::isWebmasterCan($retour['proprietaire'])) {
                        $nbrr++;
                    }
                }
            }
            return $nbrr;
        } else {
            $retour = $execution->fetch();
            if ($retour) {
                return $retour['nbre'];
            } else {
                return 0;
            }
        }
    }

    public static function getAnnuaireType($valeur) {
        if ($valeur == 1) {
            $result = "Annuaires Internes";
        } else if ($valeur == 2) {
            $result = "Annuaires du Client";
        } else {
            $result = "Tous les annuaires";
        }
        return $result;
    }

    public static function getResult($valeur) {
        $result = Functions::getFrequence($valeur);

        if ($result[1] == 0 || $result[1] == "") {
            $result[1] = 1;
        }
        if ($result[0] == 0 || $result[0] == "") {
            $result[0] = 1;
        }
        return $result[0] . " annuaire(s) tous les " . $result[1] . " Jour(s)";
    }

    public static function getFrequence($valeur) {
        $frequenceDATA = explode("x", $valeur);
//        $result[] = $frequenceDATA[0];
//        $result[] = $frequenceDATA[1];
        return $frequenceDATA;
    }

    public static function getResultAdd($lastDateSoumission, $frequence, $lvl = 0) {
        if (intval($frequence) > 0) {
            $int = intval($frequence);
            $result = $lastDateSoumission + ($int * 24 * 3600);
        } else {
            $frequence = substr($frequence, 1, strlen($frequence));
            if ($lvl == $frequence) {
                $result = $lastDateSoumission + (24 * 3600);
            } else {
                $result = $lastDateSoumission;
            }
        }
        return $result;
    }

    public static function getDoublon($table, $champ, $valeur, $champ2 = "", $valeur2 = "") {
        global $dbh;

        $tableau = array();
        if ($champ2 != "" && $valeur2 != "") {
            $requete = "SELECT * FROM " . $table . " WHERE " . $champ . "='" . $valeur . "' AND " . $champ2 . "='" . $valeur2 . "'";
        } else {
            $requete = "SELECT * FROM " . $table . " WHERE " . $champ . "='" . $valeur . "'";
        }
        $execution = $dbh->query($requete)->fetch();
        if ($execution != false) {
            return true;
        } else {
            return false;
        }
    }

    public static function getCategorieName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM categories WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['libelle'];
    }

    public static function getListAnnuaireName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['libelle'];
    }

    public static function getAnnuaireNameById($id) {
        global $dbh;
        global $idUser;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id=" . $id;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['id'] = $retour['id'];
        $tableau['libelle'] = $retour['libelle'];
        $tableau['annuairesList'] = $retour['annuairesList'];
        $tableau['proprietaire'] = $retour['proprietaire'];

        return $tableau;
    }

    public static function getListAnnuaireNameById($id) {
        global $dbh;
        global $idUser;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id='" . $id . "' AND proprietaire =" . $idUser;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['libelle'] = $retour['libelle'];
        $tableau['annuairesList'] = $retour['annuairesList'];
        $tableau['proprietaire'] = $retour['proprietaire'];

        return $tableau;
    }

    public static function getListAnnuaireDataByUser($user) {
        global $dbh;
        global $idUser;
        $count = 1;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE proprietaire ='" . $user . "' ORDER BY id";
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['annuairesList'] = $retour['annuairesList'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['annuaire'];
    }

    public static function getcontrat($idTypeUser) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM contrats WHERE typeUser='" . $idTypeUser . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['typeUser'] = $retour['typeUser'];
        $tableau['contrat'] = $retour['content'];

        return $tableau;
    }

    public static function getcontratById($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM contrats WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['typeUser'] = $retour['typeUser'];
        $tableau['contrat'] = $retour['content'];

        return $tableau;
    }

    public static function getfullname($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return strtoupper($retour['nom']) . " " . $retour['prenom'];
    }

    public static function getNumberMsg($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT COUNT(1) AS nbre FROM messages WHERE lu=1 AND receiver='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();

        return $retour['nbre'];
    }

    public static function isWebmasterCan($id = 0, $compareTO = 0) {
        if ($id != 0) {
            $soldeAuteur = Functions::getSolde($id);
            $tarifWebmaster = Functions::getRemunuerationWebmaster($id);
            if ($compareTO > 0) {
                $tarifWebmaster = $compareTO;
            }

            if ($soldeAuteur >= $tarifWebmaster) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function webmasterPossibilities($id = 0) {
        $raportQ = 0;

        if ($id > 0) {
            $compteSolde = Functions::getSolde($id);
            $getFacturation = Functions::getRemunuerationWebmaster($id);

            $raportQ = round(($compteSolde / $getFacturation), 0, PHP_ROUND_HALF_DOWN);
        }

        return $raportQ;
    }

    public static function getSolde($id = 0) {
        global $idUser;
        if ($id == 0) {
            $id = $idUser;
        }
        $result = Functions::getUserInfos($id);
        if ($result) {
            return $result['solde'];
        } else {
            return "ERREUR404";
        }
    }

    public static function getRemunueration($id = 0) {
        global $dbh;
        $remu = 0;
        if ($id > 0) {
            $remu = Functions::getRemunuerationThisUser($id);
        }
        if ($remu > 0) {
            return $remu;
        } else {
            $requete = "SELECT valeur FROM parametres WHERE id='20'";
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            return $retour['valeur'];
        }
    }

    public static function getRemunuerationWebmaster($id = 0) {
        global $dbh;
        $remu = 0;
        if ($id > 0) {
            $remu = Functions::getRemunuerationThisUser($id);
        }
        if ($remu > 0) {
            return $remu;
        } else {
            $requete = "SELECT valeur FROM parametres WHERE id='26'";
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            return $retour['valeur'];
        }
    }

    public static function getRemunuerationThisUser($id) {
        global $dbh;
        $requete = "SELECT frais FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['frais'];
    }

    public static function getPaiementByIdUser($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE requesterID = '" . $id . "' AND answer = 0 ORDER BY time DESC LIMIT 1";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['type'] = $retour['type'];
            $tableau['requesterID'] = $retour['requesterID'];
            $tableau['requestAmount'] = $retour['requestAmount'];
            $tableau['answer'] = $retour['answer'];
            $tableau['answerTime'] = $retour['answerTime'];
            $tableau['time'] = $retour['time'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getPaiementById($isAdmin = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE id = '" . $isAdmin . "' AND answer = 0 ORDER BY time DESC LIMIT 1";


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['type'] = $retour['type'];
            $tableau['requesterID'] = $retour['requesterID'];
            $tableau['requestAmount'] = $retour['requestAmount'];
            $tableau['answer'] = $retour['answer'];
            $tableau['answerTime'] = $retour['answerTime'];
            $tableau['time'] = $retour['time'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getPaiementsList($isAdmin = 3) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE type = '" . $isAdmin . "' AND answer = 0 ORDER BY time ASC";
        if ($isAdmin == 1) {
            $requete = "SELECT * FROM requestpayment WHERE answer = 1 ORDER BY answerTime DESC, type ASC";
        }


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['type'] = $retour['type'];
            $tableau[$count]['requesterID'] = $retour['requesterID'];
            $tableau[$count]['requestAmount'] = $retour['requestAmount'];
            $tableau[$count]['answer'] = $retour['answer'];
            $tableau[$count]['answerTime'] = $retour['answerTime'];
            $tableau[$count]['time'] = $retour['time'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getPaiementsListMe() {
        global $dbh;
        global $idUser;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE requesterID = '" . $idUser . "' AND answer = 0 ORDER BY time ASC";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['type'] = $retour['type'];
            $tableau[$count]['requesterID'] = $retour['requesterID'];
            $tableau[$count]['requestAmount'] = $retour['requestAmount'];
            $tableau[$count]['answer'] = $retour['answer'];
            $tableau[$count]['answerTime'] = $retour['answerTime'];
            $tableau[$count]['time'] = $retour['time'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAllUsersInfos($id = 0, $isAdmin = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '" . $isAdmin . "' ORDER BY active ASC, nom ASC, prenom ASC";
        if ($isAdmin == 5) {
            $requete = "SELECT * FROM utilisateurs WHERE joinTime > 0 AND active = 0 ORDER BY id DESC";
        }


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['nom'] = $retour['nom'];
            $tableau[$count]['prenom'] = $retour['prenom'];
            $tableau[$count]['email'] = $retour['email'];
            $tableau[$count]['ville'] = $retour['ville'];
            $tableau[$count]['codepostal'] = $retour['codepostal'];
            $tableau[$count]['avatar'] = $retour['avatar'];
            $tableau[$count]['societe'] = $retour['societe'];
            $tableau[$count]['adresse'] = $retour['adresse'];
            $tableau[$count]['siteweb'] = $retour['siteweb'];
            $tableau[$count]['telephone'] = $retour['telephone'];
            $tableau[$count]['devise'] = $retour['devise'];
            $tableau[$count]['solde'] = $retour['solde'];
            $tableau[$count]['frais'] = $retour['frais'];
            $tableau[$count]['typeutilisateur'] = $retour['typeutilisateur'];
            $tableau[$count]['active'] = $retour['active'];
            $tableau[$count]['connected'] = $retour['connected'];
            $tableau[$count]['lastlogin'] = $retour['lastlogin'];
            $tableau[$count]['joinTime'] = $retour['joinTime'];
            $tableau[$count]['lastPayment'] = $retour['lastPayment'];
            $tableau[$count]['AmountLastPayment'] = $retour['AmountLastPayment'];
            $count++;
        }

        return $tableau;
    }

    public static function isEmailExist($email = "") {
        global $dbh;

        $requete = "SELECT id FROM utilisateurs WHERE email='" . htmlentities($email, ENT_QUOTES, "UTF8") . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        if (isset($retour['id'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUserInfos($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['nom'] = $retour['nom'];
            $tableau['prenom'] = $retour['prenom'];
            $tableau['email'] = $retour['email'];
            $tableau['avatar'] = $retour['avatar'];
            $tableau['ville'] = $retour['ville'];
            $tableau['codepostal'] = $retour['codepostal'];
            $tableau['societe'] = $retour['societe'];
            $tableau['adresse'] = $retour['adresse'];
            $tableau['siteweb'] = $retour['siteweb'];
            $tableau['telephone'] = $retour['telephone'];
            $tableau['devise'] = $retour['devise'];
            $tableau['solde'] = $retour['solde'];
            $tableau['frais'] = $retour['frais'];
            $tableau['tentatives'] = $retour['tentatives'];
            $tableau['typeutilisateur'] = $retour['typeutilisateur'];
            $tableau['active'] = $retour['active'];
            $tableau['connected'] = $retour['connected'];
            $tableau['lastlogin'] = $retour['lastlogin'];
            $tableau['joinTime'] = $retour['joinTime'];
            $tableau['lastPayment'] = $retour['lastPayment'];
            $tableau['AmountLastPayment'] = $retour['AmountLastPayment'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaire($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['importedBy'] = $retour['importedBy'];
            $tableau['active'] = $retour['active'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireListAllSans($id = "") {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE importedBy <> '" . $id . "' ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['importedBy'] = $retour['importedBy'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireList($count = 1) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $tableau = array();
        if (isAdmin($idTypeUser) || isSu($idTypeUser)) {
            $requete = "SELECT * FROM annuaires WHERE importedBy ='0' ORDER BY id ASC";
        } else {
            $requete = "SELECT * FROM annuaires WHERE importedBy ='0' OR importedBy ='" . $idUser . "' ORDER BY created DESC";
        }
        $execution = $dbh->query($requete);
//        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['importedBy'] = $retour['importedBy'];
            $tableau[$count]['active'] = $retour['active'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getCategoriesList() {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM categories WHERE active = 1 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getOneAnnuaireListe($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id =" . $id;
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['libelle'] = $retour['libelle'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['annuairesList'] = $retour['annuairesList'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getUserAnnuairelist($selector = 0, $UserId = -1) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $user = $idUser;
        if ($UserId > -1) {
            $user = $UserId;
        }

        $tableau = "";

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '0' ORDER BY id DESC";
        } else {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $user . "' ORDER BY id DESC";
        }

        $execution = $dbh->query($requete);
        $count = 1;

        $tableau = '<select name="annuaire" class="select"><option value="0" >Annuaires � utiliser </option>';
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $selected = "";
            if ($selector == $retour['id']) {
                $selected = "selected='selected'";
            }
            $tableau .= '<option value="' . $retour['id'] . '" ' . $selected . '>' . $retour['libelle'] . '</option>';

            $count++;
        }
        $tableau .= "</select>";
        if ($tableau == '<select name="annuaire" class="select"><option value="0" >Annuaires � utiliser </option></select>') {
            $tableau = '<p style="color:red;">Aucune liste disponible.<br/> <a href="./compte/ajout/listannuaire.html">Cliquez ici</a> pour en cr�er.</p>';
        }
        return $tableau;
    }

    public static function getUserAnnuairelistCheckCase($selector = 0) {
        global $dbh;
        global $idUser;
        $tableau = "";

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '0' ORDER BY id DESC";
        } else {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $idUser . "' ORDER BY id DESC";
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $selected = "";
            if ($selector == $retour['id']) {
                $selected = "checked='checked'";
            }
            $tableau .= '<div style="width:100%;float:left;"><input value="' . $retour['id'] . '" ' . $selected . ' type="checkbox" name="repertoire[]"  id="repertoire' . $retour['id'] . '">&nbsp;&nbsp;&nbsp;<label for="repertoire' . $retour['id'] . '" style="display:inline-block;">' . $retour['libelle'] . '</label></div>';

            $count++;
        }
        return $tableau;
    }

    public static function getAnnuaireListeList($id = 2000) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        } else {
            $who = $idUser;
        }

        if ($id != 2000) {
            $who = $id;
        }

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $who . "' ORDER BY libelle ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['annuairesList'] = $retour['annuairesList'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireAllList() {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['annuairesList'] = $retour['annuairesList'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getContratsList() {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM contrats ORDER BY typeUser ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['typeUser'] = $retour['typeUser'];
            $tableau[$count]['content'] = $retour['content'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireListById($id = 0) {
        global $dbh;

        $tableau = array();
        if (isSu() || isAdmin()) {
            $requete = "SELECT * FROM annuaires ORDER BY id ASC";
        } else {
            $requete = "SELECT * FROM annuaires WHERE importedBy='" . $id . "' ORDER BY id ASC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['importedBy'] = $retour['importedBy'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getJob($id = 0, $iduser = 0) {
        global $dbh;

        $tableau = array();
        if ($iduser != 0) {
            if ($iduser == 1000001) {
                $requete = "SELECT * FROM jobs ORDER BY id DESC";
            } else {
                $requete = "SELECT * FROM jobs WHERE affectedto='" . $iduser . "' ORDER BY id DESC";
            }
        } else {
            $requete = "SELECT * FROM jobs WHERE siteID='" . $id . "' AND adminApprouved > 1 ORDER BY id ASC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['siteID'] = $retour['siteID'];
            $tableau[$count]['annuaireID'] = $retour['annuaireID'];
            $tableau[$count]['emailSoumission'] = $retour['emailSoumission'];
            $tableau[$count]['affectedto'] = $retour['affectedto'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau[$count]['soumissionTime'] = $retour['soumissionTime'];
            $tableau[$count]['adminApprouved'] = $retour['adminApprouved'];
            $tableau[$count]['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau[$count]['payed'] = $retour['payed'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getJobByID($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM jobs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['siteID'] = $retour['siteID'];
            $tableau['annuaireID'] = $retour['annuaireID'];
            $tableau['emailSoumission'] = $retour['emailSoumission'];
            $tableau['affectedto'] = $retour['affectedto'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau['soumissionTime'] = $retour['soumissionTime'];
            $tableau['adminApprouved'] = $retour['adminApprouved'];
            $tableau['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau['payed'] = $retour['payed'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getLastJob($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM jobs WHERE siteID='" . $id . "' AND adminApprouved > 1 ORDER BY id DESC LIMIT 1";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['siteID'] = $retour['siteID'];
            $tableau['annuaireID'] = $retour['annuaireID'];
            $tableau['emailSoumission'] = $retour['emailSoumission'];
            $tableau['affectedto'] = $retour['affectedto'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau['soumissionTime'] = $retour['soumissionTime'];
            $tableau['adminApprouved'] = $retour['adminApprouved'];
            $tableau['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau['payed'] = $retour['payed'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getLastJobs($id = 0, $nbre = 1) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM jobs WHERE siteID='" . $id . "' AND adminApprouved > 1 ORDER BY id DESC LIMIT 0," . $nbre;
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['siteID'] = $retour['siteID'];
            $tableau[$count]['annuaireID'] = $retour['annuaireID'];
            $tableau[$count]['emailSoumission'] = $retour['emailSoumission'];
            $tableau[$count]['affectedto'] = $retour['affectedto'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau[$count]['soumissionTime'] = $retour['soumissionTime'];
            $tableau[$count]['adminApprouved'] = $retour['adminApprouved'];
            $tableau[$count]['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau[$count]['payed'] = $retour['payed'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function ChangeMessageStatut($id) {
        global $dbh;
        $requete = "UPDATE messages SET lu=0 WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
    }

    public static function getCommentaire($id) {
        global $dbh;
        global $idUser;
        global $idTypeUser;


        $tableau = array();
        if (isAdmin() || isSu()) {
            $requete = "SELECT * FROM commentaires WHERE id='" . $id . "'";
        } else {
            $requete = "SELECT * FROM commentaires WHERE id='" . $id . "' AND typeuser=" . $idTypeUser;
        }

        if ($typeUSSer > 0 && $idProjet > 0) {
            
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['idprojet'] = $retour['idprojet'];
            $tableau['idannuaire'] = $retour['idannuaire'];
            $tableau['user'] = $retour['user'];
            $tableau['typeuser'] = $retour['typeuser'];
            $tableau['commentaire'] = $retour['com'];
            $tableau['edited'] = $retour['edited'];
            $tableau['created'] = $retour['created'];
        }
        return $tableau;
    }

    public static function getListCommentaire($typeUSSer = 0, $idProjet = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $requete = "SELECT * FROM commentaires WHERE idprojet='" . $idProjet . "' AND typeuser=" . $typeUSSer . " ORDER BY idannuaire ASC";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['idannuaire']]['id'] = $retour['id'];
            $tableau[$retour['idannuaire']]['idprojet'] = $retour['idprojet'];
            $tableau[$retour['idannuaire']]['idannuaire'] = $retour['idannuaire'];
            $tableau[$retour['idannuaire']]['user'] = $retour['user'];
            $tableau[$retour['idannuaire']]['typeuser'] = $retour['typeuser'];
            $tableau[$retour['idannuaire']]['commentaire'] = $retour['com'];
            $tableau[$retour['idannuaire']]['edited'] = $retour['edited'];
            $tableau[$retour['idannuaire']]['created'] = $retour['created'];
        }
        return $tableau;
    }

    public static function getRandomUser($id = 0, $typeuser = 0) {
        global $dbh;
        global $idUser;

        $requete = 'SELECT id FROM utilisateurs WHERE typeutilisateur = "' . $typeuser . '" AND id <> "' . $id . '" ORDER BY RAND() LIMIT 1';
        $idRec = $id;

        while ($id == $idRec) {
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            $idRec = $retour['id'];
        }

        return $idRec;
    }

    public static function getAllCommentaire($projet, $annuaire) {
        global $dbh;

        $tableau = array();
        $tableau[$count]['totalcom'] = "";
        $requete = "SELECT * FROM commentaires WHERE idprojet='" . $projet . "' AND idannuaire ='" . $annuaire . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['typeuser']]["id"] = $retour['id'];
            $tableau[$retour['typeuser']]["user"] = $retour['user'];
            $tableau[$retour['typeuser']]["typeuser"] = $retour['typeuser'];
            $tableau[$retour['typeuser']]['edited'] = $retour['edited'];
            $tableau[$retour['typeuser']]['created'] = $retour['created'];
            $tableau[$retour['typeuser']]['commentaire'] = $retour['com'];


            $tableau['common']['idprojet'] = $retour['idprojet'];
            $tableau['common']['idannuaire'] = $retour['idannuaire'];
            $tableau['common']['totalcom'] .= $retour['com'] . PHP_EOL;
        }

        return $tableau;
    }

    public static function getMessageList($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM messages WHERE receiver='" . $id . "' AND viewReceiver=1 ORDER BY lu DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['objet'] = $retour['objet'];
            $tableau[$count]['content'] = $retour['content'];
            $tableau[$count]['sender'] = Functions::getfullname($retour['sender']);
            $tableau[$count]['receiver'] = Functions::getfullname($retour['receiver']);
            $tableau[$count]['lu'] = $retour['lu'];
            $tableau[$count]['viewSender'] = $retour['viewSender'];
            $tableau[$count]['viewReceiver'] = $retour['viewReceiver'];
            $tableau[$count]['time'] = $retour['time'];
            $count++;
        }

        return $tableau;
    }

    public static function getUserOptionList($typeUser = 3, $egal = 0, $selector = 0, $linkUser) {
        global $dbh;
        global $idUser;
        global $arrayLvl;
        $tampon = 0;
        $tampon2 = 0;
        $tableau = "";

        if ($egal == 0) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur < " . $typeUser . " AND id <> " . $idUser . " ORDER BY typeutilisateur ASC";
        } else if ($egal == 1) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '" . $typeUser . "' AND id <> " . $idUser . " ORDER BY id ASC";
        } else if ($egal == 3) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur > " . $typeUser . " AND id <> " . $idUser . " ORDER BY typeutilisateur ASC";
        }

        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {

            $selected = "";
            if ($selector == $retour['id']) {
                $selected = "selected='selected'";
            }

            if ($tampon == 0 || ($tampon != $retour['typeutilisateur'])) {
                if ($retour['typeutilisateur'] > 0) {
                    $tampon2 = 3;
                    $tampon3 = 1;
                    $tampon = $retour['typeutilisateur'];
                } else {
                    $tampon2 = 3000;
                    $tampon3 = 3000;
                }
            } else {
                $tampon2 = 1;
                $tampon3 = 0;
            }

            $var = '<option value="' . $linkUser . '' . $retour['id'] . '" ' . $selected . '>' . strtoupper($retour['nom']) . ' ' . $retour['prenom'] . '</option>';

            if ($tampon2 == 3 && $tampon4 == 1) {
                $tableau .= "</optgroup>";
                $tampon4 = 0;
            }

            if (($tampon2 == 3 && $tampon3 == 1 && $tampon4 == 0)) {
                $tableau .= "<optgroup label='" . $arrayLvl[$retour['typeutilisateur']] . "'>";
                $tampon4 = 1;
                $tampon2 = 1;
            }

            if ($tampon2 == 1) {
                $tableau .= $var;
            }
        }
        return $tableau;
    }

    public static function getCategorieList($selectedID = 0) {
        global $dbh;

        $requete = "SELECT * FROM categories WHERE active =1 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $tableau = "";
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $selected = "";
            if ($selectedID == $retour['id']) {
                $selected = "selected='selected'";
            }
            $tableau .= '<option value="' . $retour['id'] . '" ' . $selected . '>' . $retour['libelle'] . '</option>';
        }
        return $tableau;
    }

    public static function getMessageSended($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM messages WHERE sender='" . $id . "' AND viewSender=1 ORDER BY id DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['objet'] = $retour['objet'];
            $tableau[$count]['content'] = $retour['content'];
            $tableau[$count]['sender'] = Functions::getfullname($retour['sender']);
            $tableau[$count]['receiver'] = Functions::getfullname($retour['receiver']);
            $tableau[$count]['viewSender'] = $retour['viewSender'];
            $tableau[$count]['viewReceiver'] = $retour['viewReceiver'];
            $tableau[$count]['lu'] = $retour['lu'];
            $tableau[$count]['time'] = $retour['time'];
            $count++;
        }

        return $tableau;
    }

    public static function getRechercheResultats($id = 0, $type = "projets", $motsCles = "") {
        global $dbh;
        global $idTypeUser;
        if ($type == "projets") {
            if (isAdmin($idTypeUser) || isSu($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE lien LIKE '%" . $motsCles . "%' ORDER BY id DESC";
            }
            if (isReferer($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE affectedTO ='" . $id . "' AND lien LIKE '%" . $motsCles . "%' ORDER BY id DESC";
            }

            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire ='" . $id . "' AND lien LIKE '%" . $motsCles . "%' ORDER BY id DESC";
            }
        }
        if ($type == "projetsd") {
            
        }

        if ($type == "utilisateurs" && (isAdmin($idTypeUser) || isSu($idTypeUser))) {
            $requete = "SELECT * FROM utilisateurs WHERE nom ='%" . $motsCles . "%' OR prenom ='%" . $motsCles . "%' OR email ='%" . $motsCles . "%' OR telephone ='%" . $motsCles . "%' OR societe ='%" . $motsCles . "%' ORDER BY id DESC";
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            if ($type == "utilisateurs" && (isAdmin($idTypeUser) || isSu($idTypeUser))) {
                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['nom'] = $retour['nom'];
                $tableau[$count]['prenom'] = $retour['prenom'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['ville'] = $retour['ville'];
                $tableau[$count]['codepostal'] = $retour['codepostal'];
                $tableau[$count]['avatar'] = $retour['avatar'];
                $tableau[$count]['societe'] = $retour['societe'];
                $tableau[$count]['adresse'] = $retour['adresse'];
                $tableau[$count]['siteweb'] = $retour['siteweb'];
                $tableau[$count]['telephone'] = $retour['telephone'];
                $tableau[$count]['devise'] = $retour['devise'];
                $tableau[$count]['solde'] = $retour['solde'];
                $tableau[$count]['frais'] = $retour['frais'];
                $tableau[$count]['typeutilisateur'] = $retour['typeutilisateur'];
                $tableau[$count]['active'] = $retour['active'];
                $tableau[$count]['connected'] = $retour['connected'];
                $tableau[$count]['lastlogin'] = $retour['lastlogin'];
                $tableau[$count]['joinTime'] = $retour['joinTime'];
                $tableau[$count]['lastPayment'] = $retour['lastPayment'];
                $tableau[$count]['AmountLastPayment'] = $retour['AmountLastPayment'];
            }
            if ($type == "projets") {

                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['parent'] = $retour['parent'];
                $tableau[$count]['categorieID'] = $retour['categories'];
                $tableau[$count]['categorie'] = Functions::getCategorieName($retour['categories']);
                $tableau[$count]['lien'] = $retour['lien'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['proprietaire'] = $retour['proprietaire'];
                $tableau[$count]['frequence'] = $retour['frequence'];
                $tableau[$count]['annuaire'] = $retour['annuaire'];
                $tableau[$count]['consignes'] = $retour['consignes'];
                $tableau[$count]['envoyer'] = $retour['envoyer'];
                $tableau[$count]['sendTime'] = $retour['sendTime'];
                $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
                $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
                $tableau[$count]['adminRaison'] = $retour['adminRaison'];
                $tableau[$count]['over'] = $retour['over'];
                $tableau[$count]['overTime'] = $retour['overTime'];
                $tableau[$count]['affectedTO'] = $retour['affectedTO'];
                $tableau[$count]['affectedBY'] = $retour['affectedBY'];
                $tableau[$count]['affectedTime'] = $retour['affectedTime'];
                $tableau[$count]['showProprio'] = $retour['showProprio'];
                $tableau[$count]['budget'] = $retour['budget'];
            }

            if ($type == "projetss") {
                
            }


            $count++;
        }

        return $tableau;
    }

    public static function getProjetsListByUser($id = 0, $isAdmin = 3, $over = 0, $adminApprouve = 1, $envoyer = 1, $affected = 1) {
        global $dbh;
        if ($isAdmin == 3) {
            $requete = "SELECT * FROM projets WHERE affectedTO ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = '" . $over . "' ORDER BY affectedTime DESC LIMIT 5";
        }

        if ($isAdmin == 4) {
            $requete = "SELECT * FROM projets WHERE proprietaire ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = '" . $over . "' ORDER BY affectedTime DESC LIMIT 5";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['parent'] = $retour['parent'];
            $tableau[$count]['categorieID'] = $retour['categories'];
            $tableau[$count]['categorie'] = Functions::getCategorieName($retour['categories']);
            $tableau[$count]['lien'] = $retour['lien'];
            $tableau[$count]['email'] = $retour['email'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['frequence'] = $retour['frequence'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['consignes'] = $retour['consignes'];
            $tableau[$count]['envoyer'] = $retour['envoyer'];
            $tableau[$count]['sendTime'] = $retour['sendTime'];
            $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
            $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau[$count]['adminRaison'] = $retour['adminRaison'];
            $tableau[$count]['over'] = $retour['over'];
            $tableau[$count]['overTime'] = $retour['overTime'];
            $tableau[$count]['affectedTO'] = $retour['affectedTO'];
            $tableau[$count]['affectedBY'] = $retour['affectedBY'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['showProprio'] = $retour['showProprio'];
            $tableau[$count]['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getCommande($id, $idUser, $isAdmin = 0) {
        global $dbh;

        $tableau = array();
        if ($isAdmin == 1) {
            $requete = "SELECT * FROM projets WHERE id='" . $id . "'";
        } else {
            $requete = "SELECT * FROM projets WHERE id='" . $id . "' AND proprietaire='" . $idUser . "' AND showProprio=1";
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['parent'] = $retour['parent'];
            $tableau['categorieID'] = $retour['categories'];
            $tableau['categorie'] = Functions::getCategorieName($retour['categories']);
            $tableau['lien'] = $retour['lien'];
            $tableau['email'] = $retour['email'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['frequence'] = $retour['frequence'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['envoyer'] = $retour['envoyer'];
            $tableau['sendTime'] = $retour['sendTime'];
            $tableau['adminApprouve'] = $retour['adminApprouve'];
            $tableau['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau['adminRaison'] = $retour['adminRaison'];
            $tableau['over'] = $retour['over'];
            $tableau['overTime'] = $retour['overTime'];
            $tableau['affectedTO'] = $retour['affectedTO'];
            $tableau['affectedBY'] = $retour['affectedBY'];
            $tableau['showProprio'] = $retour['showProprio'];
            $tableau['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getAllCommande($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM projets WHERE proprietaire='" . $id . "' ORDER BY over ASC, adminApprouve DESC, envoyer DESC ";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['parent'] = $retour['parent'];
            $tableau[$count]['categorieID'] = $retour['categories'];
            $tableau[$count]['categorie'] = Functions::getCategorieName($retour['categories']);
            $tableau[$count]['lien'] = $retour['lien'];
            $tableau[$count]['email'] = $retour['email'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['frequence'] = $retour['frequence'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['consignes'] = $retour['consignes'];
            $tableau[$count]['envoyer'] = $retour['envoyer'];
            $tableau[$count]['sendTime'] = $retour['sendTime'];
            $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
            $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau[$count]['adminRaison'] = $retour['adminRaison'];
            $tableau[$count]['over'] = $retour['over'];
            $tableau[$count]['overTime'] = $retour['overTime'];
            $tableau[$count]['affectedTO'] = $retour['affectedTO'];
            $tableau[$count]['affectedBY'] = $retour['affectedBY'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['showProprio'] = $retour['showProprio'];
            $tableau[$count]['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireAll($id = "") {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires ORDER BY id ASC ";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['id']] = $retour['annuaire'];
            $count++;
        }

        return $tableau;
    }

    public static function getRetard($time = 0, $timePret, $dureeJour = 86400) {
        $jourRetarder = 0;
        $data = array();
        if ($time == 0) {
            $time = time();
        }
        if ($timePret > 0) {
            $retardataires = ($time - $timePret) / $dureeJour;

//        $ecxploe = explode('.', $retardataires);
//        $jourRetarder = $ecxploe[0];
//        if ($ecxploe[1] > 60) {
//            $retardataire++;
//        }
            $jourRetarder = round($retardataires);
        }

        $data[] = $retardataires;
        $data[] = $jourRetarder;
        return $data;
    }

    public static function getSpecialAllCommandeReferer($id, $default) {
        global $dbh;
        global $idTypeUser;
        global $idUser;


        $requete = "SELECT * FROM projets WHERE categories LIKE '%" . $id . "%' AND envoyer = 1 AND adminApprouve = 1 AND over = 0 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        $time = time();

        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $isOkPass = 0;
            $jourRetarder = 0;

            if ($retour['adminApprouveTime'] <= time() && ((!isReferer($idTypeUser) || (isReferer($idTypeUser) && Functions::isWebmasterCan($retour['proprietaire']))))) {
                if (strpos($retour['email'], "|")) {
                    list($timePret, $pointAffaire) = explode("|", $retour['email']);
                    if ($timePret <= time() && $pointAffaire > 0) {
                        $isOkPass = 1;
                    }
                }
            }

            if ($isOkPass == 1) {
                $infoRetard = Functions::getRetard($time, $timePret);
                $jourRetarder = $infoRetard[1];
            }

            $inside = 0;
            if ($retour['categories'] != "" && $retour['categories'] != $default) {
                $secondaireRef = explode(";", $retour['categories']);
                $secondaireRef = array_filter($secondaireRef);
                if (in_array($idUser, $secondaireRef)) {
                    $inside = 1;
                }
            }

            if ($jourRetarder > 0 && $inside == 1) {
                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['parent'] = $retour['parent'];
                $tableau[$count]['affectedSec'] = $retour['categories'];
                $tableau[$count]['categorie'] = "";
                $tableau[$count]['lien'] = $retour['lien'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['proprietaire'] = $retour['proprietaire'];
                $tableau[$count]['frequence'] = $retour['frequence'];
                $tableau[$count]['annuaire'] = $retour['annuaire'];
                $tableau[$count]['consignes'] = $retour['consignes'];
                $tableau[$count]['envoyer'] = $retour['envoyer'];
                $tableau[$count]['sendTime'] = $retour['sendTime'];
                $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
                $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
                $tableau[$count]['adminRaison'] = $retour['adminRaison'];
                $tableau[$count]['over'] = $retour['over'];
                $tableau[$count]['overTime'] = $retour['overTime'];
                $tableau[$count]['affectedTO'] = $retour['affectedTO'];
                $tableau[$count]['affectedBY'] = $retour['affectedBY'];
                $tableau[$count]['affectedTime'] = $retour['affectedTime'];
                $tableau[$count]['showProprio'] = $retour['showProprio'];
                $tableau[$count]['budget'] = $retour['budget'];
                $tableau[$count]['retard'] = $jourRetarder;
                $count++;
            }
        }

        return $tableau;
    }

    public static function getAllCommandeReferer($id, $isAdmin = 0, $param = " adminApprouveTime ASC") {
        global $dbh;
        global $idTypeUser;
        global $idUser;
        $time = time();

        if ($param == "das") {
            $param = " adminApprouveTime ASC";
        } else if ($param == "dde") {
            $param = " adminApprouveTime DESC";
        } else if ($param == "ras") {
            $param = " affectedTO ASC, adminApprouveTime ASC";
        } else if ($param == "rde") {
            $param = " affectedTO ASC, adminApprouveTime DESC";
        } else if ($param == "was") {
            $param = " proprietaire ASC, adminApprouveTime ASC";
        } else if ($param == "wde") {
            $param = " proprietaire ASC, adminApprouveTime DESC";
        } else if ($param == "lat") {
            $param = " affectedTO ASC, adminApprouveTime DESC";
        } else {
            $param = $param;
        }

        $tableau = array();
        if ($isAdmin == 1) {
            if (isReferer($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND envoyer = 1 AND adminApprouve = 1 AND over = 0 ORDER BY" . $param;
            }
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer =1 AND adminApprouve =1 AND affectedTO <> 0 AND over =0 AND showProprio = 1 ORDER BY" . $param;
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve =1 AND over =0 AND affectedTO <> 0 ORDER BY" . $param;
            }
        } else if ($isAdmin == 2) {
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire ='" . $id . "' AND envoyer =1 AND adminApprouve = 1 AND affectedTO = 0 AND over = 0 AND showProprio = 1 ORDER BY" . $param;
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE envoyer = 1 AND adminApprouve = 1 AND over = 0 AND affectedTO = 0 ORDER BY" . $param;
            }
        } else if ($isAdmin == 3) {
            if (isReferer($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND envoyer =1 AND adminApprouve =1 and over =1 ORDER BY over DESC";
            }
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer =1 AND adminApprouve =1 and over =1 AND showProprio = 1 ORDER BY over DESC";
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve =1 AND over = 1 ORDER BY over DESC";
            }
        } else if ($isAdmin == 4) {
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer =0 AND adminApprouve = 0 AND over = 0 AND showProprio = 1 ORDER BY" . $param;
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve = 0 AND over = 0 ORDER BY" . $param;
            }
        } else if ($isAdmin == 5) {
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer = 1 AND adminApprouve = 2 OR adminApprouve = 2 AND over = 0 AND showProprio = 1 ORDER BY parent ASC, id DESC";
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve = 2 AND over = 0 ORDER BY parent ASC, id DESC";
            }
        } else if ($isAdmin == 6) {
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $user = Functions::getUserInfos($id);
                if ($user['typeutilisateur'] == 4) {
                    $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND over = 0 ORDER BY" . $param . "";
                }
                if ($user['typeutilisateur'] == 3) {
                    $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND over = 0 ORDER BY" . $param . "";
                }
            }
        } else if ($isAdmin == 7) {
            if (isAdmin($idTypeUser) || isSU($idTypeUser)) {
                $user = Functions::getUserInfos($id);
                if ($user['typeutilisateur'] == 4) {
                    $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND over = 1  ORDER BY" . $param . "";
                }
                if ($user['typeutilisateur'] == 3) {
                    $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND over = 1  ORDER BY" . $param . "";
                }
            }
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $isOkPass = 0;
            $jourRetarder = 0;

            if ($retour['adminApprouveTime'] <= time() && ((!isReferer($idTypeUser) || (isReferer($idTypeUser) && Functions::isWebmasterCan($retour['proprietaire']))))) {
                if (strpos($retour['email'], "|")) {
                    list($timePret, $pointAffaire) = explode("|", $retour['email']);
                    if ($timePret <= time() && $pointAffaire > 0) {
                        $isOkPass = 1;
                    }
                }
            }

            $autreCond = 0;
            if ($isOkPass == 1) {
//                $infoRetard = Functions::getRetard($time, $timePret);
//                $jourRetarder = $infoRetard[1];
            }

            if (!empty($retour['id']) && (!isReferer($idTypeUser) || (isReferer($idTypeUser) && ($isOkPass == 1)))) {
                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['parent'] = $retour['parent'];
                $tableau[$count]['affectedSec'] = $retour['categories'];
                $tableau[$count]['categorie'] = "";
                $tableau[$count]['lien'] = $retour['lien'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['proprietaire'] = $retour['proprietaire'];
                $tableau[$count]['frequence'] = $retour['frequence'];
                $tableau[$count]['annuaire'] = $retour['annuaire'];
                $tableau[$count]['consignes'] = $retour['consignes'];
                $tableau[$count]['envoyer'] = $retour['envoyer'];
                $tableau[$count]['sendTime'] = $retour['sendTime'];
                $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
                $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
                $tableau[$count]['adminRaison'] = $retour['adminRaison'];
                $tableau[$count]['over'] = $retour['over'];
                $tableau[$count]['overTime'] = $retour['overTime'];
                $tableau[$count]['affectedTO'] = $retour['affectedTO'];
                $tableau[$count]['affectedBY'] = $retour['affectedBY'];
                $tableau[$count]['affectedTime'] = $retour['affectedTime'];
                $tableau[$count]['showProprio'] = $retour['showProprio'];
                $tableau[$count]['budget'] = $retour['budget'];
                $tableau[$count]['retard'] = $jourRetarder;
                $count++;
            }
        }

        return $tableau;
    }

    public static function getLiaisonByID($idSite) {
        global $dbh;

        $tableau = array();

        $requete = "SELECT * FROM projets WHERE parent ='" . $idSite . "' AND id=" . $idSite;


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['parent'] = $retour['parent'];
            $tableau['affectedSec'] = $retour['categories'];
            $tableau['categorie'] = "";
            $tableau['lien'] = $retour['lien'];
            $tableau['email'] = $retour['email'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['frequence'] = $retour['frequence'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['envoyer'] = $retour['envoyer'];
            $tableau['adminApprouve'] = $retour['adminApprouve'];
            $tableau['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau['over'] = $retour['over'];
            $tableau['overTime'] = $retour['overTime'];
            $tableau['affectedTO'] = $retour['affectedTO'];
            $tableau['affectedBY'] = $retour['affectedBY'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['showProprio'] = $retour['showProprio'];
            $tableau['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getCommandeByID($idSite) {
        global $dbh;

        $tableau = array();

        $requete = "SELECT * FROM projets WHERE id=" . $idSite;


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['parent'] = $retour['parent'];
            $tableau['affectedSec'] = $retour['categories'];
            $tableau['categorie'] = "";
            $tableau['lien'] = $retour['lien'];
            $tableau['email'] = $retour['email'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['frequence'] = $retour['frequence'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['sendTime'] = $retour['sendTime'];
            $tableau['envoyer'] = $retour['envoyer'];
            $tableau['adminRaison'] = $retour['adminRaison'];
            $tableau['adminApprouve'] = $retour['adminApprouve'];
            $tableau['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau['over'] = $retour['over'];
            $tableau['overTime'] = $retour['overTime'];
            $tableau['affectedTO'] = $retour['affectedTO'];
            $tableau['affectedBY'] = $retour['affectedBY'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['showProprio'] = $retour['showProprio'];
            $tableau['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getAllTranslate() {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM traductions";
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['fr'][$retour['mot_fr']] = ucfirst($retour['mot_fr']);
            $tableau['en'][$retour['mot_fr']] = ucfirst($retour['mot_en']);
        }

        return $tableau;
    }

    public static function getPub($id = 1) {
        
    }

    public static function getSocialIcons() {
        $socialData = Functions::getAllParameters("", "", $group = "social");
        $count = count($socialData);
        $start = 1;
        $social = "";
        foreach ($socialData as $key => $valeur) {
            if ($valeur['valeur'] == "") {
                $valeur['valeur'] = "#";
            } else {
                $social .= '<li><a href="' . $valeur['valeur'] . '" target="_blank"><i class="icon-social-' . $key . '"></i></a></li>';
            }
            $start++;
        }
        return $social;
    }

    public static function getParameter($parameterName) {
        global $dbh;

        $tableau = array();
        $toTranslate = strtolower($toTranslate);
        $requete = "SELECT * FROM parametres WHERE parametre'" . $parameterName . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
//        $tableau[$retour['parametre']] = $retour['valeur'];
        return $retour['valeur'];
    }

    public static function getLogo($parametre = "", $position = "top", $style = "") {

        if ($position == "top") {
            $logo = Functions::getAllParameters("logo", "valeur", "");
        } else {
            $logo = Functions::getAllParameters("logofooter", "valeur", "");
        }
        $logotext = Functions::getAllParameters("logotext", "valeur", "");
        $titre = Functions::getAllParameters("titre", "valeur", "");

        if (trim($logo['valeur']) != "") {
            return '<a href="./"" id="logo" title="' . $logotext['valeur'] . '"><img src="' . $logo['valeur'] . '" alt="' . $titre['valeur'] . '" height="50" title="' . $logotext['valeur'] . '"/></a>';
        } else {
            return '<a href="./"" id="logo" title="' . $logotext['valeur'] . '"><h1 class="logo">' . $logotext['valeur'] . '</h1></a>';
        }
    }

    public static function getAllParameters($name = "", $champ = "", $group = "") {
        global $dbh;
        $requete = "SELECT * FROM parametres";

        if ($name != "") {

            if ($champ == "") {
                $champ = "*";
            }
            $requete = "SELECT " . $champ . " FROM parametres WHERE name='" . $name . "'";
        }

        if ($group != "") {
            $requete = "SELECT * FROM parametres WHERE groupe='" . $group . "'";
        }

        $tableau = array();

        $execution = $dbh->query($requete);
        if ($name == "" || $group != "") {
            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                $tableau[$retour['name']]["id"] = $retour['id'];
                $tableau[$retour['name']]["nom"] = $retour['name'];
                $tableau[$retour['name']]["description"] = $retour['description'];
                $tableau[$retour['name']]["libelle"] = $retour['libelle'];
                $tableau[$retour['name']]["valeur"] = $retour['valeur'];
            }
        } else {
            $retour = $execution->fetch();
            if ($champ == "") {
                $tableau["id"] = $retour['id'];
                $tableau["nom"] = $retour['name'];
                $tableau["description"] = $retour['description'];
                $tableau["libelle"] = $retour['libelle'];
                $tableau["valeur"] = $retour['valeur'];
            } else {
                $tableau[$champ] = $retour[$champ];
            }
        }
        return $tableau;
    }

    public static function getPageInfos($id = 1) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM pages WHERE id=" . $id;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau["id"] = $retour['id'];
        $tableau["menu"] = $retour['menu'];
        $tableau["titre"] = $retour['titre'];
        $tableau["post"] = $retour['post'];
        $tableau["url"] = $retour['url'];
        $tableau["scriptname"] = $retour['scriptname'];
        $tableau["url"] = $retour['url'];
        $tableau["created"] = $retour['created'];

        return $tableau;
    }

    public static function getDevises($id = 1) {
        global $dbh;
        $requete = "SELECT * FROM devises WHERE active= 1 AND id=" . intval($id);
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau["id"] = $retour['id'];
        $tableau["libelle"] = $retour['libelle'];
        $tableau["code"] = $retour['code'];
        $tableau["code"] = $retour['code'];
        $tableau["taux"] = $retour['taux'];
        $tableau["symbole"] = $retour['symbole'];
        return $tableau;
    }

    public static function getPost() {
        global $dbh;
        $tableau = array();
        $requete = "SELECT * FROM post WHERE active= 1 ORDER BY createdTime DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]["id"] = $retour['id'];
            $tableau[$count]["categorie"] = $retour['categorie'];
            $tableau[$count]["attachment"] = $retour['attachement'];
            $tableau[$count]["titre"] = $retour['titre'];
            $tableau[$count]["content"] = $retour['content'];
            $tableau[$count]["keywords"] = $retour['keywords'];
            $tableau[$count]["createdTime"] = $retour['createdTime'];
            $tableau[$count]["active"] = $retour['active'];
            $tableau[$count]["created"] = $retour['created'];
            $count++;
        }
        return $tableau;
    }

    public static function getAttachement($id = "") {
        global $dbh;
        $tableau = array();
        if ($id == "") {
            $requete = "SELECT * FROM attachment created DESC";
        } else {
            $requete = "SELECT * FROM attachment created DESC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]["id"] = $retour['id'];
//            $tableau[$count]["categorie"] = $retour['categorie'];
//            $tableau[$count]["attachment"] = $retour['attachment'];
//            $tableau[$count]["titre"] = $retour['titre'];
//            $tableau[$count]["content"] = $retour['content'];
//            $tableau[$count]["keywords"] = $retour['keywords'];
//            $tableau[$count]["createdTime"] = $retour['createdTime'];
//            $tableau[$count]["active"] = $retour['active'];
//            $tableau[$count]["created"] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getUserType($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM typeutilisateur WHERE id=" . $id;
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau["id"] = $retour['id'];
            $tableau["parent"] = $retour['typeuserparent'];
            $tableau["userid"] = $retour['typeuserid'];
            $tableau["code"] = $retour['code'];
            $tableau["libelle"] = $retour['libelle'];
            $tableau["codepostal"] = $retour['codepostal'];
            $count++;
        }

        return $tableau;
    }

    public static function Pagination($count = 0, $page = 1) {

        $nbre_result = $count;

        if ($nbre_result > pagination) {

            $nb_pag = ceil($nbre_result / pagination);
            $st = 1;


            if ($page > 1) {
                $lien_page_precedent = $_SERVER['REQUEST_URI'] . "?page=" . ($page - 1);
            } else {
                $lien_page_precedent = "#";
            }

            $list_page = '<nav class="pagination"><ul>';
            $list_page.='<li class="spe2"><a href="' . $lien_page_precedent . '" >� Pr�c�dent</a></li>';

            if ($nb_pag < 12) {
                while ($st < $nb_pag + 1) {
                    if ($st == $page) {
                        $current = "current";
                    } else {
                        $current = "";
                    }
                    $list_page.='<li><a href="' . $_SERVER['REQUEST_URI'] . '?page=' . $st . '" class="' . $current . '">' . $st . '</a></li>';
                    $st++;
                }
            } else {
                $nbr_display = $config['limit_....'];

                if (($page + $nbr_display) < ($nb_pag - $nbr_display + 1)) {
                    $st = $page;
                } else {
                    $st = $nb_pag - $nbr_display - $nbr_display + 1;
                }

                if ($st > 1) {
                    $list_page.='...';
                }

                $std = $st + $nbr_display;
                $st_save = 0;

                while ($st < $std) {
                    $lien_page = $lien_pages;
                    $lien_page.=$_SERVER['REQUEST_URI'] . "?page=" . $st;
                    if ($st == $page) {
                        $current = "current";
                    } else {
                        $current = "";
                    }
                    $list_page.='<li><a href="' . $lien_page . '" class="' . $current . '">' . $st . '</a></li>';
                    $st++;
                    $st_save = $st;
                }

                if (($nb_pag - $nbr_display) > ($st_save)) {
                    $list_page.='...';
                }

                $st = $nb_pag - $nbr_display + 1;

                while ($st < $nb_pag + 1) {
                    $lien_page = $lien_pages;
                    $lien_page.=$_SERVER['REQUEST_URI'] . "?page=" . $st;
                    if ($st == $page) {
                        $current = "current";
                    } else {
                        $current = "";
                    }
                    $list_page.='<li><a href="' . $lien_page . '" class="' . $current . '">' . $st . '</a></li>';
                    $st++;
                }
            }

            if ($page < $nb_pag) {
                $lien_page_suivant = $_SERVER['REQUEST_URI'] . "?page=" . ($page + 1);
            } else {
                $lien_page_suivant = "#";
            }

            $list_page.='<li><a href="' . $lien_page_suivant . '" >Suivant �</a></li>';
            $list_page.="</ul><div class='clearfix'></div></nav>";
        }



        if ($nbre_result > pagination) {
            $limit2 = $page * pagination;
        } else {
            $limit2 = $nbre_result;
        }

        if ($page == 1) {
            $limit1 = 1;
        } else {
            $limit1 = ($limit2 - pagination) + 1;
        }

        $return[] = $limit1;
        $return[] = $limit2;
        $return[] = $list_page;
    }

    public static function userSatut($id = 0) {
        if ($id == 1) {
            return "isSu";
        } else if ($id == 2) {
            return "isAdmin";
        } else if ($id == 4) {
            return "isWebmaster";
        } else if ($id == 3) {
            return "isReferer";
        } else {
            return "isUser";
        }
    }

    static function decodage($filter, $str) {
        $filter = sha1($filter);
        $letter = -1;
        $newstr = '';
        $str = base64_decode($str);
        $strlen = strlen($str);
        for ($i = 0; $i < $strlen; $i++) {
            $letter++;
            if ($letter > 31) {
                $letter = 0;
            }
            $neword = ord($str{$i}) - ord($filter{$letter});
            if ($neword < 1) {
                $neword += 256;
            }
            $newstr .= chr($neword);
        }
        return $newstr;
    }

    static function encodage($filter, $str) {
        $filter = sha1($filter);
        $letter = -1;
        $newpass = '';
        $newstr = "";
        $strlen = strlen($str);
        for ($i = 0; $i < $strlen; $i++) {
            $letter++;
            if ($letter > 31) {
                $letter = 0;
            }
            $neword = ord($str{$i}) + ord($filter{$letter});
            if ($neword > 255) {
                $neword -= 256;
            }
            $newstr .= chr($neword);
        }
        return base64_encode($newstr);
    }

    static function personalEncodage($str) {
        return sha1(md5(md5(sha1(base64_encode($str)))));
    }

}

function isSu($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isSu") {
        return true;
    } else {
        return false;
    }
}

function isAdmin($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isAdmin") {
        return true;
    } else {
        return false;
    }
}

function isWebmaster($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isWebmaster") {
        return true;
    } else {
        return false;
    }
}

function isReferer($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isReferer") {
        return true;
    } else {
        return false;
    }
}

function refererCanAsk($send = 0) {
    global $idUser;
    global $limitepaiement;
    global $dbh;
    if (!refererHavePay()) {
        $requete = "SELECT lastPayment FROM utilisateurs WHERE id='" . $idUser . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        if (isset($retour['lastPayment'])) {
            $difference = time() - $retour['lastPayment'];
            if ($difference >= ($limitepaiement * 24 * 3600)) {
                return true;
            } else {
                if ($send == 1) {
                    $attente = 0;
                    $attente = ((($limitepaiement * 24 * 3600) - $difference) / (3600 * 24));
                    $attente = ceil($attente);
                    return $attente;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function refererHavePay() {
    global $idUser;
    global $idTypeUser;
    global $dbh;

    $requete = "SELECT * FROM requestpayment WHERE requesterID ='" . $idUser . "' AND answer =0 AND type = '" . $idTypeUser . "' LIMIT 1";
    $execution = $dbh->query($requete);
    $retour = $execution->fetch();

    if (isset($retour['answer'])) {
        return true;
    } else {
        return false;
    }
}

function isRight($list) {
    global $dbh;
    global $idUser;
    global $idTypeUser;
    global $urlBase;

    $list = explode(",", $list);
    foreach ($list as $value) {
        if ($value != "") {
            if ($idTypeUser == intval($value)) {
                $dbh->query('UPDATE utilisateurs SET active = "0" WHERE id=' . $idUser);
                header('location: ' . $urlBase . '/logout.html');
            }
        }
    }
}

function isMultiple($content) {
    if (!empty($content) && preg_match('#\\n#', $content)) {
        return true;
    } else {
        return false;
    }
}

function isMultiple_($content) {
    if (preg_match("#" . PHP_EOL . "#", $content)) {
        $tableau = explode(PHP_EOL, $content);
        return count($tableau);
    } else {
        return false;
    }
}

function traiterArray() {
    
}

function isButton() {
    global $lienButton;
    global $texteButton;
    global $widthButton;
    global $lienButton2;
    global $texteButton2;
    global $widthButton2;

    if ($lienButton != "" && $texteButton != "") {
        $val = ' <p class="p_menu" style="width:' . $widthButton . ';text-align:center"><a href="' . $lienButton . '" class="a_menu button color round">' . $texteButton . '</a></p>';
        if ($texteButton2 && $lienButton2) {
            $val .= ' <p class="p_menu" style="width:' . $widthButton2 . ';text-align:center"><a href="' . $lienButton2 . '" class="a_menu button color round">' . $texteButton2 . '</a></p>';
        }
        return $val;
    }
}

function backup_tables($host, $user, $pass, $name, $tables = '*', $adrDoc = "./") {

    $link = mysql_connect($host, $user, $pass);
    mysql_select_db($name, $link);

    //get all of the tables
    if ($tables == '*') {
        $tables = array();
        $result = mysql_query('SHOW TABLES');
        while ($row = mysql_fetch_row($result)) {
            $tables[] = $row[0];
        }
    } else {
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

    //cycle through
    foreach ($tables as $table) {
        $result = mysql_query('SELECT * FROM ' . $table);
        $num_fields = mysql_num_fields($result);

        $return.= 'DROP TABLE ' . $table . ';';
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
        $return.= "\n\n" . $row2[1] . ";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = mysql_fetch_row($result)) {
                $return.= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                    if (isset($row[$j])) {
                        $return.= '"' . $row[$j] . '"';
                    } else {
                        $return.= '""';
                    }
                    if ($j < ($num_fields - 1)) {
                        $return.= ',';
                    }
                }
                $return.= ");\n";
            }
        }
        $return.="\n\n\n";
    }

    //save file
    $handle = fopen($adrDoc . 'db-' . date("d.m.Y.H\h.i\m\i\\n", time()) . '.sql', 'w+');
    fwrite($handle, $return);
    fclose($handle);

    $monfichier = fopen($adrDoc . 'timer.php', 'w+');
    fseek($monfichier, 0);
    fputs($monfichier, '<?PHP $timerBAckup = "' . time() . '"; ?>');
    fclose($monfichier);
}

?>
