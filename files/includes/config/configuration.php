<?PHP

date_default_timezone_set('Europe/Paris');
error_reporting(1);
//echo session_id();
define('USERTIMEOUT', 0);

//unset($_SESSION['TIMEOUT']);

$langues = array('fr');

include("dbconfig.php");


define('MinLastMonth', 250);
define('MinCurrentMonth', 300);
define('ConditionBonusSurCentaineSoumissions', 300);
define('BonusSurCentaineSoumissions', 0.01);
define('BonusSurLiensTrouves', 0.02);
define('PercentLinkFounded', 75);
define('APPLICATION_ENV', 'development');
//define('APPLICATION_ENV', 'production');

define('pagination', 12);
define('limitepaiement', (30 * 24 * 3600));
define('CLEINTERNE', 'J[m@-az <PvBx0:<{jtyGG)PoA0*{-!x(}&5]FTIlf8Vi_ ,!Y31`GQkpGE$m~n_');
//define('CLEINTERNE', 'Lkd53241sdGjksd23LKod63');

try {
    $dbh = new PDO(DB_TYPE . ':host=' . DB_SERVER . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
//    $dbh->exec("SET CHARACTER SET utf8");
} catch (PDOException $e) {
    echo "<p>Erreur :" . $e->getMessage() . ": veuillez re&eacutessayez plus tard</p>";
//    exit();
}


$sous_page = isset($sous_page) ? $sous_page : "";

//if (isset($_GET['lang']) && !empty($_GET['lang']) && in_array($_GET['lang'], $langues)) {
//    $_SESSION['languageSite'] = $_GET['lang'];
//}
//

$arrayLvl = array("Administrateur", "Administrateur", "Administrateur", "Référenceur", "Webmaster", "Affiliation");
$arrayLvl_ = array("Administrateur", "Administrateur", "Administrateur", "Référenceur", "Commentaires du Client");
$statutLvl = array("Déconnecté", "Connecté");


if (isset($_SESSION['languageSite']) && $_SESSION['languageSite'] == "fr") {
    $currentLanguageIsFr = "currentLang";
    $currentLanguageIsEn = "";
} else {
    $currentLanguageIsFr = "";
    $currentLanguageIsEn = "currentLang";
}

$menuList = array(
    array("accueil" => "bienvenue"),
        )
?>