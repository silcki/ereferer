<?
if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
    error_reporting(E_ALL);
    //ini_set('errors_display',1);
}


if (isset($_SERVER['HTTP_REFERER'])) {
    ob_start("ob_gzhandler");
}
if (!isset($_SESSION['languageSite'])) {
    $_SESSION['languageSite'] = "fr";
}

if (isset($a)) {
    $_SESSION['a'] = $a;
}
if (isset($b)) {
    $_SESSION['b'] = $b;
}

if (isset($pageTilte) && $pageTilte != "ajout" && $pageTilte != "modif") {
    unset($_SESSION['POSTDOUBLON']);
}

if (preg_match("#compte/#", $_SERVER['REQUEST_URI']) && !isset($_SESSION['connected'])) {
    header("location:./../../logout.html"); die;
}


include("config/configuration.php");
include("functions.php");
include("emailsContent.php");




//error_reporting(E_ALL);

// Refactoring
require_once 'Classes/Backlinks.php';
require_once 'Classes/Comments.php';
require_once 'Classes/Likes.php';
require_once 'Classes/Jobs.php';



require_once 'Classes/RedactionLikes.php';


require_once 'Classes/Projects.php';

require_once 'Classes/RedactionProjects.php';
require_once 'Classes/HTML.php';
require_once 'Classes/User.php';

// "models"
$likesModel = new Likes();

//@require_once('phpmailer/class.phpmailer.php');
//echo $timerBAckup;

//var_dump(strpos($_SERVER['SERVER_NAME'],'boobase.com')!==false); die;


// REDACTION module
$isRedaction = (strpos($_SERVER['REQUEST_URI'],'redaction'));
if ($isRedaction) {
   //$a = -100; // removing others sub-menus
   //var_dump($idUser);
}


$isEchange = (strpos($_SERVER['REQUEST_URI'],'echanges'));



//unset($_SESSION['TIMEOUT']);
if (!isset($_SESSION['TIMEOUT']) || (isset($_SESSION['TIMEOUT']) && $_SESSION['TIMEOUT'] < time())) {
    unset($_SESSION['allTrad']);
    unset($_SESSION['allParameters']);
//    $_SESSION['languageSite'] = "fr";
    $_SESSION['TIMEOUT'] = time() + USERTIMEOUT;
    $_SESSION['allParameters'] = Functions::getAllParameters();

    $styleLogin = 'style="float:right;';
    $stylelogo = "";
    if (isset($_SESSION['connected'])) {
        $stylelogo = 'style="font-size:15px;float:right;"';
        $styleLogin = 'style="float:left;"';

        $devise = Functions::getDevises($_SESSION['connected']['devise']);
        $_SESSION['devise'] = "<i class='icon-" . $devise["libelle"] . "'></i> (" . ucfirst($devise["libelle"]) . ")";
        $_SESSION['deviseSymb'] = "<i class='icon-" . $devise["libelle"] . "'></i>";

        $typeCompte = Functions::getUserType($_SESSION['connected']['typeutilisateur']);
        $_SESSION['typeCompte'] = $typeCompte["libelle"];
        $_SESSION['droit'] = $_SESSION['connected']['typeutilisateur'];

        $_SESSION['tampon']['logoText'] = $_SESSION['allParameters']['logotext']['valeur'];
        $_SESSION['tampon']['logoTop'] = Functions::getLogo();
        $_SESSION['tampon']['logoFooter'] = Functions::getLogo("", "footer");

        if ($_SESSION['connected']['avatar'] != "" && file_exists($_SESSION['avatar'])) {
            $_SESSION['avatar'] = $_SESSION['connected']['avatar'];
        } else {
            $_SESSION['avatar'] = "images/default.png";
        }
    }
}


if (isset($_SESSION['connected'])) {

    $idTypeUser = $_SESSION['connected']['typeutilisateur'];
    $idUser = $_SESSION['connected']['id'];

    // create "User"
    $userModel = new User($idUser);

    if ( !isReferer() ){
        //error_reporting(E_ALL);
        // check contact accepted from DB
        // for saving compability with "Benin  shit code"
        //$contract_accepted = intval($userModel->getParam('contractAccepted',false));

        // quick fix
        if (!$_SESSION['connected']["contractAccepted"]) {
            $_SESSION['connected']["contractAccepted"] = 1;
            $dbh->query('UPDATE utilisateurs SET contractAccepted = "' . $_SESSION['connected']["contractAccepted"] . '" WHERE id=' . $idUser);
        }

        /*
        if ( !$contract_accepted || (($_SESSION['connected']["contractAccepted"] != 1) && $a != 100) ){
            header("location:./contrat.html");
            die;
        }elseif ( $contract_accepted || ($_SESSION['connected']["contractAccepted"] == 1 && $a == 100) ){
            header("location:./accueil.html");
            die;
        }*/

    }else{

        // writer_status
        $is_annuaire_writer = $_SESSION['connected']['annuaire_writer'];
        $is_redaction_writer = $_SESSION['connected']['redaction_writer'];

        $annuaire_contract_accepted = $_SESSION['connected']["contractAccepted"];
        $redaction_contract_accepted = $_SESSION['connected']["redaction_contract_accepted"];

        // attempt to access redaction without accepted contract - redirect to contracts-page
        if ($is_redaction_writer && $isRedaction){

            if (empty($redaction_contract_accepted)){
                header("location:https://".$_SERVER['SERVER_NAME']."/compte/contrat.html"); die;
            }
        }

        // attempt to access basic section without -//-
        if (!$isRedaction && $is_annuaire_writer && !$annuaire_contract_accepted && ($a != 100) ) {
            header("location:./contrat.html");
            die;
        }

    }
    /*
    // access to annuaire section
    if ( ($common_contract_accepted != 1) || ( $is_redaction_writer && !$redaction_contract_accepted ) && $a != 100) {


            // access to redaction section
            if ($is_redaction_writer && ($_SESSION['connected']["redaction_contract_accepted"] == 0) && $isRedaction) {
              header("location:https://".$_SERVER['SERVER_NAME']."/compte/contrat.html"); die;
            }else{

                header("location:./contrat.html");
                die;
            }


    } else {


            if ($_SESSION['connected']["contractAccepted"] == 1 && $a == 100) {
                header("location:./accueil.html");
                die;
       }
    }*/

    $solde = Functions::getSolde();
    $credit = Functions::getCredit();

    if (isSuperReferer()) {
        $_SESSION['typeCompte'] = "Super - " . $_SESSION['typeCompte'];
    }
}
?>