<?php


class Account {
    private  $DB;

    function __construct(){
        global $dbh; // very bad, but..
        $this->DB = $dbh;
    }

    public function getTotalWebmastersBalance(){
        $sql = "SELECT SUM(solde) AS total
                FROM utilisateurs
                WHERE typeutilisateur=4";

        $result = $this->DB->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row['total'];
    }

} 