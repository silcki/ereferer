<?php


class RedactionLikes {
    private  $DB;
    private  $writers_worse_percent = 0.6;
    private  $writers_best_percent =  0.85;

    function __construct($id=0){
        global $dbh; // very bad, but..
        $this->DB = $dbh;
    }

    public function getWritersLikesByProjectId($id){
        $sql = "SELECT * FROM redaction_writers_likes
                WHERE project_id = ".$id;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getFavoriteWriters(){
        $sql = "SELECT t.client_id,
                       t.writer_id,
                       u.nom,
                       u.prenom
                FROM (SELECT p.webmaster_id  AS client_id,
                             wl.writer_id AS writer_id,
                             SUM(wl.value)/COUNT(wl.value) AS rating
                FROM redaction_writers_likes AS wl
                INNER JOIN redaction_projets AS p
                 ON ( wl.project_id = p.id)
                GROUP BY wl.writer_id,p.webmaster_id
                HAVING rating > 0 ) AS t
                INNER JOIN utilisateurs AS u
                  ON (u.id = t.writer_id)
                GROUP BY t.client_id,t.writer_id
                ORDER BY rating ASC";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        foreach($rows as $row){
            $out[$row['client_id']] = strtoupper($row['nom'].' '.$row['prenom']);
        }
        return $out;
    }

    public function getStatsForWriter($writer_id,$status=1){
        $sql = 'SELECT COUNT(value) AS total,
                       COUNT(DISTINCT p.webmaster_id) AS clients_cnt
                FROM redaction_writers_likes AS wl
                LEFT JOIN redaction_projects AS p
                 ON ( p.id = wl.project_id)
                WHERE writer_id = '.$writer_id.' AND value='.$status;

        $result = $this->DB->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getStatsForWriters($status){
        $sql = 'SELECT COUNT(value) AS total,
                       COUNT(DISTINCT p.webmaster_id) AS clients_cnt,
                       wl.writer_id AS writer_id
                FROM  redaction_writers_likes AS wl
                LEFT JOIN redaction_projects AS p
                 ON ( p.id = wl.project_id)
                WHERE value= '.$status.'
                GROUP BY writer_id';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[$row['writer_id']] = $row;
            }
        }
        return $out;
    }

    public function getSatisfactionRatesForWebmasters(){
        $sql = 'SELECT   SUM(CASE WHEN wl.value = 1 THEN 1 ELSE 0 END) AS likes_cnt,
                         SUM(CASE WHEN wl.value = 0 THEN 1 ELSE 0 END) AS unlikes_cnt,
                         ROUND((SUM(wl.value)/COUNT(wl.value))*100,0) AS rate,
                         p.proprietaire AS user_id
                FROM  redaction_writers_likes AS wl
                INNER JOIN redaction_projects AS p
                ON ( p.id = wl.project_id)
                GROUP BY p.proprietaire';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[$row['user_id']] = $row;
            }
        }
        return $out;
    }


    public function getWorseWritersByProjectID($project_id){
        $sql = 'SELECT wl.writer_id AS writer_id,
                       SUM(wl.value)/COUNT(wl.value) AS rating,
                       COUNT(wl.value) AS cnt
                FROM redaction_writers_likes AS wl
                INNER JOIN redaction_projects AS p
                 ON ( wl.project_id = p.id)
                WHERE p.webmaster_id = (SELECT webmaster_id
                                        FROM redaction_projects
                                        WHERE id='.$project_id.')
                GROUP BY wl.writer_id,p.webmaster_id
                HAVING (rating >= 0 AND rating <='.$this->writers_worse_percent.' AND cnt >= 3) OR ( cnt = 2 AND rating = 0 )
                ORDER BY rating ASC
                LIMIT 5';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[] = $row['writer_id'];
            }
        }
        return $out;
    }

    public function getBestWritersByProjectID($project_id){
        $sql = 'SELECT wl.writer_id AS writer_id,
                       SUM(wl.value)/COUNT(wl.value) AS rating
                FROM redaction_writers_likes AS wl
                INNER JOIN redaction_projects AS p
                 ON ( wl.project_id = p.id)
                WHERE p.webmaster_id = (SELECT webmaster_id
                                        FROM redaction_projects
                                        WHERE id='.$project_id.')
                GROUP BY wl.writer_id,p.webmaster_id
                HAVING rating >='.$this->writers_best_percent;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[] = $row['writer_id'];
            }
        }
        return $out;
    }

    public function setLike($data){
        error_reporting(E_ERROR);

        $result = $this->DB->query('DELETE FROM `redaction_writers_likes` WHERE project_id ='.intval($data['project_id']));

        if ($result){

            $sql ='INSERT INTO `redaction_writers_likes` VALUES (:project_id,
                                                               :writer_id,
                                                               :value,
                                                               :unliked_comment,
                                                               :clicked_time)';


            $stmt = $this->DB->prepare($sql);
            $clicked_time = time();
            $stmt->bindParam(":project_id",$data['project_id'],PDO::PARAM_INT);
            $stmt->bindParam(":writer_id",$data['writer_id'],PDO::PARAM_INT);
            $stmt->bindParam(":value",$data['value'],PDO::PARAM_INT);
            $stmt->bindParam(":unliked_comment",$data['unliked_comment'],PDO::PARAM_STR);
            $stmt->bindParam(":clicked_time",$clicked_time,PDO::PARAM_INT);
            $ok = $stmt->execute();

        }

        // checking worse writer & changing redaction writers
        if ($ok && !$data['value']){

            /*
            $project_id = $_POST['project_id'];
            $writer_id = $_POST['writer_id'];

            $likesModel = new Likes();
            $worse_ids = $likesModel->getWorseWritersByProjectID($project_id);
            //var_dump('writer_id',$writer_id);
            //var_dump('worse_ids',$worse_ids);

            if(!empty($worse_ids) && in_array($writer_id,$worse_ids)){
                $new_writer_id = 0;

                // check best writer
                $best_writers_ids = $likesModel->getBestWritersByProjectID($project_id);
                //var_dump('best_ids',$best_writers_ids);
                if(!empty($best_writers_ids)){
                    $new_writer_id = $best_writers_ids[array_rand($best_writers_ids)];
                }

                // get random writer
                if(!$new_writer_id){
                    $not_in_sql = implode(',',$worse_ids);
                    $result = $dbh->query('SELECT id FROM utilisateurs WHERE typeutilisateur = 3 AND id NOT IN ('.$not_in_sql.')');
                    $writers_ids = $result->fetchAll(PDO::FETCH_COLUMN,0);
                    $new_writer_id = $writers_ids[array_rand($writers_ids)];

                }

                //var_dump('new_writer_id',$new_writer_id);
                // setting new writer_id
                $currentProject = Functions::getCommandeByID($project_id);

                $timers = time();
                $timer = Functions::timeToSet($timers);

                $freq = Functions::getFrequence($currentProject['frequence']);
                $frequenceTache = $freq[0];
                $varNew = $timer . "|" . $frequenceTache;

                $ok = $dbh->query('UPDATE projets SET email = "' . $varNew . '", budget = "' . $timer . '",  affectedTO = "' . $new_writer_id. '", affectedBY = "1", affectedTime = "' . $timer . '" WHERE id=' . $currentProject['id']);

                if($ok){
                    $return = "1";
                }

            }*/
        }

        return $result;
    }


    /*
     *  Annuaire Estimated
     */
    public function getEstimatedProjectsForWriter($writer_id,$count = false,$like_state = -1,$page=1,$per_page=20,$order_by='asc'){
        $offset = ($page - 1)*$per_page;

        //error_reporting(E_ERROR);

        if ($count){
            $fields = 'COUNT(*) AS cnt';
        }else {
            $fields = 'p.*,
                       wl.`value` AS like_status,
                       wl.writer_id AS writer_id,
                       wl.webmaster_comment AS webmaster_comment,
                       rr.text AS report_text';
        }

        $sql = 'SELECT '.$fields.'
                FROM  redaction_writers_likes AS wl
                INNER JOIN redaction_projects AS p
                 ON ( p.id = wl.project_id )
                INNER JOIN redaction_reports AS rr
                 ON ( p.id = rr.project_id )
                WHERE p.`status` = "finished" AND wl.writer_id = '.$writer_id;

        // like state all/liked/unliked
        if ($like_state != -1){
            $sql.= ' AND wl.`value` = '.$like_state;
        }

        // order
        $sql.= ' ORDER BY wl.clicked_time DESC ';

        if (!$count)
            $sql.=' LIMIT '.$offset.','.$per_page;

        $result = $this->DB->query($sql);


        if ($count){
            return $result->fetch(PDO::FETCH_COLUMN,0);
        }else{
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        }
    }
}