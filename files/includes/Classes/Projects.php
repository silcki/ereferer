<?php

//error_reporting(E_ALL);

/*
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/config/configuration.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/functions.php';

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/MailProvider.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Jobs.php';
*/

$document_root = dirname(dirname(__FILE__));

require_once $document_root . '/config/configuration.php';
require_once $document_root . '/functions.php';

require_once $document_root . '/Classes/MailProvider.php';
require_once $document_root . '/Classes/Annuaire.php';
require_once $document_root . '/Classes/Jobs.php';


class Projects
{
    private $DB;
    private $site_params = array();
    private $estimate_per_day = 10;

    /**
     * Projects constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->DB = $dbh;
        $this->site_params = Functions::getAllParameters();
    }

    /*
     *  Calculate current project state
     *
     *  Tasks count
     *  Reaffecting
     *  Logic from account-details.php ( compte/details-page )
     */
    public function updateProjectState($project_id, $user_id = 0, $user_type = 'admin', $ajax = false, $cron = false)
    {
        $error_message = array();

        // default values
        $dureeJournee = 3600 * 24;
        $joursRetards = $this->site_params['delai']['valeur'];

        $LIST_ANNUAIRE_WORDS_COUNT = 0;

        // define roles types as vars
        $isReferer = ($user_type == 'writer');
        $isSu = isSu();
        $isSuperReferer = isSuperReferer();

        // if cron-job - it`s admin
        if ($cron) {
            $isSu = true;
        }


        if ($isReferer) {
            $idUser = $user_id;
        }

        $status = '';
        $out_data = array();

        $jobsModel = new Jobs;

        $getCommande = Functions::getCommandeByID($project_id);
        $count = 0;

        if ($getCommande) {
            $count = count($getCommande);
        }

        if ($count > 0 && $getCommande) {

            if ($getCommande['affectedSec'] != "" && $getCommande['affectedSec'] != $defaultHash) {
                $specialAssignement = 0;
                $secondaireRef = explode(";", $getCommande['affectedSec']);
                $secondaireRef = array_filter($secondaireRef);
                if (in_array($idUser, $secondaireRef)) {
                    $idUser = $getCommande['affectedTO'];
                    $specialAssignement = 1;
                }
            }

            if ($isReferer && ($getCommande['adminApprouveTime'] > time() || $getCommande['adminApprouveTime'] == 0)) {
                $errors_messages[] = "Ce projet n'est pas encore d�marrer ou est reporter pour plutard.<br/>";
            } else {
                $timeControl = $getCommande['adminApprouveTime'];
            }

            $varExplode = $getCommande['email'];
            $timeControl = "";
            if (strpos($varExplode, "|")) {
                list($timeControl, $pointAffaireControl) = explode("|", $varExplode);
            }
        }

        //var_dump(isAdmin() || isSu()  || ($isReferer && ($timeControl == $timeControl)));

        // main
        if ($isSuperReferer || $isSu || ($isReferer && ($timeControl == $timeControl))) {

            if ($count > 0 && (($getCommande['affectedTO'] == $idUser && $isReferer) || $isSu || $isSuperReferer)) {

                $nnu = Functions::getAnnuaireNameById($getCommande['annuaire']);
                $LIST_ANNUAIRE_WORDS_COUNT = $nnu['words_count'];


                $idAnnuaireParent = $getCommande['annuaire'];
                $projet_id = $getCommande['id'];

                $getRestarted = Functions::getRestartedSoumissionsByProcject($getCommande['id']);
                $getSupps = Functions::getPersoWebmasterCompte($getCommande['id']);
                $webmasterCan = Functions::isWebmasterCan($getCommande['proprietaire']);


                $ownerInfo = Functions::getUserInfos($getCommande['proprietaire']);
                $owner_solde = floatval($ownerInfo['solde']);

                $annuaireModel = new Annuaire();

                // generation annuaires lists
                $tableau = array();
                $temoin = array();
                $keyTab = 0;
                $noAnnuaire = 0;
                $erreurLienAnnuaire = "URL de l'Annuaire Introuvable";

                $annuairesTous = Functions::getAnnuaireAll();
                $ancreAnnuaireAll = Functions::getAncre($getCommande['id']);

                $idUserPropList = 0;
                if ($isReferer) {
                    $idUserPropList = $getCommande['proprietaire'];
                }

                $currentAnnuaireList = Functions::getOneAnnuaireListe($idAnnuaireParent, $idUserPropList);

                if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                    $annuairesFromListNew = explode(";", $currentAnnuaireList['annuairesList']);
                } else {
                    $annuairesFromListNew = array();
                }


                if (trim($getCommande['affectedSec'] != "")) {
                    $affectSecSuccess = false;
                    $changedOccur = 0;

                    $secondairesRef = explode(";", $getCommande['affectedSec']);
                    $secondairesRef = array_filter($secondairesRef);

                    if (in_array($getCommande['affectedTO'], $secondairesRef) === true) {
                        unset($secondairesRef[array_search($getCommande['affectedTO'], $secondairesRef)]);
                        $changedOccur = 1;
                    }

                    if (array_search(0, $secondairesRef) === true) {
                        unset($secondairesRef[array_search(0, $secondairesRef)]);
                        $changedOccur = 1;
                    }

                    if ($changedOccur == 1) {
                        $newSecondaire = implode(";", $secondairesRef);
                        $affectSecSuccess = $this->DB->query('UPDATE projets SET categories = "' . $newSecondaire . '" WHERE id=' . $getCommande['id']);
                        if ($affectSecSuccess) {
                            $getCommande['affectedSec'] = $newSecondaire;
                        }
                    }
                }

                $raisonArray = array();
                if ($getCommande['adminRaison'] != "" || $getCommande['adminRaison'] != ";") {
                    $raisonArray = explode(";", $getCommande['adminRaison']);
                    $raisonArray = array_filter($raisonArray);
                }

                $annuairesFromListNew = array_filter($annuairesFromListNew);

                $currentAnnuaireList = "";

                if ($idAnnuaireParent) {
                    $currentAnnuaireList = Functions::getOneAnnuaireListe($idAnnuaireParent, $idUserPropList);
                }


                if ($currentAnnuaireList != false && $currentAnnuaireList['annuairesList'] != "" && preg_match("#;#", $currentAnnuaireList['annuairesList'])) {
                    $annuairesFromList = explode(";", $currentAnnuaireList['annuairesList']);
                } else {
                    $annuairesFromList = array();
                }


                $annuairesFromListNew = $annuairesFromList;
                $annuairesFromListNew = array_filter($annuairesFromListNew);

                // fucking code
                // changing project annuaires list
                // by activite/disable annuaire
                $aEdit = 0;

                $annuaireModel = new Annuaire;
                // get not allowed annuaires
                $projectNotActiveAnnuaires = $annuaireModel->getNotAllowAnnuairesByProjectId($getCommande['id']);

                //var_dump($projectNotActiveAnnuaires);

                $disabled_annuaires = $annuaireModel->getDisabledAnnuiresByProjectId($getCommande['id']);
                $new_disabled_annuaires = array();
                foreach ($annuairesFromListNew as $annuaireThatId) {
                    if (!isset($annuairesTous[$annuaireThatId]) || (isset($annuairesTous[$annuaireThatId]) && (!Functions::checkUrlFormat($annuairesTous[$annuaireThatId]['annuaire']) || $annuairesTous[$annuaireThatId]['active'] == 0))) {
                        $aEdit++;
                        // prepare disabled annuaire
                        $disabled = array();
                        $disabled['project_id'] = $getCommande['id'];
                        $disabled['annuaire_id'] = $annuaireThatId;
                        $new_disabled_annuaires[] = $disabled;
                        unset($annuairesFromListNew[array_search($annuaireThatId, $annuairesFromListNew)]);
                    }
                }

                // checking annuaires status & removing disabled annuaires
                foreach ($annuairesTous as $annuaire) {
                    if (($annuaire['active'] == 1) && (in_array($annuaire['id'], $disabled_annuaires))) {
                        $aEdit++; // throw update annuaires list
                        $annuaireModel->removeDisabledAnnuiare($getCommande['id'], $annuaire['id']);
                        $annuairesFromListNew[] = $annuaire['id']; // return disabled annuaire to project list
                    }
                }

                //var_dump($annuairesFromListNew);
                // inserting new disabled annuaires
                if (!empty($new_disabled_annuaires)) {
                    $annuaireModel->setDisabledAnnuaires($new_disabled_annuaires);
                }
                //die;

                $contentThatListImploded = "";
                if ($aEdit > 0) {
                    $contentThatListImploded = implode(";", $annuairesFromListNew);
                    $contentThatListImploded .= ";";
                    if ($contentThatListImploded != ";") {
                        $requete = "UPDATE annuaireslist SET annuairesList = '" . $contentThatListImploded . "' WHERE id=" . intval($getCommande['annuaire']);
                        $execution = $this->DB->query($requete);
                        $currentAnnuaireList['annuairesList'] = $contentThatListImploded;
                    }
                }


                $annuairesFromListNew = array_filter($annuairesFromListNew);
                $annuCount = count($annuairesFromListNew);

                if ($getCommande['adminRaison'] == "" || $getCommande['adminRaison'] == 0 || ($getCommande['adminRaison'] != "" && (count(array_diff($annuairesFromListNew, $raisonArray)) > 0 || count(array_diff($raisonArray, $annuairesFromListNew)) > 0))) {
                    shuffle($annuairesFromListNew);
                    shuffle($annuairesFromListNew);
                    shuffle($annuairesFromListNew);

                    $annuairesFromListUpdate = implode(";", $annuairesFromListNew);
                    $annuairesFromListUpdate .= ";";
                    $this->DB->query('UPDATE projets SET adminRaison = "' . $annuairesFromListUpdate . '" WHERE id=' . $getCommande['id']);
                    $getCommande['adminRaison'] = $annuairesFromListUpdate;
                }

                $annuairesFromList = explode(";", $getCommande['adminRaison']);
                $annuairesFromList = array_filter($annuairesFromList);


                if ($getCommande['over'] == 0) {
                    $done = Functions::getCountTaskDone($getCommande['id']);
                    $doneCount = count($done);

                    if ($annuCount > 0 && $doneCount > 0) {
                        $fusion = array_diff($annuairesFromListNew, $done);
                        if (count($fusion) == 0) {
                            $this->DB->query('UPDATE projets SET over = "1", overTime = "' . time() . '", email = 0 WHERE id=' . $getCommande['id']);
                            $getCommande['over'] = 1;

                            // notice user
                            $mailer = new MailProvider();
                            $mailer->NoticeProjectIsFinished($getCommande['id']);
                        }
                    }
                }


                $countNamesAnnuaires = count($annuairesFromList);


                // rejected tasks to top
                $project_has_rejected_tasks = false;
                $rejected_task_annuaires = $jobsModel->getRejectedByProjectId($getCommande['id']);

                $rejected_cnt = $jobsModel->getCountRejectedByProjectId($getCommande['id']);

                if (count($rejected_task_annuaires) > 0) {
                    $top_rejected_tasks = array();
                    $top_rejected_tasks = $rejected_task_annuaires;
                    foreach ($annuairesFromList as $item) {
                        //$item = intval($item);
                        if (!in_array($item, $rejected_task_annuaires)) {
                            $top_rejected_tasks[] = $item;
                        }
                    }
                    $annuairesFromList = $top_rejected_tasks;
                    $project_has_rejected_tasks = true;
                }

            }

            // fill annuaires-data
            $annuaires = array();
            $startUp = 1;
            if ($countNamesAnnuaires > 0) {
                foreach ($annuairesFromList as $value) {
                    if ($value != "" && $value != NULL && $value && $value != 0) {
                        $currentData = Functions::getAnnuaire($value);

                        if ($currentData && $currentData['annuaire'] != "") {
                            $annuaires[$startUp]['id'] = $currentData['id'];
                            $annuaires[$startUp]['annuaire'] = $currentData['annuaire'];
                            $annuaires[$startUp]['webmasterAncre'] = $currentData['webmasterAncre'];
                            $annuaires[$startUp]['AnnuaireDisplayAncre'] = $currentData['webmasterAncre'];
                            $annuaires[$startUp]['page_rank'] = $currentData['page_rank'];
                            $annuaires[$startUp]['tarifW'] = $currentData['tarifW'];
                            $annuaires[$startUp]['tarifR'] = $currentData['tarifR'];
                            $annuaires[$startUp]['active'] = $currentData['active'];
                            $annuaires[$startUp]['display'] = $currentData['display'];
                            $annuaires[$startUp]['consignes'] = $currentData['consignes'];
                            $annuaires[$startUp]['importedBy'] = $currentData['importedBy'];
                            $annuaires[$startUp]['created'] = $currentData['created'];
                            $annuaires[$startUp]['link_submission'] = $currentData['link_submission'];
                            $annuaires[$startUp]['min_words_count'] = $currentData['min_words_count'];
                            $annuaires[$startUp]['max_words_count'] = $currentData['max_words_count'];
                            $startUp++;
                        }
                    }
                }
            }

            // START GENERATING/PREPARING TASKS FOR TABLE
            // get done-tasks
            $isJobbed = Functions::getJob($project_id);

            $point = count($isJobbed);

            // #8
            $array_frequence_checker = array();
            $array_frequence_not_done_checker = array();

            while ($keyTab < $point) {


                // hide not allowed annuaires
                if (isset($isJobbed[$keyTab]['annuaireID'])) {
                    // inc|dec $keyTab - don`t break logic
                    $keyTab++;
                    if (in_array($isJobbed[$keyTab]['annuaireID'], $projectNotActiveAnnuaires)) {
                        continue;
                    } else {
                        $keyTab--;
                    }
                }


                $keyTab++;
                if (isset($isJobbed[$keyTab]['annuaireID'])) {
                    //                            $currentAnnuaire = Functions::getAnnuaireName($isJobbed[$keyTab]['annuaireID']);
                    $currentData = Functions::getAnnuaire($isJobbed[$keyTab]['annuaireID']);

                    Functions::getAnnuaire($value);
                    $tableau[$keyTab]["id"] = $isJobbed[$keyTab]['id'];
                    $tableau[$keyTab]["siteID"] = $isJobbed[$keyTab]['siteID'];
                    $tableau[$keyTab]["affectedTO"] = $isJobbed[$keyTab]['affectedto'];

                    $tableau[$keyTab]["annuaire"] = $currentData['annuaire'];
                    $tableau[$keyTab]["AnnuaireDisplayAncre"] = $currentData['webmasterAncre'];
                    $tableau[$keyTab]["tarifW"] = $currentData['tarifW'] + Functions::getRemunuerationWebmaster($getCommande['proprietaire']);
                    if ($isJobbed[$keyTab]['coutWebmaster'] > 0) {
                        $tableau[$keyTab]["tarifW"] = $isJobbed[$keyTab]['coutWebmaster'];
                    }
                    $tableau[$keyTab]["tarifR"] = 0;
                    $tableau[$keyTab]["annuaireID"] = $isJobbed[$keyTab]['annuaireID'];
                    if (trim($tableau[$keyTab]["annuaire"]) == "") {
                        $tableau[$keyTab]["annuaire"] = $isJobbed[$keyTab]['annuaireURL'];
                        if (trim($tableau[$keyTab]["annuaire"]) == "") {
                            $tableau[$keyTab]["annuaire"] = $erreurLienAnnuaire;
                        }
                    }

                    $tableau[$keyTab]["soumissible"] = date("d/m/Y", $isJobbed[$keyTab]['soumissibleTime']);
                    $tableau[$keyTab]["soumissibleTimestamp"] = $isJobbed[$keyTab]['soumissibleTime'];
                    $tableau[$keyTab]["soumission"] = date("d/m/Y", $isJobbed[$keyTab]['soumissionTime']);
                    $tableau[$keyTab]["soumission_unix"] = $isJobbed[$keyTab]['soumissionTime'];
                    $tableau[$keyTab]["statut"] = 'OK';
                    $tableau[$keyTab]["dayJour"] = '0';
                    $tableau[$keyTab]["todayJob"] = $isJobbed[$keyTab]['adminApprouved'];

                    // calcualating delays
                    //$tableau[$]

                    $check_date = $tableau[$keyTab]["soumissibleTimestamp"];
                    if (!isset($array_frequence_checker[$check_date])) {
                        $array_frequence_checker[$check_date] = 1;
                    } else {
                        $array_frequence_checker[$check_date]++;
                    }

                    $temoin[] = $isJobbed[$keyTab]['annuaireID'];
                }
            }


            // table-start
            $countTableau = count($tableau);
            $keyAnnuaire = 1;
            $keyTab++;
            $countAnnuaires = count($annuaires);
            $frequenceTime = 0;
            $lastSoumissible = 0;
            $firstSoumissibleTampon = 0;
            $indic = 0;
            $retard = 0;
            $nbreJoursRetard = 0;
            $affectedTO['id'] = 0;
            $retardataires = 0;
            $terminator = 0;
            $pointAffaire = 1;
            $afficher = 0;
            $iscreated = 0;
            $todayJob = 0;
            $pointAffaireReferer = 0;
            $retardTampon = 0;
            $keyAnnuaireAuto = 0;
            $timeRounded = strtotime(date('d-m-Y', time()));

            $freq = Functions::getFrequence($getCommande['frequence']);


            $frequenceTime = (24 * $freq[1] * 3600);
            $frequenceTache = $freq[0];

            if ($pointAffaireControl) {
                $toShow = $pointAffaireControl;
            }
            //                    echo $toShow;

            $indicateur = 1;
            $addOne = 0;

            if ($countTableau == 0 || !isset($tableau[$countTableau]['soumissibleTimestamp'])) {
                $tableau[$countTableau]['soumissibleTimestamp'] = $getCommande['adminApprouveTime'];
                $tableau[$countTableau]['affectedTO'] = $getCommande['affectedTO'];
                $tableau[$countTableau]["todayJob"] = 0;
                $iscreated = 1;
            }


            // set time of last submitting task ( TABLE JOBS)
            $lastSoumissible = $tableau[$countTableau]['soumissibleTimestamp'];
            $lastaffectedTO = $tableau[$countTableau]['affectedTO'];

            if ($timeControl > 0 && $lastSoumissible < $timeControl) {
                //                        $lastSoumissible = $timeControl;
                //                        $iscreated = 1;
            }


            // check if project was reaffecteds
            $reffected_time = 0;
            $is_reaffected = $getCommande['is_reaffected'];
            $reaffecting_continue = $getCommande['reaffecting_continue'];

            if ($is_reaffected) {
                $reaffected_time = $getCommande['reaffected_time'];

            }

            //if (!$is_reaffected) {

            if (($lastSoumissible < $getCommande['affectedTime'])) {
                $lastSoumissible = $getCommande['affectedTime'];
                $iscreated = 1;
            }

            if ($lastSoumissible < $getCommande['adminApprouveTime']) {
                $lastSoumissible = $getCommande['adminApprouveTime'];
                $iscreated = 1;
            }
            // }


            // if project was stopped before and then restarted
            // start tasks from restarting-time
            //$getCommande['from_pause'] = 0;
            if (($countTableau > 0) && ($getCommande['from_pause'])) {
                $lastSoumissible = $getCommande['affectedTime'];
                // disable overriding
                $getCommande['from_pause'] = 0;
            }


            // check if writer submit taks not in order by min_date
            // if yes - override last submission time to start-project time
            if (($countTableau > 0 && (!$getCommande['from_pause']))) {
                $checking_not_order = false;
                ksort($array_frequence_checker);

                foreach ($array_frequence_checker as $task_time => $done_cnt) {
                    if (($task_time < $lastSoumissible)
                        && ($done_cnt < $frequenceTache)
                        && ($task_time > $getCommande['adminApprouveTime'])) {
                        $lastSoumissible = $task_time;
                        $checking_not_order = true;
                        break;
                    }
                }

                // if makes not in order and nothing before
                if (!$checking_not_order) {
                    if ($lastSoumissible > $getCommande['adminApprouveTime']) {
                        $lastSoumissible = $getCommande['adminApprouveTime'];
                    }
                }

            }

            // ???
            // TODO add explanation
            if (!$webmasterCan) {
                $lastSoumissible = $timeRounded;
                $iscreated = 1;
            }

            $lastSoumissibleTemoin = $lastSoumissible;
            $frequenceTacheInit = $frequenceTache;

            $pointNew = 0;
            $nextTime = 0;
            $soumissibleSave = 0;
            $currentTimeD = Functions::timeToSet(time());


            $currentTime = time();

            $TIME_SHIFT = 1 * 24 * 3600;
            $TIME_SHIFT = 0;

            $currentTimeD += $TIME_SHIFT;
            $currentTime += $TIME_SHIFT;

            //var_dump(date('Y-m-d H:i:s',$currentTime));
            $decompteTache = 0;
            $tacheFaite = count($tableau);
            //                    print_r($temoin);

            $sum_paid = 0.0;
            $sum_not_paid = 0.0;

            $checker_step_finishing = false;
            $project_delay_step = $getCommande['project_delay_step'];
            $writer_delay_step = $getCommande['writer_delay_step'];

            // TEST
            $test_time = time() + $TIME_SHIFT;
            $checker_step_finishing = false;


            $first_task_date = 0;
            $writer_status_first_set = false;

            $hidden_writer_today_tasks = array();
            $webmaster_default_task_cost = Functions::getRemunuerationWebmaster($getCommande['proprietaire']);


            $checked_first_days_delay = false;
            $first_days_delay = 0;

            if ($getCommande['over'] == 0) {
                if ($countAnnuaires > 0) {
                    while ($keyAnnuaire <= $countAnnuaires) {

                        $rejected_task = false;
                        if (isset($annuaires[$keyAnnuaire]['id']) && $annuaires[$keyAnnuaire]['active'] == 1 && !in_array($annuaires[$keyAnnuaire]['id'], $temoin) && $annuaires[$keyAnnuaire]['id'] != "") {
                            $terminator = 1;


                            $bonusAdded = 0;
                            $bonusAdded_Web = 0;

                            /* */
                            // hide not allowed annuaires
                            $annuaire_id = $annuaires[$keyAnnuaire]['id'];

                            if (in_array($annuaire_id, $projectNotActiveAnnuaires)) {
                                //var_dump($annuaire_id . ' excluded');
                                $keyAnnuaire++;
                                //$indic++;
                                continue;
                            }

                            // check if rejected task
                            // changing logice
                            if (in_array($annuaires[$keyAnnuaire]['id'], $rejected_task_annuaires)) {
                                $rejected_task = true;
                                //var_dump('Rejected Task = '.$annuaires[$keyAnnuaire]['id']);
                            }

                            /*
                            * Check if owner has enough money for task
                            */

                            //if ($isReferer) {
                            if ($getSupps[$projet_id] && in_array($annuaires[$keyAnnuaire]['id'], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                                $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                                if ($bonusAdded_Web == 0) {
                                    $bonusAdded_Web = $ComptePersoWebmaster;
                                }
                            }
                            $owner_task_cost = $annuaires[$keyAnnuaire]['tarifW'] + Functions::getRemunuerationWebmaster($annuaires[$keyAnnuaire]["proprietaire"]) + $bonusAdded_Web;

                            // add extra cost for task
                            $extra_webmaster_cost = 0.0;

                            if ($LIST_ANNUAIRE_WORDS_COUNT > 0) {
                                $extra_webmaster_cost = $annuaireModel->calculateExtraPrice($annuaires[$keyAnnuaire], $LIST_ANNUAIRE_WORDS_COUNT);
                                if ($extra_webmaster_cost > 0) {
                                    $owner_task_cost += $extra_webmaster_cost;
                                }
                            }


                            // check if possible for standard task count
                            // save it for writer-checking
                            if ($webmasterCan) {
                                $hidden_writer_today_tasks[$annuaires[$keyAnnuaire]['id']] = $owner_task_cost;
                            }


                            if (($owner_task_cost > $owner_solde) && $isReferer) {

                                if ($rejected_task) {
                                    $rejected_key = array_search($annuaires[$keyAnnuaire]['id'], $rejected_task_annuaires);
                                    //var_dump($rejected_key);
                                    unset($rejected_task_annuaires[$rejected_key]);
                                }

                                $keyAnnuaire++;
                                //echo 'Hide '.$annuaires[$keyAnnuaire]['id'].' '.$owner_task_cost.' '.$annuaires[$keyAnnuaire]['annuaire'].'<br/>';
                                continue;
                            }
                            //}

                            /* BUG "3 days later" FIXED */
                            if (!$rejected_task) {
                                // "shift"-increasing current date
                                if (!isset($array_frequence_checker[$lastSoumissible])) {
                                    $array_frequence_checker[$lastSoumissible] = 1;
                                } else {
                                    //$shift_future = $array_frequence_checker[$lastSoumissible];
                                    //$shift_future++;
                                    //if ()
                                    //if ($array_frequence_checker[$lastSoumissible] < $frequenceTacheInit)
                                    $array_frequence_checker[$lastSoumissible]++;
                                }

                                // skip date for done tasks if done cnt = frequence per day
                                if (isset($array_frequence_checker[$lastSoumissible]) && ($array_frequence_checker[$lastSoumissible] > $frequenceTacheInit)) {
                                    $checker_step_finishing = false;
                                    $array_frequence_checker[$lastSoumissible]--;
                                    $lastSoumissible = $lastSoumissible + $frequenceTime;
                                    continue;
                                }

                                // calculating current date/range for task
                                if (($iscreated == 1) || ($keyAnnuaire == 1)) { // "NULL-project" or first task
                                    //$lastSoumissible = $lastSoumissible;
                                    $iscreated = 2;
                                    $first_task_date = $lastSoumissible;
                                }

                            }


                            if ($tacheFaite > 0 && ($indic == 0 || $decompteTache > 0)) {
                                // $tableau[$keyTab]["soumissibleTimestamp"]
                            }

                            // affected not paid
                            $tableau[$keyTab]["id"] = $annuaires[$keyAnnuaire]['id'];
                            $tableau[$keyTab]["siteID"] = $getCommande['id'];
                            $tableau[$keyTab]["affectedTO"] = $getCommande['affectedTO'];
                            $tableau[$keyTab]["annuaire"] = $annuaires[$keyAnnuaire]['annuaire'];
                            $tableau[$keyTab]["annuaireDisplay"] = $annuaires[$keyAnnuaire]['display'];
                            $tableau[$keyTab]["consignesAnnuaire"] = $annuaires[$keyAnnuaire]['consignes'];
                            $tableau[$keyTab]["annuaireID"] = $annuaires[$keyAnnuaire]['id'];
                            $tableau[$keyTab]["AnnuaireDisplayAncre"] = $annuaires[$keyAnnuaire]['webmasterAncre'];
                            $tableau[$keyTab]["DejaPaid"] = 0;
                            $tableau[$keyTab]["tarifW"] = $annuaires[$keyAnnuaire]['tarifW'] + $remuneration;
                            $tableau[$keyTab]["tarifR"] = $annuaires[$keyAnnuaire]['tarifR'];


                            $tableau[$keyTab]["soumissible"] = date("d/m/Y", $lastSoumissible);
                            $tableau[$keyTab]["soumissibleTimestamp"] = $lastSoumissible;


                            if ($rejected_task) {
                                $tableau[$keyTab]["soumissible"] = date("d/m/Y", time());
                                $tableau[$keyTab]["soumissibleTimestamp"] = time();
                            }

                            $tableau[$keyTab]["link_submission"] = $annuaires[$keyAnnuaire]['link_submission'];

                            // annuaire min/max word counts
                            $tableau[$keyTab]["min_words_count"] = $annuaires[$keyAnnuaire]['min_words_count'];
                            $tableau[$keyTab]["max_words_count"] = $annuaires[$keyAnnuaire]['max_words_count'];

                            $tableau[$keyTab]["soumission"] = "Indisponible";
                            $tableau[$keyTab]["statut"] = 'NOK';
                            $sum_not_paid += $tableau[$keyTab]['tarifW'];
                            if (in_array($tableau[$keyTab]["annuaireID"], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                                $sum_not_paid += $getSupps[$projet_id]['costWebmaster'];
                                if ($getSupps[$projet_id]['costWebmaster'] == 0) {
                                    $sum_not_paid += $ComptePersoWebmaster;
                                }
                                //                                        $sum_not_paid += $ComptePersoWebmaster;
                            }


                            if ($project_has_rejected_tasks) {
                                if (in_array($tableau[$keyTab]["id"], $rejected_task_annuaires)) {
                                    $tableau[$keyTab]["todayJob"] = 1;
                                    $toShow++;
                                }
                            }

                            //if ($lastSoumissible <= time() || ($lastSoumissible > time() && date("d/m/Y", $lastSoumissible) == date("d/m/Y", time()))) {
                            if ($rejected_task || ($lastSoumissible <= $test_time) || ($lastSoumissible > $test_time && date("d/m/Y", $lastSoumissible) == date("d/m/Y", $test_time))) {
                                $tableau[$keyTab]["todayJob"] = 1;
                                $toShow++;

                            }

                            if (!$webmasterCan) {
                                $tableau[$keyTab]["todayJob"] = 0;
                                $tableau[$keyTab]["soumissible"] = "Indisponible";
                            }

                            if ($keyAnnuaireAuto == 0) {
                                $keyAnnuaireAuto = 1;
                                $nextTime = $lastSoumissible;
                            }

                            if ($tableau[$keyTab]["todayJob"] == 1) {
                                $pointAffaire++;
                                $todayJob++;
                            }

                            // Calcilating  delays-periods
                            // Override
                            if ($tableau[$keyTab]["todayJob"] == 1) {

                                // OLD BEHAVIOUR
                                // calculating delay-days_count
                                $infoRetard = Functions::getRetard($currentTime, $tableau[$keyTab]["soumissibleTimestamp"], $dureeJournee);
                                $retardataireDta = $infoRetard[0];
                                $retardataires = $infoRetard[1];
                                $nbreJoursRetards = $retardataires;

                                if ($nbreJoursRetards > 0) {
                                    //var_dump($nbreJoursRetards);
                                }

                                // calculating $writer_delay_step
                                $basic_delay = $retardataires;

                                // override "default" calculation retard - days count
                                // by current writer_step
                                if ($is_reaffected) {
                                    $reaffRetard = Functions::getRetard($currentTime, $reaffected_time, $dureeJournee);

                                    $reaffected_delay = $reaffRetard[1];
                                    //var_dump($reaffected_delay);
                                    //echo 'Reaffected Delay = '.$reaffected_delay.'<br/>';
                                    $writer_delay_step = $reaffected_delay;

                                    if ($writer_delay_step > 0) { // action-continue reaffecting
                                        $diff_delay = $basic_delay - $writer_delay_step;

                                        //$out = '<br/>'.$tableau[$keyTab]["soumissible"].' ---  LW = '.$writer_delay_step.'  '.$current_delay.' '.$diff_delay.'<br/>';
                                        //echo $out;

                                        if (($basic_delay > $writer_delay_step)) { // reaffecting day there!
                                            $nbreJoursRetards = $writer_delay_step;
                                        } else {
                                            $nbreJoursRetards = $basic_delay;
                                        }

                                    } else {  // after writer`s reseting
                                        $nbreJoursRetards = 0;
                                    }
                                }

                                //var_dump($nbreJoursRetards);

                                if ($retardTampon == 0) {
                                    $retardTampon = $nbreJoursRetards;
                                }

                                //var_dump($nbreJoursRetards);


                                if ($nbreJoursRetards > 0) {
                                    $tableau[$keyTab]["soumission"] = "<span style='color:red'>" . $nbreJoursRetards . " jour(s) de retard.</span>";
                                } else {
                                    $tableau[$keyTab]["soumission"] = "<span style='color:green'>T�che du jour</span>";
                                }

                                //echo $tableau[$keyTab]["soumission"];

                                if ($ajax && !$writer_status_first_set) {
                                    $out_data['writer_task_status'] = $tableau[$keyTab]["soumission"];
                                    $writer_status_first_set = true;
                                }

                                $tableau[$keyTab]["dayJour"] = $nbreJoursRetards;

                                // save first delay days value
                                if (!$checked_first_days_delay) {
                                    $checked_first_days_delay = true;
                                    $first_days_delay = $nbreJoursRetards;
                                }
                            }

                            if ($lastSoumissible < time()) {
                                //echo date("d/m/Y", $lastSoumissible).'<br/>';
                            }

                            $keyTab++;
                        }

                        $keyAnnuaire++;
                        $indic++;
                    }

                    //die;

                    // REAFFECTING USER
                    // if retard-delay is more
                    if ($getCommande['over'] == 0) {

                        if ($getCommande['over'] == 0) {

                            if ($retardTampon > 0) {
                                $retard = 1;

                                // changing project`s writer
                                // if delay-days limit is exceed

                                // TEST
                                //$getCommande['is_fixed_writer'] = true;
                                if (($retardTampon >= $joursRetards) && (!$getCommande['is_fixed_writer'])) {

                                    if ($varNew == "") {
                                        $varNew = $nextTime . "|" . $frequenceTache;
                                    }


                                    //var_dump($nextTime,$frequenceTache);
                                    $affectedTOnew = Functions::getRandomUser($getCommande['affectedTO'], 3, $getCommande['affectedSec']);

                                    //$affectedTO = 593;

                                    //var_dump($_SESSION['test_details_reaffecting'],$affectedTOnew);
                                    //die;

                                    //var_dump("REAFFECTING"); die;

                                    $this->DB->query('UPDATE projets
                                                         SET email = "' . $varNew . '",
                                                             affectedTO = "' . $affectedTOnew . '",
                                                             affectedBY = "1",
                                                             affectedTime = "' . $currentTimeD . '" ,
                                                             is_reaffected = 1,
                                                             reaffected_time = "' . $currentTimeD . '" ,
                                                             writer_delay_step = 0,
                                                             from_pause = 0
                                                         WHERE id=' . $getCommande['id']);


                                    if ($ajax) {
                                        $out_data['status'] = 'reaffection';
                                        $out_data['new_writer_id'] = $affectedTOnew;
                                    }

                                }

                                if (!$webmasterCan && ($currentTime - $getCommande['affectedTime'] > ($dureeJournee))) {
                                    $webAffected = $this->DB->query('UPDATE projets SET affectedTime = "' . $currentTimeD . '" WHERE id=' . $getCommande['id']);
                                    $getCommande['affectedTime'] = $currentTime;
                                }
                            }
                        }

                        //var_dump('UPDATED'); die;


                        $varJobs = "";
                        if ($todayJob > 0) {
                            $timePut = $nextTime;
                            $jobPut = $todayJob;
                        }

                        if ($todayJob == 0) {
                            $timePut = $nextTime;
                            $jobPut = $frequenceTache;
                        }

                        // check if possible old "standard" possability
                        // but not possible with extra cost ( min-max annuaire )
                        if (!empty($hidden_writer_today_tasks) && $webmasterCan) {
                            $count_possibility = 0;

                            foreach ($hidden_writer_today_tasks as $annuaire_id => $cost) {
                                if ($owner_solde > $cost)
                                    $count_possibility++;
                            }

                            // update project "status `On pause`" - not enough money
                            if ($count_possibility == 0) {
                                $webAffected = $this->DB->query('UPDATE projets SET `not_enough_money` = 1 WHERE id=' . $getCommande['id']);
                                $webmasterCan = false;
                            } else {
                                $webAffected = $this->DB->query('UPDATE projets SET `not_enough_money` = 0 WHERE id=' . $getCommande['id']);
                            }
                        }

                        //var_dump('TOTAL = '.$jobPut.'<br/>');
                        if (!$webmasterCan) {
                            if ($timePut < $timeRounded) {
                                $timePut = $timeRounded;
                            }
                        } else {
                            $raportQ = Functions::webmasterPossibilities($getCommande['proprietaire']);

                            //var_dump('MAX possible ='.$raportQ.'<br/>');
                            if ($raportQ <= $pointAffaire) {
                                $jobPut = $raportQ;
                            }
                        }
                        //  echo $jobPut;

                        $varJobs = $timePut . "|" . $jobPut;

                        $setter = false;

                        //var_dump($varJobs);

                        //                                echo $varJobs;
                        $setter = $this->DB->query('UPDATE projets
                                                    SET    email = "' . $varJobs . '",
                                                           writer_delay_step = ' . $first_days_delay . '
                                                    WHERE id=' . $getCommande['id']);

                        if ($setter) {
                            $getCommande['email'] = $varJobs;
                            if ($isReferer && !empty($hidden_writer_today_tasks) && ($count_possibility == 0)) {
                                $jobPut = 0;
                            }
                            $out_data['today_task_count'] = $jobPut;
                        }
                    }
                } else {
                    $nnu = Functions::getAnnuaireNameById($getCommande['annuaire']);
                    //echo "<h4>
                    // Le repertoire d'annuaires associ� � ce projet est vide. <br/>Pensez � ajouter des annuaires � la liste : " . $nnu['libelle'] . "
                    // </h4>";
                    $noAnnuaire = 1;
                }
            } else {
                if (!isset($_GET['from'])) {
                    //header('location:./details.html?from=over&id=' . $getCommande['id']);
                }
            }

            if ($getCommande['over'] == 1) {
                $out_data['status'] = 'project_finished';
            }
        }

        if ($ajax) {

        }

        // output
        if ($cron) {
            echo '<pre>';
            var_dump($project_id);
            var_dump($out_data);

            echo '</pre>';
        }

        //$out_data['status'] = 'reaffection';

        return $out_data;
    }

    /*
     *  Multiple updating project`s states by admin/writers
     */
    public function updateAllProjectsStates($user_id, $user_type, $ajax = false)
    {
        $projects_ids = $this->getActiveProjectIds($user_id, $user_type);

        //var_dump($projects_ids);
        if (!empty($projects_ids)) {
            foreach ($projects_ids as $index => $project_id) {
                $this->updateProjectState($project_id, $user_id, $user_type, $ajax);
            }
        }
        // TODO check processed project`s count
    }

    /*
     *  Update all projects states via cron
     */
    public function updateCronAllProjectsStates()
    {
        $projects_ids = array();

        $user_id = 1; // admin
        $user_type = 'admin';
        $ajax = false;
        $cron = true;

        $sql = 'SELECT id
                FROM projets
                WHERE envoyer = 1
                      AND adminApprouve = 1
                      AND over = 0
                      AND affectedTO <> 0
                ORDER BY affectedTO ASC, adminApprouveTime DESC';


        $result = $this->DB->query($sql);
        $projects_ids = $result->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($projects_ids)) {
            foreach ($projects_ids as $row) {
                $project_id = $row['id'];
                $this->updateProjectState($project_id, $user_id, $user_type, $ajax, $cron);

                // sleeping 1 second
                sleep(1);
            }
        }

    }


    /*
     *  Get "active" projects
     *
     */
    public function getActiveProjectIds($user_id, $user_type)
    {
        $activeProjects = array();
        $ids = array();

        if ($user_type == 'admin') {
            $activeProjects = Functions::getAllCommandeReferer(0, 1, $_SESSION['OrdreProjets'], 1);
        } else {
            $activeProjects = Functions::getAllCommandeReferer($user_id, 1, $_SESSION['OrdreProjets']);
        }

        if (!empty($activeProjects)) {
            foreach ($activeProjects as $index => $project) {
                $ids[] = intval($project['id']);
            }
        }
        return $ids;
    }


    public function getCronActiveProjectIds()
    {
        $ids = array();

        if (!empty($activeProjects)) {
            foreach ($activeProjects as $index => $project) {
                $ids[] = intval($project['id']);
                //var_dump($project['id']);

            }
        }
        return $ids;
    }

    public function testDetailsPage($project_id, $writer_id = 593, $start_date, $frequence = '1x1', $rea_time = "1-11-2016", $reaffecting = false)
    {
        $time = strtotime($start_date);
        $time = $time + 60 * 60 * 12;
        $affected_time = $time + 100;
        $reaffected_time = strtotime($rea_time);

        $reaffecting = ($reaffecting) ? 1 : 0;

        $sql = "UPDATE projets
                 SET  adminApprouveTime = " . $time . ",
	                  affectedTime = " . $affected_time . ",
                      affectedTO = " . $writer_id . ",
                      writer_delay_step = 0,
                      reaffected_time = " . $reaffected_time . ",
                      is_reaffected = " . $reaffecting . ",
                      frequence = '" . $frequence . "'
                 WHERE id = " . $project_id;


        $this->DB->query($sql);
    }


    public function getTopForWriterChoosing($writer_id)
    {
        $sql = "SELECT *
                FROM projets AS p
                LEFT JOIN (
                    SELECT p.proprietaire  AS client_id,
                           wl.writer_id AS writer_id,
                           SUM(wl.value)/COUNT(wl.value) AS rating
                    FROM writers_likes AS wl
                    INNER JOIN projets AS p
                     ON ( wl.project_id = p.id)
                    WHERE writer_id = " . $writer_id . "
                    GROUP BY wl.writer_id,p.proprietaire
                    HAVING rating < 0.5 AND COUNT(wl.value) > 0
                ) as rated_writers
                ON ( p.proprietaire <> rated_writers.client_id )
                WHERE p.envoyer = 1
                      AND p.adminApprouve = 1
                      AND p.over = 0
                      AND p.affectedTO = 0
                      AND p.proprietaire NOT IN (
                           SELECT id
                           FROM utilisateurs
                           WHERE writers_choosing = 1)
                ORDER BY adminApprouveTime ASC
                LIMIT 3";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getProjectsByFavouriteWriter($writer_id)
    {
        $sql = "SELECT *
                FROM projets AS p
                INNER JOIN (SELECT p.proprietaire  AS client_id,
                             wl.writer_id AS writer_id,
                             SUM(wl.value)/COUNT(wl.value) AS rating
                            FROM writers_likes AS wl
                            INNER JOIN projets AS p
                             ON ( wl.project_id = p.id)
                            GROUP BY wl.writer_id,p.proprietaire
                            HAVING rating > 0.5 ) AS rated_writers
                ON ( p.proprietaire = rated_writers.client_id )
                WHERE rated_writers.writer_id = " . $writer_id . " AND  p.envoyer = 1
                                      AND p.adminApprouve = 1
                                      AND p.over = 0
                                      AND p.affectedTO = 0
                                      AND p.proprietaire NOT IN (
                                           SELECT id
                                           FROM utilisateurs
                                           WHERE writers_choosing = 1)
                GROUP BY p.id, rated_writers.client_id,rated_writers.writer_id
                HAVING MAX(rated_writers.rating)";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }


    /*
        Get esmtimated strating-time for waiting projects
     */
    public function getEstimatedStartDates($user_id = 0, $user_type = 'admin')
    {
        $sql = "SELECT * FROM projets
                WHERE envoyer = 1 AND adminApprouve = 1 AND affectedTO = 0 AND over = 0 AND showProprio = 1";

        if ($user_id) {
            $sql .= " AND proprietaire = " . $user_id;
        }
        $sql .= " ORDER BY adminApprouveTime ASC";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        // calcuates estimated dates
        $estimated_dates = array();
        $current_time = time();
        $count_waiting = count($rows);

        foreach ($rows as $index => $row) {
            ++$index;
            $current_step = ceil($index / $this->estimate_per_day);
            $estimated_dates[$row['id']] = $current_time + $current_step * 24 * 60 * 60;
        }
        return $estimated_dates;
    }

    /*
     *  Get Last estimated
     */

    public function getEstimatedStartDaysCount()
    {
        $estimated_start_dates = $this->getEstimatedStartDates();
        $estimated_days_count = 0;
        if (!empty($estimated_start_dates)) {
            // get last
            $latest_date_unix = end($estimated_start_dates);
            $latest_date = DateTime::createFromFormat('U', $latest_date_unix);
            $now = new DateTime();
            $diff = $latest_date->diff($now);
            $estimated_days_count = $diff->d; // days cnt
        }
        return $estimated_days_count;
    }

    /*
     *  Update project start-dates. Sortable behaviour.
     */
    public function updateProjectStartDates($projects)
    {
        $count_affected = 0;
        if (!empty($projects)) {
            foreach ($projects as $project) {
                $sql = 'UPDATE projets
                         SET    adminApprouveTime = ' . $project['time'] . '
                         WHERE  envoyer = 1 AND adminApprouve = 1 AND affectedTO = 0 AND over = 0 AND showProprio = 1
                                AND id = ' . $project['id'];
                $result = $this->DB->query($sql);
                if ($result)
                    $count_affected++;
            }
        }
        return $count_affected;
    }

    /*
     *  Reset paused projects after increasing balance by owner
     */
    public function getPausedProjectsIds($user_id, $is_admin = false)
    {
        $ids = array();

        // fetching current projects for user
        $sql = 'SELECT *
                FROM projets
                WHERE proprietaire = ' . $user_id . '
                      AND envoyer =1
                      AND adminApprouve =1
                      AND affectedTO <> 0
                      AND over =0
                      AND showProprio = 1
                ORDER BY affectedTO ASC, adminApprouveTime DESC';

        $result = $this->DB->query($sql);
        $projects = $result->fetchAll(PDO::FETCH_ASSOC);

        $webmasterCan = Functions::isWebmasterCan($user_id);
        // check if "paused"
        if (!$webmasterCan && !empty($projects)) {
            foreach ($projects as $project) {
                $ids[] = $project['id'];
            }
        }
        return $ids;
    }

    public function resetPausedProjects($ids, $user_id)
    {
        if (is_array($ids)) {
            foreach ($ids as $project_id) {
                $sql = 'UPDATE projets
                            SET    is_reaffected = 1,
                                   affectedTime = UNIX_TIMESTAMP(),
                                   adminApprouveTime = UNIX_TIMESTAMP(),
                                   reaffected_time = UNIX_TIMESTAMP(),
                                   from_pause = 1,
                                   not_enough_money = 0
                            WHERE id=' . $project_id . ' AND proprietaire = ' . $user_id;
                //var_dump($sql);
                //die;
                $this->DB->query($sql);
            }
        }
    }


    /*
     *  Ref 2017
     *  Create
     */

    public function createTemporySelectionAnnuaireDashboard($ids)
    {
        $this->dropTemporySelectionAnnuaireDashboard();
        $result = $this->DB->query('CREATE TEMPORARY TABLE IF NOT EXISTS `annuaire_dashboard` (
                                     `project_id` int(11) NOT NULL,
                                     INDEX (project_id)
                                  ) ENGINE=MEMORY');
        if ($result) {
            $values_parts = array();
            foreach ($ids as $id) {
                $values_parts[] = '(' . $id . ')';
            }
            $sql = 'INSERT INTO `annuaire_dashboard` (project_id)  VALUES  ' . implode(',', $values_parts);
            $this->DB->query($sql);
        }
    }

    public function dropTemporySelectionAnnuaireDashboard()
    {
        $sql = 'DROP TEMPORARY TABLE IF EXISTS `annuaire_dashboard`';
        $this->DB->query($sql);
    }

}