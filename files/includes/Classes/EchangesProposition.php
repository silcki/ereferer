<?php

class EchangesProposition
{

    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * Partners constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param integer $echangesPropositionId
     *
     * @return array
     */
    public function getData($echangesPropositionId)
    {
        $sql = "SELECT *
                FROM `echanges_proposition`
                WHERE id = :id";

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':id' => $echangesPropositionId,
            ]
        );

        return $sth->fetch(PDO::FETCH_ASSOC);
    }
}