<?php


class Settings{

    /*
     *  Get param from $_SESSION ( if exists; otherwise - return default)
     */
    public static function getParamValue($param,$default_value='',$percent = false){
        if (!empty($param) && isset($_SESSION['allParameters'][$param])){
            $param_info = $_SESSION['allParameters'][$param];
            $value = !empty($param_info['valeur'])? $param_info['valeur'] : $default_value ;
            if ($percent){
                $value = floatval($value)/100;
            }
            return $value;
        }
    }
}