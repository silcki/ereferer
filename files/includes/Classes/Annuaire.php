<?php


class Annuaire{
    private  $DB;
    private  $redaction_price_per_100;
    private  $words_count_for_extra_price = 100;

    function __construct(){
        global $dbh; // very bad, but..
        $this->DB = $dbh;

        $this->redaction_price_per_100 = isset($_SESSION['allParameters']['price_100_words']['valeur'])? $_SESSION['allParameters']['price_100_words']['valeur'] : 1.65;
    }


    /*
    *  Calculate price for annuaires
    *  Based on min&max annuaire`s counts and user`s requiared count size
    */

    public function calculateExtraPrice($annuaire,$user_words_count){
        $annuaire_min_count = intval($annuaire['min_words_count']);
        $annuaire_max_count = intval($annuaire['max_words_count']);

        $extra_price = 0.0;
        if ($annuaire_max_count < $this->words_count_for_extra_price)
            return $extra_price;

        if ($user_words_count <= $annuaire_min_count)
            return $extra_price;


        $limit_max = false;
        $extra_words_count = 0;
        if ($annuaire_min_count && $annuaire_max_count){

            if ($user_words_count > $annuaire_max_count){
                $user_words_count = $annuaire_max_count;
                $limit_max = true;
            }

            if ( ($annuaire_min_count <= $user_words_count) && ($user_words_count <=$annuaire_max_count)){
               $extra_words_count = $user_words_count - $annuaire_min_count;
               $extra_price = ($extra_words_count/100)*$this->redaction_price_per_100;
               $extra_price = round($extra_price,2);
            }

        }
        return $extra_price;
    }

    /*
    *  Calculate price for annuaires
    *  Based on min&max annuaire`s counts and user`s requiared count size
    */

    public function calculateExtraPriceForWriter($annuaire,$user_words_count,$writer_tarif_redaction){
        $annuaire_min_count = intval($annuaire['min_words_count']);
        $annuaire_max_count = intval($annuaire['max_words_count']);

        $extra_price = 0.0;
        if ($annuaire_max_count < $this->words_count_for_extra_price)
            return $extra_price;

        if ($user_words_count <= $annuaire_min_count)
            return $extra_price;


        $limit_max = false;
        $extra_words_count = 0;
        if ($annuaire_min_count && $annuaire_max_count){

            if ($user_words_count > $annuaire_max_count){
                $user_words_count = $annuaire_max_count;
                $limit_max = true;
            }

            if ( ($annuaire_min_count <= $user_words_count) && ($user_words_count <=$annuaire_max_count)){
                $extra_words_count = $user_words_count - $annuaire_min_count;
                $extra_price = ($extra_words_count/100)*$writer_tarif_redaction;
                $extra_price = round($extra_price,2);
            }

        }
        return $extra_price;
    }


    public function getDisabledAnnuiresByProjectId($project_id){
        $sql = "SELECT annuaire_id
                FROM annuaireslist_disabled
                WHERE project_id=".$project_id;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_COLUMN,0);
        return $rows;
    }

    public function setDisabledAnnuaires($items){
       $insert_data = array();
       foreach($items as $item){
           $insert_data[] = '('.$item['project_id'].','.$item['annuaire_id'].')';
       }
       $sql = 'INSERT INTO annuaireslist_disabled VALUES '.implode(',',$insert_data);
       $result = $this->DB->query($sql);
       return $result;
    }

    public function removeDisabledAnnuiare($project_id,$annuaire_id){
        $sql = 'DELETE FROM annuaireslist_disabled
                WHERE project_id='.$project_id.' AND annuaire_id='.$annuaire_id;
        $result = $this->DB->query($sql);
        return $result;
    }


    public function checkAllowedInnerPagesAndSubdomains($annuaire_list_id){
        // get ids of annuaire`s list
        $sql = "SELECT annuaireslist
                FROM annuaireslist
                WHERE id=".$annuaire_list_id."
                LIMIT 1";

        $result = $this->DB->query($sql);
        $annuaires_list = $result->fetchColumn(0);
        $ids = array_filter(explode(';',$annuaires_list));

        $rows = array();
        if (!empty($ids)) {
            // get all appropriate annuaires
            $ids_query = '(.' . implode(',', $ids) . ')';
            $sql = "SELECT id,annuaire
                FROM annuaires
                WHERE id IN " . $ids_query . " AND  accept_inner_pages = 0 ";

            $result = $this->DB->query($sql);
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        return $rows;
    }

    public function checkLegalAnnuaires($annuaire_list_id){
        // get ids of annuaire`s list
        $sql = "SELECT annuaireslist
                FROM annuaireslist
                WHERE id=".$annuaire_list_id."
                LIMIT 1";

        $result = $this->DB->query($sql);
        $annuaires_list = $result->fetchColumn(0);
        $ids = array_filter(explode(';',$annuaires_list));

        $rows = array();

        if (!empty($ids)) {
            // get all appropriate annuaires
            $ids_query = '(.' . implode(',', $ids) . ')';
            $sql = "SELECT id,annuaire
                FROM annuaires
                WHERE id IN " . $ids_query . " AND accept_legal_info = 0 ";


            $result = $this->DB->query($sql);
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        }
        return $rows;
    }

    public function insertActiveStatusProjectAnnuaires($items,$project_id,$type){
        $sql = 'INSERT INTO `projects2annuaires_active` ';
        $sql_parts = array();

        foreach($items as $id=>$value){
            $sql_parts[] = '('.$project_id.','.$id.','.$value.',"'.$type.'")';
        }

        $sql = $sql.' VALUES '.implode(',',$sql_parts).';';

        $result = $this->DB->query($sql);
        return $result;
    }

    public function getNotAllowAnnuairesByProjectId($project_id){
        $sql = "SELECT DISTINCT annuaire_id
                FROM `projects2annuaires_active`
                WHERE project_id=".$project_id." AND  value = 0";


        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_COLUMN,0);
        return $rows;
    }

    // Ref 2017

    /*
     *  Get annuare infor by ids
     */
    public function getListByIds($ids){
        $out = array();

        if (empty($ids)) return null;

        $sql = "SELECT *
                FROM annuaireslist
                WHERE id IN  (".implode(',',$ids).")";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        // processing results
        foreach($rows as $row){
            $out[$row['id']] = $row;
        }

        return $out;
    }
    /*
     *  Get annuare name by id
     */
    public function getAnnuaireNameById($id){

        $sql = "SELECT *
                FROM annuaireslist
                WHERE id = " . $id;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        return $rows[0]['libelle'];
    }
 
    /*
     *  Return disabled annuaires by project Ids
     */
    public function getDisabledAnnuiresByProjectIds($ids){
        $out = array();

        if (empty($ids)) return null;

        $sql = "SELECT GROUP_CONCAT(annuaire_id) AS `annuaire_ids`,
                       project_id
                FROM annuaireslist_disabled
                WHERE project_id IN (".implode(',',$ids).")
                GROUP BY project_id";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($rows)){
            foreach($rows as $row){
                $out[$row['project_id']] = explode(',',$row['annuaire_ids']);
            }
        }

        return $out;
    }

    /*
     *  Return disabled annuaires by project Ids
     */
    public function getNotActiveAnnuiresByProjectIds($ids){
        $out = array();

        if (empty($ids)) return null;

        $sql = "SELECT GROUP_CONCAT(p2a_a.annuaire_id) AS `annuaire_ids`,
                       p2a_a.project_id AS project_id
                FROM projects2annuaires_active AS p2a_a
                WHERE p2a_a.`value` = 0 AND p2a_a.project_id IN (".implode(',',$ids).")
                GROUP BY p2a_a.project_id";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($rows)){
            foreach($rows as $row){
                $out[$row['project_id']] = explode(',',$row['annuaire_ids']);
            }
        }

        return $out;
    }


}