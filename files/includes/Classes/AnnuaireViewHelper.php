<?php


require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/functions.php';

class AnnuaireViewHelper {
    private  $DB;
    private  $admin_annuaire_list;

    function __construct($id=0){
        global $dbh; // very bad, but..
        $this->DB = $dbh;
        $this->admin_annuaire_list = Functions::getListAnnuaireDataByUser(0);
    }


    // It`s HELL)))))))
    // "Behaviour" from
    //  block listannuairecontent from account-modifier.php
    public function annuaireListFilterConfig($filter_config){
        $out = '';
        $config_blocks = array();

        $filter_param_names = array(
           'type' => 'Type',
           'parent_category' => "Liste d'annuaire",
           'tr' => "Tarif",
           'a'  => "Age",
           'tf' => "Trust Flow",
           'tv' => "Temps de validation",
           'tav' => "Taux de validation",
           'tb' => "Total Backlinks",
           'accept_inner_pages' => 'Accepte page profonde',
           'accept_legal_info' => 'Pas de mentions l�gales',
        );

        // signs  for params ( after value )
        $param_sings = array(
            'tr' => "&euro;",
            'a'  => " An(s)",
            'tv' => " Jr(s)",
            'tav' => "%"
        );

        // default "mask"-array
        // Required?
        $default_params = array(

        );


        $filter_parts = explode('&',$filter_config);
        $query_params = array();
        foreach($filter_parts as $part){
            list($parsed_param,$parsed_value) = explode('=',$part,2);
            $query_params[$parsed_param] = $parsed_value;
        }

        if (isset($query_params['filtrageavance']) && isset($query_params['filtre'])){

            $Cond1 = 0;
            $Cond2 = 0;
            $Cond3 = 0;
            $Cond4 = 0;
            $Cond5 = 0;
            $Cond6 = 0;

            $tCond = 1;
            $tVal = 0;

            $prCond = 1;
            $prVal = 0;

            $tfCond = 1;
            $tfVal = 0;

            $aCond = 1;
            $aVal = 0;

            $tvCond = 1;
            $tvVal = 0;

            $tavCond = 1;
            $tavVal = 0;

            $tbCond = 1;
            $tbVal = 0;

            $arrayComp  = array( "<="  , ">=");
            $arrayComp_ = array( ">="  , "<=");

            $filtre_infos = base64_decode($query_params['filtre']);
            $filtre_infos = explode("|", $filtre_infos);

            $annuaireSelected = isset($filtre_infos[0]) ? $filtre_infos[0] : "";

            $cp = isset($filtre_infos[1]) ? $filtre_infos[1] : "";
            $liste = isset($filtre_infos[2]) ? intval($filtre_infos[2]) : 0;
            $otherData = isset($filtre_infos[3]) ? $filtre_infos[3] : "";
            $filtrageavance = isset($filtre_infos[4]) ? $filtre_infos[4] : 0;
            $ExtraOtherData = isset($filtre_infos[5]) ? $filtre_infos[5] : "";

            // type
            if ($cp !== ''){
                $type = ($cp == 0)? 'Annuaire': 'CP';
                $config_blocks['type'] = $filter_param_names['type'].' : '.$type;
            }

            // "parent" category
            if (!empty($liste)){
                $caterory_title = '';
                foreach($this->admin_annuaire_list as $caterory){
                    if ($caterory['id'] == $liste){
                        $caterory_title = $caterory['libelle'];
                        break;
                    }
                }
                if (!empty($caterory_title)) {
                    $config_blocks['parent_category'] = $filter_param_names['parent_category'].' : '.ucfirst($caterory_title);
                }
            }

            // fucking code....
            $otherDataExX = explode(";", $ExtraOtherData);

            if (is_array($otherDataExX)) {

                $Cond1 = intval($otherDataExX[2]);
                $Cond2 = intval($otherDataExX[5]);
                $Cond3 = intval($otherDataExX[8]);
                $Cond4 = intval($otherDataExX[11]);
                $Cond5 = intval($otherDataExX[14]);
                $Cond6 = intval($otherDataExX[17]);

                // Tarif
                $tCond = intval($otherDataExX[0]);
                $tVal = floatval($otherDataExX[1]);
                if ($tVal>0){
                    $config_blocks['tr'] = $filter_param_names['tr'].' '.$arrayComp[$tCond].' '.$tVal;
                }

                // @deprecated
                // PR
                $prCond = intval($otherDataExX[3]);
                $prVal = intval($otherDataExX[4]);

                $tfCond = intval($otherDataExX[6]);
                $tfVal = intval($otherDataExX[7]);
                if ($tfVal>0){
                    $config_blocks['tf'] = $filter_param_names['tf'].' '.$arrayComp[$tfCond].' '.$tfVal;
                }

                $aCond = intval($otherDataExX[9]);
                $aVal = intval($otherDataExX[10]);
                if ($aVal>0){
                    $config_blocks['a'] = $filter_param_names['a'].' '.$arrayComp[$aCond].' '.$aVal;
                }



                $tvCond = intval($otherDataExX[12]);
                $tvVal = intval($otherDataExX[13]);
                if ($tvVal>0){
                    $config_blocks['tv'] = $filter_param_names['tv'].' '.$arrayComp[$tvCond].' '.$tvVal;
                }


                $tavCond = intval($otherDataExX[15]);
                $tavVal = intval($otherDataExX[16]);
                if ($tavVal>0){
                    $config_blocks['tav'] = $filter_param_names['tav'].' '.$arrayComp[$tavCond].' '.$tavVal;
                }


                $tbCond = intval($otherDataExX[18]);
                $tbVal = intval($otherDataExX[19]);
                if ($tbVal>0){
                    $config_blocks['tb'] = $filter_param_names['tb'].' '.$arrayComp[$tbCond].' '.$tbVal;
                }

            }

            // not in "flow" params
            if (isset($query_params['accept_inner_pages'])){
                $config_blocks['accept_inner_pages'] = $filter_param_names['accept_inner_pages'].': OUI';
            }

            if (isset($query_params['accept_legal_info'])){
                $config_blocks['accept_legal_info'] = $filter_param_names['accept_legal_info'].': OUI';
            }



            if (!empty($config_blocks)){
                foreach($config_blocks as $param=>$block){
                    $sign = '';
                    if ( isset($param_sings[$param])){
                        $sign = $param_sings[$param];
                    }
                    $out[] = $block.$sign;
                }

                $out = implode('<br/>',$out);
            }

        }

        return $out;
    }


} 