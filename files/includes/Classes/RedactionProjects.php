<?php

//namespace RedactionProjects;

if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
    //error_reporting(E_ALL);
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/MailProvider.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/functions.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/HTML.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';


// TODO REFACTOR ALl!!!

class RedactionProjects
{
    /**
     * @var PDO
     */
    private $DB;
    private $HTML;

    /**
     * RedactionProjects constructor.
     * @param \PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->DB = $dbh;
        $this->HTML = new HTML;
    }

    public function getTemplates($client_id)
    {
        $sql = "SELECT *
                FROM `redaction_projects`
                WHERE is_template=1 AND parent_id = 0 AND webmaster_id = " . intval($client_id);

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    /**
     * @param array $data
     */
    public function createProject($data)
    {
        $projects = isset($data['projects']) ? $data['projects'] : null;
        $form_data = isset($data['form_data']) ? $data['form_data'] : null;

        // check template
        $is_template = isset($form_data['is_template']) ? 1 : 0;

        $last_id = null;
        if (!empty($form_data)) {
            $is_single = $form_data['is_single'];
            $is_single = filter_var($is_single, FILTER_VALIDATE_BOOLEAN);

            // single project/template
            if ($is_single) {
                unset($form_data['is_single']);
                $info = $form_data;
                $info['is_template'] = $is_template;
                $info['parent_id'] = 0;
                $info = $this->HTML->purifyParams($info);
                $last_id = $this->insert($info);

            } else { // multiple projects/templates
                if (!empty($projects)) {

                    $projects = json_decode($projects, true);
                    $projects = array_filter($projects);

                    // if template - we create fake-project as parent
                    $template_parent_id = 0;
                    if ($is_template) {
                        $info = array();
                        $project = reset($projects);

                        $info = $project['form_data'];
                        $info['parent_id'] = 0;
                        $info['webmaster_id'] = $form_data['webmaster_id'];
                        $info['title'] = $form_data['title'];
                        $info['desc'] = $form_data['desc'];
                        $info['is_template'] = $is_template;
                        $info['article_title'] = '';
                        $info['instructions'] = '';
                        $info['words_count'] = 0;
                        $info['optimized'] = 0;

                        // purify
                        $info = $this->HTML->purifyParams($info);

                        $template_parent_id = $this->insert($info);
                    }

                    // insert all projects
                    foreach ($projects as $project) {

                        // prepare for inserting
                        $info = empty($project['form_data']) || !is_array($project['form_data']) ? []:$project['form_data'];// form-data for specified
                        $info['parent_id'] = 0;
                        // if template - set fake-parent-id
                        if ($is_template && $template_parent_id) {
                            $info['parent_id'] = $template_parent_id;
                        }

                        // common for all
                        $info['webmaster_id'] = $form_data['webmaster_id'];
                        $info['title'] = $form_data['title'];
                        $info['desc'] = $form_data['desc'];
                        $info['is_template'] = $is_template;

                        // set for specified
                        $info['article_title'] = isset($project['article_title']) ? $project['article_title'] : '';
                        $info['instructions'] = isset($project['instructions']) ? $project['instructions'] : '';
                        $info['words_count'] = !empty($project['words_count']) ? $project['words_count'] : 0;
                        $info['optimized'] = !empty($project['optimized']) ? $project['optimized'] : 0;

                        // purify
                        $info = $this->HTML->purifyParams($info);


                        //var_dump($info);

                        $last_id = $this->insert($info);
                    }
                }

            }


        }

        return $last_id;
    }


    public function insert($data)
    {
        // multiple related data
        //$data['counts'] = isset($data['counts'])? $data['counts']: array();

        $images = isset($data['images']) ? $data['images'] : array();
        $seo_words = isset($data['seo_words']) ? $data['seo_words'] : array();

        /*
         * @deprecated
        if (empty($data['counts'])){
            $data['counts'][0] = array('texts'=>$data['texts_count'],'words'=>$data['words_count']);
        }
        $counts = $data['counts']; */

        // removing "trash"-elements for related tables
        unset($data['type']);
        unset($data['seo_word']);
        unset($data['texts_count']);
        //unset($data['words_count']);
        unset($data['image']);
        unset($data['image_alt']);
        unset($data['counts']);
        unset($data['images']);
        unset($data['seo_words']);
        unset($data['id']);
        //unset($data['optimized']);
        unset($data['form_data']);
        unset($data['selected_id']);
        unset($data['fake_articles']); //

        // set default
        $data['is_template'] = isset($data['is_template']) ? $data['is_template'] : 0;
        $data['meta_title'] = !empty($data['meta_title']) ? 1 : 0;
        $data['meta_desc'] = !empty($data['meta_desc']) ? 1 : 0;
        $data['H1_set'] = !empty($data['H1_set']) ? 1 : 0;
        $data['H2_start'] = !empty($data['H2_start']) ? $data['H2_start'] : 0;
        $data['H2_end'] = !empty($data['H2_end']) ? $data['H2_end'] : 0;
        $data['H3_start'] = !empty($data['H3_start']) ? $data['H3_start'] : 0;
        $data['H3_end'] = !empty($data['H3_end']) ? $data['H3_end'] : 0;
        $data['bold_text'] = !empty($data['bold_text']) ? 1 : 0;
        $data['UL_set'] = !empty($data['UL_set']) ? 1 : 0;

        $data['seo_meta_title'] = !empty($data['seo_meta_title']) ? 1 : 0;
        $data['seo_H1_set'] = !empty($data['seo_H1_set']) ? 1 : 0;
        $data['seo_H2_set'] = !empty($data['seo_H2_set']) ? 1 : 0;
        $data['seo_H3_set'] = !empty($data['seo_H3_set']) ? 1 : 0;
        $data['seo_percent_start'] = !empty($data['seo_percent_start']) ? $data['seo_percent_start'] : 0;
        $data['seo_percent_end'] = !empty($data['seo_percent_end']) ? $data['seo_percent_end'] : 0;

        $data['images_count_from'] = !empty($data['images_count_from']) ? $data['images_count_from'] : 0;
        $data['images_count_to'] = !empty($data['images_count_to']) ? $data['images_count_to'] : 0;

        $is_template = $data['is_template'];
        $webmaster_id = $data['webmaster_id'];

        // fake-projects
        $data['amount'] = !empty($data['amount']) ? floatval($data['amount']) : 0;
        $data['words_count'] = empty($data['words_count']) ? 0 : $data['words_count'];
        $data['instructions'] = !empty($data['instructions']) ? $data['instructions']:'';

        // project edition
        $is_edition = !empty($data['edition']) ? 1 : 0;
        if ($is_edition) {
            $project_id = intval($data['project_id']);
        }


        //var_dump($webmaster_id); die;
        //var_dump($webmaster_id); die;

        // get oneToMany fields
        unset($data['counts']);
        $data['created_time'] = time();


        $sql = "INSERT INTO `redaction_projects`
                               (
                                `parent_id`,
                                `webmaster_id`,
                                `desc`,
                                `title`,
                                `meta_title`,
                                `meta_desc`,
                                `article_title`,
                                `instructions`,
                                `H1_set`,
                                `H2_start`,
                                `H2_end`,
                                `H3_start`,
                                `H3_end`,
                                `bold_text`,
                                `seo_percent_start`,
                                `seo_percent_end`,
                                `UL_set`,
                                `seo_meta_title`,
                                `seo_H1_set`,
                                `seo_H2_set`,
                                `seo_H3_set`,
                                `images_count_from`,
                                `images_count_to`,
                                `is_template`,
                                `amount`,
                                `created_time`,
                                `words_count`,
                                `optimized`)
                            VALUES
                                (
                                 :parent_id,
                                 :webmaster_id,
                                 :desc,
                                 :title,
                                 :meta_title,
                                 :meta_desc,
                                 :article_title,
                                 :instructions,
                                 :H1_set,
                                 :H2_start,
                                 :H2_end,
                                 :H3_start,
                                 :H3_end,
                                 :bold_text,
                                 :seo_percent_start,
                                 :seo_percent_end,
                                 :UL_set,
                                 :seo_meta_title,
                                 :seo_H1_set,
                                 :seo_H2_set,
                                 :seo_H3_set,
                                 :images_count_from,
                                 :images_count_to,
                                 :is_template,
                                 :amount,
                                 FROM_UNIXTIME(:created_time),
                                 :words_count,
                                 :optimized)";
        $execute_data = array();
        foreach ($data as $key => $value) {
            $execute_data[':' . $key] = $value;
        }

        // if edition
        if ($is_edition) {
            $execute_data[':project_id'] = $project_id;
            $execute_data[':words_count'] = $data['words_count'];

            return $this->update($execute_data, $images, $seo_words);
        }


        //var_dump($execute_data); die;


        // check text & counts
        // insert multiple project
        $this->DB->beginTransaction();
        try {
            if (!$is_template) {

                $webmaster_price_100_words = $this->getPriceForWordsByUserId($webmaster_id);
                $project_amount = $this->calculatingProjectAmount($execute_data[':words_count'], $webmaster_price_100_words);
                $execute_data[':amount'] = $project_amount;
                //
                $this->DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $this->DB->prepare($sql);
                $result = $stmt->execute($execute_data);

                $project_id = $this->DB->lastInsertId();
                // inserting related data
                // counts,images,seo words
                if (!empty($images)) {
                    $this->insertImagesForProject($images, $project_id);
                }

                if (!empty($seo_words)) {
                    $this->insertSeoWordsForProject($seo_words, $project_id);
                }

                // reducing user balance
                $this->reduceUserSolde($webmaster_id, $project_amount);

            } else {
                $stmt = $this->DB->prepare($sql);
                $result = $stmt->execute($execute_data);
                $project_id = $this->DB->lastInsertId();

                //$this->insertTextAndWordsCountsForProject($counts, $project_id);
                // inserting related data
                // counts,images,seo words
                if (!empty($images)) {
                    $this->insertImagesForProject($images, $project_id);
                }
                if (!empty($seo_words)) {
                    $this->insertSeoWordsForProject($seo_words, $project_id);
                }
            }
            $this->DB->commit();
        } catch (PDOException $exception) {
            //var_dump($sql);
            //var_dump($execute_data);
            // var_dump(strip_tags($exception));
            //die;

            $this->DB->rollBack();
            // failed
            return false;
        }

        return $project_id;
    }


    function sql_debug($sql_string, array $params = null)
    {
        if (!empty($params)) {
            $indexed = $params == array_values($params);
            foreach ($params as $k => $v) {
                if (is_object($v)) {
                    if ($v instanceof \DateTime) $v = $v->format('Y-m-d H:i:s');
                    else continue;
                } elseif (is_string($v)) $v = "'$v'";
                elseif ($v === null) $v = 'NULL';
                elseif (is_array($v)) $v = implode(',', $v);

                if ($indexed) {
                    $sql_string = preg_replace('/\?/', $v, $sql_string, 1);
                } else {
                    if ($k[0] != ':') $k = ':' . $k; //add leading colon if it was left out
                    $sql_string = str_replace($k, $v, $sql_string);
                }
            }
        }
        return $sql_string;
    }

    public function update($execute_data, $images = array(), $words = array())
    {
        // remove not used
        unset($execute_data[':webmaster_id']);
        unset($execute_data[':edition']);
        unset($execute_data[':is_template']);
        unset($execute_data[':created_time']);

        //var_dump($execute_data);


        $desc = $execute_data[':desc'];
        $words_count = $execute_data[':words_count'];
        $title = $execute_data[':title'];
        $article_title = $execute_data[':article_title'];
        $instructions = $execute_data[':instructions'];
        $meta_title = $execute_data[':meta_title'];
        $meta_desc = $execute_data[':meta_desc'];
        $H1_set = $execute_data[':H1_set'];
        $H2_start = $execute_data[':H2_start'];
        $desc = $execute_data[':desc'];
        $H2_end = $execute_data[':H2_end'];
        $H3_start = $execute_data[':H3_start'];
        $H3_end = $execute_data[':H3_end'];
        $bold_text = $execute_data[':bold_text'];
        $UL_set = $execute_data[':UL_set'];
        $seo_percent_start = $execute_data[':seo_percent_start'];
        $seo_percent_end = $execute_data[':seo_percent_end'];
        $UL_set = $execute_data[':UL_set'];
        $seo_meta_title = $execute_data[':seo_meta_title'];
        $seo_H1_set = $execute_data[':seo_H1_set'];
        $seo_H2_set = $execute_data[':seo_H2_set'];
        $seo_H3_set = $execute_data[':seo_H3_set'];
        $amount = $execute_data[':amount'];
        $images_count_from = $execute_data[':images_count_from'];
        $images_count_to = $execute_data[':images_count_to'];
        $project_id = $execute_data[':project_id'];

        // get project befor updating
        $project = $this->getProject($project_id);
        $old_amount = $project['amount'];
        $old_words_count = $project['words_count'];


        $sql = "UPDATE `redaction_projects`
                SET `desc` = :desc,
                    `words_count` = :words_count,
                    `title` = :title,
                    `article_title` = :article_title,
                    `instructions` = :instructions,
                    `meta_title` = :meta_title,
                    `meta_desc` = :meta_desc,
                    `article_title` = :article_title,
                    `instructions` = :instructions,
                    `H1_set` = :H1_set,
                    `H2_start` = :H2_start,
                    `H2_end` = :H2_end,
                    `H3_start` = :H3_start,
                    `H3_end` = :H3_end,
                    `bold_text` = :bold_text,
                    `UL_set` = :UL_set,
                    `seo_percent_start` = :seo_percent_start,
                    `seo_percent_end` = :seo_percent_end,
                    `seo_meta_title` = :seo_meta_title,
                    `seo_H1_set` = :seo_H1_set,
                    `seo_H2_set` = :seo_H2_set,
                    `seo_H3_set` = :seo_H3_set,
                    `amount` = :amount,
                    `images_count_from` = :images_count_from,
                    `images_count_to` = :images_count_to
                WHERE `id` = :project_id";

        $stmt = $this->DB->prepare($sql);

        $stmt->bindParam(':desc', $desc);
        $stmt->bindParam(':words_count', $words_count);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':article_title', $article_title);
        $stmt->bindParam(':instructions', $instructions);
        $stmt->bindParam(':meta_title', $meta_title);
        $stmt->bindParam(':meta_desc', $meta_desc);
        $stmt->bindParam(':H1_set', $H1_set);
        $stmt->bindParam(':H2_start', $H2_start);
        $stmt->bindParam(':H2_end', $H2_end);
        $stmt->bindParam(':H3_start', $H3_start);
        $stmt->bindParam(':H3_end', $H3_end);
        $stmt->bindParam(':bold_text', $bold_text);
        $stmt->bindParam(':UL_set', $UL_set);
        $stmt->bindParam(':seo_meta_title', $seo_meta_title);
        $stmt->bindParam(':seo_percent_start', $seo_percent_start);
        $stmt->bindParam(':seo_percent_end', $seo_percent_end);
        $stmt->bindParam(':seo_H1_set', $seo_H1_set);
        $stmt->bindParam(':seo_H2_set', $seo_H2_set);
        $stmt->bindParam(':seo_H3_set', $seo_H3_set);
        $stmt->bindParam(':amount', $amount);
        $stmt->bindParam(':images_count_from', $images_count_from);
        $stmt->bindParam(':images_count_to', $images_count_to);
        $stmt->bindParam(':project_id', $project_id);

        $result = $stmt->execute();

        // change balance, based on new words count
        $new_amount = $amount;
        $webmaster_id = $project['webmaster_id'];
        if ($old_words_count != $words_count) {
            $user_model = new User($webmaster_id);
            $transaction = new Transaction($this->DB, $webmaster_id);
            if ($old_amount > $new_amount) {
                $diff = $old_amount - $new_amount;
                $user_model->changeBalance($webmaster_id, $diff, true);
                // save transactions when editing project
                $transaction->addTransaction("Edition projet : " . $_POST['form_data']['title'], $diff, 0);
            }
            if ($new_amount > $old_amount) {
                $diff = $new_amount - $old_amount;
                $user_model->changeBalance($webmaster_id, $diff, false);
                // save transactions when editing project
                $transaction->addTransaction("Edition projet : " . $_POST['form_data']['title'], 0, $diff);
            }
        }


        // update images & words
        $this->deleteImages($project_id);
        $this->insertImagesForProject($images, $project_id);

        $this->deleteWords($project_id);
        $this->insertSeoWordsForProject($words, $project_id);
        return true;
    }

    /*
     * DELETE
     */
    public function deleteByParentId($parent_id)
    {
        $sql = 'DELETE FROM  `redaction_projects`
                WHERE parent_id= ' . $parent_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    public function getUserSolde($user_id)
    {
        $sql = 'SELECT `solde`
                FROM  `utilisateurs`
                WHERE id = '.$user_id;

        $result = $this->DB->query($sql);

        return $result->fetchColumn(0);
    }

    private function reduceUserSolde($webmaster_id, $amount)
    {
        /*$sql = 'SELECT `solde`
                FROM  `utilisateurs`
                WHERE id = '.$webmaster_id;

        $result = $this->DB->query($sql);
        $balance = $result->fetchColumn(0);
        $balance = $balance - $amount;*/

        $sql = 'UPDATE utilisateurs SET solde = solde - ' . $amount . ' WHERE id = ' . $webmaster_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    private function calculatingProjectAmount($words_count, $price_per_100 = null)
    {
        if (!$price_per_100)
            $price_per_100 = $this->getPriceForWords();

        $amount = ($price_per_100 * floatval($words_count)) / 100;
        $amount = round($amount, 2);
        return $amount;
    }

    public function getPriceForWordsByUserId($user_id)
    {
        $sql = 'SELECT `tarif_redaction`
                FROM utilisateurs
                WHERE id = ' . $user_id;

        $result = $this->DB->query($sql);
        $price = $result->fetchColumn(0);
        return $price;
    }

    public function getPriceForWords()
    {
        $sql = 'SELECT `valeur`
                FROM  `parametres`
                WHERE name = "price_100_words"';

        $result = $this->DB->query($sql);
        $price = $result->fetchColumn(0);
        return $price;
    }

    private function deleteImages($project_id)
    {
        $sql = 'DELETE FROM  `redaction_images`
                WHERE project_id= ' . $project_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    private function deleteWords($project_id)
    {
        $sql = 'DELETE FROM  `redaction_seo_words`
                WHERE project_id= ' . $project_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    private function insertImagesForProject($images, $project_id)
    {
        $sql = 'INSERT INTO `redaction_images`(project_id,image_url,alt)
                           VALUES(:project_id,:image_url,:alt)';
        $stmt = $this->DB->prepare($sql);

        //var_dump($images);
        try {
            foreach ($images as $data) {
                // purify
                $url = $this->HTML->purifyParams($data['image_url']);
                $alt = $this->HTML->purifyParams($data['alt']);

                $stmt->bindParam(':project_id', $project_id);
                $stmt->bindParam(':image_url', $url);
                $stmt->bindParam(':alt', $alt);
                $result = $stmt->execute();
            }

        } catch (PDOException $e) {
            //var_dump($e);
        }
        //die;
    }

    private function insertSeoWordsForProject($words, $project_id)
    {
        $sql = 'INSERT INTO `redaction_seo_words`(project_id,expression)
                           VALUES(:project_id,:word)';
        $stmt = $this->DB->prepare($sql);
        foreach ($words as $word) {
            // purify
            $word = $this->HTML->purifyParams($word);

            $stmt->bindParam(':project_id', $project_id);
            $stmt->bindParam(':word', trim($word));
            $stmt->execute();
        }
    }

    /*
     *  @deprecated
     */
    private function insertTextAndWordsCountsForProject($counts, $project_id)
    {
        $sql = 'INSERT INTO `redaction_project_counts`(project_id,texts_count,words_count)
                           VALUES(:project_id,:texts_count,:words_count)';

        $stmt = $this->DB->prepare($sql);

        foreach ($counts as $data) {
            $texts_count = $data['texts'];
            $words_count = $data['words'];
            $stmt->bindParam(':project_id', $project_id);
            $stmt->bindParam(':texts_count', $texts_count);
            $stmt->bindParam(':words_count', $words_count);
            $stmt->execute();
        }
    }

    /**
     * @param integer $project_id
     * @param bool    $is_template
     *
     * @return array|bool|mixed
     */
    public function getProject($project_id, $is_template = false)
    {
        $sql = "SELECT *
                FROM `redaction_projects`
                WHERE id = " . intval($project_id);

        // if template
        if ($is_template) {
            return $this->getProjectTemplate($project_id);
        }

        $result = $this->DB->query($sql);
        $project = $result->fetch(PDO::FETCH_ASSOC);
        $words = $this->getProjectSeoWords($project_id);
        if (!empty($words)) {
            $project['words'] = $words;
        }
        $images = $this->getProjectImages($project_id);
        if (!empty($images)) {
            $project['images'] = $images;
        }

        return $project;
    }

    public function getProjectTemplate($project_id)
    {

        $sql = "SELECT *
                FROM `redaction_projects`
                WHERE is_template = 1 AND  parent_id = " . $project_id;

        $result = $this->DB->query($sql);
        $projects = $result->fetchAll(PDO::FETCH_ASSOC);

        // it means - single template
        // if nothing by parent
        if (empty($projects)) {
            $sql = "SELECT *
                FROM `redaction_projects`
                WHERE is_template = 1  AND id = " . $project_id;

            $result = $this->DB->query($sql);
            $projects = $result->fetchAll(PDO::FETCH_ASSOC);
        }


        // remove unrequired data
        $unrequired_fields = array(
            "admin_approved_time",
            "created_time",
            "affected_time",
            "adminApproved",
            "viewed",
            "status",
            "affectedTO"
        );

        if (count($projects) > 0) {
            $full_projects = array();
            foreach ($projects as $project) {
                $words = $this->getProjectSeoWords($project['id']);
                if (!empty($words)) {
                    $project['seo_words'] = $words;
                }
                $images = $this->getProjectImages($project['id']);
                if (!empty($images)) {
                    $project['images'] = $images;
                }
                $data = array_diff_key($project, array_flip($unrequired_fields));
                $full_projects[] = $data;
            }

            $out = array();
            // check multiple template
            if (count($full_projects) > 1) {
                $out['multiple'] = true;
                foreach ($full_projects as $project) {
                    $category = 'category' . $project['words_count'];
                    $out['categories']['children'][$category][] = $project; // unit into "category" by words count
                }
            } else {
                $out['multiple'] = false;
                $out['project'] = array_shift($full_projects);
            }

            return $out;
        } else
            return false;
    }

    public function getProjectSeoWords($project_id)
    {
        $sql = "SELECT expression
                FROM `redaction_seo_words`
                WHERE project_id = " . $project_id;
        $result = $this->DB->query($sql);
        return $result->fetchAll(PDO::FETCH_COLUMN, 0);
    }

    public function getProjectImages($project_id)
    {
        $sql = "SELECT image_url,alt
                FROM `redaction_images`
                WHERE project_id = " . $project_id;
        $result = $this->DB->query($sql);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProjectCounts($project_id)
    {
        $sql = "SELECT words_count,texts_count
                FROM `redaction_project_counts`
                WHERE project_id = " . $project_id;
        $result = $this->DB->query($sql);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param integer $user_id
     * @param string  $status
     * @param integer $page
     * @param integer $per_page
     * @param string  $type
     * @param string  $order_by
     * @param array   $filters
     *
     * @return array
     */
    public function getProjects($user_id, $status, $page, $per_page, $type = 'admin', $order_by = 'asc', $filters = array())
    {
        $offset = ($page - 1) * $per_page;
        $sql = "SELECT p.*,
                       w.nom AS w_nom,
                       w.prenom AS w_prenom,
                       wr.nom AS wr_nom,
                       wr.prenom AS wr_prenom,
                       r.approved_date AS finished_time
                FROM `redaction_projects` AS p
                LEFT JOIN `redaction_reports` AS r
                 ON (p.id = r.project_id)
                LEFT JOIN `utilisateurs` AS w
                 ON ( w.id = p.webmaster_id)
                LEFT JOIN `utilisateurs` AS wr
                 ON ( wr.id = p.affectedTO)
                LEFT JOIN `redaction_writers_likes` AS rwl
                 ON ( rwl.project_id = p.id)
                WHERE  p.is_template=0";

        if ($user_id && $type == 'webmaster') {
            $sql .= ' AND p.webmaster_id=' . $user_id . ' AND p.prop_id = 0';
        }

        if (($type == 'webmaster') && ($status == 'progress')) {
            $sql .= ' AND (  p.status = "progress" OR  p.status="review" OR  p.status="modification" )';
        } elseif (($type == 'admin') && ($status == 'progress')) {
            $sql .= ' AND ( p.status = "progress"  OR  p.status="modification" OR  p.status="review" )';
        } else {
            $sql .= ' AND p.status = "' . $status . '"';
        }

        /*
        if ( ($type=='admin')  && ($status=='progress') ){
            $sql.=' OR status="modification"';
        }*/

        if ($user_id && $type == 'writer') {
            $sql .= ' AND p.affectedTO=' . $user_id;
        }

        // filter
        if (!empty($filters)) {
            $sql .= $this->fitlersToWhereSql($filters);
        }

        if ($status == 'waiting') {
            $sql .= ' ORDER BY p.prop_id != 0 DESC, p.created_time ASC ';
        }


        if ($status == 'finished') {
            $sql .= ' ORDER BY r.approved_date DESC';
        }

        $sql .= ' LIMIT ' . $offset . ',' . $per_page;

        //echo '<pre>';
        //var_dump($sql);
        //echo '</pre>';

        $result = $this->DB->query($sql);

        $projects = $result->fetchAll(PDO::FETCH_ASSOC);

        return $projects;
    }

    public function getProjectsCount($status = 'waiting', $user_id = 0, $type = 'admin', $not_viewed = false, $filters = array())
    {
        $sql = "SELECT COUNT(*)
                FROM `redaction_projects` AS p
                LEFT JOIN `redaction_reports` AS r
                 ON (p.id = r.project_id)
                LEFT JOIN `utilisateurs` AS w
                 ON ( w.id = p.webmaster_id)
                LEFT JOIN `utilisateurs` AS wr
                 ON ( wr.id = p.affectedTO)
                LEFT JOIN `redaction_writers_likes` AS rwl
                 ON ( rwl.project_id = p.id)
                WHERE  p.is_template=0";

        // for clients
        if ($user_id && ($type == 'webmaster')) {
            $sql .= ' AND p.webmaster_id = ' . $user_id . ' AND p.prop_id = 0';
        }

        if (($type == 'webmaster') && ($status == 'progress')) {
            $sql .= ' AND ( p.status = "progress" OR status="review" OR status="modification" )';
        } elseif (($type == 'admin') && ($status == 'progress')) {
            $sql .= ' AND ( p.status = "progress"  OR status="modification" OR status="review" )';
        } else {
            $sql .= ' AND p.status = "' . $status . '"';
        }

        // not viewed by client projects
        if ($not_viewed) {
            $sql .= ' AND p.viewed = 0';
        }

        // for writers
        if ($user_id && ($type == 'writer')) {
            $sql .= ' AND p.affectedTO = ' . $user_id;
        }


        // filters
        if (!empty($filters)) {
            $sql .= $this->fitlersToWhereSql($filters);
            //var_dump($sql); die;
        }

        // if ($status=='finished')
        // var_dump($sql);


        $result = $this->DB->query($sql);
        $count = $result->fetchColumn(0);
        return $count;
    }

    private function fitlersToWhereSql($filters)
    {
        $sql_parts = array();

        if (isset($filters['not_liked_texts'])) {
            $sql_parts[] = ' rwl.value = 0 ';
        }


        if (!empty($sql_parts)) {
            if (count($sql_parts) > 1) {
                return implode(' AND ', $sql_parts);
            } else {
                return ' AND ' . array_shift($sql_parts);
            }
        }
    }

    private function filtersToGroupBySql($filters)
    {

    }


    public function setStatus($status, $project_id)
    {
        $sql = 'UPDATE `redaction_projects`
                SET status = "' . $status . '"
                WHERE id = ' . $project_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    public function finishProccessing($project_id, $corrector_id, $step = 'finishing')
    {
        global $idUser;

        $mailProvider = new MailProvider();
        $reportsModel = new RedactionReports($this->DB);
        $project = $this->getProject($project_id);

        if ($step == 'finishing') {

            $writer_id = $project['affectedTO'];
            $project_words_count = $project['words_count'];

            $userModel = new User($idUser);
            $writer_tarif = $userModel->getRedactionPrice100Words($writer_id, 'writer');

            $writer_earn = $this->calculatingProjectAmount($project_words_count, $writer_tarif);
            $userModel->changeBalance($writer_id, $writer_earn, true);

            if (isSuperReferer()) {
                $corrector_tarif = $userModel->getRedactionPrice100Words($corrector_id, 'corrector');
                $corrector_earn = $this->calculatingProjectAmount($project_words_count, $corrector_tarif);
                $userModel->changeBalance($corrector_id, $corrector_earn, true);
            }

            // finish report
            $reportsModel->finishProccesing($project_id, $corrector_id, $writer_earn, $corrector_earn);

            // send email to client
            $webmaster_id = $project['webmaster_id'];
            $project_title = $project['title'];

            $mailProvider->NoticeRedactionFinishedProject($project_id, $webmaster_id, $project_title);
            //var_dump($project);
            //die;
            $this->setStatus('finished', $project_id);
            $this->setViewed($project_id, 0);
        }
    }

    public function setViewed($project_id, $state = 1)
    {
        $sql = 'UPDATE `redaction_projects`
                SET viewed = ' . $state . '
                WHERE id = ' . $project_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    public function delete($project_id)
    {
        $sql = "DELETE
                FROM `redaction_projects`
                WHERE id = " . $project_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    public function getTopForWriterChoosing($writer_id)
    {
        $sql = "SELECT *
                FROM redaction_projects AS p
                LEFT JOIN (
                    SELECT p.webmaster_id  AS client_id,
                           wl.writer_id AS writer_id,
                           SUM(wl.value)/COUNT(wl.value) AS rating
                    FROM redaction_writers_likes AS wl
                    INNER JOIN redaction_projects AS p
                     ON ( wl.project_id = p.id)
                    WHERE writer_id = " . $writer_id . "
                    GROUP BY wl.writer_id,p.webmaster_id
                    HAVING rating < 0.5 AND COUNT(wl.value) > 0
                ) as rated_writers
                ON ( p.webmaster_id <> rated_writers.client_id )
                WHERE p.status = 'waiting'
                      AND p.affectedTO = 0
                      AND p.is_template = 0
                      AND p.webmaster_id NOT IN (
                           SELECT id
                           FROM utilisateurs
                           WHERE writers_choosing = 1)
                ORDER BY p.prop_id != 0 DESC, p.created_time ASC
                LIMIT 3";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getProjectsByFavouriteWriter($writer_id)
    {
        $sql = "SELECT *
                FROM redaction_projects AS p
                INNER JOIN (SELECT p.webmaster_id  AS client_id,
                             wl.writer_id AS writer_id,
                             SUM(wl.value)/COUNT(wl.value) AS rating
                            FROM redaction_writers_likes AS wl
                            INNER JOIN redaction_projects AS p
                             ON ( wl.project_id = p.id)
                            GROUP BY wl.writer_id,p.webmaster_id
                            HAVING rating > 0.5 ) AS rated_writers
                ON ( p.webmaster_id = rated_writers.client_id )
                WHERE rated_writers.writer_id = " . $writer_id . " AND
                                      p.status = 'waiting'
                                      AND p.affectedTO = 0
                                      AND p.is_template = 0
                                      AND p.webmaster_id NOT IN (
                                           SELECT id
                                           FROM utilisateurs
                                           WHERE writers_choosing = 1)
                GROUP BY rated_writers.client_id,rated_writers.writer_id
                HAVING MAX(rated_writers.rating)";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }


    /*
     *  Get Last estimated start date
     *
     *  Returning count days
     *  CHANGED
     */

    public function getDaysCountWaiting($user_id, $words_capacity)
    {

        // min-created date
        $sql = "SELECT MAX(created_time)
					FROM redaction_projects
                    WHERE is_template = 0 AND status='waiting'";
        $result = $this->DB->query($sql);
        $min_date = $result->fetchColumn(0);

        // get total count before-projects
        $sql = "SELECT SUM(words_count)
                FROM redaction_projects
                WHERE is_template = 0 AND  `status` = 'waiting'";

        if (!empty($min_date)) {
            $sql .= " AND created_time < '" . $min_date . "'";
        }
        $result = $this->DB->query($sql);
        $total_words = $result->fetchColumn(0);

        $days_start = ceil($total_words / $words_capacity);
        return $days_start;
    }


    /*
       Get esmtimated strating-time for waiting projects
    */
    public function getEstimatedStartDates($user_id = 0, $user_type = 'admin')
    {

        // get param words possible by writers for 1 days
        $words_capacity = $_SESSION['allParameters']['delay_words_count']['valeur'];
        if (!$words_capacity)
            $words_capacity = 5000;

        $sql = "SELECT
                        rp.id,
                       (SUM(rp_dates.words_count) - rp.words_count) AS words_count_after,
                        rp.created_time AS created_time
                FROM redaction_projects AS rp
                LEFT JOIN redaction_projects AS rp_dates
                 ON (  rp.`created_time` >= rp_dates.`created_time`  )
                WHERE rp.`status` = 'waiting'
                      AND rp_dates.`status` = 'waiting'
                      AND rp.is_template = 0
                      AND rp_dates.is_template = 0
                GROUP BY rp.id
                ORDER BY created_time ASC";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        $estimated_dates = array();

        if (!empty($rows)) {
            // calcuates estimated dates
            $current_time = time();
            $count_waiting = count($rows);

            foreach ($rows as $index => $row) {
                ++$index;
                $current_step = ceil($row['words_count_after'] / $words_capacity);
                if ($current_step == 0)
                    $current_step = 1;
                $estimated_dates[$row['id']] = $current_time + $current_step * 24 * 60 * 60;
                //$estimated_dates[$row['id']]['words_after'] = $row['words_count_after'];
            }
            return $estimated_dates;


        }

    }

    /*
     *  Update project start-dates. Sortable behaviour.
     */
    public function updateProjectStartDates($projects)
    {
        $count_affected = 0;

        //error_reporting(E_ALL);

        if (!empty($projects)) {
            foreach ($projects as $project) {
                $sql = 'UPDATE redaction_projects
                          SET    created_time =  FROM_UNIXTIME(' . $project['time'] . ')
                         WHERE  status = "waiting"  AND id = ' . $project['id'];
                //var_dump($sql);
                $result = $this->DB->query($sql);
                if ($result)
                    $count_affected++;
            }
            die;
        }
        return $count_affected;
    }


}