<?php


class RedactionReports
{

    /**
     * @var PDO
     */
    private $DB;

    /**
     * RedactionReports constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->DB = $dbh;
    }

    /**
     * @return PDO
     */
    public function getDB()
    {
        return $this->DB;
    }

    /**
     * @param $param
     * @param $project_id
     *
     * @return mixed
     */
    public function getParam($param, $project_id)
    {
        $sql = "SELECT $param
                FROM `redaction_reports`
                WHERE project_id=" . intval($project_id);

        $result = $this->DB->query($sql);

        return $result->fetchColumn();
    }

    /**
     * @param $param
     * @param $value
     * @param integer $project_id
     *
     * @return bool
     */
    public function setParam($param, $value, $project_id)
    {
        $params = array(
            ':value' => $value,
            ':id' => $project_id
        );
        $sql = "UPDATE `redaction_reports` SET $param = :value
                WHERE project_id=:id";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute($params);

        return true;
    }

    public function insert($data, $project_id)
    {
        $exists = $this->getReport($project_id);
        $created_time = time();

        if (!$exists) {
            $sql = 'INSERT INTO `redaction_reports`(project_id,report,created_date,text,meta_title,meta_desc,image_sources, front_image)
                           VALUES(:project_id,:report,FROM_UNIXTIME(:created_date),:text,:meta_title,:meta_desc,:image_sources,:front_image)';

        } else {
            $sql = 'UPDATE `redaction_reports`
                    SET text=:text,
                        meta_title = :meta_title,
                        meta_desc = :meta_desc,
                        report = :report,
                        image_sources=:image_sources,
                        front_image=:front_image
                    WHERE project_id = :project_id';
        }

        $stmt = $this->DB->prepare($sql);
        $stmt->bindParam(':project_id', $project_id);
        $stmt->bindParam(':report', $data['report']);
        $stmt->bindParam(':text', $data['text']);
        $stmt->bindParam(':meta_title', $data['meta_title']);
        $stmt->bindParam(':meta_desc', $data['meta_desc']);
        $stmt->bindParam(':image_sources', $data['image_sources']);
        $stmt->bindParam(':front_image', $data['front_image']);

        if (!$exists) {
            $stmt->bindParam(':created_date', $created_time);
        }
        return $stmt->execute();
    }

    /**
     * @param integer $project_id
     *
     * @return mixed
     */
    public function getReport($project_id)
    {
        $sql = "SELECT *
                FROM `redaction_reports`
                WHERE project_id = " . $project_id;

        $result = $this->DB->query($sql);

        return $result->fetch(PDO::FETCH_ASSOC);
    }

    public function finishProccesing($project_id, $corrector_id, $writer_earn, $corrector_earn)
    {
        $approved_time = time();
        $sql = 'UPDATE `redaction_reports`
                SET user_approved_id= :corrector_id,
                    writer_earn = :writer_earn,
                    corrector_earn = :corrector_earn,
                    approved_date = FROM_UNIXTIME(:approved_date)
                WHERE project_id = :project_id';

        $stmt = $this->DB->prepare($sql);
        $stmt->bindParam(':project_id', $project_id);
        $stmt->bindParam(':writer_earn', $writer_earn);
        $stmt->bindParam(':corrector_earn', $corrector_earn);
        $stmt->bindParam(':corrector_id', $corrector_id);
        $stmt->bindParam(':approved_date', $approved_time);
        return $stmt->execute();
    }

    public function getWriterStatsData($time_periods, $writer_id)
    {
        $data = array();

        if (is_array($time_periods) && !empty($time_periods)) {

            foreach ($time_periods as $period) {

                $sql = "SELECT COUNT(rr.project_id) AS texts_count,
                               SUM(rr.writer_earn) AS sum_per_month
                        FROM redaction_reports AS rr
                        INNER JOIN redaction_projects AS rp
                            ON ( rp.id = rr.project_id )
                        WHERE rp.affectedTO= " . $writer_id . "
                              AND rp.`status` = 'finished'
                              AND rr.approved_date >= " . strtotime($period['start']) . "
                              AND rr.approved_date <= " . strtotime($period['end']);

                $data_per_month = $this->DB->query($sql)->fetch(PDO::FETCH_ASSOC);
                $period['texts_count'] = $data_per_month['texts_count'];
                $period['writer_earn'] = round($data_per_month['sum_per_month'], 2);
                $data[] = $period;
            }

        }

        return $data;
    }

    /*
    *  @Corrector stats
    *
    */
    public function getAdminWriterStatsData($time_periods, $writer_id)
    {
        $data = array();

        if (is_array($time_periods) && !empty($time_periods)) {

            foreach ($time_periods as $period) {

                $sql = "SELECT COUNT(rr.project_id) AS texts_count,
                               SUM(rr.corrector_earn) AS sum_per_month
                        FROM redaction_reports AS rr
                        INNER JOIN redaction_projects AS rp
                            ON ( rp.id = rr.project_id )
                        WHERE rr.user_approved_id = " . $writer_id . "
                              AND rp.`status` = 'finished'
                              AND rr.approved_date >= '" . $period['start'] . "'
                              AND rr.approved_date <= '" . $period['end'] . "'";

                $data_per_month = $this->DB->query($sql)->fetch(PDO::FETCH_ASSOC);

                $period['texts_count'] = $data_per_month['texts_count'];
                $period['corrector_earn'] = round($data_per_month['sum_per_month'], 2);
                $data[] = $period;
            }

        }

        return $data;
    }


}