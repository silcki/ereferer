<?php

class Messages
{

    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * @var \PHPMailer
     */
    private $mail;

    /**
     * Messages constructor.
     *
     * @param PDO $dbh
     * @param PDO $mail
     */
    public function __construct(\PDO $dbh, PHPMailer $mail)
    {
        $this->dbh = $dbh;
        $this->mail = $mail;

        $this->mail->Timeout = '30';
        $this->mail->CharSet = 'UTF-8';
        $this->mail->From = "noreply@ereferer.fr";
        $this->mail->AltBody = "";
        $this->mail->IsHTML(true);
    }

    /**
     * @param array   $data
     *
     * @return bool
     */
    public function addNewMassage($data)
    {
        $stmt = $this->dbh->prepare('INSERT INTO messages VALUES(null, :objet, :message, :connected , :destinataire, 1, 1, 1, :time)');

        $stmt->bindParam(':objet', $data['objet']);
        $stmt->bindParam(':message', $data['message']);
        $stmt->bindParam(':connected', $data['connected']);
        $stmt->bindParam(':destinataire', $data['destinataire']);
        $stmt->bindParam(':time', $data['current_time']);

        return $stmt->execute();
    }

    /**
     * @param string $fromName
     * @param string $subject
     * @param string $recipient
     * @param string $body
     */
    public function sendMessage($fromName, $subject, $recipient, $body)
    {
        $this->mail->FromName = $fromName;
        $this->mail->Subject = $subject;
        $this->mail->MsgHTML($body);
        $this->mail->AddAddress($recipient, "");
        $this->mail->Send();
    }

    /**
     * @return string
     */
    public function getSimpleMessage()
    {
        $body = '<br/>';
        $body .= 'Connectez-vous &agrave; votre compte pour lire le message<br/>';

        return $body;
    }

    /**
     * @param array  $userInfo
     * @param string $message
     *
     * @return string
     */
    public function getPreparedMessage($userInfo, $message)
    {
        $replace = [
            '$$prenom$$' => $userInfo['prenom'],
        ];

        return strtr($message, $replace);
    }
}