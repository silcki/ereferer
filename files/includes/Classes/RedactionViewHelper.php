<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionLikes.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';

class RedactionViewHelper {
    private  $DB;

    function __construct($id=0){
        global $dbh; // very bad, but..
        $this->DB = $dbh;
    }

    public function getWritersOptionList($project_id,$writer_id=null) {
        global $idUser;
        global $arrayLvl;
        $tampon = 0;
        $tampon2 = 0;
        $tableau = "";

        if (!empty($project_id) || isset( $_POST['projets'][0])){
            $likesModel = new RedactionLikes();
            $project_id = !empty($project_id)?$project_id:$_POST['projets'][0]; // get first project_id
            // testing project_id
            //$project_id = 963;
            $worse_writers_ids = $likesModel->getWorseWritersByProjectID($project_id);
            $best_writers_ids = $likesModel->getBestWritersByProjectID($project_id);
        }
        $condition_worse_writers = '';
        if (!empty($worse_writers_ids)){
            $condition_worse_writers = ' AND id NOT IN ('.implode(',',$worse_writers_ids).') ';
        }
        $typeUser = 3; // writer type
        $requete = "SELECT *
                    FROM utilisateurs
                    WHERE typeutilisateur = '" . $typeUser . "' ".$condition_worse_writers."
                    AND redaction_writer = 1
                    ORDER BY nom ASC";

        //var_dump($requete); die;

        $best_writers_top = ''; // put best writes to select-top
        $execution = $this->DB->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {

            $selected = '';
            if ($writer_id && ($writer_id == $retour['id'])) {
                $selected = "selected='selected'";
            }


            $best_writer_flag = '';
            if (!empty($best_writers_ids) && (in_array($retour['id'],$best_writers_ids)) ){
                $best_writer_flag = 'class="writer_best"';
            }

            if (empty($best_writer_flag)) {
                $var = '<option ' . $best_writer_flag . ' value="' . $linkUser . '' . $retour['id'] . '" ' . $selected . '>' . strtoupper($retour['nom']) . ' ' . $retour['prenom'] . '</option>';
            }else{
                $best_writers_top.= '<option ' . $best_writer_flag . ' value="' . $linkUser . '' . $retour['id'] . '" ' . $selected . '>' . strtoupper($retour['nom']) . ' ' . $retour['prenom'] . '</option>';
            }

            $tableau .= $var;
        }

        if (!empty($best_writers_top)){
            $tableau = $best_writers_top.$tableau;
        }

        return $tableau;
    }

    public function task_rules(){
        return array(
            'words_count' => array(
                'fail' => 'Votre texte contient %d mots.',
                'word_fail' => true,
                'success' => 'Le texte contient %d mots ( Dont %d pour le meta-title et %d pour la meta description)',
                'rule' => '%d mots.',
                'couple' => true,
                'multiple'  => true,
                'order' => 1
            ),
            //'words_missed' => array(
               //'fail' => 'Il manque les mots ',
            //),
            'words_used' => array(
                'rule' => 'Les mots ',
                'success' => 'Le texte contient tous les mots ',
            ),
            'min_seo_words' => array(
                'fail' => 'Le client veut au moins %d expressions cl� dans son texte.',
                'word_fail' => true,
            ),
            'max_seo_words' => array(
                'fail' => "Entre %d et %d fois l'occurence de ces mots cl�s. Ces mots ont �t� utilis�s %d fois dans le texte.",
                'word_fail' => true,
            ),
            //'words_used_count'
            'meta_title' => array(
                'rule' => 'Une balise meta title.',
                'success' => 'Le texte en contient une.',
                'fail' => "Le champ ".htmlentities('<title>')." ne peut-�tre vite.",
                //'tag'  => 'title'
                'order' => 2
            ),
            'meta_desc' => array(
                'rule' => 'Une meta description.',
                'success' => 'Le texte en contient une.',
                'fail' => 'La meta description ne peut-etre vide.',
                //'tag'  => 'title'
                'order' => 2
            ),
            'H1_set' => array(
                'rule' => 'Une balise H1.',
                'success' => 'Le texte contient une balise H1.',
                'fail' => "Il manque une balise H1.",
                'tag'  => 'h1',
                'single' => true,
                'order' => 3
            ),
            'H2_start' => array(
                'rule' => 'Entre %d et %d balises H2.',
                'success' => 'Le texte contient %d balise(s) H2.',
                'fail' => "Il manque %d balise H2.",
                'tag'  => "h2",
                'couple' => true,
                'related' => 'H2_end',
                'order' => 4
            ),
            'H3_start' => array(
                'rule' => 'Entre %d et %d balises H3.',
                'success' => 'Le texte contient %d balise(s) H3.',
                'fail' => "Il manque %d balise H3.",
                'tag'  => "h3",
                'couple' => true,
                'related' => 'H3_end',
                'order' => 5
            ),
            'bold_text' => array(
                'rule' => 'Des mots en gras.',
                'success' => 'Le texte contient des mots en gras.',
                'fail' => "Il n'y a pas de mots en gras.",
                'tag'  => 'b',
                'order' => 6
            ),
            'UL_set' => array(
                'rule' => 'Une liste � puce.',
                'success' => 'Le texte contient une �num�ration avec liste � puce.',
                'fail' => "Il n'y a pas de puces.",
                'tag'  => 'ul',
                'order' => 7
            ),
            'seo_meta_title' => array(
                'rule' => "L'utilisation d'un de ces mots-cl�s dans la meta title.",
                'success' => "La meta title contient l'expression '%s'.",
                'fail' => "Le meta title doit contenir au moins une expression SEO.",
                'word'  => true,
                'order' => 8
                //'expression' => true,
            ),
            'seo_H1_set' => array(
                'rule' => "L'utilisation de l'un de ses mots cl�s dans le titre H1.",
                'success' => "Le H1 contient l'expression '%s'.",
                'fail' => "Au moins une expression cl� doit �tre utilis�e dans le titre H1.",
                'word'  => true,
                'expression' => 'h1',
                'order' => 9
            ),
            'seo_H2_set' => array(
                'rule' => "L'utilisation de l'un de ses mots cl�s dans le titre H2.",
                'success' => "Le H2 contient l'expression '%s'.",
                'fail' => "Au moins une expression cl� doit �tre utilis�e dans le titre H2.",
                'word'  => true,
                'expression' => 'h2',
                'order' => 10
            ),
            'seo_H3_set' => array(
                'rule' => "L'utilisation de l'un de ses mots cl�s dans le titre H3.",
                'success' => "Le H2 contient l'expression '%s'.",
                'fail' => "Au moins une expression cl� doit �tre utilis�e dans le titre H3.",
                'word'  => true,
                'expression' => 'h3',
                'order' => 11
            ),
            'images_count_from' => array(
                'rule' => "%d � %d images.",
                'success' => 'Le texte contient %d images.',
                'fail' => "Vous devez utilisez dans votre texte entre %d et %d images.",
                'couple' => true,
                'word' => true,
                'array' => 'images',
                'related' => 'images_count_to',
                'order' => 12
            ),
            'seo_percent_start' => array(
                'rule' => "Entre %d et %d fois l'occurrence de ces mots cl�s.",
                'success' => 'Ces mots ont �t� utilis�s %d fois dans le texte.',
                'fail' => "Le client veut au moins %d expressions cl� dans son texte.",
                'couple' => true,
                'word_fail' => true,
                'array' => 'words',
                'related' => 'seo_percent_end',
                'order' => 13
            ),

        );
    }

    public function show_report($project,$report_data){
        $rules = $this->task_rules();

        /*
        echo '<pre>';
        var_dump($report_data);
        echo '</pre>';*/

        // get declined conditions
        $declined_conditions = isset($report_data['declined_conditions'])? $report_data['declined_conditions'] : array() ;

        // bugfix seo_words_min
        if (isset($declined_conditions['min_seo_words'])){
            $declined_conditions['seo_percent_start'] = $declined_conditions['min_seo_words'];
        }

        foreach($project as $field=>$value){

            $output = '';
            // checking declined conditions
            $declined = isset($declined_conditions[$field]);



            if (isset($rules[$field]) && $project[$field]){

                $rule = $rules[$field];
                $rule_text = $rules[$field]['rule'];
                $success_text =  $rules[$field]['success'];
                $error_text = $rules[$field]['fail'];

                // multiple (only one - words count)
                if (isset($rule['multiple'])){

                    $rule_value = $value;
                    $rule_text = sprintf($rule_text,$value);

                    $f = $report_data[$field];
                    $s = isset( $report_data['meta_title_words_count'] ) ? $report_data['meta_title_words_count'] : 0;
                    $t = isset( $report_data['meta_desc_words_count'] ) ? $report_data['meta_desc_words_count'] : 0;

                    if (!$declined){
                        $success_text = sprintf($success_text,$f,$s,$t);
                        $output =  '<div>'.$rule_text.' <span class="done">'.$success_text.'</span></div>';
                    }
                    else{
                        $success_text = sprintf($error_text,$f,$s,$t);
                        $output =  '<div><span>'.$success_text.'</span> ( Demand�: '.$rule_text.' )</div>';
                    }

                    // TODO fix Les mots problem
                    //echo $output;
                    //continue;
                }

                if (isset($rule['couple']) && empty($output)){
                    $f = $value; // first value
                    $s = $project[$rule['related']]; // second value
                    $rule_text = sprintf($rule_text,$f,$s);
                    $report_value = $report_data[$field];
                    //var_dump($field,$report_value);

                    // related multiple-data
                    // report field name <=> rules field name
                    // words
                    $seo_words_extra_info = '';
                    if ($field=='seo_percent_start'){
                        $report_value = $report_data['words_used_count'];

                        // override for declined
                        if ($declined){
                            $report_value = $report_data['min_seo_words'];
                        }

                        $words_used = $report_data['words_used'];
                        $extra_rule_text = $rules['words_used']['rule'];
                        $extra_success_text = $rules['words_used']['success'];

                        $words_used = array_map(function($word){
                            return "'".$word."'";
                        },$words_used);
                        $extra_rule_text .= implode(',',$words_used);
                        $extra_success_text .= implode(',',$words_used);

                        if (!$declined) {
                            $seo_words_extra_info = '<div>' . $extra_rule_text . '. <span class="done">' . $extra_success_text . '.</span></div>';
                        }else{
                            $seo_words_extra_info = '<div>' . $extra_rule_text . '. <span>' . $extra_success_text . '.</span></div>';
                        }
                    }
                    // images
                    if ( ($field == 'images_count_from') && !$declined){
                        $report_value = $report_data['images'];
                    }

                    if(!empty($report_value)) {
                        $success_text = sprintf($success_text, $report_value);
                    }

                    // TODO ?
                    // or deprecated?
                    if (isset($rule['array']) && isset($project[$rule['array']]) ){
                        $items = $project[$rule['array']];
                        $type = $rule['array'];
                    }

                    if (!$declined){

                        $output =  '<div>'.$rule_text.' <span class="done">'.$success_text.'</span></div>';
                    }
                    else{ // declined conditions
                        $success_text = sprintf($error_text,$report_value);

                        // bugfix
                        if ( ($field == 'images_count_from') && isset($report_data['images_count_from'])){
                              $success_text = sprintf($rules[$field]['success'],$report_value);
                        }
                        $output =  '<div><span>'.$success_text.'</span> ( Demand�: '.$rule_text.' )</div>';
                    }

                    // show special words info
                    if (!empty($seo_words_extra_info)){
                        $output .= $seo_words_extra_info;
                    }

                }else{

                    $report_value = $report_data[$field];

                    if ( ($field == 'seo_H1_set') || ($field == 'seo_H2_set') || ($field == 'seo_H3_set') || ($field=='meta_tile')){
                        $report_value = utf8_decode($report_value);
                    }

                    if(!empty($report_value))
                        $success_text = sprintf($success_text,$report_value);

                    if (!$declined){
                        $output =  '<div>'.$rule_text.' <span class="done">'.$success_text.'</span></div>';
                    }
                    else{
                        $success_text = sprintf($error_text,$report_value);
                        $output =  '<div><span>'.$success_text.'</span> ( Demand�: '.$rule_text.' )</div>';
                    }
                }

                // check declined conditions - override standard-view
                if ($declined){
                   $output = '<div class="declined_block">
                                <div class="declined_rule">'.$output.'</div>
                                <div class="declined_reason"> <b>Raison : </b><br/>'.$declined_conditions[$field].'</div>
                              </div>';
                }

                echo $output;
            }
        }


    }

    public function show_task_errors($project,$errors,$declined_conditions=array(),$extra_param = array()){
        $rules = $this->task_rules();
        $class = 'error';

        foreach($errors as $rule_param=>$error){

            if ( isset($rules[$rule_param])){

                //$rule = $rules[$field];
                //$error_text =  $rule['fail'];
                $error_text = $errors[$rule_param];
                $rule = $rules[$rule_param];


                if (isset($rule['word_fail'])){
                    $value = $project[$rule_param];

                    // checking words_count - extra
                    if ( ($rule_param=='words_count') && isset($errors['words_count_error'])){
                        $value = $errors['words_count_error'];
                    }

                    $error_text = sprintf($error_text,$value);
                }

                if (isset($rule['words_fail'])){
                    $value = $project[$rule_param];
                    $error_text = sprintf($error_text,$value);
                }
                /*if (isset($rule['couple'])){
                    $f = $value; // first value
                    $s = $project[$rule['related']]; // second value
                    $error_text = sprintf($error_text,$f,$s);
                    echo '<div class="'.$action.'">'.$rule_text.'</div>';
                }else{*/

                $declined_error = false;
                $declined_text = '';
                $display_css_rule = 'style="display:none;"';


                //var_dump($rule_param);
                //var_dump($declined_conditions);
                if (isset($declined_conditions[$rule_param])){
                    $display_css_rule = 'style="display:block;"';
                    $declined_error = true;
                    $declined_text = htmlspecialchars($declined_conditions[$rule_param], ENT_COMPAT,'ISO-8859-1', true);
                    //$declined_text = htmlspecialchars($declined_conditions[$rule_param]);
                }

                $button_text = (!$declined_error)? 'OK' : 'Modifier';
                $rule_param = trim($rule_param);

                $error_block =  '<div class="error_decline_block " data-type="'.$rule_param.'"><span class="error_header '.$action.' '.(($declined_error)? 'error_declined' : '' ).'">Erreur : '.$error_text.'</span>
                         <div class="error_decline_btn error_active" data-type="'.$rule_param.'">( Impossible de respecter ce point )</div>
                         <div class="error_inputs" '.$display_css_rule.' >
                            <b>Expliquer la raison :</b> <br/>';

                if ($declined_error){
                    $error_block .= '<span class="declined_text">'.$declined_text.'</span>';
                    $error_block .= '<input style="display:none;" data-declined="1" type="text" data-modify="true" data-type="'.$rule_param.'" value="'.$declined_text.'" class="error_input input_declined" />';
                }else{
                    $error_block .= '<span style="display: none;" class="declined_text">'.$declined_text.'</span>';
                    $error_block .= '<input type="text" data-type="'.$rule_param.'" value="'.$declined_text.'" class="error_input input_declined" />';
                }

                $error_block .= '<div class="error_buttons">
                                    <button class="confirm_reject">'.$button_text.'</button> <button class="remove_reject">Supprimer</button>
                                </div></div></div>';

                echo $error_block;
            }
        }

        //die;
    }

    public function show_task_rules($project,$action='rule'){
        $rules = $this->task_rules();
        $class = $action;

        foreach($project as $field=>$value){

            if (isset($rules[$field])){
                $rule = $rules[$field];
                $rule_text = $rules[$field]['rule'];
                if (isset($rule['couple'])){
                    $f = $value; // first value
                    $s = $project[$rule['related']]; // second value
                    $rule_text = sprintf($rule_text,$f,$s);

                    if (empty($f) && empty($s))
                        continue;


                    echo '<div class="'.$action.'">'.$rule_text.'</div>';

                    if (isset($rule['array']) && isset($project[$rule['array']]) ){
                        $items = $project[$rule['array']];
                        $type = $rule['array'];
                        $this->show_task_rules_array($items,$type);
                    }
                }else{

                    if (!empty($value))
                        echo '<div class="'.$action.'">'.$rule_text.'</div>';
                }
            }
        }
    }

    public function show_task_rules_array($items,$type){
        echo '<div class="array_list">';

        foreach($items as $item){
            $row = '';
            if ($type=='images'){
                $row = '<div class="image">'.$item['image_url'];
                $row.='</div>';
            }
            if ($type=='words'){
                $row = '<div class="word">'.utf8_decode($item).'</div>';
            }
            echo $row;
        }
        echo '</div>';
    }


    function convert_cp1252_to_utf8($input, $default = '', $replace = array()) {
        if ($input === null || $input == '') {
            return $default;
        }

        // https://en.wikipedia.org/wiki/UTF-8
        // https://en.wikipedia.org/wiki/ISO/IEC_8859-1
        // https://en.wikipedia.org/wiki/Windows-1252
        // http://www.unicode.org/Public/MAPPINGS/VENDORS/MICSFT/WINDOWS/CP1252.TXT
        $encoding = mb_detect_encoding($input, array('Windows-1252', 'ISO-8859-1'), true);
        if ($encoding == 'ISO-8859-1' || $encoding == 'Windows-1252') {
            /*
             * Use the search/replace arrays if a character needs to be replaced with
             * something other than its Unicode equivalent.
             */

            /*$replace = array(
                128 => "&#x20AC;",      // http://www.fileformat.info/info/unicode/char/20AC/index.htm EURO SIGN
                129 => "",              // UNDEFINED
                130 => "&#x201A;",      // http://www.fileformat.info/info/unicode/char/201A/index.htm SINGLE LOW-9 QUOTATION MARK
                131 => "&#x0192;",      // http://www.fileformat.info/info/unicode/char/0192/index.htm LATIN SMALL LETTER F WITH HOOK
                132 => "&#x201E;",      // http://www.fileformat.info/info/unicode/char/201e/index.htm DOUBLE LOW-9 QUOTATION MARK
                133 => "&#x2026;",      // http://www.fileformat.info/info/unicode/char/2026/index.htm HORIZONTAL ELLIPSIS
                134 => "&#x2020;",      // http://www.fileformat.info/info/unicode/char/2020/index.htm DAGGER
                135 => "&#x2021;",      // http://www.fileformat.info/info/unicode/char/2021/index.htm DOUBLE DAGGER
                136 => "&#x02C6;",      // http://www.fileformat.info/info/unicode/char/02c6/index.htm MODIFIER LETTER CIRCUMFLEX ACCENT
                137 => "&#x2030;",      // http://www.fileformat.info/info/unicode/char/2030/index.htm PER MILLE SIGN
                138 => "&#x0160;",      // http://www.fileformat.info/info/unicode/char/0160/index.htm LATIN CAPITAL LETTER S WITH CARON
                139 => "&#x2039;",      // http://www.fileformat.info/info/unicode/char/2039/index.htm SINGLE LEFT-POINTING ANGLE QUOTATION MARK
                140 => "&#x0152;",      // http://www.fileformat.info/info/unicode/char/0152/index.htm LATIN CAPITAL LIGATURE OE
                141 => "",              // UNDEFINED
                142 => "&#x017D;",      // http://www.fileformat.info/info/unicode/char/017d/index.htm LATIN CAPITAL LETTER Z WITH CARON
                143 => "",              // UNDEFINED
                144 => "",              // UNDEFINED
                145 => "&#x2018;",      // http://www.fileformat.info/info/unicode/char/2018/index.htm LEFT SINGLE QUOTATION MARK
                146 => "&#x2019;",      // http://www.fileformat.info/info/unicode/char/2019/index.htm RIGHT SINGLE QUOTATION MARK
                147 => "&#x201C;",      // http://www.fileformat.info/info/unicode/char/201c/index.htm LEFT DOUBLE QUOTATION MARK
                148 => "&#x201D;",      // http://www.fileformat.info/info/unicode/char/201d/index.htm RIGHT DOUBLE QUOTATION MARK
                149 => "&#x2022;",      // http://www.fileformat.info/info/unicode/char/2022/index.htm BULLET
                150 => "&#x2013;",      // http://www.fileformat.info/info/unicode/char/2013/index.htm EN DASH
                151 => "&#x2014;",      // http://www.fileformat.info/info/unicode/char/2014/index.htm EM DASH
                152 => "&#x02DC;",      // http://www.fileformat.info/info/unicode/char/02DC/index.htm SMALL TILDE
                153 => "&#x2122;",      // http://www.fileformat.info/info/unicode/char/2122/index.htm TRADE MARK SIGN
                154 => "&#x0161;",      // http://www.fileformat.info/info/unicode/char/0161/index.htm LATIN SMALL LETTER S WITH CARON
                155 => "&#x203A;",      // http://www.fileformat.info/info/unicode/char/203A/index.htm SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
                156 => "&#x0153;",      // http://www.fileformat.info/info/unicode/char/0153/index.htm LATIN SMALL LIGATURE OE
                157 => "",              // UNDEFINED
                158 => "&#x017e;",      // http://www.fileformat.info/info/unicode/char/017E/index.htm LATIN SMALL LETTER Z WITH CARON
                159 => "&#x0178;",      // http://www.fileformat.info/info/unicode/char/0178/index.htm LATIN CAPITAL LETTER Y WITH DIAERESIS
            );*/

            if (count($replace) != 0) {
                $find = array();
                foreach (array_keys($replace) as $key) {
                    $find[] = chr($key);
                }
                $input = str_replace($find, array_values($replace), $input);
            }
            /*
             * Because ISO-8859-1 and CP1252 are identical except for 0x80 through 0x9F
             * and control characters, always convert from Windows-1252 to UTF-8.
             */
            $input = iconv('Windows-1252', 'UTF-8//IGNORE', $input);
            if (count($replace) != 0) {
                $input = html_entity_decode($input);
            }
        }
        return $input;
    }

} 