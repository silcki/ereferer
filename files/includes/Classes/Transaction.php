<?php
//namespace Transaction;

//use RedactionProjects;

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';

/**
 * Class Transaction
 */
class Transaction
{
    private  $dbConnect;
    private  $userId;

    /**
     * @param $dbh
     * @param integer $userId
     */
    public function __construct($dbh, $userId)
    {
        $this->dbConnect = $dbh;
        $this->userId = $userId;
    }

    /**
     * @param string  $details
     * @param integer $debit
     * @param integer $credit
     * @param integer $projectsId
     */
    public function addTransaction($details, $debit, $credit, $projectsId = NULL)
    {        
        $time = date("Y-m-d H:i:s");
        $redactionProjects = new RedactionProjects($this->dbConnect);
        $solde = $redactionProjects->getUserSolde($this->userId);

        $stmt = $this->dbConnect->prepare('INSERT INTO `transaction` (user_id , debit, credit, solde, created_time, details, project_id)  
                                                          VALUES  (:user_id , :debit, :credit, :solde, :created_time, :details, :project_id)');

        $stmt->bindParam(':user_id', $this->userId);
        $stmt->bindParam(':debit', $debit);
        $stmt->bindParam(':credit', $credit);
        $stmt->bindParam(':solde', $solde);
        $stmt->bindParam(':created_time', $time);
        $stmt->bindParam(':details', $details);
        $stmt->bindParam(':project_id', $projectsId);

        $stmt->execute();
    }

    /**
     * @param integer $debit
     * @param integer $credit
     * @param integer $projectsId
     */
    public function removeTransaction($debit, $credit, $projectId = NULL)
    {
        if($projectId){
            $this->dbConnect->query("DELETE FROM `transaction` WHERE user_id ='" . $this->userId . "' AND project_id ='" . $projectId . "' AND debit='" . $debit . "' AND credit= '" . $credit . "' ORDER BY id DESC");
        }
    }

    /**
     * @return array
     */
    public function getTransactionForTable()
    {
        $tableData = [];
        if (isSu($this->userId)) {
            $request = "SELECT * FROM `transaction` ORDER BY id DESC";
        } else {
            $request = "SELECT * FROM `transaction` WHERE user_id ='" . $this->userId . "' ORDER BY id DESC";
        }
        $execution = $this->dbConnect->query($request);
        while ($return = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableData[] = [
                'date' => $return['created_time'],
                'details' => $return['details'],
                'debit' => $return['debit'],
                'credit' => $return['credit'],
                'solde' => $return['solde']
            ];
        }

        return $tableData;
    }
}