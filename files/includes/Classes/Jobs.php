<?php


class Jobs{
    private  $DB;

    function __construct(){
        global $dbh; // very bad, but..
        $this->DB = $dbh;
    }

    public function getCountDoneTaskCurrentMonth($writer_id){
        // get unix start-time for current month
        $unix_start_time   = strtotime(date('01-m-Y'));

        $sql = "SELECT COUNT(*) AS cnt
                FROM jobs AS j
                WHERE j.affectedto= " . $writer_id . "
                      AND j.adminApprouved=2
                      AND j.adminApprouvedTime > " . $unix_start_time . "
                      AND j.adminApprouvedTime <= " . time();

        $result = $this->DB->query($sql);
        $count = $result->fetch(PDO::FETCH_COLUMN,0);
        return $count;
    }

    public function getWriterStatsData($time_periods,$writer_id){
        $data = array();

        if (is_array($time_periods) && !empty($time_periods)){
            foreach($time_periods as $period){

                $sql = "SELECT COUNT(*) AS task_done,
                               SUM(j.coutReferer) AS sum_per_month
                        FROM jobs AS j
                        WHERE j.affectedto= " .$writer_id. "
                              AND j.adminApprouved=2
                              AND j.adminApprouvedTime >= " . strtotime($period['start']) . "
                              AND j.adminApprouvedTime <= " . strtotime($period['end']);


                $data_per_month = $this->DB->query($sql)->fetch(PDO::FETCH_ASSOC);
                $period['task_done'] = $data_per_month['task_done'];
                $period['writer_earn'] = round($data_per_month['sum_per_month'],2);
                $data[] = $period;
            }
        }

        return $data;
    }

    public function getRejectedByProjectId($project_id){
        $sql = "SELECT annuaire_id
                FROM jobs_rejected
                WHERE project_id=".$project_id;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_COLUMN,0);
        return $rows;
    }

    public function getCountRejectedByProjectId($project_id){
        $sql = "SELECT COUNT(*)
                FROM jobs_rejected
                WHERE project_id=".$project_id;

        $result = $this->DB->query($sql);
        $rows = $result->fetch(PDO::FETCH_COLUMN,0);
        return $rows;
    }

    /*
     *  Delete Rejected taks
     */
    public function deleteRejectedJob($project_id,$annuaire_id){
        $sql = 'DELETE FROM jobs_rejected
                WHERE project_id='.$project_id.' AND annuaire_id='.$annuaire_id;
        $result = $this->DB->query($sql);
        return $result;
    }

    /*
     *  Rejected task by Admin/SuperWriter
     */
    public function insertRejected($project_id,$annuaire_id){
        $this->deleteRejectedJob($project_id,$annuaire_id);
        $sql = 'INSERT INTO `jobs_rejected`
                VALUES ('.$project_id.','.$annuaire_id.')';
        $result = $this->DB->query($sql);
        return $result;
    }

    /*
     *  Get done tasks ids subset (with/without not active annuaires) by project ids
     *
     */
    public function getDoneTasksCountsByProjectsIds($ids){
        $out_done = array();
        $out_not_active = array();
        $out_full = array();

        if (empty($ids)) return null;

        // done tasks count
        $sql = "SELECT  GROUP_CONCAT(annuaireID) AS tasks_ids,
                        siteID as project_id
                    FROM jobs
                    WHERE siteID IN (".implode(',',$ids).")
                    GROUP BY siteID";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        foreach($rows as $row){
            $out_done[$row['project_id']] = explode(',',$row['tasks_ids']);
        }

        // fetch count of not active annuaires by projects
        $sql = "SELECT  GROUP_CONCAT(annuaire_id) AS tasks_ids,
                             project_id
                     FROM projects2annuaires_active AS p2a_a
                     WHERE project_id IN (".implode(',',$ids).")
                     GROUP BY project_id";
        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($rows)){
            foreach($rows as $row){
                $out_not_active[$row['project_id']] = explode(',',$row['tasks_ids']);
            }
        }

        // generate full task count
        foreach($ids as $project_id){
           $done= isset($out_done[$project_id]) ? $out_done[$project_id] : array();
           $not_active = isset($out_not_active[$project_id]) ? $out_not_active[$project_id] : array();

           if (!empty($not_active)){
               $out_full[$project_id] = array_merge($done,$not_active);
           }else{
               $out_full[$project_id] = $done;
           }
        }

        return array($out_done,$out_full);
    }



}