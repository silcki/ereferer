<?php

class Comments {


    public  static function getByProjectsIds(array $ids){
        global $dbh;

        if (empty($ids)) return null;


        $before = microtime(true);
        $sql = 'SELECT *
                FROM commentaires WHERE typeuser = 3
                AND idprojet IN ('.implode(',',$ids).')';

        $result = $dbh->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();

        foreach ($rows as $row) {
            $out[$row['idprojet']][$row['idannuaire']] = $row;
        }
        //echo 'Projects Comments'.(microtime(true)-$before);

        return $out;
    }
} 