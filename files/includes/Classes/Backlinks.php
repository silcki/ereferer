<?php

// refactoring

/*
ini_set('xdebug.var_display_max_depth', -1);
ini_set('xdebug.var_display_max_children', -1);
ini_set('xdebug.var_display_max_data', -1);

error_reporting(E_ALL);*/

class Backlinks {

    public  static function getByProjectsIds(array $ids){
        global $dbh;
        if (empty($ids)) return null;

        $before = microtime(true);
        $sql = "SELECT
              a2b.site_id AS site_id,
              a2b.annuaire_id AS annuaire_id,
              a2b.backlink AS backlink,
              a2b.status AS status,
              a2b.date_checked AS date_checked,
              a2b.date_checked_first AS date_checked_first,
              a.annuaire AS annuaire_link
             FROM annuaires2backlinks AS a2b
             INNER JOIN annuaires AS a
                ON ( a.id = a2b.annuaire_id)
             WHERE a2b.site_id IN (".implode(',',$ids).')';
        $result = $dbh->query($sql);

        //var_dump($sql);
        //die;

        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        $months_3 = 60 * 60 * 24 * 90; // ~3 mothns in seconds
        foreach ($rows as $row) {
            $date_checked_first = strtotime($row['date_checked_first']);
            $out[$row['site_id']][$row['annuaire_id']] = $row;
            $out[$row['site_id']][$row['annuaire_id']]['first_checked'] = empty($row['date_checked_first']) ? false : true;
        }
        return $out;
    }

    public  static function getTauxByProjectsIds(array $ids){
        global $dbh;
        if (empty($ids)) return null;

        $before = microtime(true);
        $sql = 'SELECT
                j.adminApprouved AS posted,
                a2b.status AS status,
                a2b.annuaire_id,
                a2b.site_id
                FROM jobs AS j
                INNER JOIN annuaires2backlinks AS a2b
                ON ( a2b.site_id = j.siteID AND a2b.annuaire_id = j.annuaireID)
                 WHERE j.siteID IN ('.implode(',',$ids).')';

        $result = $dbh->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        foreach ($rows as $row) {
            // set zeros for all starting keys
            if (!isset($out[$row['site_id']]['found'])){
                $out[$row['site_id']]['found'] = 0;
            }
            if (!isset($out[$row['site_id']]['notfoundyet'])){
                $out[$row['site_id']]['notfoundyet'] = 0;
            }
            if (!isset($out[$row['site_id']]['notfound'])){
                $out[$row['site_id']]['notfound'] = 0;
            }
            if (!isset($out[$row['site_id']]['impossible'])){
                $out[$row['site_id']]['impossible'] = 0;
            }


            if ($row['posted'] == 3) {
                $count_impossible++;
                $out[$row['site_id']]['impossible']++;
            }
            if ($row['posted'] == 2) {
                if (!empty($row['status'])) {
                    if ($row['status'] == "found") {
                        $count_found++;
                        $out[$row['site_id']]['found']++;
                    }
                    if ($row['status'] == "not_found") {
                        $count_notfound++;
                        $out[$row['site_id']]['notfound']++;
                    }
                    if ($row['status'] == "not_found_yet") {
                        $count_notfoundyet++;
                        $out[$row['site_id']]['notfoundyet']++;
                    }
                }
            }
        }
        foreach($out as &$item){
            $item['total'] = $item['found']+$item['notfound']+$item['notfoundyet'];
        }        unset($item);

        return $out;
    }

    public static function getBacklinksTauxTemps($annuaire_id = 0, $dateMin = '2015-01-01', $param = "cron")
    {
        global $dbh;

        //error_reporting(E_ALL);

        $sql = 'SELECT
                  annuaire_id  AS annuaire_id,
                  SUM(IF(status="found",1,0)) AS total_found,
                  SUM(IF(status="not_found",1,0)) AS total_not_found,
                  SUM(IF(status="not_found_yet",1,0)) AS total_not_found_yet,
                  SUM(IF(ROUND((UNIX_TIMESTAMP(date_found)-UNIX_TIMESTAMP(date_checked_first))/(60*60*24))>0
                        AND status_type="cron"
                        AND UNIX_TIMESTAMP(date_checked_first) > UNIX_TIMESTAMP(\''.$dateMin .'\'),
                         ROUND((UNIX_TIMESTAMP(date_found)-UNIX_TIMESTAMP(date_checked_first))/(60*60*24)),0)) AS jourCalculer,
                    SUM(IF(ROUND((UNIX_TIMESTAMP(date_found)-UNIX_TIMESTAMP(date_checked_first))/(60*60*24))>0
                        AND status_type="cron"
                        AND UNIX_TIMESTAMP(date_checked_first) > UNIX_TIMESTAMP(\''.$dateMin .'\'),
                        1,0)) AS count_Temps
                FROM annuaires2backlinks
                GROUP BY annuaire_id
                ORDER BY annuaire_id  ASC';

        $result = $dbh->query($sql);

        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        $out = array();

        foreach ($rows as $row) {
            $out[$row['annuaire_id']]['reussi'] = $row['total_found'];
            $out[$row['annuaire_id']]['echoue'] = $row['total_not_found'];
            $out[$row['annuaire_id']]['echoueyet'] = $row['total_not_found_yet'];

            $taux  = 0;
            $totalCompteTaux = intval($row['total_found'] + $row['total_not_found']);
            if ($totalCompteTaux > 0) {
                $taux = (($row['total_found']* 100) / $totalCompteTaux);
            }
            $out[$row['annuaire_id']]['taux'] = round($taux, 0);
            $out[$row['annuaire_id']]['temps'] = round($row['jourCalculer']/$row['count_Temps']);
            if (!$out[$row['annuaire_id']]['temps'])
                $out[$row['annuaire_id']]['temps'] = 0;

        }
        return $out;
    }

    public static function getWritersRatio($page=1,$limit=20){
        global $dbh;

        $sql = 'SELECT
                  a.annuaire AS annuaire,
                  annuaire_id  AS annuaire_id,
                  SUM(IF(status="found",1,0)) AS total_found,
                  SUM(IF(status="not_found",1,0)) AS total_not_found,
                  SUM(IF(status="not_found_yet",1,0)) AS total_not_found_yet,
                  j.affectedto AS writer_id,
                  u.nom AS last_name,
                  u.prenom AS first_name
                FROM annuaires2backlinks AS a2b
                INNER JOIN jobs AS j
                    ON ( a2b.site_id = j.siteID AND j.annuaireID = a2b.annuaire_id)
                INNER JOIN utilisateurs AS u
                    ON (u.id = j.affectedto)
                INNER JOIN annuaires AS a
                    ON ( a.id = a2b.annuaire_id )
                GROUP BY  annuaire_id,writer_id
                ORDER BY annuaire_id  ASC';

        $result = $dbh->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        $out = array();
        foreach($rows as $row){
            $ratio = 0;
            $total = $row['total_found']+$row['total_not_found'];
            if ($total>0){
                $ratio = (($row['total_found']* 100) / $total);
            }
            $row['ratio'] = round($ratio,0);
            $out[$row['annuaire_id']]['writers'][] = $row;
            $out[$row['annuaire_id']]['id'] = $row['annuaire_id'];
            $out[$row['annuaire_id']]['link'] = $row['annuaire'];
        }

        // emulate limiting
        $start = ($page - 1) * $limit;
        $count = count($out);
        $out = array_slice($out,$start,$limit);
        return array($out,$count);
    }
} 