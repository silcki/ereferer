<?php
/**
 * Created by PhpStorm.
 * User: voodoo417
 * Date: 04.11.2016
 * Time: 11:03
 */

require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/HTML/HTMLPurifier.auto.php';

class HTML{
    private $pufirier;
    private $config;

    function __construct(){
        $this->config = HTMLPurifier_Config::createDefault();
        $this->config->set('Core.Encoding', 'ISO-8859-1'); // fix French characters issue
        //$this->config->set('Core.EscapeNonASCIICharacters', true);
        $this->purifier = new HTMLPurifier($this->config);
    }

    public function purifyParams($data,$fields = array(),$ajax = false){
        if (is_array($data)){
            $out = array();
            foreach($data as $param=>$value){
               if (!is_array($value)) { // fix seo_words & images
                   $value = str_replace("�","'",$value);
                   $value = str_replace('�','oe',$value);
                   $out[$param] = $this->purifier->purify($value);

               }else{
                   $out[$param] = $value;
               }
            }
            return $out;
        }else{
            $data = str_replace("�","'",$data);
            $data = str_replace('�','oe',$data);
            return $this->purifier->purify($data);
        }
    }


}