<?php

class Logger
{

    const CHANNEL_INFO = 'INFO';

    const CHANNEL_ERROR = 'ERROR';

    const CHANNEL_CRITICAL = 'CRITICAL';

    const TEMPLATE = '[%s] %s: %s';

    /**
     * @var
     */
    protected $fp;

    /**
     * Logger constructor.
     *
     * @param string $file
     */
    public function __construct($file)
    {
        $this->fp = fopen(realpath(dirname(__FILE__). '/../../../logs') . DIRECTORY_SEPARATOR . $file, 'a+');
    }

    /**
     * @param string $message
     */
    public function info($message)
    {
        $this->write(self::CHANNEL_INFO, $message);
    }

    /**
     * @param string $message
     */
    public function error($message)
    {
        $this->write(self::CHANNEL_INFO, $message);
    }

    /**
     * @param string $message
     */
    public function critical($message)
    {
        $this->write(self::CHANNEL_INFO, $message);
    }

    /**
     * @param string $channel
     * @param string $message
     */
    private function write($channel, $message)
    {
        fwrite($this->fp, sprintf(self::TEMPLATE, date('Y-m-d H:i:s'), $channel, $message) . PHP_EOL);
    }

    // [2017-11-03 11:21:13] doctrine.DEBUG:

    public function __destruct()
    {
        fclose($this->fp);
    }
}