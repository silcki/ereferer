<?php


// user
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/RedactionViewHelper.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/Classes/User.php';

require_once $_SERVER['DOCUMENT_ROOT'] . '/files/includes/parsing/phpQuery.php';

class RedactionValidation
{
    private $pufirier;

    private $config;

    /**
     * @var PDO
     */
    private $dbh;

    /**
     * RedactionValidation constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    /*
     * Validate redaction project
     */

    function validateText($_POST_DATA, $report_checking = false)
    {
        $errors = array();


        // checking meta & desc
        $_POST_DATA['meta_title'] = isset($_POST_DATA['meta_title']) ? $_POST_DATA['meta_title'] : '';
        $_POST_DATA['meta_desc'] = isset($_POST_DATA['meta_desc']) ? $_POST_DATA['meta_desc'] : '';

        $text = $_POST_DATA['text'];

        $text = str_replace("�", "'", $text);

        // Get declined conditions
        $declined_conditions = isset($_POST['declined_conditions']) ? $_POST['declined_conditions'] : array();


        $project_id = $_POST_DATA['project_id'];
        $RedactionProjects = new RedactionProjects($this->dbh);
        $project = $RedactionProjects->getProject($project_id);
        $finished_project = array();

        $viewHelper = new RedactionViewHelper();
        $rules = $viewHelper->task_rules();

        // add title
        //if (!empty($_POST_DATA['meta_title'])){
        //$text = '<title>'.$_POST_DATA['meta_title'].'</title>'.$text;
        //

        $text = $_POST_DATA['meta_title'] . ' ' . $_POST_DATA['meta_desc'] . ' ' . $text;

        // words
        $text_strip = str_replace(array('&nbsp;', '.'), ' ', strip_tags($text));
        $text_words = preg_split('/\s+/', $text_strip);
        $text_words = array_filter($text_words);
        $text_words_count = count($text_words);

        if ($text_words_count < $project['words_count']) {
            $errors['words_count'] = $rules['words_count']['fail'];
            $errors['words_count_error'] = $text_words_count;
        } else {
            $finished_project['words_count'] = $text_words_count;
        }

        // save error-data if declined
        if (isset($declined_conditions['words_count']) && isset($errors['words_count'])) {
            $finished_project['words_count'] = $text_words_count;
        }

        // meta title
        if (!strlen($_POST_DATA['meta_title']) && $project['meta_title']) {
            $errors['meta_title'] = $rules['meta_title']['fail'];
        } else {
            $split_title = preg_split('/\s+/', $_POST_DATA['meta_title']);
            $split_title = array_filter($split_title);
            $finished_project['meta_title_words_count'] = count($split_title);
        }

        // save error-data if declined
        if (isset($declined_conditions['meta_title_words_count']) && isset($errors['meta_title_words_count'])) {
            $finished_project['meta_title_words_count'] = count($split_title);
        }

        // meta desc
        if (!strlen($_POST_DATA['meta_desc']) && $project['meta_desc']) {
            $errors['meta_desc'] = $rules['meta_desc']['fail'];
        } else {
            $split_desc = preg_split('/\s+/', $_POST_DATA['meta_desc']);
            $split_desc = array_filter($split_desc);
            $finished_project['meta_desc_words_count'] = count($split_desc);
        }

        // save error-data if declined
        if (isset($declined_conditions['meta_desc_words_count']) && isset($errors['meta_desc_words_count'])) {
            $finished_project['meta_desc_words_count'] = count($split_title);
        }


        count($split_title);

        $text_lower = strtolower($text);


        if (isset($project['words']) && (!empty($project['words']))) {
            $project_words = array_map('trim', $project['words']);

            $occurrence_count = 0;
            $words_used = array();
            $occurrencies = array();
            foreach ($project_words as $word) {
                $word = utf8_decode($word);
                $stripped_text = strip_tags($text_lower);
                $pattern = '#(' . preg_quote(strtolower(trim($word))) . ')#';

                preg_match_all($pattern, $stripped_text, $matches);

                if (!empty($matches[0])) {
                    $matches_count = count($matches[0]);
                    $occurrence_count += $matches_count;
                    $words_used[] = array_shift($matches[0]);
                    $occurrencies[$word] = $matches_count;
                }

            }

            //var_dump($occurrencies);

            // get "real" occurrencies count
            foreach ($project_words as $word) {
                $word = strtolower($word);

                foreach ($occurrencies as $occurrence => $count) {

                    // word it`s substrig
                    if (($word !== $occurrence) && (strpos($occurrence, $word) !== FALSE)) {
                        // check if word part of seo word
                        $occ_words = preg_split('/\s/', $occurrence);
                        $part_of_seo_word = false;
                        foreach ($occ_words as $occ_word) {
                            if ((strpos($occ_word, $word) !== FALSE) && (strlen($occ_word) != strlen($word))) {
                                $part_of_seo_word = true;
                                $occurrencies[$word] = $occurrencies[$word] - $count;
                                break;
                            }
                        }


                        //var_dump($word.' is substing of '.$occurrence);
                        if (!$part_of_seo_word)
                            $occurrencies[$word] = $occurrencies[$word] - $count;
                        //var_dump($occurrencies[$word]);
                    }

                }
                // die;

            }
            $occurrence_count = array_sum($occurrencies);

            // remove duplicates
            $words_used = array_unique(array_values($words_used));


            //var_dump('occurrence_count = ',$occurrence_count);
            //var_dump('words_used = ',$words_used);

            //die;

            // checking
            $words_used = array_filter($words_used);
            $words_not_used = array_diff($project_words, $words_used);

            // some words not used
            /*
            if (!empty($words_not_used)){
                $words_html = [];
                foreach($words_not_used as $word){
                    $words_html[] = '<b>'.$word.'</b>';
                }
                $errors['words_missed'] = $rules['words_missed']['fail'].implode(' et ',$words_html).'.';
            }else{
                $finished_project['words_used'] = $words_used;
            }*/

            $finished_project['words_used'] = $words_used;

            // calculating seo counts
            // too much used words
            $seo_percent_start = $project['seo_percent_start'];
            $seo_percent_end = $project['seo_percent_end'];
            $min_words_count = $project['words_count'];

            $min_seo_words_cnt = intval($seo_percent_start);
            $max_seo_words_cnt = intval($seo_percent_end);


            if ($min_seo_words_cnt || $max_seo_words_cnt) {

                // not required minimum
                if ($occurrence_count < $min_seo_words_cnt) {
                    $errors['min_seo_words'] = sprintf($rules['min_seo_words']['fail'], $min_seo_words_cnt);
                }

                // too many used seo words
                if ($occurrence_count > $max_seo_words_cnt) {
                    $errors['max_seo_words'] = sprintf($rules['max_seo_words']['fail'], $min_seo_words_cnt, $max_seo_words_cnt, $occurrence_count);
                }

                // report used seo words
                if (empty($errors['min_seo_words']) && empty($errors['max_seo_words'])) {
                    $finished_project['words_used_count'] = $occurrence_count;
                }

                // save error-data if declined
                if (isset($declined_conditions['min_seo_words'])
                    && (!empty($errors['min_seo_words']) || !empty($errors['max_seo_words']))) {
                    $finished_project['min_seo_words'] = $occurrence_count;
                }

            }
        }

        $doc = phpQuery::newDocument($text);

        // html tags
        foreach ($rules as $field => $rule) {

            if (isset($rule['tag'])) {
                $tag = $rule['tag'];

                $tag_length = $doc->find($tag)->length;
                if (isset($project[$field])) {
                    if (!$tag_length && $project[$field]) {
                        if (!isset($rule['couple'])) {
                            $errors[$field] = sprintf($rule['fail'], $project[$field]);

                            // save error-data if declined
                            if (isset($declined_conditions[$field]) && isset($errors[$field])) {
                                $finished_project[$field] = $project[$field];
                            }

                        } else {
                            $start_value = $project[$field];
                            $end_value = $project[$rule['related']];
                            //$tag_length = 10;

                            if (!(($tag_length >= $start_value) && ($tag_length <= $end_value))) {
                                $errors[$field] = sprintf($rule['rule'], $start_value, $end_value);
                            }

                            // save error-data if declined
                            if (isset($declined_conditions[$field]) && isset($errors[$field])) {
                                $finished_project[$field] = $tag_length;
                            }

                        }

                    }

                    if ($tag_length && $project[$field]) {
                        if (isset($rule['single']) && $tag_length > 1) {
                            $errors[$field] = sprintf($rule['fail'], $project[$field]);
                        }

                        // save error-data if declined
                        if (isset($declined_conditions[$field]) && isset($errors[$field])) {
                            $finished_project[$field] = $tag_length;
                        }

                        if (isset($rule['couple'])) {
                            $start_value = $project[$field];
                            $end_value = $project[$rule['related']];
                            //$tag_length = 10;

                            if (!(($tag_length >= $start_value) && ($tag_length <= $end_value))) {
                                $errors[$field] = sprintf($rule['rule'], $start_value, $end_value);
                            } else {
                                $finished_project[$field] = $tag_length;
                            }

                            // save error-data if declined
                            if (isset($declined_conditions[$field]) && isset($errors[$field])) {
                                $finished_project[$field] = $tag_length;
                            }
                        }
                    }
                }
                //var_dump($tag,$element);
            }


            //$project["seo_meta_title"] = true;
            // seo expression
            // check "<title>"
            if ($project[$field] && ($field == "seo_meta_title")) {
                if (isset($project['words']) && !empty($project['words'])) {
                    $meta_title = $_POST_DATA['meta_title'];
                    $checking = false;

                    foreach ($project['words'] as $word) {
                        $word = utf8_decode(trim($word));
                        $word = strtolower($word);
                        $meta_title = strtolower($meta_title);
                        if (strpos($meta_title, $word) !== false) {
                            $finished_project[$field] = $word;
                            $checking = true;
                            break;
                        }
                    }

                    if (!$checking) {
                        $errors[$field] = $rules[$field]['fail'];
                    }

                    // save error-data if declined
                    if (isset($declined_conditions[$field]) && isset($errors[$field])) {
                        $finished_project[$field] = $checking;
                    }
                }
            }

            //$project['seo_H1_set'] = true;
            //$project['seo_H2_set'] = true;
            //$project['seo_H3_set'] = true;

            $seo_doc = phpQuery::newDocument(utf8_encode($text));

            // seo headers
            if (isset($rule['expression']) && $project[$field]) {
                if (isset($project['words']) && !empty($project['words'])) {
                    $checking = false;
                    $exp_tag = $rule['expression'];
                    foreach ($project['words'] as $word) {
                        $word = trim($word);
                        $tags = $seo_doc->find($exp_tag);

                        foreach ($tags as $el) {
                            $pq = pq($el);
                            $tag_text = strtolower(trim($pq->text()));
                            $word = strtolower($word);

                            if (strpos($tag_text, $word) !== false) {
                                $finished_project[$field] = $word;
                                $checking = true;
                                break;
                            }
                        }
                        if ($checking) break;
                    }
                    if (!$checking) {
                        $errors[$field] = $rules[$field]['fail'];
                    }
                }
            }
        }

        // check images
        if (isset($project['images']) && !empty($project['images'])) {
            $images = $project['images'];
            $images_lenght = $doc->find('img')->length;

            $min_images_cnt = intval($project['images_count_from']);
            $max_images_cnt = intval($project['images_count_to']);

            if (!($min_images_cnt <= $images_lenght) && ($images_lenght <= $max_images_cnt)) {
                $errors['images_count_from'] = sprintf($rules['images_count_from']['fail'], $min_images_cnt, $max_images_cnt);
            } else {
                $finished_project['images'] = $images_lenght;
            }

            // save error-data if declined
            if (isset($declined_conditions['images_count_from']) && isset($errors['images_count_from'])) {
                $finished_project['images_count_from'] = $images_lenght;
            }

        }

        if ($report_checking) {
            return $errors;
        } else {
            return array($finished_project, $errors);
        }

    }


}
