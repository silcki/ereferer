<?php

//$document_root = dirname(dirname(__FILE__));


//require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/functions.php';
//require_once $document_root.'/functions.php';


class User {
    private  $id;
    private  $DB;
    private  $common_webmaster_tarif;

    function __construct($id){
        global $dbh; // very bad, but..
        $this->id = $id;
        $this->DB = $dbh;

        // set common tarif for all owners
        $this->setCommonWebmasterTarif();
    }

    public function getInfo($used_id){
        $sql = "SELECT *
                FROM `utilisateurs`
                WHERE id=".$used_id;

        $result = $this->DB->query($sql);
        return $result->fetch(PDO::FETCH_ASSOC);
    }

    public function getParam($param,$serialize=true){
        $sql = "SELECT $param
                FROM `utilisateurs`
                WHERE id=".$this->id;

        $result = $this->DB->query($sql);
        $data = $result->fetchColumn();
        return ($serialize)? unserialize($data):$data;
    }

    public function setParam($param,$value,$serialize=true){
        $value = ($serialize)? serialize($value):$value;
        $params = array(
            ':value'=>$value,
            ':id' => $this->id
        );
        $sql = "UPDATE `utilisateurs` SET $param = :value
                WHERE id=:id";
        $stmt = $this->DB->prepare($sql);
        $stmt->execute($params);
        return $result;
    }

    public function getRedactionPrice100Words($user_id,$user_type='webmaster'){
        $info = $this->getInfo($user_id);
        $settings = $_SESSION['allParameters'];

        $tarif = $info['tarif_redaction'];

        if (!empty($tarif)){
            return $tarif;
        }else{
            switch ($user_type){
                case 'webmaster':
                    $tarif = $settings['price_100_words'];break;
                case 'writer':
                    $tarif = $settings['writer_price_100_words'];break;
                case 'corrector':
                    $tarif = $settings['corrector_price_100_words'];break;
            }
            return $tarif['valeur'];
        }

    }

    public function changeBalance($user_id,$sum,$increasing=true){
        $sign = ($increasing)?'+':'-';
        $sql = 'UPDATE utilisateurs
                SET solde = solde '.$sign.' '.$sum.'
                WHERE id = '.$user_id;

        //var_dump($sql);

        $result = $this->DB->query($sql);
        return $result;
    }


    /*
    *  Get writer with their earnings per periods
    */
    public function getWriterEarningData($time_periods){
        $periods_data = array();

        //error_reporting(E_ERROR);

        if (is_array($time_periods) && !empty($time_periods)){
            foreach($time_periods as $period){
                $sql = "SELECT u.*,
                               SUM(j.coutReferer) AS earn
                        FROM `utilisateurs` AS u
                        LEFT JOIN  (SELECT *
                                            FROM  `jobs`
                                    WHERE adminApprouved = 2
                                        AND adminApprouvedTime >= ".strtotime($period['start'])."
                                        AND adminApprouvedTime <= ".strtotime($period['end']).") AS j
                        ON ( u.id = j.affectedto )
                        WHERE u.typeutilisateur = 3
                        GROUP BY u.id
                        HAVING earn > 0
                        ORDER BY nom ASC";

                $periods_data[] = $this->DB->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        if (count($periods_data)==1){
            return array_shift($periods_data);
        }else{
            return $periods_data;
        }
    }

    /*
     *  TODO finish for all type users
     */
    public function getUsersByType($type='writer'){

        switch($type){
           case 'writer':
               $type_id = 3;
               break;
           default:
               $type_id = 1;
        }

        $sql = "SELECT *
                FROM utilisateurs
                WHERE typeutilisateur = ".$type_id."
                ORDER BY nom ASC";

        return $this->DB->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }


    /*
     *  Get Writer Cost for 12 months by years
     */
    public function getWritersEarnStatsMonthsByYear($year,$writer_id){
        $out = array();

        $sql = "SELECT COUNT(j.coutReferer) AS `tasks_count`,
                       SUM(j.coutReferer) AS `month_sum`,
                       substr(MONTHNAME(FROM_UNIXTIME(j.adminApprouvedTime)),1,3) AS `mon`
                FROM jobs AS j
                #INNER JOIN
                WHERE j.affectedto= ".$writer_id."
                      AND j.adminApprouved=2
                      AND YEAR(FROM_UNIXTIME(j.adminApprouvedTime)) = ".$year."
                GROUP BY MONTH(FROM_UNIXTIME(j.adminApprouvedTime)) ASC";

        $rows = $this->DB->query($sql)->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($rows)){
            foreach($rows as $row){
                $out[$row['mon']]['tasks_count'] = $row['tasks_count'];
                $out[$row['mon']]['month_sum'] = $row['month_sum'];
            }
        }
        return $out;
    }

    /*
     *   REFACTORING 2017
     *   Compability
     */

    /*
     *  Reset to zero field `tentatives`
     *  Field - last time of sending for user mail about "not enough balance"
     */
    public function resetTimeEmailsLowBalanceByIds($ids){
        if (!empty($ids)) {
            $sql = 'UPDATE utilisateurs
                    SET tentatives = "0" WHERE id IN (' . implode(',', $ids) . ')';
           return $this->DB->query($sql);
        }
    }


    /*
     *  Get projects user info by project_ids
     *  Annuaire-dashboard
     */

    public function getFullInfoByIds($ids){
        $out = array();

        if (empty($ids)) return null;

        $sql = "SELECT u.*,
                       p.id AS project_id,
                       p.affectedTO AS writer_id,
                       w.nom AS writer_nom,
                       w.prenom AS writer_prenom
                FROM utilisateurs AS u
                INNER JOIN projets AS p
                    ON ( u.id = p.proprietaire)
                LEFT JOIN utilisateurs AS w
                    ON ( w.id = p.affectedTO )
                WHERE p.id IN  (".implode(',',$ids).")";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        // processing results
        foreach($rows as $row){
            // writer full_name
            if (!empty($row['writer_nom']) && !empty($row['writer_prenom'])){
                $row['writer_fullname'] = strtoupper($row['writer_nom'].' '.$row['writer_prenom']);
            }else{
                $row['writer_fullname'] = " "; // for compability with shit-code
            }

            // owner full name
            $row['owner_fullname'] = strtoupper($row['nom'].' '.$row['prenom']);

            // checking if wembaster can has tasks
            $row['webmaster_can'] = $this->isWebmasterCan($row);
            $row['raportQ'] = $this->webmasterPossibilities($row);

            $out[$row['project_id']] = $row;
        }

        return $out;
    }

    /*
     *  Rewritting "Functions::isWebmasterCan"
     */
    public function isWebmasterCan($user_info,$compareTO = 0){
        $soldeAuteur = $user_info['solde'];
        $tarifWebmaster = $user_info['frais'];

        // owner has specified tarif - set specified
        // otherwise - set common for all
        $tarifWebmaster = ($tarifWebmaster > 0.0) ? $tarifWebmaster : $this->common_webmaster_tarif;

        if ($compareTO > 0) {
            $tarifWebmaster = $compareTO;
        }

        if ($soldeAuteur >= $tarifWebmaster) {
            return true;
        } else {
            return false;
        }

    }

    /*
     *  Rewritting "Functions::isWebmasterCan"

     */
    public function webmasterPossibilities($user_info) {
        $raportQ = 0;

        if ($user_info['id'] > 0) {
            $compteSolde = $user_info['solde'];
            $getFacturation = $user_info['frais'];
            $getFacturation = ($getFacturation > 0.0) ? $getFacturation : $this->common_webmaster_tarif;

            $raportQ = round(($compteSolde / $getFacturation), 0, PHP_ROUND_HALF_DOWN);
        }

        return $raportQ;
    }

    /*
     * Param ID = 26. Table `params`.
     * "Tarification des webmaster"
     */
    public function setCommonWebmasterTarif(){
        $sql = "SELECT valeur
                FROM parametres
                WHERE id='26'";
        $result = $this->DB->query($sql);
        $this->common_webmaster_tarif = $result->fetch(PDO::FETCH_COLUMN,0);
    }




} 