<?php

class Partners
{
    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var RedactionProjects
     */
    private $redactionProjects;

    /**
     * @var RedactionReports
     */
    private $redactionReports;

    /**
     * @var EchangesProposition
     */
    private $echangesProposition;

    /**
     * @var EchangeSites
     */
    private $echangeSites;

    private $logger;

    /**
     * Partners constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;

        if (function_exists('apache_request_headers')) {
            $this->headers = apache_request_headers();
        } else {
            $this->headers['Authorization'] = !empty($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION']:null;
        }

        $this->redactionProjects = new RedactionProjects($dbh);
        $this->redactionReports = new RedactionReports($dbh);
        $this->echangesProposition = new EchangesProposition($dbh);
        $this->echangeSites = new EchangeSites($dbh);

        $this->logger = new Logger('partners.log');
    }

    /**
     * @return string
     */
    public function validate()
    {
        if (strtolower($_SERVER['REQUEST_METHOD']) != 'patch') {
            return $this->response(sprintf('Expected method PATCH, got %s ', $_SERVER['REQUEST_METHOD']), 400);
        }

        if (!$this->hasBearerToken()) {
            return $this->response(sprintf('Expected authorization token'), 400);
        }

        if (null == ($token = $this->getToken())) {
            return $this->response(sprintf('Token is null'), 400);
        }

        if (null == ($partnerId = $this->getPartnerByToken($token))) {
            return $this->response(sprintf('Partner wit API key %s hasn\'t found', $token), 400);
        }

        $data = file_get_contents('php://input');

        if (empty($data)) {
            return $this->response(sprintf('Data is empty'), 400);
        }

        try {
            $json = json_decode($data, true);
        } catch (\Exception $e) {
            return $this->response(sprintf('JSON data is not valid'), 400);
        }

        if (empty($json['website_url'])) {
            return $this->response(sprintf('JSON key website_url is empty'), 400);
        }

        if (empty($json['plugin_url'])) {
            return $this->response(sprintf('JSON key plugin_url is empty'), 400);
        }

        if (empty($json['categories'])) {
            return $this->response(sprintf('JSON key categories is empty'), 400);
        }

        if (!$this->updatePartner($partnerId, $json)) {
            return $this->response(sprintf('Couldn\'t update partner site'), 400);
        }

        return $this->response('Success');
    }

    /**
     * @param integer $project_id
     *
     * @return bool
     */
    public function publishArticle($project_id)
    {
        $this->logger->info('Project = ' . $project_id);

        $projectInfo = $this->redactionProjects->getProject($project_id);

        if (empty($projectInfo)) {
            $this->logger->error(sprintf('Project info for ID = %s is empty', $project_id));

            return false;
        }

        $echangesPropositionInfo = $this->echangesProposition->getData($projectInfo['prop_id']);

        if (empty($echangesPropositionInfo)) {
            $this->logger->error(sprintf('Echanges proposition info for project ID = %s is empty', $project_id));

            return false;
        }

        if (!$this->isPluginSet($echangesPropositionInfo['to_site_id'])) {
            $this->logger->error('Plugin not found');

            return false;
        }

        $echangeSitesInfo = $this->echangeSites->getData($echangesPropositionInfo['to_site_id']);

        if (empty($echangeSitesInfo)) {
            $this->logger->error(sprintf('Echanges sites for project ID = %s is empty', $project_id));

            return false;
        }

        if (empty($echangeSitesInfo['plugin_url']) || empty($echangeSitesInfo['api_key'])) {
            $this->logger->error('Plugin url or API key are empty');

            return false;
        }

        $redactionReportInfo = $this->redactionReports->getReport($project_id);

        if (empty($redactionReportInfo)) {
            return false;
        }

        $linksArray = unserialize($projectInfo['links_array']);
        $postTitle = isset($linksArray[0]['anchor']) ? $linksArray[0]['anchor']:'';

        if ($projectInfo['H1_set'] == 1) {
            $postTitle = $this->parsePostTitle($redactionReportInfo['text'], $postTitle);
        }

        $data = [
            'post_title' => $postTitle,
            'post_content' => $redactionReportInfo['text'],
            'categories' => $this->getCheckedCategory($project_id),
            'front_image' => $redactionReportInfo['front_image'],
        ];

        if (!empty($redactionReportInfo['meta_title'])) {
            $data['meta_title'] = $redactionReportInfo['meta_title'];
        }

        if (!empty($redactionReportInfo['meta_desc'])) {
            $data['meta_description'] = $redactionReportInfo['meta_desc'];
        }

        if (!empty($redactionReportInfo['image_sources'])) {
            $data['custom_field'] = $redactionReportInfo['image_sources'];
        }

        array_walk_recursive($data, function(&$value) {
            $value = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
        });

        $dataJson = json_encode($data, JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP);

        if ($dataJson === false) {
            $this->logger->critical('Array data is invalid');

            return false;
        }

        $this->logger->info(sprintf('Sending data: %s', $dataJson));

        $curl = curl_init($echangeSitesInfo['plugin_url']);
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $echangeSitesInfo['api_key']
        ) );
        curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $dataJson );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

        $response = curl_exec($curl);

        curl_close($curl);

        $responseJson = json_decode($response, true);

        return isset($responseJson['status']) ? $responseJson['status']:false;
    }

    /**
     * @param integer $siteId
     *
     * @return bool
     */
    public function isPluginSet($siteId)
    {
        $sql = 'SELECT `website_url`, `plugin_url` FROM `echange_sites` WHERE `id` = :id';

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':id' => $siteId,
            ]
        );
        $result = $sth->fetch();
        if ( isset($result['website_url'], $result['plugin_url']) ){

            return true;
        }

        return false;
    }

    /**
     * @param integer $siteId
     *
     * @return array
     */
    public function isCategoriesSet($siteId)
    {
        $return = [];
        $sql = 'SELECT * FROM `echange_sites_categories` WHERE `echange_sites_id` = :id';
        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(array(':id' => $siteId));

        while ($result = $sth->fetch()) {
            $return[$result['ext_id']] = $result['name'];
        }

        return $return;
    }

    /**
     * @param integer $projectId
     * @param array   $categories
     */
    public function setRedactionReportsCategories($projectId, $categories)
    {
        try {
            $sql = 'DELETE FROM `redaction_reports_categories` WHERE `project_id`=:project_id';

            $sth = $this->dbh->prepare($sql);
            $sth->bindParam(':project_id', $projectId);
            $sth->execute();

            $sth = $this->dbh->prepare("INSERT INTO `redaction_reports_categories` (project_id , ext_id) VALUES  (:project_id , :ext_id)");
            foreach ($categories as $extId => $category){
                $data = [
                    ':project_id' => $projectId,
                    ':ext_id' => $extId
                ];
                $sth->execute($data);
            }
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param integer $projectId
     *
     * @return array
     */
    public function getCheckedCategory($projectId)
    {
        $return = [];
        $sth = $this->dbh->prepare("SELECT * FROM `redaction_reports_categories` WHERE `project_id` = :project_id");
        $sth->execute(array(':project_id' => $projectId));
        while ($result = $sth->fetch()) {
            $return[] = (int) $result['ext_id'];
        }

        return $return;
    }

    /**
     * @return bool
     */
    private function hasBearerToken()
    {
        return isset($this->headers['Authorization']) && (strpos(strtolower($this->headers['Authorization']), 'bearer') !== false);
    }

    /**
     * @return string
     */
    private function getToken()
    {
        $bearer = strtolower($this->headers['Authorization']);

        if (!preg_match('/' . preg_quote('bearer', '/') . '\s(\S+)/', $bearer, $matches)) {
            return null;
        }

        return $matches[1];
    }

    /**
     * @param string $token
     *
     * @retrun int|null
     */
    private function getPartnerByToken($token)
    {
        $sql = 'SELECT `id` FROM `echange_sites` WHERE `api_key` = :api_key';

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(array(':api_key' => $token));
        $result = $sth->fetch();

        return !empty($result['id']) ? (int) $result['id']:null;
    }

    /**
     * @param integer $partnerId
     * @param array   $data
     *
     * @return bool
     */
    private function updatePartner($partnerId, $data)
    {
        try {
            $sql = 'UPDATE `echange_sites` SET `website_url`=:website_url, `plugin_url`=:plugin_url WHERE `id`=:id';

            $sth = $this->dbh->prepare($sql);
            $sth->bindParam(':website_url', $data['website_url']);
            $sth->bindParam(':plugin_url', $data['plugin_url']);
            $sth->bindParam(':id', $partnerId);
            $sth->execute();

            $sql = 'DELETE FROM `echange_sites_categories` WHERE `echange_sites_id`=:echange_sites_id';

            $sth = $this->dbh->prepare($sql);
            $sth->bindParam(':echange_sites_id', $partnerId);
            $sth->execute();

            foreach ($data['categories'] as $category) {
                $sql = 'INSERT INTO `echange_sites_categories` 
                        SET `echange_sites_id`=:echange_sites_id, 
                        `ext_id`=:ext_id,
                        `name`=:name';

                $sth = $this->dbh->prepare($sql);
                $sth->bindParam(':echange_sites_id', $partnerId);
                $sth->bindParam(':ext_id', $category['internal_id']);
                $sth->bindParam(':name', $category['name']);
                $sth->execute();
            }

        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param mixed $message
     * @param int   $code
     *
     * @return string
     */
    private function response($message = null, $code = 200)
    {
        header_remove();
        http_response_code($code);

        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
        header('Content-Type: application/json');
        $status = [
            200 => '200 OK',
            400 => '400 Bad Request',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error'
        ];

        header('Status: '.$status[$code]);

        return json_encode(array(
            'status' => $code < 300,
            'message' => $message,
        ));
    }

    /**
     * @param string $text
     * @param string $default
     *
     * @return string
     */
    private function parsePostTitle($text, $default = '')
    {
        $pattern = '/<h1(?:.*)?>(.*)<\/h1>/Uis';

        preg_match($pattern, $text, $out);

        return !empty($out[1]) ? strip_tags($out[1]): $default;
    }

}