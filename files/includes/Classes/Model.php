<?php


class Model
{
    protected  $table_name = '';

    public function array_column($array, $column_title){
        $column_items = array_map(function($item) use ($column_title){
            return !empty($item[$column_title])? $item[$column_title] : null;
        },$array);
        return $column_items;
    }
}