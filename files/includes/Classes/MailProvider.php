<?php


// PHPMailer
// error_reporting(E_ALL);

//require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/functions.php';
//require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/phpmailer/class.phpmailer.php';
//require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';

$document_root = dirname(dirname(__FILE__));

require_once $document_root . '/functions.php';
require_once $document_root . '/phpmailer/class.phpmailer.php';
require_once $document_root . '/Classes/User.php';

//var_dump($document_root.'/Classes/User.php');
//var_dump(file_exists( $document_root.'/Classes/User.php'));


class MailProvider
{

    public function NoticeBacklinkFound($project_id, $annuaire_id, $backlink)
    {
        $annuaire_data = Functions::getAnnuaireById($annuaire_id);
        $project_data = Functions::getProjetByID($project_id);
        $project_user_id = $project_data['proprietaire'];
        $user = Functions::getUserInfos($project_user_id);

        // check user settings - send or not notice email
        $user_settings = $this->checkUserSettings($project_user_id);
        if (!$user_settings['notification_backlink_found'])
            return;

        $annuaire_url = $annuaire_data['annuaire'];
        $recipient = $user['email'];

        $subject = 'Votre lien sur ' . $annuaire_url . ' a �t� trouv�.';
        $body = "Bonjour,<br/><br/>"
            . "L'annuaire " . $annuaire_url . " a valid� votre lien. Vous pouvez le voir � l'adresse suivante :<br/>"
            . $backlink
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $this->sendMessage($subject, $body, $recipient);
    }

    // cron
    public function CronNoticeBacklinkFound($project_id, $annuaire_id, $backlink, $con)
    {
        $annuaire_data = $this->getAnnuaireById($annuaire_id, $con);
        $project_data = $this->getProjetByID($project_id, $con);
        $project_user_id = $project_data['proprietaire'];
        $user = $this->getUserInfos($project_user_id, $con);

        // check user settings - send or not notice email
        $user_settings = $this->checkUserSettingsCron($project_user_id, $con);
        if (!$user_settings['notification_backlink_found'])
            return;

        $annuaire_url = $annuaire_data['annuaire'];
        $recipient = $user['email'];

        $subject = 'Votre lien sur ' . $annuaire_url . ' a �t� trouv�.';
        $body = "Bonjour,<br/><br/>"
            . "L'annuaire " . $annuaire_url . " a valid� votre lien. Vous pouvez le voir � l'adresse suivante :<br/>"
            . $backlink
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $this->sendMessage($subject, $body, $recipient);
    }


    public function NoticeProjectIsFinished($project_id)
    {
        $project_data = Functions::getProjetByID($project_id);
        $project_user_id = $project_data['proprietaire'];
        $user = Functions::getUserInfos($project_user_id);

        // check user settings - send or not notice email
        $user_settings = $this->checkUserSettings($project_user_id);
        if (!$user_settings['notification_project_finished'])
            return;

        $project_url = $project_data['lien'];
        $recipient = $user['email'];
        $user_name = $user['prenom'];

        $subject = 'Votre projet, ' . $project_url . ', est termin� sur Ereferer';

        $body = "Bonjour " . $user_name . ",<br/><br/>"
            . "Votre projet," . $project_url . ", est maintenant termin�. Vous pouvez voir le rapport d�taill� � cette adresse:<br/>"
            . "http://www.ereferer.fr/compte/details.html?id=" . $project_id . "&from=over"
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $this->sendMessage($subject, $body, $recipient);
    }

    public function NoticeRedactionFinishedProject($project_id, $user_id, $project_title)
    {

        $user = Functions::getUserInfos($user_id);
        $recipient = $user['email'];
        $user_name = $user['prenom'];

        $subject = 'Votre projet,' . utf8_decode($project_title) . ', est termin� sur Ereferer';

        $body = "Bonjour " . $user_name . ",<br/><br/>"
            . "Votre projet de r�daction est maintenant termin�. Vous pouvez le voir � l'adresse suivante:<br/>"
            . "http://www.ereferer.fr/compte/redactionview.html?id=" . $project_id . ""
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $this->sendMessage($subject, $body, $recipient);

    }

    public function NoticeNewProp($user_id)
    {

        $user = Functions::getUserInfos($user_id);
        $recipient = $user['email'];
        $user_name = $user['prenom'];

        $subject = 'Vous avez re�u une nouvelle proposition dans le module d\'�change d\'articles sur Ereferer';

        $body = "Bonjour " . $user_name . ",<br/><br/>"
            . "Vous avez re�u une nouvelle proposition dans le module d'�change d'articles :<br/>"
            . "http://www.ereferer.fr/compte/echangespropositionsrecues/liste.html"
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $user_settings = $this->checkUserSettings($user_id);

        if ($user_settings['notification_new_prop']) $this->sendMessage($subject, $body, $recipient);

    }

    public function NoticeModProp($user_id)
    {

        $user = Functions::getUserInfos($user_id);
        $recipient = $user['email'];
        $user_name = $user['prenom'];

        $subject = 'Vous avez re�u une demande de modification dans le module d\'�change d\'articles sur Ereferer';

        $body = "Bonjour " . $user_name . ",<br/><br/>"
            . "Vous avez re�u une demande de modification dans le module d'�change d'articles :<br/>"
            . "http://www.ereferer.fr/compte/echangespropositionsrecues/liste.html"
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $user_settings = $this->checkUserSettings($user_id);

        if ($user_settings['notification_mod_prop']) $this->sendMessage($subject, $body, $recipient);

    }


    public function NoticeSiteDisabled($user_id, $site_id)
    {

        $user = Functions::getUserInfos($user_id);
        $recipient = $user['email'];
        $user_name = $user['prenom'];
        $site = Functions::getSite($site_id);

        $subject = 'Votre site ' . $site['domain'] . ' � �t� desactiv� sur Ereferer';

        $body = "Bonjour " . $user_name . ",<br/><br/>"
            . "Vous n'avez pas r�agit assez vite � une proposition d'�change d'article sur Ereferer, de ce fait votre site $site[domain] � �t� desactiv�, vous pouvez le r�activer manuellement � cette adresse : <br/>"
            . "http://www.ereferer.fr/compte/echangesgestion/liste.html"
            . "<br/><br/>Bien cordialement,<br/>Emmanuel";

        $user_settings = $this->checkUserSettings($user_id);
        if ($user_settings['notification_site_disabled']) $this->sendMessage($subject, $body, $recipient);
    }


    private function sendMessage($subject, $body, $recipient)
    {
        $mail = new PHPMailer();

        // test params
        if (($_SERVER['REMOTE_ADDR'] == '127.0.0.1') || ($_SERVER['REMOTE_ADDR'] == '37.45.210.116')) {
            $recipient = 'vladimir.zabelo@gmail.com';

            $mail->IsSMTP(); // send via SMTP
            $mail->SMTPAuth = true; // turn on SMTP authentication
            $mail->SMTPSecure = "tls";
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 587;
            $mail->Username = "vladimir.zabelo@gmail.com"; // SMTP username
            $mail->Password = "voodoo6242231RTI"; // SMTP password
            $mail->SMTPDebug = 2;
        }

        $fromName = 'Ereferer';

        $mail->Timeout = '30';
        $mail->CharSet = 'UTF-8';
        $mail->From = "noreply@ereferer.fr";
        $mail->FromName = $fromName;
        $mail->Subject = $this->convert_cp1252_to_utf8($subject);
        $mail->AltBody = "";
        $mail->IsHTML(true);
        $mail->MsgHTML($this->convert_cp1252_to_utf8($body));
        $mail->AddAddress($recipient, "");
        if (!$mail->Send()) {
            // throw error
            if (($_SERVER['REMOTE_ADDR'] == '127.0.0.1') || ($_SERVER['REMOTE_ADDR'] == '37.45.210.116')) {
                var_dump($mail->ErrorInfo);
            }
        };
    }

    private function checkUserSettings($user_id)
    {
        $user = new User($user_id);
        $user_settings = $user->getParam('settings');

        // if empty - set default
        if (empty($user_settings)) {
            $user_settings['notification_zero_amount'] = true;
            $user_settings['notification_project_finished'] = true;
            $user_settings['notification_backlink_found'] = false;
            $user_settings['notification_new_prop'] = true;
            $user_settings['notification_site_disabled'] = true;
            $user_settings['notification_mod_prop'] = true;
        }

        //var_dump($user_settings);
        return $user_settings;
    }


    private function checkUserSettingsCron($user_id, $con)
    {
        $sql = "SELECT settings
                FROM `utilisateurs`
                WHERE id=" . $user_id;

        $result = $con->query($sql);
        $data = $result->fetchColumn();
        $data = unserialize($data);

        // if empty - set default
        if (empty($user_settings)) {
            $user_settings['notification_zero_amount'] = true;
            $user_settings['notification_project_finished'] = true;
            $user_settings['notification_backlink_found'] = false;
            $user_settings['notification_new_prop'] = true;
            $user_settings['notification_site_disabled'] = true;
            $user_settings['notification_mod_prop'] = true;
        }

        return $user_settings;
    }

    function convert_cp1252_to_utf8($input, $default = '', $replace = array())
    {
        if ($input === null || $input == '') {
            return $default;
        }

        // https://en.wikipedia.org/wiki/UTF-8
        // https://en.wikipedia.org/wiki/ISO/IEC_8859-1
        // https://en.wikipedia.org/wiki/Windows-1252
        // http://www.unicode.org/Public/MAPPINGS/VENDORS/MICSFT/WINDOWS/CP1252.TXT
        $encoding = mb_detect_encoding($input, array('Windows-1252', 'ISO-8859-1'), true);
        if ($encoding == 'ISO-8859-1' || $encoding == 'Windows-1252') {
            /*
             * Use the search/replace arrays if a character needs to be replaced with
             * something other than its Unicode equivalent.
             */

            /*$replace = array(
                128 => "&#x20AC;",      // http://www.fileformat.info/info/unicode/char/20AC/index.htm EURO SIGN
                129 => "",              // UNDEFINED
                130 => "&#x201A;",      // http://www.fileformat.info/info/unicode/char/201A/index.htm SINGLE LOW-9 QUOTATION MARK
                131 => "&#x0192;",      // http://www.fileformat.info/info/unicode/char/0192/index.htm LATIN SMALL LETTER F WITH HOOK
                132 => "&#x201E;",      // http://www.fileformat.info/info/unicode/char/201e/index.htm DOUBLE LOW-9 QUOTATION MARK
                133 => "&#x2026;",      // http://www.fileformat.info/info/unicode/char/2026/index.htm HORIZONTAL ELLIPSIS
                134 => "&#x2020;",      // http://www.fileformat.info/info/unicode/char/2020/index.htm DAGGER
                135 => "&#x2021;",      // http://www.fileformat.info/info/unicode/char/2021/index.htm DOUBLE DAGGER
                136 => "&#x02C6;",      // http://www.fileformat.info/info/unicode/char/02c6/index.htm MODIFIER LETTER CIRCUMFLEX ACCENT
                137 => "&#x2030;",      // http://www.fileformat.info/info/unicode/char/2030/index.htm PER MILLE SIGN
                138 => "&#x0160;",      // http://www.fileformat.info/info/unicode/char/0160/index.htm LATIN CAPITAL LETTER S WITH CARON
                139 => "&#x2039;",      // http://www.fileformat.info/info/unicode/char/2039/index.htm SINGLE LEFT-POINTING ANGLE QUOTATION MARK
                140 => "&#x0152;",      // http://www.fileformat.info/info/unicode/char/0152/index.htm LATIN CAPITAL LIGATURE OE
                141 => "",              // UNDEFINED
                142 => "&#x017D;",      // http://www.fileformat.info/info/unicode/char/017d/index.htm LATIN CAPITAL LETTER Z WITH CARON
                143 => "",              // UNDEFINED
                144 => "",              // UNDEFINED
                145 => "&#x2018;",      // http://www.fileformat.info/info/unicode/char/2018/index.htm LEFT SINGLE QUOTATION MARK
                146 => "&#x2019;",      // http://www.fileformat.info/info/unicode/char/2019/index.htm RIGHT SINGLE QUOTATION MARK
                147 => "&#x201C;",      // http://www.fileformat.info/info/unicode/char/201c/index.htm LEFT DOUBLE QUOTATION MARK
                148 => "&#x201D;",      // http://www.fileformat.info/info/unicode/char/201d/index.htm RIGHT DOUBLE QUOTATION MARK
                149 => "&#x2022;",      // http://www.fileformat.info/info/unicode/char/2022/index.htm BULLET
                150 => "&#x2013;",      // http://www.fileformat.info/info/unicode/char/2013/index.htm EN DASH
                151 => "&#x2014;",      // http://www.fileformat.info/info/unicode/char/2014/index.htm EM DASH
                152 => "&#x02DC;",      // http://www.fileformat.info/info/unicode/char/02DC/index.htm SMALL TILDE
                153 => "&#x2122;",      // http://www.fileformat.info/info/unicode/char/2122/index.htm TRADE MARK SIGN
                154 => "&#x0161;",      // http://www.fileformat.info/info/unicode/char/0161/index.htm LATIN SMALL LETTER S WITH CARON
                155 => "&#x203A;",      // http://www.fileformat.info/info/unicode/char/203A/index.htm SINGLE RIGHT-POINTING ANGLE QUOTATION MARK
                156 => "&#x0153;",      // http://www.fileformat.info/info/unicode/char/0153/index.htm LATIN SMALL LIGATURE OE
                157 => "",              // UNDEFINED
                158 => "&#x017e;",      // http://www.fileformat.info/info/unicode/char/017E/index.htm LATIN SMALL LETTER Z WITH CARON
                159 => "&#x0178;",      // http://www.fileformat.info/info/unicode/char/0178/index.htm LATIN CAPITAL LETTER Y WITH DIAERESIS
            );*/

            if (count($replace) != 0) {
                $find = array();
                foreach (array_keys($replace) as $key) {
                    $find[] = chr($key);
                }
                $input = str_replace($find, array_values($replace), $input);
            }
            /*
             * Because ISO-8859-1 and CP1252 are identical except for 0x80 through 0x9F
             * and control characters, always convert from Windows-1252 to UTF-8.
             */
            $input = iconv('Windows-1252', 'UTF-8//IGNORE', $input);
            if (count($replace) != 0) {
                $input = html_entity_decode($input);
            }
        }
        return $input;
    }

    // helpers-methods for cron-job
    public function getAnnuaireById($id = 0, $con)
    {
        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE id=" . intval($id);
        $execution = $con->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['annuaire'] = $retour['annuaire'];
        }
        return $tableau;
    }

    public function getProjetByID($id, $con)
    {
        $tableau = array();
        $requete = "SELECT * FROM projets WHERE id=" . $id;
        $execution = $con->query($requete);
        $retour = $execution->fetch();
        return $retour;
    }

    public function getUserInfos($id = 0, $con)
    {
        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE id=" . intval($id);
        $execution = $con->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['nom'] = $retour['nom'];
            $tableau['prenom'] = $retour['prenom'];
            $tableau['email'] = $retour['email'];
            $tableau['avatar'] = $retour['avatar'];
            $tableau['ville'] = $retour['ville'];
            $tableau['codepostal'] = $retour['codepostal'];
            $tableau['societe'] = $retour['societe'];
            $tableau['adresse'] = $retour['adresse'];
            $tableau['siteweb'] = $retour['siteweb'];
            $tableau['styleAdmin'] = $retour['styleAdmin'];
            $tableau['lastIP'] = $retour['lastIP'];
            $tableau['telephone'] = $retour['telephone'];
            $tableau['devise'] = $retour['devise'];
            $tableau['solde'] = $retour['solde'];
            $tableau['frais'] = $retour['frais'];
            $tableau['tentatives'] = $retour['tentatives'];
            $tableau['typeutilisateur'] = $retour['typeutilisateur'];
            $tableau['active'] = $retour['active'];
            $tableau['connected'] = $retour['connected'];
            $tableau['lastlogin'] = $retour['lastlogin'];
            $tableau['joinTime'] = $retour['joinTime'];
            $tableau['lastPayment'] = $retour['lastPayment'];
            $tableau['AmountLastPayment'] = $retour['AmountLastPayment'];
            $count++;
        }
        return $tableau;
    }


}