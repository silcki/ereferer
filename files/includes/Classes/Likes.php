<?php


/*
 * Annuaire Module
 */

class Likes {
    private  $DB;
    private  $writers_worse_percent = 0.6;
    private  $writers_best_percent =  0.85;

    function __construct($id=0){
        global $dbh; // very bad, but..
        $this->DB = $dbh;
    }

    public function getWritersLikesByProjectId($id){
        $sql = "SELECT * FROM writers_likes
                WHERE project_id = ".$id;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function getFavoriteWriters(){
        $sql = "SELECT t.client_id,
                       t.writer_id,
                       u.nom,
                       u.prenom
                FROM (SELECT p.proprietaire  AS client_id,
                             wl.writer_id AS writer_id,
                             SUM(wl.value)/COUNT(wl.value) AS rating
                FROM writers_likes AS wl
                INNER JOIN projets AS p
                 ON ( wl.project_id = p.id)
                GROUP BY wl.writer_id,p.proprietaire
                HAVING rating > 0 ) AS t
                INNER JOIN utilisateurs AS u
                  ON (u.id = t.writer_id)
                GROUP BY t.client_id,t.writer_id
                ORDER BY rating ASC";

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        foreach($rows as $row){
            $out[$row['client_id']] = strtoupper($row['nom'].' '.$row['prenom']);
        }
        return $out;
    }

    public function getStatsForWriter($writer_id,$status=1){
        $sql = 'SELECT COUNT(value) AS total,
                       COUNT(DISTINCT p.proprietaire) AS clients_cnt
                FROM writers_likes AS wl
                LEFT JOIN projets AS p
                 ON ( p.id = wl.project_id)
                WHERE writer_id = '.$writer_id.' AND value='.$status;

        $result = $this->DB->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getStatsForWriters($status){
        $sql = 'SELECT COUNT(value) AS total,
                       COUNT(DISTINCT p.proprietaire) AS clients_cnt,
                       wl.writer_id AS writer_id
                FROM  writers_likes AS wl
                LEFT JOIN projets AS p
                 ON ( p.id = wl.project_id)
                WHERE value= '.$status.'
                GROUP BY writer_id';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[$row['writer_id']] = $row;
            }
        }
        return $out;
    }

    public function getSatisfactionRatesForWebmasters(){
        $sql = 'SELECT   SUM(CASE WHEN wl.value = 1 THEN 1 ELSE 0 END) AS likes_cnt,
                         SUM(CASE WHEN wl.value = 0 THEN 1 ELSE 0 END) AS unlikes_cnt,
                         ROUND((SUM(wl.value)/COUNT(wl.value))*100,0) AS rate,
                         p.proprietaire AS user_id
                FROM  writers_likes AS wl
                INNER JOIN projets AS p
                ON ( p.id = wl.project_id)
                GROUP BY p.proprietaire';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[$row['user_id']] = $row;
            }
        }
        return $out;
    }


    public function getWorseWritersByProjectID($project_id){
        $sql = 'SELECT wl.writer_id AS writer_id,
                       SUM(wl.value)/COUNT(wl.value) AS rating,
                       COUNT(wl.value) AS cnt
                FROM writers_likes AS wl
                INNER JOIN projets AS p
                 ON ( wl.project_id = p.id)
                WHERE p.proprietaire = (SELECT proprietaire
                                        FROM projets
                                        WHERE id='.$project_id.')
                GROUP BY wl.writer_id,p.proprietaire
                HAVING (rating >= 0 AND rating <='.$this->writers_worse_percent.' AND cnt >= 3) OR ( cnt = 2 AND rating = 0 )
                ORDER BY rating ASC
                LIMIT 5';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[] = $row['writer_id'];
            }
        }
        return $out;
    }

    public function getBestWritersByProjectID($project_id){
        $sql = 'SELECT wl.writer_id AS writer_id,
                       SUM(wl.value)/COUNT(wl.value) AS rating
                FROM writers_likes AS wl
                INNER JOIN projets AS p
                 ON ( wl.project_id = p.id)
                WHERE p.proprietaire = (SELECT proprietaire
                                        FROM projets
                                        WHERE id='.$project_id.')
                GROUP BY wl.writer_id,p.proprietaire
                HAVING rating >='.$this->writers_best_percent;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if ($rows){
            foreach($rows as $row){
                $out[] = $row['writer_id'];
            }
        }
        return $out;
    }


    /*
     * Refactoring 2017
     *
     */

    /*
     *  Get best writers ids subset by users ids
     */
    public function getBestWritersByUsersIds($ids){
        /*
         *  TODO optimize query with TEMPORAY TABLE ?
         */
        $sql = 'SELECT
                     group_concat(best_writers.writer_id) AS writers_ids,
                     best_writers.owner_id AS owner_id
                FROM (
                    SELECT wl.writer_id AS writer_id,
                           SUM(wl.value)/COUNT(wl.value) AS rating,
                           p.proprietaire AS owner_id
                    FROM writers_likes AS wl
                    INNER JOIN projets AS p
                      ON ( wl.project_id = p.id)
                    WHERE p.proprietaire IN ( '.implode(',',$ids).' )
                    GROUP BY wl.writer_id,p.proprietaire
                    HAVING rating >='.$this->writers_best_percent.' ) as best_writers
                 GROUP BY owner_id ';

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if (!empty($rows)){
            foreach($rows as $row){
                $out[$row['owner_id']] = explode(',',$row['writers_ids']);
            }
        }
        return $out;
    }

    /*
     *  Get worse writers ids subset by users ids
     */
    public function getWorseWritersByUsersIds($ids){
        /*
         *  TODO optimize query with TEMPORAY TABLE ?
         */
        $sql = 'SELECT
                     GROUP_CONCAT(worse_writers.writer_id) AS writers_ids,
                     worse_writers.owner_id AS owner_id
                FROM (
                        SELECT wl.writer_id AS writer_id,
                               SUM(wl.value)/COUNT(wl.value) AS rating,
                               COUNT(wl.value) AS cnt,
                               p.proprietaire AS owner_id
                        FROM writers_likes AS wl
                        INNER JOIN projets AS p
                         ON ( wl.project_id = p.id)
                        WHERE p.proprietaire IN ( '.implode(',',$ids).' )
                        GROUP BY wl.writer_id,p.proprietaire
                        HAVING (rating >= 0 AND rating <='.$this->writers_worse_percent.' AND cnt >= 3) OR ( cnt = 2 AND rating = 0 )
                        ORDER BY rating ASC
                        LIMIT 5 ) as worse_writers
                 GROUP BY owner_id ';

        //var_dump($sql);
        //die;

        $result = $this->DB->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);

        $out = array();
        if (!empty($rows)){
            foreach($rows as $row){
                $out[$row['owner_id']] = explode(',',$row['writers_ids']);
            }
        }
        return $out;
    }




    /*
     *  Estimated tasks by webmaster for specified writer
     */
    public function getEstimatedProjectsForWriter($writer_id,$count = false,$like_state = -1,$page=1,$per_page=20,$order_by='asc'){
        $offset = ($page - 1)*$per_page;

        //error_reporting(E_ERROR);

        if ($count){
            $fields = 'COUNT(*) AS cnt';
        }else {
            $fields = 'p.id AS project_id,
                       p.lien AS lien,
                       a.id AS annuaire_id,
                       a.annuaire AS annuaire,
                       wl.`value` AS like_status,
                       wl.webmaster_comment AS webmaster_comment,
                       c.com AS writer_comment,
                       c.created AS writer_comment_created';
        }

        $sql = 'SELECT '.$fields.'
                FROM writers_likes AS wl
                INNER JOIN projets AS p
                 ON ( wl.project_id = p.id)
                INNER JOIN annuaires AS a
                 ON ( wl.annuaire_id = a.id)
                INNER JOIN commentaires AS c
                 ON ( c.idprojet = p.id AND wl.annuaire_id = c.idannuaire)
                WHERE wl.writer_id = '.$writer_id.' AND c.typeuser = 3';


        // like state all/liked/unliked
        if ($like_state != -1){
            $sql.= ' AND wl.`value` = '.$like_state;
        }

        // order
        $sql.= ' ORDER BY wl.clicked_time DESC ';

        // limits
        if (!$count)
            $sql.=' LIMIT '.$offset.','.$per_page;


        //var_dump($sql);

        $result = $this->DB->query($sql);

        if ($count){
            return $result->fetch(PDO::FETCH_COLUMN,0);
        }else{
            $rows = $result->fetchAll(PDO::FETCH_ASSOC);
            return $rows;
        }
    }

} 