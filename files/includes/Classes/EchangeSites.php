<?php

class EchangeSites
{
    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * Partners constructor.
     *
     * @param PDO $dbh
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    /**
     * @param integer $echangeSitesId
     *
     * @return array
     */
    public function getData($echangeSitesId)
    {
        $sql = "SELECT *
                FROM `echange_sites`
                WHERE id = :id";

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':id' => $echangeSitesId,
            ]
        );

        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param string  $domain
     * @param integer $userId
     *
     * @return mixed
     */
    public function isDomainExists($domain, $userId)
    {
        $sql = "SELECT * 
                FROM `echange_sites` 
                WHERE `domain` = :dmn 
                  AND `user_id` = :user_id";

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':dmn' => $domain,
                ':user_id' => $userId,
            ]
        );

        return $sth->fetchAll();
    }

    /**
     * @param string  $domain
     * @param integer $userId
     * @param integer $echangeSitesId
     *
     * @return mixed
     */
    public function isEditDomainExists($domain, $userId, $echangeSitesId)
    {
        $sql = "SELECT * 
                FROM `echange_sites` 
                WHERE `domain` = :dmn 
                  AND `user_id` = :user_id
                  AND `id` != :id";

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':dmn' => $domain,
                ':user_id' => $userId,
                ':id' => $echangeSitesId,
            ]
        );

        return $sth->fetchAll();
    }

    /**
     * @param integer $userId
     *
     * @return mixed
     */
    public function getUserSites($userId)
    {
        $sql = "SELECT * FROM echange_sites WHERE user_id = :user_id";

        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':user_id' => $userId,
            ]
        );

        return $sth->fetchAll();
    }

    /**
     * @param integer $userId
     * @param integer $echangeSitesId
     *
     * @return mixed
     */
    public function getAllSitesByCat($userId, $echangeSitesId)
    {
        $sql = "SELECT * 
                    FROM `echange_sites` 
                    WHERE `user_id` = :user_id 
                      AND `id` != :id";


        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':user_id' => $userId,
                ':id' => $echangeSitesId,
            ]
        );

        return $sth->fetchAll();
    }

    /**
     * @param integer $userId
     * @param integer $echangeSitesId
     *
     * @return bool
     */
    public function isOwner($userId, $echangeSitesId)
    {
        $sql = "SELECT count(*) as cnt 
                    FROM `echange_sites` 
                    WHERE `user_id` = :user_id 
                      AND `id` = :id";


        $sth = $this->dbh->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY));
        $sth->execute(
            [
                ':user_id' => $userId,
                ':id' => $echangeSitesId,
            ]
        );

        $result = (int) $sth->fetchColumn();

        return $result > 0;
    }
}