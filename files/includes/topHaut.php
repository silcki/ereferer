<?php
ini_set('default_charset','iso-8859-1');

if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1'){
    error_reporting(E_ALL);
    //ini_set('errors_display',1);
}


if (isset($_SERVER['HTTP_REFERER'])) {
    ob_start("ob_gzhandler");
}
if (!isset($_SESSION['languageSite'])) {
    $_SESSION['languageSite'] = "fr";
}

if (isset($a)) {
    $_SESSION['a'] = $a;
}
if (isset($b)) {
    $_SESSION['b'] = $b;
}

if (isset($pageTilte) && $pageTilte != "ajout" && $pageTilte != "modif") {
    unset($_SESSION['POSTDOUBLON']);
}

if (preg_match("#compte/#", $_SERVER['REQUEST_URI']) && !isset($_SESSION['connected'])) {
    header("location:./../../logout.html"); die;
}


include("config/configuration.php");


include("functions.php");




include("emailsContent.php");



//error_reporting(E_ALL);

// Refactoring
require_once 'Classes/Backlinks.php';
require_once 'Classes/Comments.php';
require_once 'Classes/Likes.php';
require_once 'Classes/Jobs.php';



require_once 'Classes/RedactionLikes.php';


require_once 'Classes/Projects.php';

require_once 'Classes/RedactionProjects.php';
require_once 'Classes/HTML.php';
require_once 'Classes/User.php';

// "models"
$likesModel = new Likes();

//@require_once('phpmailer/class.phpmailer.php');
//echo $timerBAckup;

//var_dump(strpos($_SERVER['SERVER_NAME'],'boobase.com')!==false); die;


// REDACTION module
$isRedaction = (strpos($_SERVER['REQUEST_URI'],'redaction'));
if ($isRedaction) {
   //$a = -100; // removing others sub-menus
   //var_dump($idUser);
}


$isEchange = (strpos($_SERVER['REQUEST_URI'],'echanges'));



//unset($_SESSION['TIMEOUT']);
if (!isset($_SESSION['TIMEOUT']) || (isset($_SESSION['TIMEOUT']) && $_SESSION['TIMEOUT'] < time())) {
    unset($_SESSION['allTrad']);
    unset($_SESSION['allParameters']);
//    $_SESSION['languageSite'] = "fr";
    $_SESSION['TIMEOUT'] = time() + USERTIMEOUT;
    $_SESSION['allParameters'] = Functions::getAllParameters();

    $styleLogin = 'style="float:right;';
    $stylelogo = "";
    if (isset($_SESSION['connected'])) {
        $stylelogo = 'style="font-size:15px;float:right;"';
        $styleLogin = 'style="float:left;"';

        $devise = Functions::getDevises($_SESSION['connected']['devise']);
        $_SESSION['devise'] = "<i class='icon-" . $devise["libelle"] . "'></i> (" . ucfirst($devise["libelle"]) . ")";
        $_SESSION['deviseSymb'] = "<i class='icon-" . $devise["libelle"] . "'></i>";

        $typeCompte = Functions::getUserType($_SESSION['connected']['typeutilisateur']);
        $_SESSION['typeCompte'] = $typeCompte["libelle"];
        $_SESSION['droit'] = $_SESSION['connected']['typeutilisateur'];

        $_SESSION['tampon']['logoText'] = $_SESSION['allParameters']['logotext']['valeur'];
        $_SESSION['tampon']['logoTop'] = Functions::getLogo();
        $_SESSION['tampon']['logoFooter'] = Functions::getLogo("", "footer");

        if ($_SESSION['connected']['avatar'] != "" && file_exists($_SESSION['avatar'])) {
            $_SESSION['avatar'] = $_SESSION['connected']['avatar'];
        } else {
            $_SESSION['avatar'] = "images/default.png";
        }
    }
}


if (isset($_SESSION['connected'])) {

    $idTypeUser = $_SESSION['connected']['typeutilisateur'];
    $idUser = $_SESSION['connected']['id'];

    // create "User"
    $userModel = new User($idUser);

    if ( !isReferer() ){
        //error_reporting(E_ALL);
        // check contact accepted from DB
        // for saving compability with "Benin  shit code"
        //$contract_accepted = intval($userModel->getParam('contractAccepted',false));

        // quick fix
        if (!$_SESSION['connected']["contractAccepted"]) {
            $_SESSION['connected']["contractAccepted"] = 1;
            $dbh->query('UPDATE utilisateurs SET contractAccepted = "' . $_SESSION['connected']["contractAccepted"] . '" WHERE id=' . $idUser);
        }

        /*
        if ( !$contract_accepted || (($_SESSION['connected']["contractAccepted"] != 1) && $a != 100) ){
            header("location:./contrat.html");
            die;
        }elseif ( $contract_accepted || ($_SESSION['connected']["contractAccepted"] == 1 && $a == 100) ){
            header("location:./accueil.html");
            die;
        }*/

    }else{

        // writer_status
        $is_annuaire_writer = $_SESSION['connected']['annuaire_writer'];
        $is_redaction_writer = $_SESSION['connected']['redaction_writer'];

        $annuaire_contract_accepted = $_SESSION['connected']["contractAccepted"];
        $redaction_contract_accepted = $_SESSION['connected']["redaction_contract_accepted"];

        // attempt to access redaction without accepted contract - redirect to contracts-page
        if ($is_redaction_writer && $isRedaction){

            if (empty($redaction_contract_accepted)){
                header("location:https://".$_SERVER['SERVER_NAME']."/compte/contrat.html"); die;
            }
        }

        // attempt to access basic section without -//-
        if (!$isRedaction && $is_annuaire_writer && !$annuaire_contract_accepted && ($a != 100) ) {
            header("location:./contrat.html");
            die;
        }

    }
    /*
    // access to annuaire section
    if ( ($common_contract_accepted != 1) || ( $is_redaction_writer && !$redaction_contract_accepted ) && $a != 100) {


            // access to redaction section
            if ($is_redaction_writer && ($_SESSION['connected']["redaction_contract_accepted"] == 0) && $isRedaction) {
              header("location:https://".$_SERVER['SERVER_NAME']."/compte/contrat.html"); die;
            }else{

                header("location:./contrat.html");
                die;
            }


    } else {


            if ($_SESSION['connected']["contractAccepted"] == 1 && $a == 100) {
                header("location:./accueil.html");
                die;
       }
    }*/

    $solde = Functions::getSolde();
    $credit = Functions::getCredit();

    if (isSuperReferer()) {
        $_SESSION['typeCompte'] = "Super - " . $_SESSION['typeCompte'];
    }
}

$bonus = 0;
$lienButton = "";
$texteButton = "";
$NomSite = $_SESSION['allParameters']['logotext']["valeur"];
$remunerationDefault = $_SESSION['allParameters']['remuneration']["valeur"];
$URLsiteServer = $_SESSION['allParameters']['url']["valeur"];
$EmailPaypal = $_SESSION['allParameters']['emailpaypal']["valeur"];
$refsecNbre = $_SESSION['allParameters']['refsec']["valeur"];
$limitePaiement = $_SESSION['allParameters']['paiement']["valeur"];
$joursRetards = ($_SESSION['allParameters']['delai']["valeur"]);
$chargeMinimale = $_SESSION['allParameters']['charge']["valeur"];
$ComptePersoWebmaster = floatval($_SESSION['allParameters']['tarifcomptemembreWebmaster']["valeur"]);
$ComptePersoReferenceur = floatval($_SESSION['allParameters']['tarifcomptemembreReferenceur']["valeur"]);
$thisUser = Functions::getUserInfos($idUser);
$phraz = "";
$pagination = pagination;
if (!empty($_SESSION['allParameters']['pagination']["valeur"])) {
    $pagination = $_SESSION['allParameters']['pagination']["valeur"];
}
$pageEnCours = !empty($_GET['page']) ? intval($_GET['page']) : 1;

$delaiReferer = 15;

//$delaiRefererInsert = 0;
$delaiRefererInsert = $delaiReferer;
$jourTime = 3600 * 24;

$defaultHash = "";
$st = 1;
while ($st <= $refsecNbre) {
    $defaultHash .= "0;";
    $st++;
}


$bonus = 0;
$bonusNew = 0;
$dureeJournee = 3600 * 24;

$quotien = 0;
$bonus = 0;
$bonusToAddedCentaine = 0;
$countLastMonth = 0;
$countThisMonth = 0;

if (isReferer()) {
    $number_of_sommisions = 0;
    $number_of_sommisions_found = 0;
    $number_of_sommisions_notfoundyet = 0;
    $number_of_sommisions_notfound = 0;

    $NotYetfoundSoumissions = 0;
    $foundSoumissions = 0;
    $number_of_sommisions_notfoundandnotfoudyet = 0;


    //$start_time = microtime(true);
    $getResUsers = Functions::GetTauxUser($idUser);
    //echo (microtime(true) - $start_time).'<br/>';

    if ($getResUsers) {
        $number_of_sommisions = $getResUsers['total'];
        $number_of_sommisions_found = $getResUsers['found'];
        $number_of_sommisions_notfoundyet = $getResUsers['notfoundyet'];
        $number_of_sommisions_notfound = $getResUsers['notfound'];
        $number_of_sommisions_notfoundandnotfoudyet = $number_of_sommisions_notfoundyet + $number_of_sommisions_notfound;

        $foundSoumissions = Functions::getPourcentage($number_of_sommisions_found, $number_of_sommisions);
        $NotYetfoundSoumissions = Functions::getPourcentage($number_of_sommisions_notfoundyet, $number_of_sommisions);
        $NotfoundSoumissions = round(100 - ($foundSoumissions + $NotYetfoundSoumissions), 1);
    }

    $monthCurrent = date('n');
    $yearCurrent = date('Y');

    if (intval($monthCurrent) == 1) {
        $monthChoosed = 12;
        $yearChoosed = $yearCurrent - 1;
    } else {
        $monthChoosed = $monthCurrent - 1;
        $yearChoosed = $yearCurrent;
    }


    $countLastMonth = Functions::getCountSoumissions($monthChoosed, $yearChoosed, $idUser);
    $countThisMonth = Functions::getCountSoumissions($monthCurrent, $yearCurrent, $idUser);
}

if ($countLastMonth > MinLastMonth && $foundSoumissions > PercentLinkFounded) {
    $bonus = $_SESSION['allParameters']['bonus']["valeur"];
    if ($foundSoumissions > PercentLinkFounded) {
//        $quotien = (((30 - $NotfoundSoumissions ) / 100) * 1.33333333333333);
        $quotien = (intval($foundSoumissions - PercentLinkFounded) * BonusSurLiensTrouves);
//        $quotien += round($quotien, 2);
        $bonus = $bonus + $quotien;
    }
    if ($countThisMonth > ConditionBonusSurCentaineSoumissions && $countThisMonth > MinCurrentMonth) {
//        $bonusToAddedCentaine = round(($countThisMonth * BonusSurCentaineSoumissions * 0.01), 2);
        $bonusToAddedCentaine = (intval($countThisMonth * BonusSurCentaineSoumissions) * 0.01);
        $bonus = $bonus + $bonusToAddedCentaine;
    }
}

//$bonus = 0;
if (isReferer()) {
    $remuneration = Functions::getRemunueration($idUser);
    $bonusNew = ($remunerationDefault + $bonus) - $remuneration;
    if ($bonusNew <= 0) {
        $bonusToAddedCentaine = 0;
        $quotien = 0;
        $bonusNew = 0;
    }
    $phraz = "R�mun�ration";
}

if (isWebmaster()) {
    $remuneration = Functions::getRemunuerationWebmaster($idUser);
    $Affiliation = Functions::getRemunuerationAffiliation($idUser);
    $phraz = "Facturation";
}

$urlBase = $_SESSION['allParameters']['url']["valeur"];

if (!empty($_GET['messageID'])) {
    Functions::ChangeMessageStatut(intval($_GET['messageID']));
}


// full access by default
$redactionAccess = true;
$annuireAccess = true;
$echangeAccess = true;

if (isReferer() && !isSuperReferer()){
    $annuireAccess = ($_SESSION['connected']['annuaire_writer']);
    $redactionAccess = ($_SESSION['connected']['redaction_writer']);

    // access to Redaction-Module denied
    if ($isRedaction && !$redactionAccess){
        header("location:/");die;
    }


    // redirect to Redaction if denied for annuiares-section
    if (!$annuireAccess && $redactionAccess && !$isRedaction){

        // try access hidden sections
        $annuiares_urls = array('plusprojects.html','backlinks.html');
        $request_uri = $_SERVER['REQUEST_URI'];
        foreach($annuiares_urls as $url){
            if (strpos($request_uri, $url) !== FALSE){
                header("location:./../../logout.html");die;
            }
        }

        // start page annuiares - redirect to redaction - main
        if (strpos($request_uri, 'accueil.html') !== FALSE){
            header("location:./redactionlisting/progress.html"); die;
        }
    }
}


/*
// temp block by users
if ( isAdmin() ||  isSu() || (isWebmaster() && ($idUser==19))
    || (isSuperReferer() && ($idUser==206) )
    || (isReferer() && ($idUser==891) && !isSuperReferer()) )
    $redactionAccess = true;
else
    $redactionAccess = false;
*/


// REDACTION menu counts
$RedactionProjects = new RedactionProjects($dbh);


if (isWebmaster() || isSuperReferer() || isSu() || isReferer()){
    $user_id = 0;
    $type = 'admin';
    if (isWebmaster()){
        $user_id = $idUser;
        $type = 'webmaster';
    }

    $redaction_waiting_count = $RedactionProjects->getProjectsCount('waiting',$user_id,$type);
    $redaction_progress_count = $RedactionProjects->getProjectsCount('progress',$user_id,$type);
    $redaction_finished_count = $RedactionProjects->getProjectsCount('finished',$user_id,$type);



    $echanges_sites_count = count(Functions::getSitesList(array('user-id'=>$_SESSION['connected']['id'],'active'=>'0')));



    $echanges_receivedprop_count = count(Functions::getReceivedPropList(array('user-id'=>$_SESSION['connected']['id'],'status'=>'0,200'))) + count(Functions::getReceivedPropList(array('user-id'=>$_SESSION['connected']['id'],'mod_status'=>'1')));

      $echanges_propresult_count = count(Functions::getSentPropList(array('user-id'=>$_SESSION['connected']['id'],'viewed'=>'0')));

    $total_echanges=$echanges_sites_count+$echanges_propresult_count+$echanges_receivedprop_count;



    if(isAdmin() || isSuperReferer() || isSu()){
        $redaction_review_count = $RedactionProjects->getProjectsCount('review',$user_id,$type);
    }

    if (isWebmaster()) {
        $redaction_webmaster_not_viewed_count = $RedactionProjects->getProjectsCount('finished', $user_id, $type, true);
        //var_dump($redaction_webmaster_not_viewed_count);
    }

    $redaction_main_count = 0;
    if (isReferer() && !isSuperReferer()){
        $type = 'writer';
        //$redaction_main_count =  $RedactionProjects->getProjectsCount('progress',$idUser,$type);
        $redaction_modification_count = $RedactionProjects->getProjectsCount('modification',$idUser,$type);
        $redaction_progress_count = $RedactionProjects->getProjectsCount('progress',$idUser,$type);
        //var_dump($redaction_modification_count);
    }



}


$thisPage = Functions::getPageInfos($page);
$breadcumbs = '';
if (isset($_SESSION['connected'])) {
    $breadcumbs = '<div id="breadcrumb"></div>';
}
if ($thisPage['titre'] != "") {
    $breadcumbs = '    
    <div id="breadcrumb"><!-- breadcrumb starts-->
        <div class="container">
            <div class="one-half">
                <h4>' . $thisPage['titre'] . '</h4>
            </div>
            <div class="one-half last">
                <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                    <ul>
                        <li>Vous �tes ici:</li>
                        <li><a href="' . $thisPage['url'] . '">Accueil</a></li>
                        <li>' . $thisPage['titre'] . '</li>
                    </ul>
                </nav><!--breadcrumb nav ends -->
            </div>
        </div>
    </div>';
}

//echo Functions::getAllParameters("", "", $group = "social");
//print_r( $_SESSION['allParameters'] );
//$_SESSION['allTrad'] = Functions::getAllTranslate();
//$_SESSION['allParameters'] = Functions::getAllParameters();
//print_r($_SERVER);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5shiv.js"></script>
<![endif]-->
<html lang="fr">
<head>
    <meta charset="text/html;charset=iSO-8859-1">
    <meta http-equiv="Content-Language" content="fr" />

    <base href="<?php echo $urlBase; ?>"/>
    <title><?php echo $_SESSION['allParameters']['titre']["valeur"]; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="images/favicon.png"/>


    <?php
    // redaction
    $request_uri = isset($_SERVER['REQUEST_URI'])? $_SERVER['REQUEST_URI'] : '';
    $redaction_creating_page = (strpos($request_uri, 'redaction/creating_project.html') !== FALSE);

    if ($redaction_creating_page){ ?>
        <link rel="stylesheet" href="css/bootstrap/bootstrap-dropdown.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap/bootstrap-multiselect.css" type="text/css"/>
    <?php } ?>

    <link rel="stylesheet" href="css/style.css?<?php echo time(); ?>" media="screen"/>
    <link rel="stylesheet" id="main-color" href="css/colors/<?php echo $_SESSION['allParameters']['template']["valeur"]; ?>.css" media="screen"/>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/css.css"/>
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" media="screen"/>
    <link rel="stylesheet" href="css/fontello/fontello.css" media="screen"/>
    <link rel="stylesheet" href="css/jquery.datepick.css" media="screen"/>
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/chosen.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/jquery.modal.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/jquery.tagsinput.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/datatables.css" media="screen" />
    <link rel="stylesheet" href="css/colorbox.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/uploadifive.css">
      <link rel="stylesheet" href="css/rating.css">

    <?php if (isReferer() /* && isset($_GET['block']) && ($_GET['block']=='userstats') */) { ?>
    <link rel="stylesheet" href="css/chartist/chartist.min.css" type="text/css" media="screen" />
    <?php } ?>


    <?php if ($redaction_creating_page) { ?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.1.0/jquery.browser.js"></script>
    <?php }else{?>
        <script type="text/javascript" src="js/jquery.min.js"></script> <!-- version 1.8.3 -->
    <?php } ?>
        <script type="text/javascript" src="js/datatables.js"></script>
        <script type="text/javascript" src="js/datatables.plugins.js"></script>
    <?php
    // redaction
    if ($redaction_creating_page){ ?>
        <script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap-multiselect.js"></script>
    <?php } ?>
    <script type="text/javascript" src="js/jquery.uploadifive.min.js"></script>
    <script type="text/javascript" src="js/jquery.tagsinput.min.js"></script>
    <script type="text/javascript" src="js/jquery.plugin.js"></script>
    <script type="text/javascript" src="js/jquery.datepick.js"></script>
    <script type="text/javascript" src="js/jquery.datepick-fr.js"></script>
    <script type="text/javascript" src="js/navigation.min.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.colorbox.min.js"></script>

    <script type="text/javascript" src="js/rating.js"></script>

    <script type="text/javascript" src="js/jquery.modal.min.js"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

    <!--<script src="js/instantedit.js" type="text/javascript"></script>-->
    <!--<script type="text/javascript" src="js/custom.js?version=<?php echo rand(rand(4, 8), rand(1, 8)); ?>"></script>-->
    <script type="text/javascript" src="js/custom.js?<?php echo time(); ?>"></script>
    <script type="text/javascript" src="js/chosen/chosen.jquery.js"></script>

    <?php if ( (isWebmaster() && ($_GET['section']=='waiting'))
               || ($isRedaction && isset($_GET['block']) && ($_GET['block'] == 'waiting') ) ) {?>
    <script type="text/javascript" src="js/Sortable.min.js"></script>
    <script type="text/javascript" src="js/jquery.binding.js"></script>
    <?php } ?>

    <?php if (  $isRedaction && isset($_GET['block']) &&
             ( ( $_GET['block'] == 'waiting' )  ) ) { ?>
    <script type="text/javascript" src="js/redaction.js"></script>
    <?php } ?>

    <?php if (isReferer() /*&& isset($_GET['block']) && ($_GET['block']=='userstats') */) { ?>
    <script type="text/javascript" src="js/chartist/chartist.min.js"></script>
    <?php } ?>


    <?php
    if ($rechargement && $rechargement == "true") {
        ?>
        <script type="text/javascript">

            //            alert('ok');
        </script>
        <?php
    }
    ?>

</head>
<body>
    <div id="container"><!-- main container starts-->
        <div id="wrapp"><!-- main wrapp starts-->
            <header id="header" style="position: relative;"><!--header starts -->

                <?php if (isset($_SESSION['connected']) && (isset($_SESSION['allParameters']['top_user_message_groups']))) { ?>
                    <?php
                        // message position for different type
                        $is_top_message_simple_writer =  (isReferer() && !isSuperReferer());
                        $is_top_message_super_writer =  (isReferer() && isSuperReferer());

                        // check/show global message by user`s type
                        $user_groups = unserialize($_SESSION['allParameters']['top_user_message_groups']['valeur']);
                        $user_allowed_roles = array_keys($user_groups);

                        $checking_user_type_id = $idTypeUser;

                        // override writer type id by module type
                        if ($is_top_message_simple_writer){
                            $checking_user_type_id = -1;

                            $is_annuaire_writer = $_SESSION['connected']['annuaire_writer'];
                            $is_redaction_writer = $_SESSION['connected']['redaction_writer'];

                            if ($is_annuaire_writer){
                                $checking_user_type_id = 3;
                            }

                            if ($is_redaction_writer){
                                $checking_user_type_id = 5;
                            }
                        }

                        if($is_top_message_super_writer){
                                $checking_user_type_id = 2;
                        }

                        $reflected_role = '';
                        $array_role_reflection = array(
                            //0 => 'admin',
                            2 => 'super_writer',
                            3 => 'annuaire_writer',
                            4 => 'webmaster',
                            5 => 'redaction_writer'
                        );

                        $reflected_role = isset($array_role_reflection[$checking_user_type_id])? $array_role_reflection[$checking_user_type_id] : '';

                        $top_message = $_SESSION['allParameters']['top_user_message']['valeur'];

                        if (!empty($user_groups) && !empty($top_message) && (in_array($reflected_role,$user_allowed_roles))) {
                    ?>
                        <div id="user_groups_top_message"
                            <?php if ($is_top_message_simple_writer) { ?> style="right: 20px;top:15px;"<?php } ?>
                            <?php if ($is_top_message_super_writer)  { ?>  style="right: 2px;top:80px;"<?php } ?>
                            >
                            <div class="header">Nouveau:</div>
                            <div class="message"><?php echo html_entity_decode($top_message,ENT_COMPAT,'ISO-8859-1');?></div>
                        </div>
                        <?php } ?>
                <?php } ?>

                <div class="container">
                    <div id="header-top">
                        <?php
                        if (isset($_SESSION['connected']) && (!isReferer() || (isReferer() && isSuperReferer()))) {
                            ?>

                            <nav class="top-search"><!-- search starts-->
                                <?php
                                    $is_listannuairecontent_page = (strpos($_SERVER['REQUEST_URI'],'listannuairecontent.html')!==false);
                                ?>
                                <form action="compte/recherche.html" method="post" style="margin-top:-30px;<?php if ($is_listannuairecontent_page) { ?>margin-right:-170px;<?php } ?>">
                                    <!--<button class="search-btn"></button>-->
                                    <fieldset style="">
                                        <!--<legend class="color">Connexion</legend>-->
                                        <input name="recherche" class="search-field" type="text" onblur="if (this.value == '')
                                                        this.value = 'Rechercher';" onfocus="if (this.value == 'Rechercher')
                                                                    this.value = '';" value="<?php
                                               if (!empty($_SESSION['rechercheWords'])) {
                                                   // purify search-fieldd
                                                   $topHTML = new HTML;
                                                   $_SESSION['rechercheWords'] = $topHTML->purifyParams($_SESSION['rechercheWords']);
                                                   echo $_SESSION['rechercheWords'];
                                               } else {
                                                   echo 'Rechercher';
                                               }
                                               ?>" style="margin-bottom:5px;"><br/>
                                               <?php
                                               if (isAdmin() || isSu()) {
                                                   ?>
                                            <select name="section" style="padding:2px;width:100%;height:25px;">
                                                <option value="projets" selected="selected">Projets</option>
                                                <option value="utilisateurs">Utilisateurs</option>
                                                <option value="sites">Sites</option>
                                            </select>
                                            <?php
                                        }
                                        ?>
                                        <input type="submit" class="button color small round" value="rechercher" style="color:white;padding:2px;width:100%;margin-top:3px;"/>
                                    </fieldset>
                                </form>
                            </nav>

                            <?php
                        } else {
                            if (!isset($_SESSION['connected'])) {
                                echo '<a href="./"" id="logo" title="' . $_SESSION['allParameters']['logotext']["valeur"] . '"><h1 class="logo">' . $_SESSION['allParameters']['logotext']["valeur"] . '</h1></a>';
                            }
                        }
                        ?>
                        <?php

                        /* Problems ABOVE_ALSO */  ?>
                        <!--your logo-->

                        <div id="header-links" <?php echo $styleLogin; ?> >
                            <?php
                            if (isset($_SESSION['connected'])) {

                                ?>
                                <div id="loginAccount" style="float:left !important;">
                                    <?php /* <img src="<?php echo $_SESSION['avatar']; ?>" id="avatar"/> */ ?>
                                    <div id="avatar_div" style="font-weight:bold;">
                                        <!--<strong>-->
                                        Bienvenue, <span><a nohref=""><?php echo $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom']; ?></a></span><br/>
                                        Compte : <span style="color:blue;"><?php echo $_SESSION['typeCompte']; ?></span><br/>
                                        <?php if (!isAdmin() && !isSu()) { ?>
                                            Solde : <span style="color:green;"><span id="soldeLabeled"><?php echo $solde . "</span> " . $_SESSION['devise']; ?></span><br/>

                                                Cr�dit : <span style="color:green;"><span id="creditLabeled"><?php echo $credit . "</span> - <a href=\"./compte/obtenir_credits.html\">Obtenir des cr�dits</a>";

                                                if($credit>=5){
                                                    echo " | <a href=\"./compte/vendre_credits.html\">Vendre / �changer ses cr�dits</a>";

                                                }
                                                echo"<br/>(<span id=\"what\">Qu'est ce que c'est ?</span>)"?></span><br/>

                                                <script type="text/javascript">
                                                    jQuery("#what").hover(function(){
                                                        jQuery("#what").text("Il s'agit de vos cr�dits disponibles pour l'�change d'articles");
                                                    },function(){
                                                        jQuery("#what").text("Qu'est ce que c'est ?");
                                                    });
                                                </script>
                                                <?php if (!isReferer()) { ?>
                                                    <?php echo $phraz; ?>: <span style="color:green;"><?php echo $remuneration . " " . $_SESSION['deviseSymb']; ?>/soumission</span>
                                                <?php } else { ?>
                                                    <?php
                                                        $is_annuaire_writer = $_SESSION['connected']['annuaire_writer'];
                                                        $is_redaction_writer = $_SESSION['connected']['redaction_writer'];

                                                        $redaction_tarif = $userModel->getRedactionPrice100Words($idUser,'writer');
                                                        $redaction_tarif = number_format($redaction_tarif,2);
                                                    ?>
                                                    <div class="writer_earning">
                                                        <div class="award_title">R�mun�ration: </div>
                                                        <div class="award_types">
                                                            <?php if ($is_annuaire_writer){ ?>
                                                                <span class="annuare_writer"> <?php echo $remuneration . " " . $_SESSION['deviseSymb']; ?>/soumission</span> <?php if ($is_redaction_writer){?><br/><?php } ?>
                                                            <?php } ?>
                                                            <?php if ($is_redaction_writer){ ?>
                                                                <span class="redaction_writer"> <?php echo $redaction_tarif . " " . $_SESSION['deviseSymb']; ?>/100 mots</span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                <?php } ?>


                                                <?php  if (isReferer()){
                                                    if ($is_annuaire_writer) {
                                                        $jobsModel = new Jobs();
                                                        $soumission_current_month = $jobsModel->getCountDoneTaskCurrentMonth($idUser);
                                                        ?>
                                                        Soumissions effectu�es : <span
                                                            style="color:green;"><?php echo $soumission_current_month; ?></span>
                                                        <br/>
                                                    <?php
                                                    }

                                                    if (!isset($_SESSION['connected']['malus_project_count'])) {
                                                        $malus_project_count = Functions::getBacklinkCouplesCount('not_found_yet', 0, $idUser, 0, 1);
                                                        $_SESSION['connected']['malus_project_count'] = $malus_project_count;
                                                    }else{
                                                        $malus_project_count = $_SESSION['connected']['malus_project_count'];
                                                    }
                                                    if ($malus_project_count && $is_annuaire_writer){
                                                    ?>
                                                        Malus : <span style="color:red;">-<?php echo $_SESSION['allParameters']['malus_writer']["valeur"]; ?> $</span><br/>
                                                    <?php } ?>
                                                        <a class="writer_stats_link" href="./compte/userstats.html">Voir les statistiques <span class="sign">!</span></a>
                                                <?php } ?>
                                                <?php if (isWebmaster()) {
                                                    ?>
                                                    <?php if (intval($_SESSION['connected']['lastIP']) == 111111) { ?>
                                                        & <span style="color:orangered;"><?php echo $Affiliation . " " . $_SESSION['deviseSymb']; ?>/affiliation</span><br/>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if (isWebmaster()) { ?>
                                                <?php if (intval($_SESSION['connected']['lastIP']) == 111111) { ?>
            <!--R�mun�ration : <span style="color:green;"><?php echo $Affiliation . " " . $_SESSION['deviseSymb']; ?> /affiliation</span><br/>-->

                                                <?php } ?>
                                            <?php } ?>
                                            <?php   if (isAdmin() || isSu()) { ?>
                                                <br/><span ><i class="icon-power-off"></i> <a href="logout.html" style="color:red;">D�connexion</a></span>
                                            <?php } ?>
                                            <!--</strong>-->
                                            <?php   if (isWebmaster()) { ?>
                                                 <br/><span ><i class="icon-power-off"></i> <a href="logout.html" style="color:red;">D�connexion</a></span>
                                            <?php   } ?>
                                    </div>
                                </div>
                                <?php

                                if (isReferer()) {
                                    // X texts are liked from Y clients & X texts are not liked from Y clients
                                    $total_likes = $likesModel->getStatsForWriter($idUser,1);
                                    $total_unlikes = $likesModel->getStatsForWriter($idUser,0);


                                    $redaction_likes = new RedactionLikes;

                                    $redaction_total_likes = $redaction_likes->getStatsForWriter($idUser,1);
                                    $redaction_total_unlikes = $redaction_likes->getStatsForWriter($idUser,0);

                                    $total_likes['total']+=  $redaction_total_likes['total'];
                                    $total_likes['clients_cnt']+=  $redaction_total_likes['clients_cnt'];

                                    $total_unlikes['total']+=  $redaction_total_unlikes['total'];
                                    $total_unlikes['clients_cnt']+=  $redaction_total_unlikes['clients_cnt'];
                                    ?>
                                    <div id="" style="font-weight:bold;float:left !important;margin-top:-30px;margin-left:-60px;">
                                        <span class="Notes" style="color:green">Liens Trouv�(s)</span> :  <?php echo $foundSoumissions . " % (" . $number_of_sommisions_found . " liens)" ?> <br/>
                                        <span class="Notes" style="color:orange">Liens en attente(s)</span> :  <?php echo $NotYetfoundSoumissions . " % (" . $number_of_sommisions_notfoundyet . " liens)"                                     ?> <br/>
                                        <!--<span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?php // echo Functions::getPourcentage(($number_of_sommisions_notfoundyet + $number_of_sommisions_notfound), $number_of_sommisions) . " % (" . $number_of_sommisions_notfound . " liens)";                                    ?> <br/>-->
                                        <!--<span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?php // echo Functions::getPourcentage($number_of_sommisions_notfound, $number_of_sommisions) . " % (" . $number_of_sommisions_notfound . " liens)";                                         ?> <br/>-->
                                        <span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?php echo $NotfoundSoumissions . " % (" . ($number_of_sommisions_notfound) . " liens)"; ?> <br/>
                                        <?php
//                                            echo $foundSoumissions;

                                        $baseNote = 10;
                                        $baseReference = 100;
                                        $getRating = Functions::getNoteUser($NotfoundSoumissions, $baseReference, $baseNote, 1);
//                                            print_r($getRating);
                                        ?>
                                        <!--<span class="Notes" style="color:blueviolet">Ma Note</span> :  <?php echo $getRating['note'] . "/" . $baseNote; ?> <br/>-->
                                        <?php /*<span class="Notes" style="color:blueviolet">Ma Mention</span> :  <?php echo $getRating['congrats']; ?> <span data-action="tooltip_raison" class="myTooltips">?</span><br/> */ ?>
                                        <span class="Notes" style="color:blueviolet">Bonus liens trouv�s</span> :  <?php echo $quotien; ?> <i class="icon-dollar"></i>  <span data-action="tooltip_raison2" class="myTooltips">?</span><br/>
                                        <span class="Notes" style="color:blueviolet">Bonus soumissions</span> :  <?php echo $bonusToAddedCentaine; ?> <i class="icon-dollar"></i>  <span data-action="tooltip_raison3" class="myTooltips">?</span><br/>

                                        <span class="Notes" ><span style="color:red"><?php echo $total_likes['total'];?></span> textes ont �t� aim�s par <span style="color:red"><?php echo $total_likes['clients_cnt'];?></span> diff�rents clients</span><br/>
                                        <span class="Notes" style=""><span style="color:red"><?php echo $total_unlikes['total'];?></span> textes n'ont pas �t� aim�s par <span style="color:red"><?php echo $total_unlikes['clients_cnt'];?></span> diff�rents clients</span><br/>


                                        <!--</strong>-->
                                    </div>
                                    <div style="display:none;" id="tooltip_raison" class="myTooltips" >
                                        <div class="tooltip_header">Comment est attribu�e la mention:</div>
                                        <div class="tooltip_body">
                                            <p>Votre mention est bas�e sur votre taux de liens non-trouv�s.</p>
                                        </div>
                                    </div>
                                    <div style="display:none;" id="tooltip_raison2" class="myTooltips" >
                                        <div class="tooltip_header">Bonus liens trouv�s:</div>
                                        <div class="tooltip_body">
                                            <p>
                                                Aucun bonus de r�mun�ration si liens trouv�s < <?php echo PercentLinkFounded ?>%.
                                                +<?php echo BonusSurLiensTrouves ?> <i class="icon-dollar"></i> par tanche de 1% au dessus de ce taux.
                                            </p>
                                        </div>
                                    </div>
                                    <div style="display:none;" id="tooltip_raison3" class="myTooltips" >
                                        <div class="tooltip_header">Bonus Quantit�:</div>
                                        <div class="tooltip_body">
                                            <p>
                                                Aucun bonus si soumissions du mois pr�c�dent < <?php echo MinLastMonth ?>.
                                                +<?php echo BonusSurCentaineSoumissions ?> <i class="icon-dollar"></i> par
                                                tranche de 100 (applicable seulement apr�s <?php echo MinCurrentMonth ?> soumissions durant le mois en cours).
                                            </p>
                                        </div>
                                    </div>
                                <?php }
                                ?>
                                <?php
                            } else if (isset($a) && ($a == 1 || $a == 3)) {
                                ?>




                                <nav class="top-search" style=""><!-- search starts-->
                                    <form action="loginscript.php" method="post" style="margin-top:-30px;">
                                        <!--<button class="search-btn"></button>-->
                                        <fieldset style="">
                                            <!--<legend class="color">Connexion</legend>-->
                                            <input name="login-input" class="search-field" type="text" onblur="if (this.value == '')
                                                            this.value = 'Utilisateur/Email';" onfocus="if (this.value == 'Utilisateur/Email')
                                                                        this.value = '';" value="Utilisateur/Email" style="margin-bottom:5px;"><br/>
                                            <input name="password-input" class="search-field" type="password" onblur="if (this.value == '')
                                                            this.value = 'Mot de passe';" onfocus="if (this.value == 'Mot de passe')
                                                                        this.value = '';" value="Mot de passe" style="margin-bottom:5px;"><br/>
                                            <input type="submit" class="button color small round" value="connexion" style="color:white;"/>
                                            &nbsp;&nbsp;<a href="reset.html">Passe oubli� ?</a>
                                        </fieldset>
                                    </form>
                                </nav>


                                <?php
                            } else {
                                ?>

                                <nav class="top-search"><!-- search starts-->
                                    <form action="#" method="post" style="margin-top:10px;">
                                        <br/>
                                        <br/>
                                        <br/>

                                    </form>
                                </nav>
                                <?php
                            }
                            ?>
<!--<h4>Contact: <span><?php echo $_SESSION['allParameters']['telephone1']["valeur"]; ?></span></h4>contact phone number-->
                        </div>
                    </div>
                </div>
                <div id="main-navigation"  <?php if ($is_top_message_super_writer) { ?> style="margin-top: 30px;" <?php } ?> ><!--main navigation wrapper starts -->
                    <div class="container">
                        <ul class="main-menu"><!--main navigation starts -->
                            <?php
                            if (isset($_SESSION['connected'])) {
                                ?>

                                <?php include("files/includes/menuCompte.php"); ?>
                                <li style="" class="<?php
                                if (isset($z) && $z == 8) {
                                    echo "active";
                                }
                                ?>">
                                    <a href="./compte/modifier/profil.html"><i class="icon-user"></i>Mon compte</a>
                                    <ul id="specialCompte">
                                        <?php
                                        if (isWebmaster() || isSu()) {
                                            ?>
                                            <li><a href="./compte/factures.html"><i class="icon-money"></i>Mes factures</a></li>
                                            <li><a href="./compte/transaction.html"><i class="icon-money"></i>Mes transaction</a></li>
                                            <?php
                                        }
                                        ?>

                                        <?php
                                        if (isWebmaster()) {
                                            ?>
                                            <li><a href="./compte/affiliation.html"><i class="icon-money"></i>Affiliation</a></li>
                                            <li><a href="./compte/commissions.html"><i class="icon-money"></i>Commissions</a></li>
                                            <?php
                                        }
                                        ?>

                                        <li><a href="./compte/modifier/profil.html"><i class="icon-user"></i>Modifier Profil</a></li>
                                        <li><a href="./compte/modifier/password.html"><i class="icon-wrench"></i>Modifier Mot de passe</a></li>
                <!--                                        <li><a href="logout.html"><i class="icon-power-off"></i>D�connexion</a></li>-->
                                        <?php
                                        if (isWebmaster()) {
                                            ?>
                                            <li><a href="./compte/notifications.html"><i class="icon-envelope-alt"></i>Notifications par email</a></li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </li>


                                <?php
                                    // show/hide main-menu Redaction-Item
                                    if (isReferer() && ($_SESSION['connected']['redaction_contract_accepted'] == 0)){
                                         //$redactionAccess = false;
                                    }
                                ?>

                                <?php if ($redactionAccess){ ?>

                                   <li class="<?php
                                    if ($isRedaction) {
                                        echo "active";
                                    }
                                    ?>">
                                        <?php
                                            $redaction_section_base = "./compte/redactionpages/about.html";
                                            if (isReferer() || (isSu() || isSuperReferer())){
                                                $redaction_section_base  = "./compte/redactionlisting/progress.html";
                                            }
                                        ?>
                                        <a href="<?php echo  $redaction_section_base;?>" class="service_redaction_btn service_redaction_btn_section" title="Service de r�daction">
                                            Service de r�daction
                                            <span class="mytextwithicon color2_">
                                                <?php if (isWebmaster()) : ?><?php echo $redaction_webmaster_not_viewed_count;?><?php endif;?>
                                                <?php if (isReferer() && !isSuperReferer()) : ?><?php echo $redaction_modification_count ;?><?php endif;?>
                                                <?php if (isSuperReferer() || isSu() || isAdmin()) : ?><?php echo $redaction_review_count;?><?php endif;?>
                                            </span>
                                        </a>
                                    </li>


                                <?php } ?>

                                <?php if ($echangeAccess){ ?>
                                <?php if ( (isWebmaster()) || isAdmin() || isSuperReferer() || isSu() ){ ?>
                                   <li class="<?php
                                    if ($isEchange || $a==2000) {
                                        echo "active";
                                    }
                                    ?>">
                                        <?php /* ECHANGES D'ARTICLES */

                                            $echange_section_base = "./compte/echanges/about.html";
                                            /*
                                            if (isReferer() || (isSu() || isSuperReferer())){
                                                $redaction_section_base  = "./compte/redactionlisting/progress.html";
                                            } */

                                        ?>

                                        <a href="<?php echo  $echange_section_base;?>" class="service_echange_btn service_echange_btn_section" title="Echange d'articles">
                                            Echange d'articles
                                             <?php if(!isSu()){ ?>
                                            <span class="mytextwithicon color2_">
                                               <?=$total_echanges?>

                                            </span>
                                            <? } ?>
                                        </a>
                                    </li>


                                <?php } } ?>

                                <?php if (!isWebmaster()){ ?>
                                    <li class="super_writer_exit_bth" style="float:right;" > <a href="logout.html"  title="D�connexion"><i class="icon-power-off"></i>Quitter</a></li>
                                <?php } ?>
                            <?php
                            } else {
                                ?>

                                <li class="<?php
                                if ($a == 1) {
                                    echo "active";
                                }
                                ?>"> <a href="./" title="Accueil : Inscription">Accueil</a></li>
                                <li class="<?php
                                if ($a == 3) {
                                    echo "active";
                                }
                                ?>"> <a href="inscription.html" title="Inscription">Inscription</a></li>

                                <li class="<?php
                                if ($a == 2) {
                                    echo "active";
                                }
                                ?>"><a href="connexion.html">Connexion</a></li>

                                <?php
                            }
                            ?>
                            <li></li>


                        </ul><!-- main navigation ends-->

                    </div>
                </div><!--main navigation wrapper ends -->
            </header><!-- header ends-->
            <div id="content">
                <?php
                if (($a == 2 || $a == 1 || $a == 10 || $a == 8 || $isRedaction || $isEchange) && isset($_SESSION['connected'])) {
                    ?>
                    <style>
                        #breadcrumb{
                            margin:0px auto 10px auto;
                            padding:0px;
                        }
                        #breadcrumb p{
                            margin-bottom:0px;
                        }

                        #breadcrumb a{
                            width:auto;
                            float:left;
                            margin : 3px;
                            font-size:11px;
                        }
                    </style>
                    <div id="breadcrumb" align="center">

                        <?php
                        // crutch :( - crazy code!
                        $back_href = false;
                        if (strpos($_SERVER['REQUEST_URI'], '/backlinks')) {
                            $a = -1;
                            $back_href = true;
                        }

                        ?>
                        <?php
                        if (isWebmaster() && ($a == 8 || $a == -1)) {
                            ?>
                            <p class="p_menu">
                                <a href="./compte/backlinks.html" class="a_menu round button color <?php echo $back_href; ?> <?php
                                if ($a == -1 && $b == 16) {
                                    echo "active";
                                }
                                ?>">
                                    Liens en attente de validation &nbsp;&nbsp;
                                    <span class="mytextwithicon">
                                        <?php  echo Functions::getBacklinkCouplesCount('not_found_yet', 0, $idUser); ?>
                                    </span>
                                </a>
                            </p>
                        <?php } ?>

                        <?php if (isReferer() && !isset($_GET['typeRef']) && ($a == -1 || $a == 1) && $annuireAccess) { ?>
                            <p class="p_menu">
                                <a href="./compte/backlinks.html" class="a_menu round button color <?php echo $back_href; ?> <?php
                                if ($a == -1 && $b == 16) {
                                    echo "active";
                                }
                                ?>">
                                    Mes plus anciennes soumissions non-trouv�es &nbsp;&nbsp;
                                    <span class="mytextwithicon">
                                        <?php
                                        echo Functions::getBacklinkCouplesCount('not_found_yet', 0, $idUser, $delaiRefererInsert);
                                        ;
                                        ?>
                                    </span>
                                </a>
                            </p>
                        <?php } ?>



                        <?php
                            // checking
                            $projectModel = new Projects($dbh);
                            $top_projects = $projectModel->getTopForWriterChoosing($idUser);
                            $favorite_projects = $projectModel->getProjectsByFavouriteWriter($idUser);

                        ?>


                        <?php if ( !$isRedaction && (isReferer() || isSuperReferer() )&& !isset($_GET['typeRef']) &&
                                  ( (($enCours>0) && (count($favorite_projects) ))
                                  || ( ($enCours==0) && ( count($favorite_projects) || count($top_projects)) ) )) { ?>

                            <?php  if ($annuireAccess) { ?>
                                <p class="p_menu">
                                    <a href="./compte/plusprojects.html" class="a_menu round button color <?php echo $back_href; ?> <?php
                                    if ($b == 28) {
                                        echo "active";
                                    }
                                    ?>">
                                        Plus de projets
                                    </a>
                                </p>
                            <?php } ?>

                        <?php } ?>




                        <?php
                            if ($isRedaction) { /* SOUS MENU REDACTION */
                        ?>
                                <?php if ( isWebmaster()){ ?>
                                    <p class="p_menu"><a href="./compte/redactionpages/about.html" class="a_menu redaction round button  <?php
                                        if ($a == 40 && $b == 1) {
                                            echo "active";
                                        }
                                        ?>">Ce qu'il faut savoir</a></p>
                                    <p class="p_menu"><a href="./compte/redaction/creating_project.html" class="a_menu redaction round button <?php
                                        if ($a == 40 && $b == 2) {
                                            echo "active";
                                        }
                                        ?>">Cr�er un projet</a></p>
                                <?php } ?>

                                 <?php if (isReferer() && ($_SESSION['connected']['redaction_contract_accepted'] == 1 ) && !isSuperReferer()){ ?>

                                    <?php
                                        //var_dump($redaction_modification_count);
                                        //var_dump($redaction_progress_count);
                                    ?>
                                    <?php if ( ($redaction_modification_count == 0) && ($redaction_progress_count==0)){?>
                                        <p class="p_menu"><a href="./compte/redactionplus.html" class="a_menu redaction round button <?php
                                            if ($a == 40 && $b == 21) {
                                                echo "active";
                                            }
                                            ?>">Plus de projets</a></p>
                                    <?php } ?>

                                    <p class="p_menu"><a href="./compte/redactionlisting/modification.html" class="a_menu redaction round button  <?php
                                        if ($a == 40 && $b == 20) {
                                            echo "active";
                                        }
                                        ?>">Textes � corriger <span class="mytextwithicon color2_"><?php echo $redaction_modification_count;?></span></a></p>
                                 <?php } ?>

                                 <?php if ( (isWebmaster()) || isAdmin() || isSu() || isSuperReferer()){ ?>
                                    <p class="p_menu"><a href="./compte/redactionlisting/progress.html" class="a_menu redaction round button <?php
                                        if ($a == 40 && $b == 3) {
                                            echo "active";
                                        }
                                        ?>">Projets en cours <span class="mytextwithicon color2_"><?php echo $redaction_progress_count; ?></span></a></p>
                                    <p class="p_menu"><a href="./compte/redactionlisting/waiting.html" class="a_menu redaction round button  <?php
                                        if ($a == 40 && $b == 4) {
                                            echo "active";
                                        }
                                        ?>">Projets en attente <span class="mytextwithicon color2_"><?php echo $redaction_waiting_count; ?></span></a></p>
                                    <p class="p_menu"><a href="./compte/redactionlisting/finished.html" class="a_menu redaction round button <?php
                                        if ($a == 40 && $b == 5) {
                                            echo "active";
                                        }
                                        if (isWebmaster()){
                                            $redaction_finished_count = $redaction_webmaster_not_viewed_count;
                                        }
                                        ?>">Projets termin�s <span class="mytextwithicon color2_"><?php echo $redaction_finished_count; ?></span></a></p>
                                <?php } ?>

                                <?php if ( isAdmin() || isSu() || isSuperReferer()){ ?>
                                    <p class="p_menu"><a href="./compte/redactionlisting/review.html" class="a_menu redaction round button <?php
                                        if ($a == 40 && $b == 6) {
                                            echo "active";
                                        }
                                        ?>">Textes � contr�ler <span class="mytextwithicon color2_"><?php echo $redaction_review_count;?></span></a></p>
                                <?php  } ?>

                                <?php if (  isSu() || isAdmin() ){ ?>
                                    <p class="p_menu"><a href="./compte/redactionstats.html" class="a_menu redaction round button <?php
                                        if ($a == 40 && $b == 8) {
                                            echo "active";
                                        }
                                        ?>">Statistiques</a></p>
                                <?php  } ?>


                        <?php } ?>

                         <?php
                            if ($isEchange) { /* SOUS MENU ECHANGE */

                        ?>




                                 <?php if ( (isWebmaster()) || isAdmin() || isSuperReferer()){ ?>

                                  <p class="p_menu"><a href="./compte/echanges/about.html" class="a_menu echange round button <?php
                                        if ($a == 1001) {
                                            echo "active";
                                        }
                                        ?>">Ce qu'il faut savoir</a></p>

                                    <p class="p_menu"><a href="./compte/echangesgestion/liste.html" class="a_menu echange round button <?php
                                        if ($a == 1002) {
                                            echo "active";
                                        }
                                        ?>">Gestion de vos sites <span class="mytextwithicon color2_"><?php echo $echanges_sites_count; ?></span></a></p>
                                    <p class="p_menu"><a href="./compte/echangesgestion/recherche.html" class="a_menu echange round button  <?php
                                        if ($a == 1004) {
                                            echo "active";
                                        }
                                        ?>">Trouver des sites</a></p>
                                     <p class="p_menu"><a href="./compte/echangespropositionsrecues/liste.html" class="a_menu echange round button  <?php
                                        if ($a == 1005) {
                                            echo "active";
                                        }
                                        ?>">Propositons re�ues <span class="mytextwithicon color2_"><?php echo $echanges_receivedprop_count; ?></span></a></p>
                                     <p class="p_menu"><a href="./compte/echangesresultatspropositions/liste.html" class="a_menu echange round button  <?php
                                        if ($a == 1006) {
                                            echo "active";
                                        }
                                        ?>">R�sultats de vos propositions <span class="mytextwithicon color2_"><?php echo $echanges_propresult_count; ?></span></a></p>

                                <?php } elseif (isSu()) {?>

                                 <p class="p_menu"><a href="./compte/echangesgestion/liste.html" class="a_menu echange round button <?php
                                        if ($a == 1002) {
                                            echo "active";
                                        }
                                        ?>">Gestion des sites</a></p>
                                 <p class="p_menu"><a href="./compte/echangesgestion/recherche.html" class="a_menu echange round button  <?php
                                        if ($a == 1004) {
                                            echo "active";
                                        }
                                        ?>">Trouver des sites</a></p>
                                 <p class="p_menu"><a href="./compte/echangesresultatspropositions/liste.html" class="a_menu echange round button  <?php
                                        if ($a == 1006) {
                                            echo "active";
                                        }
                                        ?>">Propositions</a></p>

                                <? } ?>



                        <?php } ?>


                        <?php if ($a == 2 || $a == 1 || $a == 3 || $a == 4 || $a == 5 || $a == 10) {

                            ?>

                            <?php if ($_SESSION['droit'] == 1 || $_SESSION['droit'] == 2) { ?>

                                <p class="p_menu"><a href="./compte/accueil.html" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 0) {
                                        echo "active";
                                    }
                                    ?>"> projets en cours &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $enCours; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/accueil.html?section=waiting" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 8) {
                                        echo "active";
                                    }
                                    ?>"> projets en attente&nbsp;&nbsp;<span class="mytextwithicon <?php
                                                     if ($enAttente > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?php echo $enAttente; ?></span></a></p>
                        <!--                                <p class="p_menu"><a href="./compte/accueil.html?section=nostart" class="a_menu round button color <?php
                                if ($a == 1 && $b == 2) {
                                    echo "active";
                                }
                                ?>">  non trait�(s) &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $aValider; ?></span></a></p>-->
                        <!--                <p class="p_menu"><a href="./compte/accueil.html?section=rejected" class="a_menu round button color <?php
                                if ($a == 1 && $b == 16) {
                                    echo "active";
                                }
                                ?>"> Projets Rejet�(s) &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $rejected; ?></span></a></p>-->
                                <p class="p_menu"><a href="./compte/accueil.html?section=over" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 3) {
                                        echo "active";
                                    }
                                    ?>">  projets termin�s &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $terminer; ?></span></a></p>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                <p class="p_menu"><a href="./compte/ajout/listannuaire.html" class="a_menu round button color <?php
                                if ($b == 9) {
                                    echo "active";
                                }
                                ?>"> Cr�er une liste d'annuaire</a></p>-->

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                <p class="p_menu"><a href="./compte/ajout/ajouterannuaire.html" class="a_menu round button color <?php
                                if ($b == 6) {
                                    echo "active";
                                }
                                ?>"> Ajouter un Annuaire</a></p>-->
                                <p class="p_menu">
                                    <a href="./compte/soumissions.html" class="a_menu round button color <?php
                                    if ($b == 5) {
                                        echo "active";
                                    }
                                    ?>"> liste des soumissions</a></p>
                                <p class="p_menu">
                                    <a href="./compte/gererliste.html" class="a_menu round button color <?php
                                    if ($b == 11) {
                                        echo "active";
                                    }
                                    ?>"> G�rer mes listes</a></p>

                                <p class="p_menu">
                                    <a href="./compte/annuaires.html" class="a_menu round button color <?php
                                    if ($b == 7) {
                                        echo "active";
                                    }
                                    ?>"> Tous les annuaires&nbsp;&nbsp;<span class="mytextwithicon"><?php echo $nbreAnnuaires; ?></span></a></p>


                                    <p class="p_menu">
                                        <a href="./compte/tauxvalidation.html" class="a_menu round button color <?php
                                        if ($b == 20) {
                                            echo "active";
                                        }
                                        ?>"> Taux de validation </a>
                                    </p>

                                <?php $back_href = $back_href ? "active" : ''; ?>
                                <p class="p_menu">
                                    <a href="./compte/backlinks.html" class="a_menu round button color <?php echo $back_href; ?>">Backlinks </a>
                                </p>
                            <?php } ?>

                            <?php if (isSuperReferer() && isset($_GET['typeRef'])) { ?>
                                <p class="p_menu"><a href="./compte/superreferenceur.html" class="a_menu round button color <?php
                                    if ($a == 10 && $b == 0) {
                                        echo "active";
                                    }
                                    ?>"> Tous les projets &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $enCours_; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/superreferenceur.html?section=waiting" class="a_menu round button color <?php
                                    if ($a == 10 && $b == 8) {
                                        echo "active";
                                    }
                                    ?>"> projets en attente&nbsp;&nbsp;<span class="mytextwithicon <?php
                                                     if ($enAttente > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?php echo $enAttente; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/superreferenceur.html?section=over" class="a_menu round button color <?php
                                    if ($a == 10 && $b == 3) {
                                        echo "active";
                                    }
                                    ?>">  projets termin�s &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $terminer_; ?></span></a></p>



                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                <p class="p_menu"><a href="./compte/ajout/ajouterannuaire.html" class="a_menu round button color <?php
                                if ($b == 6) {
                                    echo "active";
                                }
                                ?>"> Ajouter un Annuaire</a></p>-->
                                <p class="p_menu"><a href="./compte/soumissions.html" class="a_menu round button color <?php
                                    if ($b == 5) {
                                        echo "active";
                                    }
                                    ?>"> liste des soumissions</a></p>
                                <?php } ?>

                            <?php if ($_SESSION['droit'] == 4) { ?>

                                <p class="p_menu"><a href="./compte/ajout/site.html" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 1) {
                                        echo "active";
                                    }
                                    ?>"> Cr�er un projet</a></p>
                                <p class="p_menu"><a href="./compte/accueil.html" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 0) {
                                        echo "active";
                                    }
                                    ?>"> Projets en cours &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $enCours; ?></span></a></p>

                                <p class="p_menu"><a href="./compte/accueil.html?section=waiting" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 8) {
                                        echo "active";
                                    }
                                    ?>"> Projets en attente&nbsp;&nbsp;<span class="mytextwithicon <?php
                                                     if ($enAttente > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?php echo $enAttente; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/accueil.html?section=nostart" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 2) {
                                        echo "active";
                                    }
                                    ?>"> Projets non D�marr�(s)&nbsp;&nbsp;<span class="mytextwithicon <?php
                                                     if ($creer > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?php echo $creer; ?></span></a></p>
                        <!--                <p class="p_menu"><a href="./compte/accueil.html?section=rejected" class="a_menu round button color <?php
                                if ($a == 1 && $b == 16) {
                                    echo "active";
                                }
                                ?>"> Projets Rejet�(s) &nbsp;&nbsp;<span class="mytextwithicon"><?php echo $rejected; ?></span></a></p>-->
                                <p class="p_menu"><a href="./compte/accueil.html?section=over" class="a_menu round button color <?php
                                    if ($a == 1 && $b == 3) {
                                        echo "active";
                                    }
                                    ?>"> Projets termin�s&nbsp;&nbsp;<span class="mytextwithicon"><?php echo $terminer; ?></span></a></p>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--                                <p class="p_menu"><a href="./compte/ajout/listannuaire.html" class="a_menu round button color <?php
                                if ($b == 9) {
                                    echo "active";
                                }
                                ?>"> Cr�er ma liste d'annuaires</a></p>-->
                                <p class="p_menu"><a href="./compte/gererliste.html" class="a_menu round button color <?php
                                    if ($b == 11) {
                                        echo "active";
                                    }
                                    ?>"> G�rer mes listes d'annuaires</a></p>
                        <!--                <p class="p_menu"><a href="./compte/ajout/ajouterannuaire.html" class="a_menu round button color <?php
                                if ($b == 6) {
                                    echo "active";
                                }
                                ?>"> Ajouter un Annuaire</a></p>-->
                        <!--                <p class="p_menu"><a href="./compte/importer.html" class="a_menu round button color <?php
                                if ($b == 5) {
                                    echo "active";
                                }
                                ?>"> Importer mes annuaires</a></p>-->
                        <!--                <p class="p_menu"><a href="./compte/annuaires.html" class="a_menu round button color <?php
                                if ($b == 7) {
                                    echo "active";
                                }
                                ?>"> Voir mes annuaires&nbsp;&nbsp;<span class="mytextwithicon"><?php echo $nbreAnnuaires; ?></span></a></p>-->
                            <?php } ?>

                        <?php } ?>





                    </div>
                    <div class='cleared'>
                        <br/>
                    </div>
                    <?php
                } else {
                    echo $breadcumbs;
                }

                // MAIN FUCKING PROBLEM

                $requeteSoumissions = $dbh->query("SELECT * FROM soumissionsage ORDER BY id ASC LIMIT 1");


                while ($retour = $requeteSoumissions->fetch(PDO::FETCH_ASSOC)) {
                    $diffDay = intval((time() - $retour['update']) / $jourTime);
                    if ($diffDay > 0) {
                        $UpdateColumns = $dbh->query("UPDATE soumissionsage SET age = age + " . $diffDay . ", `update` = '" . time() . "' ");
                    }
                    break;
                }


                // check|update
                if (isWebmaster()) {
                    $totalComissions = 0;
                    if (Functions::isWebmasterCommission($idUser)) {
                        $requeteCommissionsLibelle = $dbh->query("SELECT id, WebPartenairePrice FROM annuaires WHERE webmasterPartenaire = '" . $idUser . "' AND WebPartenairePrice > 0 ORDER BY id ASC");

                        while ($retourCommissions = $requeteCommissionsLibelle->fetch(PDO::FETCH_ASSOC)) {



                            /*
                            $queryCHeck = $dbh->query("SELECT COUNT(*) FROM comissions WHERE annuaire_id = '" . $retourCommissions['id'] . "' AND webmaster = '" . $idUser . "'")->fetchColumn(0);


                            if ($queryCHeck > 0) {


                                $requeteCommissionsFinalLibeleQuery = "SELECT a2b.date_found AS date_found, "
                                        . "a2b.annuaire_id AS annuaire_id, "
                                        . "a2b.date_checked_first AS date_checked_first, "
                                        . "a2b.site_id AS site_id, "
                                        . "com.site_id AS comsite_id, "
                                        . "com.annuaire_id AS comannuaire_id, "
                                        . "com.amount AS amount "
                                        . "FROM annuaires2backlinks AS a2b "
                                        . "INNER JOIN comissions AS com "
                                        . "ON (com.annuaire_id = '" . $retourCommissions['id'] . "' AND com.webmaster = '" . $idUser . "') "
                                        . "WHERE a2b.annuaire_id = '" . $retourCommissions['id'] . "' AND a2b.site_id <> com.site_id AND a2b.status = 'found' "
                                        . "ORDER BY a2b.date_checked_first ASC";




                            } else {
                                $requeteCommissionsFinalLibeleQuery = "SELECT * FROM annuaires2backlinks WHERE annuaire_id = '" . $retourCommissions['id'] . "' AND status = 'found' ORDER BY annuaire_id ASC";
                            }*/


                            // REFACTORING
                            $requeteCommissionsFinalLibeleQuery = "SELECT *
                                    FROM annuaires2backlinks AS a2b
                                    WHERE a2b.annuaire_id = ".$retourCommissions['id']."
                                    AND a2b.status='found'
                                    AND a2b.site_id NOT IN (
                                        SELECT  site_id
                                        FROM comissions
                                        WHERE webmaster = ".$idUser." AND annuaire_id = ".$retourCommissions['id'].")";


    //                            $requeteCommissionsFinalLibeleQuery = 'SELECT * FROM annuaires2backlinks WHERE annuaire_id = ' . $retourCommissions['id'] . ' AND status = "found"';
                            $requeteCommissionsFinalLibele = $dbh->query($requeteCommissionsFinalLibeleQuery);

                            while ($retourCommissionsFinal = $requeteCommissionsFinalLibele->fetch(PDO::FETCH_ASSOC)) {

                                $dateNoticed = $retourCommissionsFinal['date_found'];
                                if ($dateNoticed == "0000-00-00 00:00:00") {
                                    $dateNoticed = $retourCommissionsFinal['date_checked_first'];
                                    if ($dateNoticed == "0000-00-00 00:00:00") {
                                        $dateNoticed = date("Y-m-d H:i:s");
                                    }
                                }

                                $timeCommissions = strtotime($dateNoticed);
                                $ThisTaskCommission = $retourCommissions['WebPartenairePrice'];

                                $moneyEarned = false;
                                if (!Functions::getDoublon("comissions", "annuaire_id", $retourCommissionsFinal['annuaire_id'], "site_id", $retourCommissionsFinal['site_id'])) {
                                    $moneyEarned = $dbh->query("INSERT IGNORE INTO comissions VALUES('', '" . $idUser . "', '" . $retourCommissionsFinal['annuaire_id'] . "', '" . $retourCommissionsFinal['site_id'] . "', '" . $ThisTaskCommission . "', '" . date('m', $timeCommissions) . "', '" . date('Y', $timeCommissions) . "', '" . $timeCommissions . "')");
                                    if ($moneyEarned) {
                                        $totalComissions += $ThisTaskCommission;
                                    }
                                }


                            }
                        }

                        if ($totalComissions > 0) {
                            $soldeActuel = Functions::getSolde();
                            $newSoldeUpdated = 0;
                            $newSoldeUpdated = $totalComissions + $soldeActuel;
                            $sqlSolde = "UPDATE `utilisateurs` SET `solde` = '" . $newSoldeUpdated . "' WHERE id = '" . $idUser . "'";
                            $dbh->query($sqlSolde);
                            ?>
                            <script>
                                (function ($) {
                                    $('#soldeLabeled').text("<?Php echo $newSoldeUpdated; ?>");
                                })(jQuery);

                            </script>
                            <?php
                        }
//                        echo $totalComissions;
                    }
                }
                //die;
//die;

?>