<?PHP
include("entete.php");

$messages = 0;
$messages = Functions::getNumberMsg($_SESSION['connected']['id']);
$creer = 0;
$enCours = 0;
$enCours_ = 0;
$enAttente = 0;
$aValider = 0;
$terminer = 0;
$terminer_ = 0;
$rejected = 0;
$inscriptions = 0;
$recharges = 0;
$notifs = 0;

$nbreAnnuaires = 0;
$nbreReferenceurs = 0;
$nbreWebmasters = 0;
$nbreAdministrateurs = 0;
$nbreAffilies = 0;
$nbreParrains = 0;
$nbreReferenceursAdmin = 0;


$rechargementEnAttente = 0;
$paiementEnAttente = 0;

if (isset($_SESSION['connected'])) {

    if (isAdmin() || isSU() || (isSuperReferer() && isset($_GET['typeRef']))) {
        $enCours = Functions::getCountProjetByUser(1, 1, 0, 1, 1, 1);
        $enCours_ = $enCours;
        $terminer = Functions::getCountProjetByUser(1, 1, 1);
        $terminer_ = $terminer;
        $aValider = Functions::getCountProjetByUser(1, 1, 0, 0, 1);
        $enAttente = Functions::getCountProjetByUser(1, 1, 0, 1, 1, 0);
        $rejected = Functions::getCountProjetByUser(1, 1, 0, 2, 1, 0);
        $rechargementEnAttente = Functions::getCountRequest(0, 4, 0);

        $paiementEnAttente = count(Functions::getPaiementsRequestsList());

        $nbreAnnuaires = Functions::getNumberAnnuaire(0);

        $inscriptions = Functions::getNewUsers();
        $recharges = Functions::getRecharges();
        if ($inscriptions > 0) {
            $notifs = $inscriptions;
        }
        if ($recharges > 0) {
            $notifs = $recharges;
        }
        if ($notifs == 0) {
            
        }
        $nbreReferenceurs = Functions::getCountUserByType(3);
        $nbreWebmasters = Functions::getCountUserByType(4);
        $nbreAdministrateurs = Functions::getCountUserByType(2);
        $nbreAffilies = Functions::getCountAffiliation();
        $nbreParrains = Functions::getCountParrains();
        $nbreReferenceursAdmin = Functions::getCountRefAdmin();
    }

    if (isWebmaster()) {
  
        $enCours = Functions::getCountProjetByUser($idUser, 4, 0, 1, 1, 1);
        $terminer = Functions::getCountProjetByUser($idUser, 4, 1, 1, 1, 2, 1);
        $enAttente = Functions::getCountProjetByUser($idUser, 4, 0, 1, 1, 0);
        $creer = Functions::getCountProjetByUser($idUser, 4, 0, 0, 0, 0);
        $rejected = Functions::getCountProjetByUser($idUser, 4, 0, 2, 1, 0);
        $nbreAnnuaires = Functions::getNumberAnnuaire($idUser);
    }


    if (isReferer()) {
        $enCours = Functions::getCountProjetByUser($idUser, 3);
        if ($enCours > 0) {
            $pas = true;
        }
        $terminer = Functions::getCountProjetByUser($idUser, 3, 1);
    }


    // check annuires access
    $annuireAccess = true;
    if (isReferer()){
        $annuireAccess = ($_SESSION['connected']['annuaire_writer']);
    }
}
?>


<?PHP
if ($_SESSION['droit'] == 3) {
    $comp = "";
    if (isSuperReferer()) {
        $comp = "?action=mestaches";
    }
    ?>
    <li class="<?PHP

    if ($enCours < 0) {
        $enCours = "...";
    }
    if ($a == 1) {
        echo "active";
    }
    ?>">
        <?php if ($annuireAccess){ ?>
        <a href="./compte/accueil.html<?PHP echo $comp; ?>" title="Tableau de bord"><i class="icon-desktop"></i>Tableau de bord &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $enCours; ?></a></li>
        <?php } ?>
<?PHP } else { ?>
    <li class="<?PHP
    if ($a == 1) {
        echo "active";
    }
    ?>"> 
        <!--<a href="./compte/accueil.html" title="Acceuil"><i class="icon-home"></i>Accueil</a></li>-->
        <a href="./compte/accueil.html" title="Tableau de bord"><i class="icon-desktop"></i>Dashboard &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $enCours; ?></span></a></li>


<?PHP } ?>

<?PHP if (isSuperReferer()) { ?>
    <li class="<?PHP
    if ( ($a == 5) || ($a == 29) ) {
        echo "active";
    }
    ?>"><a href="./compte/superreferenceur.html" title="Super R�f�renceur"><i class="icon-list-alt"></i>Admin</a>
        <ul>
            <li class=""> <a href="./compte/statistiques.html" title="statistiques"><i class="icon-bar-chart"></i>Statistiques</a></li>
            <li class=""> <a href="./compte/backlinks.html" title="backlinks"><i class="icon-bar-chart"></i>Backlinks</a></li>
        </ul>

    </li>


<?PHP } ?>

<li class="<?PHP
if ($a == 3) {
    echo "active";
}
?>"> <a href="./compte/readmessage.html" title="Messagerie"><i class="icon-envelope"></i>Messagerie &nbsp;&nbsp;<span class="mytextwithicon <?PHP
    if ($messages > 0) {
        echo "color2_";
    }
    ?>"><?PHP echo $messages; ?></span></a>
    <ul>
        <li class=""> <a href="./compte/readmessage.html" title="Reception "><i class="icon-envelope-alt"></i>Bo�te de r�ception <span class="mytextwithicon" style="background:#2767a3;color:white;"><?PHP echo $messages; ?></span></a>
        <li class=""> <a href="./compte/writemessage.html" title="R�daction de message"><i class="icon-edit"></i>�crire un message</a>
        <li class=""> <a href="./compte/sendedmessage.html" title="Boite d'envoi"><i class="icon-edit"></i>Messages Envoy�s</a>

    </ul>
</li>




<?PHP if (isSu() || isAdmin()) { ?>

    <li class="<?PHP
    if ($a == 4) {
        echo "active";
    }
    ?>"> <a href="./compte/parametres.html" title="Gestion">Parametrages</a></li>
    <li class="<?PHP
    if ($a == 5) {
        echo "active";
    }
    ?>"> <a href="./compte/utilisateurs.html?section=inscriptions" title="Gestion des utilisateurs"><i class="icon-user-md"></i>Utilisateurs &nbsp;&nbsp; <span class="mytextwithicon <?PHP
        if ($notifs > 0) {
            echo "color2_";
        }
        ?>"><?PHP echo $notifs; ?></span></a>
        <ul>
            <li class=""> <a href="./compte/utilisateurs.html?section=referenceurs" title="R�f�renceurs "><i class="icon-list-alt"></i>R�f�renceurs</a></li>
            <li class=""> <a href="./compte/utilisateurs.html?section=webmasters" title="Webmasters"><i class="icon-list-alt"></i>Webmasters</a>
                <ul>
                    <a href="./compte/utilisateurs.html?section=parrains" title="Parrains"><i class="icon-list-alt"></i>Webmasters Parrains</a>
                    <a href="./compte/utilisateurs.html?section=affiliation" title="Affili�s"><i class="icon-list-alt"></i>Webmasters par affiliation</a>
                </ul>
            </li>
            <li class=""> <a href="./compte/utilisateurs.html?section=administrateurs" title="Administrateurs"><i class="icon-list-alt"></i>Administrateurs</a></li>
            <li class=""> <a href="./compte/statistiques.html" title="statistiques"><i class="icon-bar-chart"></i>Statistiques</a></li>

        </ul>
    </li>

<?PHP } ?>

<?PHP if ($_SESSION['droit'] == 3) { ?>
    <li class="<?PHP
    if ($a == 6) {
        echo "active";
    }
    ?>"> <a href="./compte/demandepaiement.html" title="Recharger"><i class="icon-dollar"></i>Demande de paiement</a></li>

<?PHP } ?>


<?PHP if ($_SESSION['droit'] == 4) { ?>
    <li class="<?PHP
    if ($a == 6) {
        echo "active";
    }
    ?>"> <a href="./compte/recharger.html" title="Recharger"><i class="icon-euro"></i>Recharger</a></li>

    <?PHP if (intval($_SESSION['connected']['lastIP']) == 111111) { ?>
        <li class="<?PHP
        if ($a == 7) {
            echo "active";
        }
        ?>"> <a href="./compte/affiliation.html" title="Affiliation">Affiliation</a></li>
        <?PHP } ?>

    <?PHP if (Functions::isWebmasterCommission($idUser)) { ?>
        <li class="<?PHP
        if ($a == 8) {
            echo "active";
        }
        ?>"> <a href="./compte/commissions.html" title="Commissions">Commissions</a></li>
        <?PHP } ?>
    <?PHP } ?>

<!--<li class="<?PHP
if ($a == 1) {
    echo "active";
}
?>"> <a href="logout.html" title="D�connexion"><i class="icon-power-off"></i>D�connexion</a></li>-->

