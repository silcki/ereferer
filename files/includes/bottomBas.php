
<?PHP
echo '<footer id="footer" style="background:none;heigth:0px;"></footer>';
?>


<div id="copyrights"><!-- copyrights starts-->
    <div class="container">
        <div class="one-half">
            <p>
                &copy; Copyright <?PHP echo date('Y'); ?> 
                <a href="./">
                    <?PHP echo $_SESSION['allParameters']['logotext']["valeur"]; ?>
                </a>.
                &nbsp;&nbsp;&nbsp;
                <a href="mentionslegales.html" target="_blank">
                    Mentions L�gales
                </a> - 
				<a href="cgv.html" target="_blank">
                    C G V
                </a>
            </p>
        </div>
        <div class="one-half last">
            <ul class="social-icons footer"><!-- social links starts-->
            </ul><!-- social links ends-->
        </div>
    </div>
</div><!-- copyrights ends -->
</div><!-- main content starts-->
</div> <!-- main wrapp starts-->
</div><!-- main container ends-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60745471-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
<?PHP
if (isset($_SERVER['HTTP_REFERER'])) {
    ob_end_flush();
}
?>