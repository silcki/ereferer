<?PHP
if (isset($_SERVER['HTTP_REFERER'])) {
    ob_start("ob_gzhandler");
}
if (!isset($_SESSION['languageSite'])) {
    $_SESSION['languageSite'] = "fr";
}
if (isset($a)) {
    $_SESSION['a'] = $a;
}
if (isset($b)) {
    $_SESSION['b'] = $b;
}

if (isset($pageTilte) && $pageTilte != "ajout" && $pageTilte != "modif") {
    unset($_SESSION['POSTDOUBLON']);
}

if (preg_match("#compte/#", $_SERVER['REQUEST_URI']) && !isset($_SESSION['connected'])) {
    header("location:./../../logout.html");
}

//echo $a;
include("./files/backup/db/timer.php");

include("config/configuration.php");
include("functions.php");
include("emailsContent.php");
//@require_once('phpmailer/class.phpmailer.php');
//echo $timerBAckup;

if ((time() - $timerBAckup) >= (60 * 60 * 24)) {
    backup_tables(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME, "*", "./files/backup/db/");
}

//unset($_SESSION['TIMEOUT']);
if (!isset($_SESSION['TIMEOUT']) || (isset($_SESSION['TIMEOUT']) && $_SESSION['TIMEOUT'] < time())) {
    unset($_SESSION['allTrad']);
    unset($_SESSION['allParameters']);
//    $_SESSION['languageSite'] = "fr";
    $_SESSION['TIMEOUT'] = time() + USERTIMEOUT;
    $_SESSION['allParameters'] = Functions::getAllParameters();

    $styleLogin = 'style="float:right;';
    $stylelogo = "";
    if (isset($_SESSION['connected'])) {
        $stylelogo = 'style="font-size:15px;float:right;"';
        $styleLogin = 'style="float:left;"';

        $devise = Functions::getDevises($_SESSION['connected']['devise']);
        $_SESSION['devise'] = "<i class='icon-" . $devise["libelle"] . "'></i> (" . ucfirst($devise["libelle"]) . ")";
        $_SESSION['deviseSymb'] = "<i class='icon-" . $devise["libelle"] . "'></i>";

        $typeCompte = Functions::getUserType($_SESSION['connected']['typeutilisateur']);
        $_SESSION['typeCompte'] = $typeCompte["libelle"];
        $_SESSION['droit'] = $_SESSION['connected']['typeutilisateur'];

        $_SESSION['tampon']['logoText'] = $_SESSION['allParameters']['logotext']['valeur'];
        $_SESSION['tampon']['logoTop'] = Functions::getLogo();
        $_SESSION['tampon']['logoFooter'] = Functions::getLogo("", "footer");

        if ($_SESSION['connected']['avatar'] != "" && file_exists($_SESSION['avatar'])) {
            $_SESSION['avatar'] = $_SESSION['connected']['avatar'];
        } else {
            $_SESSION['avatar'] = "images/default.png";
        }
    }
}

if (isset($_SESSION['connected'])) {

    if ($_SESSION['connected']["contractAccepted"] != 1 && $a != 100) {
        header("location:./contrat.html");
    } else if ($_SESSION['connected']["contractAccepted"] == 1 && $a == 100) {
        header("location:./accueil.html");
    }

    $idTypeUser = $_SESSION['connected']['typeutilisateur'];
    $idUser = $_SESSION['connected']['id'];
    $solde = Functions::getSolde();

    if (isSuperReferer()) {
        $_SESSION['typeCompte'] = "Super - " . $_SESSION['typeCompte'];
    }
}



$bonus = 0;
$lienButton = "";
$texteButton = "";
$NomSite = $_SESSION['allParameters']['logotext']["valeur"];
$remunerationDefault = $_SESSION['allParameters']['remuneration']["valeur"];
$URLsiteServer = $_SESSION['allParameters']['url']["valeur"];
$EmailPaypal = $_SESSION['allParameters']['emailpaypal']["valeur"];
$refsecNbre = $_SESSION['allParameters']['refsec']["valeur"];
$limitePaiement = $_SESSION['allParameters']['paiement']["valeur"];
$joursRetards = ($_SESSION['allParameters']['delai']["valeur"]);
$chargeMinimale = $_SESSION['allParameters']['charge']["valeur"];
$ComptePersoWebmaster = floatval($_SESSION['allParameters']['tarifcomptemembreWebmaster']["valeur"]);
$ComptePersoReferenceur = floatval($_SESSION['allParameters']['tarifcomptemembreReferenceur']["valeur"]);
$thisUser = Functions::getUserInfos($idUser);
$phraz = "";
$pagination = pagination;

$delaiReferer = 15;

//$delaiRefererInsert = 0;
$delaiRefererInsert = $delaiReferer;
$jourTime = 3600 * 24;

$defaultHash = "";
$st = 1;
while ($st <= $refsecNbre) {
    $defaultHash .= "0;";
    $st++;
}




if (isReferer()) {
    $number_of_sommisions = 0;
    $number_of_sommisions_found = 0;
    $number_of_sommisions_notfoundyet = 0;
    $number_of_sommisions_notfound = 0;

    $NotYetfoundSoumissions = 0;
    $foundSoumissions = 0;
    $number_of_sommisions_notfoundandnotfoudyet = 0;

    $getResUsers = Functions::GetTauxUser($idUser);
    if ($getResUsers) {
        $number_of_sommisions = $getResUsers['total'];
        $number_of_sommisions_found = $getResUsers['found'];
        $number_of_sommisions_notfoundyet = $getResUsers['notfoundyet'];
        $number_of_sommisions_notfound = $getResUsers['notfound'];
        $number_of_sommisions_notfoundandnotfoudyet = $number_of_sommisions_notfoundyet + $number_of_sommisions_notfound;

        $foundSoumissions = Functions::getPourcentage($number_of_sommisions_found, $number_of_sommisions);
//                                                   $NotYetfoundSoumissions = Functions::getPourcentage($number_of_sommisions_notfoundyet, $number_of_sommisions); 
        $NotYetfoundSoumissions = Functions::getPourcentage(($number_of_sommisions_notfoundandnotfoudyet), $number_of_sommisions);
        $NotfoundSoumissions = round(100 - ($foundSoumissions), 1);
//                                                echo $number_of_sommisions . ".";
//                                                echo $number_of_sommisions_found . ".";
//                                                echo $number_of_sommisions_notfound;
//                                                echo $number_of_sommisions_notfoundyet;
    }
}
if ($NotfoundSoumissions && $NotfoundSoumissions > 0) {
    $bonus = $_SESSION['allParameters']['bonus']["valeur"];
    $quotien = (((30 - $NotfoundSoumissions ) / 100) * 1.33333333333333);
    $bonus += round($quotien, 2);
}


if (!empty($_SESSION['allParameters']['pagination']["valeur"])) {
    $pagination = $_SESSION['allParameters']['pagination']["valeur"];
}
$dureeJournee = 3600 * 24;

if (isReferer()) {
    $remuneration = Functions::getRemunueration($idUser);
    $bonusNew = ($remunerationDefault + $bonus) - $remuneration;
    if ($bonusNew < 0) {
        $bonusNew = 0;
    }
//    $bonusNew = 0;

//    $remuneration += $bonusNew;

    $phraz = "R�mun�ration";
}
if (isWebmaster()) {
    $remuneration = Functions::getRemunuerationWebmaster($idUser);
    $Affiliation = Functions::getRemunuerationAffiliation($idUser);
    $phraz = "Facturation";
}


if ((!isSu() && !isReferer()) && (strpos($_SERVER['REQUEST_URI'], '/backlinks'))) {
//    header("location:./");
}

$urlBase = $_SESSION['allParameters']['url']["valeur"];

if (isset($_GET['messageID']) && !empty($_GET['messageID'])) {
    Functions::ChangeMessageStatut(intval($_GET['messageID']));
}

$thisPage = Functions::getPageInfos($page);
$breadcumbs = '';
if (isset($_SESSION['connected'])) {
    $breadcumbs = '<div id="breadcrumb"></div>';
}
if ($thisPage['titre'] != "") {
    $breadcumbs = '    
    <div id="breadcrumb"><!-- breadcrumb starts-->
        <div class="container">
            <div class="one-half">
                <h4>' . $thisPage['titre'] . '</h4>
            </div>
            <div class="one-half last">
                <nav id="breadcrumbs"><!--breadcrumb nav starts-->
                    <ul>
                        <li>Vous �tes ici:</li>
                        <li><a href="' . $thisPage['url'] . '">Accueil</a></li>
                        <li>' . $thisPage['titre'] . '</li>
                    </ul>
                </nav><!--breadcrumb nav ends -->
            </div>
        </div>
    </div>';
}

//echo Functions::getAllParameters("", "", $group = "social");
//print_r( $_SESSION['allParameters'] );
//$_SESSION['allTrad'] = Functions::getAllTranslate();
//$_SESSION['allParameters'] = Functions::getAllParameters();
//print_r($_SERVER);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5shiv.js"></script>
<![endif]-->
<head>
    <meta charset="text/html;charset=iSO-8859-1">
    <meta http-equiv="Content-Language" content="fr" />

    <base href="<?PHP echo $urlBase; ?>"/>
    <title><?PHP echo $_SESSION['allParameters']['titre']["valeur"]; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" href="images/favicon.png"/>
    <link rel="stylesheet" href="css/style.css" media="screen"/>
    <link rel="stylesheet" id="main-color" href="css/colors/<?PHP echo $_SESSION['allParameters']['template']["valeur"]; ?>.css" media="screen"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/css.css"/>
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css" media="screen"/>
    <link rel="stylesheet" href="css/fontello/fontello.css" media="screen"/>
    <link rel="stylesheet" href="css/jquery.datepick.css" media="screen"/>
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen" />

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.plugin.js"></script>
    <script type="text/javascript" src="js/jquery.datepick.js"></script>
    <script type="text/javascript" src="js/jquery.datepick-fr.js"></script>
    <script type="text/javascript" src="js/navigation.min.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <!--<script src="js/instantedit.js" type="text/javascript"></script>-->
    <!--<script type="text/javascript" src="js/custom.js?version=<?PHP echo rand(rand(4,8), rand(1,8));?>"></script>-->
    <script type="text/javascript" src="js/custom.js?<?PHP echo time();?>"></script>
    <?PHP
    if ($rechargement && $rechargement == "true") {
        ?>
        <script type="text/javascript">

            //            alert('ok');
        </script>
        <?PHP
    }
    ?>

</head>
<body>

    <div id="container"><!-- main container starts-->
        <div id="wrapp"><!-- main wrapp starts-->
            <header id="header"><!--header starts -->
                <div class="container">
                    <div id="header-top">
                        <?PHP
                        if (isset($_SESSION['connected']) && (!isReferer() || (isReferer() && isSuperReferer()))) {
                            ?>
                            <nav class="top-search"><!-- search starts-->
                                <form action="compte/recherche.html" method="post" style="margin-top:-30px;">
                                    <!--<button class="search-btn"></button>-->
                                    <fieldset style="">
                                        <!--<legend class="color">Connexion</legend>-->
                                        <input name="recherche" class="search-field" type="text" onblur="if (this.value == '')
                                                    this.value = 'Rechercher';" onfocus="if (this.value == 'Rechercher')
                                                                this.value = '';" value="<?PHP
                                               if (!empty($_SESSION['rechercheWords'])) {
                                                   echo $_SESSION['rechercheWords'];
                                               } else {
                                                   echo 'Rechercher';
                                               }
                                               ?>" style="margin-bottom:5px;"><br/>
                                               <?PHP
                                               if (isAdmin() || isSu()) {
                                                   ?>
                                            <select name="section" style="padding:2px;width:100%;height:25px;">
                                                <option value="projets" selected="selected">Projets</option>
                                                <option value="utilisateurs">Utilisateurs</option>
                                            </select>
                                            <?PHP
                                        }
                                        ?>
                                        <input type="submit" class="button color small round" value="rechercher" style="color:white;padding:2px;width:100%;margin-top:3px;"/>
                                    </fieldset>
                                </form>
                            </nav>

                            <?PHP
                        } else {
                            if (!isset($_SESSION['connected'])) {
                                echo '<a href="./"" id="logo" title="' . $_SESSION['allParameters']['logotext']["valeur"] . '"><h1 class="logo">' . $_SESSION['allParameters']['logotext']["valeur"] . '</h1></a>';
                            }
                        }
                        ?>

                        <!--your logo-->

                        <div id="header-links" <?PHP echo $styleLogin; ?> >
                            <?PHP
                            if (isset($_SESSION['connected'])) {
                                ?>
                                <div id="loginAccount" style="float:left !important;">
                                    <img src="<?PHP echo $_SESSION['avatar']; ?>" id="avatar"/>
                                    <div id="avatar_div" style="font-weight:bold;">
                                        <!--<strong>--> 
                                        Bienvenue, <span><a nohref=""><?PHP echo $_SESSION['connected']['nom'] . " " . $_SESSION['connected']['prenom']; ?></a></span><br/>
                                        Compte : <span style="color:blue;"><?PHP echo $_SESSION['typeCompte']; ?></span><br/>
                                        <?PHP if (!isAdmin() && !isSu()) { ?>
                                            Solde : <span style="color:green;"><span id="soldeLabeled"><?PHP echo $solde . "</span> " . $_SESSION['devise']; ?></span><br/>
                                                <?PHP echo $phraz; ?>: <span style="color:green;"><?PHP echo $remuneration . " " . $_SESSION['deviseSymb']; ?>/soumission</span>
                                                <?PHP if (isWebmaster()) { ?>
                                                    <?PHP if (intval($_SESSION['connected']['lastIP']) == 111111) { ?>
                                                        & <span style="color:orangered;"><?PHP echo $Affiliation . " " . $_SESSION['deviseSymb']; ?>/affiliation</span><br/>
                                                    <?PHP } ?>
                                                <?PHP } ?>
                                            <?PHP } ?>
                                            <?PHP if (isWebmaster()) { ?>
                                                <?PHP if (intval($_SESSION['connected']['lastIP']) == 111111) { ?>
            <!--R�mun�ration : <span style="color:green;"><?PHP echo $Affiliation . " " . $_SESSION['deviseSymb']; ?> /affiliation</span><br/>-->

                                                <?PHP } ?>
                                            <?PHP } ?>
                                            <?PHP if (isAdmin() || isSu()) { ?>
                                                <span ><i class="icon-power-off"></i> <a href="logout.html" style="color:red;">D�connexion</a></span>
                                            <?PHP } ?>
                                            <!--</strong>-->
                                    </div>
                                </div>
                                <?PHP
                                if (isReferer()) {
                                    ?>
                                    <div id="" style="font-weight:bold;float:left !important;margin-top:-30px;margin-left:-60px;">
                                        <?PHP ?>
                                        <span class="Notes" style="color:green">Liens Trouv�(s)</span> :  <?PHP echo $foundSoumissions . " % (" . $number_of_sommisions_found . " liens)" ?> <br/>
                                        <!--<span class="Notes" style="color:orange">Liens en attente)</span> :  <?PHP // echo $NotYetfoundSoumissions . " % (" . $number_of_sommisions_notfoundyet . " liens)"                     ?> <br/>-->
                                        <!--<span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?PHP // echo Functions::getPourcentage(($number_of_sommisions_notfoundyet + $number_of_sommisions_notfound), $number_of_sommisions) . " % (" . $number_of_sommisions_notfound . " liens)";                     ?> <br/>-->
                                        <!--<span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?PHP // echo Functions::getPourcentage($number_of_sommisions_notfound, $number_of_sommisions) . " % (" . $number_of_sommisions_notfound . " liens)";                         ?> <br/>-->
                                        <span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?PHP echo $NotfoundSoumissions . " % (" . ($number_of_sommisions_notfoundyet + $number_of_sommisions_notfound) . " liens)"; ?> <br/>
                                        <?PHP
//                                            echo $foundSoumissions;

                                        $baseNote = 10;
                                        $baseReference = 100;

                                        $getRating = Functions::getNoteUser($NotfoundSoumissions, $baseReference, $baseNote, 1);
//                                            print_r($getRating); 
                                        ?>
                                        <!--<span class="Notes" style="color:blueviolet">Ma Note</span> :  <?PHP echo $getRating['note'] . "/" . $baseNote; ?> <br/>-->
                                        <span class="Notes" style="color:blueviolet">Ma Mention</span> :  <?PHP echo $getRating['congrats']; ?> <br/>
                                        <span class="Notes" style="color:blueviolet">Bonus</span> :  <?PHP echo $bonusNew; ?> <i class="icon-dollar"></i> <br/>

                                        <!--</strong>-->
                                    </div>
                                <?PHP }
                                ?>
                                <?PHP
                            } else if (isset($a) && ($a == 1 || $a == 3)) {
                                ?>

                                <nav class="top-search" style=""><!-- search starts-->
                                    <form action="loginscript.php" method="post" style="margin-top:-30px;">
                                        <!--<button class="search-btn"></button>-->
                                        <fieldset style="">
                                            <!--<legend class="color">Connexion</legend>-->
                                            <input name="login-input" class="search-field" type="text" onblur="if (this.value == '')
                                                        this.value = 'Utilisateur/Email';" onfocus="if (this.value == 'Utilisateur/Email')
                                                                    this.value = '';" value="Utilisateur/Email" style="margin-bottom:5px;"><br/>
                                            <input name="password-input" class="search-field" type="password" onblur="if (this.value == '')
                                                        this.value = 'Mot de passe';" onfocus="if (this.value == 'Mot de passe')
                                                                    this.value = '';" value="Mot de passe" style="margin-bottom:5px;"><br/>
                                            <input type="submit" class="button color small round" value="connexion" style="color:white;"/>
                                            &nbsp;&nbsp;<a href="reset.html">Passe oubli� ?</a>
                                        </fieldset>
                                    </form>
                                </nav>


                                <?PHP
                            } else {
                                ?>
                                <nav class="top-search"><!-- search starts-->
                                    <form action="#" method="post" style="margin-top:10px;">
                                        <br/>
                                        <br/>
                                        <br/>

                                    </form>
                                </nav>
                                <?PHP
                            }
                            ?>
<!--<h4>Contact: <span><?PHP echo $_SESSION['allParameters']['telephone1']["valeur"]; ?></span></h4>contact phone number-->
                        </div>
                    </div>
                </div>
                <div id="main-navigation"><!--main navigation wrapper starts -->
                    <div class="container">
                        <ul class="main-menu"><!--main navigation starts -->
                            <?PHP
                            if (isset($_SESSION['connected'])) {
                                ?>

                                <?PHP include("files/includes/menuCompte.php"); ?>
                                <li style="" class="<?PHP
                                if (isset($z) && $z == 8) {
                                    echo "active";
                                }
                                ?>">
                                    <a href="./compte/modifier/profil.html"><i class="icon-user"></i>Mon compte</a>
                                    <ul id="specialCompte">
                                        <?PHP
                                        if (isWebmaster() || isSu()) {
                                            ?>
                                            <li><a href="./compte/factures.html"><i class="icon-money"></i>Mes factures</a></li>
                                            <?PHP
                                        }
                                        ?>
                                        <li><a href="./compte/modifier/profil.html"><i class="icon-user"></i>Modifier Profil</a></li>
                                        <li><a href="./compte/modifier/password.html"><i class="icon-wrench"></i>Modifier Mot de passe</a></li>
                <!--                                        <li><a href="logout.html"><i class="icon-power-off"></i>D�connexion</a></li>-->
                                    </ul>
                                </li>

                                <li class=""> <a href="logout.html" title="D�connexion"><i class="icon-power-off"></i>Quitter</a></li>
                                <?PHP
                            } else {
                                ?>

                                <li class="<?PHP
                                if ($a == 1) {
                                    echo "active";
                                }
                                ?>"> <a href="./" title="Accueil : Inscription">Accueil</a></li>
                                <li class="<?PHP
                                if ($a == 3) {
                                    echo "active";
                                }
                                ?>"> <a href="inscription.html" title="Inscription">Inscription</a></li>

                                <li class="<?PHP
                                if ($a == 2) {
                                    echo "active";
                                }
                                ?>"><a href="connexion.html">Connexion</a></li>

                                <?PHP
                            }
                            ?>
                            <li></li>


                        </ul><!-- main navigation ends-->

                    </div>
                </div><!--main navigation wrapper ends -->
            </header><!-- header ends-->
            <div id="content">
                <?PHP
                if (($a == 2 || $a == 1 || $a == 10 || $a == 8) && isset($_SESSION['connected'])) {
                    ?>
                    <style>
                        #breadcrumb{
                            margin:0px auto 10px auto;
                            padding:0px;
                        }
                        #breadcrumb p{
                            margin-bottom:0px;
                        }

                        #breadcrumb a{
                            width:auto;
                            float:left;
                            margin : 3px;
                            font-size:11px;
                        }
                    </style>
                    <div id="breadcrumb" align="center">

                        <?php
                        // crutch :( - crazy code!
                        $back_href = false;
                        if (strpos($_SERVER['REQUEST_URI'], '/backlinks')) {
                            $a = -1;
                            $back_href = true;
                        }
                        ?>
                        <?PHP
                        if (isWebmaster() && ($a == 8 || $a == -1)) {
                            ?>
                            <p class="p_menu">
                                <a href="./compte/backlinks.html" class="a_menu round button color <?php echo $back_href; ?> <?PHP
                                if ($a == -1 && $b == 16) {
                                    echo "active";
                                }
                                ?>">
                                    Liens en attente de validation &nbsp;&nbsp;
                                    <span class="mytextwithicon">
                                        <?PHP echo Functions::getBacklinkCouplesCount('not_found_yet', 0, $idUser); ?>
                                    </span>
                                </a>
                            </p>
                        <?PHP } ?>
                        <?PHP if (isReferer() && !isset($_GET['typeRef']) && ($a == -1 || $a == 1)) { ?>
                            <p class="p_menu">
                                <a href="./compte/backlinks.html" class="a_menu round button color <?php echo $back_href; ?> <?PHP
                                if ($a == -1 && $b == 16) {
                                    echo "active";
                                }
                                ?>">
                                    Mes plus anciennes soumissions non-trouv�es &nbsp;&nbsp;
                                    <span class="mytextwithicon">
                                        <?PHP
                                        echo Functions::getBacklinkCouplesCount('not_found_yet', 0, $idUser, $delaiRefererInsert);
                                        ;
                                        ?>
                                    </span>
                                </a>
                            </p>
                        <?PHP } ?> 
                        <?PHP if ($a == 2 || $a == 1 || $a == 3 || $a == 4 || $a == 5 || $a == 10) { ?>




                            <?PHP if ($_SESSION['droit'] == 1 || $_SESSION['droit'] == 2) { ?> 

                                <p class="p_menu"><a href="./compte/accueil.html" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 0) {
                                        echo "active";
                                    }
                                    ?>"> projets en cours &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $enCours; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/accueil.html?section=waiting" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 8) {
                                        echo "active";
                                    }
                                    ?>"> projets en attente&nbsp;&nbsp;<span class="mytextwithicon <?PHP
                                                     if ($enAttente > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?PHP echo $enAttente; ?></span></a></p>
                        <!--                                <p class="p_menu"><a href="./compte/accueil.html?section=nostart" class="a_menu round button color <?PHP
                                if ($a == 1 && $b == 2) {
                                    echo "active";
                                }
                                ?>">  non trait�(s) &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $aValider; ?></span></a></p>-->
                        <!--                <p class="p_menu"><a href="./compte/accueil.html?section=rejected" class="a_menu round button color <?PHP
                                if ($a == 1 && $b == 16) {
                                    echo "active";
                                }
                                ?>"> Projets Rejet�(s) &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $rejected; ?></span></a></p>-->
                                <p class="p_menu"><a href="./compte/accueil.html?section=over" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 3) {
                                        echo "active";
                                    }
                                    ?>">  projets termin�s &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $terminer; ?></span></a></p>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                <p class="p_menu"><a href="./compte/ajout/listannuaire.html" class="a_menu round button color <?PHP
                                if ($b == 9) {
                                    echo "active";
                                }
                                ?>"> Cr�er une liste d'annuaire</a></p>-->

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                <p class="p_menu"><a href="./compte/ajout/ajouterannuaire.html" class="a_menu round button color <?PHP
                                if ($b == 6) {
                                    echo "active";
                                }
                                ?>"> Ajouter un Annuaire</a></p>-->
                                <p class="p_menu">
                                    <a href="./compte/soumissions.html" class="a_menu round button color <?PHP
                                    if ($b == 5) {
                                        echo "active";
                                    }
                                    ?>"> liste des soumissions</a></p>
                                <p class="p_menu">
                                    <a href="./compte/gererliste.html" class="a_menu round button color <?PHP
                                    if ($b == 11) {
                                        echo "active";
                                    }
                                    ?>"> G�rer mes listes</a></p>
                                <p class="p_menu">
                                    <a href="./compte/annuaires.html" class="a_menu round button color <?PHP
                                    if ($b == 7) {
                                        echo "active";
                                    }
                                    ?>"> Tous les annuaires&nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $nbreAnnuaires; ?></span></a></p>
                                       <?php $back_href = $back_href ? "active" : ''; ?>
                                <p class="p_menu">
                                    <a href="./compte/backlinks.html" class="a_menu round button color <?php echo $back_href; ?>">Backlinks </a>
                                </p>
                            <?PHP } ?> 

                            <?PHP if (isSuperReferer() && isset($_GET['typeRef'])) { ?>
                                <p class="p_menu"><a href="./compte/superreferenceur.html" class="a_menu round button color <?PHP
                                    if ($a == 10 && $b == 0) {
                                        echo "active";
                                    }
                                    ?>"> Tous les projets &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $enCours_; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/superreferenceur.html?section=waiting" class="a_menu round button color <?PHP
                                    if ($a == 10 && $b == 8) {
                                        echo "active";
                                    }
                                    ?>"> projets en attente&nbsp;&nbsp;<span class="mytextwithicon <?PHP
                                                     if ($enAttente > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?PHP echo $enAttente; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/superreferenceur.html?section=over" class="a_menu round button color <?PHP
                                    if ($a == 10 && $b == 3) {
                                        echo "active";
                                    }
                                    ?>">  projets termin�s &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $terminer_; ?></span></a></p>



                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                                <p class="p_menu"><a href="./compte/ajout/ajouterannuaire.html" class="a_menu round button color <?PHP
                                if ($b == 6) {
                                    echo "active";
                                }
                                ?>"> Ajouter un Annuaire</a></p>-->
                                <p class="p_menu"><a href="./compte/soumissions.html" class="a_menu round button color <?PHP
                                    if ($b == 5) {
                                        echo "active";
                                    }
                                    ?>"> liste des soumissions</a></p>
                                <?PHP } ?> 

                            <?PHP if ($_SESSION['droit'] == 4) { ?> 

                                <p class="p_menu"><a href="./compte/ajout/site.html" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 1) {
                                        echo "active";
                                    }
                                    ?>"> Cr�er un projet</a></p>
                                <p class="p_menu"><a href="./compte/accueil.html" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 0) {
                                        echo "active";
                                    }
                                    ?>"> Projets en cours &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $enCours; ?></span></a></p>

                                <p class="p_menu"><a href="./compte/accueil.html?section=waiting" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 8) {
                                        echo "active";
                                    }
                                    ?>"> Projets en attente&nbsp;&nbsp;<span class="mytextwithicon <?PHP
                                                     if ($enAttente > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?PHP echo $enAttente; ?></span></a></p>
                                <p class="p_menu"><a href="./compte/accueil.html?section=nostart" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 2) {
                                        echo "active";
                                    }
                                    ?>"> Projets non D�marr�(s)&nbsp;&nbsp;<span class="mytextwithicon <?PHP
                                                     if ($creer > 0) {
                                                         echo "color2_";
                                                     }
                                                     ?>"><?PHP echo $creer; ?></span></a></p>
                        <!--                <p class="p_menu"><a href="./compte/accueil.html?section=rejected" class="a_menu round button color <?PHP
                                if ($a == 1 && $b == 16) {
                                    echo "active";
                                }
                                ?>"> Projets Rejet�(s) &nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $rejected; ?></span></a></p>-->
                                <p class="p_menu"><a href="./compte/accueil.html?section=over" class="a_menu round button color <?PHP
                                    if ($a == 1 && $b == 3) {
                                        echo "active";
                                    }
                                    ?>"> Projets termin�s&nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $terminer; ?></span></a></p>


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <!--                                <p class="p_menu"><a href="./compte/ajout/listannuaire.html" class="a_menu round button color <?PHP
                                if ($b == 9) {
                                    echo "active";
                                }
                                ?>"> Cr�er ma liste d'annuaires</a></p>-->
                                <p class="p_menu"><a href="./compte/gererliste.html" class="a_menu round button color <?PHP
                                    if ($b == 11) {
                                        echo "active";
                                    }
                                    ?>"> G�rer mes listes d'annuaires</a></p>
                        <!--                <p class="p_menu"><a href="./compte/ajout/ajouterannuaire.html" class="a_menu round button color <?PHP
                                if ($b == 6) {
                                    echo "active";
                                }
                                ?>"> Ajouter un Annuaire</a></p>-->
                        <!--                <p class="p_menu"><a href="./compte/importer.html" class="a_menu round button color <?PHP
                                if ($b == 5) {
                                    echo "active";
                                }
                                ?>"> Importer mes annuaires</a></p>-->
                        <!--                <p class="p_menu"><a href="./compte/annuaires.html" class="a_menu round button color <?PHP
                                if ($b == 7) {
                                    echo "active";
                                }
                                ?>"> Voir mes annuaires&nbsp;&nbsp;<span class="mytextwithicon"><?PHP echo $nbreAnnuaires; ?></span></a></p>-->
                            <?PHP } ?> 


                        <?PHP } ?> 




                    </div>
                    <div class='cleared'>
                        <br/>
                    </div>
                    <?PHP
                } else {
                    echo $breadcumbs;
                }
                ?>



                <?PHP
                $requeteSoumissions = $dbh->query("SELECT * FROM soumissionsage ORDER BY id ASC LIMIT 1");
                while ($retour = $requeteSoumissions->fetch(PDO::FETCH_ASSOC)) {
                    $diffDay = intval((time() - $retour['update']) / $jourTime);
                    if ($diffDay > 0) {
                        $UpdateColumns = $dbh->query("UPDATE soumissionsage SET age = age + " . $diffDay . ", `update` = '" . time() . "' ");
                    }
                    break;
                }

                if (isWebmaster()) {
                    $totalComissions = 0;
                    if (Functions::isWebmasterCommission($idUser)) {
                        $requeteCommissionsLibelle = $dbh->query("SELECT id, WebPartenairePrice FROM annuaires WHERE webmasterPartenaire = '" . $idUser . "' AND WebPartenairePrice > 0 ORDER BY id ASC");
                        while ($retourCommissions = $requeteCommissionsLibelle->fetch(PDO::FETCH_ASSOC)) {

                            $queryCHeck = $dbh->query("SELECT COUNT(*) FROM comissions WHERE annuaire_id = '" . $retourCommissions['id'] . "' AND webmaster = '" . $idUser . "'")->fetchColumn(0);
//                            echo $queryCHeck;
                            if ($queryCHeck > 0) {
                                $requeteCommissionsFinalLibeleQuery = "SELECT a2b.date_found AS date_found, "
                                        . "a2b.annuaire_id AS annuaire_id, "
                                        . "a2b.date_checked_first AS date_checked_first, "
                                        . "a2b.site_id AS site_id, "
                                        . "com.site_id AS comsite_id, "
                                        . "com.annuaire_id AS comannuaire_id, "
                                        . "com.amount AS amount "
                                        . "FROM annuaires2backlinks AS a2b "
                                        . "INNER JOIN comissions AS com "
                                        . "ON (com.annuaire_id = '" . $retourCommissions['id'] . "' AND com.webmaster = '" . $idUser . "') "
                                        . "WHERE a2b.annuaire_id = '" . $retourCommissions['id'] . "' AND a2b.site_id <> com.site_id AND a2b.status = 'found' "
                                        . "ORDER BY a2b.date_checked_first ASC";
                            } else {
                                $requeteCommissionsFinalLibeleQuery = "SELECT * FROM annuaires2backlinks WHERE annuaire_id = '" . $retourCommissions['id'] . "' AND status = 'found' ORDER BY annuaire_id ASC";
                            }

//                            $requeteCommissionsFinalLibeleQuery = 'SELECT * FROM annuaires2backlinks WHERE annuaire_id = ' . $retourCommissions['id'] . ' AND status = "found"';
                            $requeteCommissionsFinalLibele = $dbh->query($requeteCommissionsFinalLibeleQuery);

                            while ($retourCommissionsFinal = $requeteCommissionsFinalLibele->fetch(PDO::FETCH_ASSOC)) {

                                $dateNoticed = $retourCommissionsFinal['date_found'];
                                if ($dateNoticed == "0000-00-00 00:00:00") {
                                    $dateNoticed = $retourCommissionsFinal['date_checked_first'];
                                    if ($dateNoticed == "0000-00-00 00:00:00") {
                                        $dateNoticed = date("Y-m-d H:i:s");
                                    }
                                }

                                $timeCommissions = strtotime($dateNoticed);
                                $ThisTaskCommission = $retourCommissions['WebPartenairePrice'];

                                $moneyEarned = false;
                                if (!Functions::getDoublon("comissions", "annuaire_id", $retourCommissionsFinal['annuaire_id'], "site_id", $retourCommissionsFinal['site_id'])) {
                                    $moneyEarned = $dbh->query("INSERT IGNORE INTO comissions VALUES('', '" . $idUser . "', '" . $retourCommissionsFinal['annuaire_id'] . "', '" . $retourCommissionsFinal['site_id'] . "', '" . $ThisTaskCommission . "', '" . date('m', $timeCommissions) . "', '" . date('Y', $timeCommissions) . "', '" . $timeCommissions . "')");
                                    if ($moneyEarned) {
                                        $totalComissions += $ThisTaskCommission;
                                    }
                                }
                            }
                        }


                        if ($totalComissions > 0) {
                            $soldeActuel = Functions::getSolde();
                            $newSoldeUpdated = 0;
                            $newSoldeUpdated = $totalComissions + $soldeActuel;
                            $sqlSolde = "UPDATE `utilisateurs` SET `solde` = '" . $newSoldeUpdated . "' WHERE id = '" . $idUser . "'";
                            $dbh->query($sqlSolde);
                            ?>
                            <script>
                                (function ($) {
                                    $('#soldeLabeled').text("<?Php echo $newSoldeUpdated; ?>");
                                })(jQuery);

                            </script>
                            <?PHP
                        }
//                        echo $totalComissions;
                    }
                }
                ?>