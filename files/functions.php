<?php

Class Functions {

    public static function nameCrypt($chaine = '') {
        if ($chaine != "") {
            $chaine = trim($chaine);
            $chaine = strtr($chaine, "�����������������������������������������������������", "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
            $chaine = preg_replace('/([^a-z0-9]+)/i', '-', $chaine);
            $chaine = str_replace("--", "-", $chaine);
            if (substr($chaine, -1) == '-')
                $chaine = substr($chaine, 0, -1);
            return strtolower($chaine);
        }
    }

    public static function getAlexaRank($url = '') {
//        $domain = 'siteduzero.com';
//        $data = file_get_contents("http://www.alexa.com/siteinfo/$domain");
//        preg_match('/<a href="\/siteowners\/certify[^\"]*">([0-9, ]+)<\/a>/i', $data, $results);
//        print_r($results[0]);
        if ($url != "") {
            return '<a href="http://www.alexa.com/siteinfo/' . $url . '" target="_blank"><script type="text/javascript" src="http://xslt.alexa.com/site_stats/js/t/a?url=' . $url . '"></script></a>';
        }
    }

    public static function urlWithoutHash($lienExterne = '', $www = 0) {
        $lienExterne = preg_replace("#https://#", "", $lienExterne);
        $lienExterne = preg_replace("#http://#", "", $lienExterne);
        if ($www > 1) {
            $lienExterne = preg_replace("#www.#", "", $lienExterne);
        }

        return $lienExterne;
    }

//    public static function getAlexaRank($url = '') {
//        $xml = simplexml_load_string(file_get_contents("http://data.alexa.com/data?cli=10&url=" . urlencode($url)));
//        return $xml ? $xml->SD->POPULARITY['TEXT'] : 'None';
//    }

    public static function getCryptePassword($password = '') {
        return sha1(md5($password));
//        return md5($password);
    }

    public static function getGoogleRank($url = '') {
        $url = urlencode($url);
////        require_once("google/google_pagerank.class.php");
//        define('CACHE_DIR', dirname(__FILE__));
//
//        $gpr = new GooglePageRank($url);
//        return $gpr->pagerank;
        if ($url != "") {
            return '<a href="#"><img src="http://images.supportduweb.com/prggl.gif?site=' . $url . '&style=7" alt="PageRank" /></a>';
        }
    }

    public static function timeToSet($timeCheck = 0) {
        $return = "";
        $time = time();
        $timer = strtotime(date("d-m-Y", $time));

        if ($timeCheck < $timer) {
            $rapport = ($time - $timeCheck) / 3600;
            if ($rapport > 16 && $rapport <= 24) {
                $newDate = ($timeCheck + (3600 * 24));
                $return = strtotime(date("d-m-Y", $newDate));
            } else {
                $return = $timer;
            }
        } else {
            $return = $timeCheck;
        }

        return $return;
    }

    public static function checkNameFormat($name) {
// V�rifie si la cha�ne ressemble � une URL
        if (strlen($name) > 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function backup_tables($host, $user, $pass, $name, $tables = '*') {

        $link = mysql_connect($host, $user, $pass);
        mysql_select_db($name, $link);

//get all of the tables
        if ($tables == '*') {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while ($row = mysql_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

//cycle through
        foreach ($tables as $table) {
            $result = mysql_query('SELECT * FROM ' . $table);
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE ' . $table . ';';
            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
            $return.= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysql_fetch_row($result)) {
                    $return.= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return.= '"' . $row[$j] . '"';
                        } else {
                            $return.= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $return.= ',';
                        }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

//save file
        $handle = fopen('db-backup-' . time() . '-' . (md5(implode(',', $tables))) . '.sql', 'w+');
        fwrite($handle, $return);
        fclose($handle);
    }

    public static function checkUrlFormat2($url) {
// V�rifie si la cha�ne ressemble � une URL
        if (preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $url)) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkUrlFormat_($url) {
// V�rifie si la cha�ne ressemble � une URL
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkUrlFormat($URL) {
//        $counturl = strlen($URL);
//        $pos = stripos($URL, "/");
        if ($pos !== false && (substr_count($URL, "/") == 3 || substr_count($URL, "/") == 1)) {
//            $URL = substr($URL, 0, $pos);
        }

        if (!function_exists(filter_var) || !filter_var($URL, FILTER_VALIDATE_URL)) {
            $pattern_1 = "/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.([a-z]{2,6}))(:(\d+))?\/?/i";
            $pattern_2 = "/^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.([a-zA-Z]{2,6}))(:(\d+))?\/?/i";
            $pattern_3 = "/^(([A-Z0-9][A-Z0-9_-]*)+.([a-zA-Z]{2,6}))(:(\d+))?\/?/i";
            if (preg_match($pattern_1, $URL) || preg_match($pattern_2, $URL) || preg_match($pattern_3, $URL)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static function mepd($date) {
        if (intval($date) == 0)
            return $date;

        $tampon = time();
        $diff = $tampon - $date;

        $dateDay = date('d', $date);
        $tamponDay = date('d', $tampon);
        $diffDay = $tamponDay - $dateDay;

        if ($diff < 60 && $diffDay == "�") {
            return 'Il y a ' . $diff . 's';
        } else if ($diff < 600 && $diffDay == 0) {
            return 'Il y a ' . floor($diff / 60) . 'mn et ' . floor($diff % 60) . 's';
        } else if ($diff < 3600 && $diffDay == 0) {
            return 'Il y a ' . floor($diff / 60) . 'mn';
        } else if ($diff < 7200 && $diffDay == 0) {
            return ' Il y a ' . floor($diff / 3600) . 'h et ' . floor(($diff % 3600) / 60) . 'mn';
        } else if ($diff < 24 * 3600 && $diffDay == 0) {
            return 'Aujourd\'hui � ' . date('H\h i', $date) . ' min';
        } else if ($diff < 48 * 3600 && $diffDay == 1) {
            return 'Hier � ' . date('H\h i', $date) . ' min';
        } else {
            return 'Le ' . date('d/m/Y', $date);
        }
    }

    public static function JoursToMois($jours = 0) {
        $jours = abs($jours);

        if ($jours >= 0) {
            $mois = round($jours / 30);
            if ($mois >= 1) {
                $texto = $mois . " mois ";
                $resteJours = 0;
                $resteJours = intval($jours - (30 * $mois));
                if ($resteJours > 0) {
                    $texto .= "et " . $resteJours . " jour(s)";
                }
                return $texto;
            } else {
                if ($jours == 0) {
                    return "1 jour(s)";
                } else {
                    return $jours . " jour(s)";
                }
            }
        }
    }

    public static function mepd__($date) {
        if (intval($date) == 0)
            return $date;

        $tampon = time();
        $diff = $date - $tampon;
        $dateDay = date('d', $date);
        $tamponDay = date('d', $tampon);
        $diffDay = $tamponDay - $dateDay;

        if ($diff < (24 * 3600)) {
            return 'aujourd\'hui ';
        } else {
            return 'le ' . date('d/m/Y', $date);
        }
    }

    public static function checkEmailFormat($email) {
// V�rifie si la cha�ne ressemble � une URL
        if (preg_match('/([\w\-]+\@[\w\-]+\.[\w\-]+)/', $email)) {
            return true;
        } else {
            return false;
        }
    }

    public static function longeur($string = "", $size = 25) {
        $newString = "";
        if (strlen($string) > $size) {
            $newString = substr($string, 0, $size);
            $newString .= "...";
        } else {
            $newString = $string;
        }

        return $newString;
    }

    public static function getCountTaskDone($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT annuaireID FROM jobs WHERE siteID ='" . $id . "'";
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[] = $retour['annuaireID'];
        }
        return $tableau;
    }

    public static function getNumberAnnuaire($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT COUNT(*) AS nbre FROM annuaires WHERE importedBy='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountRequest($id = 0, $typeUser = 3, $answer = 0) {
        global $dbh;

        $tableau = array();
        if ($id != 0) {
            $requete = "SELECT COUNT(*) AS nbre FROM requestpayment WHERE requesterID = '" . $id . "' AND type = '" . $typeUser . "' AND answer = " . $answer;
        } else {
            $requete = "SELECT COUNT(*) AS nbre FROM requestpayment WHERE type = '" . $typeUser . "' AND answer = " . $answer;
        }
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountAffiliation() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE typeutilisateur = 4 AND lastIP > 0 AND lastIP < 111111";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountParrains() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE typeutilisateur = 4 AND lastIP = 111111";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountRefAdmin() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE typeutilisateur = 3 AND lastIP = 111111";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountAffilies($id = 0) {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE typeutilisateur = 4 AND lastIP = " . $id;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountClicks($id = 0, $month = 0, $year = 0) {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM affiliation_click WHERE parrain = '" . $id . "' AND  month = '" . $month . "' AND year = " . $year;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountTotalPaid($id = 0) {
        global $dbh;
        $amount = 0;
        $count = 0;
        $requete = "SELECT * FROM affiliation_affilies WHERE parrain = " . $id;
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $amount += $retour['amount'];
        }
        return intval($amount);
    }

    public static function getCountPaid($id = 0, $month = 0, $year = 0) {
        global $dbh;
        $retourner = array();
        $amount = 0;
        $count = 0;
        $requete = "SELECT * FROM affiliation_affilies WHERE parrain = '" . $id . "' AND  month = '" . $month . "' AND year = " . $year;
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $amount += $retour['amount'];
            $count++;
        }
        $retourner['count'] = intval($count);
        $retourner['paid'] = intval($amount);
        return $retourner;
    }

    public static function getAllcomissions($id = 0, $month = 0, $year = 0) {
        global $dbh;
        $retourner = array();
        $amount = 0;
        $count = 0;
        $requete = "SELECT * FROM comissions WHERE webmaster = '" . $id . "' AND  month = '" . $month . "' AND year = " . $year . " ORDER BY date ASC";
        $execution = $dbh->query($requete);
        $count = 1;

        $tableau = array();
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['site_id'] = $retour['site_id'];
            $tableau[$count]['annuaire_id'] = $retour['annuaire_id'];
            $tableau[$count]['webmaster'] = $retour['webmaster'];
            $tableau[$count]['amount'] = $retour['amount'];
            $tableau[$count]['month'] = $retour['month'];
            $tableau[$count]['year'] = $retour['year'];
            $tableau[$count]['time'] = $retour['date'];
            $count++;
        }

        return $tableau;
    }

    public static function getCountPaidComissions($id = 0, $month = 0, $year = 0) {
        global $dbh;
        $retourner = array();
        $amount = 0;
        $count = 0;
        $requete = "SELECT * FROM comissions WHERE webmaster = '" . $id . "' AND  month = '" . $month . "' AND year = " . $year;
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $amount += $retour['amount'];
            $count++;
        }
        $retourner['count'] = intval($count);
        $retourner['paid'] = floatval($amount);
        return $retourner;
    }

    public static function getCountClicksComissions($id = 0, $month = 0, $year = 0) {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM comissions WHERE webmaster = '" . $id . "' AND  month = '" . $month . "' AND year = " . $year;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountUserByType($typeUser = 3, $active = 0) {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE typeutilisateur = " . $typeUser;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getNewUsers() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM utilisateurs WHERE joinTime > 0 AND active = 0";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getRecharges() {
        global $dbh;
        $requete = "SELECT COUNT(*) AS nbre FROM requestpayment WHERE answer = 0";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['nbre'];
    }

    public static function getCountProjetByUser($id = 0, $isAdmin = 3, $over = 0, $adminApprouve = 1, $envoyer = 1, $affected = 1, $showAdmin = 1) {
        global $dbh;

        $tableau = array();
        if ($isAdmin == 1) {
            if ($affected == 0) {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE affectedTO =0 AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = " . $over;
            } else {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND affectedTO <> 0 AND over = " . $over;
            }
        } else if ($isAdmin == 3) {
            $requete = "SELECT * FROM projets WHERE affectedTO ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = " . $over;
        } else if ($isAdmin == 4) {
            if ($affected == 0) {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE proprietaire ='" . $id . "' AND affectedTO =0 AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = " . $over;
            } else if ($affected == 2) {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE proprietaire ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND showProprio = '" . $showAdmin . "' AND over = " . $over;
            } else {
                $requete = "SELECT COUNT(*) AS nbre FROM projets WHERE proprietaire ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND affectedTO <> 0 AND over = " . $over;
            }
        }
        $execution = $dbh->query($requete);
        if ($isAdmin == 3) {
            $nbrr = 0;

            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                if (strpos($retour['email'], "|")) {
                    list($timePret, $countTask) = explode("|", $retour['email']);
                    if ($countTask > 0 && $timePret <= time() && $retour['adminApprouveTime'] <= time() && Functions::isWebmasterCan($retour['proprietaire'])) {
                        $nbrr++;
                    }
                }
            }
            return $nbrr;
        } else {
            $retour = $execution->fetch();
            if ($retour) {
                return $retour['nbre'];
            } else {
                return 0;
            }
        }
    }

    public static function getAnnuaireType($valeur) {
        if ($valeur == 1) {
            $result = "Annuaires Internes";
        } else if ($valeur == 2) {
            $result = "Annuaires du Client";
        } else {
            $result = "Tous les annuaires";
        }
        return $result;
    }

    public static function getResult($valeur) {
        $result = Functions::getFrequence($valeur);

        if ($result[1] == 0 || $result[1] == "") {
            $result[1] = 1;
        }
        if ($result[0] == 0 || $result[0] == "") {
            $result[0] = 1;
        }
        return $result[0] . " annuaire(s) tous les " . $result[1] . " Jour(s)";
    }

    public static function getFrequence($valeur) {
        $frequenceDATA = explode("x", $valeur);
//        $result[] = $frequenceDATA[0];
//        $result[] = $frequenceDATA[1];
        return $frequenceDATA;
    }

    public static function getResultAdd($lastDateSoumission, $frequence, $lvl = 0) {
        if (intval($frequence) > 0) {
            $int = intval($frequence);
            $result = $lastDateSoumission + ($int * 24 * 3600);
        } else {
            $frequence = substr($frequence, 1, strlen($frequence));
            if ($lvl == $frequence) {
                $result = $lastDateSoumission + (24 * 3600);
            } else {
                $result = $lastDateSoumission;
            }
        }
        return $result;
    }

    public static function getDoublon($table, $champ, $valeur, $champ2 = "", $valeur2 = "", $champ3 = "", $valeur3 = "") {
        global $dbh;

        $tableau = array();
        if ($champ2 != "" && $valeur2 != "" && $champ3 != "" && $valeur3 != "") {
            $requete = "SELECT * FROM " . $table . " WHERE " . $champ . "='" . $valeur . "' AND " . $champ2 . "='" . $valeur2 . "' AND " . $champ3 . "='" . $valeur3 . "'";
        } else if ($champ2 != "" && $valeur2 != "" && $champ3 == "" && $valeur3 == "") {
            $requete = "SELECT * FROM " . $table . " WHERE " . $champ . "='" . $valeur . "' AND " . $champ2 . "='" . $valeur2 . "'";
        } else {
            $requete = "SELECT * FROM " . $table . " WHERE " . $champ . "='" . $valeur . "'";
        }
        $execution = $dbh->query($requete)->fetch();
        if ($execution != false) {
            return $execution;
        } else {
            return false;
        }
    }

    public static function getCategorieName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM categories WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['libelle'];
    }

    public static function getListAnnuaireName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['libelle'];
    }

    public static function getAnnuaireNameById($id) {
        global $dbh;
        global $idUser;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id = '" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['id'] = $retour['id'];
        $tableau['libelle'] = $retour['libelle'];
        $tableau['annuairesList'] = $retour['annuairesList'];
        $tableau['proprietaire'] = $retour['proprietaire'];

        return $tableau;
    }

    public static function getListAnnuaireNameById($id) {
        global $dbh;
        global $idUser;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id='" . $id . "' AND proprietaire =" . $idUser;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['libelle'] = $retour['libelle'];
        $tableau['annuairesList'] = $retour['annuairesList'];
        $tableau['proprietaire'] = $retour['proprietaire'];

        return $tableau;
    }

    public static function getListAnnuaireByUser($user, $withOut = "", $cat = 0, $otherData = "", $ExtraOtherData = "") {
        global $dbh;
        global $idUser;
        $count = 1;
        $peutLister = 0;
        $tableau = array();
        $withOutPage = array();

        if ($cat > 0) {

            $getAnnualiste = Functions::getAnnuaireNameById($cat, 0);

            if ($getAnnualiste != false && $getAnnualiste != "" && preg_match("#;#", $getAnnualiste['annuairesList'])) {
                $withOutPage = explode(";", $getAnnualiste['annuairesList']);
            } else {
                $withOutPage = array();
            }
            $withOutPage = array_filter($withOutPage);

            $label = "page_rank";
            $sens = 0;
            if ($otherData != "") {
                $otherDataEx = explode("-", $otherData);
                if (is_array($otherDataEx)) {
                    $label = $otherDataEx[0];
                    $sens = intval($otherDataEx[1]);
                }
            }


            if (is_array($withOutPage)) {

                foreach ($withOutPage as $anuuu) {
                    $getThisOne = "";
                    if ($ExtraOtherData != '') {
                        $peutLister = 1;
                        $getThisOne = Functions::getOneAnnuaire(intval($anuuu), $withOut);
                    } else {
                        $getThisOne = Functions::getOneAnnuaire(intval($anuuu));
                    }

                    if ($getThisOne && !empty($getThisOne['annuaire'])) {
                        $tableau[$count]['id'] = $getThisOne['id'];
                        $tableau[$count]['annuaire'] = $getThisOne['annuaire'] . "";
                        $tableau[$count]['page_rank'] = $getThisOne['page_rank'];
                        $tableau[$count]['tarifW'] = $getThisOne['tarifW'];
                        $tableau[$count]['tarifR'] = $getThisOne['tarifR'];
                        $tableau[$count]['age'] = intval($getThisOne['age']);
                        $tableau[$count]['ar'] = intval($getThisOne['ar']);
                        $tableau[$count]['cf'] = intval($getThisOne['cf']);
                        $tableau[$count]['tf'] = intval($getThisOne['tf']);
                        $tableau[$count]['cp'] = intval($getThisOne['cp']);
                        $tableau[$count]['tdr'] = intval($getThisOne['tdr']);
                        $tableau[$count]['tb'] = intval($getThisOne['tb']);
                        $tableau[$count]['tv'] = floatval($getThisOne['tv']);
                        $tableau[$count]['tav'] = intval($getThisOne['tav']);
                        $tableau[$count]['created'] = $getThisOne['created'];
                        $count++;
                    }
                }
            }

            function cmppage_rank($a, $b) {
                global $label;
                if ($a["page_rank"] == $b["page_rank"]) {
                    return 0;
                }
                return ($a["page_rank"] < $b["page_rank"]) ? -1 : 1;
            }

            function cmpcp($a, $b) {
                global $label;
                if ($a["cp"] == $b["cp"]) {
                    return 0;
                }
                return ($a["cp"] < $b["cp"]) ? -1 : 1;
            }

            function cmpage($a, $b) {
                global $label;
                if ($a["age"] == $b["age"]) {
                    return 0;
                }
                return ($a["age"] < $b["age"]) ? -1 : 1;
            }

            function cmptf($a, $b) {
                global $label;
                if ($a["tf"] == $b["tf"]) {
                    return 0;
                }
                return ($a["tf"] < $b["tf"]) ? -1 : 1;
            }

            function cmptv($a, $b) {
                global $label;
                if ($a["tv"] == $b["tv"]) {
                    return 0;
                }
                return ($a["tv"] < $b["tv"]) ? -1 : 1;
            }

            function cmptarifW($a, $b) {
                global $label;
                if ($a["tarifW"] == $b["tarifW"]) {
                    return 0;
                }
                return ($a["tarifW"] < $b["tarifW"]) ? -1 : 1;
            }

            function cmptdr($a, $b) {
                global $label;
                if ($a["tdr"] == $b["tdr"]) {
                    return 0;
                }
                return ($a["tdr"] < $b["tdr"]) ? -1 : 1;
            }

            function cmptb($a, $b) {
                global $label;
                if ($a["tb"] == $b["tb"]) {
                    return 0;
                }
                return ($a["tb"] < $b["tb"]) ? -1 : 1;
            }

            usort($tableau, "cmp" . $label);
            if ($sens == 1) {
                $tableau = array_reverse($tableau);
            }
        }

        if ($peutLister == 0) {
            if (trim($withOut) != '') {
                $requete = "SELECT * FROM annuaires WHERE importedBy ='" . $user . "' AND active = 1" . $withOut;
//                echo $requete;
            } else {
                $requete = "SELECT * FROM annuaires WHERE importedBy ='" . $user . "' AND active = 1 ORDER BY page_rank DESC";
            }

            if (($_SERVER['REMOTE_ADDR'] == '127.0.0.1') || ($_SERVER['REMOTE_ADDR'] == '37.214.221.173')) {
                var_dump($requete);
            }
            $execution = $dbh->query($requete);
            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                if (!in_array($retour['id'], $withOutPage)) {
//                    echo "ok";
                    $tableau[$count]['id'] = $retour['id'];
                    $tableau[$count]['annuaire'] = $retour['annuaire'];
                    $tableau[$count]['page_rank'] = $retour['page_rank'];
                    $tableau[$count]['tarifW'] = $retour['tarifW'];
                    $tableau[$count]['tarifR'] = $retour['tarifR'];
                    $tableau[$count]['ar'] = intval($retour['ar']);
                    $tableau[$count]['age'] = intval($retour['age']);
                    $tableau[$count]['cf'] = intval($retour['cf']);
                    $tableau[$count]['tf'] = intval($retour['tf']);
                    $tableau[$count]['cp'] = intval($retour['cp']);
                    $tableau[$count]['tdr'] = intval($retour['tdr']);
                    $tableau[$count]['tb'] = intval($retour['tb']);
                    $tableau[$count]['tv'] = floatval($retour['tv']);
                    $tableau[$count]['tav'] = intval($retour['tav']);
                    $tableau[$count]['created'] = $retour['created'];
                    $count++;
                }
            }
        }

        return $tableau;
    }

    public static function getTotalActiveAnnuaires() {
        global $dbh;

        $sql = "SELECT COUNT(*) FROM annuaires
                WHERE importedBy ='0' AND active = 1";

        $execution = $dbh->query($sql);
        $count = $execution->fetchColumn(0);
        return $count;
    }

    public static function getNotFilteredAnnuaires($filtered_ids, $condition) {
        global $dbh;

        $sql = "SELECT DISTINCT *
				FROM `annuaires` 
				WHERE importedBy ='0' 
					  AND active = 1";

        if (!empty($filtered_ids))
            $sql.= " AND id NOT IN  (" . implode(',', $filtered_ids) . ")";
        if (strpos($condition, 'ORDER BY')) {
            $order_param = end(explode('ORDER BY', $condition));
            $order_by = ' ORDER BY ' . $order_param;
            $sql.=$order_by;
        }
        $items = array();
        $execution = $dbh->query($sql);
        $items = $execution->fetchAll(PDO::FETCH_ASSOC);

        return $items;
    }

    public static function compareFieldWithFilter($compare, $field) {
        $compare['val'] = str_replace("'", "", $compare['val']);
        $op = $compare['op'];
        $filter_value = floatval($compare['val']);

        if ($op == '>=') {
            if (!($field >= $filter_value))
                return true;
        }

        if ($op == '<=') {
            if (!($field <= $filter_value))
                return true;
        }
        return false;
    }

    public static function getOneAnnuaire($id, $Requete = "") {
        global $dbh;
        global $idUser;
        $count = 1;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE id ='" . $id . "' AND active = 1" . $Requete;
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['display'] = $retour['display'];
            $tableau['active'] = $retour['active'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['page_rank'] = $retour['page_rank'];
            $tableau['tarifW'] = $retour['tarifW'];
            $tableau['tarifR'] = $retour['tarifR'];
            $tableau['age'] = intval($retour['age']);
            $tableau['cf'] = intval($retour['cf']);
            $tableau['tf'] = intval($retour['tf']);
            $tableau['cp'] = intval($retour['cp']);
            $tableau['tdr'] = intval($retour['tdr']);
            $tableau['tb'] = intval($retour['tb']);
            $tableau['tv'] = floatval($retour['tv']);
            $tableau['created'] = $retour['created'];
        }

        return $tableau;
    }

    public static function getFactures($id) {
        global $dbh;
        global $idUser;
        $count = 1;

        $tableau = array();
        if (isSu($idUser)) {
            $requete = "SELECT * FROM factures ORDER BY id DESC";
        } else {
            $requete = "SELECT * FROM factures WHERE user ='" . $id . "' ORDER BY id DESC";
        }
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['user'] = $retour['user'];
            $tableau[$count]['amount'] = $retour['amount'];
            $tableau[$count]['tva'] = $retour['tva'];
            $tableau[$count]['file'] = $retour['file'];
            $tableau[$count]['reference_paypal'] = $retour['reference_paypal'];
            $tableau[$count]['reference_ereferer'] = $retour['reference_ereferer'];
            $tableau[$count]['time'] = $retour['time'];
            $count++;
        }

        return $tableau;
    }

    public static function getFacturesByid($id) {
        global $dbh;
        global $idUser;
        $count = 1;

        $tableau = array();
        if (isSu($idUser)) {
            $requete = "SELECT * FROM factures WHERE id ='" . $id . "'";
        } else {
            $requete = "SELECT * FROM factures WHERE id ='" . $id . "' AND user = '" . $idUser . "'";
        }
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['user'] = $retour['user'];
            $tableau['amount'] = $retour['amount'];
            $tableau['tva'] = $retour['tva'];
            $tableau['file'] = $retour['file'];
            $tableau['reference_paypal'] = $retour['reference_paypal'];
            $tableau['reference_ereferer'] = $retour['reference_ereferer'];
            $tableau['time'] = $retour['time'];
        }

        return $tableau;
    }

    public static function getListAnnuaireDataByUser($user) {
        global $dbh;
        global $idUser;
        $count = 1;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE proprietaire ='" . $user . "' ORDER BY id";
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['id']]['id'] = $retour['id'];
            $tableau[$retour['id']]['libelle'] = $retour['libelle'];
            $tableau[$retour['id']]['annuairesList'] = $retour['annuairesList'];
            $tableau[$retour['id']]['annuairesListArray'] = array_filter(explode(";", $retour['annuairesList']));
            $tableau[$retour['id']]['proprietaire'] = $retour['proprietaire'];
            $tableau[$retour['id']]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
//        return !empty($retour['annuaire'])?$retour['annuaire']:"Annuaire Introuvable";
        return !empty($retour['annuaire']) ? $retour['annuaire'] : "";
    }

    public static function getProjetName($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM projets WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['lien'];
    }

    public static function getcontrat($idTypeUser) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM contrats WHERE typeUser='" . $idTypeUser . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['typeUser'] = $retour['typeUser'];
        $tableau['contrat'] = $retour['content'];

        return $tableau;
    }

    public static function getcontratById($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM contrats WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau['typeUser'] = $retour['typeUser'];
        $tableau['contrat'] = $retour['content'];

        return $tableau;
    }

    public static function getfullname($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        if ($retour) {
            return strtoupper($retour['nom']) . " " . $retour['prenom'];
        } else {
            return " ";
        }
    }

    public static function getNumberMsg($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT COUNT(1) AS nbre FROM messages WHERE lu=1 AND receiver='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();

        return $retour['nbre'];
    }

    public static function isWebmasterCan($id = 0, $compareTO = 0) {
        if ($id != 0) {
            $soldeAuteur = Functions::getSolde($id);
            $tarifWebmaster = Functions::getRemunuerationWebmaster($id);
            if ($compareTO > 0) {
                $tarifWebmaster = $compareTO;
            }

            if ($soldeAuteur >= $tarifWebmaster) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function webmasterPossibilities($id = 0) {
        $raportQ = 0;

        if ($id > 0) {
            $compteSolde = Functions::getSolde($id);
            $getFacturation = Functions::getRemunuerationWebmaster($id);

            $raportQ = round(($compteSolde / $getFacturation), 0, PHP_ROUND_HALF_DOWN);
        }

        return $raportQ;
    }

    public static function getSolde($id = 0) {
        global $idUser;
        if ($id == 0) {
            $id = $idUser;
        }
        $result = Functions::getUserInfos($id);
        if ($result) {
            return $result['solde'];
        } else {
            return "ERREUR404";
        }
    }

    public static function getRemunueration($id = 0) {
        global $dbh;
        $remu = 0;
        if ($id > 0) {
            $remu = Functions::getRemunuerationThisUser($id);
        }
        if ($remu > 0) {
            return $remu;
        } else {
            $requete = "SELECT valeur FROM parametres WHERE id='20'";
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            return $retour['valeur'];
        }
    }

    public static function getRemunuerationWebmaster($id = 0) {
        global $dbh;
        $remu = 0;
        if ($id > 0) {
            $remu = Functions::getRemunuerationThisUser($id);
        }
        if ($remu > 0) {
            return $remu;
        } else {
            $requete = "SELECT valeur FROM parametres WHERE id='26'";
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            return $retour['valeur'];
        }
    }

    public static function getRemunuerationAffiliation($id = 0) {
        global $dbh;
        $remu = 0;
        if ($id > 0) {
            $remu = Functions::getRemunuerationThisUserAffiliation($id);
        }
        if ($remu > 0) {
            return $remu;
        } else {
            $requete = "SELECT valeur FROM parametres WHERE id='30'";
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            return $retour['valeur'];
        }
    }

    public static function getRemunuerationThisUserAffiliation($id) {
        global $dbh;
        $requete = "SELECT styleAdmin FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['styleAdmin'];
    }

    public static function getRemunuerationThisUser($id) {
        global $dbh;
        $requete = "SELECT frais FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        return $retour['frais'];
    }

    public static function getPaiementByIdUser($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE requesterID = " . $dbh->quote($id) . " AND answer = 0 ORDER BY time DESC LIMIT 1";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['type'] = $retour['type'];
            $tableau['requesterID'] = $retour['requesterID'];
            $tableau['requestAmount'] = $retour['requestAmount'];
            $tableau['answer'] = $retour['answer'];
            $tableau['answerTime'] = $retour['answerTime'];
            $tableau['time'] = $retour['time'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getPaiementById($isAdmin = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE id = '" . $isAdmin . "' AND answer = 0 ORDER BY time DESC LIMIT 1";


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['type'] = $retour['type'];
            $tableau['requesterID'] = $retour['requesterID'];
            $tableau['requestAmount'] = $retour['requestAmount'];
            $tableau['tva'] = $retour['tva'];
            $tableau['answer'] = $retour['answer'];
            $tableau['answerTime'] = $retour['answerTime'];
            $tableau['time'] = $retour['time'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getPaiementsList($isAdmin = 3) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE type = '" . $isAdmin . "' AND answer = 0 ORDER BY time ASC";
        if ($isAdmin == 1) {
            $requete = "SELECT * FROM requestpayment WHERE answer = 1 ORDER BY answerTime DESC, type ASC";
        }


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['type'] = $retour['type'];
            $tableau[$count]['requesterID'] = $retour['requesterID'];
            $tableau[$count]['requestAmount'] = $retour['requestAmount'];
            $tableau[$count]['answer'] = $retour['answer'];
            $tableau[$count]['answerTime'] = $retour['answerTime'];
            $tableau[$count]['time'] = $retour['time'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getPaiementsListMe() {
        global $dbh;
        global $idUser;

        $tableau = array();
        $requete = "SELECT * FROM requestpayment WHERE requesterID = '" . $idUser . "' AND answer = 0 ORDER BY time ASC";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['type'] = $retour['type'];
            $tableau[$count]['requesterID'] = $retour['requesterID'];
            $tableau[$count]['requestAmount'] = $retour['requestAmount'];
            $tableau[$count]['answer'] = $retour['answer'];
            $tableau[$count]['answerTime'] = $retour['answerTime'];
            $tableau[$count]['time'] = $retour['time'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAllUsersInfos($id = 0, $isAdmin = 0, $by = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '" . $isAdmin . "' ORDER BY active ASC, nom ASC, prenom ASC";
        if ($isAdmin == 5) {
            $requete = "SELECT * FROM utilisateurs WHERE joinTime > 0 AND active = 0 ORDER BY id DESC";
        }
        if ($isAdmin == 7) {
            if ($by > 0) {
                $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '4' AND lastIP = '" . $by . "' ORDER BY active ASC, id DESC, nom ASC, prenom ASC";
            } else {
                $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '4' AND lastIP > 0 AND lastIP <> 111111 ORDER BY active ASC, id DESC, nom ASC, prenom ASC";
            }
        }
        if ($isAdmin == 9) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '3' AND lastIP = 111111 ORDER BY active DESC, id DESC, nom ASC, prenom ASC";
        }

        if ($isAdmin == 8) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '4' AND lastIP = 111111 ORDER BY active ASC, id DESC, nom ASC, prenom ASC";
        }


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['nom'] = $retour['nom'];
            $tableau[$count]['prenom'] = $retour['prenom'];
            $tableau[$count]['email'] = $retour['email'];
            $tableau[$count]['ville'] = $retour['ville'];
            $tableau[$count]['codepostal'] = $retour['codepostal'];
            $tableau[$count]['avatar'] = $retour['avatar'];
            $tableau[$count]['societe'] = $retour['societe'];
            $tableau[$count]['adresse'] = $retour['adresse'];
            $tableau[$count]['siteweb'] = $retour['siteweb'];
            $tableau[$count]['lastIP'] = $retour['lastIP'];
            $tableau[$count]['styleAdmin'] = $retour['styleAdmin'];
            $tableau[$count]['telephone'] = $retour['telephone'];
            $tableau[$count]['devise'] = $retour['devise'];
            $tableau[$count]['solde'] = $retour['solde'];
            $tableau[$count]['frais'] = $retour['frais'];
            $tableau[$count]['typeutilisateur'] = $retour['typeutilisateur'];
            $tableau[$count]['active'] = $retour['active'];
            $tableau[$count]['connected'] = $retour['connected'];
            $tableau[$count]['lastlogin'] = $retour['lastlogin'];
            $tableau[$count]['joinTime'] = $retour['joinTime'];
            $tableau[$count]['lastPayment'] = $retour['lastPayment'];
            $tableau[$count]['AmountLastPayment'] = $retour['AmountLastPayment'];
            $count++;
        }

        return $tableau;
    }

    public static function isEmailExist($email = "") {
        global $dbh;

        $requete = "SELECT id FROM utilisateurs WHERE email='" . htmlentities($email, ENT_QUOTES, "UTF8") . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        if (isset($retour['id'])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUserInfos($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM utilisateurs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['nom'] = $retour['nom'];
            $tableau['prenom'] = $retour['prenom'];
            $tableau['email'] = $retour['email'];
            $tableau['avatar'] = $retour['avatar'];
            $tableau['ville'] = $retour['ville'];
            $tableau['codepostal'] = $retour['codepostal'];
            $tableau['societe'] = $retour['societe'];
            $tableau['adresse'] = $retour['adresse'];
            $tableau['siteweb'] = $retour['siteweb'];
            $tableau['styleAdmin'] = $retour['styleAdmin'];
            $tableau['lastIP'] = $retour['lastIP'];
            $tableau['telephone'] = $retour['telephone'];
            $tableau['devise'] = $retour['devise'];
            $tableau['solde'] = $retour['solde'];
            $tableau['frais'] = $retour['frais'];
            $tableau['tentatives'] = $retour['tentatives'];
            $tableau['typeutilisateur'] = $retour['typeutilisateur'];
            $tableau['active'] = $retour['active'];
            $tableau['connected'] = $retour['connected'];
            $tableau['lastlogin'] = $retour['lastlogin'];
            $tableau['joinTime'] = $retour['joinTime'];
            $tableau['lastPayment'] = $retour['lastPayment'];
            $tableau['AmountLastPayment'] = $retour['AmountLastPayment'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaire($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['importedBy'] = $retour['importedBy'];
            $tableau['active'] = $retour['active'];
            $tableau['tarifW'] = $retour['tarifW'];
            $tableau['tarifR'] = $retour['tarifR'];
            $tableau['display'] = $retour['display'];
            $tableau['webmasterAncre'] = $retour['display'];
            $tableau['webmasterConsigne'] = $retour['webmasterConsigne'];
            $tableau['webmasterPartenaire'] = $retour['webmasterPartenaire'];
            $tableau['WebPartenairePrice'] = $retour['WebPartenairePrice'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['page_rank'] = intval($retour['page_rank']);
            $tableau['age'] = intval($retour['age']);
            $tableau['ar'] = intval($retour['ar']);
            $tableau['cf'] = intval($retour['cf']);
            $tableau['tf'] = intval($retour['tf']);
            $tableau['cp'] = intval($retour['cp']);
            $tableau['tdr'] = intval($retour['tdr']);
            $tableau['tb'] = intval($retour['tb']);
            $tableau['tv'] = floatval($retour['tv']);
            $tableau['tav'] = floatval($retour['tav']);
            $tableau['created'] = $retour['created'];
// <parsing>
            $tableau['nddcible'] = $retour['nddcible'];
            $tableau['page_count'] = $retour['page_count'];
// </parsing>
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireListAllSans($id = "") {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires WHERE importedBy <> '" . $id . "' AND active = 1 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['importedBy'] = $retour['importedBy'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAncre($idProjet = 0, $idAnnuaire = 0, $user = 0) {
        global $dbh;
        global $idUser;

        if ($user == 0) {
            $user == $idUser;
        }

        $tableau = array();
        $requete = "";
        $requete = "SELECT * FROM ancres ORDER BY id ASC";
        if ($idProjet > 0 && $idAnnuaire == 0) {
            $requete = "SELECT * FROM ancres WHERE projetID = '" . $idProjet . "' ORDER BY id ASC";
        }
        if ($idProjet > 0 && $idAnnuaire > 0) {
            $requete = "SELECT * FROM ancres WHERE projetID = '" . $idProjet . "' AND annuaireID = '" . $idAnnuaire . "' ORDER BY id DESC LIMIT 1";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($requete != "" && $retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['projetID']][$retour['annuaireID']]['id'] = $retour['id'];
            $tableau[$retour['projetID']][$retour['annuaireID']]['projetID'] = $retour['projetID'];
            $tableau[$retour['projetID']][$retour['annuaireID']]['annuaireID'] = $retour['annuaireID'];
            $tableau[$retour['projetID']][$retour['annuaireID']]['ancre'] = stripslashes($retour['ancre']);
            $tableau[$retour['projetID']][$retour['annuaireID']]['webmasterID'] = $retour['webmasterID'];
            $tableau[$retour['projetID']][$retour['annuaireID']]['updated'] = $retour['updated'];
            $tableau[$retour['projetID']][$retour['annuaireID']]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

// <parsing>
    public static function getTauxTemps($annuaire_id = 0, $dateMin = '2015-01-01', $param = "cron") {
        global $dbh;

        $count_Temps = 0;
        $count_Taux = 0;
        $count_Taux_Reussi = 0;
        $count_Taux_Echoue = 0;
        $count_Taux_Nonn_Echoue = 0;
        $jourCalculer = 0;
        $taux = 0;


        $tableau = array();
//        $requete = "SELECT * FROM annuaires2backlinks WHERE annuaire_id = '" . $annuaire_id . "' AND date_checked_first > '" . $dateMin . "' AND status_type = '" . $param . "' ORDER BY site_id ASC";
        $requete = "SELECT * FROM annuaires2backlinks WHERE annuaire_id = '" . $annuaire_id . "' ORDER BY date_checked_first DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {

            $date_checked_first = strtotime($retour['date_checked_first']);
            $date_found = strtotime($retour['date_found']);
            $date_Min = strtotime($dateMin);

            if ($date_checked_first > $date_Min && $retour['status_type'] == $param) {

                $jourCalculerSecond = 0;
                if ($date_found > 0) {
                    $jourCalculerSecond = round(($date_found - $date_checked_first) / (60 * 60 * 24));
                    if ($jourCalculerSecond > 0) {
                        $jourCalculer += $jourCalculerSecond;
                        $count_Temps++;
                    }
                }
            }

            if ($retour['status'] == 'found') {
                $count_Taux_Reussi++;
            }
            if ($retour['status'] == 'not_found') {
                $count_Taux_Echoue++;
            }
            if ($retour['status'] == 'not_found_yet') {
                $count_Taux_Nonn_Echoue++;
            }
        }

        $totalCompteTaux = intval($count_Taux_Echoue + $count_Taux_Reussi);
        if ($totalCompteTaux > 0) {
            $taux = (($count_Taux_Reussi * 100) / $totalCompteTaux);
        }

        $tableau['taux'] = round($taux, 0);
        $tableau['temps'] = round($jourCalculer / $count_Temps);
        $tableau['reussi'] = $count_Taux_Reussi;
        $tableau['echoue'] = $count_Taux_Echoue;
        $tableau['echoueyet'] = $count_Taux_Nonn_Echoue;

        return $tableau;
    }

    public static function GetTauxUser($userID = 0) {
        global $dbh;
        $count_found = 0;
        $count_notfound = 0;
        $count_notfoundyet = 0;
        $count = 0;

        $tableau = array();

        $sql = "SELECT a2b.status AS status
                 FROM annuaires2backlinks AS a2b 
                   INNER JOIN jobs AS j
                    ON ( a2b.site_id = j.siteID AND j.annuaireID = a2b.annuaire_id AND j.affectedto ='" . $userID . "' )
                 ORDER BY a2b.status DESC";

        $execution = $dbh->query($sql);

        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {//                    echo "ok";
            if (!empty($retour['status'])) {
                if ($retour['status'] == "found") {
                    $count_found++;
                } else {
                    if ($retour['status'] == "not_found") {
                        $count_notfound++;
                    } else {
                        if ($retour['status'] == "not_found_yet") {
                            $count_notfoundyet++;
                        }
                    }
                }
                $count++;
            }
        }
        $tableau['total'] = $count;
        $tableau['found'] = $count_found;
        $tableau['notfoundyet'] = $count_notfoundyet;
        $tableau['notfound'] = $count_notfound;

        return $tableau;
    }

    public static function getPourcentage($object = 0, $total = 0) {
//        return round((($object / $total) * 100),0, PHP_ROUND_HALF_UP );
        return round((($object / $total) * 100), 1);
    }

    public static function isWebmasterCommission($userID = 0) {
        global $dbh;
        global $idUser;

        $sql = "SELECT COUNT(*) FROM annuaires WHERE webmasterPartenaire = '" . $userID . "' AND WebPartenairePrice > 0";
        $return = $dbh->query($sql)->fetchColumn(0);
        if ($return > 0) {
            return true;
        } else {
            return false;
        }
        return $tableau;
    }

    public static function getNoteUser($object = 0, $reference = 0, $base = 20, $basedOnNotFound = 0) {
        $note = 0;
        $congrats = "";

        $tableau = array();

        if ($reference == 0) {
            $reference = 100;
        }

        if ($object == 0) {
            $note = 0;
            $congrats = "Aucun";
        }


        if ($basedOnNotFound == 1) {
            if ($object > ($reference * 0.1) && $object <= ($reference * 0.15)) {
                $note = 0;
                $congrats = "Bien";
            }

            if ($object > ($reference * 0.15) && $object <= ($reference * 0.2)) {
                $note = 0;
                $congrats = "Assez-Bien";
            }

            if ($object > ($reference * 0.2) && $object <= ($reference * 0.25)) {
                $note = 0;
                $congrats = "Moyen";
            }

            if ($object > ($reference * 0.25) && $object <= ($reference * 0.3)) {
                $note = 0;
                $congrats = "Tr�s Moyen";
            }

            if ($object > ($reference * 0.3) && $object <= ($reference * 0.35)) {
                $note = 0;
                $congrats = "Mauvais";
            }
            if ($object > ($reference * 0.35) && $object <= ($reference * 0.8)) {
                $note = 0;
                $congrats = "Tr�s Mauvais";
            }
        } else {
            if ($object > 0 && $object <= ($reference * 0.2)) {
                $note = 0;
                $congrats = "Mauvais";
            }

            if ($object > ($reference * 0.2) && $object <= ($reference * 0.4)) {
                $note = 0;
                $congrats = "Tr�s Moyen";
            }

            if ($object > ($reference * 0.4) && $object <= ($reference * 0.55)) {
                $note = 0;
                $congrats = "Moyen";
            }

            if ($object > ($reference * 0.55) && $object <= ($reference * 0.65)) {
                $note = 0;
                $congrats = "Assez-Bien";
            }

            if ($object > ($reference * 0.65) && $object <= ($reference * 0.8)) {
                $note = 0;
                $congrats = "Bien";
            }

            if ($object > ($reference * 0.8) && $object <= ($reference * 0.9)) {
                $note = 0;
                $congrats = "Tr�s-Bien";
            }

            if ($object > ($reference * 0.9) && $object <= ($reference * 1)) {
                $note = 0;
                $congrats = "Excellent";
            }
        }



        $note = substr($object, 0, 1);


        $tableau['note'] = $note;
        $tableau['congrats'] = $congrats;

        return $tableau;
    }

    public static function getProjectBacklinksStatuses($project_id) {
        global $dbh;

        $sql = "SELECT
                  a2b.site_id AS site_id,
                  a2b.annuaire_id AS annuaire_id,
                  a2b.backlink AS backlink,
                  a2b.status AS status,
                  a2b.date_checked AS date_checked,
                  a2b.date_checked_first AS date_checked_first
                 FROM annuaires2backlinks AS a2b
                 WHERE a2b.site_id =" . intval($project_id);

        $result = $dbh->query($sql);
        $rows = array();

        $months_3 = 60 * 60 * 24 * 90; // 3 mothns in seconds
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $date_checked_first = strtotime($row['date_checked_first']);
            $rows[$row['annuaire_id']] = $row;
            $rows[$row['annuaire_id']]['first_checked'] = empty($row['date_checked_first']) ? false : true;
        }

        return $rows;
    }

// <parsing>
    public static function getBacklinkCouples($status = 'not_found_yet', $page = 1, $limit = 20, $classer = 0, $userID = 0, $delaiReferer = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;
        global $jourTime;

        $order = "";
        $orderUser = "";
        $orderPartenaire = "";

        if ($classer > 0) {
            $order = " a2b.annuaire_id = '" . $classer . "' and ";
        }

        if ($userID > 0) {
            if (isWebmaster($idTypeUser)) {
                $orderPartenaire = " AND a.webmasterPartenaire = '" . $userID . "' AND a.WebPartenairePrice > 0";
            } else {
                $orderUser = " AND j.affectedto = '" . $userID . "'";
                $orderSpecialReferer = "";
                if ($delaiReferer > 0) {
                    $orderSpecialReferer = " AND sa.referenceur_id = '" . $userID . "' AND sa.age > (a.tv + " . $delaiReferer . ") ";
                }
            }
        }

        $start = ($page - 1) * $limit;

        $sql = "SELECT a2b.site_id AS site_id,
                 a2b.annuaire_id AS annuaire_id,
                 a2b.backlink AS backlink,
                 a2b.date_checked AS date_checked,
                 a2b.date_checked_first AS date_checked_first,
                 p.consignes AS project_desc,
                 j.id AS soumissions_id,
                 j.adminApprouvedTime AS date_project_created,
                 j.affectedto AS referenceur,
                 p.lien AS lien,
                 p.proprietaire AS webmaster,
                 p.affectedTO AS projetReferer,
                 p.consignes AS consignes,
                 p.ancres AS ancres,
                 a.annuaire AS domain,
                 a.webmasterAncre AS AnnuaireDisplayAncre,
                 a.webmasterPartenaire AS partenaire,
                 a.consignes AS consignesAnnuaire,
                 a.tv AS tv,
                 a.tav AS tav,
                 a.id AS idannu
                 FROM annuaires2backlinks AS a2b 
                 INNER JOIN projets AS p
                    ON ( p.id = a2b.site_id )
                 INNER JOIN annuaires AS a
                    ON ( a2b.annuaire_id = a.id" . $orderPartenaire . ")
                 INNER JOIN jobs AS j
                    ON ( a2b.site_id = j.siteID AND j.annuaireID = a2b.annuaire_id" . $orderUser . ")
                WHERE" . $order . " a2b.status = '" . $status . "'
                ORDER BY j.adminApprouvedTime ASC
                LIMIT " . $start . ", " . $limit;


        $result = $dbh->query($sql);
//        echo $sql;

        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function getBacklinkCouplesCount($status = 'not_found_yet', $idAnnuaire = 0, $userID = 0, $delaiReferer = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;
        global $jourTime;

        $order = '';
        $orderUser = '';
        $orderUserWeb = '';

        if (intval($idAnnuaire) > 0) {
            $order = " a2b.annuaire_id = '" . $idAnnuaire . "' and ";
        }

        if ($userID > 0) {

            if (isWebmaster($idTypeUser)) {
                if (intval($idAnnuaire) > 0) {
                    $order = " WHERE j.annuaireID = '" . $idAnnuaire . "'";
                }
                $sql = "SELECT COUNT(*)
                 FROM jobs AS j
                 INNER JOIN annuaires AS a
                    ON ( a.id = j.annuaireID AND a.webmasterPartenaire = '" . $userID . "' AND a.WebPartenairePrice > 0)
                 INNER JOIN annuaires2backlinks AS a2b
                    ON ( a2b.site_id = j.siteID  AND a2b.annuaire_id = j.annuaireID AND a2b.status = '" . $status . "')" . $order;
            } else {
                if (intval($idAnnuaire) > 0) {
                    $order = " j.annuaireID = '" . $idAnnuaire . "' and";
                }

                if ($delaiReferer > 0) {
                    $orderUser = " AND sa.age > (a.tv + " . $delaiReferer . ")";
                }

                $sql = "SELECT COUNT(*)
                 FROM jobs AS j
                 INNER JOIN annuaires AS a
                    ON ( j.annuaireID = a.id) 
                 INNER JOIN soumissionsage AS sa 
                    ON (sa.annuaire_id = j.annuaireID AND sa.projet_id = j.siteID AND sa.referenceur_id = '" . $userID . "'" . $orderUser . " )
                 INNER JOIN annuaires2backlinks AS a2b
                    ON ( a2b.site_id = j.siteID  AND a2b.annuaire_id = j.annuaireID AND a2b.status = '" . $status . "')
                         WHERE" . $order . " j.affectedto = '" . $userID . "'";
            }
        } else {
            $sql = "SELECT COUNT(*)
                 FROM annuaires2backlinks AS a2b WHERE" . $order . " a2b.status = '" . $status . "'";
        }
//        echo $sql;

        return $dbh->query($sql)->fetchColumn(0);
    }

    public static function getBacklinkAnnuaireCount($annuaireID = 0, $userID = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;
        global $delaiReferer;

        $order = "";
        $orderUser = "";
        $statusDouble = "";

        if ($annuaireID > 0 && $userID > 0) {
            if (isReferer($idTypeUser)) {
                $sql = "SELECT COUNT(*)
                 FROM jobs AS j
                 INNER JOIN annuaires AS a
                    ON ( j.annuaireID = a.id) 
                 INNER JOIN soumissionsage AS sa 
                    ON (sa.annuaire_id = j.annuaireID AND sa.projet_id = j.siteID AND sa.referenceur_id = '" . $userID . "' AND sa.age > (a.tv + " . $delaiReferer . ") )
                 INNER JOIN annuaires2backlinks AS a2b
                    ON ( a2b.site_id = j.siteID  AND a2b.annuaire_id = j.annuaireID AND a2b.status = 'not_found_yet')
                         WHERE j.affectedto = '" . $userID . "' AND j.annuaireID ='" . $annuaireID . "'";
            } else {
                $sql = "SELECT COUNT(*)
                 FROM jobs AS j
                 RIGHT JOIN annuaires2backlinks AS a2b
                    ON ( a2b.site_id = j.siteID  AND a2b.annuaire_id = j.annuaireID AND a2b.status = 'not_found_yet')
                 WHERE j.affectedto ='" . $userID . "' AND j.annuaireID ='" . $annuaireID . "'";
            }
        } else {
            if ($annuaireID > 0) {
                $sql = "SELECT COUNT(*)
                 FROM annuaires2backlinks
                 WHERE annuaire_id ='" . $annuaireID . "' AND status = 'not_found_yet' ";
            }
        }

        $result = $dbh->query($sql)->fetchColumn(0);
        return !empty($result) ? $result : 0;
    }

// </parsing>

    public static function getAnnuaireList($count = 1, $tauxTemps = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $tableau = array();
        if (isAdmin($idTypeUser) || isSu($idTypeUser)) {
            $requete = "SELECT * FROM annuaires WHERE importedBy ='0' ORDER BY id ASC";
        } else {
            $requete = "SELECT * FROM annuaires WHERE importedBy ='" . $idUser . "' ORDER BY id ASC";
        }
        $execution = $dbh->query($requete);
//        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['page_rank'] = $retour['page_rank'];
            $tableau[$count]['tv'] = $retour['tv'];
            if ($tauxTemps != 0) {
                $reponse = Functions::getTauxTemps($retour['id'], $tauxTemps, 'cron');
                if ($reponse) {
                    $tableau[$count]['taux'] = $reponse['taux'];
                    $tableau[$count]['temps'] = $reponse['temps'];
                    $tableau[$count]['reussi'] = $reponse['reussi'];
                    $tableau[$count]['echoue'] = $reponse['echoue'];
                    $tableau[$count]['echoueyet'] = $reponse['echoueyet'];
                }
            }
            $tableau[$count]['importedBy'] = $retour['importedBy'];
            $tableau[$count]['active'] = $retour['active'];
            $tableau[$count]['created'] = $retour['created'];

            $count++;
        }

        return $tableau;
    }

    public static function getCategoriesList() {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM categories WHERE active = 1 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }


   public static function getCategoriesListv2() {
        
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM categories_v2 WHERE active = 1 AND parent = 0 ORDER BY value ASC";
        $execution = $dbh->query($requete);
        $count = 1;

        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {

            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['value'] = $retour['value'];
            $tableau[$count]['level'] = $retour['level'];
            $tableau[$count]['parent'] = $retour['parent'];
            $tableau[$count]['active'] = $retour['active'];
            $count++;

            $requete2 = "SELECT * FROM categories_v2 WHERE parent = $retour[id] AND active = 1 ORDER BY value ASC";
            $execution2 = $dbh->query($requete2);
            while ($retour2 = $execution2->fetch(PDO::FETCH_ASSOC)) {
            
                $tableau[$count]['id'] = $retour2['id'];
                $tableau[$count]['value'] = $retour2['value'];
                $tableau[$count]['level'] = $retour2['level'];
                $tableau[$count]['parent'] = $retour2['parent'];
                $tableau[$count]['active'] = $retour2['active'];
                $count++;

            }

        }

        return $tableau;

    }


    public static function getOneAnnuaireListe($id = 0, $idUserWeb = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        if ($idUserWeb == 0) {
            $idUserWeb = $idUser;
        }
        if ($idUserWeb == "webmasterAdmin") {
//            $idUserWeb = 0;
        }

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE id = " . $id;
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $requete = "SELECT * FROM annuaireslist WHERE id = " . $id;
        }
        if (isWebmaster($idTypeUser)) {
            if ($idUserWeb == "webmasterAdmin") {
                $requete = "SELECT * FROM annuaireslist WHERE id = " . $id;
            } else {
                $requete = "SELECT * FROM annuaireslist WHERE id = " . $id;
//                $requete = "SELECT * FROM annuaireslist WHERE proprietaire= '" . $idUserWeb . "' AND id = " . $id;
            }
        }

        try {
            $execution = $dbh->query($requete);
            $count = 1;
            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                $tableau['id'] = $retour['id'];
                $tableau['libelle'] = $retour['libelle'];
                $tableau['proprietaire'] = $retour['proprietaire'];
                $tableau['annuairesList'] = $retour['annuairesList'];
                $tableau['created'] = $retour['created'];
                $count++;
            }
        } catch (Exception $e) {
            
        }
        return $tableau;
    }

    public static function getOneAnnuaireListeV2($id = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $tableau = array();

        $requete = "SELECT * FROM annuaireslist WHERE id = " . $id;

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['libelle'] = $retour['libelle'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['annuairesList'] = $retour['annuairesList'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getUserAnnuairelist($selector = 0, $UserId = -1) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $user = $idUser;
        if ($UserId > -1) {
            $user = $UserId;
        }

        $tableau = "";

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '0' ORDER BY id DESC";
        } else {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $user . "' ORDER BY id DESC";
        }

        $execution = $dbh->query($requete);
        $count = 1;

        $tableau = '<select name="annuaire" class="select"><option value="0" >Annuaires � utiliser </option>';
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $selected = "";
            if ($selector == $retour['id']) {
                $selected = "selected='selected'";
            }
            $tableau .= '<option value="' . $retour['id'] . '" ' . $selected . '>' . $retour['libelle'] . '</option>';

            $count++;
        }
        $tableau .= "</select>";
        if ($tableau == '<select name="annuaire" class="select"><option value="0" >Annuaires � utiliser </option></select>') {
            $tableau = '<p style="color:red;">Aucune liste disponible.<br/> <a href="./compte/ajout/listannuaire.html">Cliquez ici</a> pour en cr�er.</p>';
        }
        return $tableau;
    }

    public static function getUserAnnuairelistCheckCase($selector = 0) {
        global $dbh;
        global $idUser;
        $tableau = "";

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '0' ORDER BY id DESC";
        } else {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $idUser . "' ORDER BY id DESC";
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $selected = "";
            $annuairesFromList = explode(";", $retour['annuairesList']);
            $annuairesFromListNew = $annuairesFromList;
            $annuairesFromListNew = array_filter($annuairesFromListNew);

            if (in_array($selector, $annuairesFromListNew)) {
                $selected = "checked='checked'";
            }
            $tableau .= '<div style="width:100%;float:left;"><input value="' . $retour['id'] . '" ' . $selected . ' type="checkbox" name="repertoire[]"  id="repertoire' . $retour['id'] . '">&nbsp;&nbsp;&nbsp;<label for="repertoire' . $retour['id'] . '" style="display:inline-block;">' . $retour['libelle'] . '</label></div>';

            $count++;
        }
        return $tableau;
    }

    public static function getUserAnnuairelistUser($selector = 0) {
        global $dbh;
        global $idUser;
        $tableau = "";

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '0' ORDER BY id DESC";
        } else {
            $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $idUser . "' ORDER BY id DESC";
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['annuairesList'] = $retour['annuairesList'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }
        return $tableau;
    }

    public static function getAnnuaireListeList($id = 2000) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $who = 0;
        } else {
            $who = $idUser;
        }

        if ($id != 2000) {
            $who = $id;
        }

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist WHERE proprietaire = '" . $who . "' ORDER BY libelle ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['annuairesList'] = $retour['annuairesList'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireAllList() {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $tableau = array();
        $requete = "SELECT * FROM annuaireslist ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['libelle'] = $retour['libelle'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['annuairesList'] = $retour['annuairesList'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getContratsList() {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM contrats ORDER BY typeUser ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['typeUser'] = $retour['typeUser'];
            $tableau[$count]['content'] = $retour['content'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireListById($id = 0) {
        global $dbh;
        global $idUser;

        $tableau = array();
        if (isSu($idUser) || isAdmin($idUser)) {
            $requete = "SELECT * FROM annuaires ORDER BY id ASC";
        } else {
            $requete = "SELECT * FROM annuaires WHERE importedBy='" . $id . "' ORDER BY id ASC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['importedBy'] = $retour['importedBy'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getJob($id = 0, $iduser = 0) {
        global $dbh;

        $tableau = array();
        if ($iduser != 0) {
            if ($iduser == 1000001) {
                $requete = "SELECT * FROM jobs ORDER BY id DESC";
            } else {
                $requete = "SELECT * FROM jobs WHERE affectedto='" . $iduser . "' ORDER BY id DESC";
            }
        } else {
            $requete = "SELECT * FROM jobs WHERE siteID='" . $id . "' AND adminApprouved > 1 ORDER BY id ASC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['siteID'] = $retour['siteID'];
            $tableau[$count]['annuaireID'] = $retour['annuaireID'];
            $tableau[$count]['emailSoumission'] = $retour['emailSoumission'];
            $tableau[$count]['affectedto'] = $retour['affectedto'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau[$count]['soumissionTime'] = $retour['soumissionTime'];
            $tableau[$count]['adminApprouved'] = $retour['adminApprouved'];
            $tableau[$count]['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau[$count]['payed'] = $retour['payed'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getJobByID($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM jobs WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['siteID'] = $retour['siteID'];
            $tableau['annuaireID'] = $retour['annuaireID'];
            $tableau['emailSoumission'] = $retour['emailSoumission'];
            $tableau['affectedto'] = $retour['affectedto'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau['soumissionTime'] = $retour['soumissionTime'];
            $tableau['adminApprouved'] = $retour['adminApprouved'];
            $tableau['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau['payed'] = $retour['payed'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getLastJob($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM jobs WHERE siteID='" . $id . "' AND adminApprouved > 1 ORDER BY id DESC LIMIT 1";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['siteID'] = $retour['siteID'];
            $tableau['annuaireID'] = $retour['annuaireID'];
            $tableau['emailSoumission'] = $retour['emailSoumission'];
            $tableau['affectedto'] = $retour['affectedto'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau['soumissionTime'] = $retour['soumissionTime'];
            $tableau['adminApprouved'] = $retour['adminApprouved'];
            $tableau['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau['payed'] = $retour['payed'];
            $tableau['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getLastJobs($id = 0, $nbre = 1) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM jobs WHERE siteID='" . $id . "' AND adminApprouved > 1 ORDER BY id DESC LIMIT 0," . $nbre;
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['siteID'] = $retour['siteID'];
            $tableau[$count]['annuaireID'] = $retour['annuaireID'];
            $tableau[$count]['emailSoumission'] = $retour['emailSoumission'];
            $tableau[$count]['affectedto'] = $retour['affectedto'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['soumissibleTime'] = $retour['soumissibleTime'];
            $tableau[$count]['soumissionTime'] = $retour['soumissionTime'];
            $tableau[$count]['adminApprouved'] = $retour['adminApprouved'];
            $tableau[$count]['adminApprouvedTime'] = $retour['adminApprouvedTime'];
            $tableau[$count]['payed'] = $retour['payed'];
            $tableau[$count]['created'] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function ChangeMessageStatut($id) {
        global $dbh;
        $requete = "UPDATE messages SET lu=0 WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
    }

    public static function getCommentaire($id) {
        global $dbh;
        global $idUser;
        global $idTypeUser;


        $tableau = array();
        if (isAdmin() || isSu()) {
            $requete = "SELECT * FROM commentaires WHERE id='" . $id . "'";
        }

        if (isWebmaster()) {
            $requete = "SELECT * FROM commentaires WHERE id='" . $id . "' AND typeuser=" . $idTypeUser;
        }

        if (isSuperReferer()) {
            $requete = "SELECT * FROM commentaires WHERE id='" . $id . "' AND typeuser=" . $idTypeUser;
        }

        if ($typeUSSer > 0 && $idProjet > 0) {
            
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['idprojet'] = $retour['idprojet'];
            $tableau['idannuaire'] = $retour['idannuaire'];
            $tableau['user'] = $retour['user'];
            $tableau['typeuser'] = $retour['typeuser'];
            $tableau['commentaire'] = $retour['com'];
            $tableau['edited'] = $retour['edited'];
            $tableau['created'] = $retour['created'];
        }
        return $tableau;
    }

    public static function getListCommentaire($typeUSSer = 0, $idProjet = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;

        $requete = "SELECT * FROM commentaires WHERE idprojet='" . $idProjet . "' AND typeuser=" . $typeUSSer . " ORDER BY idannuaire ASC";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['idannuaire']]['id'] = $retour['id'];
            $tableau[$retour['idannuaire']]['idprojet'] = $retour['idprojet'];
            $tableau[$retour['idannuaire']]['idannuaire'] = $retour['idannuaire'];
            $tableau[$retour['idannuaire']]['user'] = $retour['user'];
            $tableau[$retour['idannuaire']]['typeuser'] = $retour['typeuser'];
            $tableau[$retour['idannuaire']]['commentaire'] = $retour['com'];
            $tableau[$retour['idannuaire']]['edited'] = $retour['edited'];
            $tableau[$retour['idannuaire']]['created'] = $retour['created'];
        }
        return $tableau;
    }

    public static function getRandomUser($id = 0, $typeuser = 0, $secondaire = "") {
        global $dbh;
        global $idUser;


        $difference = 'id <> ' . $id;
        $array = array();

        if ($secondaire != "") {
            $secondaireRef = explode(";", $secondaire);
            $secondaireRef = array_filter($secondaireRef);
            $array = $secondaireRef;
            foreach ($secondaireRef as $refS) {
                $difference.=' AND id <> ' . $refS;
            }
        }
        $array[count($array)] = $id;

        $requete = 'SELECT id FROM utilisateurs WHERE typeutilisateur = "' . $typeuser . '" AND ' . $difference . ' ORDER BY RAND() LIMIT 1';
        $idRec = $id;

        while (in_array($idRec, $array)) {
            $execution = $dbh->query($requete);
            $retour = $execution->fetch();
            $idRec = $retour['id'];
        }

        return $idRec;
    }

    public static function getAllCommentaire($projet, $annuaire) {
        global $dbh;

        $tableau = array();
        $tableau[$count]['totalcom'] = "";
        $requete = "SELECT * FROM commentaires WHERE idprojet='" . $projet . "' AND idannuaire ='" . $annuaire . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['typeuser']]["id"] = $retour['id'];
            $tableau[$retour['typeuser']]["user"] = $retour['user'];
            $tableau[$retour['typeuser']]["typeuser"] = $retour['typeuser'];
            $tableau[$retour['typeuser']]['edited'] = $retour['edited'];
            $tableau[$retour['typeuser']]['created'] = $retour['created'];
            $tableau[$retour['typeuser']]['commentaire'] = $retour['com'];


            $tableau['common']['idprojet'] = $retour['idprojet'];
            $tableau['common']['idannuaire'] = $retour['idannuaire'];
            $tableau['common']['totalcom'] .= $retour['com'] . PHP_EOL;
        }

        return $tableau;
    }

    public static function getAllCommentairesTogether($user = 0) {
        global $dbh;

        $tableau = array();
        $tableau[$count]['totalcom'] = "";
        if ($user != 0 && $user != 1000001) {
            $requete = "SELECT * FROM commentaires WHERE user='" . $user . " ORDER BY id ASC";
        } else {
            $requete = "SELECT * FROM commentaires ORDER BY id ASC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['idprojet'] . $retour['idannuaire']]["id"] = $retour['id'];
            $tableau[$retour['idprojet'] . $retour['idannuaire']]["user"] = $retour['user'];
            $tableau[$retour['idprojet'] . $retour['idannuaire']]["typeuser"] = $retour['typeuser'];
            $tableau[$retour['idprojet'] . $retour['idannuaire']]['edited'] = $retour['edited'];
            $tableau[$retour['idprojet'] . $retour['idannuaire']]['created'] = $retour['created'];
            $tableau[$retour['idprojet'] . $retour['idannuaire']]['commentaire'] = $retour['com'];
        }

        return $tableau;
    }

    public static function getMessageList($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM messages WHERE receiver='" . $id . "' AND viewReceiver=1 ORDER BY id DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['objet'] = $retour['objet'];
            $tableau[$count]['content'] = $retour['content'];
            $tableau[$count]['sender'] = Functions::getfullname($retour['sender']);
            $tableau[$count]['receiver'] = Functions::getfullname($retour['receiver']);
            $tableau[$count]['lu'] = $retour['lu'];
            $tableau[$count]['viewSender'] = $retour['viewSender'];
            $tableau[$count]['viewReceiver'] = $retour['viewReceiver'];
            $tableau[$count]['time'] = $retour['time'];
            $count++;
        }

        return $tableau;
    }

    public static function getUserOptionList($typeUser = 3, $egal = 0, $selector = 0, $linkUser = 0) {
        global $dbh;
        global $idUser;
        global $arrayLvl;
        $tampon = 0;
        $tampon2 = 0;
        $tableau = "";

        if ($egal == 0) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur < " . $typeUser . " AND id <> " . $idUser . " ORDER BY typeutilisateur ASC, nom ASC";
        } else if ($egal == 1) {
//            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '" . $typeUser . "' AND id <> " . $idUser . " ORDER BY id ASC";
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur = '" . $typeUser . "' ORDER BY nom ASC";
        } else if ($egal == 3) {
            $requete = "SELECT * FROM utilisateurs WHERE typeutilisateur > " . $typeUser . " AND id <> " . $idUser . " ORDER BY typeutilisateur ASC, nom ASC";
        }

        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {

            $selected = "";
            if ($selector == $retour['id']) {
                $selected = "selected='selected'";
            }

            if ($tampon == 0 || ($tampon != $retour['typeutilisateur'])) {
                if ($retour['typeutilisateur'] > 0) {
                    $tampon2 = 3;
                    $tampon3 = 1;
                    $tampon = $retour['typeutilisateur'];
                } else {
                    $tampon2 = 3000;
                    $tampon3 = 3000;
                }
            } else {
                $tampon2 = 1;
                $tampon3 = 0;
            }


            $var = '<option value="' . $linkUser . '' . $retour['id'] . '" ' . $selected . '>' . strtoupper($retour['nom']) . ' ' . $retour['prenom'] . '</option>';

            if ($tampon2 == 3 && $tampon4 == 1) {
                $tableau .= "</optgroup>";
                $tampon4 = 0;
            }

            if (($tampon2 == 3 && $tampon3 == 1 && $tampon4 == 0)) {
                $tableau .= "<optgroup label='" . $arrayLvl[$retour['typeutilisateur']] . "'>";
                $tampon4 = 1;
                $tampon2 = 1;
            }

            if ($tampon2 == 1) {
                $tableau .= $var;
            }
        }
        return $tableau;
    }

    public static function getAnnuaireListOption($selector = 0, $linkUser = "", $details = 0, $condParam = '', $condParam2 = 0) {
        global $dbh;
        global $idUser;
        global $idTypeUser;
        global $arrayLvl;


        $point = 0;
        $tampon = array();
        $tampon2 = 0;
        $tableau = "";

        if ($condParam2 > 0 && isWebmaster($idTypeUser)) {
            $requete = "SELECT * FROM annuaires WHERE webmasterPartenaire = '" . $condParam2 . "' AND WebPartenairePrice > 0 ORDER BY annuaire ASC";
        } else {
            $requete = "SELECT * FROM annuaires ORDER BY annuaire ASC";
        }
        $execution = $dbh->query($requete);

        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $annuairelast = preg_replace("#http:\/\/#", "", $retour['annuaire']);
            $annuairelast = preg_replace("#www\.#", "", $annuairelast);

            if (substr($annuairelast, (strlen($annuairelast) - 1), (strlen($annuairelast))) == "/") {
                $annuairelast = substr($annuairelast, 0, (strlen($annuairelast) - 1));
            }

            $tampon[$point]['id'] = $retour['id'];
            $tampon[$point]['annuaire'] = $annuairelast;
            $point++;
        }

        function cmp($a, $b) {
            return strcmp($a["annuaire"], $b["annuaire"]);
        }

        uasort($tampon, "cmp");
        $totalBacklinks = 0;

        foreach ($tampon as $retourTampon) {
            $selected = "";
            if ($selector == $retourTampon['id']) {
                $selected = "selected='selected'";
            }
            $nbreBklks = 0;

            if ($condParam == "backlinks" && (($details > 0 && isReferer($idTypeUser)) || isSu($idTypeUser) || isAdmin($idTypeUser) || (isWebmaster($idTypeUser) && $condParam2 > 0))) {
                if (isWebmaster($idTypeUser)) {
//                    $nbreBklks = Functions::getBacklinkCouplesCount('not_found_yet', 0, $idUser);
                    $nbreBklks = Functions::getBacklinkAnnuaireCount($retourTampon['id'], $details);
                } else {
                    $nbreBklks = Functions::getBacklinkAnnuaireCount($retourTampon['id'], $details);
                }
                $condParamContent = ' (<span style="font-weight:bold;color:red !important">' . $nbreBklks . '</span>)';

                $totalBacklinks += $nbreBklks;
            }

            if ((($condParam == "backlinks" && $nbreBklks > 0 && isReferer()) || isWebmaster() || isSu() || isAdmin()) || $condParam == "") {
                $tableau .= '<option value="' . $linkUser . '' . $retourTampon['id'] . '" ' . $selected . '>' . $retourTampon['annuaire'] . $condParamContent . '</option>';
            }
        }


//        echo $totalBacklinks."xxxxxxx";
        return $tableau;
    }

    public static function getCategorieList($selectedID = 0) {
        global $dbh;

        $requete = "SELECT * FROM categories WHERE active =1 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $tableau = "";
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $selected = "";
            if ($selectedID == $retour['id']) {
                $selected = "selected='selected'";
            }
            $tableau .= '<option value="' . $retour['id'] . '" ' . $selected . '>' . $retour['libelle'] . '</option>';
        }
        return $tableau;
    }

    public static function getMessageSended($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM messages WHERE sender='" . $id . "' AND viewSender=1 ORDER BY id DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['objet'] = $retour['objet'];
            $tableau[$count]['content'] = $retour['content'];
            $tableau[$count]['sender'] = Functions::getfullname($retour['sender']);
            $tableau[$count]['receiver'] = Functions::getfullname($retour['receiver']);
            $tableau[$count]['viewSender'] = $retour['viewSender'];
            $tableau[$count]['viewReceiver'] = $retour['viewReceiver'];
            $tableau[$count]['lu'] = $retour['lu'];
            $tableau[$count]['time'] = $retour['time'];
            $count++;
        }

        return $tableau;
    }

    public static function getMessageById($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM messages WHERE id='" . $id . "'";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['objet'] = $retour['objet'];
            $tableau['content'] = $retour['content'];
            $tableau['senderID'] = $retour['sender'];
            $tableau['receiverID'] = $retour['receiver'];
            $tableau['sender'] = Functions::getfullname($retour['sender']);
            $tableau['receiver'] = Functions::getfullname($retour['receiver']);
            $tableau['viewSender'] = $retour['viewSender'];
            $tableau['viewReceiver'] = $retour['viewReceiver'];
            $tableau['lu'] = $retour['lu'];
            $tableau['time'] = $retour['time'];
        }

        return $tableau;
    }

    public static function getRechercheResultats($id = 0, $type = "projets", $motsCles = "") {
        global $dbh;
        global $idTypeUser;
        if ($type == "projets") {
            if (isAdmin($idTypeUser) || isSu($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE lien LIKE '%" . $motsCles . "%' AND over >= 0 ORDER BY over ASC";

                // prepared
                $requete = "SELECT * FROM projets WHERE lien LIKE :motsCles AND over >= 0 ORDER BY over ASC";
            }
            if (isReferer($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE affectedTO ='" . $id . "' AND over = 0 AND lien LIKE '%" . $motsCles . "%' ORDER BY id ASC";

                //prepared
                $requete = "SELECT * FROM projets WHERE affectedTO =:id AND over = 0 AND lien LIKE :motsCles ORDER BY id ASC";
            }

            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire ='" . $id . "' AND over >= 0 AND lien LIKE '%" . $motsCles . "%' ORDER BY over ASC";

                //prepared
                $requete = "SELECT * FROM projets WHERE proprietaire =:id AND over >= 0 AND lien LIKE :motsCles ORDER BY over ASC";
            }
        }
        if ($type == "projetsd") {
            
        }

        if ($type == "utilisateurs" && (isAdmin($idTypeUser) || isSu($idTypeUser))) {
            $requete = "SELECT * FROM utilisateurs WHERE nom LIKE '%" . $motsCles . "%' OR prenom LIKE '%" . $motsCles . "%' OR email LIKE '%" . $motsCles . "%' ORDER BY id ASC";

            // prepared
            $requete = "SELECT * FROM utilisateurs WHERE nom LIKE :motsCles OR prenom LIKE :motsCles OR email LIKE :motsCles ORDER BY id ASC";
        }

        // old
        //$execution = $dbh->query($requete);
        // prepared
        $search_param = '%' . $motsCles . '%';
        $stmt = $dbh->prepare($requete);
        $stmt->bindParam(':motsCles', $search_param);

        if (isReferer($idTypeUser) || isWebmaster($idTypeUser)) {
            $stmt->bindParam(':id', $id);
        }

        //var_dump($stmt->queryString);
        $stmt->execute();

        $count = 1;
        while ($retour = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($type == "utilisateurs" && (isAdmin($idTypeUser) || isSu($idTypeUser))) {
                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['nom'] = $retour['nom'];
                $tableau[$count]['prenom'] = $retour['prenom'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['ville'] = $retour['ville'];
                $tableau[$count]['codepostal'] = $retour['codepostal'];
                $tableau[$count]['avatar'] = $retour['avatar'];
                $tableau[$count]['societe'] = $retour['societe'];
                $tableau[$count]['adresse'] = $retour['adresse'];
                $tableau[$count]['siteweb'] = $retour['siteweb'];
                $tableau[$count]['telephone'] = $retour['telephone'];
                $tableau[$count]['devise'] = $retour['devise'];
                $tableau[$count]['solde'] = $retour['solde'];
                $tableau[$count]['frais'] = $retour['frais'];
                $tableau[$count]['lastIP'] = $retour['lastIP'];
                $tableau[$count]['styleAdmin'] = $retour['styleAdmin'];
                $tableau[$count]['typeutilisateur'] = $retour['typeutilisateur'];
                $tableau[$count]['active'] = $retour['active'];
                $tableau[$count]['connected'] = $retour['connected'];
                $tableau[$count]['lastlogin'] = $retour['lastlogin'];
                $tableau[$count]['joinTime'] = $retour['joinTime'];
                $tableau[$count]['lastPayment'] = $retour['lastPayment'];
                $tableau[$count]['AmountLastPayment'] = $retour['AmountLastPayment'];
            }
            if ($type == "projets") {

                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['parent'] = $retour['parent'];
                $tableau[$count]['categorieID'] = $retour['categories'];
                $tableau[$count]['categorie'] = Functions::getCategorieName($retour['categories']);
                $tableau[$count]['lien'] = $retour['lien'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['proprietaire'] = $retour['proprietaire'];
                $tableau[$count]['frequence'] = $retour['frequence'];
                $tableau[$count]['annuaire'] = $retour['annuaire'];
                $tableau[$count]['consignes'] = $retour['consignes'];
                $tableau[$count]['envoyer'] = $retour['envoyer'];
                $tableau[$count]['sendTime'] = $retour['sendTime'];
                $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
                $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
                $tableau[$count]['adminRaison'] = $retour['adminRaison'];
                $tableau[$count]['over'] = $retour['over'];
                $tableau[$count]['overTime'] = $retour['overTime'];
                $tableau[$count]['affectedTO'] = $retour['affectedTO'];
                $tableau[$count]['affectedBY'] = $retour['affectedBY'];
                $tableau[$count]['affectedTime'] = $retour['affectedTime'];
                $tableau[$count]['showProprio'] = $retour['showProprio'];
                $tableau[$count]['budget'] = $retour['budget'];
            }

            if ($type == "projetss") {
                
            }


            $count++;
        }

        return $tableau;
    }

    public static function getProjetsListByUser($id = 0, $isAdmin = 3, $over = 0, $adminApprouve = 1, $envoyer = 1, $affected = 1) {
        global $dbh;
        if ($isAdmin == 3) {
            $requete = "SELECT * FROM projets WHERE affectedTO ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = '" . $over . "' ORDER BY affectedTime DESC LIMIT 5";
        }

        if ($isAdmin == 4) {
            $requete = "SELECT * FROM projets WHERE proprietaire ='" . $id . "' AND envoyer = '" . $envoyer . "' AND adminApprouve = '" . $adminApprouve . "' AND over = '" . $over . "' ORDER BY affectedTime DESC LIMIT 5";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['parent'] = $retour['parent'];
            $tableau[$count]['categorieID'] = $retour['categories'];
            $tableau[$count]['categorie'] = Functions::getCategorieName($retour['categories']);
            $tableau[$count]['lien'] = $retour['lien'];
            $tableau[$count]['email'] = $retour['email'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['frequence'] = $retour['frequence'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['consignes'] = $retour['consignes'];
            $tableau[$count]['envoyer'] = $retour['envoyer'];
            $tableau[$count]['sendTime'] = $retour['sendTime'];
            $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
            $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau[$count]['adminRaison'] = $retour['adminRaison'];
            $tableau[$count]['over'] = $retour['over'];
            $tableau[$count]['overTime'] = $retour['overTime'];
            $tableau[$count]['affectedTO'] = $retour['affectedTO'];
            $tableau[$count]['affectedBY'] = $retour['affectedBY'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['showProprio'] = $retour['showProprio'];
            $tableau[$count]['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getCommande($id, $idUser, $isAdmin = 0) {
        global $dbh;

        $tableau = array();
        if ($isAdmin == 1) {
            $requete = "SELECT * FROM projets WHERE id='" . $id . "'";
        } else {
            $requete = "SELECT * FROM projets WHERE id='" . $id . "' AND proprietaire='" . $idUser . "' AND showProprio=1";
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['parent'] = $retour['parent'];
            $tableau['categorieID'] = $retour['categories'];
            $tableau['categorie'] = Functions::getCategorieName($retour['categories']);
            $tableau['lien'] = $retour['lien'];
            $tableau['email'] = $retour['email'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['frequence'] = $retour['frequence'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['ancres'] = $retour['ancres'];
            $tableau['envoyer'] = $retour['envoyer'];
            $tableau['sendTime'] = $retour['sendTime'];
            $tableau['adminApprouve'] = $retour['adminApprouve'];
            $tableau['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau['adminRaison'] = $retour['adminRaison'];
            $tableau['over'] = $retour['over'];
            $tableau['overTime'] = $retour['overTime'];
            $tableau['affectedTO'] = $retour['affectedTO'];
            $tableau['affectedBY'] = $retour['affectedBY'];
            $tableau['showProprio'] = $retour['showProprio'];
            $tableau['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getAllCommande($id) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM projets WHERE proprietaire='" . $id . "' ORDER BY over ASC, adminApprouve DESC, envoyer DESC ";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]['id'] = $retour['id'];
            $tableau[$count]['parent'] = $retour['parent'];
            $tableau[$count]['categorieID'] = $retour['categories'];
            $tableau[$count]['categorie'] = Functions::getCategorieName($retour['categories']);
            $tableau[$count]['lien'] = $retour['lien'];
            $tableau[$count]['email'] = $retour['email'];
            $tableau[$count]['proprietaire'] = $retour['proprietaire'];
            $tableau[$count]['frequence'] = $retour['frequence'];
            $tableau[$count]['annuaire'] = $retour['annuaire'];
            $tableau[$count]['consignes'] = $retour['consignes'];
            $tableau[$count]['envoyer'] = $retour['envoyer'];
            $tableau[$count]['sendTime'] = $retour['sendTime'];
            $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
            $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau[$count]['adminRaison'] = $retour['adminRaison'];
            $tableau[$count]['over'] = $retour['over'];
            $tableau[$count]['overTime'] = $retour['overTime'];
            $tableau[$count]['affectedTO'] = $retour['affectedTO'];
            $tableau[$count]['affectedBY'] = $retour['affectedBY'];
            $tableau[$count]['affectedTime'] = $retour['affectedTime'];
            $tableau[$count]['showProprio'] = $retour['showProprio'];
            $tableau[$count]['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getAnnuaireAll($id = "") {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM annuaires ORDER BY id ASC ";

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['id']]['id'] = $retour['id'];
            $tableau[$retour['id']]['annuaire'] = $retour['annuaire'];
            $tableau[$retour['id']]['importedBy'] = $retour['importedBy'];
            $tableau[$retour['id']]['active'] = $retour['active'];
            $tableau[$retour['id']]['page_rank'] = $retour['page_rank'];
            $tableau[$retour['id']]['display'] = $retour['display'];
            $tableau[$retour['id']]['consignes'] = $retour['consignes'];
            $tableau[$retour['id']]['nddcible'] = $retour['nddcible'];
            $tableau[$retour['id']]['page_count'] = $retour['page_count'];
            $count++;
        }

        return $tableau;
    }

    public static function getRetard($time = 0, $timePret, $dureeJour = 86400) {
        $jourRetarder = 0;
        $data = array();
        if ($time == 0) {
            $time = time();
        }
        if ($timePret > 0) {
            $retardataires = ($time - $timePret) / $dureeJour;

//        $ecxploe = explode('.', $retardataires);
//        $jourRetarder = $ecxploe[0];
//        if ($ecxploe[1] > 60) {
//            $retardataire++;
//        }
            $jourRetarder = round($retardataires);
        }

        $data[] = $retardataires;
        $data[] = $jourRetarder;
        return $data;
    }

    public static function getSpecialAllCommandeReferer($id, $default) {
        global $dbh;
        global $idTypeUser;
        global $idUser;


        $requete = "SELECT * FROM projets WHERE categories LIKE '%" . $id . "%' AND envoyer = 1 AND adminApprouve = 1 AND over = 0 ORDER BY id ASC";
        $execution = $dbh->query($requete);
        $count = 1;
        $time = time();

        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $isOkPass = 0;
            $jourRetarder = 0;

            if ($retour['adminApprouveTime'] <= time() && ((!isReferer($idTypeUser) || (isReferer($idTypeUser) && Functions::isWebmasterCan($retour['proprietaire']))))) {
                if (strpos($retour['email'], "|")) {
                    list($timePret, $pointAffaire) = explode("|", $retour['email']);
                    if ($timePret <= time() && $pointAffaire > 0) {
                        $isOkPass = 1;
                    }
                }
            }

            if ($isOkPass == 1) {
                $infoRetard = Functions::getRetard($time, $timePret);
                $jourRetarder = $infoRetard[1];
            }

            $inside = 0;
            if ($retour['categories'] != "" && $retour['categories'] != $default) {
                $secondaireRef = explode(";", $retour['categories']);
                $secondaireRef = array_filter($secondaireRef);
                if (in_array($idUser, $secondaireRef)) {
                    $inside = 1;
                }
            }

            if ($jourRetarder > 0 && $inside == 1) {
                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['parent'] = $retour['parent'];
                $tableau[$count]['affectedSec'] = $retour['categories'];
                $tableau[$count]['categorie'] = "";
                $tableau[$count]['lien'] = $retour['lien'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['proprietaire'] = $retour['proprietaire'];
                $tableau[$count]['frequence'] = $retour['frequence'];
                $tableau[$count]['annuaire'] = $retour['annuaire'];
                $tableau[$count]['consignes'] = $retour['consignes'];
                $tableau[$count]['envoyer'] = $retour['envoyer'];
                $tableau[$count]['sendTime'] = $retour['sendTime'];
                $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
                $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
                $tableau[$count]['adminRaison'] = $retour['adminRaison'];
                $tableau[$count]['over'] = $retour['over'];
                $tableau[$count]['overTime'] = $retour['overTime'];
                $tableau[$count]['affectedTO'] = $retour['affectedTO'];
                $tableau[$count]['affectedBY'] = $retour['affectedBY'];
                $tableau[$count]['affectedTime'] = $retour['affectedTime'];
                $tableau[$count]['showProprio'] = $retour['showProprio'];
                $tableau[$count]['budget'] = $retour['budget'];
                $tableau[$count]['retard'] = $jourRetarder;
                $count++;
            }
        }

        return $tableau;
    }

    public static function getAllCommandeReferer($id, $isAdmin = 0, $param = " adminApprouveTime ASC", $supRef = 0) {
        global $dbh;
        global $idTypeUser;
        global $idUser;
        $time = time();

        if ($param == "das") {
            $param = " adminApprouveTime ASC";
        } else if ($param == "dde") {
            $param = " adminApprouveTime DESC";
        } else if ($param == "ras") {
            $param = " affectedTO ASC, adminApprouveTime ASC";
        } else if ($param == "rde") {
            $param = " affectedTO ASC, adminApprouveTime DESC";
        } else if ($param == "was") {
            $param = " proprietaire ASC, adminApprouveTime ASC";
        } else if ($param == "wde") {
            $param = " proprietaire ASC, adminApprouveTime DESC";
        } else if ($param == "lat") {
            $param = " affectedTO ASC, adminApprouveTime DESC";
        } else {
            $param = $param;
        }

        $tableau = array();
        if ($isAdmin == 1) {
            if (isReferer($idTypeUser) && $supRef == 0) {
                $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND envoyer = 1 AND adminApprouve = 1 AND over = 0 ORDER BY" . $param;
            }
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer =1 AND adminApprouve =1 AND affectedTO <> 0 AND over =0 AND showProprio = 1 ORDER BY" . $param;
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $requete = "SELECT * FROM projets WHERE envoyer = 1 AND adminApprouve =1 AND over =0 AND affectedTO <> 0 ORDER BY" . $param;
            }
        } else if ($isAdmin == 2) {
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire ='" . $id . "' AND envoyer =1 AND adminApprouve = 1 AND affectedTO = 0 AND over = 0 AND showProprio = 1 ORDER BY" . $param;
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $requete = "SELECT * FROM projets WHERE envoyer = 1 AND adminApprouve = 1 AND over = 0 AND affectedTO = 0 ORDER BY" . $param;
            }
        } else if ($isAdmin == 3) {
            if (isReferer($idTypeUser) && $supRef == 0) {
                $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND envoyer =1 AND adminApprouve =1 and over =1 ORDER BY over DESC";
            }
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer =1 AND adminApprouve =1 and over =1 AND showProprio = 1 ORDER BY over DESC";
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve =1 AND over = 1 ORDER BY over DESC";
            }
        } else if ($isAdmin == 4) {
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer =0 AND adminApprouve = 0 AND over = 0 AND showProprio = 1 ORDER BY" . $param;
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve = 0 AND over = 0 ORDER BY" . $param;
            }
        } else if ($isAdmin == 5) {
            if (isWebmaster($idTypeUser)) {
                $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND envoyer = 1 AND adminApprouve = 2 OR adminApprouve = 2 AND over = 0 AND showProprio = 1 ORDER BY parent ASC, id DESC";
            }
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $requete = "SELECT * FROM projets WHERE envoyer =1 AND adminApprouve = 2 AND over = 0 ORDER BY parent ASC, id DESC";
            }
        } else if ($isAdmin == 6) {
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $user = Functions::getUserInfos($id);
                if ($user['typeutilisateur'] == 4) {
                    $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND over = 0 ORDER BY" . $param . "";
                }
                if ($user['typeutilisateur'] == 3) {
                    $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND over = 0 ORDER BY" . $param . "";
                }
            }
        } else if ($isAdmin == 7) {
            if (isAdmin($idTypeUser) || isSU($idTypeUser) || (isSuperReferer($idUser) && $supRef == 1)) {
                $user = Functions::getUserInfos($id);
                if ($user['typeutilisateur'] == 4) {
                    $requete = "SELECT * FROM projets WHERE proprietaire = '" . $id . "' AND over = 1  ORDER BY" . $param . "";
                }
                if ($user['typeutilisateur'] == 3) {
                    $requete = "SELECT * FROM projets WHERE affectedTO = '" . $id . "' AND over = 1  ORDER BY" . $param . "";
                }
            }
        }

        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $isOkPass = 0;
            $jourRetarder = 0;

            if ($retour['adminApprouveTime'] <= time() && ((!isReferer($idTypeUser) || isSuperReferer($idUser) || (isReferer($idTypeUser) && $supRef == 0 && Functions::isWebmasterCan($retour['proprietaire']))))) {
                if (strpos($retour['email'], "|")) {
                    list($timePret, $pointAffaire) = explode("|", $retour['email']);
                    if ($timePret <= time() && $pointAffaire > 0) {
                        $isOkPass = 1;
                    }
                }
            }

            $autreCond = 0;
            if ($isOkPass == 1) {
//                $infoRetard = Functions::getRetard($time, $timePret);
//                $jourRetarder = $infoRetard[1];
            }

            if (!empty($retour['id']) && (!isReferer($idTypeUser) || isSuperReferer($idUser) || (isReferer($idTypeUser) && $supRef == 0 && ($isOkPass == 1)))) {
                $tableau[$count]['id'] = $retour['id'];
                $tableau[$count]['parent'] = $retour['parent'];
                $tableau[$count]['affectedSec'] = $retour['categories'];
                $tableau[$count]['categorie'] = "";
                $tableau[$count]['lien'] = $retour['lien'];
                $tableau[$count]['email'] = $retour['email'];
                $tableau[$count]['proprietaire'] = $retour['proprietaire'];
                $tableau[$count]['frequence'] = $retour['frequence'];
                $tableau[$count]['annuaire'] = $retour['annuaire'];
                $tableau[$count]['consignes'] = $retour['consignes'];
                $tableau[$count]['envoyer'] = $retour['envoyer'];
                $tableau[$count]['sendTime'] = $retour['sendTime'];
                $tableau[$count]['adminApprouve'] = $retour['adminApprouve'];
                $tableau[$count]['adminApprouveTime'] = $retour['adminApprouveTime'];
                $tableau[$count]['adminRaison'] = $retour['adminRaison'];
                $tableau[$count]['over'] = $retour['over'];
                $tableau[$count]['overTime'] = $retour['overTime'];
                $tableau[$count]['affectedTO'] = $retour['affectedTO'];
                $tableau[$count]['affectedBY'] = $retour['affectedBY'];
                $tableau[$count]['affectedTime'] = $retour['affectedTime'];
                $tableau[$count]['showProprio'] = $retour['showProprio'];
                $tableau[$count]['budget'] = $retour['budget'];
                $tableau[$count]['retard'] = $jourRetarder;
                $count++;
            }
        }

        return $tableau;
    }

    public static function getLiaisonByID($idSite) {
        global $dbh;

        $tableau = array();

        $requete = "SELECT * FROM projets WHERE parent ='" . $idSite . "' AND id=" . $idSite;


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['parent'] = $retour['parent'];
            $tableau['affectedSec'] = $retour['categories'];
            $tableau['categorie'] = "";
            $tableau['lien'] = $retour['lien'];
            $tableau['email'] = $retour['email'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['frequence'] = $retour['frequence'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['envoyer'] = $retour['envoyer'];
            $tableau['adminApprouve'] = $retour['adminApprouve'];
            $tableau['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau['over'] = $retour['over'];
            $tableau['overTime'] = $retour['overTime'];
            $tableau['affectedTO'] = $retour['affectedTO'];
            $tableau['affectedBY'] = $retour['affectedBY'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['showProprio'] = $retour['showProprio'];
            $tableau['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getRestartedSoumissionsByProcject($idSite = 0, $idAnnuaire = 0) {
        global $dbh;

        $tableau = array();

        $requete = 'SELECT * FROM restartprojects ORDER BY id ASC';
        if ($idSite > 0) {
            $requete = 'SELECT * FROM restartprojects WHERE project_id = "' . $idSite . '"';
        }
        if ($idSite > 0 && $idAnnuaire > 0) {
            $requete = 'SELECT * FROM restartprojects WHERE project_id = "' . $idSite . '" AND annuaire_id = "' . $idAnnuaire . '"';
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['project_id'] . $retour['annuaire_id']] = $retour;
            $count++;
        }

        return $tableau;
    }

    public static function getAllSoumissionsAge($idSite = 0, $idAnnuaire = 0, $idutilisateur = 0) {
        global $dbh;

        $tableau = array();

        $requete = 'SELECT * FROM soumissionsage ORDER BY id ASC';
        if ($idSite > 0) {
            $requete = 'SELECT age FROM soumissionsage WHERE projet_id = "' . $idSite . '"';
        }
        if ($idAnnuaire > 0) {
            $requete = 'SELECT * FROM soumissionsage WHERE annuaire_id = "' . $idAnnuaire . '"';
        }
        if ($idutilisateur > 0) {
            $requete = 'SELECT * FROM soumissionsage WHERE referenceur_id = "' . $idutilisateur . '"';
        }
        if ($idSite > 0 && $idAnnuaire > 0) {
            $requete = 'SELECT * FROM soumissionsage WHERE project_id = "' . $idSite . '" AND annuaire_id = "' . $idAnnuaire . '"';
        }
        if ($idSite > 0 && $idAnnuaire > 0 && $idutilisateur > 0) {
            $requete = 'SELECT * FROM soumissionsage WHERE project_id = "' . $idSite . '" AND annuaire_id = "' . $idAnnuaire . '" AND referenceur_id = "' . $idutilisateur . '"';
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$retour['projet_id'] . $retour['annuaire_id']] = $retour;
            $count++;
        }

        return $tableau;
    }

    public static function getCommandeByID($idSite) {
        global $dbh;

        $tableau = array();

        $requete = "SELECT * FROM projets WHERE id=" . $idSite;


        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['id'] = $retour['id'];
            $tableau['parent'] = $retour['parent'];
            $tableau['affectedSec'] = $retour['categories'];
            $tableau['categorie'] = "";
            $tableau['lien'] = $retour['lien'];
            $tableau['email'] = $retour['email'];
            $tableau['proprietaire'] = $retour['proprietaire'];
            $tableau['frequence'] = $retour['frequence'];
            $tableau['annuaire'] = $retour['annuaire'];
            $tableau['consignes'] = $retour['consignes'];
            $tableau['ancres'] = $retour['ancres'];
            $tableau['sendTime'] = $retour['sendTime'];
            $tableau['envoyer'] = $retour['envoyer'];
            $tableau['adminRaison'] = $retour['adminRaison'];
            $tableau['adminApprouve'] = $retour['adminApprouve'];
            $tableau['adminApprouveTime'] = $retour['adminApprouveTime'];
            $tableau['over'] = $retour['over'];
            $tableau['overTime'] = $retour['overTime'];
            $tableau['affectedTO'] = $retour['affectedTO'];
            $tableau['affectedBY'] = $retour['affectedBY'];
            $tableau['affectedTime'] = $retour['affectedTime'];
            $tableau['showProprio'] = $retour['showProprio'];
            $tableau['budget'] = $retour['budget'];
            $count++;
        }

        return $tableau;
    }

    public static function getAllTranslate() {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM traductions";
        $execution = $dbh->query($requete);
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau['fr'][$retour['mot_fr']] = ucfirst($retour['mot_fr']);
            $tableau['en'][$retour['mot_fr']] = ucfirst($retour['mot_en']);
        }

        return $tableau;
    }

    public static function getPub($id = 1) {
        
    }

    public static function getSocialIcons() {
        $socialData = Functions::getAllParameters("", "", $group = "social");
        $count = count($socialData);
        $start = 1;
        $social = "";
        foreach ($socialData as $key => $valeur) {
            if ($valeur['valeur'] == "") {
                $valeur['valeur'] = "#";
            } else {
                $social .= '<li><a href="' . $valeur['valeur'] . '" target="_blank"><i class="icon-social-' . $key . '"></i></a></li>';
            }
            $start++;
        }
        return $social;
    }

    public static function getParameter($parameterName) {
        global $dbh;

        $tableau = array();
        $toTranslate = strtolower($toTranslate);
        $requete = "SELECT * FROM parametres WHERE parametre'" . $parameterName . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
//        $tableau[$retour['parametre']] = $retour['valeur'];
        return $retour['valeur'];
    }

    public static function getLogo($parametre = "", $position = "top", $style = "") {

        if ($position == "top") {
            $logo = Functions::getAllParameters("logo", "valeur", "");
        } else {
            $logo = Functions::getAllParameters("logofooter", "valeur", "");
        }
        $logotext = Functions::getAllParameters("logotext", "valeur", "");
        $titre = Functions::getAllParameters("titre", "valeur", "");

        if (trim($logo['valeur']) != "") {
            return '<a href="./"" id="logo" title="' . $logotext['valeur'] . '"><img src="' . $logo['valeur'] . '" alt="' . $titre['valeur'] . '" height="50" title="' . $logotext['valeur'] . '"/></a>';
        } else {
            return '<a href="./"" id="logo" title="' . $logotext['valeur'] . '"><h1 class="logo">' . $logotext['valeur'] . '</h1></a>';
        }
    }

    public static function getAllParameters($name = "", $champ = "", $group = "") {
        global $dbh;
        $requete = "SELECT * FROM parametres";

        if ($name != "") {

            if ($champ == "") {
                $champ = "*";
            }
            $requete = "SELECT " . $champ . " FROM parametres WHERE name='" . $name . "'";
        }

        if ($group != "") {
            $requete = "SELECT * FROM parametres WHERE groupe='" . $group . "'";
        }

        $tableau = array();

        $execution = $dbh->query($requete);
        if ($name == "" || $group != "") {
            while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
                $tableau[$retour['name']]["id"] = $retour['id'];
                $tableau[$retour['name']]["nom"] = $retour['name'];
                $tableau[$retour['name']]["description"] = $retour['description'];
                $tableau[$retour['name']]["libelle"] = $retour['libelle'];
                $tableau[$retour['name']]["valeur"] = $retour['valeur'];
            }
        } else {
            $retour = $execution->fetch();
            if ($champ == "") {
                $tableau["id"] = $retour['id'];
                $tableau["nom"] = $retour['name'];
                $tableau["description"] = $retour['description'];
                $tableau["libelle"] = $retour['libelle'];
                $tableau["valeur"] = $retour['valeur'];
            } else {
                $tableau[$champ] = $retour[$champ];
            }
        }
        return $tableau;
    }

    public static function getPageInfos($id = 1) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM pages WHERE id=" . $id;
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau["id"] = $retour['id'];
        $tableau["menu"] = $retour['menu'];
        $tableau["titre"] = $retour['titre'];
        $tableau["post"] = $retour['post'];
        $tableau["url"] = $retour['url'];
        $tableau["scriptname"] = $retour['scriptname'];
        $tableau["url"] = $retour['url'];
        $tableau["created"] = $retour['created'];

        return $tableau;
    }

    public static function getDevises($id = 1) {
        global $dbh;
        $requete = "SELECT * FROM devises WHERE active= 1 AND id=" . intval($id);
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        $tableau["id"] = $retour['id'];
        $tableau["libelle"] = $retour['libelle'];
        $tableau["code"] = $retour['code'];
        $tableau["code"] = $retour['code'];
        $tableau["taux"] = $retour['taux'];
        $tableau["symbole"] = $retour['symbole'];
        return $tableau;
    }

    public static function getPost() {
        global $dbh;
        $tableau = array();
        $requete = "SELECT * FROM post WHERE active= 1 ORDER BY createdTime DESC";
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]["id"] = $retour['id'];
            $tableau[$count]["categorie"] = $retour['categorie'];
            $tableau[$count]["attachment"] = $retour['attachement'];
            $tableau[$count]["titre"] = $retour['titre'];
            $tableau[$count]["content"] = $retour['content'];
            $tableau[$count]["keywords"] = $retour['keywords'];
            $tableau[$count]["createdTime"] = $retour['createdTime'];
            $tableau[$count]["active"] = $retour['active'];
            $tableau[$count]["created"] = $retour['created'];
            $count++;
        }
        return $tableau;
    }

    public static function getAttachement($id = "") {
        global $dbh;
        $tableau = array();
        if ($id == "") {
            $requete = "SELECT * FROM attachment created DESC";
        } else {
            $requete = "SELECT * FROM attachment created DESC";
        }
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau[$count]["id"] = $retour['id'];
//            $tableau[$count]["categorie"] = $retour['categorie'];
//            $tableau[$count]["attachment"] = $retour['attachment'];
//            $tableau[$count]["titre"] = $retour['titre'];
//            $tableau[$count]["content"] = $retour['content'];
//            $tableau[$count]["keywords"] = $retour['keywords'];
//            $tableau[$count]["createdTime"] = $retour['createdTime'];
//            $tableau[$count]["active"] = $retour['active'];
//            $tableau[$count]["created"] = $retour['created'];
            $count++;
        }

        return $tableau;
    }

    public static function getUserType($id = 0) {
        global $dbh;

        $tableau = array();
        $requete = "SELECT * FROM typeutilisateur WHERE id=" . $id;
        $execution = $dbh->query($requete);
        $count = 1;
        while ($retour = $execution->fetch(PDO::FETCH_ASSOC)) {
            $tableau["id"] = $retour['id'];
            $tableau["parent"] = $retour['typeuserparent'];
            $tableau["userid"] = $retour['typeuserid'];
            $tableau["code"] = $retour['code'];
            $tableau["libelle"] = $retour['libelle'];
            $tableau["codepostal"] = $retour['codepostal'];
            $count++;
        }

        return $tableau;
    }

    public static function Pagination($count = 0, $page = 1) {

        $nbre_result = $count;

        if ($nbre_result > pagination) {

            $nb_pag = ceil($nbre_result / pagination);
            $st = 1;


            if ($page > 1) {
                $lien_page_precedent = $_SERVER['REQUEST_URI'] . "?page=" . ($page - 1);
            } else {
                $lien_page_precedent = "#";
            }

            $list_page = '<nav class="pagination"><ul>';
            $list_page.='<li class="spe2"><a href="' . $lien_page_precedent . '" >� Pr�c�dent</a></li>';

            if ($nb_pag < 12) {
                while ($st < $nb_pag + 1) {
                    if ($st == $page) {
                        $current = "current";
                    } else {
                        $current = "";
                    }
                    $list_page.='<li><a href="' . $_SERVER['REQUEST_URI'] . '?page=' . $st . '" class="' . $current . '">' . $st . '</a></li>';
                    $st++;
                }
            } else {
                $nbr_display = $config['limit_....'];

                if (($page + $nbr_display) < ($nb_pag - $nbr_display + 1)) {
                    $st = $page;
                } else {
                    $st = $nb_pag - $nbr_display - $nbr_display + 1;
                }

                if ($st > 1) {
                    $list_page.='...';
                }

                $std = $st + $nbr_display;
                $st_save = 0;

                while ($st < $std) {
                    $lien_page = $lien_pages;
                    $lien_page.=$_SERVER['REQUEST_URI'] . "?page=" . $st;
                    if ($st == $page) {
                        $current = "current";
                    } else {
                        $current = "";
                    }
                    $list_page.='<li><a href="' . $lien_page . '" class="' . $current . '">' . $st . '</a></li>';
                    $st++;
                    $st_save = $st;
                }

                if (($nb_pag - $nbr_display) > ($st_save)) {
                    $list_page.='...';
                }

                $st = $nb_pag - $nbr_display + 1;

                while ($st < $nb_pag + 1) {
                    $lien_page = $lien_pages;
                    $lien_page.=$_SERVER['REQUEST_URI'] . "?page=" . $st;
                    if ($st == $page) {
                        $current = "current";
                    } else {
                        $current = "";
                    }
                    $list_page.='<li><a href="' . $lien_page . '" class="' . $current . '">' . $st . '</a></li>';
                    $st++;
                }
            }

            if ($page < $nb_pag) {
                $lien_page_suivant = $_SERVER['REQUEST_URI'] . "?page=" . ($page + 1);
            } else {
                $lien_page_suivant = "#";
            }

            $list_page.='<li><a href="' . $lien_page_suivant . '" >Suivant �</a></li>';
            $list_page.="</ul><div class='clearfix'></div></nav>";
        }



        if ($nbre_result > pagination) {
            $limit2 = $page * pagination;
        } else {
            $limit2 = $nbre_result;
        }

        if ($page == 1) {
            $limit1 = 1;
        } else {
            $limit1 = ($limit2 - pagination) + 1;
        }

        $return[] = $limit1;
        $return[] = $limit2;
        $return[] = $list_page;
    }

    public static function userSatut($id = 0) {
        if ($id == 1) {
            return "isSu";
        } else if ($id == 2) {
            return "isAdmin";
        } else if ($id == 4) {
            return "isWebmaster";
        } else if ($id == 3) {
            return "isReferer";
        } else {
            return "isUser";
        }
    }

    static function decodage($filter, $str) {
        $filter = sha1($filter);
        $letter = -1;
        $newstr = '';
        $str = base64_decode($str);
        $strlen = strlen($str);
        for ($i = 0; $i < $strlen; $i++) {
            $letter++;
            if ($letter > 31) {
                $letter = 0;
            }
            $neword = ord($str{$i}) - ord($filter{$letter});
            if ($neword < 1) {
                $neword += 256;
            }
            $newstr .= chr($neword);
        }
        return $newstr;
    }

    static function encodage($filter, $str) {
        $filter = sha1($filter);
        $letter = -1;
        $newpass = '';
        $newstr = "";
        $strlen = strlen($str);
        for ($i = 0; $i < $strlen; $i++) {
            $letter++;
            if ($letter > 31) {
                $letter = 0;
            }
            $neword = ord($str{$i}) + ord($filter{$letter});
            if ($neword > 255) {
                $neword -= 256;
            }
            $newstr .= chr($neword);
        }
        return base64_encode($newstr);
    }

    static function personalEncodage($str) {
        return sha1(md5(md5(sha1(base64_encode($str)))));
    }

}

function isSu($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isSu") {
        return true;
    } else {
        return false;
    }
}

function isSuperReferer($id = 0) {
    global $idUser;
    global $idTypeUser;
    if ($id == 0) {
        $id = $idUser;
    }
    $infos = Functions::getUserInfos($id);
    if ($infos && $infos['lastIP'] == 111111 && isReferer($idTypeUser)) {
        return true;
    } else {
        return false;
    }
}

function isAdmin($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isAdmin") {
        return true;
    } else {
        return false;
    }
}

function isWebmaster($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isWebmaster") {
        return true;
    } else {
        return false;
    }
}

function isReferer($id = 0) {
    global $idTypeUser;
    if ($id == 0) {
        $id = $idTypeUser;
    }

    if (Functions::userSatut($id) == "isReferer") {
        return true;
    } else {
        return false;
    }
}

function refererCanAsk($send = 0) {
    global $idUser;
    global $limitepaiement;
    global $dbh;
    if (!refererHavePay()) {
        $requete = "SELECT lastPayment FROM utilisateurs WHERE id='" . $idUser . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
        if (isset($retour['lastPayment'])) {
            $difference = time() - $retour['lastPayment'];
            if ($difference >= ($limitepaiement * 24 * 3600)) {
                return true;
            } else {
                if ($send == 1) {
                    $attente = 0;
                    $attente = ((($limitepaiement * 24 * 3600) - $difference) / (3600 * 24));
                    $attente = ceil($attente);
                    return $attente;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function refererHavePay() {
    global $idUser;
    global $idTypeUser;
    global $dbh;

    $requete = "SELECT * FROM requestpayment WHERE requesterID ='" . $idUser . "' AND answer =0 AND type = '" . $idTypeUser . "' LIMIT 1";
    $execution = $dbh->query($requete);
    $retour = $execution->fetch();

    if (isset($retour['answer'])) {
        return true;
    } else {
        return false;
    }
}

function isRight($list) {
    global $dbh;
    global $idUser;
    global $idTypeUser;
    global $urlBase;

    $list = explode(",", $list);
    foreach ($list as $value) {
        if ($value != "") {
            if ($idTypeUser == intval($value)) {
//                $dbh->query('UPDATE utilisateurs SET active = "0" WHERE id=' . $idUser);
                exit();
                header('location: ' . $urlBase . 'logout.html');
            }
        }
    }
}

function isMultiple($content) {
    if (!empty($content) && preg_match('#\\n#', $content)) {
        return true;
    } else {
        return false;
    }
}

function isMultiple_($content) {
    if (preg_match("#" . PHP_EOL . "#", $content)) {
        $tableau = explode(PHP_EOL, $content);
        return count($tableau);
    } else {
        return false;
    }
}

function traiterArray() {
    
}

function isButton() {
    global $lienButton;
    global $texteButton;
    global $widthButton;
    global $lienButton2;
    global $texteButton2;
    global $widthButton2;

    if ($lienButton != "" && $texteButton != "") {
        $val = ' <p class="p_menu" style="width:' . $widthButton . ';text-align:center"><a href="' . $lienButton . '" class="a_menu button color round">' . $texteButton . '</a></p>';
        if ($texteButton2 && $lienButton2) {
            $val .= ' <p class="p_menu" style="width:' . $widthButton2 . ';text-align:center"><a href="' . $lienButton2 . '" class="a_menu button color round">' . $texteButton2 . '</a></p>';
        }
        return $val;
    }
}

function backup_tables__($host, $user, $pass, $name, $tables = '*', $adrDoc = "./") {
    
}

function backup_tables($host, $user, $pass, $name, $tables = '*', $adrDoc = "./") {

    $link = mysql_connect($host, $user, $pass);
    mysql_select_db($name, $link);

//get all of the tables
    if ($tables == '*') {
        $tables = array();
        $result = mysql_query('SHOW TABLES');
        while ($row = mysql_fetch_row($result)) {
            $tables[] = $row[0];
        }
    } else {
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

//cycle through
    foreach ($tables as $table) {
        $result = mysql_query('SELECT * FROM ' . $table);
        $num_fields = mysql_num_fields($result);

        $return.= 'DROP TABLE ' . $table . ';';
        $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
        $return.= "\n\n" . $row2[1] . ";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = mysql_fetch_row($result)) {
                $return.= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = ereg_replace("\n", "\\n", $row[$j]);
                    if (isset($row[$j])) {
                        $return.= '"' . $row[$j] . '"';
                    } else {
                        $return.= '""';
                    }
                    if ($j < ($num_fields - 1)) {
                        $return.= ',';
                    }
                }
                $return.= ");\n";
            }
        }
        $return.="\n\n\n";
    }

//save file
    $handle = fopen($adrDoc . 'dataBase-du-' . date("d.m.Y-\a-H\h.i\m\i\\n", time()) . '', 'w+');
    fwrite($handle, $return);
    fclose($handle);

    $monfichier = fopen($adrDoc . 'timer.php', 'w+');
    fseek($monfichier, 0);
    fputs($monfichier, '<?PHP $timerBAckup = "' . time() . '"; ?>');
    fclose($monfichier);
}

//echo strtotime('09-09-2015')."*";
//echo strtotime('2015-09-09');
?>
