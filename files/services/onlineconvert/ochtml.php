<? 
if($_GET['key']=="OIZfionef5ef5651ze54gEzeg845"){
require_once __DIR__ . '/vendor/autoload.php';

$config = new \OnlineConvert\Configuration();
$config->setApiKey('main', '59923f53efe50ecd1efbc4b1fda8c4c1');

// Remember to specify your own downloads folder.
$config->downloadFolder = __DIR__ . '/downloads';

$client = new \OnlineConvert\Client\OnlineConvertClient($config, 'main');
$syncApi = new \OnlineConvert\Api($client);
$outputEndpoint = $syncApi->getOutputEndpoint();

$syncJob = [
    'input' => [
        [
            'type' => \OnlineConvert\Endpoint\InputEndpoint::INPUT_TYPE_REMOTE,
            'source' => $_GET['file']
        ]
    ],
    'conversion' => [
        [
            'target' => 'html'
        ]
    ]
];

$job = $syncApi->postFullJob($syncJob)->getJobCreated();

// You will find the file/s in the downloads folder
$outputEndpoint->downloadOutputs($job);


//print_r($job);

$id=$job['output']['0']['id'];

$ts=$_GET['filename'];

$zipfile=$_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/".$ts.".zip";

if(is_file($zipfile)){

    //Unzip & count images
    $zip = new ZipArchive;
    if ($zip->open( $zipfile ) === TRUE) {
        $zip->extractTo($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/".$ts);
        $zip->close();
        
    } else {
       $errors[]='La décompression du document a recontré un problème.';
       $error++;
    }

    //move to right dir
    rename($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/".$ts."/".$ts.".html",$_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/".$ts."/index.html");
    rename($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/".$ts,$_SERVER['DOCUMENT_ROOT']."/files/docs/".$ts);

  

} else {

   
    rename($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/".$ts.".html",$_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id."/index.html");

    @rename($_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id,$_SERVER['DOCUMENT_ROOT']."/files/docs/".$ts);



}

$definitive = $_SERVER['DOCUMENT_ROOT']."/files/docs/".$ts."/index.html";

include( __DIR__."/../../../files/includes/simple_html_dom.php");

$ct=file_get_contents($definitive);

$html=str_get_html($ct);
$links=0;

foreach($html->find('a') as $element) {
 
    if($element->plaintext){
        $links++;
        $results['links_href'][$element->plaintext]=html_entity_decode($element->href);
    }

}

$results['links']=$links;
$results['plaintext']="";

$words=0;

foreach($html->find('p, h1, h2, h3, h4, h5, h6') as $element) {
    
    $el=trim($element->plaintext);

    if(!empty($el)){
        $ct=count(explode(" ",trim($element->plaintext)));
        $words+=$ct;
        $results['plaintext'].=trim($element->plaintext)." ";
    }

}

$results['words']=$words;

//clean

$dir=$_SERVER['DOCUMENT_ROOT']."/files/services/onlineconvert/downloads/".$id;

if(is_dir($dir)){
    $it = new RecursiveDirectoryIterator($dir);
    $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
    foreach($it as $file) {
        if ('.' === $file->getBasename() || '..' ===  $file->getBasename()) continue;
        if ($file->isDir()) rmdir($file->getPathname());
        else unlink($file->getPathname());
    }
    rmdir($dir);
}

echo serialize($results);

}
?>