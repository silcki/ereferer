<?php

	//test : http://eref.mediacell.fr/files/services/phpword/service/ereferer.php?file=files/docs/3ba527656ba4198d9d9b5e47f9e6cfea.docx&key=OIZfionef5ef5651ze54gEzeg845

	require_once __DIR__ . '/../bootstrap.php';

	use PhpOffice\PhpWord\Settings;

if($_GET['key']=="OIZfionef5ef5651ze54gEzeg845"){
	date_default_timezone_set('UTC');
	error_reporting(E_ALL);
	define('CLI', (PHP_SAPI == 'cli') ? true : false);
	define('EOL', CLI ? PHP_EOL : '<br />');
	define('SCRIPT_FILENAME', basename($_SERVER['SCRIPT_FILENAME'], '.php'));
	define('IS_INDEX', SCRIPT_FILENAME == 'index');

	Settings::loadConfig();

	// Set writers
	$writers = array( 'HTML' => 'html');

	// Turn output escaping on
	Settings::setOutputEscapingEnabled(true);

	function write($phpWord, $filename, $writers)
	{
	    $result = '';

	    // Write documents
	    foreach ($writers as $format => $extension) {
	       // $result .= date('H:i:s') . " Write to {$format} format";
	        if (null !== $extension) {
	            $targetFile = __DIR__ . "/results/{$filename}.{$extension}";
	            $phpWord->save($targetFile, $format);
	        } else {
	            $result .= ' ... NOT DONE!';
	        }
	        //$result .= EOL;
	    }

	    //$result .= getEndingNotes($writers);

	    return $result;
	}

	// Read contents
	$name = basename(__FILE__, '.php');
	$source = __DIR__."/../../../../".$_GET['file'] ;

	//echo date('H:i:s'), " Reading contents from `{$source}`", EOL;
	$phpWord = \PhpOffice\PhpWord\IOFactory::load($source);

	// Save file
	write($phpWord, basename(__FILE__, '.php'), $writers);

	//open html
	include( __DIR__."/../../../../files/includes/simple_html_dom.php");

	$ct=file_get_contents(__DIR__ . "/results/ereferer.html");

	$html=str_get_html($ct);
	$links=0;

	foreach($html->find('a') as $element) {
		$links++;
		$results['links_href'][$element->plaintext]=html_entity_decode($element->href);
	}

	$results['links']=$links;
	$results['plaintext']="";
	$results['html']=$ct;
	
	$words=0;

	foreach($html->find('p') as $element) {
		if(!empty(trim($element->plaintext))){
			$ct=count(explode(" ",trim($element->plaintext)));
			$words+=$ct;
			$results['plaintext'].=trim($element->plaintext)." ";
		}
	}

	$results['words']=$words;
	echo serialize($results);


}