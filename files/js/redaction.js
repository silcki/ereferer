(function($){
    $(document).ready(function(){
        var ajax_url = 'files/includes/ajax/update.php';

        //*** Listing-Page. Webmaster drag-n-drop

        // set the same rows-heights for both table
        function synch_table_rows(){
            var table_dates_rows = $('.table_waiting tr').filter(':not(:eq(0))');
            var table_drag_rows = $('.table_draggable tr').filter(':not(:eq(0))');

            console.clear();
            $(table_drag_rows).each(function(i,item){
                // synch heights between tables
                drag_row = $(item);
                date_row = $(table_dates_rows).eq(i)
                $(date_row).height($(drag_row).height());

                // synch projects-data between two tables
                drag_project_id = $(drag_row).data('project_id');
                $(date_row).data('project_id',drag_project_id);

                //date_sent_time = $(date_row).find('.created_time').eq(0);
                //$(date_sent_time).html($(drag_row).data('created_time'));

                /*date_checkbox = $(date_row).find('input[type=checkbox]').eq(0);
                checkbox_id_name = 'check'+drag_project_id;
                $(date_checkbox).val(drag_project_id);
                $(date_checkbox).prop('id',checkbox_id_name);*/
            });

        }

        // after loading page => refresh heights
        synch_table_rows();

        // drag-n-drop
        var dnd_options = {
            sort: true,
            draggable: ".sortable-drag-item",
            ghostClass: "sortable-ghost",  // Class name for the drop placeholder
            chosenClass: "sortable-chosen",  // Class name for the chosen item
            dragClass: "sortable-drag",  // Class name for the dragging item

            onChoose: function (e) {
                e.oldIndex;  // element index within parent
            },

            onStart: function (e) {
                e.oldIndex;  // element index within parent
            },

            onEnd: function (e) {
                e.oldIndex;  // element's old index within parent
                e.newIndex;  // element's new index within parent
                synch_table_rows();
                save_estimated_dates_order(e.oldIndex,e.newIndex);
            },


            // Changed sorting within list
            onUpdate: function (/**Event*/evt) {
                var itemEl = evt.item;  // dragged HTMLElement
            },

            // Called by any change to the list (add / update / remove)
            onSort: function (/**Event*/evt) {

            },

            onMove: function (/**Event*/evt, /**Event*/originalEvent) {
                synch_table_rows();
            },
        };

        $("#tableClasses tbody").sortable(dnd_options);

        // save changing estimated dates
        // + update ids -
        function save_estimated_dates_order(old_index,new_index){
            if (old_index!=new_index) { // order has been changed
                // prevent multi sending ajax
                if ( $('.table_waiting').data('ajax_send') )
                    return false;

                var table_dates_rows = $('.table_waiting tr').filter(':not(:eq(0))');

                var sorted_projects = [];
                $.each(table_dates_rows,function(i,item){
                    project_id = $(item).data('project_id');
                    unix_time = $(item).data('created_time_unix');
                    project = {
                        id: project_id,
                        time : unix_time,
                    }
                    sorted_projects.push(project);
                });


                var post_data = {
                    type: 'redaction_projects_webmaster_start_dates',
                    projects : sorted_projects,
                };



                $('.table_waiting').data('ajax_send', true);
                $.post(ajax_url, post_data)
                    .done(function (response) {
                        console.log(response);
                    })
                    .fail(function (error) {
                        console.log(error);
                    })
                    .always(function(){
                        $('.table_waiting').data('ajax_send',false)
                    });

            }
        }


    });
})(jQuery);