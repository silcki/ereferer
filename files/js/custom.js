jQuery.noConflict()(function ($) {
    $(document).ready(function ($) {

        $("#tableClasses tr:even").css("background-color", "#F3F3F3");

        // TODO remove to dashboard
        $(".sortable-drag-item").css("background-color", "");
        $(".table_waiting tr").css("background-color", "");

        $(".displaySoumissions").click(function () {
            var rel = $(this).data('action');
            var completion = '';




            $(".displaySoumissions").removeClass('selectedListing');
            if (rel != "all") {
                completion = "." + rel;
            }
            $('#detailsTable tr.secondaireTR').fadeOut(10, function () {
                $('#detailsTable tr' + completion).fadeIn();
            });
            $(this).addClass('selectedListing');

        });


        $(".ancresDisplay").fancybox({
            'titlePosition': 'outside',
            'transitionIn': 'none',
            'transitionOut': 'none'
        });


        /*STICKY NAVIGATION*/
        var aboveHeight = $('#copyrights').outerHeight();
        $(window).scroll(function () {
            if ($(window).scrollTop() > aboveHeight && $(window).width() > 959) {
                $('#main-navigation.fixed').addClass('sticky');
            }
            else {
                $('#main-navigation.fixed').removeClass('sticky');
            }
        });
        /*STICKY NAVIGATION*/
        /*----------------------------------------------------------*/

        initAccordion();
        function initAccordion() {
            jQuery('.accordion-item').each(function (i) {
                var item = jQuery(this);
                item.find('.accordion-content').slideUp(0);
                item.find('.accordion-switch').click(function () {
                    var displ = item.find('.accordion-content').css('display');
                    item.closest('ul').find('.accordion-switch').each(function () {
                        var li = jQuery(this).closest('li');
                        li.find('.accordion-content').slideUp(300);
                        jQuery(this).parent().removeClass("selected");
                    });
                    if (displ == "block") {
                        item.find('.accordion-content').slideUp(300)
                        item.removeClass("selected");
                    } else {
                        item.find('.accordion-content').slideDown(300)
                        item.addClass("selected");
                    }
                });
            });
        }
        /*----------------------------------------------------------*/
        /*ISOTOPE JS */
        /*----------------------------------------------------------*/
        /*RESPONSIVE MAIN NAVIGATION STARTS*/
        var $menu_select = $("<select />");
        $("<option />", {"selected": "selected", "value": "", "text": "Main Navigation"}).appendTo($menu_select);
        $menu_select.appendTo("#main-navigation");
        $("#main-navigation ul li a").each(function () {
            var menu_url = $(this).attr("href");
            var menu_text = $(this).text();
            if ($(this).parents("li").length == 2) {
                menu_text = '- ' + menu_text;
            }
            if ($(this).parents("li").length == 3) {
                menu_text = "-- " + menu_text;
            }
            if ($(this).parents("li").length > 3) {
                menu_text = "--- " + menu_text;
            }
            $("<option />", {"value": menu_url, "text": menu_text}).appendTo($menu_select)
        })
        field_id = "#main-navigation select";
        $(field_id).change(function ()
        {
            value = $(this).attr('value');
            window.location = value;
        });
        /*RESPONSIVE MAIN NAVIGATION ENDS*/

        (function () {
            var $tabsNav = $('.tabs-nav'),
                    $tabsNavLis = $tabsNav.children('li'),
                    $tabContent = $('.tab-content');
            $tabContent.hide();
            $tabsNavLis.first().addClass('active').show();
            $tabContent.first().show();
            $tabsNavLis.on('click', function (e) {
                var $this = $(this);
                $tabsNavLis.removeClass('active');
                $this.addClass('active');
                $tabContent.hide();
                $($this.find('a').attr('href')).fadeIn(700);
                e.preventDefault();
            });
        })();
        $('ul.main-menu').superfish({
            delay: 100, // one second delay on mouseout 
            animation: {opacity: 'show', height: 'show'}, // fade-in and slide-down animation 
            speed: 'fast', // faster animation speed 
            autoArrows: false                           // disable generation of arrow mark-up 
        });
        (function () {

            var $carousel = $('#projects-carousel');
            if ($carousel.length) {

                var scrollCount;
                if ($(window).width() < 480) {
                    scrollCount = 1;
                } else if ($(window).width() < 768) {
                    scrollCount = 1;
                } else if ($(window).width() < 960) {
                    scrollCount = 3;
                } else {
                    scrollCount = 4;
                }

                $carousel.jcarousel({
                    animation: 600,
                    easing: 'easeOutCirc',
                    scroll: scrollCount
                });
            }

        })();
        /*INSTAGRAM PHOTOS FEEDS ENDS*/


        /*----------------------------------------------------------*/
        /*SKILLS BAR JS */
        /*----------------------------------------------------------*/
        if ($('.bar_graph').length && jQuery()) {
            function animateBar() {
                $('.bar_graph li').each(function (i) {
                    var percent = $(this).find('span').attr('data-width');
                    $(this).find('span').animate({
                        'width': percent + '%'
                    }, 1700, 'easeOutCirc');
                });
            }
            if ($('.bar_graph').length > 0) {
                animateBar();
                $(window).scroll(animateBar);
            }
        }


        /*----------------------------------------------------------*/
        /*FLEX SLIDER*/
        /*----------------------------------------------------------*/






        /*----------------------------------------------------------*/
        /*GOOGLE MAPS */
        /*----------------------------------------------------------*/


        /*$("a[data-rel^='prettyPhoto']").prettyPhoto({overlay_gallery: false});
         $("body").fitVids(); */

    });


    $(".okAnnuaireProjet").click(function () {
        var attr = $(this).attr("rel");
        var valueInput = $("#content" + attr).val();

        $(this).fadeOut(50, function () {
            $("#inputValue" + attr).html(valueInput);
            $("#Edit" + attr).fadeOut(10, function () {
                $("#Edited" + attr).fadeIn(10, function () {
                    $("#editBtn" + attr).fadeIn(10, function () {
                        $("#Alert" + attr).fadeOut();
                    });
                });
            });
        });

//        $(this).css("display", "none").hide();
    })


    $(".editAnnuaireProjet").click(function () {
        var attr = $(this).attr("rel");
        var valueInput = $("#content" + attr).val();

        $(this).fadeOut(50, function () {
            $("#Edited" + attr).fadeOut(10, function () {
                $("#Edit" + attr).fadeIn(10, function () {
                    $("#ok" + attr).fadeIn(10, function () {
                        $("#Alert" + attr).fadeIn();
                    });
                });
            });
        });


//        $("#Edit" + attr).css("display", "block").fadeIn();
    })

    $(".activeComment").click(function () {
        var attr = $(this).attr("rel");
        if ($(this).attr("data-title")) {
            var title = $(this).attr("data-title");
        } else {
            var title = "les commentaires";

        }
        var styler = $("#commentaireslide" + attr).css('display');
        if (styler == "none") {
            $("#commentaireslide" + attr).slideDown("slow");
            $(this).html("<i class='icon-eye-close'></i> Masquer " + title);
        }
        if (styler == "block") {
            $("#commentaireslide" + attr).slideUp("slow");
            $(this).html("<i class='icon-eye-open'></i> Afficher " + title);
        }
    });

    $(".activePlus").click(function () {
        var attr = $(this).attr("rel");
        var styler = $("#reduire" + attr).css('display');
        if (styler == "none") {
            $("#reduire" + attr).slideDown("slow");
            $(this).html("<i class='icon-eye-close'></i> Masquer les d�tails de ce projet");
        }
        if (styler == "block") {
            $("#reduire" + attr).slideUp("slow");
            $(this).html("<i class='icon-eye-open'></i> Afficher les d�tails de ce projet");
        }
    });

    // reading messages
    $(".Messagerie").click(function () {
        $(this).css({color: "inherit", "font-weight": "normal"});
        var idMessage = $(this).attr('id');
        $.post("files/includes/ajax/message.php", {id: idMessage}).done(function (data) {
//            alert("Data Loaded"+data);
        });
    });

    $(".message_subject").click(function () {
        var subject = $(this);
        var idMessage = $(subject).data('id');
        $.post("files/includes/ajax/message.php", {id: idMessage}).done(function (data) {
                   $(subject).children(':first-child').css({color: "inherit", "font-weight": "normal"});
        });
    });


    setTimeout(function () {
        $("#paypal_form_Auto").trigger("submit");
    }, 5000);

    $("#order").change(function () {
        $(location).attr('href', $("#order option:selected").val());
    });

    $("#refererLink").change(function () {
        $(location).attr('href', $("#refererLink option:selected").val());
    });

    $(".editRef").change(function () {
        var changing = $(this).val();
        var tampon = $(this).attr("rel");
        var idProjet = $(this).attr("data-idProjet");
        var d = new Date();
        var strDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();

        if (changing > 0) {
            $.post(
                    "files/includes/ajax/update.php",
                    {
                        affectedTOO: changing,
                        projet: idProjet,
                        type: "referenceur"
                    },
            function (data) {
                if (data != "1" && data.length > 10) {
//                    alert("ok");
                    $(this).attr("rel", changing);
                    $('span.refTOO' + idProjet).fadeOut("slow").html("").stop().html(data).fadeIn("slow");
                    $('span.statoo' + idProjet).fadeOut("slow").html("").stop().html("Nouveau r�f�renceur affect� avec succ�s.").fadeIn("slow");
                    $('span.affTOO' + idProjet).fadeOut("slow").html("").stop().html(strDate).fadeIn("slow");
                }
            });
        }
    });

    $(".redaction_reaffect").change(function () {
        var changing = $(this).val();
        var tampon = $(this).attr("rel");
        var idProjet = $(this).data("project_id");
        var d = new Date();
        var strDate = d.getDate() + "-" + (d.getMonth() + 1) + "-" + d.getFullYear();

        if (changing > 0) {
            $.post(
                "files/includes/ajax/update.php",
                {
                    affectedTOO: changing,
                    project_id: idProjet,
                    type: "redaction_referenceur"
                },
                function (data) {
                    if (data != "1" && data.length > 10) {
//                    alert("ok");
                        $(this).attr("rel", changing);
                        $('span.writer' + idProjet).fadeOut("slow").html("").stop().html(data).fadeIn("slow");
                        $('span.statoo' + idProjet).addClass('no_delay_days').fadeOut("slow").html("").stop().html("Nouveau r�f�renceur affect� avec succ�s.").fadeIn("slow");
                        //$('span.writer' + idProjet).fadeOut("slow").html("").stop().html(strDate).fadeIn("slow");
                    }
                });
        }
    });

    $("#paginationExpressButton").click(function () {
        var paginer = $("#paginationExpress").val();

        if (paginer > 0) {
            $.post(
                    "files/includes/ajax/update.php",
                    {
                        paginationExpress: paginer,
                        type: "pagination"
                    },
            function (data) {
                if (data == "1") {
                    location.reload();
                }
            });
        }
    });


    $(".tableAnnuaires .checkedLine").click(function () {
        var rel = $(this).val();
        var cases = $(".tableAnnuaires").find('#check' + rel + ':checkbox');
        if (this.checked) {
            cases.prop('checked', this.checked);
        } else {
            cases.prop('checked', this.checked);
        }
        calc_estimate();
    });

    $(".reponseNeed").change(function () { // bind a function to the change event
        if ($("#possouiCompte").is(":checked")) {
            $('#showUp').slideDown();
        } else {
            $('#showUp').slideUp();
        }

    });

    $(".checkboxs").click(function () { // bind a function to the change event
//        var total = $('#costTotal').text();
        var cost = $(this).attr("rel");
        var nbre = $('.checkboxs:checked').size();
        var total = nbre * cost;
        $('#costTotal').fadeOut(function () {
            $('#costTotal').text(total).fadeIn();
            $('#TotalAnnuaires').fadeOut(function () {
                $('#TotalAnnuaires').text(nbre).fadeIn();
            });
        });

    });


    $("#checkClasses").click(function () {
        var cases = $("#tableClasses").find(':checkbox');
        if (this.checked) {
            cases.prop('checked', this.checked);
            $(this).attr("title", "D�cocher Tout");
        } else {
            cases.prop('checked', this.checked);
            $(this).attr("title", "Cocher Tout");
        }

    });

    $("#checkkall").click(function () {
        var cases = $("#listConcernee").find(':checkbox');
        if (this.checked) {
            cases.prop('checked', this.checked);
            $(this).attr("title", "D�cocher Tous");
            $('.checkkall').text("D�cocher Tous");
        } else {
            cases.prop('checked', this.checked);
            $(this).attr("title", "Cocher Tous");
            $('.checkkall').text("Cocher Tous");

        }

    });

    // set writers to proeject page


    $("#is_fixed_writer").click(function(){
        if (this.checked){
            $(".writer_secondary").prop('disabled',true).trigger("chosen:updated");
            $(".writer_secondary").addClass('disabled');
        }else{
            $(".writer_secondary").prop('disabled',false).trigger("chosen:updated");
            $(".writer_secondary").removeClass('disabled');
        }
    });

    // set best writers
    $(".writer_secondary,.affected_select").chosen({
        inherit_select_classes: true
    });

    // check setting fixed-writer
    if ($("#is_fixed_writer").is(':checked')){
        $(".writer_secondary").prop('disabled',true).trigger("chosen:updated");
        $(".writer_secondary").addClass('disabled').trigger("chosen:updated");
    }


    $(".checkClasseses").click(function () {
        var rel = "";
        if ($(this).attr("rel") != "") {
            rel = $(this).attr("rel");
        }

        var cases = $("#tableClasses" + rel).find(':checkbox');
        if (this.checked) {
            cases.prop('checked', this.checked);
            $(this).attr("title", "D?cocher Tout");
        } else {
            cases.prop('checked', this.checked);
            $(this).attr("title", "Cocher Tout");
        }

        calc_estimate();
    });

    function calc_estimate() {
        var total_sum = 0;

        $(".annuaire input[type=checkbox]:checked").each(function () {
            var tarif_td = $(this).parent().parent().next().next().next();
            if (tarif_td.hasClass("tarifW")) {
                total_sum += parseFloat(tarif_td.text());
            }
        });
        total_sum = total_sum.toFixed(1);
        $(".estimation_total_sum").text(total_sum);
        console.log(' estimation = '+total_sum);
    }

    // vip-text explanation for annuaires
    $('.vip_text').on('mouseenter', function (e) {
        var notice = $(this).data("notice");
        var pos = $(this).position();
        $(".tooltip .tooltip_body").html(notice);
        $(".tooltip").css({top: pos.top + 25, left: pos.left + 10}).fadeIn('fast');
    }).on('mouseleave', function (e) {
        $(".tooltip").hide();
    });

    $(".myTooltips").on('mouseenter', function (e) {
        var pos = $(this).position();
        var divName = $(this).data('action');
        $("#" + divName).css({top: pos.top + 30, left: pos.left - 110}).fadeIn('fast');
    }).on('mouseleave', function (e) {
        var divName = $(this).data('action');
        $("#" + divName).hide();
    });

    $("#statut_what_is").on('mouseenter', function (e) {
        var pos = $(this).position();
        $("#tooltip_status").css({top: pos.top + 30, left: pos.left - 110}).fadeIn('fast');
    }).on('mouseleave', function (e) {
        $("#tooltip_status").hide();
    });



    $(".statut_what_is_proposee").on('mouseenter', function (e) {
        var pos = $(this).position();
        $(".tooltip_status_backlinks").css({top: pos.top + 30, left: pos.left - 110}).fadeIn('fast');
    }).on('mouseleave', function (e) {
        $(".tooltip_status_backlinks").hide();
    });

    $("[data-type=cost_tooltip]").on('mouseenter', function (e) {
        var elem = $(this).data("required");
        var pos = $(this).position();
        $("#" + elem).css({top: pos.top + 25, left: pos.left + 10}).fadeIn('fast');
    }).on('mouseleave', function (e) {
        var elem = $(this).data("required");
        $("#" + elem).hide();
    });


    $('#datedemarrage').datepick({
        dateFormat: 'dd-mm-yyyy'
    });

    $('.specialdatedemarrage').datepick({
        dateFormat: 'dd-mm-yyyy'
    });

    $('.specialdatedemarrage').click(function () {
//        var thisClass = $(this).attr('class');
        $(this).datepick({
            dateFormat: 'dd-mm-yyyy'
        });
    });

    $(".buttonSend").click(function () {
        var idProjet = $(this).attr('rel');
        var ValCurrent = $("#specialdatedemarrage" + idProjet).val();
        $.post(
                "files/includes/ajax/update.php",
                {
                    datedemarrage: ValCurrent,
                    projet: idProjet,
                    type: "datedemarrage"
                },
        function (data) {
//            alert("'" + ValCurrent + "'");
            if (data == "1") {
                $(this).val(ValCurrent);
            } else {
                $(this).val(data);
                alert("Erreur de s�lection de la date. R�essayez svp")
            }
        });
    });

    // bugfix ?!s
    $("#bottom_save_list").click(function (e) {
        e.preventDefault();
        $("#top_save_list").click();
    });

    calc_estimate();


    /*
      Page : /compte/accueil.html
      Update project state-buttons
     */


    // global-button update all project
    $("#global_update_projects_states").on('click',function(e){
        var form = $("#superForm");

        if (confirm('En �tes-vous s�r?')){
            var href = window.location.href;

            $(form).prop('action',href);
            $(form).append('<input type="hidden" name="update_projects_states" value="1" />');
            $(form).submit();
        }

    });


    $(".project_update_state").click(function (e) {
        e.preventDefault();

        var parent_row = $(this).parent().parent(); // <tr>....</tr>
        var project_id = $(this).data('project_id');
        var user_id = $(this).data('user_id');
        var is_admin = $(this).data('is_admin');
        var is_writer = $(this).data('is_writer');

        var user_type = is_admin? 'admin': 'writer';

        var admin_messages = {};



        $.post(
            "files/includes/ajax/update.php",
            {
                type: "project_update_current_state",
                project_id: project_id,
                user_id: user_id,
                user_type: user_type
            },
            function (data) {
                 //console.log(data.new_writer_id);

                 // processing for writers
                 if (is_writer){
                    //console.log('Writer');
                    //console.log(data);

                     // if project was reaffected - hide from author
                    if (data.status && ( (data.status == 'reaffection') || (data.status=='project_finished') )){
                        $(parent_row).hide('slow');
                    }

                    if (data.today_task_count > 0){
                        //console.log(data.today_task_count);
                        $(parent_row).find('.project_update_today_task').text(data.today_task_count);
                        $(parent_row).find('.project_update_writer_task_status').html(data.writer_task_status);
                    }

                    if ( data.today_task_count == 0){
                        $(parent_row).hide('slow');
                    }

                    var tasks_rows = $('#tableClasses tr').filter(':not(:eq(0))');

                    //if ( tasks_rows_cnt == 0 ){
                       // $('.simple-table').hide();
                    //}
                 }

                /*
                // processing for admin
                if (is_admin){
                    console.log('Admin');
                    console.log(data);


                    // if project was reaffected - hide from author
                    if (data.status && data.status == 'reaffection'){
                        $(parent_row).find('.refTOO'+project_id).html(data.admin_new_writer_link);
                        $(parent_row).find('select[name=affectedTOO]').val(data.new_writer_id).trigger("chosen:updated");
                    }
                }*/

            });

    });




});



function ve(champ)
{
    var chiffres = new RegExp("[0-9]"); /* Modifier pour : var chiffres = new RegExp("[0-9]"); */
    var verif;
    var points = 0; /* Supprimer cette ligne */

    for (x = 0; x < champ.value.length; x++)
    {
        verif = chiffres.test(champ.value.charAt(x));
        if (champ.value.charAt(x) == ".") {
            points++;
        } /* Supprimer cette ligne */
        if (points > 1) {
            verif = false;
            points = 1;
        } /* Supprimer cette ligne */
        if (verif == false) {
            champ.value = champ.value.substr(0, x) + champ.value.substr(x + 1, champ.value.length - x + 1);
            x--;
        }
    }

}


function ve1(champ)
{
    var chiffres = new RegExp("[0-9\.,]"); /* Modifier pour : var chiffres = new RegExp("[0-9]"); */
    var verif;
    var points = 0; /* Supprimer cette ligne */

    for (x = 0; x < champ.value.length; x++)
    {
        verif = chiffres.test(champ.value.charAt(x));
        if ( (champ.value.charAt(x) == ".") || (champ.value.charAt(x) == ",") ) {
            points++;
        }
         /* Supprimer cette ligne */
        if (points > 1) {
            verif = false;
            points = 1;
        } /* Supprimer cette ligne */
        if (verif == false) {
            champ.value = champ.value.substr(0, x) + champ.value.substr(x + 1, champ.value.length - x + 1);
            x--;
        }
    }

}

function Paiementconfirmation()
{
    var msg = "Avez vous d�ja effectu� ce paiement ?";
    if (confirm(msg))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function confirmation()
{
    var msg = "�tes vous s�r de vouloir effectuer cette action ?";
    if (confirm(msg))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function  confirmationReferer()
{
    var msg = "�tes vous s�r d'avoir d�j� effectu� cette t�che?";
    if (confirm(msg))
    {
        return true;
    }
    else
    {
        return false;
    }
}
function  confirmationRefererer()
{
    var msg = "�tes vous s�r de vouloir effectuer cette action ?";
    if (confirm(msg))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function  confirmationReferererNo()
{
    var msg = "�tes vous s�r de ne pas pouvoir effectuer cette t�che ?";
    if (confirm(msg))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function  confirmationAccepter()
{
    var msg = "�tes vous s�r de vouloir accepter le projet?";
    if (confirm(msg))
    {
        return true;
    }
    else
    {
        return false;
    }
}