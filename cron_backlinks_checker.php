<?php

// var_dump('Checker');
// Cron Backlinks Checker
// @voodoo417

//error_reporting(E_ALL);

$document_root = dirname(__FILE__);

// mailing manager
require_once $document_root.'/files/includes/Classes/MailProvider.php';

session_start();

var_dump(ini_get('max_execution_time'));
var_dump(ini_get('memory_limit'));

ini_set('memory_limit', '1024M');
var_dump(ini_get('memory_limit'));

set_time_limit(0);
//error_reporting(E_ALL);

require_once('files/includes/config/dbconfig.php');


// log open
$fp = fopen(dirname(__FILE__).'/cron_backlinks.log', 'a+');
if ($fp) {
    $time = date("Y-m-d H:i:s");
    fwrite($fp, __FILE__ . ' running at ' . $time . '. Process ID = ' . getmypid() . PHP_EOL);
    fclose($fp);
}


//var_dump('unset_running'); unset($_SESSION['checker_running']);die;

$checker = new Checker();
//var_dump($checker->getDomains());die;

if (empty($_SESSION['checker_running'])) {
    echo 'Checker.Running<br/>';
    $_SESSION['checker_running'] = true;
    $checker->run();
} else {
    echo 'Already running<br/>';
    die;
}

class Checker {

    private $dbh;
    private $find = '';
    private $monlien = '';
    private $monlien_id = '';
    private $multiple = '';
    private $multiple_urls = '';
    private $multiple_urls_current = '';
    private $domains = array();
    private $curl_timeout_limit = 0;

    function __construct() {
        try {
            $this->dbh = new PDO(DB_TYPE . ':host=' . DB_SERVER . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbh->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
        } catch (PDOException $e) {
            echo "<p>Erreur :" . $e->getMessage() . ": veuillez re&eacutessayez plus tard</p>";
            die;
        }
    }

    function getDomains() {
        $sql = "SELECT *
                FROM annuaires
                WHERE importedBy ='0' AND active = 1
                ORDER BY id ASC";

        return $this->dbh->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    function getBacklinks($domain_id) {
        $sql = "SELECT j.siteId AS `id`,
                       p.lien AS `lien`
                FROM jobs AS j
                INNER JOIN projets AS p
                ON ( p.id = j.siteID )
                LEFT JOIN annuaires2backlinks AS a2b
                ON ( a2b.site_id = j.siteID AND j.annuaireID = a2b.annuaire_id )
                WHERE j.annuaireID = " . $domain_id . "
                AND IFNULL(a2b.backlink,'') = '' AND  status_type <>'manually' ";


        return $this->dbh->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }

    function run() {
        $domains = $this->getDomains();
        $this->domains = $domains;




        $i = 0;
        // loop all domains
        $start = 0;

        //__debug($domains);

        /*
          $problems = array(
          "http://www.webrankinfo.com",
          "http://www.1two.org/",
          "http://www.gralon.net",
          "http://www.dechiffre.fr/annuaire/",
          "http://www.francite.com",
          "http://fr.webmaster-rank.info/",  // FIXED
          "http://prosduweb.fr/"
          ); */

        foreach ($domains as $domain) {
            $domain_url = !empty($domain['nddcible']) ? $domain['nddcible'] : $domain['annuaire'];
            // echo $domain_url.'<br/>';
            //if ($domain_url!='http://www.carnetduweb.info/nouveautes.html')
            //var_dump($domain_url);


            $domain_url = str_replace('http://', '', $domain_url);
            $domain_url = str_replace('www.', '', $domain_url);

            /*
              $problem_found = false;
              foreach($problems as $index=>$problem){
              $problem = str_replace('http://', '', $problem);
              $problem = str_replace('www.', '', $problem);
              $problem = rtrim($problem,'/');

              if ( (strpos($domain_url,$problem) !== false )){
              echo $domain_url.'<br/>';
              unset($problems[$index]);
              $problem_found = true;
              break;
              }
              }

              if ($problem_found){
              echo $domain_url.'<br/>';
              }else{

              continue;
              } */

            $domain_id = $domain['id'];
            $page_count = $domain['page_count'];

            $backlinks = $this->getBacklinks($domain_id);

            $backlinks_count = count($backlinks);
            //var_dump($domain_id);
            var_dump($backlinks_count);

            //echo $domain_url.'<br/>';
            //$backlinks_count = 1;
            $this->multiple = true;

            if ($backlinks_count) {
                $this->prepareCheckDomain($domain_url);

                // check multiple checking
                $this->multiple = ($backlinks_count >= 1) ? true : false;
                if ($this->multiple) {
                    $this->setMultipleUrls($backlinks);
                } else {
                    $monlien = array_shift($backlinks); // get first-single
                    $this->monlien = $monlien['lien'];
                    $this->monlien_id = $monlien['id'];
                }


                var_dump($page_count);

                $iterator = 0;
                //$page_count = 20;
                $maxIteration = $page_count;


                $this->curl_timeout_limit = 0;
                //var_dump('Checking for nddcible '. $domain_url.' starting..');
                if ($this->checkSiteOnlineStatus($domain_url, 10)) {
                    while (($urlEnAnalyse = $this->findInterneLink()) !== '' && $iterator < $maxIteration) {
                        $iterator++;
                        $this->checkDomain($urlEnAnalyse, $domain_url, $domain_id, $page_count);

                        echo '<li style="font-size:10px;color:';
                        if ($this->find != '') {
                            echo '#46B525;';
                            if (!$this->multiple)
                                $iterator = $maxIteration + 1;
                            else {
                                if (count($this->multiple_urls) == 0) {
                                    break;
                                }
                            }
                        } else {
                            echo '#dd0000;';
                        }
                        echo '"> ' . $urlEnAnalyse;
                        if ($this->multiple && ($this->find != '')) { // backlink
                            echo '    Backlink for: <b>' . strtoupper($this->multiple_urls_current) . '</b>';
                        }
                        echo '</li>';

                        // break loop if all backlinks are checked
                        if ($this->multiple && empty($this->multiple_urls)) {
                            var_dump('Multiple checking: all are founded');
                            break;
                        }

                        if ($iterator > $maxIteration)
                            break;

                        echo $iterator . '<br/>';

                        if ($this->curl_timeout_limit > 100)//
                            break;
                    }
                }else {
                    var_dump($domain_url . ' isn`t available');
                }
                //var_dump('Checking for nddcible '. $domain_url.' is ended');
                if (!empty($this->multiple_urls)) {
                    foreach ($this->multiple_urls as $site_id => $url) {
                        $this->setBacklinkStatus($site_id, $domain_id, '');
                    }
                    $this->multiple_urls = null;
                }
                echo '</ol>';

                echo '<br/>Total nddcibles =' . $i++ . '<br/>';
            }
        }


        unset($_SESSION['checker_running']);
    }

    function getHost($address) {
        if (!empty($address)) {
            $parseUrl = parse_url(trim($address));
            $host = '';
            if (!empty($parseUrl['host'])) {
                $host = $parseUrl['host'];
            } else {
                if (!empty($parseUrl['path'])) {
                    $host = array_shift(explode('/', $parseUrl['path'], 2));
                }
            }

            return str_replace('www.', '', $host);
        }
    }

    function prepareCheckingUrl($url) {
        $url = rtrim($url,'/');
        $clear_parts = array('http://','https://','www.');
        $url = str_replace($clear_parts,'',$url);
        return $url;
    }



    function getMainDomain($url) {
        return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    }

    function setMultipleUrls($items) {
        $this->multiple_urls = array();
        foreach ($items as $item) {
            $this->multiple_urls[$item['id']] = $item['lien'];
        }
        //$this->multiple_urls['11111111'] = 'http://google2.com';
    }

    // itegration
    // from analyseur
    // function runner($urlgo, $ndd) {
    function checkDomain($urlgo, $ndd, $domain_id, $page_count) {

        //$urlCnt = urldecode($urlgo);
        $urlCnt = $urlgo;

        $optionsCnt = array(
            CURLOPT_URL => $urlCnt,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
            CURLOPT_FAILONERROR => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CONNECTTIMEOUT => 1, // max 1 second for connection
            CURLOPT_TIMEOUT => 10,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13',
            CURLOPT_REFERER => 'http://www.google.fr/search?hl=fr&source=hp&q=' . $ndd . '&aq=f&aqi=g2&aql=&oq=',
        );
        $CURLCnt = curl_init();
        if (empty($CURLCnt)) {
            die("ERREUR curl_init : Il semble que cURL ne soit pas disponible.");
        }
        @curl_setopt_array($CURLCnt, $optionsCnt);
        $request_error = true;
        $attempts_count = 0;
        $max_attempts_cnt = 3; // max request-attempts

        $culr_time = microtime(true);

        while ($request_error) {
            $cnt = curl_exec($CURLCnt);
            if (curl_errno($CURLCnt)) {
                $attempts_count++;
                if ($attempts_count >= $max_attempts_cnt) {// check max request attempts
                    // may be write "total error"?
                    break;
                }

                $error_number = curl_errno($CURLCnt);
                if ($error_number == 28) {// CURLE_OPERATION_TIMEOUTED
                    $this->curl_timeout_limit++;
                }


                echo "<br/>Try again.Attempt #" . $attempts_count . '<br/>';
                echo "ERREUR curl_exec : " . curl_error($CURLCnt) . '. Url:' . $urlCnt;
                $request_error = true;
            } else {
                if ($attempts_count != 0) {
                    $attempts_count++;
                    echo "<br/>Try again.Attempt #" . $attempts_count . '<br/>';
                    echo "SUCCESS curl_exec. Url: " . $urlCnt;
                }
                break; // all fine
            }
        }

        //echo 'Curl. '.(microtime(true) - $culr_time).'<br>';

        $mimetype = curl_getinfo($CURLCnt, CURLINFO_CONTENT_TYPE); // get mitetype
        curl_close($CURLCnt);

        // fix rss feed (xml files)
        $rss_feed = false;
        if ($mimetype == 'text/xml') {
            $rss_links = $this->get_links_from_xml($cnt);
            if (!empty($rss_links)) {
                $rss_feed = true;
            }
        }

        $matches = array();
        $this->find = '';

        $dom_parsing = true;
        $dom = new DOMDocument;
        @$dom->loadHTML($cnt);

        $links = @$dom->getElementsByTagName('a');

        //__debug($links);
        //if (!$links){
        if (( preg_match_all("/<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU", $cnt, $matches))) {
            //__debug($matches[2]);
            $links = $matches[2];
            $dom_parsing = false;
        }
        // }
        //var_dump($links);

        $links_time = microtime(true);

        if (!empty($links)) {
            // set rss links
            if ($rss_feed) {
                $links = $rss_links;
            }

            if (is_array($links) && !empty($links)) {

                foreach ($links as $link_data) {
                    $urlpropre = '';

                    if ($dom_parsing && !$rss_feed)
                        $link = $link_data->getAttribute('href');
                    else
                        $link = $link_data;

                    $link = trim($link, "'");

                    // "old" code filter url for checking
                    if ('http://www.' . $ndd == substr($link, 0, (11 + strlen($ndd)))) {
                        $urlpropre = $link;
                    } else {
                        if ('http://' . $ndd == substr($link, 0, (7 + strlen($ndd)))) {
                            $urlpropre = $link;
                        } else {
                            if ('tel:' != substr($link, 0, 4) && 'https://' != substr($link, 0, 8) && 'http://' != substr($link, 0, 7) && 'javascript' != substr($link, 0, 10) && substr($link, 0, 1) != "'") {
                                $urlpropre = $link;
                                if (strlen($link) < 7 || 'http://' != substr($link, 0, 7)) {
                                    if ($link != '') {
                                        if (substr($link, 0, 1) != '/') {
                                            $urlpropre = 'http://www.' . str_replace('//', '/', $ndd . '/' . $link);
                                        } else {
                                            $urlpropre = 'http://www.' . str_replace('//', '/', $ndd . $link);
                                        }
                                    } else {
                                        $urlpropre = 'http://www.' . $ndd;
                                    }
                                }
                            }
                        }
                    }

                    if ($rss_feed) {
                        $urlpropre = $link;
                    }

                    // checking strange urls for some annuaires
                    if (empty($urlpropre)) {
                        $info = parse_url($ndd);
                        $path = $info['path'];

                        $path = explode('/', $path);
                        $path = $path[0];

                        if (strpos($link, $path) !== false) {
                            $urlpropre = $link;
                        }
                    }

                    //echo $link.'<br/>';
                    //echo $ndd.'<br/>';
                    //echo $path.'<br/>';
                    //echo $urlpropre.'<br/>';
                    // inserting urls for "parsing tree"
                    if ($urlpropre != '') {

                        $sql_total = "SELECT count(*) AS cnt  FROM bot_urls"; // echo $req . '<br/>';
                        $result = $this->dbh->query($sql_total);
                        $total_cnt = $result->fetchColumn(0); // get count of rows

                        if ($total_cnt <= $page_count) {

                            // fix: catching relative path
                            $path_pieces = explode('/', $ndd);
                            if ((count($path_pieces) > 1) && !$rss_feed) {
                                if (substr($link, 0, 1) == '/') {
                                    $host = array_shift($path_pieces);
                                    $urlpropre = 'http://www.' . str_replace('//', '/', $host . $link);
                                }
                            }

                            // check subdomain - removing `www.`
                            if (!empty($urlpropre) && !$rss_feed) {
                                $exploded = explode('.', $this->getHost($urlpropre));
                                if ((count($exploded) > 2)) {
                                    $urlpropre = str_replace('www.', '', $urlpropre);
                                }
                            }

                            $nb = 0;
                            $req = "SELECT count(1) nb FROM bot_urls WHERE url_analysee = '" . $urlpropre . "' "; // echo $req . '<br/>';
                            try {
                                $resultats2 = $this->dbh->query($req);
                            } catch (Exception $e) {
                                var_dump('Bad urlpropre:' . $urlpropre);
                            }

                            if ($resultats2) {
                                while ($resultat2 = $resultats2->fetch(PDO::FETCH_OBJ)) {
                                    $nb = intval($resultat2->nb);
                                }
                            }

                            // check if url is unique
                            if ($nb < 1) {
                                try {

                                    // bugfix related .html
                                    if (substr_count($urlpropre, '.html') > 1) {
                                        $urlpropre = str_replace('.html', '', $urlpropre);
                                        $urlpropre .= '.html';
                                    }

                                    // check domain name
                                    $urlpropre_host = $this->getHost($urlpropre);
                                    $ndd_host = $this->getHost($ndd);

                                    if (strpos($urlpropre_host, $ndd_host) !== false) {
                                        $req = "INSERT INTO `bot_urls` (`url_analysee`, `url_recherchee`, `est_traitee`) VALUES('" . $urlpropre . "', '', 0)";
                                        $this->dbh->exec($req);
                                    }
                                } catch (Exception $e) {
                                    var_dump('Bad urlpropre:' . $urlpropre);
                                }
                            }
                        }
                    }

                    // checking monlien/ single checking url
                    if (!$this->multiple) {
                        //$link_host = $this->getHost($link);
                        $link_host = $this->prepareCheckingUrl($link);

                        if ($link_host) {
                            //$backlink_host = $this->getHost($this->monlien);
                            $backlink_host = $this->prepareCheckingUrl($this->monlien);

                            if (strpos($link_host, $backlink_host) !== false) {
                                $this->find = $urlgo;
                                $this->setBacklinkStatus($this->monlien_id, $domain_id, $urlCnt);
                            }
                        }
                    } else { // multiple checking
                        //$link_host = $this->getHost($link);
                        $link_host = $this->prepareCheckingUrl($link);


                        if ($link_host) {

                            foreach ($this->multiple_urls as $site_id => $item) {
                                //$backlink_host = $this->getHost($item);
                                $backlink_host = $this->prepareCheckingUrl($item);

                                if (strpos($link_host, $backlink_host) !== false) {
                                    $this->find = $urlgo;
                                    $this->multiple_urls_current = $backlink_host;
                                    // remove found backlink from checking
                                    $this->setBacklinkStatus($site_id, $domain_id, $urlCnt);
                                    if (($key = array_search($item, $this->multiple_urls)) !== false) {
                                        unset($this->multiple_urls[$key]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        unset($dom);

        //echo 'Links . '.(microtime(true) - $links_time).'<br>';

        try {
            $this->dbh->exec("UPDATE `bot_urls` SET `est_traitee` = 1 WHERE `url_analysee` = '" . $urlgo . "' ");
        } catch (Exception $e) {
            var_dump('Bad urlgo:' . $urlgo);
        }
    }

    // check site availability
    function checkSiteOnlineStatus($domain_url, $timeout = 5) {
        $culr_time = microtime(true);
        $curlInit = curl_init($domain_url);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($curlInit, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curlInit, CURLOPT_HEADER, false);
        curl_setopt($curlInit, CURLOPT_FAILONERROR, true);
        curl_setopt($curlInit, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($curlInit,CURLOPT_NOBODY,f);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlInit, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13');
        curl_setopt($curlInit, CURLOPT_REFERER, 'http://www.google.fr/search?hl=fr&source=hp&q=' . $domain_url . '&aq=f&aqi=g2&aql=&oq=');
        //get answer
        $response = curl_exec($curlInit);
        //echo 'Curl_head . '.(microtime(true) - $culr_time).'<br>';
        //$getInfo = curl_getinfo($curlInit);
        $httpCode = curl_getinfo($curlInit, CURLINFO_HTTP_CODE);

        //__debug($httpCode);


        if (curl_error($curlInit)) {
            $error_number = curl_errno($curlInit);
            if ($error_number == 28) // CURLE_OPERATION_TIMEOUTED
                $this->curl_timeout_limit+=10; //
        }
        return true;
    }

    // insert baclkink info status
    function setBacklinkStatus($site_id, $annuaire_id, $backlink_url = '') {

        $result = $this->dbh->query(" SELECT * FROM `annuaires2backlinks`
                                      WHERE site_id = " . intval($site_id) . " AND annuaire_id=" . intval($annuaire_id));
        $row = $result->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            try {
                // __debug($row);
                //__debug($site_id,$annuaire_id,$backlink_url);
                $status = !empty($backlink_url) ? 'found' : 'not_found_yet';
                $date_checked = date("Y-m-d H:i:s");
                $date_found = "0000-00-00 00:00:00";

                if ($status == 'found' && $row['date_found'] == "0000-00-00 00:00:00") {
                    $date_found = date("Y-m-d H:i:s");

//                    if (time() > strtotime(date('18-8-2015'))) {
//
//                    $resultAnnuaire = $this->dbh->query(" SELECT * FROM `annuaires` WHERE id=" . intval($annuaire_id));
//                    $rowAnnuaire = $resultAnnuaire->fetch(PDO::FETCH_ASSOC);
//
//                    if ($rowAnnuaire && $rowAnnuaire['WebPartenairePrice'] > 0 && intval($rowAnnuaire['webmasterPartenaire']) > 0) {
//
//                        $ajoutComission = $this->dbh->query('INSERT INTO `comissions` (id,webmaster,annuaire_id,site_id,amount,month,year,date) '
//                                . 'VALUES("","' . $rowAnnuaire['webmasterPartenaire'] . '","' . $annuaire_id . '",'
//                                . '"' . $site_id . '","' . $rowAnnuaire['WebPartenairePrice'] . '","' . date('m') . '",'
//                                . '"' . date('Y') . '","' . time() . '" ');
//
//                        if ($ajoutComission) {
//
//                            $getSoldeWebmasterRequete = $this->dbh->query(" SELECT solde FROM `utilisateurs` WHERE id = " . intval($rowAnnuaire['webmasterPartenaire']));
//                            $getSoldeWebmaster = $getSoldeWebmasterRequete->fetch(PDO::FETCH_ASSOC);
//
//                            if ($getSoldeWebmaster && $getSoldeWebmaster['solde']) {
//                                $newPartenaireSolde = $getSoldeWebmaster['solde'];
//                                $newPartenaireSolde = $getSoldeWebmaster['solde'] + $rowAnnuaire['WebPartenairePrice'];
//                                $sqlSolde = "UPDATE `utilisateurs` SET `solde` = '" . $newPartenaireSolde . "' WHERE id = " . intval($rowAnnuaire['webmasterPartenaire']);
//                                $this->dbh->beginTransaction();
//                                $this->dbh->exec($sqlSolde);
//                                $this->dbh->commit();
//                            }
//                        }
////                        }
//                    }
                }

                $sql = "UPDATE `annuaires2backlinks` SET `date_checked` = '" . $date_checked . "',
                                                                     `date_found` = '" . $date_found . "',
                                                                     `backlink` = '" . $backlink_url . "',
                                                                     `status` =  '" . $status . "'
                                WHERE site_id = " . intval($site_id) . " AND annuaire_id=" . intval($annuaire_id);
                //__debug($sql);

                $this->dbh->beginTransaction();
                $this->dbh->exec($sql);
                $this->dbh->commit();
                // notice user
                if ($status=='found'){
                    $mailer = new MailProvider();
                    $mailer->CronNoticeBacklinkFound($site_id,$annuaire_id,$backlink_url,$this->dbh);
                }
            } catch (Exception $e) {
                if ($e->errorInfo[1] == 1062) {
                    echo '<h1>' . $e->getMessage() . '</h1>';
                    var_dump($sql);
                    $this->dbh->rollback();
                } else {
                    var_dump($e->getMessage());
                }
            }
        } else {
            try {
                //var_dump(date("Y-m-d H:i:s"));
                //var_dump($backlink_url);
                //return;
                $sql = "INSERT INTO `annuaires2backlinks` (`site_id`, `annuaire_id`, `backlink`,`date_checked`,`date_checked_first`,`status`)
                                 VALUES(?, ?, ?, ?, ?, ?)";
                $stmt = $this->dbh->prepare($sql);
                $date_checked = date("Y-m-d H:i:s");
                $date_checked_first = date("Y-m-d H:i:s");
                $status = !empty($backlink_url) ? 'found' : 'not_found_yet';
                $params = array();
                $params[] = $site_id;
                $params[] = $annuaire_id;
                $params[] = $backlink_url;
                $params[] = $date_checked;
                $params[] = $date_checked_first;
                $params[] = $status;
                if (!empty($backlink_url)) {
                    __debug($sql);
                }
                $stmt->execute($params);
                if ($status=='found'){
                    $mailer = new MailProvider();
                    $mailer->CronNoticeBacklinkFound($site_id,$annuaire_id,$backlink_url,$this->dbh);
                }
            } catch (Exception $e) {
                if ($e->errorInfo[1] == 1062) {
                    echo '<h1>Duplicate' . $e->getMessage() . '</h1>';
                    var_dump($sql);
                } else {
                    var_dump($e->getMessage());
                }
            }
        }
    }

    function getDomainNameById($domain_id) {
        foreach ($this->domains as $domain) {
            
        }
    }

    // itegration
    // from analyseur
    // function get_links_from_xml($content){
    function get_links_from_xml($content) {
        $rss = @simplexml_load_string($content);

        $links = array();
        if ($rss) {
            //$links[] = (string)$rss->channel->link; // need?
            foreach ($rss->channel->item as $item) {
                $links[] = (string) $item->link;
            }
        }
        return $links;
    }

    // itegration
    // from analyseur
    // function install_bdd($nomDomaine, $pageDeb)
    function findInterneLink() {
        $result = '';

        $resultats = $this->dbh->query(" SELECT url_analysee FROM bot_urls WHERE est_traitee = 0 LIMIT 0, 1");
        if ($resultats) {
            while ($resultat = $resultats->fetch(PDO::FETCH_OBJ)) {
                $result = $resultat->url_analysee;
            }
        }

        return $result;
    }

    // itegration
    // from analyseur
    // function install_bdd($nomDomaine, $pageDeb)
    function prepareCheckDomain($nomDomaine) {

        try {
            $this->dbh->exec("DROP TABLE IF EXISTS `bot_urls`");

            $this->dbh->exec("CREATE TABLE IF NOT EXISTS `bot_urls` (
							  `id_url` int(11) NOT NULL AUTO_INCREMENT,
							  `url_analysee` varchar(300) DEFAULT NULL,
							  `url_recherchee` varchar(300) DEFAULT NULL,
							  `est_traitee` int(11) DEFAULT 0,
							  PRIMARY KEY (`id_url`)
							) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");

            $this->dbh->exec("INSERT INTO `bot_urls` (`url_analysee`, `url_recherchee`, `est_traitee`) VALUES('" . $this->urlDebut($nomDomaine) . "', '', 0)");
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    // itegration
    // from analyseur
    // function urlDebut($ndd, $pageDebut) {
    function urlDebut($ndd) {
        $urlgo = $ndd;

        /*
          $ndd = str_replace('http://','',$ndd);
          $ndd = str_replace('www.','',$ndd);

          $urlgo = $pageDebut;
          if(strlen($pageDebut) < 7 || 'http://' != substr($pageDebut, 0, 7)) {
          if($pageDebut != '') {
          if(substr($pageDebut, 0, 1) != '/') {
          $urlgo = 'http://www.'. str_replace('//', '/', $ndd );
          } else {
          $urlgo = 'http://www.'. str_replace('//', '/', $ndd);
          }
          } else {
          $urlgo = 'http://www.'. $ndd;
          }
          } */

        // check subdomain - removing `www.`
        $exploded = explode('.', $this->getHost($urlgo));
        if ((count($exploded) > 2)) {
            $urlgo = str_replace('www.', '', $urlgo);
        }
        return trim($urlgo);
    }

}

function __debug() {
    echo '<pre>';
    var_dump(func_get_args());
    echo '</pre>';
}
?>

