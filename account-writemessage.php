<?PHP
@session_start();

$a = 3;
$b = 2;
$page = 1;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">
            <div class="one">

                <div class="permalink">
                    <h4><a nohref="#">R�diger un message</a></h4>
                </div>
                <br/>
                <?PHP
                if (isset($_GET['statut']) && $_GET['statut'] != "") {
                    ?>
                    <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                        <p>
                            <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                            <?PHP echo $_SESSION['alertLogincontent']; ?>
                        </p>
                    </div>
                    <?PHP
                }

                if (isset($_GET['id']) && !empty($_GET['id'])) {
                    if (preg_match("#readmessage#", $_SERVER['HTTP_REFERER'])) {
                        $msg = Functions::getMessageById(intval($_GET['id']));
                        if (!empty($msg['objet'])) {
                            $msg['objet'] = "Re:" . $msg['objet'];
                        }
                    } else {
                        $msg['objet'] = "Objet du message";
                        $msg['senderID'] = intval($_GET['id']);
                    }
                } else {
                    $msg['senderID'] = 0;
                    $msg['objet'] = "Objet du message";
                }
                ?>
                <form action="compte/ajouter/message.html" method="post" style="margin-top:0px;float:left;">
                    <!--<button class="search-btn"></button>-->
                    <fieldset style="">
                        <!--<legend class="color">Connexion</legend>-->
                        <ul class="non-list-ul">
                            <li>
                                <select name="destinataire" class="select">
                                    <option value="0">Choisir destinataire(s)</option>
                                    <?PHP
                                    if (isSU() || isAdmin()) {
                                        ?>
                                        <option value="ALL0">Tous les Utilisateurs</option>
                                        <option value="ALL2">Tous les Administeurs</option>
                                        <option value="ALL3">Tous les R�f�renceurs</option>
                                        <option value="ALL4">Tous les Webmasters</option>
                                    <?PHP } ?>
                                    <?PHP
                                    if (isReferer() || isWebmaster()) {
                                        echo Functions::getUserOptionList(2, 0, $msg['senderID']);
                                    }
                                    if (isSU() || isAdmin()) {
                                        echo Functions::getUserOptionList(5, 0, $msg['senderID']);
                                    }
                                    ?>
                                </select>
                            </li>
                            <li>
                                <input type="checkbox" name="sendMessage" value="1" id="sendMessage"><label for="sendMessage">Envoyer par email le vrai message</label>
                            </li>
                        </ul>

                        <input name="objet" class="search-field" type="text" onblur="if (this.value == '')
                                    this.value = 'Objet du message';" onfocus="if (this.value == 'Objet du message')
                                                this.value = '';" value="<?PHP echo stripslashes($msg['objet']); ?>" style="margin-bottom:5px;width:400px;"><br/>


<!--                        <input name="frequence" class="search-field" type="text" onblur="if (this.value == '')
                                    this.value = '';" onfocus="if (this.value == 'fr�quence')
                                    this.value = '';" value="fr�quence" style="margin-bottom:5px;width:50px;float:left"><br/>-->
                        <textarea rows="15" cols="" name="message" class="search-field"  onblur="if (this.value == '')
                                    this.value = 'Contenu du message';" onfocus="if (this.value == 'Contenu du message')
                                                this.value = '';" style="margin-bottom:5px;width:400px;height:200px;">Contenu du message</textarea><br/>



                        <input type="submit" class="button color small round" value="Envoyer le message" style="color:white;"/>

                </form>
            </div>


            <!-- Pagination -->

            <!-- End pagination -->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>