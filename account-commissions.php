<?PHP
@session_start();

$a = 8;
if (isset($_GET['section']) && !empty($_GET['section'])) {
    if ($_GET['section'] == "contrat") {
        $p = 3;
        $b = 2;
    } else if ($_GET['section'] == "details") {
        $p = 2;
        $b = 1;
    } else {
        $_GET['section'] = "accueil";
        $p = 2;
        $b = 1;
    }
} else {
    $_GET['section'] = "accueil";
    $p = 1;
    $b = 1;
}
$page = 1;


include('files/includes/topHaut.php');


isRight("3");
$months = array('', 'Janvier', 'F�vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao�t', 'Septembre', 'Octobre', 'Novembre', 'D�cembre');

if (Functions::isWebmasterCommission($idUser)) {
//    echo strtotime(date('18-8-2015'));
    ?>


    <!--breadcrumbs ends -->
    <div class="container">
        <div class="one">

            <?PHP
            if (!isset($_GET['id']) || empty($_GET['id'])) {
                $_GET['id'] = 0;
            }

            $sizeBlock = "one";
            if ($_GET['section'] != "details") {
                include("files/includes/menu.php");
                $sizeBlock = "three-fourth";
            } else {
                
            }
            ?>
            <div class="<?PHP echo $sizeBlock; ?>"> 
                <?PHP
                $periode = $_SESSION['allParameters']['affiliationhistory']["valeur"];

                $max = $periode + 7;
                $maxDisp = $max - 1;
                if ($_GET['section'] == "contrat") {
                    echo '<h4>Contrat et r�gles d\'utilisation</h4>';
                } else if ($_GET['section'] == "details") {
                    $periodArray = explode('-', $_GET['detailsid']);
                    echo '<h4>D�tails des gains. P�riode : ' . $months[$periodArray[0]] . ' ' . $periodArray[1] . ' </h4>';
                } else {
                    echo '<h4>R�sum� de vos commissions durant les ' . $maxDisp . ' derniers mois</h4>';
                }

                if ($_GET['section'] == "contrat") {
                    $contrat = Functions::getcontrat("6");
                    ?>

                    <div class="color pre" style="width:640px;">
                        <?PHP
                        echo $contrat['contrat'];
//                echo nl2br($contrat['contrat']);
                        ?>
                    </div>

                    <?PHP
                } else if ($_GET['section'] == "details" && !empty($_GET['detailsid'])) {
//                    echo "lool";
                    ?>
                    <a href="/compte/commissions.html" class="a_menu round button color " style="width:150px;"> Retour</a>
                    <br/>
                    <?PHP
                    $total = 0;
                    $period = explode("-", $_GET['detailsid']);
                    $data = Functions::getAllcomissions($idUser, $period[0], $period[1]);
                    if (count($data) > 0) {
//                    echo "lool";
                        ?>
                        <table class="simple-table">
                            <tbody>
                                <tr>

                                    <th style="text-align:center;">
                                        Annuaire
                                    </th>
                                    <th style="text-align:center;">
                                        Site valid�
                                    </th>
                                    <th style="text-align:center;">
                                        Gain
                                    </th>
                                    <th style="text-align:center;">
                                        Date
                                    </th>


                                </tr>
                                <?PHP
                                foreach ($data as $linedata) {
                                    ?>
                                    <tr>
                                        <td style="font-size:12px;font-weight:bold;">
                                            <?PHP
                                            echo Functions::getAnnuaireName($linedata['annuaire_id']);
                                            ?>
                                        </td>
                                        <td style="font-size:12px;font-weight:bold;">
                                            <?PHP
                                            echo Functions::getProjetName($linedata['site_id']);
                                            ?>
                                        </td>
                                        <td style="text-align:center;font-size:12px;font-weight:bold;color:green;">
                                            <?PHP
                                            echo $linedata['amount'];
                                            $total += $linedata['amount'];
                                            ?>
                                            <i class="icon-euro"></i>
                                        </td>
                                        <td style="font-size:12px;text-align:center;">
                                            <?PHP
                                            echo date('d-m-Y', $linedata['time']);
                                            ?>                                
                                        </td>

                                    </tr>
                                    <?PHP
                                }
                                ?>
                                <tr style="font-size:16px;">
                                    <td style="font-weight:bold;border:none;">

                                    </td>

                                    <td style="font-weight:bold;color:white;background:green">
                                        TOTAL DES GAINS :
                                    </td>
                                    <td style="text-align:center;font-weight:bold;color:white;background:green">
                                        <?PHP
                                        echo $total;
                                        ?>           
                                        <i class="icon-euro"></i>                     
                                    </td>
                                    <td style="font-weight:bold;border-width:0px 0px 0 1px;">

                                    </td>

                                </tr>
                            </tbody>
                        </table>
                        <?PHP
                    } else {
//                    echo "lool";
                        ?>
                        <h3>
                            Aucun d�tails � afficher
                        </h3>
                        <?PHP
                    }
                    ?>
                    <?PHP
                } else {
                    ?>
                    <table class="simple-table">
                        <tbody>
                            <tr>
                                <td style='border:none;text-align:right;'>
                                </td>
                                <th style="text-align:center;">
                                    Liens Valid�s
                                </th>
                                <th style="text-align:center;">
                                    Gains
                                </th>
                                <th style="text-align:center;">
                                    Actions
                                </th>


                            </tr>
                            <?PHP
                            $start = 1;
                            $total  = 0;
                            $startOver = $max - 1;
                            while ($start <= $max) {
                                $date = strtotime(date("Y-m-d", strtotime(date('Y') . "-" . date('m') . "-1 -" . $startOver . " months")));
                                $month = date("m", $date);
                                $monthSimple = date("n", $date);
                                $year = date("Y", $date);
                                ?>
                                <tr>

                                    <?PHP
                                    if ($start == $max) {
                                        echo "<th style='background:green'>Mois en cours (" . $months[$monthSimple] . " " . $year . ")</th>";
                                    } else {
                                        echo "<th>" . $months[$monthSimple] . " " . $year . "</th>";
                                    }
                                    ?>
                                    <td style="text-align:center;font-size:15px;font-weight:bold;">
                                        <?PHP
                                        echo intval(Functions::getCountClicksComissions($idUser, $monthSimple, $year));
                                        ?>
                                    </td>
                                    <td style="text-align:center;font-size:15px;font-weight:bold;color:green;">
                                        <?PHP
                                        $joined = Functions::getCountPaidComissions($idUser, $monthSimple, $year);
                                        echo $joined['paid'];
                                        $total +=  $joined['paid'];
                                        ?>
                                        <i class="icon-euro"></i>
                                    </td>
                                    <td style="font-size:15px;text-align:center;">
                                        <a href="./compte/commissions.html?section=details&detailsid=<?PHP echo $monthSimple . "-" . $year ?>">D�tails</a>
                                    </td>

                                </tr>
                                <?PHP
                                $start++;
                                $startOver--;
                            }
                            ?>
                            <tr style="font-size:16px;">
                                <td style="font-weight:bold;border:none;">

                                </td>

                                <td style="font-weight:bold;color:white;background:blueviolet">
                                    TOTAL DES GAINS :
                                </td>
                                <td style="text-align:center;font-weight:bold;color:white;background:blueviolet">
                                    <?PHP
                                    echo $total;
                                    ?>           
                                    <i class="icon-euro"></i>                     
                                </td>
                                <td style="font-weight:bold;border-width:0px 0px 0 1px;">

                                </td>

                            </tr>
                        </tbody>
                    </table>
                    <!-- Pagination -->
                    <?PHP
                }
                ?>
                <!-- End pagination -->

                <!--            <h4>Banni�res Ereferer</h4>
                            <img src="http://www.ereferer.fr/bannieres/468x60-1.jpg"/><br/><br/>
                            <img src="http://www.ereferer.fr/bannieres/468x60-2.gif"/><br/><br/>
                            <img src="http://www.ereferer.fr/bannieres/468x60-3.jpg"/><br/><br/>
                            <img src="http://www.ereferer.fr/bannieres/336x280-1.jpg"/>
                            <img src="http://www.ereferer.fr/bannieres/336x280-2.jpg"/>-->
            </div>


        </div>


    </div>
    <?PHP
} else {
    header("Location:/compte/accueil.hrml");
    exit;
}

include("files/includes/bottomBas.php")
?>