<?PHP

@session_start();
if (isset($_GET['iduser']) && (intval($_GET['iduser']) > 0)) {
    $_SESSION['ID_PARRAIN'] = intval($_GET['iduser']);
} else {
    $_SESSION['ID_PARRAIN'] = 0;
}

include("files/includes/config/configuration.php");
include("files/includes/functions.php");

if (isset($_SESSION['ID_PARRAIN']) AND $_SESSION['ID_PARRAIN'] > 0) {
    setcookie("ID_PARRAIN", $_SESSION['ID_PARRAIN'], (time() + 2592000));
    $dbh->query('INSERT INTO affiliation_click VALUES("", "' . $_SESSION['ID_PARRAIN'] . '", "' . date('m') . '", "' . date('Y') . '", "' . time() . '")');
}

//echo $_SESSION['ID_PARRAIN'];
header('location: ./');
?>