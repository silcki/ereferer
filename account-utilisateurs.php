<?PHP
@session_start();

$a = 5;

if (isset($_GET['section']) && !empty($_GET['section'])) {
    if ($_GET['section'] == "referenceurs") {
        $p = 3;
        $b = 8;
    } else if ($_GET['section'] == "webmasters") {
        $p = 4;
        $b = 9;
    } else if ($_GET['section'] == "administrateurs") {
        $p = 2;
        $b = 10;
    } else if ($_GET['section'] == "inscriptions") {
        $p = 5;
        $b = 14;
    } else if ($_GET['section'] == "affiliation") {
        $p = 7;
        $b = 78;
    } else if ($_GET['section'] == "parrains") {
        $p = 8;
        $b = 79;
    } else if ($_GET['section'] == "referenceursAdmin") {
        $p = 9;
        $b = 80;
    } else {
        $_GET['section'] = "referenceurs";
        $p = 3;
        $b = 8;
    }
} else {
    $_GET['section'] = "referenceurs";
    $p = 3;
    $b = 8;
}

$page = 1;
include('files/includes/topHaut.php');
isRight("3,4");


require_once 'files/includes/Classes/User.php';
require_once 'files/includes/Classes/Likes.php';
require_once 'files/includes/Classes/Account.php';
require_once 'files/includes/Classes/RedactionLikes.php';

?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">
        <?PHP
        include("files/includes/menu.php")
        ?>
        <div style="position:absolute;top:0px;right:0;margin-right:20px;z-index: 9999;">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
            }
            ?>
            <br>
            <br>
        </div>
        <div class="three-fourth">
            <?PHP
            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertLogincontent']; ?>
                    </p>
                </div>
                <?PHP
            }
            if (isset($_GET['byuser']) || (isset($_GET['byuser']) && intval($_GET['byuser']) > 0)) {
                $by = $_GET['byuser'];
            } else {
                $by = 0;
            }

            if ($_GET['section'] == "referenceurs") {
                echo '<h4>Liste des r�f�renceurs</h4>';
            } else if ($_GET['section'] == "webmasters") {
                echo '<h4>Liste des Webmasters</h4>';
            } else if ($_GET['section'] == "administrateurs") {
                echo '<h4>Liste des administrateurs</h4>';
            } else if ($_GET['section'] == "affiliation") {
                echo '<h4>Liste des comptes par affiliation';
                if ($by > 0) {
                    echo ' de ' . Functions::getfullname($by);
                }
                echo '</h4>';
            } else if ($_GET['section'] == "parrains") {
                echo '<h4>Liste des comptes parrains</h4>';
            } else if ($_GET['section'] == "inscriptions") {
                echo '<h4>Liste des demandes d\'inscription</h4>';
            }
            ?>

            <?php if ($_GET['section'] == 'webmasters') { ?>
                <?php
                    $accountModel = new Account();
                    $total_balance = $accountModel->getTotalWebmastersBalance();
                    $total_balance = round($total_balance,2);


                ?>
                <h5>Total gains webmasters: <?php echo $total_balance;?> <i class="icon-euro"></i></h5>

                <?php
                 $order_by = isset($_GET['sort'])?$_GET['sort']: -1;
                ?>
                <b>Order by</b>: <select name="webmasters_sort" id="webmasters_sort" style="display: inline-block;width:180px;">
                            <option value="-1">Default</option>
                            <option value="name_asc">Name &#8593;</option>
                            <option value="solde_asc">Solde &#8593;</option>
                            <option value="visit_asc">Derni�re connexion &#8593;</option>
                            <option value="satisfaction_asc">Taux de satisfaction &#8593;</option>
                            <option value="name_desc">Name &#8595;</option>
                            <option value="solde_desc">Solde &#8595;</option>
                            <option value="visit_desc">Derni�re connexion &#8595;</option>
                            <option value="satisfaction_desc">Taux de satisfaction &#8595;</option>
                          </select> <br/><br/>

                <script type="text/javascript">
                    (function($){
                        $(document).ready(function(){

                            $("#webmasters_sort").change(function(){
                                var order_by = $(this).val();
                                var url = '/compte/utilisateurs.html?section=webmasters';
                                if (order_by !=-1){
                                   url += ('&sort='+order_by);
                                }
                                window.location.href = url;
                            });

                            // set selected
                            <?php if (!empty($order_by)){ ?>
                                var order_by = <?php echo json_encode($order_by);?>;
                                $("#webmasters_sort").val(order_by);
                            <?php } ?>
                        });
                    })(jQuery);
                </script>

            <?php } ?>

            <?PHP
            $webmasters_order = isset($_GET['sort'])?$_GET['sort']:'';
            $getUsers = Functions::getAllUsersInfos(0, $p, $by,$webmasters_order);

            $count = 0;
            $count = count($getUsers);

            require("pages.php");

            // writers likes-stats
            $likesModel = new Likes();
            $writers_likes_stats = $likesModel->getStatsForWriters(1);
            $writers_unlikes_stats = $likesModel->getStatsForWriters(0);

            $redaction_LikesModel = new RedactionLikes();
            $redaction_writers_likes_stats = $redaction_LikesModel->getStatsForWriters(1);
            $redaction_writers_unlikes_stats = $redaction_LikesModel->getStatsForWriters(0);


            // webmaster satisfaction rate, based on his likes|unlikes
            if ($_GET['section'] == 'webmasters') {
                $satisfactions_rates = $likesModel->getSatisfactionRatesForWebmasters();
            }
            if ($count > 0) {
                ?>

                <!--<h4>Tableau de Bord</h4>-->
                <table class="simple-table">
                    <tbody>
                        <tr>
                            <th>
                                Informations
                            </th>
                            <?PHP if ($_GET['section'] != "inscriptions" || $getUsers[$limit1]['active'] == 1) { ?>
                                <?PHP if (!isAdmin($getUsers[$limit1]['typeutilisateur']) && !isSu($getUsers[$limit1]['typeutilisateur'])) { ?>
                                                                                                                                                                                                                  <!--                                <th>
                                                                                                                                                                                                                    Projets
                                                                                                                                                                                                                    </th>-->
                                <?PHP } ?>
                            <?PHP } ?>
                            <th>
                                Actions
                            </th>

                        </tr>
                        <?PHP
                        while ($limit1 <= $limit2) {

                            if ($getUsers[$limit1]['id']) {
                                if ($getUsers[$limit1]['telephone'] == 0) {
                                    $getUsers[$limit1]['telephone'] = "";
                                }
                                ?>
                                <tr>
                                    <td>
                                        <?PHP if ($_GET['section'] == "inscriptions") { ?>
                                            <span class="Notes">Type de compte</span> :  <?PHP echo $arrayLvl[$getUsers[$limit1]['typeutilisateur']]; ?> <br/>
                                        <?PHP } ?>
                                        <?PHP if ($_GET['section'] == "affiliation" && $getUsers[$limit1]['active'] == 1 && $getUsers[$limit1]['lastIP'] > 0) { ?>
                                            <span class="Notes">Parrain</span> : <a href='./compte/utilisateurs.html?section=affiliation&byuser=<?PHP echo $getUsers[$limit1]['lastIP']; ?>' title="Voir la liste des affili�s de ce webmaster"><?PHP echo Functions::getfullname($getUsers[$limit1]['lastIP']); ?>&nbsp;<i class="icon-list"></i></a> <br/>
                                        <?PHP } ?>
                                        <span class="Notes">Nom & Pr�nom(s)</span> :  <?PHP echo $getUsers[$limit1]['nom'] . " " . $getUsers[$limit1]['prenom']; ?><br/>
                                        <span class="Notes">Email</span> : <?PHP echo $getUsers[$limit1]['email']; ?><br/>
                                        <span class="Notes">T�l�phone</span> : <?PHP echo $getUsers[$limit1]['telephone']; ?><br/>
                                        <span class="Notes">Code Postal</span> : <?PHP echo $getUsers[$limit1]['codepostal']; ?><br/>
                                        <span class="Notes">Ville</span> : <?PHP echo $getUsers[$limit1]['ville']; ?><br/>
                                        <?PHP if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) { ?>
                                            <span class="Notes">Soci�t�</span> :  <?PHP echo $getUsers[$limit1]['societe']; ?><br/>
                                        <?PHP } ?>
                                        <?PHP if (isReferer($getUsers[$limit1]['typeutilisateur'])) { ?>
                                            <span class="Notes">Projet(s)</span> :  <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 3); ?>  en cours, <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 3, 1); ?> termin�(s)<br/>
                                        <?PHP } ?>
                                        <?PHP if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) { ?>
                                            <span class="Notes">Projet(s)</span> :  <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 4); ?> en cours, <?PHP echo Functions::getCountProjetByUser($getUsers[$limit1]['id'], 4, 1); ?> termin�(s)<br/>
                                        <?PHP } ?>
                                        <span class="Notes">Statut</span> : <?PHP echo $statutLvl[$getUsers[$limit1]['connected']]; ?><br/>
                                        <?PHP if ($getUsers[$limit1]['lastlogin'] > 0) { ?>
                                            <span class="Notes">Derni�re Connexion</span> : <?PHP echo date("d/m/Y � H:i:s", $getUsers[$limit1]['lastlogin']); ?><br/>
                                        <?PHP } ?>
                                        <span class="Notes">Inscription</span> : <?PHP echo date("d/m/Y � H:i:s", $getUsers[$limit1]['joinTime']); ?><br/>
                                        <?PHP if ($_GET['section'] == "affiliation" && $getUsers[$limit1]['active'] == 1) { ?>
                                            <span class="Notes" style="color:blue;text-decoration:none;">A recharger</span> : 
                                            <?PHP
                                            echo "au moins " . intval($getUsers[$limit1]['AmountLastPayment']);
                                            if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) {
                                                $deviz = " <i class='icon-euro'></i>";
                                                echo $deviz;
                                            }
                                            if (isReferer($getUsers[$limit1]['typeutilisateur'])) {
                                                $deviz = " <i class='icon-dollar'></i>";
                                                echo $deviz;
                                            }
                                            if (isAdmin($getUsers[$limit1]['typeutilisateur'])) {
                                                $deviz = " <i class='icon-euro'></i>";
                                                echo $deviz;
                                            }
                                            ?>
                                            dans son compte.
                                            <br/>    
                                        <?PHP } ?>

                                        <?PHP
                                        if ($_GET['section'] == "parrains" && $getUsers[$limit1]['active'] == 1) {
                                            $joined = Functions::getCountPaid($getUsers[$limit1]['id'], date('n'), date('Y'));
                                            ?>
                                            <span class="Notes" style="color:green">Compte(s) Affili�(s)</span> :  <?PHP echo Functions::getCountAffilies($getUsers[$limit1]['id']) ?> <a href='./compte/utilisateurs.html?section=affiliation&byuser=<?PHP echo $getUsers[$limit1]['id']; ?>' title="Voir la liste des affili�s de ce webmaster"><i class="icon-list"></i></a> <br/>
                                            <span class="Notes" style="color:green">Gain ce mois</span> :  <?PHP echo $joined['paid']; ?> <i class='icon-euro'></i><br/>
                                            <span class="Notes" style="color:green">Total des gains</span> :  <?PHP echo Functions::getCountTotalPaid($getUsers[$limit1]['id']); ?> <i class='icon-euro'></i><br/>
                                        <?PHP } ?>

                                        <?PHP
                                        if ($_GET['section'] == "referenceursAdmin" || $_GET['section'] == "referenceurs") {
                                            $number_of_sommisions = 0;
                                            $number_of_sommisions_found = 0;
                                            $number_of_sommisions_notfoundyet = 0;
                                            $number_of_sommisions_notfound = 0;

                                            $NotYetfoundSoumissions = 0;
                                            $foundSoumissions = 0;

                                            $getResUsers = Functions::GetTauxUser($getUsers[$limit1]['id']);
                                            if ($getResUsers) {
                                                $number_of_sommisions = $getResUsers['total'];
                                                $number_of_sommisions_found = $getResUsers['found'];
                                                $number_of_sommisions_notfoundyet = $getResUsers['notfoundyet'];
                                                $number_of_sommisions_notfound = $getResUsers['notfound'];

                                                $foundSoumissions = Functions::getPourcentage($number_of_sommisions_found, $number_of_sommisions);
                                                $NotYetfoundSoumissions = Functions::getPourcentage($number_of_sommisions_notfoundyet, $number_of_sommisions);
//                                                $NotYetfoundSoumissions = Functions::getPourcentage(($number_of_sommisions_notfoundyet + $number_of_sommisions_notfound), $number_of_sommisions);
                                                $NotfoundSoumissions = round(100 - ($foundSoumissions + $NotYetfoundSoumissions), 1);
                                                $NotfoundSoumissionsForBonus = round(100 - ($foundSoumissions), 1);
//                                                echo $number_of_sommisions . ".";
//                                                echo $number_of_sommisions_found . ".";
//                                                echo $number_of_sommisions_notfound;
                                            }

                                            $monthCurrent = date('n');
                                            $yearCurrent = date('Y');

                                            if (intval($monthCurrent) == 1) {
                                                $monthChoosed = 12;
                                                $yearChoosed = $yearCurrent - 1;
                                            } else {
                                                $monthChoosed = $monthCurrent - 1;
                                                $yearChoosed = $yearCurrent;
                                            }


                                            $quotien = 0;
                                            $bonus = 0;
                                            $bonusToAddedCentaine = 0;
                                            $countLastMonth = 0;
                                            $countThisMonth = 0;

                                            $countLastMonth = Functions::getCountSoumissions($monthChoosed, $yearChoosed, $getUsers[$limit1]['id']);
                                            $countThisMonth = Functions::getCountSoumissions($monthCurrent, $yearCurrent, $getUsers[$limit1]['id']);
                                            ?>
                                            <br/>

                                            <span class="Notes" style="color:gray">Total mois pass�</span> :  <?PHP echo $countLastMonth . " soumission(s)" ?> <br/>
                                            <span class="Notes" style="color:gray">Total mois actuel</span> :  <?PHP echo $countThisMonth . " soumission(s)" ?> <br/>
                                            <br/>

                                            <span class="Notes" style="color:green">Liens Trouv�(s)</span> :  <?PHP echo $foundSoumissions . " % (" . $number_of_sommisions_found . " liens)" ?> <br/>
                                            <span class="Notes" style="color:orange">Liens En attente(s)</span> :  <?PHP echo $NotYetfoundSoumissions . " % (" . ( $number_of_sommisions_notfoundyet ) . " liens)"; ?> <br/>
                                            <span class="Notes" style="color:red">Liens Non-trouv�(s)</span> :  <?PHP echo $NotfoundSoumissions . " % (" . ( $number_of_sommisions_notfound) . " liens)"; ?> <br/>

                                            <?php
                                               $user_id = $getUsers[$limit1]['id'];

                                               $annuaire_writer = $getUsers[$limit1]['annuaire_writer'];
                                               $redaction_writer = $getUsers[$limit1]['redaction_writer'];

                                               // annuaires
                                               $total_likes = 0;
                                               $total_likes_clients = 0;

                                               $total_unlikes = 0;
                                               $total_unlikes_clients = 0;

                                               if(isset($writers_likes_stats[$user_id])){
                                                   $total_likes = $writers_likes_stats[$user_id]['total'];
                                                   $total_likes_clients = $writers_likes_stats[$user_id]['clients_cnt'];
                                               }

                                                if(isset($writers_unlikes_stats[$user_id])){
                                                    $total_unlikes = $writers_unlikes_stats[$user_id]['total'];
                                                    $total_unlikes_clients = $writers_unlikes_stats[$user_id]['clients_cnt'];
                                                }

                                                // redaction
                                                $redaction_total_likes = 0;
                                                $redaction_total_likes_clients = 0;

                                                $redaction_total_unlikes = 0;
                                                $redaction_total_unlikes_clients = 0;

                                                if(isset($redaction_writers_likes_stats[$user_id])){
                                                    $redaction_total_likes = $redaction_writers_likes_stats[$user_id]['total'];
                                                    $redaction_total_likes_clients = $redaction_writers_likes_stats[$user_id]['clients_cnt'];
                                                }

                                                if(isset($redaction_writers_unlikes_stats[$user_id])){
                                                    $redaction_total_unlikes = $redaction_writers_unlikes_stats[$user_id]['total'];
                                                    $redaction_total_unlikes_clients = $redaction_writers_unlikes_stats[$user_id]['clients_cnt'];
                                                }
                                            ?>

                                            <?php if ($annuaire_writer){ ?>
                                                <br/><span class="Notes" >Annuaire`s Likes:</span><br/>
                                                <span class="Notes" ><span style="color:red"><?php echo $total_likes;?></span> textes ont �t� aim�s par <span style="color:red"><?php echo $total_likes_clients;?></span> diff�rents clients</span><br/>
                                                <span class="Notes" style=""><span style="color:red"><?php echo $total_unlikes;?></span> textes n'ont pas �t� aim�s par <span style="color:red"><?php echo $total_unlikes_clients;?></span> diff�rents clients</span><br/>
                                            <?php } ?>

                                            <?php if ($redaction_writer){ ?>
                                                <br/><span class="Notes" >Redaction`s Likes:</span><br/>
                                                <span class="Notes" ><span style="color:red"><?php echo $redaction_total_likes;?></span> textes ont �t� aim�s par <span style="color:red"><?php echo $redaction_total_likes_clients;?></span> diff�rents clients</span><br/>
                                                <span class="Notes" style=""><span style="color:red"><?php echo $redaction_total_unlikes;?></span> textes n'ont pas �t� aim�s par <span style="color:red"><?php echo $redaction_total_unlikes_clients;?></span> diff�rents clients</span><br/><br/>
                                            <?php } ?>
<?PHP
//                                            echo $foundSoumissions;

                                            $baseNote = 10;
                                            $baseReference = 100;

                                            $getRating = Functions::getNoteUser($NotfoundSoumissionsForBonus, $baseReference, $baseNote, 1);
//                                            print_r($getRating); 
                                            ?>
                                            <!--<span class="Notes" style="color:blueviolet">Note</span> :  <?PHP echo $getRating['note'] . "/" . $baseNote; ?> <br/>-->
                                            <span class="Notes" style="color:blueviolet">Mention</span> :  <?PHP echo $getRating['congrats']; ?> <br/>
                                            <span class="Notes" style="color:green;">R�munaration</span> : 
                                            <?PHP
//                                            if ($NotfoundSoumissionsForBonus && $NotfoundSoumissionsForBonus > 0 && $countLastMonth >= MinLastMonth && $countThisMonth >= MinCurrentMonth) {
                                            if ($countLastMonth > MinLastMonth && $foundSoumissions > PercentLinkFounded) {
                                                $bonus = $_SESSION['allParameters']['bonus']["valeur"];
                                                if ($foundSoumissions > PercentLinkFounded) {
//                                                    $quotien = round((((30 - $NotfoundSoumissionsForBonus ) / 100) * 1.33333333333333),2);
                                                    $quotien = (intval($foundSoumissions - PercentLinkFounded) * BonusSurLiensTrouves);
//                                                    $quotien += round($quotien, 2);
                                                    $bonus = $bonus + $quotien;
//                                                    echo intval($foundSoumissions - PercentLinkFounded)."----";
                                                }
                                                if ($countThisMonth > ConditionBonusSurCentaineSoumissions && $countThisMonth > MinCurrentMonth) {
                                                    $bonusToAddedCentaine = (intval($countThisMonth * BonusSurCentaineSoumissions) * 0.01);
                                                    $bonus = $bonus + $bonusToAddedCentaine;
                                                }
                                            }
                                            $remuneration = 0;
                                            $remuneration = !empty($getUsers[$limit1]['frais']) ? $getUsers[$limit1]['frais'] : $remunerationDefault;
                                            $bonusNew = ($remunerationDefault + $bonus) - $remuneration;
                                            if ($bonusNew < 0) {
                                                $bonusNew = 0;
                                            }
                                            $remuneration += $bonusNew;

                                            $deviz = " <i class='icon-dollar'></i>";
                                            echo $remuneration . '' . $deviz;
                                            echo ' ( <b>Bonus</b>: ' . $bonusNew . '' . $deviz . ' <span style=""> [<b>Liens Trouv�s</b>: ' . $quotien . ' <i class="icon-dollar"></i> + <b>Bonus Soumissions</b>: ' . $bonusToAddedCentaine . ' <i class="icon-dollar"></i> ]</span> )';
                                            ?>
                                            <br/>
                                        <?PHP } ?>
                                        <span class="Notes" style="color:blue;">Solde</span> : 
                                        <?PHP
                                        echo $getUsers[$limit1]['solde'];
                                        if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) {
                                            $deviz = " <i class='icon-euro'></i>";
                                            echo $deviz;
                                        }
                                        if (isReferer($getUsers[$limit1]['typeutilisateur'])) {
                                            $deviz = " <i class='icon-dollar'></i>";
                                            echo $deviz;
                                        }
                                        if (isAdmin($getUsers[$limit1]['typeutilisateur'])) {
                                            $deviz = " <i class='icon-euro'></i>";
                                            echo $deviz;
                                        }
                                        ?>
                                        <br/>
                                        <?php if (isWebmaster($getUsers[$limit1]['typeutilisateur']) && (isset($satisfactions_rates[$getUsers[$limit1]['id']])) ) { ?>
                                            <span class="Notes">Taux de satisfaction</span> : <?php echo $satisfactions_rates[$getUsers[$limit1]['id']]['rate'];?>%
                                            ( <?php echo $satisfactions_rates[$getUsers[$limit1]['id']]['likes_cnt'];?> likes, <?php echo $satisfactions_rates[$getUsers[$limit1]['id']]['unlikes_cnt'];?> unlikes )
                                        <?php } ?>
                                    </td>
                                    <?PHP if ($_GET['section'] != "inscriptions" || $getUsers[$limit1]['active'] == 1) { ?>
                                        <?PHP if (!isAdmin($getUsers[$limit1]['typeutilisateur']) && !isSu($getUsers[$limit1]['typeutilisateur'])) { ?>
                                                                                                                                                                                                                            <!--<td>--> 

                                            <?PHP
//                                        if ($getUsers[$limit1]['typeutilisateur'] == 3) {
//                                            $encoursProject = Functions::getProjetsListByUser($getUsers[$limit1]['id'], $getUsers[$limit1]['typeutilisateur'], 0, 1, 1, 1);
//                                        }
//                                        if ($getUsers[$limit1]['typeutilisateur'] == 4) {
//                                        $encoursProject = Functions::getProjetsListByUser($getUsers[$limit1]['id'], $getUsers[$limit1]['typeutilisateur'], 0, 1, 1, 1);
////                                        }
//                                        $ptiCount = count($encoursProject);
//                                        $startPtiCount = 1;
//                                        $rapport = "";
//                                        $rapport .= "<span class='Notes'>Projets en cours</span> (" . $ptiCount . ") : <br/>";
//                                        while ($startPtiCount <= $ptiCount && $ptiCount > 0) {
//                                            if ($encoursProject[$startPtiCount]['lien'] != "") {
//                                                $rapport .="- " . $encoursProject[$startPtiCount]['lien'] . "<br/>";
//                                            }
//                                            $startPtiCount++;
//                                        }
//                                        if ($startPtiCount > 0) {
//                                            echo $rapport;
//                                        } else {
//                                            $rapport = "Aucun projet";
//                                            echo $rapport;
//                                        }
                                            ?>

                                            <?PHP if (isWebmaster($getUsers[$limit1]['typeutilisateur'])) { ?>

                                                <?PHP
//                                            if ($getUsers[$limit1]['typeutilisateur'] == 4) {
//                                                $encoursProject = Functions::getProjetsListByUser($getUsers[$limit1]['id'], 4, 0, 0, 1, 1);
//                                            }
//                                            $ptiCount = count($encoursProject);
//                                            $startPtiCount = 1;
//                                            $rapport = "";
//                                            $rapport .= "<br/><span class='Notes'>Projets en Attente</span>  : <br/>";
//                                            while ($startPtiCount <= $ptiCount) {
//                                                if ($encoursProject[$startPtiCount]['lien'] != "") {
//                                                    $rapport .="- " . $encoursProject[$startPtiCount]['lien'] . "<br/>";
//                                                }
//                                                $startPtiCount++;
//                                            }
//                                            if ($startPtiCount > 0) {
//                                                echo $rapport;
//                                            } else {
//                                                $rapport = "Aucun projet";
//                                                echo $rapport;
//                                            }
                                                ?>
                                            <?PHP } ?>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <!--                                        <br/><span class="Notes">Termin�</span> : <br/>-->

                                            <!--</td>-->
                                        <?PHP } ?>
                                    <?PHP } ?>
                                    <td style="">
                                        <?PHP
                                        if (isAdmin() || isSu()) {
                                            ?>
                                            <?PHP if ($_GET['section'] != "inscriptions" || $getUsers[$limit1]['active'] == 1) { ?>
                                                <a href="compte/writemessage.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-envelope"></i> Envoyer un message</a><br/>
                                                <a href="compte/accueil.html?section=projects&iduser=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-list"></i> Liste de ses projets</a><br/>
                                                <a href="compte/soumissions.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-list"></i> Liste des soumissions</a><br/>
                                                <a href="compte/modifier/profil.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-user"></i> Modifier le Profil</a><br/>
                                                <a href="compte/modifier/password.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-edit"></i> Modifier le password</a><br/>
                                                <a href="compte/modifier/solde.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><?PHP echo $deviz; ?> Modifier le solde</a><br/>

                                                <?PHP if ($getUsers[$limit1]['active'] == 1) { ?> 
                                                    <a href="compte/modification/utilisateurdisable.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-power-off"></i> D�sactiver le compte</a><br/>
                                                <?PHP } if ($getUsers[$limit1]['active'] == 0) { ?> 
                                                    <a href="compte/modification/utilisateurenable.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-check"></i> Activer le compte</a><br/>

                                                <?PHP } ?> 
                                                <a href="compte/supprimer/utitlisateur.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-trash"></i> Supprimer le compte </a>
                                            <?PHP } ?> 

                                            <?PHP if ($_GET['section'] == "inscriptions" && $getUsers[$limit1]['active'] == 0) { ?>
                                                <a href="compte/modification/utilisateurenable.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>"><i class="icon-check-sign"></i> Approuver la demande</a><br/>
                                                <a href="compte/supprimer/utitlisateur.html?id=<?PHP echo $getUsers[$limit1]['id']; ?>" onclick="return confirmation();"><i class="icon-remove-sign"></i> Rejeter la demande </a>

                                            <?PHP } ?> 
                                        <?PHP } ?>

                                    </td>

                                </tr>
                                <?PHP
                            }
                            $limit1++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- Pagination -->
                <?PHP
                if ($nbre_result > pagination) {
                    echo $list_page;
                }
            } else {
                ?>
                <div align="center" width="500" style="width:700px;margin:auto;">
                    <br/>
                    <h1>Aucun utilisateur enregistr� dans ce groupe.</h1>
                    <?PHP
                    if (isAdmin()) {
                        ?>
                        <p class="p_menu" style="width:200px;"><a href="compte/ajout/utilisateur.html" class="a_menu button color <?PHP
                                                                  if ($b == 1) {
                                                                      echo "active";
                                                                  }
                                                                  ?>"> Ajouter un projet</a></p>
                            <?PHP
                        }
                        ?>
                </div>

                <?PHP
            }
            ?>


            <?php if (isSu() && ( $_GET['section'] == "referenceurs") ) { ?>
                <!-- Writer earning current month -->
                <?php
                $userModel = new User($idUser);

                // @deprecated ?
                // prepare current month timings
                /*$offset_months = 0;
                $periods = array();
                $periods[0]['start'] = date("Y-m-01", strtotime( date("Y-m-01")." -$offset_months months")).' 00:00:00';
                $last_month_day = date('Y-m-t',strtotime($periods[0]['start']));
                $periods[0]['end'] = $last_month_day." 23:59:59";*/

                $writers = $userModel->getUsersByType('writer');
                $total_writers_sum = 0.0;
                if (count($writers)):
                    ?>
                    <br/><br/>
                    <h4>Writers Earnings</h4>
                    <table class="simple-table" style="width:400px;">
                        <tr>
                            <th></th><th></th>
                        </tr>
                        <?php foreach($writers as $writer) { ?>
                            <tr>
                                <td>
                                    <?php echo strtoupper($writer['nom']).' '.ucfirst(strtolower($writer['prenom'])); ?>
                                </td>
                                <td>
                                    <?php echo round($writer['solde'],2);?> $
                                </td>
                            </tr>
                            <?php $total_writers_sum += $writer['solde'];?>
                        <?php } ?>
                        <tr>
                            <td><b>Total:</b></td>
                            <td><b><?php echo round($total_writers_sum,2); ?> $</b></td>
                        </tr>
                    </table>
                <?php endif; ?>

                <?php die; ?>
            <?php } ?>



            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>