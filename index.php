<?php

//echo 'Error';
//error_reporting(0);
//ini_set('errors_display',0);


@session_start();


if (preg_match("#accueil.html#", $_SERVER['REQUEST_URI'])) {
    header("location:./");
} else {
    if (isset($_SESSION['connected']['id'])) {
        header("location:compte/");
    }

    $a = 1;
    $page = 1;

    include('files/includes/topHaut.php');

    $requete = "SELECT valeur FROM parametres WHERE id='26'";
    $execution = $dbh->query($requete);
    $retour = $execution->fetch();
    $remunu = $retour['valeur'];
//print_r($_SERVER);
    
    ?>

    <div class="home-intro"><!--home intro starts-->
        <div class="container">
            <div class="three-fourth">
                <h4>R�f�rencement annuaire, service de r�daction, �change/achat/vente d'articles, trouvez votre bonheur sur Ereferer !</h4>
                <h5>Utilisez <?PHP
                    ;
                    echo $_SESSION['tampon']['logoText'];
                    ?> d�s maintenant.</h5>
                <!--<p>     A super clean template ready for some serious business.          </p>-->
            </div>
            <div class="one-fourth last">
                <a href="inscription.html" class="button color huge round">Inscrivez vous maintenant!</a>
            </div>
        </div>
    </div><!--home intro ends-->
    <div class="container">
        <div class="one-third"><!--block with icon starts-->
            <div class="img-holder">
                <i class="icon-book"></i>
            </div>
            <div class="feature-text">
                <h4>Un r�f�rencement dans les annuaires</h4>
                <p>
                    Un r�f�rencement manuel effectu� dans les meilleurs annuaires francophones  : 
                    Webrankinfo.com, el-annuaire.com,... 
                    Tous les annuaires sont <strong>ind�pendants</strong> et <strong>actifs</strong>. </br>Cochez tout simplement les annuaires dans lesquels vous souhaitez �tre r�f�renc� et le tour est jou� ! <b>A partir de 1.85� HT par annuaire</b>.
                </p>
                <br/>
                <p>
                    <a href="garde.html" class="button color small round">En savoir plus</a>
                </p>
            </div>
        </div><!--block with icon ends-->
        <div class="one-third"><!--block with icon starts-->
            <div class="img-holder">
                <i class="icon-ok"></i>
            </div>
            <div class="feature-text">
                <h4>Un service de r�daction</h4>
                <p>
                    Vous souhaitez obtenir des textes de qualit� � un prix d�risoire ? Au tarif de 1�65 HT les 100 mots, Ereferer met � votre disposition ses meilleurs r�dacteurs pour r�diger vos articles. Ces derniers sont ensuite <b>relus et corrig�s</b> par un membre de l'�quipe.
                </p>
                <br/>
                <p>
                    <a href="redaction.php" class="button color small round">En savoir plus</a>
                </p>
            </div>
        </div><!--block with icon ends-->
        <div class="one-third last"><!--block with icon starts-->
            <div class="img-holder">
                <i class="icon-dollar"></i>
            </div>
            <div class="feature-text">
                <h4>Echange, achat et vente d'articles</h4>
                <p>
                    Ereferer met � votre disposition un service d'�change d'articles enti�rement gratuit et fonctionnant avec des cr�dits. <b>Termin�s les �changes r�ciproques ou triangulaires !</b> Il vous sera �galement possible d'acheter et de vendre des cr�dits.
                </p>
                <br/>
                <p>
                    <a href="echange.php" class="button color small round">En savoir plus</a>
                </p>
            </div>
        </div><!--block with icon ends-->
    </div>

<?PHP
    include("files/includes/bottomBas.php");
}
?>