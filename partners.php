<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/config/configuration.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionReports.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/EchangesProposition.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/EchangeSites.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Partners.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Logger.php';

$partners = new Partners($dbh);
echo $partners->validate();