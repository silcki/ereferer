<?PHP

@session_start();
include("files/includes/config/configuration.php");

if (isset($_SESSION['connected'])) {
    global $dbh;
    @$dbh->query('UPDATE utilisateurs SET connected=0, lastlogin = "' . time() . '" WHERE id=' . $_SESSION['connected']['id']);
}

if (isset($_SESSION['URLDECONNEXION']) && empty($_SESSION['URLDECONNEXION'])) {
    $session_lien = $_SESSION['URLDECONNEXION'];
}

unset($_SESSION);
session_destroy();

@session_start();

$_SESSION['URLDECONNEXION'] = $session_lien;
$_SESSION['alertLogincontent'] = "vous �tes d�connect�";

header("location:login-success.html");
?>