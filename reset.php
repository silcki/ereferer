<?PHP
@session_start();

if (isset($_SESSION['connected'])) {
    header("location:compte/accueil.html");
}

$a = 2;
$page = 2;
include('files/includes/topHaut.php');
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">
        <div class="three-fourth">
            <div class="blog-post">

                <div class="permalink">
                    <h4><a nohref="#">Réinitialisation de mot de passe. </a></h4>
                    <br/>
                </div>
                <?PHP
                if (isset($_SESSION['alertLogincontent'])) {
                    if (isset($_GET['statut']) && $_GET['statut'] != "") {
                        ?>
                        <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                            <p>
                                <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                                <?PHP echo $_SESSION['alertLogincontent']; ?>
                            </p>
                        </div>
                    <?PHP }
                } ?>
                <form action="resetdone.html" method="post" style="margin-top:0px;float:left;">
                    <!--<button class="search-btn"></button>-->
                    <fieldset style="">
                        <!--<legend class="color">Connexion</legend>-->
                        <input name="login-input" class="search-field" type="text" onblur="if (this.value == '')
                                        this.value = 'Utilisateur/Email';" onfocus="if (this.value == 'Utilisateur/Email')
                                        this.value = '';" value="Utilisateur/Email" style="margin-bottom:5px;width:350px;"><br/>

                        <input type="submit" class="button color small round" value="Réinitialiser mon mot de passe" style="color:white;"/>
                    </fieldset>
                </form>
            </div>


            <!-- Pagination -->

            <!-- End pagination -->
        </div>
        <div class="one-fourth last sidebar">
            <div class="widget">
                <!--<h4 class="widget-title">A propos de nous</h4>-->
                <p>
<?PHP echo $_SESSION['allParameters']['description']["valeur"]; ?>                    </p>
            </div>


        </div>
    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>