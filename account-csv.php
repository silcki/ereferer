<?PHP

@session_start();
ini_set('max_execution_time', 0);
set_time_limit(0);

function nameCrypt($chaine = '') {
    if ($chaine != "") {
        $chaine = trim($chaine);
        $chaine = strtr($chaine, "�����������������������������������������������������", "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn");
        $chaine = preg_replace('/([^a-z0-9]+)/i', '-', $chaine);
        $chaine = str_replace("--", "-", $chaine);
        if (substr($chaine, -1) == '-')
            $chaine = substr($chaine, 0, -1);
        return strtolower($chaine);
    }
}

function LienClean($nameSheet = '') {
    if ($nameSheet != "") {
        $nameSheet = preg_replace("#https://#", "", $nameSheet);
        $nameSheet = preg_replace("#http://#", "", $nameSheet);
        $nameSheet = preg_replace("#www.#", "", $nameSheet);
    }
    return $nameSheet;
}

function LienReduce($nameSheet = '') {
    if ($nameSheet != "") {
        $nameSheet = substr($nameSheet, 0, 30);
    }
    return $nameSheet;
}

function convert_to_csv($input_array, $output_file_name, $delimiter = "") {
    $output_file_name = $output_file_name . '';


    error_reporting(E_ALL);
    ini_set('display_errors', TRUE);
    ini_set('display_startup_errors', TRUE);
    define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
    date_default_timezone_set('Europe/Paris');
    ini_set('include_path', ini_get('include_path') . ';./files/includes/Classes/');

    include './files/includes/Classes/PHPExcel.php';
    include './files/includes/Classes/PHPExcel/Writer/Excel2007.php';

    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Emmanuel Higel");
    $objPHPExcel->getProperties()->setLastModifiedBy("Emmanuel Higel");
    $objPHPExcel->getProperties()->setTitle($delimiter);
    $objPHPExcel->getProperties()->setSubject($delimiter);
    $objPHPExcel->getProperties()->setDescription("Document genere sur WWW.EREFERER.FR.");
    $objPHPExcel->getProperties()->setKeywords("projet " . $delimiter);
    $objPHPExcel->getProperties()->setCategory("Rapport de projet");


    $input_array = array_filter($input_array);
    $totalSheet = count($input_array);
    foreach ($input_array as $keys => $input_array_line) {
        if (!empty($input_array_line['content'][0]) && $keys <= $totalSheet) {

            $array_to_csv = array();
            $input_array_line_content = array();
            $array_to_csv[] = Array(
                'Annuaire',
                'Soumis le',
                'Statut',
                'Page Externe Cible',
                'Description'
            );

            $line = 1;
            $column = 1;
            $sheet = 0;
            $lettreFollow = 0;
            $lettre = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I');


            $input_array_line_content = array_merge($array_to_csv, $input_array_line['content']);

//            $sheet = $keys;
            $nameSheet = LienReduce(nameCrypt(LienClean($input_array_line['nameSheet'])));

            $objPHPExcel->createSheet();
            $sheet = $objPHPExcel->getSheetCount();
            $objPHPExcel->setActiveSheetIndex($sheet - 1);

            $objPHPExcel->getActiveSheet()->setTitle($nameSheet);

            foreach ($input_array_line_content as $lineOfData) {
                $lettreFollowTotal = isset($lettreFollow) ? $lettreFollow : 0;
                $lettreFollow = 1;

                foreach ($lineOfData as $dataContent) {
                    $cell = (string) $lettre[$lettreFollow - 1] . $line;
                    if ($lettreFollow <= 5) {
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($cell, utf8_encode($dataContent), PHPExcel_Cell_DataType::TYPE_STRING);
                    }
                    if ($lettreFollow == 6) {
                        if ($dataContent == 1) {
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $line)->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_GREENTWO);
                        } else if ($dataContent == 2) {
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $line)->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_ORANGE);
                        } else {
                            $objPHPExcel->getActiveSheet()->getStyle("C" . $line)->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
                        }
                        $objPHPExcel->getActiveSheet()->getStyle("C" . $line)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                    }
                    $lettreFollow++;
                }

                $line++;
                $column++;
//        $sheet++;
            }
            $objPHPExcel->getActiveSheet()->getStyle('A1:' . $lettre[$lettreFollowTotal] . '1')->getFont()->setBold(true)->getColor()->setARGB(PHPExcel_Style_Color::COLOR_BLUE);
            foreach (range('A', $lettre[$lettreFollowTotal]) as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($columnID)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            }
        }
    }

    if ($sheet > 1) {
        $objPHPExcel->removeSheetByIndex(0);
    }


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $output_file_name . '.xlsx"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header("Cache-Control: no-cache, must-revalidate");
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Pragma: public');

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save('php://output');
}

if (!empty($_SESSION['Print_projet']['nom'])) {
    convert_to_csv($_SESSION['Print_projet']['contents'], substr($_SESSION['Print_projet']['nom'], 0, 31), $_SESSION['Print_projet']['titre']);
    unset($_SESSION['Print_projet']);
}
?>