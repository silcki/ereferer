<?PHP

session_start();
//echo session_id();
if (!isset($_SESSION['connected'])) {
    if (!empty($_POST['password-input']) && !empty($_POST['login-input'])) {


        include("files/includes/config/configuration.php");
        include("files/includes/functions.php");

        require_once 'files/includes/Classes/HTML.php';
        $HTML = new HTML();


        // purify
        $_POST['login-input'] = $HTML->purifyParams($_POST['login-input']);



        global $dbh;

        /* @deprecated
        $requete = "SELECT * FROM utilisateurs WHERE username='" . addslashes($_POST['login-input']) . "' AND password='" . Functions::getCryptePassword($_POST['password-input']) . "' OR password='" . md5($_POST['password-input']) . "'";
        $execution = $dbh->query($requete);
        $retour = $execution->fetch();
         */

        // prepared
        $stmt = $dbh->prepare("SELECT * FROM utilisateurs
                      WHERE username = :username
                      AND password = :password
                      OR password = :password_hash
                      ");

        /*
        echo '<pre>';
        var_dump($_POST);


        var_dump(Functions::getCryptePassword($_POST['password-input']));
        var_dump(md5($_POST['password-input']));*/




        $stmt->bindParam(':username', $_POST['login-input']);
        $stmt->bindParam(':password', Functions::getCryptePassword($_POST['password-input']));
        $stmt->bindParam(':password_hash', md5($_POST['password-input']));

        if ($stmt->execute())
            $retour = $stmt->fetch(PDO::FETCH_ASSOC);

        /*
        var_dump($retour);
        echo '</pre>';
        die;*/

        if (isset($retour['email'])) {
            $setPass = false;

            if ($retour['password'] == md5($_POST['password-input'])) {

                /* @deprecated
                $setPass = $dbh->query('UPDATE utilisateurs SET password = "' . Functions::getCryptePassword($_POST['password-input']) . '" WHERE id=' . $retour['id']);
                */

                // prepared
                $stmt = $dbh->prepare("UPDATE utilisateurs SET password = :password
                                       WHERE id = :retour_id");

                $stmt->bindParam(':password', Functions::getCryptePassword($_POST['password-input']));
                $stmt->bindParam(':retour_id',$retour['id']);

                $setPass = $stmt->execute();

                if ($setPass) {
                    $retour['password'] = Functions::getCryptePassword($_POST['password-input']);
                }
            }

            if ($retour['active'] == 1) {
                unset($_SESSION['alertLogincontent']);
                $_SESSION['connected'] = $retour;

                /* @deprectated
                $dbh->query('UPDATE utilisateurs SET connected=1, lastlogin = "' . time() . '" WHERE id = :id');
                */

                $stmt = $dbh->prepare('UPDATE utilisateurs SET connected=1, lastlogin = :lastlogin
                                       WHERE id = :id');

                // fixed

                $stmt->bindParam(':lastlogin', time());
                $stmt->bindParam(':id',$_SESSION['connected']['id']);

                $setPass = $stmt->execute();

                // fix???
                $_SESSION['connected']['connected'] = 1;


                if (isset($_SESSION['URLDECONNEXION']) && !empty($_SESSION['URLDECONNEXION'])) {
                    header('location:' . $_SESSION['URLDECONNEXION']); die;
                } else {
                    header('location:compte/accueil.html?action=mestaches');die;
                }
            } else {
                $_SESSION['alertLogincontent'] = "Votre compte est d�sactiv�. Contactez un administrateur.";
                header('location:login-warning.html'); die;
            }
        } else {
            $_SESSION['alertLogincontent'] = "identifiants incorrects";
            header('location:login-error.html'); die;
        }
    } else {
        $_SESSION['alertLogincontent'] = "Veuillez entrer votre identifiant et votre mot de passe.";
        header('location:login-error.html'); die;
    }
} else {
    header('location:compte/accueil.html');die;
}

//echo print_r($_SESSION);
?>