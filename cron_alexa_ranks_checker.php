<?php

session_start();
// Cron  Alexa Ranks Checker
// @voodoo417

//error_reporting(E_ALL);

$document_root = dirname(__FILE__);


ini_set('memory_limit', '1024M');
ini_set('max_execution_time','999');

var_dump(ini_get('max_execution_time'));
var_dump(ini_get('memory_limit'));


set_time_limit(0);
//error_reporting(E_ALL);

require_once('files/includes/config/dbconfig.php');


// log open
$fp = fopen(dirname(__FILE__).'/cron_alexa.log', 'a+');
if ($fp) {
    $time = date("Y-m-d H:i:s");
    fwrite($fp, __FILE__ . ' running at ' . $time . '. Process ID = ' . getmypid() . PHP_EOL);
    fclose($fp);
}


//var_dump('unset_running'); unset($_SESSION['checker_running']);die;

$checker = new AlexaRankChecker();
var_dump('unset_running');
unset($_SESSION['alexa_checker_running']);
if (empty($_SESSION['alexa_checker_running'])) {
    echo 'Alexa Rank Checker.Running<br/>';
    $_SESSION['alexa_checker_running'] = true;
    $checker->run();
} else {
    echo 'Already running<br/>';
    die;
}

class AlexaRankChecker {
    private $dbh;


    function __construct() {
        try {
            $this->dbh = new PDO(DB_TYPE . ':host=' . DB_SERVER . ';dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->dbh->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
        } catch (PDOException $e) {
            echo "<p>Erreur :" . $e->getMessage() . ": veuillez re&eacutessayez plus tard</p>";
            die;
        }
    }

    function getAnnuaires() {
        $sql = "SELECT *
                FROM annuaires
                ORDER BY id ASC";
        return $this->dbh->query($sql)->fetchAll(PDO::FETCH_ASSOC);
    }


    function run(){
         $domains = $this->getAnnuaires();
         $clear_parts = array('http://','https://','www.');

         $step = 0;
         foreach($domains as $domain_info){
            $domain_url = str_replace($clear_parts,'',$domain_info['annuaire']);
            $rank = $this->get_rank($domain_url);

            if ($rank && $this->updateRank($rank,$domain_info['id'])){
                echo $domain_url.' Rank = '.$rank.' <br/>';
            }
         }

    }


    function updateRank($rank,$annuaire_id){
        $sql = "UPDATE annuaires
                SET `ar` = ".$rank."
                WHERE id = ".$annuaire_id;
        return $this->dbh->query($sql);
    }

    public function get_rank($domain){
        $url = "https://data.alexa.com/data?cli=10&dat=snbamz&url=".$domain;

        //var_dump($url); die;

        //Initialize the Curl
        $ch = curl_init();

        //Set curl to return the data instead of printing it to the browser.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,2);

        //Set the URL
        curl_setopt($ch, CURLOPT_URL, $url);

        //Execute the fetch
        $data = curl_exec($ch);

        //Close the connection
        curl_close($ch);

        $xml = new SimpleXMLElement($data);

        //Get popularity node
        $popularity = $xml->xpath("//POPULARITY");

        //Get the Rank attribute
        $rank = (string)$popularity[0]['TEXT'];
        return intval($rank);
    }

    function __destruct(){
        unset($_SESSION['alexa_checker_running']);
    }

}


// helper-functions
function __debug() {
    echo '<pre>';
    var_dump(func_get_args());
    echo '</pre>';
}
?>

