<?PHP
@session_start();

$a = 0;
$b = 0;
$specialMessage = "";

if (isset($_SESSION['a'])) {
    $a = $_SESSION['a'];
}

if (isset($_SESSION['b'])) {
    $b = $_SESSION['b'];
}



$page = 1;
include('files/includes/topHaut.php');
$_SESSION['alertLogincontent'] = "";

if (!isset($_GET['block']) || empty($_GET['block'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
}
if (!isset($_GET['id']) || empty($_GET['id'])) {
    header('location:' . $_SERVER['HTTP_REFERER']);
} else {
    $_GET['id'] = intval($_GET['id']);
}

// models
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/RedactionProjects.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/User.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Annuaire.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/files/includes/Classes/Transaction.php';


if ($_GET['block'] == "site") {
    $getCommande = Functions::getCommandeByID($_GET['id']);

    // checking access
    Functions::checkAccess(isWebmaster(),$idUser,$getCommande['proprietaire']);

//    if ($getCommande['over'] == 0) {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM projets WHERE id=' . intval($_GET['id']));
    }
    if (isWebmaster($idTypeUser)) {
        $dbh->query('DELETE FROM projets WHERE proprietaire = "' . $idUser . '" AND id=' . intval($_GET['id']));
    }


//    }
}

if ($_GET['block'] == "retirersite") {
//    $getCommande = Functions::getCommandeByID($_GET['id']);
    if ($getCommande['over'] == 1) {
        if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
            $dbh->query('UPDATE projets SET showProprio = 0 WHERE id=' . intval($_GET['id']));
        }
        if (isWebmaster($idTypeUser)) {
            $dbh->query('UPDATE projets SET showProprio = 0 WHERE proprietaire="' . $idUser . '" AND id=' . intval($_GET['id']));
        }
    }
}

if ($_GET['block'] == "factures") {
//    $getCommande = Functions::getCommandeByID($_GET['id']);
    if (isSu($idTypeUser)) {
        $factures = Functions::getFacturesByid($_GET['id']);
        if (!empty($factures['file'])) {
            @unlink($factures['file']);
        }
    }
}

if ($_GET['block'] == "facturesSupprimer") {
//    $getCommande = Functions::getCommandeByID($_GET['id']);
    if (isSu($idTypeUser)) {
        $factures = Functions::getFacturesByid($_GET['id']);
        if (!empty($factures['file'])) {
            $dbh->query('DELETE FROM factures WHERE id=' . intval($_GET['id']));
            @unlink($factures['file']);
        }
    }
}

if ($_GET['block'] == "commentaire") {
//    $getCommande = Functions::getCommandeByID($_GET['id']);
    if (isWebmaster() || isReferer()) {
        $dbh->query('DELETE FROM commentaires WHERE user="' . $idUser . '" AND typeuser="' . $idTypeUser . '" AND id=' . intval($_GET['id']));
    }
    if (isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer()) {
        $dbh->query('DELETE FROM commentaires WHERE id=' . intval($_GET['id']));
    }
}

if ($_GET['block'] == "repropositionTache") {
    $getCommande = Functions::getJobByID($_GET['id']);
    if ($getCommande) {
        $getCommandeProjets = Functions::getCommandeByID($getCommande['siteID']);
    }

    if (isSu() || ((isReferer()) && strpos($_SERVER['HTTP_REFERER'], '/backlinks') && $getCommande['affectedto'] == $idUser) && $getCommande && $getCommandeProjets) {
        $relanceTache = false;
        $referenceur_id = $getCommande['affectedto'];
        if (isReferer()) {
            $referenceur_id = $idUser;
        }
        if (!Functions::getDoublon("restartprojects", "project_id", $getCommande['siteID'], "annuaire_id", $getCommande['annuaireID'])) {
            $relanceTache = $dbh->query('INSERT INTO restartprojects VALUES("", "' . $getCommande['siteID'] . '", "' . $getCommande['annuaireID'] . '", "' . $referenceur_id . '", "' . time() . '", "' . time() . '", "' . time() . '", "' . time() . '", "' . $getCommande['adminApprouvedTime'] . '", "' . $idUser . '", "1")');
        } else {
            $relanceTache = $dbh->query('UPDATE restartprojects SET referenceur_id = "' . $referenceur_id . '", LatestRestart = "' . time() . '", RestartedBy = "' . $idUser . '", RestartCount = RestartCount+1 WHERE project_id = "' . $getCommande['siteID'] . '" AND annuaire_id = "' . $getCommande['annuaireID'] . '"');
        }
        if (!Functions::getDoublon("soumissionsage", "projet_id", $getCommande['siteID'], "annuaire_id", $getCommande['annuaireID'])) {
            $relanceTache = $dbh->query('INSERT INTO soumissionsage VALUES("", "' . $getCommande['siteID'] . '", "' . $getCommande['annuaireID'] . '", "' . $referenceur_id . '", "0", "1", "' . time() . '")');
        } else {
            $relanceTache = $dbh->query('UPDATE soumissionsage SET age = "0" WHERE projet_id = "' . $getCommande['siteID'] . '" AND annuaire_id = "' . $getCommande['annuaireID'] . '"');
        }
        echo 1;
    }
    header('location: ' . $_SERVER['HTTP_REFERER']);
}

if ($_GET['block'] == "revoquerjobs") {
    $getCommande = Functions::getJobByID($_GET['id']);

    $jobsModel = new Jobs();
    $annuaireModel = new Annuaire();

    if ($getCommande) {
        $getCommandeProjets = Functions::getCommandeByID($getCommande['siteID']);
    }
//    print_r($getCommandeProjets);

    if ((isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer() || isWebmaster($idTypeUser)) && $getCommande && $getCommandeProjets) {
        error_reporting(E_ERROR);

        // TODO  Checking rejection bonus-tasks
        // get project comment ( project_id, annuiare_id, type_user = 3  (writer)
        $project_writer_comment = Functions::getOneCommentaire($getCommande['siteID'],$getCommande['annuaireID'],3);
        $project_writer_submission = Functions::getAllSoumissionsAge($getCommande['siteID'],$getCommande['annuaireID']);

        $delete = false;
        if ($getCommande['adminApprouved'] == 3) {
            $delete = true;
        }

        // checking bonus => overriding project writer_id
        // if related project writer id != writer id for comment - as bonus

        if ( isset($project_writer_comment['user']) && ( $project_writer_comment['user'] != $getCommandeProjets['affectedTO'] ) ){
            $getCommandeProjets['affectedTO'] = $project_writer_comment['user'];
        }

        if (!empty($project_writer_submission)){
            $project_writer_submission = array_shift($project_writer_submission);
        }

        if ( empty($project_writer_comment) && isset($project_writer_submission['referenceur_id']) && ( $project_writer_submission['referenceur_id'] != $getCommandeProjets['affectedTO'] ) ){
            $getCommandeProjets['affectedTO'] = $project_writer_submission['referenceur_id'];
        }

        if (isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer() || (isWebmaster($idTypeUser) && $getCommandeProjets['proprietaire'] == $idUser) || (isReferer($idTypeUser) && $getCommandeProjets['affectedTO'] == $idUser)) {
           $delete = $dbh->query('DELETE FROM jobs WHERE id=' . $getCommande['id']);
           $delete_ = $dbh->query('DELETE FROM commentaires WHERE typeuser = 3 AND idprojet="' . $getCommande['siteID'] . '" AND idannuaire="' . $getCommande['annuaireID'] . '"');
           $delete__ = $dbh->query('DELETE FROM soumissionsage WHERE projet_id="' . $getCommande['siteID'] . '" AND annuaire_id="' . $getCommande['annuaireID'] . '"');

           $jobsModel->insertRejected($getCommande['siteID'],$getCommande['annuaireID']);
        }


//        }

        if ($getCommande['adminApprouved'] == 2) {
            $refererStats = false;
            $webmasterStats = false;

            $solde = 0;
            $soldeWebmaster = 0;

            $getAnnuaireInfos = Functions::getAnnuaire($getCommande['annuaireID']);

            // checking bonus => overriding project writer_id
            // if related project writer id != writer id for comment - as bonus

            //var_dump($getCommandeProjets['affectedTO']);
            if ( isset($project_writer_comment['user']) && ( $project_writer_comment['user'] != $getCommandeProjets['affectedTO'] ) ){
                $getCommandeProjets['affectedTO'] = $project_writer_comment['user'];
            }

            if ( empty($project_writer_comment) && isset($project_writer_submission['referenceur_id']) && ( $project_writer_submission['referenceur_id'] != $getCommandeProjets['affectedTO'] ) ){
                $getCommandeProjets['affectedTO'] = $project_writer_submission['referenceur_id'];
            }

            // writer balance
            $solde = Functions::getSolde($getCommandeProjets['affectedTO']);

             // webmaster balance
            $soldeWebmaster = Functions::getSolde($getCommandeProjets['proprietaire']);

            $remuneration = Functions::getRemunueration($getCommandeProjets['affectedTO']);

            // webmaster renumeration
            $tarifWebmaster = Functions::getRemunuerationWebmaster($getCommandeProjets['proprietaire']);

            //var_dump($solde,$soldeWebmaster,$tarifWebmaster);

            // get bonuses & calculating bonuses
            $projet_id = $getCommandeProjets['id'];

            $getSupps = Functions::getPersoWebmasterCompte($projet_id);
            $bonusAdded = 0;
            $bonusAdded_Web = 0;

            if ($getSupps && in_array($getCommande['annuaireID'], $getSupps[$projet_id]['annuairesList']) && $getSupps[$projet_id]['active'] == 1) {
                $bonusAdded = floatval($getSupps[$projet_id]['costReferenceur']);
                $bonusAdded_Web = floatval($getSupps[$projet_id]['costWebmaster']);
                if ($bonusAdded == 0) {
                    $bonusAdded = $ComptePersoReferenceur;
                }
                if ($bonusAdded_Web == 0) {
                    $bonusAdded_Web = $ComptePersoWebmaster;
                }
            }


            $bonus = 0.0;
            // calculating $varBonus
            // yes,yes,yes COPYPASTE from topHaut.php!
            // I don`t understand this fucking Benin`s shit!!!!!!
            $number_of_sommisions = 0;
            $number_of_sommisions_found = 0;
            $number_of_sommisions_notfoundyet = 0;
            $number_of_sommisions_notfound = 0;

            $NotYetfoundSoumissions = 0;
            $foundSoumissions = 0;
            $number_of_sommisions_notfoundandnotfoudyet = 0;

            $writer_id = $getCommandeProjets['affectedTO'];

            $getResUsers = Functions::GetTauxUser($writer_id);

            if ($getResUsers) {
                $number_of_sommisions = $getResUsers['total'];
                $number_of_sommisions_found = $getResUsers['found'];
                $number_of_sommisions_notfoundyet = $getResUsers['notfoundyet'];
                $number_of_sommisions_notfound = $getResUsers['notfound'];
                $number_of_sommisions_notfoundandnotfoudyet = $number_of_sommisions_notfoundyet + $number_of_sommisions_notfound;

                $foundSoumissions = Functions::getPourcentage($number_of_sommisions_found, $number_of_sommisions);
                $NotYetfoundSoumissions = Functions::getPourcentage($number_of_sommisions_notfoundyet, $number_of_sommisions);
                $NotfoundSoumissions = round(100 - ($foundSoumissions + $NotYetfoundSoumissions), 1);
            }

            $monthCurrent = date('n');
            $yearCurrent = date('Y');

            if (intval($monthCurrent) == 1) {
                $monthChoosed = 12;
                $yearChoosed = $yearCurrent - 1;
            } else {
                $monthChoosed = $monthCurrent - 1;
                $yearChoosed = $yearCurrent;
            }

            $countLastMonth = Functions::getCountSoumissions($monthChoosed, $yearChoosed, $writer_id);
            $countThisMonth = Functions::getCountSoumissions($monthCurrent, $yearCurrent, $writer_id);

            if ($countLastMonth > MinLastMonth && $foundSoumissions > PercentLinkFounded) {
                $bonus = $_SESSION['allParameters']['bonus']["valeur"];
                if ($foundSoumissions > PercentLinkFounded) {
                    $quotien = (intval($foundSoumissions - PercentLinkFounded) * BonusSurLiensTrouves);
                    $bonus = $bonus + $quotien;
                }
                if ($countThisMonth > ConditionBonusSurCentaineSoumissions && $countThisMonth > MinCurrentMonth) {
                    $bonusToAddedCentaine = (intval($countThisMonth * BonusSurCentaineSoumissions) * 0.01);
                    $bonus = $bonus + $bonusToAddedCentaine;
                }
            }

            $remuneration = Functions::getRemunueration($writer_id);
            $bonusNew = ($remunerationDefault + $bonus) - $remuneration;
            if ($bonusNew <= 0) {
                $bonusToAddedCentaine = 0;
                $quotien = 0;
                $bonusNew = 0;
            }

            //var_dump($bonusAdded);
            //var_dump($bonusAdded_Web);
            //var_dump($bonus);
            //var_dump($remuneration);
            $CoutWebmaster = $tarifWebmaster + $getAnnuaireInfos['tarifW'] + $bonusAdded_Web;
            $CoutReferenceur = $remuneration + $getAnnuaireInfos['tarifR'] + $bonusAdded + $bonusNew;


            // calculate extra costs
            $annuaire_list = Functions::getAnnuaireNameById($getCommandeProjets['annuaire']);
            $LIST_ANNUAIRE_WORDS_COUNT = $annuaire_list['words_count'];

            //var_dump($LIST_ANNUAIRE_WORDS_COUNT);

            $writer_tarif_redaction = 0.0;
            if (isReferer()){
                // specified for writer tarif
                $writer_tarif_redaction = floatval($_SESSION['connected']['tarif_redaction']);
                if (!$writer_tarif_redaction){ // common tarif
                    $writer_tarif_redaction = floatval($_SESSION['allParameters']['writer_price_100_words']['valeur']);
                }
            }

            if (isSu() || isAdmin() || isWebmaster()){
                $current_writer_id = $getCommandeProjets['affectedTO'];
                if (!empty($current_writer_id)){
                    $userInfo = Functions::getUserInfos($current_writer_id);

                    $writer_tarif_redaction = floatval($userInfo['tarif_redaction']);
                    if (!$writer_tarif_redaction){ // common tarif
                        $writer_tarif_redaction = floatval($_SESSION['allParameters']['writer_price_100_words']['valeur']);
                    }
                }
            }


            $writer_extra_cost = 0;
            $webmaster_extra_cost = 0;
            //var_dump($writer_tarif_redaction);

            if ( $LIST_ANNUAIRE_WORDS_COUNT > 0 ){
                $annuaire_counts['min_words_count'] = $getAnnuaireInfos['min_words_count'];
                $annuaire_counts['max_words_count'] = $getAnnuaireInfos['max_words_count'];
                $writer_extra_cost = $annuaireModel->calculateExtraPriceForWriter($annuaire_counts,$LIST_ANNUAIRE_WORDS_COUNT,$writer_tarif_redaction);
                $webmaster_extra_cost = $annuaireModel->calculateExtraPrice($annuaire_counts,$LIST_ANNUAIRE_WORDS_COUNT);
            }

            //var_dump($writer_extra_cost);

            // add extra costs
            if ($writer_extra_cost > 0){
                $CoutReferenceur += $writer_extra_cost;
            }
            if ($webmaster_extra_cost > 0){
                $CoutWebmaster += $webmaster_extra_cost;
            }

            $newSolde = $solde - $CoutReferenceur; // reduce writer balance
            $newSoldeWebmaster = $soldeWebmaster + ($CoutWebmaster); // increasing webmaster balance

            $repnSend = "yes";

            //var_dump($CoutWebmaster,$CoutReferenceur,'Soldes',$newSolde,$newSoldeWebmaster);


            //die;

            //$delete = true;
            //$delete_ = true;
            //$delete__ = true;

            // updating webmaster & writers balance
            //var_dump($getCommandeProjets['proprietaire'],$writer_id);
            if ($delete || $delete_ || $delete__){
                try {
                    $dbh->beginTransaction();
                    $setWebmaster = $dbh->query('UPDATE utilisateurs SET solde = "' . $newSoldeWebmaster . '" WHERE id=' . $getCommandeProjets['proprietaire']);
//                        // remove transaction when admin reject a task of a writer (for webmaster)
                    $transaction = new Transaction($dbh, $getCommandeProjets['proprietaire']);
                    $transaction->removeTransaction(0, $CoutWebmaster, $getCommandeProjets['id']);
                    if ($setWebmaster) {
                        $reSo = $dbh->query('UPDATE utilisateurs SET solde = "' . $newSolde . '" WHERE id=' . $writer_id);
                        // remove transaction when admin reject a task of a writer (for writer)
                        $transaction = new Transaction($dbh, $writer_id);
                        $transaction->removeTransaction($CoutReferenceur, 0, $getCommandeProjets['id']);
                    }
                    $dbh->commit();
                }catch (PDOException $e){
                    //var_dump($e);
                    $dbh->rollBack();
                }
            }

            /*
            if ($solde != "ERREUR404" && $delete) {
                $newSolde = $solde - ($thisRemuneration + $getAnnuaireInfos['tarifR']);
//                echo $newSolde;
                if (isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer()) {
                    $refererStats = $dbh->query('UPDATE utilisateurs SET solde = "' . $newSolde . '" WHERE id = ' . $getCommande['affectedto']);
                }
                if (isWebmaster($idTypeUser)) {
                    $refererStats = $dbh->query('UPDATE utilisateurs SET solde = "' . $newSolde . '" WHERE id = ' . $getCommande['affectedto']);
                }
            } else {
                $refererStats = true;
            }
            if ($soldeWebmaster != "ERREUR404" && $refererStats) {
                $newSoldeWebmasterOne = $soldeWebmaster + ($thisRemunerationWebmaster + $getAnnuaireInfos['tarifW']);
                if (isSu($idTypeUser) || isAdmin($idTypeUser) || isSuperReferer()) {
                    $webmasterStats = $dbh->query('UPDATE utilisateurs SET solde = "' . $newSoldeWebmasterOne . '" WHERE id = ' . $getCommandeProjets['proprietaire']);
                }
                if (isWebmaster($idTypeUser)) {
                    $webmasterStats = $dbh->query('UPDATE utilisateurs SET solde = "' . $newSoldeWebmasterOne . '" WHERE id = ' . $idUser);
                }
            } else {
                $webmasterStats = true;
            }*/

            if ($webmasterStats && $refererStats) {
                $delete = true;
            }
        }


//        header('location:./details.html?id=' . $_GET['id']);
        header('location: ' . $_SERVER['HTTP_REFERER']);
    }
}

if ($_GET['block'] == "annuaire") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM annuaires WHERE id=' . intval($_GET['id']));
    } else if (isWebmaster($idTypeUser)) {
        $dbh->query('DELETE FROM annuaires WHERE importedBy ="' . $idUser . '" AND id=' . intval($_GET['id']));
    }
    header('location: ' . $_SERVER['HTTP_REFERER']);
}

if ($_GET['block'] == "listannuaire") {
    $currentAnnuaireList = Functions::getOneAnnuaireListe(intval($_GET['id']));

    // checking access
    Functions::checkAccess(isWebmaster(),$idUser,$currentAnnuaireList['proprietaire']);

    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM annuaireslist WHERE id=' . intval($_GET['id']));
    }
    if (isWebmaster($idTypeUser)) {
        $dbh->query('UPDATE annuaireslist SET is_deleted = 1
                     WHERE proprietaire = "' . $idUser . '" AND id=' . intval($_GET['id']));

        //$dbh->query('DELETE FROM annuaireslist WHERE proprietaire = "' . $idUser . '" AND id=' . intval($_GET['id']));
    }
}

if ($_GET['block'] == "categorie") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM categories WHERE id=' . intval($_GET['id']));
    }
}

if ($_GET['block'] == "requete") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM requestpayment WHERE id=' . intval($_GET['id']));
    } else {
        $dbh->query('DELETE FROM requestpayment WHERE id=' . intval($_GET['id']) . ' AND requesterID = ' . $idUser);
    }
}

if ($_GET['block'] == "utitlisateur") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM utilisateurs WHERE id=' . intval($_GET['id']));
    }
}

if ($_GET['block'] == "contrat") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM contrats WHERE id=' . intval($_GET['id']));
    }
}

if ($_GET['block'] == "message") {
    if (isSu($idTypeUser) || isAdmin($idTypeUser)) {
        $dbh->query('DELETE FROM messages WHERE id=' . intval($_GET['id']));
    } else {

        $is = $dbh->query('SELECT * FROM messages WHERE id=' . intval($_GET['id']))->fetch();
        if ($is['sender'] == $idUser) {
            $dbh->query('UPDATE messages SET viewSender = 0 WHERE id=' . intval($_GET['id']));
        } else if ($is['receiver'] == $idUser) {
            $dbh->query('UPDATE messages SET viewReceiver = 0 WHERE id=' . intval($_GET['id']));
        } else {
            echo "";
        }
    }
}


if ($_GET['block'] == "redaction"){
    $model = new RedactionProjects($dbh);
    $project_id = $_GET['id'];

    if ($project_id){
        $project = $model->getProject($project_id);

        // checking access
        Functions::checkAccess(isWebmaster(),$idUser,$project['webmaster_id']);

        // webmaster
        if (isWebmaster() && ($idUser == $project['webmaster_id'])){
            $project_price = $project['amount'];
            $model->delete($project_id);
            $userModel = new User($idUser);
            $userModel->changeBalance($idUser,$project_price,true);

        }

        // admin
        if (isSu()|| isSuperReferer() || isAdmin()){
            $project_price = $project['amount'];
            $user_id = $project['webmaster_id'];
            $model->delete($project_id);
            $userModel = new User($idUser);
            $userModel->changeBalance($user_id,$project_price,true);
        }

        // save transaction
        $transaction = new Transaction($dbh, $project['webmaster_id']);
        $transaction->addTransaction("Annulation de votre projet de r�daction", $project['amount'], 0);
    }

}



$redirectURL = str_replace('?statut=error', '', $_SERVER['HTTP_REFERER']);

if ($_SESSION['alertLogincontent'] != "") {
    header('location:' . $redirectURL . "?statut=error");
} else {
    header('location:' . $redirectURL . "");
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php")
        ?>
        <div class="three-fourth">

            <div class="permalink">
                <h4><a nohref="#">Statut de l'op�ration en cours</a></h4>
            </div>

            <div class="notification success">
                <?PHP
                if ($specialMessage != "") {
                    ?>
                    <span>Success</span> <?PHP echo $specialMessage; ?>.
                    <?PHP
                } else {
                    ?>
                    <span>Success</span> Op�ration effectu�e avec succ�s.
                    <?PHP
                }
                ?>
            </div>



            <!-- Pagination -->

            <!-- End pagination -->
        </div>

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>