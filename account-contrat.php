<?PHP
session_start();

$a = 100;
$b = 1;
$page = 1;
include('files/includes/topHaut.php');

// fake-common contract
$contrat = Functions::getcontrat($idTypeUser);

// get writer types
$is_annuaire_writer = $_SESSION['connected']['annuaire_writer'];
$is_redaction_writer = $_SESSION['connected']['redaction_writer'];

$annuaire_contract_accepted = $_SESSION['connected']["contractAccepted"];
$redaction_contract_accepted = $_SESSION['connected']["redaction_contract_accepted"];

if ($is_annuaire_writer && !$annuaire_contract_accepted){
    $annuaire_contract = Functions::getcontrat($idTypeUser);
}

if ($is_redaction_writer && !$redaction_contract_accepted){
    $redaction_contract = Functions::getcontrat($idTypeUser,'redaction');
}

// set accepted contract by default for all not-writers
if (!isReferer() && (!isset($contrat['contrat']) || empty($contrat['contrat']) || $contrat['contrat']=="") ) {
    /*$_SESSION['connected']["contractAccepted"] = 1;
    $dbh->query('UPDATE utilisateurs SET contractAccepted = "' . $_SESSION['connected']["contractAccepted"] . '" WHERE id=' . $idUser);
    header("location:../compte/accueil.html");*/
} else {

    ?>

    <!--breadcrumbs ends -->
    <div class="container">
        <div class="one">

            <div class="permalink">
                <h4><a nohref="#">Veuillez lire et accepter le contrat des r�f�renceurs avant de continuer</a></h4>
            </div>
            <br/>
            <?PHP

            if (isset($_GET['statut']) && $_GET['statut'] != "") {
                ?>
                <div class="notification <?PHP echo $_GET['statut']; ?>" style="width:350px">
                    <p>
                        <!--<span><?PHP echo ucfirst($_GET['statut']); ?></span>--> 
                        <?PHP echo $_SESSION['alertLogincontent']; ?>
                    </p>
                </div>
            <?PHP } ?>


            <?php if (!empty($annuaire_contract)) : ?>
                <form action="compte/modification/contrat.html" method="post" style="margin-top:0px;float:left;">
                    <!--<button class="search-btn"></button>-->
                    <fieldset style="">
                        <label class="color">Contrat et r�gles d'utilisation : </label>
                    </fieldset>
                    <div class="color pre">
                        <?PHP
                        echo  $annuaire_contract['contrat'];
                        ?>
                    </div>

                    <input type="hidden" name="contratReponse" value="1"/>
                    <input type="submit" class="button color small round" value="Accepter le contrat" style="color:white;"/>

                </form>
            <?php endif; ?>

            <?php if (!empty($redaction_contract)) : ?>
                <form action="compte/modification/contrat.html" method="post" style="margin-top:0px;float:left;">
                    <!--<button class="search-btn"></button>-->
                    <fieldset style="clear:both;margin: 20px 0px 0px 0px;">
                        <label class="color">Contrat et r�gles d'utilisation : </label>
                    </fieldset>
                    <div class="color pre">
                        <?PHP
                        echo $redaction_contract['contrat'];
                        ?>
                    </div>
                    <input type="hidden" name="redaction_module" value="1"/>
                    <input type="hidden" name="contratReponse" value="1"/>
                    <input type="submit" class="button color small round" value="Accepter le contrat" style="color:white;"/>
                </form>
            <?php endif; ?>


        </div>
    </div>


    <?PHP
}
    include("files/includes/bottomBas.php");

?>