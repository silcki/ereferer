<?PHP
@session_start();

if (isset($_SESSION['connected'])) {
    header("location:compte/accueil.html");
}

$a = 2;
$page = 2;
$statut = "warning";
include('files/includes/topHaut.php');

if (!empty($_GET['hash']) && Functions::checkEmailFormat(base64_decode($_GET['hash']))) {
    $_GET['hash'] = base64_decode($_GET['hash']);
    $requete = "SELECT * FROM utilisateurs WHERE username='" . $_GET['hash'] . "'";
    $execution = $dbh->query($requete);
    $retour = $execution->fetch();
    if (isset($retour['email'])) {
        unset($_SESSION['alertLogincontent']);
        $setPass = false;
        $name = $_SESSION['allParameters']['logotext']["valeur"];

        if ($retour['active'] == 0) {
            $setPass = $dbh->query('UPDATE utilisateurs SET active=1 WHERE id=' . $retour['id']);

            if ($setPass) {

                if ($retour['lastIP'] > 0 && $retour['lastIP'] != 111111) {
                    if (!Functions::getDoublon("affiliation_affilies", "parrain", $retour['lastIP'], "affilie", $retour['id'])) {
                        $tarifWebmaster = Functions::getRemunuerationAffiliation($retour['lastIP']);
                        $setupAffiliation = $dbh->query('INSERT INTO affiliation_affilies VALUES(null, "' . $retour['lastIP'] . '", "' . $retour['id'] . '", "' . $tarifWebmaster . '", "' . date('m') . '", "' . date('Y') . '", "' . time() . '")');
                        if ($setupAffiliation) {
                            $soldeWebmaster = Functions::getSolde($retour['lastIP']);
                            $newBalance = $tarifWebmaster + $soldeWebmaster;
                            $reSo = $dbh->query('UPDATE utilisateurs SET solde = "' . $newBalance . '" WHERE id=' . $retour['lastIP']);
                        }
                    }
                }

                @require_once('./files/includes/phpmailer/class.phpmailer.php');
                $body = '';
                $body .= 'Cher ' . $retour['nom'] . ' ' . $retour['prenom'] . '<br/><br/>';
                $body .= 'Votre compte ' . $name . ' est � pr�sent actif.<br/><br/>';
                $body .= 'Votre solde est actuellement de 0 euro.<br/> ';
                $body .= 'Pensez � recharger votre compte afin de d�marrer votre r�f�rencement.<br/><br/>';
                $body .= 'Cordialement,<br/>';
                $body .= 'Emmanuel';

                $mail = new PHPMailer();
                $mail->Timeout = '30';
                $mail->CharSet = 'UTF-8';
                $mail->From = "noreply@ereferer.fr";
                $mail->FromName = $name;
//                $mail->addReplyTo($datas['repondre'], $datas['expediteur']);
                $mail->Subject = "Votre compte " . $name . " est activ�.";
                $mail->AltBody = "";
                $mail->IsHTML(true);
                $mail->MsgHTML($body);
                $mail->AddAddress($retour['username'], "");
                $mail->Send();

                $statut = "success";
                $_SESSION['alertLogincontent'] = "";
            }
        } else {
            $_SESSION['alertLogincontent'] = "Ce compte a d�j� �t� activ�. Vous pouvez vous connecter.";
        }
    } else {
        $_SESSION['alertLogincontent'] = "Ce compte est introuvable. Veuillez r�essayer svp.";
    }
} else {
    $_SESSION['alertLogincontent'] = "Ce compte est introuvable. Veuillez r�essayer svp.";
}


if (isset($_SESSION['alertLogincontent']) && $_SESSION['alertLogincontent'] != "") {
//    header('location:reset.html?statut=error');
}
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <div class="permalink">
            <h4><a nohref="#">Activation de compte.</a></h4>
        </div>

        <div class="notification <?PHP echo $statut; ?>" style="width:500px;">
            <?PHP
            if (!isset($_SESSION['alertLogincontent']) || $_SESSION['alertLogincontent'] == "") {
                ?>
                <p>
                    <span>Succ�s</span> 
                    Ce compte est � pr�sent actif. <br/>
                    Vous pouvez d�s � pr�sent vous connecter.<br/><br/>

                    Merci.
                </p>
                <?PHP
            } else {
                ?>
                <p>
                    <span>Attention: </span> 
                    <?PHP
                    echo $_SESSION['alertLogincontent'];
                    ?>
                    <br/>
                </p>
                <?PHP
            }
            ?>
        </div>



        <!-- Pagination -->

        <!-- End pagination -->

    </div>
</div>
<?PHP
include("files/includes/bottomBas.php")
?>