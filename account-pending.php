<?PHP
@session_start();

$a = 5;
if (isset($_GET['section']) && !empty($_GET['section'])) {
    if ($_GET['section'] == "paiement") {
        $p = 3;
        $b = 6;
    } else if ($_GET['section'] == "rechargement") {
        $p = 4;
        $b = 7;
    } else if ($_GET['section'] == "historique") {
        $p = 1;
        $b = 11;
    } else {
        $_GET['section'] = "rechargement";
        $p = 2;
        $b = 7;
    }
} else {
    $_GET['section'] = "rechargement";
    $p = 1;
    $b = 7;
}
$page = 1;
include('files/includes/topHaut.php');
isRight("3,4");
?>


<!--breadcrumbs ends -->
<div class="container">
    <div class="one">

        <?PHP
        include("files/includes/menu.php");
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            $_GET['id'] = 0;
        }
        ?>
        <div style="position:absolute;top:0px;right:0;margin-right:20px;">
            <?PHP
            if (isSu()) {
                echo "Pagination : <input type='' id='paginationExpress' value='" . $pagination . "' style='width:40px;height:20px;border:1px solid red;text-align:center;vertical-align:middle'/>";
                echo "&nbsp;<input type='button' value='ok' id='paginationExpressButton' class='buttonSend button color small round' style='padding:0px;'/>";
            }
            ?>
        </div>
        <div class="three-fourth">

<!--        <h4>D�tails du site : <?PHP echo $getCommande['lien']; ?></h4>-->

            <?PHP
            if ($_GET['section'] == "paiement") {
                echo '<h4>Demande de paiement de compte r�f�renceurs en attente.</h4>';
            } else if ($_GET['section'] == "historique") {
                echo '<h4>Historique des transactions sur la plateforme.</h4>';
            } else {
                echo '<h4>Demande de rechargement de compte webmasters en attente.</h4>';
            }

            $requests = Functions::getPaiementsList($p);
            $count = 0;

            $count = count($requests);

            include("pages.php");


            if ($count > 0) {
                ?>
                <table class="simple-table">
                    <tbody>
                        <tr>
                            <th>
                                Utilisateurs

                            </th>
                            <th>
                                Montant
                            </th>
                            <th>
                                Demande
                            </th>
                            <?PHP if ($_GET['section'] == "historique") { ?>
                                <th>
                                    R�ponse
                                </th>
                            <?PHP } ?>
                            <?PHP if ($_GET['section'] != "historique") { ?>
                                <th>
                                    Actions
                                </th>
                            <?PHP } ?>

                        </tr>
                        <?PHP
                        while ($limit1 <= $limit2) {
                            ?>
                            <tr>
                                <td>
                                    <?PHP echo Functions::getfullname(intval($requests[$limit1]['requesterID'])); ?><br/>
                                    <?PHP
                                    if ($_GET['section'] == "historique") {
                                        echo "(" . $arrayLvl[$requests[$limit1]['type']] . ")";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?PHP
                                    echo $requests[$limit1]['requestAmount'];

                                    if ($requests[$limit1]['type'] == 4) {
                                        echo " <i class='icon-euro'></i>";
                                    } else {
                                        echo " <i class='icon-dollar'></i>";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?PHP echo date("d/m/Y � H:i:s", $requests[$limit1]['time']); ?>
                                </td>
                                <?PHP if ($_GET['section'] == "historique") { ?>
                                    <td>
                                        <?PHP echo date("d/m/Y � H:i:s", $requests[$limit1]['answerTime']); ?>
                                    </td>
                                <?PHP } ?>
                                <?PHP if ($_GET['section'] != "historique") { ?>
                                    <td>

                                        <a href="compte/modification/validerrequete.html?id=<?PHP echo $requests[$limit1]['id']; ?>"  onclick="return Paiementconfirmation();" ><i class="icon-check"></i> Approuver <?PHP echo ucfirst($_GET['section']); ?></a><br/>
                                        <a href="compte/supprimer/requete.html?id=<?PHP echo $requests[$limit1]['id']; ?>" onclick="return confirmation();" ><i class="icon-remove"></i> Rejeter la demande</a>

                                    </td>
                                <?PHP } ?>
                                                                                                                            <!--                            <td>
                                                                                                                                                        <a href="" ></a>
                                                                                                                                                    </td>-->

                            </tr>
                            <?PHP
                            $limit1++;
                        }
                        ?>
                    </tbody>
                </table>
                <!-- Pagination -->
                <?PHP
                if ($nbre_result > $pagination) {
                    echo $list_page;
                }
            } else {
                ?>
                <div align="center" width="500" style="width:700px;margin:auto;">
                    <h1>Aucun r�sultat � afficher.</h1>

                </div>

                <?PHP
            }
            ?>
            <!-- End pagination -->
        </div>


    </div>


</div>
<?PHP
include("files/includes/bottomBas.php")
?>